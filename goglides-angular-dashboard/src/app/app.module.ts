import { ReviewService } from './service/review.service';
import { BookingCartService } from './service/booking-cart.service';
import { BookingService } from './service/booking.service';
import { ListingMultimediaService } from './service/listing-multimedia.service';
import { ListingAddonsService } from './service/listing-addons.service';
import { ListingTransportRouteService } from './service/listing-transport-route.service';
import { BusinessCertificateService } from './service/business-certificate.service';
import { BusinessContactPersonService } from './service/business-contact-person.service';
import { CheckListService } from './service/check-list.service';
import { StatusService } from './service/status.service';
import { ListingService } from './service/listing.service';
import { IncludeExcludeService } from './service/include-exclude.service';
import { CategoryService } from './service/category.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';




import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { ListingItineraryService } from './service/listing-itinerary.service';
import { CommonService } from './service/common.service';
import { KeycloakService } from './keycloak-service/keycloak.service';
import { UserService } from './service/user.service';

import { BusinessDirService } from './service/business-dir.service';
import { UploadSerivce } from './service/file.upload.service';
import { DatePipe } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ToastModule } from 'ng2-toastr';
import { ToasterMessageService } from './service/toaster-message.service';
import { ListingFactService } from './service/listing-fact.service';
import { ListingDestinationService } from './service/listing-destination.service';
import { ListingChecklistService } from './service/listing-checklist.service';
import { ListingIncludeService } from './service/listing-include.service';
import { ListingPriceService } from './service/listing-price.service';
import { ListingFaqService } from './service/listing-faq.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ForexService } from './service/forex.service';
import {NgPipesModule, TrimPipe} from 'ngx-pipes';
import { MyDatePickerModule } from 'mydatepicker';
import { ListingSettingService } from './service/listing-setting.service';
import { KeycloakHttpInterceptorService } from './keycloak-http-interceptor.service';
@NgModule({
  imports: [
    
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    NgPipesModule,
    ToastModule.forRoot(),
    NgbModule.forRoot(),
    
    // AgmCoreModule.forRoot({
    //   apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    // }),
    // ToastrModule.forRoot(), 
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
  ],

  schemas: [NO_ERRORS_SCHEMA],
  providers: [CategoryService, IncludeExcludeService,

    ListingService, ListingItineraryService,
    CommonService, KeycloakService,
    StatusService, CheckListService, UserService,
    BusinessDirService, UploadSerivce, DatePipe, BusinessContactPersonService, ToasterMessageService,
    BusinessCertificateService,ListingFactService,ListingDestinationService,
    ListingChecklistService,
    ListingTransportRouteService,ListingIncludeService,ListingPriceService,
    ListingAddonsService,ListingFaqService,ListingMultimediaService,BookingService,ListingSettingService,
    BookingCartService,ForexService, TrimPipe,ReviewService
    ,{
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakHttpInterceptorService,
      multi: true
    },
  ],

  bootstrap: [AppComponent],
})
export class AppModule { }
