import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
    { path: '/bookings', title: 'Bookings', icon: 'shopping_cart', class: '' },
    { path: '/packages/list', title: 'Packages', icon: 'list', class: '' },
    { path: '/businessdirectory/list', title: 'Business Directory', icon: 'business', class: '' },
    { path: '/catalog', title: 'Catalog', icon: 'layers', class: '' },
    { path: '/settings/list', title: 'Settings', icon: 'settings', class: '' },
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() { }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
