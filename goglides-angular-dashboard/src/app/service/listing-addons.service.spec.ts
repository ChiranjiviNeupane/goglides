import { TestBed, inject } from '@angular/core/testing';

import { ListingAddonsService } from './listing-addons.service';

describe('ListingAddonsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingAddonsService]
    });
  });

  it('should be created', inject([ListingAddonsService], (service: ListingAddonsService) => {
    expect(service).toBeTruthy();
  }));
});
