import { TestBed, inject } from '@angular/core/testing';

import { BusinessContactPersonService } from './business-contact-person.service';

describe('BusinessContactPersonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusinessContactPersonService]
    });
  });

  it('should be created', inject([BusinessContactPersonService], (service: BusinessContactPersonService) => {
    expect(service).toBeTruthy();
  }));
});
