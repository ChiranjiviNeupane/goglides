import { BusinessCertificateService } from './business-certificate.service';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { DatePipe } from "@angular/common";
import { environment } from "environments/environment";
import * as S3 from 'aws-sdk/clients/s3';
import { ImageCropperComponent } from "ngx-image-cropper";
import { BusinessDirService } from './business-dir.service';
import { CommonService } from './common.service';
import { ListingMultimeda } from '../model/listing-multimedia';
import { ListingMultimediaService } from './listing-multimedia.service';
import { ToasterMessageService } from './toaster-message.service';


@Injectable()
export class UploadSerivce {
  public fileUploadResult: boolean = false;
  public imageUrl: string
  businessCertificateInfo: any
  businessCertificateDetail: any
  userId: number;
  businessId: number;
  urlStatus: string

  private url: string = environment.baseUrl + "businessCertificate";

  constructor(
    private datePipe: DatePipe,
    public httpClient: HttpClient,
    public commonService: CommonService,
    public businessCertificateService: BusinessCertificateService,
    private businessDirService: BusinessDirService,
    private listingMultimediaService: ListingMultimediaService,
    private toasterMessageService: ToasterMessageService,
  ) { }



  uploadfile(file: any, businessId: number, userId: number, vcr) {
    let date = new Date();
    let name = this.datePipe.transform(date, 'yyyyMMddHHmmss');
    name = name + this.datePipe.transform(date, 'sss');
    const bucket = new S3(environment.aws);

    let saveImageUrl = this.businessDirService

    let getCertificateDetail = function (logoUrl: String, user: number) {
      return {
        logoUrl: logoUrl,
        user: {
          id: user
        }
      }
    }

    const params = {
      Bucket: environment.container,
      Key: "original/business/logo/" + name + ".png",
      Body: file,
      ContentType: 'image/png',
      ACL: 'public-read'
    };

    let businessCertificate = this.businessCertificateService;
    let toasterMessage = this.toasterMessageService

    bucket.upload(params, function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        this.file
      }
      saveImageUrl.updateBusinessLogo(businessId, (getCertificateDetail(data['Location'], userId))).subscribe(
        (data) => {
          toasterMessage.success(vcr, "Logo saved !!!", "Success");
        }
      );

    }).on('httpUploadProgress', function (progress) {
      let value = Math.round(progress.loaded / progress.total * 100)
      businessCertificate.logoProgressPercentage.next(progress);
    });;
  }
  //called when uploading business Certificate

  uploadBusinessCertificate(file: File, businessDirectoryId: number, userId: number, vcr) {
    const s3 = new S3(environment.aws);
    let date = new Date();
    let name = this.datePipe.transform(date, 'yyyyMMddHHmmss');
    name = name + this.datePipe.transform(date, 'sss');
    console.log(file)
    let typeArray: any = file.type.split('/');
    let type = typeArray[1];
    let newName = name + "." + type;
    let contentType = ""
    switch (type) {
      case "pdf": {
        contentType = "application/pdf"
        break;
      }
      case "png": {
        contentType = "image/png"
        break;
      }
      case "jpg": {
        contentType = "image/jpg"
        break;
      }
      case "jpeg": {
        contentType = "image/jpeg"
        break;
      }
    }

    const params = {
      Bucket: environment.container,
      Key: "certificates/businessCertificate/certificate/" + newName,
      Body: file,
      ContentType: contentType,
      ACL: 'public-read'
    };

    let getCertificateDetail = function (fileType: String, fileUrl: string, businessDirectory: number, user: number) {
      return {
        fileType: fileType,
        fileUrl: fileUrl,
        user: {
          id: user
        },
        businessDirectory: {
          id: businessDirectory
        }
      }
    }
    let toasterMessage = this.toasterMessageService
    let businessCertificate = this.businessCertificateService;
    let saveImageUrl = this.businessCertificateService
    var uploadData = s3.upload(params, function (err, data) {
      if (err) {
        return err;
      }
      saveImageUrl.saveBusinessCertificate((getCertificateDetail('_certificate', data['Location'], businessDirectoryId, userId))).subscribe(
        (data) => {
          toasterMessage.success(vcr, "Business Certificate saved !!!", "Success");
        }
      );
    }).on('httpUploadProgress', function (progress) {
      let value = Math.round(progress.loaded / progress.total * 100)
      businessCertificate.businessProgressPercentage.next(progress);
    });
  }

  //called when uloading BusinessPan

  uploadBusinessPan(file: File, businessDirectoryId: number, userId: number, vcr) {
    const s3 = new S3(environment.aws);
    let date = new Date();
    let name = this.datePipe.transform(date, 'yyyyMMddHHmmss');
    name = name + this.datePipe.transform(date, 'sss');
    let typeArray: any = file.type.split('/');
    let type = typeArray[1];
    let newName = name + "." + type;
    let contentType = ""
    switch (type) {
      case "pdf": {
        contentType = "application/pdf"
        break;
      }
      case "png": {
        contentType = "image/png"
        break;
      }
      case "jpg": {
        contentType = "image/jpg"
        break;
      }
      case "jpeg": {
        contentType = "image/jpeg"
        break;
      }
    }
    const params = {
      Bucket: environment.container,
      Key: "certificates/businessCertificate/pan/" + newName,
      Body: file,
      ContentType: contentType,
      ACL: 'public-read'
    };

    let getCertificateDetail = function (fileType: String, fileUrl: string, businessDirectory: number, user: number) {
      return {
        fileType: fileType,
        fileUrl: fileUrl,
        user: {
          id: user
        },
        businessDirectory: {
          id: businessDirectory
        }
      }
    }
    let toasterMessage = this.toasterMessageService
    let businessCertificate = this.businessCertificateService;
    let saveImageUrl = this.businessCertificateService
    var uploadData = s3.upload(params, function (err, data) {
      if (err) {
        return err;
      }
      saveImageUrl.saveBusinessPan((getCertificateDetail('_pan', data['Location'], businessDirectoryId, userId))).subscribe(
        (data) => {
          this.set
          toasterMessage.success(vcr, "Business Document saved !!!", "Success");
        }
      );
    }).on('httpUploadProgress', function (progress) {
      let value = Math.round(progress.loaded / progress.total * 100)
      businessCertificate.panProgressPercentage.next(progress);
    });
  }

  mulipleUpload(files, object, imageType, vcr) {
    let length = files.length
    let counter = 0;
    const bucket = new S3(environment.aws);
    let file: any;
    let date = new Date();
    if (files !== null) {
      for (let i = 0; i < files.length; i++) {
        file = files[i]

        let name = this.datePipe.transform(date, "yyyyMMddHHmmss")
        const params = {
          Bucket: environment.container,
          Key: "original/listing/" + imageType + "/" + name + i + ".jpeg",
          Body: file,
          ContentType: 'image/jpeg',
          ACL: 'public-read',
        };

        let lisitngMultimedia = this.listingMultimediaService
        let toasterMessage = this.toasterMessageService


        bucket.upload(params, function (err, data) {
          if (err) {
            console.log(err)
            return false
          }
          object['fileUrl'] = data['Location']
          lisitngMultimedia.saveMultiMedia(object).subscribe(data => {
            counter++;
            if (counter === length) {
              toasterMessage.success(vcr, imageType +" saved !!!", "Success");
            }
          });
        }).on('httpUploadProgress', function (progress) {
          console.log(progress)
          if(imageType === "Gallery")
            lisitngMultimedia.galleryProgressValue.next(progress);
          else
            lisitngMultimedia.mapProgressValue.next(progress);
        });
      }
    }
  }

  uploadFeatureAndBannerImage(file, object, imageType, vcr) {
    if (file !== null) {
      const bucket = new S3(environment.aws);
      let date = new Date();
      let name = this.datePipe.transform(date, "yyyyMMddHHmmss")
      const params = {
        Bucket: environment.container,
        Key: "original/listing/" + imageType + "/" + name + ".jpeg",
        Body: file,
        ContentType: 'image/jpeg',
        ACL: 'public-read',
      };

      let lisitngMultimedia = this.listingMultimediaService
      let toasterMessage = this.toasterMessageService

      bucket.upload(params, function (err, data) {
        if (err) {
          console.log(err)
          return false
        }
        console.log(object)
        if (object['id'] === null || object['id'] === undefined || object['id'] === "") {
          object['fileUrl'] = data['Location']
          lisitngMultimedia.saveMultiMedia(object).subscribe(data => {
            toasterMessage.success(vcr, imageType + " Image saved !!!", "Success");

          });
        } else {
          object['fileUrl'] = data['Location']
          lisitngMultimedia.updateMultiMedia(object).subscribe(data => {
            toasterMessage.success(vcr, imageType + " Image updated !!!", "Success");
          });
        }
      }).on('httpUploadProgress', function (progress) {
        let value = Math.round(progress.loaded / progress.total * 100)
        if (imageType === 'Feature')
          lisitngMultimedia.featureProgressValue.next(progress);
        else if(imageType === 'Banner')
          lisitngMultimedia.bannerProgressValue.next(progress);
        
        return true;
      });
    }
  }

}