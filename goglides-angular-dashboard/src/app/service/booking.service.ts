import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BookingService {
  private url: string =environment.baseUrl;

  constructor(private http: HttpClient) { }
  getFilterOption(businessId:number,packageId:number){
    return this.http.get(this.url+"booking/packageSearchComponent?businessDirectoryId="+businessId+"&listingId="+packageId)
  }
  getPricing(data){
    // localhost:8082/v1/manage/booking/packageSearch/?businessDirectoryId=1&listingId=1&type=Solo&duration=00:00:00:30
    return this.http.get(this.url+"booking/packageSearch/?businessDirectoryId="+data.busId+"&listingId="+data.packageId+"&type="+data.type
    +"&duration="+data.duration+"&schedule="+data.schedule)
  }
  saveBooking(data){
    return this.http.post(this.url+"booking/save",data)
  }

  updateBooking(id, data){
    return this.http.put(this.url+"booking/"+id,data);
  } 

  issueTicke(id){
    return this.http.get(this.url+"booking/issueTicket/?bookingId="+id);
  }
  searchBooking(seracghField,type){
    return this.http.get(this.url +"booking/bookingFilter?name="+seracghField+"&type="+type)
  }
  searchRecentBooking(){
    return this.http.get(this.url +"booking/showRecetBooking")
  }

  advanceFilter(from,to){
    return this.http.get(this.url +"booking/advanceFilterBooking?from="+from+"&to="+to)
  }

  updateBookingStatus(data){
    return this.http.post(this.url+"booking/update_status",data)
  }
}
