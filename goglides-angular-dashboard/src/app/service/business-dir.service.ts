import { ToasterMessageService } from './toaster-message.service';
import { BusinessDirectory } from '../model/business-directory';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operator/map';

@Injectable()
export class BusinessDirService {
  businessDirInfo: BusinessDirectory
  private url:string = environment.baseUrl+"businessDirectory"
  constructor( private http: HttpClient,
  ) { }

  public getBusinessDirInfo(){
    return this.businessDirInfo;
  }

  getAllBusiness(){
    return this.http.get(this.url)
  }
  getSingleBusiness(id :number){
    return this.http.get(this.url+"/"+id)
  }
  
  getBusinessById(businessId: number) {
    return this.http.get(this.url + "/" + businessId).map((data: Object) => {
      return data
    })
  }

  savaBusinessDirectoryGeneralInfo(data){
    return this.http.post(this.url, data);
  }

  saveBusinessContactPersonInfo(data){
    return this.http.post(this.url,data)
  }

  saveBusinessDirectoryLocation(businessId: number,data: any){
    return this.http.put(this.url+"/"+businessId,data);
  }
  saveBusinessDirSocialMedia(businessId: number, data: any){
    return this.http.put(this.url+"/"+businessId, data);
  }

  updateBusinessDirBasicInfo(businessId: number, data: any){
    return this.http.put(this.url+"/"+businessId, data).subscribe((data: BusinessDirectory) => {
      this.businessDirInfo=data;
    })
  }
  
  updateBusiness(id, data){
    return this.http.put(this.url+"/"+id, data)
  }


  updateBusinessDirLocationInfo(businessId: number, data: any){
    return this.http.put(this.url+"/"+businessId, data).subscribe((data: BusinessDirectory) => {
      this.businessDirInfo=data;
      console.log(data)
    })
  }

  updateBusinessLogo(businessId: number, data: any){
    return this.http.put(this.url+"/"+businessId, data);
  
  }

  public updateBusinessDirStatus(businessId: number, data:any){
    return  this.http.put(this.url+"/"+businessId,data);
    
  }

  public countPackages(id){
    return this.http.get(environment.baseUrl+"listing/countStatus?businessDirectoryId="+id+"&status=PUBLISH");
  }

}
