import { Subject } from 'rxjs';
import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ReviewService {
  public reviews = new Subject<Object>();

  constructor(
    private http: HttpClient
  ) { }


  public getReviews(refId:number,packageId:number){
    return this.http.get(environment.baseUrl+"review/packageReview/"+packageId)
  }

  updateReview(id, data){
    return this.http.put(environment.baseUrl+"review/"+id, data)
  }
}
