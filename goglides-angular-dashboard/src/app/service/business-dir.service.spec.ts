import { TestBed, inject } from '@angular/core/testing';

import { BusinessDirService } from './business-dir.service';

describe('BusinessDirService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusinessDirService]
    });
  });

  it('should be created', inject([BusinessDirService], (service: BusinessDirService) => {
    expect(service).toBeTruthy();
  }));
});
