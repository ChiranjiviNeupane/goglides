import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ListingTransportRouteService {

  private url: string =environment.baseUrl+"listingTransportRoute";
  constructor(private http: HttpClient) { }

  addListingTransportRoute(data:any){
    return this.http.post(this.url, data);
  }
  getTransportRoute(businessId:number,listingId:number){
    return this.http.get(environment.baseUrl+"businessDirectory/"+businessId+"/listing/"+listingId+"/listingTransportRoute")
  }

  deleteRoute(id){
    return this.http.delete(this.url+"/"+id);
  }

  update(id, data){
    return this.http.put(this.url+"/"+id, data);
  }

}
