import { TestBed, inject } from '@angular/core/testing';

import { ListingTransportRouteService } from './listing-transport-route.service';

describe('ListingTransportRouteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingTransportRouteService]
    });
  });

  it('should be created', inject([ListingTransportRouteService], (service: ListingTransportRouteService) => {
    expect(service).toBeTruthy();
  }));
});
