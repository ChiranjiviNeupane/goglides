import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BookingCartService {
  private url: string =environment.baseUrl;
  constructor(private http: HttpClient) { }
  checkCart(userId){
    return this.http.get(this.url+"bookingCart/checkCart?userId="+userId)
  }
  addCart(data){
    // return this.http.post(this.url+"bookingCart",data)
    return this.http.post(this.url+"booking/save",data)
  }
  saveBooking(id,data){
    return this.http.post(this.url+"booking/updateBookingItem/"+id,data)
  }
  
  view(id){
    return this.http.get(this.url+"booking/show/"+id)
  }

  viewAll(){
    return this.http.get(this.url+"booking/")
  }
  getAddons(businessId,packageId){

    return this.http.get(this.url+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingAddon")
  }
  saveAddons(data){
    return this.http.post(this.url+"booking/saveBookingAddon",data)
  }
  getTransportRout(businessId,packageId){
    // listingTransportRoute/routeList?businessDirectoryId=1&listingId=1
    return this.http.get(this.url+"listingTransportRoute/routeList?businessDirectoryId="+businessId+"&listingId="+packageId)
  }
  saveOtherInfo(data,id){
    return this.http.put(this.url+"booking/"+id,data)
  }

  
}
