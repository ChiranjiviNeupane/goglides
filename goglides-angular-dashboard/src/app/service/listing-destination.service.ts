import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ListingDestinationService {
  private url: string = environment.baseUrl + "listingDestination"
  businessDirectory : number;
  listing : number;
  constructor(private http: HttpClient) { }
  getListingDestByPackage(businessId:number,packageId:number){
    return this.http.get(environment.baseUrl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingDestination")
  }

  saveListingDestination(data){
    let res: Boolean =false
    return this.http.post(this.url, data);
  }

  updateDestination(id, data){
    return this.http.put(this.url+"/"+id, data);
  }

  deleteDestination(id){
    return this.http.delete(this.url+"/"+id);
  }

}
