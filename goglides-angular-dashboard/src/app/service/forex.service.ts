import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ForexService {
  private url: string =environment.baseUrl;

  constructor(private http: HttpClient) { }
    public countTotal() {
    return this.http.get(this.url + "forex/countData")
  }
  public countStatus(status: string) {
    return this.http.get(this.url + "forex/countStatus?status=" + status)
  }
  getForex(){
    return this.http.get(this.url+"forex")
  }
  addForex(data){
    return this.http.post(this.url+"forex",data)
  }
  updateForex(id,data){
    return this.http.put(this.url+"forex/"+id,data)
  }

}
