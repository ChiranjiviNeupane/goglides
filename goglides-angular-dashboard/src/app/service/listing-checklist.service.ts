import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Subject } from 'rxjs';

@Injectable()
export class ListingChecklistService {
  public url: String = environment.baseUrl 
  categoryCheckBoxArraySubject = new Subject<String[]>();
  status = new Subject<Boolean>()
  constructor(
    public http: HttpClient
  ) { }


  getProvideAndBring(categoryId: number){
    return this.http.get(this.url+"checkList/provideAndBring?id="+categoryId)
  }

  saveCheckList(data){
    return this.http.post(this.url+"listingChecklist",data).map((response)=>{
      const status =true;
      return status;
    });
  }
  viewCheckList(businessId:number,listingId:number){
    return this.http.get(environment.baseUrl+"businessDirectory/"+businessId+"/listing/"+listingId+"/listingChecklist")
  }

  deleteCheckList(id){
    return this.http.delete(this.url + "listingChecklist/"+id);
  }
  
}
