import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable()
export class ListingMultimediaService {
  private baseurl: string=environment.baseUrl
  private url: string = environment.baseUrl+ "listingMultimedia"
  public galleryProgressValue = new Subject();
  public featureProgressValue = new Subject();
  public bannerProgressValue = new Subject();
  public mapProgressValue = new Subject();
  public status = new Subject<any>();
  constructor(private http: HttpClient) { }

  getListingMultimedia(businessId:number,packageId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingMultimedia")
  }
  getListingMultimediaGallery(businessId:number,packageId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingMultimedia/?type=GALLERY")
  }
  getListingMultimediaFeature(businessId:number,packageId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingMultimedia/?type=FEATURE")
  }
  getListingMultimediaBanner(businessId:number,packageId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingMultimedia/?type=BANNER")
  }

  getListingMultimediaVideo(businessId:number,packageId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingMultimedia/?type=VIDEO")
  }

  getListingMultimediaMap(businessId:number,packageId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingMultimedia/?type=MAP")
  }

  saveMultiMedia(data){
    return this.http.post(this.url, data);
  }

  updateMultiMedia(data){
    return this.http.post(this.url,data);
  }

  deleteMedia(id){
    return this.http.delete(this.url+"/"+id);
  }

}
