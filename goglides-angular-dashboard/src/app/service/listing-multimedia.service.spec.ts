import { TestBed, inject } from '@angular/core/testing';

import { ListingMultimediaService } from './listing-multimedia.service';

describe('ListingMultimediaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingMultimediaService]
    });
  });

  it('should be created', inject([ListingMultimediaService], (service: ListingMultimediaService) => {
    expect(service).toBeTruthy();
  }));
});
