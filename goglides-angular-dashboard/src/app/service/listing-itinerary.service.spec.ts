import { TestBed, inject } from '@angular/core/testing';

import { ListingItineraryService } from './listing-itinerary.service';

describe('ListingItineraryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingItineraryService]
    });
  });

  it('should be created', inject([ListingItineraryService], (service: ListingItineraryService) => {
    expect(service).toBeTruthy();
  }));
});
