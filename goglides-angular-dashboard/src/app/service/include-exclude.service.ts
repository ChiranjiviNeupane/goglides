import { IncExcComponent } from '../catalog/inc-exc/inc-exc.component';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IncludeExclude } from '../model/include-exclude';
import { Subject } from 'rxjs';

@Injectable()
export class IncludeExcludeService {

  dataChange: BehaviorSubject<IncludeExclude[]> =new BehaviorSubject<IncludeExclude[]>([]);
  public includeExcludeSubject = new Subject<Object[]>();
  dialogData: IncludeExclude
  public category : number ;

  private url: string = environment.baseUrl+"includeExclude"
  constructor(private http: HttpClient) { }
  
  public countTotal(){
    return this.http.get(this.url+"/countData")
  }
  public countStatus(status : string){
    return this.http.get(this.url+"/countStatus?status="+status)
  }

  public get dataTableData(): IncludeExclude[]{
    return this.dataChange.value;
  }

  getAllData(){
    return this.http.get(this.url)
  }

  public getDialogData(){
    return this.dialogData;
  }

  getAllInclude(include: string): Observable<IncludeExclude[]> {
    return this.http.get<IncludeExclude[]>(this.url+"?types="+include);
  }

  getAllExclude(exclude: string): Observable<IncludeExclude[]> {
    return this.http.get<IncludeExclude[]>(this.url+"?types="+exclude)
  }

  getAllIncludeExclude():Observable<IncludeExclude[]>{
    return this.http.get<IncludeExclude[]>(this.url);
  }

  addInclude(data){
    return this.http.post(this.url,data);
  }

  addExclude(data){
    return this.http.post(this.url, data)
  }
  
  addIncludeExclude(data){
    return this.http.post(this.url,data);
  }

 

  deleteIncludeExclude(id: number){
    return this.http.delete(this.url+ "/"+ id).subscribe((data:IncludeExclude) => {
      this.dialogData=data;
      console.log("sucessfully deleted.........")
    })
  }


  updateIncludeExclude(id:number, data:any){
    return this.http.put(this.url+"/"+id, data).subscribe((data:IncludeExclude) => {
      this.dialogData = data;
      console.log("Sucessfully updated.............");
    })
  }

  getIncludeAndExclude(categoryId: number){
    return this.http.get(this.url+"/includeAndExclude?id="+categoryId)
  }
}
