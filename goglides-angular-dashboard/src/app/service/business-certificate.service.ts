import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class BusinessCertificateService {
private url: string= environment.baseUrl+"businessCertificate"

public logoProgressPercentage = new Subject<any>();
public businessProgressPercentage = new Subject<any>();
public panProgressPercentage = new Subject<any>();

  constructor(private http: HttpClient) { }

  public saveBusinessCertificate(data: any){
    return this.http.post(this.url, data);
  }

  public saveBusinessPan(data: any){
    return this.http.post(this.url,data);
  }
  
  public getCertificate(bussId:number){
    return this.http.get(environment.baseUrl+"businessDirectory/"+bussId+"/businessCertificate")
  }

  public verifiedCert(bussId,data,certId){
    return this.http.put(environment.baseUrl+"businessDirectory/"+bussId+"/businessCertificate/"+certId,data)
  }

}
