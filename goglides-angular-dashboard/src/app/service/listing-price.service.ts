import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ListingPricing } from '../model/listing-pricing';
@Injectable()
export class ListingPriceService {
  private baseurl: string=environment.baseUrl;
  private url: string=environment.baseUrl+"listingPrice";


  constructor(private http: HttpClient) { }

  getListingPrice(businessId:number,listingId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+listingId+"/listingPrice")
  }

  addPricing(data){
    return this.http.post(this.url, data)
  }

  updatePricing(id,data){
    return this.http.put(this.url+"/"+id, data);
  }

  getListingPriceById(id){
    return this.http.get(this.url+"/"+id).map((data: ListingPricing)=>{
      return data['title']
    })
  }
  
  deletePricing(id){
    return this.http.delete(this.url+"/"+id);
  }

}
