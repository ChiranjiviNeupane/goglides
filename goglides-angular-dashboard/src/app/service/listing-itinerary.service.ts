import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ListingItinerary } from '../model/listing-itinerary';

@Injectable()
export class ListingItineraryService {
  private url: string=environment.baseUrl+"listingItinerary"
  private baseurl: string=environment.baseUrl;

  // private url: string ="http://localhost:8082/v1/manage/listingItinerary"
  constructor(private http: HttpClient) { }

  public addListingItinerary(data){
     return this.http.post(this.url,data);
  }

  public getListingItineraryByListingId(businessId:number, listingId: number): Observable<ListingItinerary[]>{
    return this.http.get<ListingItinerary[]>(this.baseurl+"businessDirectory/"+businessId+"/listing/"+listingId+"/listingItinerary")
  }
//work left to do
  public getListingItinerary(businessId:number,packageId:number){
    return this.http.get(environment.baseUrl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingItinerary")
  }

  public updateListingItinerary(id: number, data: any){
    return this.http.put(this.url+"/"+id,data);
  }

  public deleteItinerary(id){
    return this.http.delete(this.url+"/"+id)
  }
}
