import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from '../keycloak-service/keycloak.service';
import { LogService } from './log.service';

@Injectable()
export class UserService {
  private userReference: {id: number, referenceId: string, username: string, fullname: string, authenticated: boolean, roles: string[]};
  private loggedUser: {username: string, firstname: string, lastname: string, email: string, mobile: number, phone: number, gender: string, profile: string, address: string, state: string, zipCode: number, country: string, id: number };
  
  constructor(public http:HttpClient,private kc: KeycloakService) { 
    this.userReference = LogService.userRef;
    this.loggedUser = LogService.user;
  }
  login(params?: any){
    return this.kc.login(params);
  }

  logout(params?: any) {
    return this.kc.logout(params);
  }

  

  /** loggedUser Info fxn **/
  userRef() {
    return this.userReference;
  }

  setLoggedUser(user){
    this.loggedUser=user;
  }

  getLoggedUser(){
    return this.loggedUser;
  }
}
