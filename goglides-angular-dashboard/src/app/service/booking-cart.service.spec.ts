import { TestBed, inject } from '@angular/core/testing';

import { BookingCartService } from './booking-cart.service';

describe('BookingCartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookingCartService]
    });
  });

  it('should be created', inject([BookingCartService], (service: BookingCartService) => {
    expect(service).toBeTruthy();
  }));
});
