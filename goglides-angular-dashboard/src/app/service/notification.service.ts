import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NotificationService {
  private url: string=environment.baseUrl+"listing"
  private baseUrl: string=environment.baseUrl
  constructor(private http:HttpClient) { }

  getNotification(){
    return this.http.get(this.baseUrl+"notification")
   
  }
  
  countNotification(){
    return this.http.get(this.baseUrl+"notification/unReadNotification")
   
  }

  deleteNotification(id){
    return this.http.delete(this.baseUrl+"notification/delete/"+id)
  }
}
