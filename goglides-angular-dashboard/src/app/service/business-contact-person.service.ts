import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class BusinessContactPersonService {

  private url:string=environment.baseUrl;
  constructor(private http: HttpClient) { }

  public saveBusinessContactPerson(data){
    return this.http.post(this.url+"businessContactPerson",data);
  }

  public listByBusinessId(id:number){
    return this.http.get(this.url+"businessDirectory/"+id+"/businessContactPerson/")
  }

  public getBusinessContactPerson(id: number){
    return this.http.get(this.url+"businessContactPerson/"+id);
  }

  public updateBusinessContactPerson(id:number, data:any){
    return this.http.put(this.url+"businessContactPerson/"+id,data);
  }

  public addBusinessContactPerson(data){
    return this.http.post(this.url+"businessContactPerson",data);
  }

}
