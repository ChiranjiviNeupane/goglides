import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ListingFactService {
  private url: string=environment.baseUrl

  constructor(private http: HttpClient) { }

  getListingFactByPackage(businessId:number,packageId:number){
    return this.http.get(this.url+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingFact")
  }

  public addListingFact(data){
    return  this.http.post(this.url+"listingFact",data);
  }

  public updateFact(id, data){
    return this.http.put(this.url+"listingFact/"+id, data).map(data=>{
    });
  }

}
