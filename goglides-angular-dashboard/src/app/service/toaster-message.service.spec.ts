import { TestBed, inject } from '@angular/core/testing';

import { ToasterMessageService } from './toaster-message.service';

describe('ToasterMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToasterMessageService]
    });
  });

  it('should be created', inject([ToasterMessageService], (service: ToasterMessageService) => {
    expect(service).toBeTruthy();
  }));
});
