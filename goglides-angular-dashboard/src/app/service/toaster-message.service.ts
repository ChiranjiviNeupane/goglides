import { Injectable } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

@Injectable()
export class ToasterMessageService {

  constructor(
    public toaster: ToastsManager
  ) { }

  view(vcr){
    this.toaster.setRootViewContainerRef(vcr);
  }
  
  success(vcr, message, status){
    this.view(vcr)
    this.toaster.success(message, status);
  }

  error(vcr, message, status){
    this.view(vcr)
    this.toaster.error(message, status);
  }
  
  warning(vcr, message, status) {
    this.view(vcr)
    this.toaster.warning(message, status);
  }

  info(vcr, message, status) {
    this.view(vcr)
    this.toaster.info(message);
  }
  
  custom(vcr, message, status) {
    this.toaster.custom('<span style="color: red">'+message+'</span>', null, {enableHTML: true});
  }

}
