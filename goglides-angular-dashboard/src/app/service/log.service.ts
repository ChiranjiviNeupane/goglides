import { Injectable } from '@angular/core';
import { User } from '../keycloak-service/keycloak.func';

@Injectable()
export class LogService {
  //loggedUser Info
  static userRef: any = {
    id: null,
    referenceId: null,
    username: null,
    fullname: null,
    authenticated: false,
    roles: null
}
static user: any = null;

static setLoggedUserInfo(id: number, ref: string, uName: string, name: string, stat: boolean, roles: string[]) {
    this.userRef.id = id;
    this.userRef.referenceId = ref;
    this.userRef.username = uName;
    this.userRef.fullname = name;
    this.userRef.authenticated = stat;
    this.userRef.roles = roles;
}

static setUser(user: any){
    this.user = user;
}

static check(ref: string){
    let log = new User();
    return log.checkUser(ref);
}

static save(ref: string){
    let log = new User();
    return log.saveUser(ref);
}

static get(id: number) {
    let log = new User();
    return log.getUser(id);
}
}
