import { environment } from 'environments/environment';
import { Category } from '../model/category';
import { HttpClient } from '@angular/common/http';
import { Injectable, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject, Subject } from 'rxjs';
import { ToastsManager } from 'ng2-toastr';


@Injectable()
export class CategoryService {

  dataChange: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>([]);
  dailogData: Category;
  title: string = "";
  private url: string = environment.baseUrl + 'category';

  constructor(private httpClient: HttpClient) { }
  public countTotal() {
    return this.httpClient.get(this.url + "/countData")
  }
  public countStatus(status: string) {
    return this.httpClient.get(this.url + "/countStatus?status=" + status)
  }


  public get dataTableData(): Category[] {
    return this.dataChange.value;
  }

  public getDialogData() {
    return this.dailogData;
  }

  getAllDataInDataTable() {
    this.httpClient.get<Category[]>(this.url).subscribe((data: Category[]) => {
      this.dataChange.next(data);
    })
  }

  getAllCategory(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(this.url);
  }

  saveCategory(data) {
    return this.httpClient.post(this.url, data);
  }

  deleteCategory(categoryId: number) {
    return this.httpClient.delete(this.url + "/" + categoryId).subscribe((data: Category) => {
      this.dailogData = data;
    })
  }

  updateCategory(id: number, data: any) {
    return this.httpClient.put(this.url + "/" + id, data).subscribe((data: Category) => {
      this.dailogData = data;
    });
  }

  getCategoryById(categoryId: number) {
    return this.httpClient.get(this.url + "/" + categoryId).map((data: Object) => {
      return data
    })
  }
}
