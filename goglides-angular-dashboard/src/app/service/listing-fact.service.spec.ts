import { TestBed, inject } from '@angular/core/testing';

import { ListingFactService } from './listing-fact.service';

describe('ListingFactService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingFactService]
    });
  });

  it('should be created', inject([ListingFactService], (service: ListingFactService) => {
    expect(service).toBeTruthy();
  }));
});
