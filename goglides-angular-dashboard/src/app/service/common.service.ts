import { Category } from 'aws-sdk/clients/support';
import { Injectable, Pipe } from '@angular/core';
import { Subject } from 'rxjs';
import { TrimPipe } from 'ngx-pipes';

@Injectable()
export class CommonService {
  updloadStatus:string
  constructor(
    private trimPipe: TrimPipe,
  ) { }

  listing=new Subject<any>();
  listingPhotos = new Subject<any>();
  cartCount = new Subject<any>();
  listingReferenceNo:string;
  listingId:number
  businessDirectoryId: number
  catergoryId: number
  uploadedPhotos: any
  dialogStatus = new Subject<Boolean>();
  addonsCart = []
  
  setListingId(id: number){
    console.log(" common service set id"+id);
    this.listingId = id
  }

  getListingId(){
    console.log("common service get id "+this.listingId);
    return this.listingId
  }
 
  setBusinessDirectoryId(id: number){
    // console.log("Business Directory id is:"+id)
    this.businessDirectoryId= id;
  }

  getBusinessDirectoryId(){
    return this.businessDirectoryId;
  }

  setStatus(status:string){
    this.updloadStatus = status
  }
  getStatus(){
    return this.updloadStatus
  }

  setCategoryId(id: number){
    console.log("Category saved"+id);
    this.catergoryId = id
    console.log("Category saved"+id);
  }

  getCategoryId(){
    return this.catergoryId;
  }

  setPhotos(array){
    this.uploadedPhotos = array
  }

  getPhotos(){
    return this.uploadedPhotos
  }

  formatCategory(categories){
    // let categoryDetail:Object[]=[{
    //   Category:"",
    //   totalPax:"",
    //   type:"",
    //   schedule:"",
    //   duration:"",
    // }]
    let cat:any =categories.split(',')
    console.log(cat)
  }

  formatDuration(duration:any){
    let formated = " "
    let dur:any = duration.split(':') 
    if(dur[0]!= '00'){
      formated += dur[0] +" weeks "
    }
    if(dur[1]!= '00'){
      formated += dur[1]+" days "
     }
     if(dur[2]!= '00'){
    formated += dur[2] +" hours "
     }
     if(dur[3]!= '00' ){
       formated += dur[3] + " minutes "
    }
    return(formated) 
  }

  formatSchedule(schdeule:any){
    let formated = " "
    let schl:any = schdeule.split(':')
    if(schl[0]>=0 && schl[0]<12){
      formated = schl[0] + ":" +schl[1] + " AM" 
    }
    if(schl[0]==24 ){
      schl[0]='00'
      formated = schl[0] + ":" +schl[1] + " AM" 
    }
    else if(schl[0]>12){
      schl[0]=schl[0]-12
      formated = schl[0] + ":" +schl[1] + " PM" 
    }
    return(formated)
    // alert(formated)
  }

  nuberToWord(number){
    console.log(number)
    var converter = require('number-to-words');
    converter.toWords(number);
    return converter
  }

  innterText
  trimInfo
  addonsJson

  countfxn(val,arr){
    let count = 0;
    let j =-1
    for(let i = 0; i<arr.length; i++){
      if(val === arr[i]){
        count++
      }
    }
    return count
  }

  buildAddonsJson(addonsJson, addonData, data){
    var uniqueAddons = Array.from(new Set(addonData))
    for(let i =0; i<uniqueAddons.length; i++){
      let value = JSON.stringify(uniqueAddons[i])
      addonsJson['addon']['addon'+i+'_qty'] = this.countfxn(uniqueAddons[i], addonData)
      addonsJson['addon']['addon'+i+'_value'] = (value.split('->')[0])
      addonsJson['addon']['addon'+i+'_rate'] = parseInt((value.split('->')[1]))
      addonsJson['addon']['addon'+i+'_total'] = parseInt((value.split('->')[1])) * this.countfxn(uniqueAddons[i], addonData)
      addonsJson['addon']['total'] += addonsJson['addon']['addon'+i+'_total']
    }
    if(localStorage.getItem('addonsCart')){
      this.trimInfo = this.trimPipe.transform(localStorage.getItem('addonsCart'), '/')
        this.addonsCart = JSON.parse(this.trimInfo)
    }
    localStorage.removeItem('addonData')
    this.addonsCart.push(addonsJson['addon'])
    localStorage.setItem('addonsCart', JSON.stringify(this.addonsCart))
    addonData = []
    addonData.push(data)
  }

  jsonBuilder(size){
    let addon={ addon:{} }
    for(let i=1; i<size; i++){
      addon['addon']['addon'+i+'_qty'] = 0 
      addon['addon']['addon'+i+'_value'] = ''
      addon['addon']['addon'+i+'_rate'] = 0
      addon['addon']['addon'+i+'_total'] = 0
      addon['addon']['total'] = 0
    }
    return addon
  }

}
