import { TestBed, inject } from '@angular/core/testing';

import { ListingSettingService } from './listing-setting.service';

describe('ListingSettingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingSettingService]
    });
  });

  it('should be created', inject([ListingSettingService], (service: ListingSettingService) => {
    expect(service).toBeTruthy();
  }));
});
