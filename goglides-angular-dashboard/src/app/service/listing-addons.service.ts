import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class ListingAddonsService {
  private baseurl: string=environment.baseUrl;
  private url: string=environment.baseUrl+"listingAddon";


  constructor(private http: HttpClient) { }

  getListingAddonPrice(businessId:number,listingId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+listingId+"/listingAddon")
  }

  saveAddons(data){
    return this.http.post(this.url,data);
  }

  updateAddons(id, data){
    return this.http.put(this.url+"/"+id, data);
  }

  deleteAddons(id){
    return this.http.delete(this.url+"/"+id)
  }

}
