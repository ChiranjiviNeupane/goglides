import { Injectable } from '@angular/core';

@Injectable()
export class StatusService {

  constructor() { }
  statusList = [
    { value: 'DRAFT', viewValue: 'Draft' },
    { value: 'PUBLISH', viewValue: 'Publish' },
    { value: 'TRASH', viewValue: 'Trash' },
    { value: 'AWAITING_APPROVAL', viewValue: 'Awaiting Approval' },
    { value: 'DECLINED', viewValue: 'Declined' },
    { value: 'CLOSED', viewValue: 'Closed' }
  ];


  bookingStatus = [
    { value: 'PENDING', viewValue: 'Pending' },
    { value: 'DECLINED', viewValue: 'Declined' },
    { value: 'COMPLETED', viewValue: 'Completed' },
    { value: 'CONFIRMED', viewValue: 'Confirmed' },
    { value: 'REFUNDED', viewValue: 'Refunded' },
    { value: 'TRASH', viewValue: 'Trash' },
    { value: 'AWAITING_APPROVAL', viewValue: 'Awating_aprovval' },
    { value: 'CLOSED', viewValue: 'Closed' },
    { value: 'CANCELLED', viewValue: 'Cancelled' },
    { value: 'CANCELLATION_REQUEST', viewValue: 'Cancellation_request' },
    { value: 'RESERVED', viewValue: 'Reserved' },
    
  ];

  paymentStatus = [
    { value: 'PENDING', viewValue: 'Pending' },
    { value: 'PAID', viewValue: 'Paid' },
    { value: 'FAILED', viewValue: 'Failed' },
    { value: 'REVERSED', viewValue: 'Reserved' },
    { value: 'REFUNDED', viewValue: 'Refunded' },
    { value: 'CANCELLED', viewValue: 'Cancelled' },
  ];

  getStatusList(){
    return this.statusList;
  }

  getBookingStatus(){
    return this.bookingStatus
  }

  getPaymentStatus(){
    return this.paymentStatus
  }

}
