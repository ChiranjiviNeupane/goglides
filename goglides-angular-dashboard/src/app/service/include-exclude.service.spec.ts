import { TestBed, inject } from '@angular/core/testing';

import { IncludeExcludeService } from './include-exclude.service';

describe('IncludeExcludeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IncludeExcludeService]
    });
  });

  it('should be created', inject([IncludeExcludeService], (service: IncludeExcludeService) => {
    expect(service).toBeTruthy();
  }));
});
