import { TestBed, inject } from '@angular/core/testing';

import { ListingPriceService } from './listing-price.service';

describe('ListingPriceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingPriceService]
    });
  });

  it('should be created', inject([ListingPriceService], (service: ListingPriceService) => {
    expect(service).toBeTruthy();
  }));
});
