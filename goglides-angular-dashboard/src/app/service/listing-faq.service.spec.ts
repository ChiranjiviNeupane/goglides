import { TestBed, inject } from '@angular/core/testing';

import { ListingFaqService } from './listing-faq.service';

describe('ListingFaqService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingFaqService]
    });
  });

  it('should be created', inject([ListingFaqService], (service: ListingFaqService) => {
    expect(service).toBeTruthy();
  }));
});
