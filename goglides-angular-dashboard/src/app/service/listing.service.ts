import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { Listing } from '../model/listing';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ListingService {
  public listingSubject = new Subject<Object[]>();
  public bookingSubject = new Subject<Object[]>();

  private url: string=environment.baseUrl+"listing"
  private baseUrl: string=environment.baseUrl
  // "http://localhost:8082/v1/manage/listing"
  constructor(private http:HttpClient) { }

  addListingGeneral(data) :Observable<Listing>{
    return this.http.post<Listing>(this.url, data);
  } 

  public getListingById(listingId:number){
    return this.http.get(this.url+"/"+listingId)
  }

  getAllListing(){
    return this.http.get(this.url)
  }
  getAllListingByBusiness(businessId:number){
    return this.http.get(this.baseUrl+"businessDirectory/"+businessId+"/listing")
  }
  getListingByCode(code:number){
    return this.http.get(this.baseUrl+"booking/packageByCode?code="+code)
  }
  getDetailPackageList(id:number){
    return this.http.get(this.url+"/"+id)
  }

  updateListingGeneralInfo(id:number, data:any){
    return this.http.put(this.url+"/"+id,data);
  }

  public getListingByBusinessId(id: number){
    return this.http.get(environment.baseUrl+"businessDirectory/"+id+"/listing")
  }
  
}
