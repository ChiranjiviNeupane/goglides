import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ListingSettingService {

  private url = environment.baseUrl + "listingSetting"
  
  constructor(
    private http: HttpClient
  ) { }

  saveLisitngSetting(data){
    return this.http.post(this.url, data);
  }

  updateLisitngSetting(id,data){
    return this.http.put(this.url+"/"+id, data);
  }

  getLisintgSetting(businessId, listingId){
    return this.http.get(environment.baseUrl+"businessDirectory/"+businessId+"/listing/"+listingId+"/listingSetting")
  }

}
