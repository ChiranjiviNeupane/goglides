import { CheckList } from '../model/check-list';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject, Subject } from 'rxjs';
import { Category } from '../model/category';
@Injectable()
export class CheckListService {

  dataChange: BehaviorSubject<CheckList[]> = new BehaviorSubject<CheckList[]>([]);
  public categorySubject = new Subject<Object[]>();
  public category = new Subject<Object>();
  //temporarly add
  dialogData: CheckList;
  checkList = []
  private url: string = environment.baseUrl + "checkList"
  constructor(private http: HttpClient) { }

  public countTotal() {
    return this.http.get(this.url + "/countData")
  }
  public countStatus(status: string) {
    return this.http.get(this.url + "/countStatus?status=" + status)
  }

  public get data(): CheckList[] {
    return this.dataChange.value;
  }

  public getDialogData() {
    return this.dialogData;
  }
  
    getAllData(){
   return this.http.get(this.url);
  }

  addCheckList(data): Observable<CheckList> {
    return this.http.post<CheckList>(this.url, data);
  }

  getAllCheckList(): Observable<CheckList[]> {
    return this.http.get<CheckList[]>(this.url);
  }

  deleteCheckList(id: number) {
    return this.http.delete(this.url + "/" + id).subscribe((data: CheckList) => {
      this.dialogData = data;
    });
  }

  updateCheckList(id: number, data: any) {
    return this.http.put(this.url + "/" + id, data).subscribe((data: CheckList) => {
      this.dialogData = data;

    });
  }
}
