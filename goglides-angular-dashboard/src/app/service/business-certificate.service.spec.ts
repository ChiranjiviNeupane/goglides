import { TestBed, inject } from '@angular/core/testing';

import { BusinessCertificateService } from './business-certificate.service';

describe('BusinessCertificateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusinessCertificateService]
    });
  });

  it('should be created', inject([BusinessCertificateService], (service: BusinessCertificateService) => {
    expect(service).toBeTruthy();
  }));
});
