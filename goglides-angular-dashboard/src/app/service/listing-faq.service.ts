import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ListingFaqService {
  private baseurl: string=environment.baseUrl
  private url: string = environment.baseUrl + "listingFaq"

  constructor(private http: HttpClient) { }
  getListingFaq(businessId:number,packageId:number){
    return this.http.get(this.baseurl+"businessDirectory/"+businessId+"/listing/"+packageId+"/listingFaq")
  }

  saveFaq(data){
    return this.http.post(this.url,data);
  }

  public updateFaq(id, data){
    return this.http.put(this.url +"/"+id, data );
  }

  deleteFaq(id){
    return this.http.delete(this.url+"/"+id);
  }

}
