import { TestBed, inject } from '@angular/core/testing';

import { ListingDestinationService } from './listing-destination.service';

describe('ListingDestinationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingDestinationService]
    });
  });

  it('should be created', inject([ListingDestinationService], (service: ListingDestinationService) => {
    expect(service).toBeTruthy();
  }));
});
