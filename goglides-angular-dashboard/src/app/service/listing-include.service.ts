import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable()
export class ListingIncludeService {
  private url = environment.baseUrl ;
  private listincurl = environment.baseUrl + "listingIncludeExclude";
  public status = new Subject<any>();
  constructor(
    private http: HttpClient
  ) { }

  saveIncludeAndExclude(data){
    return this.http.post(this.listincurl, data)
  }
  viewIncExc(businessId:number,listingId:number){
    return this.http.get(environment.baseUrl+"businessDirectory/"+businessId+"/listing/"+listingId+"/listingIncludeExclude")
  }

  deleteIncExc(id){
    return this.http.delete(this.listincurl+"/"+id)
  }

}
