import { ForexService } from '../service/forex.service';
import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../service/category.service';
import { IncludeExcludeService } from '../service/include-exclude.service';
import { CheckListService } from '../service/check-list.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  categoryTotalCount : number
  categoryTotalPublish : number
  categoryTotalPending : number
  categoryTotalTrash : number
  incExcTotalCount : number
  incExcTotalPublish : number
  incExcTotalPending : number
  incExcTotalTrash : number
  checkListTotalCount : number
  checkListTotalPublish : number
  checkListTotalPending : number
  checkListTotalTrash : number
  forexTotalCount : number
  forexTotalPublish : number
  forexTotalPending : number
  forexTotalTrash : number
  constructor(
    private catergoryService: CategoryService,
     private incExcService : IncludeExcludeService,
      private checkListService : CheckListService,
      private forexService : ForexService
    ) { }

  ngOnInit() {
    // category
    this.catergoryService.countTotal().subscribe((data:Object)=>{this.categoryTotalCount=data['count']})
    this.catergoryService.countStatus("PUBLISH").subscribe((data:Object)=>{this.categoryTotalPublish=data['count']})
    this.catergoryService.countStatus("AWAITING_APPROVAL").subscribe((data:Object)=>{this.categoryTotalPending=data['count']})
    this.catergoryService.countStatus("TRASH").subscribe((data:Object)=>{this.categoryTotalTrash=data['count']})
    // incExc
    this.incExcService.countTotal().subscribe((data:Object)=>{this.incExcTotalCount=data['count']})
    this.incExcService.countStatus("PUBLISH").subscribe((data:Object)=>{this.incExcTotalPublish=data['count']})
    this.incExcService.countStatus("AWAITING_APPROVAL").subscribe((data:Object)=>{this.incExcTotalPending=data['count']})
    this.incExcService.countStatus("TRASH").subscribe((data:Object)=>{this.incExcTotalTrash=data['count']})
    // checklist
    this.checkListService.countTotal().subscribe((data:Object)=>{this.checkListTotalCount=data['count']})
    this.checkListService.countStatus("PUBLISH").subscribe((data:Object)=>{this.checkListTotalPublish=data['count']})
    this.checkListService.countStatus("AWAITING_APPROVAL").subscribe((data:Object)=>{this.checkListTotalPending=data['count']})
    this.checkListService.countStatus("TRASH").subscribe((data:Object)=>{this.checkListTotalTrash=data['count']})
    // forex
    this.forexService.countTotal().subscribe((data:Object)=>{this.forexTotalCount=data['count']})
    this.forexService.countStatus("PUBLISH").subscribe((data:Object)=>{this.forexTotalPublish=data['count']})
    this.forexService.countStatus("AWAITING_APPROVAL").subscribe((data:Object)=>{this.forexTotalPending=data['count']})
    this.forexService.countStatus("TRASH").subscribe((data:Object)=>{this.forexTotalTrash=data['count']})
  }

}
