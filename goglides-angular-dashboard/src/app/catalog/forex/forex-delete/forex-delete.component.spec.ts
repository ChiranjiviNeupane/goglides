import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForexDeleteComponent } from './forex-delete.component';

describe('ForexDeleteComponent', () => {
  let component: ForexDeleteComponent;
  let fixture: ComponentFixture<ForexDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForexDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForexDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
