import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-forex-delete',
  templateUrl: './forex-delete.component.html',
  styleUrls: ['./forex-delete.component.scss']
})
export class ForexDeleteComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ForexDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }


  ngOnInit() {
  }

}
