import { Forex } from '../../model/forex';
import { ForexService } from '../../service/forex.service';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, ViewContainerRef } from '@angular/core';
import {MatTableDataSource, MatDialog, MatSort, MatPaginator} from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ForexDeleteComponent } from './forex-delete/forex-delete.component';
import { ForexUpdateComponent } from './forex-update/forex-update.component';
import { StatusService } from '../../service/status.service';
import { ToasterMessageService } from '../../service/toaster-message.service';

@Component({
  selector: 'app-forex',
  templateUrl: './forex.component.html',
  styleUrls: ['./forex.component.scss']
})
export class ForexComponent implements OnInit,AfterViewInit {
  forex:Forex[]
  statusList = this.status.getStatusList();
   /*---------------------Data table----------------------------*/
   @ViewChild(MatSort) sort: MatSort;
   @ViewChild(MatPaginator) paginator: MatPaginator;
   @ViewChild('filter') filter: ElementRef
   ELEMENT_DATA = []
   displayedColumns = ['position', 'countrycode', 'currencycode','exchangerate', 'status', 'action'];
   dataSource
   /*---------------------Data table----------------------------*/



  forexForm: FormGroup


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  

  constructor(
    private formBuilder : FormBuilder, 
    public dialog: MatDialog,
    private forexService:ForexService,
    private status: StatusService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService,
  ) { }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(ForexDeleteComponent, {
  //     // width: '250px',
  //     data: {name: this.name, animal: this.animal}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //     this.animal = result;
  //   });
  // }

  openDialog1(data): void {
    const dialogRef = this.dialog.open(ForexUpdateComponent, {
      width: '800px',
      data:data
    });

    dialogRef.afterClosed().subscribe(result => {
     if(result===0){ this.toasterMessageService.success(this.vcr, "Forex Updated !", "Success")
      this.ELEMENT_DATA = []
      this.getForex()}
        }), (error) => {
      this.toasterMessageService.error(this.vcr, "Error Ocured !", "Error")
    };
  }

  ngOnInit() {
    this.forexForm= this.formBuilder.group({
      countryCode: ['', Validators.required],
      currencyCode: ['', Validators.required],
      rate: ['', Validators.required],
      status: ['', Validators.required]
    })
    this.getForex()

  }
  getForex(){
    
    this.forexService.getForex().subscribe((data:Forex[])=>{
      this.ELEMENT_DATA=data
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
      this.dataSource.paginator =this.paginator
      this.dataSource.sort=this.sort;
      })
  }
  saveForex(data){
    this.forexService.addForex(data).subscribe((data)=>{
      this.ELEMENT_DATA = []
      this.getForex()
      this.toasterMessageService.success(this.vcr, "Forex Saved !", "Success")
    
    }), (error) => {
      this.toasterMessageService.error(this.vcr, "Error Ocured !", "Error")
    }
  }

  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
    this.dataSource.sort = this.sort;
  }

}


