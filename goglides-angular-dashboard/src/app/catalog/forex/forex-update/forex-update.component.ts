import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StatusService } from '../../../service/status.service';
import { ForexService } from '../../../service/forex.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';
export interface DialogData {
  animal: string;
  name: string;
}
export interface option {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-forex-update',
  templateUrl: './forex-update.component.html',
  styleUrls: ['./forex-update.component.scss']
})
export class ForexUpdateComponent implements OnInit {
  statusList = this.status.getStatusList();
  update:number=0;

  forexUpdateForm: FormGroup

  constructor(
    public dialogRef: MatDialogRef<ForexUpdateComponent>,
    private status: StatusService,
    private forexService: ForexService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private fb: FormBuilder) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.forexUpdateForm= this.fb.group({
      countryCode: [this.data['countryCode'], Validators.required],
      currencyCode: [this.data['currencyCode'], Validators.required],
      rate: [this.data['rate'], Validators.required],
      status: [this.data['status'], Validators.required]
    })
  }
  updateForex(data){
    this.forexService.updateForex(this.data['id'],data).subscribe((data)=>{
     
     
      this.update=1;
    
    })
  }

}
