import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForexUpdateComponent } from './forex-update.component';

describe('ForexUpdateComponent', () => {
  let component: ForexUpdateComponent;
  let fixture: ComponentFixture<ForexUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForexUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForexUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
