import { CategoryUpdateComponent } from './category-update/category-update.component';
import { CategoryDeleteComponent } from './category-delete/category-delete.component';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Category } from '../../model/category';
import { Observable } from 'rxjs/Observable';
import { CategoryService } from '../../service/category.service';
import { DataSource } from '@angular/cdk/table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, ViewContainerRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { PageEvent } from '@angular/material';
import { StatusService } from '../../service/status.service';
import { UserService } from '../../service/user.service';
import { ToastsManager } from 'ng2-toastr';
import { ToasterMessageService } from '../../service/toaster-message.service';
// import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, AfterViewInit {

  statusList = this.statusService.getStatusList()
  categoryForm: FormGroup
  index: number
  id: number

  // MatPaginator Output
  pageEvent: PageEvent;
  displayedColumns = ['position', 'Title', 'Status', 'Action'];
  dataSource: CategoryDataSource | null;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef


  constructor(private categoryService: CategoryService,
    private statusService: StatusService,
    private fb: FormBuilder,
    private userService: UserService,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService
    // private toastr: ToastrService,
  ) {
  }

  ngOnInit() {
    this.categoryForm = this.fb.group({
      title: ["", Validators.required],
      status: ["", Validators.required],
      code: ['', Validators.required],
      user: this.fb.group({
        id: [this.userService.userRef().id, Validators.required]
      })
    });
    this.loadData();
  }
  ngAfterViewInit(): void {
    this.refreshTable();
  }

  public saveCategory(data) {
    this.categoryService.saveCategory(data).subscribe((data: Category) => {
      this.toasterMessageService.success(this.vcr, "Category Saved !", "Success");
    }
    );
  }
  public deleteCategory(i: number, id: number, title: string) {
    // this.showSuccess();
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(CategoryDeleteComponent, {
      // width: '300px',
      data: { id: id, title: title }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.toasterMessageService.error(this.vcr, "Category Deleted !", "Success");
        const foundIndex = this.categoryService.dataChange.value.findIndex(x => x.id === this.id);
        this.categoryService.dataChange.value[foundIndex] = this.categoryService.getDialogData();
        this.refreshTable();
      }
    });
  }

  public updateCategory(i: number, id: number, title: string, status: string, isVisible: number) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(CategoryUpdateComponent, {
      data: { 
        id: id, 
        title: title, 
        status: status, 
        isVisible: isVisible
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.toasterMessageService.success(this.vcr, "Category Updated !", "Success");
        const foundIndex = this.categoryService.dataChange.value.findIndex(x => x.id === this.id);
        this.categoryService.dataChange.value[foundIndex] = this.categoryService.getDialogData();
        this.refreshTable();
      }
    });
  }

  public loadData() {
    this.dataSource = new CategoryDataSource(this.categoryService, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }


  // If you don't need a filter or a pagination this can be simplified, you just use code from else block
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

}


export class CategoryDataSource extends DataSource<Category>{

  _filterChange = new BehaviorSubject('');


  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Category[] = [];
  renderedData: Category[] = [];

  constructor(public _categoryService: CategoryService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }


  connect(): Observable<Category[]> {
    const displayChanges = [
      this._categoryService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];
    this._categoryService.getAllDataInDataTable();
    return Observable.merge(...displayChanges).map(() => {
      this.filteredData = this._categoryService.dataTableData.slice().filter((category: Category) => {
        console.log("category Title " + category.title)
        const searchStr = (category.title + category.status).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });



      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });

  }

  disconnect() {

  }


  /** Returns a sorted copy of the database data. */
  sortData(data: Category[]): Category[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'title': [propertyA, propertyB] = [a.title, b.title]; break;
        case 'type': [propertyA, propertyB] = [a.status, b.status]; break

      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}