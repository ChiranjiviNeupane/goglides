import { StatusService } from '../../../service/status.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CategoryService } from '../../../service/category.service';
import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { Category } from 'aws-sdk/clients/support';

@Component({
  selector: 'app-category-update',
  templateUrl: './category-update.component.html',
  styleUrls: ['./category-update.component.scss']
})
export class CategoryUpdateComponent implements OnInit {
  statusList:any
  categoryUpdateForm: FormGroup
  dailogData: Category;
  constructor(private fb: FormBuilder, 
    private categoryService: CategoryService,
    public dialogRef : MatDialogRef<CategoryUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private statusService: StatusService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService
  ) { console.log(data)
  }

  ngOnInit() {

    this.categoryUpdateForm= this.fb.group({
      title: [(this.data['title'])?this.data['title']:'',Validators.required],
      status: [(this.data['status'])?this.data['status']:''],
      isVisible: [(this.data['isVisible'] === 1)?true: false]
    });
    this.statusList=this.statusService.getStatusList();
    
  }

  public confirmUpdate(updateCategory): void{
    updateCategory['isVisible'] =  (updateCategory['isVisible'])?1:0
    let category = this.categoryService.updateCategory(this.data.id, updateCategory);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  

}
