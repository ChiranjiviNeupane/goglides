import { CategoryService } from '../../../service/category.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { ToasterMessageService } from '../../../service/toaster-message.service';

@Component({
  selector: 'app-category-delete',
  templateUrl: './category-delete.component.html',
  styleUrls: ['./category-delete.component.scss']
})
export class CategoryDeleteComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CategoryDeleteComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private categoryService : CategoryService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService
    ) { }
  
    ngOnInit() {
    }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
    confirmDelete(): void {
      let category = this.categoryService.deleteCategory(this.data.id);
    }
  
}
