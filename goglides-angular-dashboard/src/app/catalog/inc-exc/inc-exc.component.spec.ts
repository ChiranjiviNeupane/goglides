import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncExcComponent } from './inc-exc.component';

describe('IncExcComponent', () => {
  let component: IncExcComponent;
  let fixture: ComponentFixture<IncExcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncExcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncExcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
