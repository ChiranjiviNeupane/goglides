import { IncExcUpdateComponent } from './inc-exc-update/inc-exc-update.component';
import { IncExcDeleteComponent } from './inc-exc-delete/inc-exc-delete.component';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IncludeExclude } from '../../model/include-exclude';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IncludeExcludeService } from '../../service/include-exclude.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/table';
import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { PageEvent } from '@angular/material';
import { StatusService } from '../../service/status.service';
import { UserService } from '../../service/user.service';
import { CategoryService } from '../../service/category.service';
import { ToasterMessageService } from '../../service/toaster-message.service';

@Component({
  selector: 'app-inc-exc',
  templateUrl: './inc-exc.component.html',
  styleUrls: ['./inc-exc.component.scss']
})

export class IncExcComponent implements OnInit {
    cat
  index: number
  id: number
  inclusionExclusionForm: FormGroup;
  statusList = this.statusService.getStatusList();
  public categories = [];

  /*---------------------Data table----------------------------*/
  @ViewChild(MatSort) sort: MatSort;
  ELEMENT_DATA = []
  displayedColumns = ['position', 'title', 'category', 'status', 'action'];
  dataSource
  /*---------------------Data table----------------------------*/


  constructor(private statusService: StatusService,
    private includeExcludeService: IncludeExcludeService,
    private fb: FormBuilder,
    private userService: UserService,
    public dialog: MatDialog,
    public categoryService: CategoryService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService
  ) { }

  public saveIncludeExclude(data) {
    this.includeExcludeService.addIncludeExclude(data)
      .subscribe(
        (data: IncludeExclude) => {
          this.toasterMessageService.success(this.vcr, "Include/Exclude Saved !", "Success")
          this.includeExcludeService.dataChange.value.push(data);
          this.ELEMENT_DATA = []
          this.getAllIncludeExclude();
        }, (error) => {
          this.toasterMessageService.error(this.vcr, "Error Occured !!", "Error")
        })
    // this.inclusionExclusionForm.reset();
  }

  public deleteIncludeExclude(index: number, id: number, title: string, status: string) {
    this.index = index
    this.id = id
    const dialogRef = this.dialog.open(IncExcDeleteComponent, {
      data: { id: id, title: title, status: status }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.toasterMessageService.success(this.vcr, "Include/Exclude Deleted !", "Success")
        const foundIndex = this.includeExcludeService.dataChange.value.findIndex(x => x.id === this.id);
        this.includeExcludeService.dataChange.value[foundIndex] = this.includeExcludeService.getDialogData();
        this.ELEMENT_DATA = []
        this.getAllIncludeExclude();

      }
    })
  }

  public updateIncludeExclude(index: number, id: number, title: string, status: string, category: number) {
    this.index = index;
    this.id = id;
    const dialogRef = this.dialog.open(IncExcUpdateComponent, {
      data: { id: id, title: title, status: status, category: category }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.toasterMessageService.success(this.vcr, "Include/Exclude Updated !", "Success")
        const foundIndex = this.includeExcludeService.dataChange.value.findIndex(x => x.id === this.id);
        this.includeExcludeService.dataChange.value[foundIndex] = this.includeExcludeService.getDialogData();
        this.ELEMENT_DATA = []
        this.getAllIncludeExclude();
      }
    })

  }

  type = [
    { value: 'Include', viewValue: 'Include' },
    { value: 'Exclude', viewValue: 'Exclude' },
  ];


  // MatPaginator Output
  pageEvent: PageEvent;

  // displayedColumns = ['position', 'Title', 'Type', 'Status','Action'];
  // dataSource : IncludeExcludeDataSource | null;

  // @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef


  ngOnInit() {
    this.inclusionExclusionForm = this.fb.group({
      title: ["", Validators.required],
      category: ["", Validators.required],
      status: ["", Validators.required],
      user: this.fb.group({
        id: [this.userService.userRef().id, Validators.required]
      })
    });
    this.categoryService.getAllCategory().subscribe(data => {
      this.categories = data
    })
    this.getAllIncludeExclude();
  }

  public getAllIncludeExclude() {
    let includeExclude = {}
    let includeExcludeArray = []
    this.includeExcludeService.getAllData().subscribe(data => {
      for (let i in data) {
        this.categoryService.getCategoryById(data[i]['category']['id']).subscribe(inculdeAndExclude => {
          includeExclude = {
            "id": data[i]['id'],
            "title": data[i]['title'],
            "status": data[i]['status'],
            "category": {
              "id": inculdeAndExclude['id'],
              "title": inculdeAndExclude['title']
            }
          }
          includeExcludeArray.push(includeExclude);

          this.includeExcludeService.includeExcludeSubject.next(includeExcludeArray);
        })
      }
    })
    this.includeExcludeService.includeExcludeSubject.subscribe(includeExcludeData => {
      this.ELEMENT_DATA = includeExcludeData;
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }


  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  // If you don't need a filter or a pagination this can be simplified, you just use code from else block

}