import { IncludeExcludeService } from '../../../service/include-exclude.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { StatusService } from '../../../service/status.service';
import { CategoryService } from '../../../service/category.service';

@Component({
  selector: 'app-inc-exc-update',
  templateUrl: './inc-exc-update.component.html',
  styleUrls: ['./inc-exc-update.component.scss']
})
export class IncExcUpdateComponent implements OnInit {

  statusList:any
  type:any
  updateIncludeExcludeForm:FormGroup
  public categories = [];

  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<IncExcUpdateComponent>,
  public includeExcludeService : IncludeExcludeService,
  private statusService: StatusService,
  private categoryService: CategoryService
) { console.log(data)}

  ngOnInit() {
  this.updateIncludeExcludeForm =this.fb.group({
    title: ['', Validators.required],
    category: ['', Validators.required],
    status: ['', Validators.required]
  });
  this.statusList=this.statusService.getStatusList();
  this.type = [
    { value: 'Include', viewValue: 'Include' },
    { value: 'Exclude', viewValue: 'Exclude' },
  ];
  this.categoryService.getAllCategory().subscribe(data => {
    this.categories = data
  })

  }

  public confirmUPdate(updateIncludeExclude){
    this.includeExcludeService.updateIncludeExclude(this.data.id, updateIncludeExclude);
  }


  onNoClick(): void {
    this.dialogRef.close();
  }


}
