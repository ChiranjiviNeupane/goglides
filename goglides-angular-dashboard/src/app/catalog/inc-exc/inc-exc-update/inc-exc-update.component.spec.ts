import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncExcUpdateComponent } from './inc-exc-update.component';

describe('IncExcUpdateComponent', () => {
  let component: IncExcUpdateComponent;
  let fixture: ComponentFixture<IncExcUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncExcUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncExcUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
