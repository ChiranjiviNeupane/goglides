import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncExcDeleteComponent } from './inc-exc-delete.component';

describe('IncExcDeleteComponent', () => {
  let component: IncExcDeleteComponent;
  let fixture: ComponentFixture<IncExcDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncExcDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncExcDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
