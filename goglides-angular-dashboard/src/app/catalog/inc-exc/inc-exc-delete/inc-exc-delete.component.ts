import { IncludeExcludeService } from '../../../service/include-exclude.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-inc-exc-delete',
  templateUrl: './inc-exc-delete.component.html',
  styleUrls: ['./inc-exc-delete.component.scss']
})
export class IncExcDeleteComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef :MatDialogRef<IncExcDeleteComponent>,
  private includeExcludService: IncludeExcludeService
) { }

ngOnInit() {
}

onNoClick(): void {
  this.dialogRef.close();
}

confirmDelete(): void {
  this.includeExcludService.deleteIncludeExclude(this.data.id);
}

}
