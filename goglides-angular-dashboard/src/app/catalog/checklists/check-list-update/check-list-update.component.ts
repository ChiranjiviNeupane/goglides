import { StatusService } from '../../../service/status.service';
import { CheckListService } from '../../../service/check-list.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CategoryService } from '../../../service/category.service';

@Component({
  selector: 'app-check-list-update',
  templateUrl: './check-list-update.component.html',
  styleUrls: ['./check-list-update.component.scss']
})
export class CheckListUpdateComponent implements OnInit {

  updateCheckListForm: FormGroup
  statusList:any
  type:any
  public categories = [];
  
  constructor(public dialogRef : MatDialogRef<CheckListUpdateComponent>,
    @Inject( MAT_DIALOG_DATA) public data: any,
    private chickListService : CheckListService,
    private fb: FormBuilder,
    private statusService: StatusService,
    private checkListService: CheckListService,
    private categoryService: CategoryService,
  ) {console.log(data) }

  ngOnInit() {
    console.log(this.data)
    this.updateCheckListForm=this.fb.group( {
    title: ['',Validators.required],
    status:['', Validators.required],
    category:['', Validators.required]
   }
    );
    
    this.statusList=this.statusService.getStatusList();
    this.type = [
      { value: 'Provide', viewValue: 'Provide' },
      { value: 'Bring', viewValue: 'Bring' },
    ];

    this.categoryService.getAllCategory().subscribe(data => {
      this.categories = data
    })
  }

  public confirmUpdate(updateCheckList): void{
    this.checkListService.updateCheckList(this.data.id, updateCheckList);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
}
