import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckListUpdateComponent } from './check-list-update.component';

describe('CheckListUpdateComponent', () => {
  let component: CheckListUpdateComponent;
  let fixture: ComponentFixture<CheckListUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckListUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckListUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
