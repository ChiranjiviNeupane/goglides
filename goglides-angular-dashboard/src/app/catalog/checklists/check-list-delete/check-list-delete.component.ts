import { CheckListService } from '../../../service/check-list.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-check-list-delete',
  templateUrl: './check-list-delete.component.html',
  styleUrls: ['./check-list-delete.component.scss']
})
export class CheckListDeleteComponent implements OnInit {


  constructor(public dialogRef: MatDialogRef<CheckListDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private checkListService:  CheckListService) { }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.checkListService.deleteCheckList(this.data.id);
  }


}
