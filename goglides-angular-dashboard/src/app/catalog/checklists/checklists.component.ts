import { CheckListDeleteComponent } from './check-list-delete/check-list-delete.component';
import { CheckListUpdateComponent } from './check-list-update/check-list-update.component';
import { Component, ElementRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { UserService } from '../../service/user.service';
import { CheckList } from '../../model/check-list';
import { CheckListService } from '../../service/check-list.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StatusService } from '../../service/status.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DataSource } from '@angular/cdk/collections';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { CategoryService } from '../../service/category.service';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { Category } from '../../model/category';


@Component({
  selector: 'app-checklists',
  templateUrl: './checklists.component.html',
  styleUrls: ['./checklists.component.scss']
})
export class ChecklistsComponent implements OnInit {

  index: number
  id: number
  statusList = this.status.getStatusList();
  checkListForm: FormGroup
  public checkLists: any[];
  public categories = [];
  private v: string;

  /*---------------------Data table----------------------------*/
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef
  @ViewChild(MatSort) sort: MatSort;
  ELEMENT_DATA = []
  displayedColumns = ['position', 'title', 'category', 'status', 'action'];
  dataSource
  /*---------------------Data table----------------------------*/

  type = [
    { value: 'Provide', viewValue: 'Provide' },
    { value: 'Bring', viewValue: 'Bring' },
  ];

  // MatPaginator Output
  constructor(private status: StatusService, private fb: FormBuilder,
    private checkListService: CheckListService,
    private userService: UserService,
    private dialog: MatDialog,
    private categoryService: CategoryService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService,
  ) {
  }

  public getAllData() {
    this.checkListService.getAllCheckList().subscribe((data: object[]) => {
      this.checkLists = data;
    });
  }

  public saveCheckList() {
    let data = this.checkListForm.value;
    this.checkListService.addCheckList(data).subscribe((data: CheckList) => {
      this.checkListService.dataChange.value.push(data);
      this.toasterMessageService.success(this.vcr, "Checklist Saved !", "Success")
      this.ELEMENT_DATA = []
      this.getAllCheckList();
    }, (error) => {
      this.toasterMessageService.error(this.vcr, "Error Ocured !", "Error")
    }
    );
    // this.loadData();
  }


  public deleteCheckList(i: number, id: number, title: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(CheckListDeleteComponent, {
      // width: '300px',
      data: { id: id, title: title }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.toasterMessageService.success(this.vcr, "Checklist Deleted !", "Success")
        const foundIndex = this.checkListService.dataChange.value.findIndex(x => x.id === this.id);
        //remove data from data table
        //  this.checkListService.dataChange.value.splice(foundIndex,1);
        this.checkListService.dataChange.value[foundIndex] = this.checkListService.getDialogData();
        this.ELEMENT_DATA = []
        this.getAllCheckList();
      }
    });
  }

  public editCheckList(i: number, id: number, title: string, status: string, category: number) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(CheckListUpdateComponent, {
      data: { id: id, title: title, status: status, category:category},
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.toasterMessageService.success(this.vcr, "Checklist Updated !", "Success")
        const foundIndex = this.checkListService.dataChange.value.findIndex(x => x.id === this.id);
        this.checkListService.dataChange.value[foundIndex] = this.checkListService.getDialogData();
        this.functionFormBuilder();
        this.ELEMENT_DATA = []
        this.getAllCheckList();
      }
    })
  }

  public functionFormBuilder() {
    this.checkListForm = this.fb.group({
      title: ["", Validators.required],
      category: ['', Validators.required],
      status: ["", Validators.required],
      user: this.fb.group(
        {
          id: [1, Validators.required]
        }
      )
    }
    );
  }

  ngOnInit() {
    this.functionFormBuilder();
    this.categoryService.getAllCategory().subscribe(data => {
      this.categories = data
    })
    this.getAllCheckList();
  }

  public getAllCheckList() {
    let checklist = {}
    let checkListArray = []
    this.checkListService.getAllData().subscribe(data => {
      for (let i in data) {
        this.categoryService.getCategoryById(data[i]['category']['id']).subscribe(cat => {
          checklist = {
            "id": data[i]['id'],
            "title": data[i]['title'],
            "status": data[i]['status'],
            "category": {
              "id":cat['id'],
              "title": cat['title']
            },
          }
          checkListArray.push(checklist);
          this.checkListService.categorySubject.next(checkListArray);
        })
      }
    })
    this.checkListService.categorySubject.subscribe(checkListData => {
      this.ELEMENT_DATA = checkListData;
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
}

