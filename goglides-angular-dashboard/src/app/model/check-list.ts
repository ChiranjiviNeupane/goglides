import { User } from '../keycloak-service/keycloak.func';
import { Category } from './category';
export class CheckList {
     id: number;
     user : User= new User();
     referenceId : string;
     title : string;
     slug : string;
     type : string;
     status : string;
     createdAt : Date;
     updatedAt : Date;
     deletedAt : Date;
     category : Category = new Category();
}
