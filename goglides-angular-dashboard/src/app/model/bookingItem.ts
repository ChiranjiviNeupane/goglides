import { ListingPricing } from './listing-pricing';
import { Booking } from './booking';
import { User } from "./user";
import { BusinessDirectory } from "./business-directory";
import { Listing } from "./listing";


export class BookingItem {
    id:number
    user: User 
    businessDirectory: BusinessDirectory 
    listingPrice: ListingPricing 
    listing: Listing 
    booking:Booking
    bookingDate:Date
    schedule:string
    duration:string
    type:string
    category:string
    nationality:string
    email:string
    fullName:string
    title:string
    identificationNo:string
    identificationType:string
    ticketNo:string
    ageGroup:string
    addAddon: number  
    bookingAddon:any
    retailRate: number  
    weight: number  
    createdAt: Date 
    updatedAt: Date 
    deletedAt: Date
}
