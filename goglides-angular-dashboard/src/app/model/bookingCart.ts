import { User } from "./user";
import { BusinessDirectory } from "./business-directory";


export class BookingCart {
    id:number
    user: User 
    businessDirectory: BusinessDirectory 
    cart:any
    type: number 
    createdAt: Date 
    updatedAt: Date 
    deletedAt: Date
}
