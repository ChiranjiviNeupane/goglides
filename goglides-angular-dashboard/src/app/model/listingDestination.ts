import { Category } from './category';
import { User } from './user';
import { BusinessDirectory } from './business-directory';
import { Listing } from './listing';
export class ListingDestination {
     id:number;
     user: User;
     businessDirectory: BusinessDirectory;
     listing :Listing
     city: string;
     country: string;
     createdAt: Date;
     updatedAt: Date;
     deletedAt: Date;
}
