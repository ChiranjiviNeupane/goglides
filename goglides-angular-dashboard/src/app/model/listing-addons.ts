import { Listing } from './listing';
import { User } from "./user";
import { BusinessDirectory } from './business-directory';

export class ListingAddons{
    id:number
    user: User
    businessDirectory: BusinessDirectory;
    listing: Listing;
    retailRate:number
    wholesaleRate:number
    title:string
    type:number
    status:string
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    
}
