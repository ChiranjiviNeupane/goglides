import { User } from "./user";

export class Category {
    id: number;
    user: User= new User();
    referenceId: string;
    title: string;
    slug: string;
    bannerUrl: string;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date
    isVisible: number
}
