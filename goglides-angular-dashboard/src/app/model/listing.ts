import { Category } from './category';
import { User } from './user';
import { BusinessDirectory } from './business-directory';
export class Listing {
    id: number;
    user: User;
    businessDirectory: BusinessDirectory;
    category: Category;
    referenceId: string;
    parentId: number;
    title: string;
    slug: string;
    code: string;
    tripCode: string;
    highlight: string;
    description: string;
    address: string;
    city: string;
    country: string;
    latitude: number;
    longitude: number;
    nearestCity: number;
    nearestAirport: number;
    isVisible: number;
    isVerified: number;
    status: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    isFeatured: number
    featuredStart: Date
    featuredEnd: Date
    policyType: number
    instantBooking: number
    metaTitle: string
    metaKeyword: string
    metaDescription: string
    
}
