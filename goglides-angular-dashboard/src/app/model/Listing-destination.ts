import { DatePipe } from '@angular/common';
import { User } from './user';
import { BusinessDirectory } from './business-directory';
import { Listing } from './listing';

export class businessCertificate {
    user : User = new User();
    businessDirectory : BusinessDirectory = new BusinessDirectory();
    listing : Listing = new Listing();

    country: String 
    city : String 
    createdAt : Date 
    updatedAt : Date 
    deletedAt : Date 
}
