import { User } from './user';
import { BusinessDirectory } from './business-directory';
import { Listing } from './listing';
export class ListingSetting {
    
    user: User
    businessDirectory: BusinessDirectory
    listing: Listing

    isFeatured: number
    featuredStart: Date
    featuredEnd: Date
    policyType: number
    instantBooking: number
    createdAt: Date
    updatedAt: Date
    deletedAt: Date
}