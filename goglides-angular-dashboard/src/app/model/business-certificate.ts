import { DatePipe } from '@angular/common';
import { User } from './user';

export class businessCertificate {
    BusinessDirectory:object[]
    user: User= new User();
    referenceId: string
    fileType: string
    fileUrl: string
    status: string
    parentId: number
    isApproved: number
    updatedAt: Date
    createdAt: Date
    deletedAt : DatePipe
}
