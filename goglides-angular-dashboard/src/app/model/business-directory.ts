import { BusinessDirectoryContactPerson } from './business-directory-contact-person';
import { User } from './user';
export class BusinessDirectory {
    address: string
    businessCertificate:object[];
    businessContactPerson: BusinessDirectoryContactPerson[];
    businessName:string
    businessStaff:object[]
    businessType:string
    city:string
    country:string
    createdAt
    description:Text
    email:string
    facebookUrl:string
    fax:string
    googlePlusUrl:string
    id:number
    isClaimable:number
    isVerified: number
    isVisible: number
    latitude: number
    linkedInUrl: string

    logoUrl :string
    longitude : number
    metaDescription: string
    metaKeyword: string
    parentId : number
    phone1 : string
    phone2 : string
    referenceId : string
    registrationNumber :string 
    slug :string
    status: string
    state: string
    street: string
    twitterUrl: string
    updatedAt: Date
    user: User= new User();
    website : string
    instagramUrl: string
    tumblerUrl: string
    youtubeUrl: string

    setId(id: number){
        this.id = id;
    }

    getId(){
        return this.id;
    }
}
