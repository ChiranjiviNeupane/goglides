import { Category } from './category';
import { User } from './user';
import { BusinessDirectory } from './business-directory';
import { Listing } from './listing';
export class ListingFact {
     id:number;
     user: User;
     businessDirectory: BusinessDirectory;
     availableCapacity :number;
     listing :Listing
     bestSeason: string;
     daysCount : number; 
     maxAge : number; 
     maxCapacity : number; 
     maxPax : number; 
     maxWeight : number; 
     minAge : number; 
     minPax : number; 
     minWeight : number; 
     nightCount : number; 
     parentId : number; 
     endPoint : string; 
     highestElevation : string; 
     startElevation : string; 
     lowestElevation : string; 
     startPoint : string; 
     bridgeHeight : string; 
     riverDistance : string; 
     healthCondition : string; 
     tripGrade: number;
     tripStandard: number;
     status: number;
     createdAt: Date;
     updatedAt: Date;
     deletedAt: Date;
}
