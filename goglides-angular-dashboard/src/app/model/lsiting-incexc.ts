import { IncludeExclude } from './include-exclude';
import { User } from "./user";
import { BusinessDirectory } from "./business-directory";
import { Listing } from "./listing";
import { CheckList } from "./check-list";

export class ListingIncExc {
    id:number
    user: User 
    businessDirectory: BusinessDirectory 
    listing: Listing 
    includeExclude: IncludeExclude
    referenceId: String 
    type: String 
    status: String 
    createdAt: Date 
    updatedAt: Date 
    deletedAt: Date
}
