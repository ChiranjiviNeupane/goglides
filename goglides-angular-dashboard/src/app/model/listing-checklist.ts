import { User } from "./user";
import { BusinessDirectory } from "./business-directory";
import { Listing } from "./listing";
import { CheckList } from "./check-list";

export class ListingCheckList {
    user: User 
    businessDirectory: BusinessDirectory 
    listing: Listing 
    checkList: CheckList

    referenceId: String 
    type: String 
    status: String 
    createdAt: Date 
    updatedAt: Date 
    deletedAt: Date
}
