import { User } from "./user";
import { BusinessDirectory } from "./business-directory";
import { Listing } from "./listing";


export class Booking {
    id:number
    user: User 
    businessDirectory: BusinessDirectory 
    listing: Listing 
    cart:any
    referenceId:string
    clientContact:string
    bookingStatus:string
    clientName:string
    clientEmail:string
    paymentStatus:string
    categories:string
    bookingDate:Date
    bookingItem:Object[]
    total: number  
    bookingMode: string  
    paxTotal: number  
    grandTotal: number  
    paymentMode: string  
    paxTotalCount: number  
    type: number  
    createdAt: Date 
    updatedAt: Date 
    deletedAt: Date
    invoiceNo: number
    code: number
    addonsCart: any
}
