
export class Forex {
    id:number;
    rate:number;
    currencyCode:String
    countryCode:String
    status:String
    createdAt: Date 
    updatedAt: Date 
    deletedAt: Date
   
}
