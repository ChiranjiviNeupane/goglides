import { BusinessDirectory } from './business-directory';
import { User } from "./user";

export class BusinessDirectoryContactPerson {
    id : number;
    user:User =new User();
    businessDirectory: BusinessDirectory = new BusinessDirectory();
    contactPersonJobTitle:  string;
    contactPersonFirstName : string
    contactPersonLastName : string
    contactPersonEmail : string
    contactPersonMobile : string
    contactPersonPrimaryPhone : string
    contactPersonSecondaryPhone : string
    createdAt : Date
    updatedAt : Date
    deletedAt :Date

}
