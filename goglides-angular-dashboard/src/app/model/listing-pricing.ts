import { Listing } from './listing';
import { User } from "./user";
import { BusinessDirectory } from './business-directory';

export class ListingPricing{
    id:number
    user: User
    businessDirectory: BusinessDirectory;
    listing: Listing;
    retailRate:number
    wholesaleRate:number
    salesRate:number
    childRetailRate:number
    childWholesaleRate:number
    childSalesRate:number
    seniorRetailRate:number
    seniorWholesaleRate:number
    seniorSalesRate:number
    infantRetailRate:number
    infantWholesaleRate:number
    infantSalesRate:number
    availableStockUnit:number
    stockUnit:number
    ageGroupRate:number
    enableSale:number
    nationality:string
    type:string
    duration:string
    schedule:string
    status:string
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    
}
