import { User } from "./user";

export class IncludeExclude {
    id: number;
    user: User= new User();
    referenceId: string;
    title: string;
    slug: string;
    type: string;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;

    constructor(){
        
    }
}
