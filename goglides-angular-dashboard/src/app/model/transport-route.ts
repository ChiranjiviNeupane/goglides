import { Category } from './category';
import { User } from './user';
import { BusinessDirectory } from './business-directory';
import { Listing } from './listing';
export class TransportRoute {
     id:number;
     user: User;
     businessDirectory: BusinessDirectory;
     listing :Listing
     address:string;
     pickUp:number;
     dropOff:number;
     status: number;
     createdAt: Date;
     updatedAt: Date;
     deletedAt: Date;
}
