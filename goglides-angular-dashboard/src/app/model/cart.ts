
export class Cart {
    id:number;
    nationality:string;
    type:string;
    schedule:string;
    duration:string;
    date:string;
    seniorRate:number;
    adultRate:number;
    childRate:number;
    infantRate:number;
    seniorQty:number;
    adultQty:number;
    childQty:number;
    infantQty:number;
    seniorTotal:number;
    adultTotal:number;
    childTotal:number;
    infantTotal:number;
    total:number;
}
