import { Listing } from './listing';
import { User } from "./user";
import { BusinessDirectory } from './business-directory';

export class ListingMultimeda {
    id:number
    user: User
    businessDirectory: BusinessDirectory;
    listing: Listing;
    fileUrl: string;
    type: string;
    isApproved: number;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
