import { Listing } from './listing';
import { User } from "./user";
import { BusinessDirectory } from './business-directory';

export class ListingFaq {
    id:number
    user: User
    businessDirectory: BusinessDirectory;
    listing: Listing;
    question: string;
    answer: string;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
