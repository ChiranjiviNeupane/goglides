import { Listing } from './listing';
import { User } from "./user";
import { BusinessDirectory } from './business-directory';

export class ListingItinerary {

    user: User
    businessDirectory: BusinessDirectory;
    listing: Listing;
    referenceId: string;
    parentId: number;
    title: string;
    description: string;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
