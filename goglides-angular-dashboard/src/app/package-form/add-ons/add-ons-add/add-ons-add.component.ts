import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from '../../../service/user.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { ListingAddonsService } from '../../../service/listing-addons.service';
// import { UserService } from '../../service/user.service';
// import { ListingService } from '../../service/listing.service';

@Component({
  selector: 'app-add-ons-add',
  templateUrl: './add-ons-add.component.html',
  styleUrls: ['./add-ons-add.component.scss']
})
export class AddOnsAddComponent implements OnInit {

  addonsForm: FormGroup;
  public userId: number
  public businessId: number
  public listingId: number

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AddOnsAddComponent>,
    private userService: UserService,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    private listingAddonsService: ListingAddonsService
  ) { }

  ngOnInit() {
   this.getIds()
   this.addonsFromBuilder();
  }

  public addonsFromBuilder(): void {
    this.addonsForm = this.fb.group({
      title: ['', Validators.required],
      wholesaleRate: ['', Validators.required],
      retailRate: ['', Validators.required],
      type: [false,],
      user: this.fb.group({
        id: [this.userService.userRef().id]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessId]
      }),
      listing: this.fb.group({
        id: [this.listingId]
      })
    })
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = parseInt(this.data['listingId'])
  }

  public onClick(): void{
    this.dialogRef.close();
  }

}
