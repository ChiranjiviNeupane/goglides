import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from '../../../service/user.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { ListingAddonsService } from '../../../service/listing-addons.service';
import { ListingAddons } from '../../../model/listing-addons';

@Component({
  selector: 'app-add-ons-update',
  templateUrl: './add-ons-update.component.html',
  styleUrls: ['./add-ons-update.component.scss']
})
export class AddOnsUpdateComponent implements OnInit {
  public addonsForm: FormGroup;
  public userId: number
  public businessId: number
  public listingId: number
  public checked: Boolean 

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AddOnsUpdateComponent>,
    private userService: UserService,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    private listingAddonsService: ListingAddonsService
  ) {}

  ngOnInit() {
    this.getIds();
    this.addonsFromBuilder();
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = parseInt(this.data['listingId'])
  }

  public addonsFromBuilder(): void {
    this.addonsForm = this.fb.group({
      title: [this.data['addons']['title'],],
      wholesaleRate: [this.data['addons']['wholesaleRate']],
      retailRate: [this.data['addons']['retailRate'],],
      type: [(this.data['addons']['type']===1)?true:false, Validators.required],
      user: this.fb.group({
        id: [this.userService.userRef().id]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessId]
      }),
      listing: this.fb.group({
        id: [this.listingId]
      })
    })
  }

  public matToogele(event): void{
    this.addonsForm['type'] = (event.checked === true) ? true : false;   
  }

  public onClick(){
    this.dialogRef.close();
  }

  public update(data): any{
    
  }

}
