import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOnsUpdateComponent } from './add-ons-update.component';

describe('AddOnsUpdateComponent', () => {
  let component: AddOnsUpdateComponent;
  let fixture: ComponentFixture<AddOnsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOnsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOnsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
