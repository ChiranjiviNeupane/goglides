import { Component, OnInit, Inject, ViewChild, ViewContainerRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator } from '@angular/material';
import { SocialMediaComponent } from '../../bussiness-directory/social-media/social-media.component';
import { MatTableDataSource, MatSort } from '@angular/material';
import { UserService } from '../../service/user.service';
import { ListingService } from '../../service/listing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { ListingAddonsService } from '../../service/listing-addons.service';
import { ListingAddons } from '../../model/listing-addons';

@Component({
  selector: 'app-add-ons',
  templateUrl: './add-ons.component.html',
  styleUrls: ['./add-ons.component.scss']
})
export class AddOnsComponent implements OnInit {

  public addonsForm: FormGroup;
  public value: string;
  public name: string;
  public businessId: number
  public listingId: number
  public userId: number
  public addonsArray = []

  /*---------------------Data table----------------------------*/
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef
  @ViewChild(MatSort) sort: MatSort;
  ELEMENT_DATA = []
  displayedColumns = ['position', 'addons', 'wholesale', 'retail', 'type', 'action'];
  dataSource
  /*---------------------Data table----------------------------*/

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private activateRouter: ActivatedRoute,
    private fb: FormBuilder,
    public dialog: MatDialog,
    private userService: UserService,
    private listingService: ListingService,
    private router: Router,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    private listingAddonsService: ListingAddonsService

  ) { }

  openDialog(): void {
    let dialogRef = this.dialog.open(SocialMediaComponent, {
      // width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.value = result;
    });
  }

  ngOnInit() {
    this.getIds();
    this.addonsFromBuilder();
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = parseInt(this.activateRouter.snapshot.params['businessId']);
    this.listingId = parseInt(this.activateRouter.snapshot.params['listingId']);
  }

  public addonsFromBuilder(): void {
    this.addonsForm = this.fb.group({
      title: ['',],
      wholesaleRate: ['',],
      retailRate: ['',],
      type: [0, Validators.required],
      user: this.fb.group({
        id: [this.userService.userRef().id]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessId]
      }),
      listing: this.fb.group({
        id: [this.listingId]
      })
    })
  }

  public redirectTo(): void {
    this.router.navigate(['/packages/media/', this.businessId, this.listingId]);
  }

  public addAddons(event): void {
    this.addonsArray.push(this.addonsForm.value)
    let value = (event.checked) ? 1 : 0;
    this.addonsForm.value.type = value;
    this.ELEMENT_DATA = this.addonsArray;
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public saveAddons(): void {
    let counter: number = 0;
    if(this.ELEMENT_DATA){
      for (let data of this.ELEMENT_DATA) {
        this.listingAddonsService.saveAddons(data).subscribe((data: ListingAddons) => {
          counter++;
          if (counter === this.ELEMENT_DATA.length) {
            this.toasterMessageService.success(this.vcr, "Addons Saved !!", "Success")
            setTimeout(() => {
              this.redirectTo();
            }, 3000);
          }
        },(error)=>{
          this.toasterMessageService.error(this.vcr, "Error !!", "Error")
        })
      }
    }else{
      this.redirectTo();
    }
  }

  public cancelAddons(i): void {
    this.addonsArray.splice(i, 1);
    this.ELEMENT_DATA = [];
    this.ELEMENT_DATA = this.addonsArray
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
