import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListingPriceService } from '../../../service/listing-price.service';

@Component({
  selector: 'app-pricin-delete',
  templateUrl: './pricin-delete.component.html',
  styleUrls: ['./pricin-delete.component.scss']
})
export class PricinDeleteComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<PricinDeleteComponent>,
    private listingPricingService: ListingPriceService
  ) { }

  ngOnInit() {
  }

  public onNoClick(): void{
    this.dialogRef.close();
  }

}
