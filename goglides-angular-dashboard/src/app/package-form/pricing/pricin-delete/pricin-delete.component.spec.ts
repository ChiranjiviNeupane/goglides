import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricinDeleteComponent } from './pricin-delete.component';

describe('PricinDeleteComponent', () => {
  let component: PricinDeleteComponent;
  let fixture: ComponentFixture<PricinDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricinDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricinDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
