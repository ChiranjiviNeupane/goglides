import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { UserService } from '../../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ListingPriceService } from '../../service/listing-price.service';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  public show: boolean = false;
  public pricingForm: FormGroup;

  public userId: number
  public businessId: number
  public listingId: number
  public duration: any
  private durationArray: any = []
  toggle() {
    this.show = !this.show;
  }

  constructor(
    private fb: FormBuilder,
    private atp: AmazingTimePickerService,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    public userService: UserService,
    public activateRouter: ActivatedRoute,
    public listingPricingService: ListingPriceService,
    public router: Router
  ) { }

  ngOnInit() {
    this.getIds();
    this.pricinFormBuilder();
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = parseInt(this.activateRouter.snapshot.params['businessId']);
    this.listingId = parseInt(this.activateRouter.snapshot.params['listingId']);
  }

  public pricinFormBuilder() {
    this.pricingForm = this.fb.group({
      pricing_info: this.fb.array([
        this.pricingTitle(),
      ]),
    })
  }

  pricingTitle(): FormGroup {
    return this.fb.group({
      listing: this.fb.group({
        id: [this.listingId]
      }),
      businessDirectory: this.fb.group({
        id: this.businessId
      }),
      user: this.fb.group({
        id: [this.userId]
      }),
      retailRate: ["", Validators.required],
      wholesaleRate: ["", Validators.required],
      salesRate: ['',],
      childRetailRate: ['',],
      childWholesaleRate: ['',],
      childSalesRate: ['',],
      seniorRetailRate: ['',],
      seniorWholesaleRate: ['',],
      seniorSalesRate: ['',],
      infantRetailRate: ['',],
      infantWholesaleRate: ['',],
      infantSalesRate: ['',],
      availableStockUnit: [0],
      stockUnit: ['',],
      ageGroupRate: [0,],
      enableSale: [0,],
      nationality: ["", Validators.required],
      type: ["", Validators.required],
      duration: ["",],
      schedule: ["00:00", Validators.required],
    });
  }

  get pricingForms() {
    return this.pricingForm.get('pricing_info') as FormArray
  }

  addTitle() {
    this.pricingForms.push(this.pricingTitle());
    console.log(this.duration)
    if (this.duration === null || this.duration === undefined || this.duration === '') {
      return false
    } else {
      this.durationArray.push(this.duration);
    }
  }

  deleteTitle(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.pricingForms.removeAt(i)
      this.durationArray.splice(i, 1)
      this.duration = '';
      console.log(this.durationArray);
    }
  }

  // timepicker
  open() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      console.log(time);
    });
  }

  validation(week, day, hours, minutes) {
    if (week === '' || week === null || week === undefined)
      week = '00'

    if (week < 0 || week > 52) {
      week = '00'
      this.toasterMessageService.error(this.vcr, "Week must be in between 0 - 52", "Error")
    }

    if (day === '' || day === null || day === undefined)
      day = '00'

    if (day < 0 || day > 7) {
      day = '00'
      this.toasterMessageService.error(this.vcr, "Days must be in between 0 - 7", "Error")
    }

    if (hours === '' || hours === null || hours === undefined)
      hours = '00'

    if (hours < 0 || hours > 23) {
      hours = '00'
      this.toasterMessageService.error(this.vcr, "Hour must be in between 0 - 23", "Error")
    }

    if (minutes === '' || minutes === null || minutes === undefined)
      minutes = '00'

    if (minutes < 0 || minutes > 59) {
      minutes = '00'
      this.toasterMessageService.error(this.vcr, "Minute must be in between 0 - 59", "Error")
    }
    let w = (week.toString().length === 1) ? ("0" + week).slice(-2) : week;
    let d = (day.toString().length === 1) ? ("0" + day).slice(-2) : day;
    let h = (hours.toString().length === 1) ? ("0" + hours).slice(-2) : hours;
    let m = (minutes.toString().length === 1) ? ("0" + minutes).slice(-2) : minutes;

    this.duration = w + ':' + d + ':' + h + ':' + m
    if (this.duration === "00:00:00:00") {
      this.toasterMessageService.error(this.vcr, "Please provide duraiton", "Erro")
    }
  }


  public savePricing(): void {
    let i = 0;
    let length: number = 0;
    if (this.duration !== null || this.duration !== undefined || this.duration !== '') {
      this.durationArray.push(this.duration);
    }
    length = this.duration.length
    for (let data of this.pricingForm.controls.pricing_info.value) {
      data['childRetailRate'] = (data['childRetailRate'] === '' || data['childRetailRate'] === null || data['childRetailRate'] === undefined) ? data['retailRate'] : data['childRetailRate']
      data['seniorRetailRate'] = (data['seniorRetailRate'] === '' || data['seniorRetailRate'] === null || data['seniorRetailRate'] === undefined) ? data['retailRate'] : data['seniorRetailRate']
      data['infantRetailRate'] = (data['infantRetailRate'] === '' || data['infantRetailRate'] === null || data['infantRetailRate'] === undefined) ? data['retailRate'] : data['infantRetailRate']

      data['childWholesaleRate'] = (data['childWholesaleRate'] === '' || data['childWholesaleRate'] === null || data['childWholesaleRate'] === undefined) ? data['wholesaleRate'] : data['childWholesaleRate']
      data['seniorWholesaleRate'] = (data['seniorWholesaleRate'] === '' || data['seniorWholesaleRate'] === null || data['seniorWholesaleRate'] === undefined) ? data['wholesaleRate'] : data['seniorWholesaleRate']
      data['infantWholesaleRate'] = (data['infantWholesaleRate'] === '' || data['infantWholesaleRate'] === null || data['infantWholesaleRate'] === undefined) ? data['wholesaleRate'] : data['infantWholesaleRate']

      data['childSalesRate'] = (data['childSalesRate'] === '' || data['childSalesRate'] === null || data['childSalesRate'] === undefined) ? data['salesRate'] : data['childSalesRate']
      data['seniorSalesRate'] = (data['seniorSalesRate'] === '' || data['seniorSalesRate'] === null || data['seniorSalesRate'] === undefined) ? data['salesRate'] : data['seniorSalesRate']
      data['infantSalesRate'] = (data['infantSalesRate'] === '' || data['infantSalesRate'] === null || data['infantSalesRate'] === undefined) ? data['salesRate'] : data['infantSalesRate']

      data['duration'] = this.durationArray[i];
      this.listingPricingService.addPricing(data).subscribe(data => {
        i++
        if (i === this.pricingForm.controls.pricing_info.value.length) {
          this.toasterMessageService.success(this.vcr, "Pricing Saved !!", "Success")
          setTimeout(() => {
            this.redirectTo()
          }, 3000);
        }
      }, (error) => {
        this.toasterMessageService.error(this.vcr, error.error.message + "!!!", "Error")
      })
    }
  }
  public redirectTo(): void {
    this.router.navigate(['/packages/addons/', this.businessId, this.listingId]);
  }
}
