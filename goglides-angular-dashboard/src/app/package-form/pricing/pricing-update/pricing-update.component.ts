import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { ListingPriceService } from '../../../service/listing-price.service';

@Component({
  selector: 'app-pricing-update',
  templateUrl: './pricing-update.component.html',
  styleUrls: ['./pricing-update.component.scss']
})
export class PricingUpdateComponent implements OnInit {

  show: boolean = false;
  toggle(){
      this.show = !this.show;       
  }

  pricingForm: FormGroup;
  public duration: any
  public week: any
  public days: any
  public hours: any
  public minutes: any
  constructor(
    @Inject(MAT_DIALOG_DATA) private data:any,
    private dialogRef: MatDialogRef<PricingUpdateComponent>,
    private fb: FormBuilder,
    private atp: AmazingTimePickerService,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    public listingPricingService: ListingPriceService,
  ){}

  ngOnInit() {
    this.pricingForm = this.fb.group({

      pricing_info: this.fb.array([
        this.pricingTitle(),
      ]),
    })
    this.setDurationValue(this.pricingForm.value.pricing_info[0]['duration'])
  }

  public setDurationValue(duration){
    if(duration !== null){
      let d = duration.split(':');
      this.week = d[0]
      this.days = d[1]
      this.hours = d[2]
      this.minutes = d[3]
    }
  }

  pricingTitle(): FormGroup {
    return this.fb.group({
      nationality: [this.data['pricing']['nationality'],Validators.required],
      type: [this.data['pricing']['type'],Validators.required],
      duration: [this.data['pricing']['duration'],Validators.required],
      schedule: [this.data['pricing']['schedule'],Validators.required],
      salesRate: [this.data['pricing']['salesRate']],
      retailRate: [this.data['pricing']['retailRate']],
      wholesaleRate: [this.data['pricing']['wholesaleRate']],
      childRetailRate:[this.data['pricing']['childRetailRate'],],
      childWholesaleRate:[this.data['pricing']['childWholesaleRate'],],
      childSalesRate:[this.data['pricing']['childSalesRate'],],
      seniorRetailRate:[this.data['pricing']['seniorRetailRate'],],
      seniorWholesaleRate:[this.data['pricing']['seniorWholesaleRate'],],
      seniorSalesRate:[this.data['pricing']['seniorSalesRate'],],
      infantRetailRate:[this.data['pricing']['infantRetailRate'],],
      infantWholesaleRate:[this.data['pricing']['infantWholesaleRate'],],
      infantSalesRate:[this.data['pricing']['infantSalesRate'],],
      availableStockUnit:[this.data['pricing']['availableStockUnit']],
      stockUnit:[this.data['pricing']['stockUnit'],],
      ageGroupRate:[this.data['pricing']['ageGroupRate']],
      enableSale:[this.data['pricing']['enableSale']],
      listing:this.fb.group({
        id: [this.data['pricing']['listing']['id']]
      }),
      businessDirectory:this.fb.group({
        id: [this.data['pricing']['businessDirectory']['id']]
      }),
      user:this.fb.group({
        id: [this.data['pricing']['user']['id']]
      }),
    });
  }

  


  get pricingForms() {
    return this.pricingForm.get('pricing_info') as FormArray
  }

  addTitle() {
    this.pricingForms.push(this.pricingTitle());
  }

  deleteTitle(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.pricingForms.removeAt(i)
    }
  }

  // timepicker
  open() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
    });
  }

  validation(week, day, hours, minutes) {
    if (week === '' || week === null || week === undefined)
      week = '00'

    if (week < 0 || week > 52) {
      week = '00'
      this.toasterMessageService.error(this.vcr, "Week must be in between 0 - 52", "Error")
    }

    if (day === '' || day === null || day === undefined)
      day = '00'

    if (day < 0 || day > 7) {
      day = '00'
      this.toasterMessageService.error(this.vcr, "Days must be in between 0 - 7", "Error")
    }

    if (hours === '' || hours === null || hours === undefined)
      hours = '00'

    if (hours < 0 || hours > 23) {
      hours = '00'
      this.toasterMessageService.error(this.vcr, "Hour must be in between 0 - 23", "Error")
    }

    if (minutes === '' || minutes === null || minutes === undefined)
      minutes = '00'

    if (minutes < 0 || minutes > 59) {
      minutes = '00'
      this.toasterMessageService.error(this.vcr, "Minute must be in between 0 - 59", "Error")
    }
    let w = (week.toString().length === 1) ? ("0" + week).slice(-2) : week;
    let d = (day.toString().length === 1) ? ("0" + day).slice(-2) : day;
    let h = (hours.toString().length === 1) ? ("0" + hours).slice(-2) : hours;
    let m = (minutes.toString().length === 1) ? ("0" + minutes).slice(-2) : minutes;

    this.duration = w + ':' + d + ':' + h + ':' + m
    if (this.duration === "00:00:00:00") {
      this.toasterMessageService.error(this.vcr, "Please provide duraiton", "Error")
    }
  }

  update(value){
    let data =value[0]
    data['childRetailRate'] = (data['childRetailRate'] === '' || data['childRetailRate'] === null || data['childRetailRate'] === undefined )? data['retailRate'] : data['childRetailRate']
    data['seniorRetailRate'] = (data['seniorRetailRate'] === '' || data['seniorRetailRate'] === null || data['seniorRetailRate'] === undefined )? data['retailRate'] : data['seniorRetailRate']
    data['infantRetailRate'] = (data['infantRetailRate'] === '' || data['infantRetailRate'] === null || data['infantRetailRate'] === undefined )? data['retailRate'] : data['infantRetailRate']

    data['childWholesaleRate'] = (data['childWholesaleRate'] === '' || data['childWholesaleRate'] === null || data['childWholesaleRate'] === undefined )? data['wholesaleRate'] : data['childWholesaleRate']
    data['seniorWholesaleRate'] = (data['seniorWholesaleRate'] === '' || data['seniorWholesaleRate'] === null || data['seniorWholesaleRate'] === undefined )? data['wholesaleRate'] : data['seniorWholesaleRate']
    data['infantWholesaleRate'] = (data['infantWholesaleRate'] === '' || data['infantWholesaleRate'] === null || data['infantWholesaleRate'] === undefined )? data['wholesaleRate'] : data['infantWholesaleRate']
    
    data['childSalesRate'] = (data['childSalesRate'] === '' || data['childSalesRate'] === null || data['childSalesRate'] === undefined )? data['salesRate'] : data['childSalesRate']
    data['seniorSalesRate'] = (data['seniorSalesRate'] === '' || data['seniorSalesRate'] === null || data['seniorSalesRate'] === undefined )? data['salesRate'] : data['seniorSalesRate']
    data['infantSalesRate'] = (data['infantSalesRate'] === '' || data['infantSalesRate'] === null || data['infantSalesRate'] === undefined )? data['salesRate'] : data['infantSalesRate']
    if(this.duration === value[0].duraiton){
      let data =value[0]
    return this.listingPricingService.updatePricing(this.data['pricing']['id'],data).subscribe(data=>{
      
    })
  }else{
    data.duration = this.duration
    return this.listingPricingService.updatePricing(this.data['pricing']['id'],data).subscribe(data=>{

    })
  }
  }
}
