import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { UserService } from '../../../service/user.service';
import { PricingUpdateComponent } from '../pricing-update/pricing-update.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { ListingPriceService } from '../../../service/listing-price.service';
import { ListingPricing } from '../../../model/listing-pricing';

@Component({
  selector: 'app-pricing-add',
  templateUrl: './pricing-add.component.html',
  styleUrls: ['./pricing-add.component.scss']
})
export class PricingAddComponent implements OnInit {

  show: boolean = false;
  toggle() {
    this.show = !this.show;
  }

  public userId: number
  public businessId: number
  public listingId: number
  public pricingForm: FormGroup;

  public duration: any

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<PricingAddComponent>,
    private fb: FormBuilder,
    private atp: AmazingTimePickerService,
    private userService: UserService,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    private listingPriceService: ListingPriceService
  ) { }

  ngOnInit() {
    this.getIds()
    this.pricingForm = this.fb.group({
      pricing_info: this.fb.array([
        this.pricingTitle(),
      ]),
    })

  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = parseInt(this.data['listingId'])
  }

  pricingTitle(): FormGroup {
    return this.fb.group({
      listing: this.fb.group({
        id: [this.listingId]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessId]
      }),
      user: this.fb.group({
        id: [this.userId]
      }),
      retailRate: ["", Validators.required],
      wholesaleRate: ["", Validators.required],
      salesRate: ['',],
      childRetailRate: ['',],
      childWholesaleRate: ['',],
      childSalesRate: ['',],
      seniorRetailRate: ['',],
      seniorWholesaleRate: ['',],
      seniorSalesRate: ['',],
      infantRetailRate: ['',],
      infantWholesaleRate: ['',],
      infantSalesRate: ['',],
      availableStockUnit: [0],
      stockUnit: ['',],
      ageGroupRate: [0,],
      enableSale: [0,],
      nationality: ["", Validators.required],
      type: ["", Validators.required],
      duration: ["",],
      schedule: ["00:00", Validators.required],
    });
  }

  get pricingForms() {
    return this.pricingForm.get('pricing_info') as FormArray
  }

  addTitle() {
    this.pricingForms.push(this.pricingTitle());
  }

  deleteTitle(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.pricingForms.removeAt(i)
    }
  }

  // timepicker
  open() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
    });
  }

  validation(week, day, hours, minutes) {
    if (week === '' || week === null || week === undefined)
      week = '00'

    if (week < 0 || week > 52) {
      week = '00'
      this.toasterMessageService.error(this.vcr, "Week must be in between 0 - 52", "Error")
    }

    if (day === '' || day === null || day === undefined)
      day = '00'

    if (day < 0 || day > 7) {
      day = '00'
      this.toasterMessageService.error(this.vcr, "Days must be in between 0 - 7", "Error")
    }

    if (hours === '' || hours === null || hours === undefined)
      hours = '00'

    if (hours < 0 || hours > 23) {
      hours = '00'
      this.toasterMessageService.error(this.vcr, "Hour must be in between 0 - 23", "Error")
    }

    if (minutes === '' || minutes === null || minutes === undefined)
      minutes = '00'

    if (minutes < 0 || minutes > 59) {
      minutes = '00'
      this.toasterMessageService.error(this.vcr, "Minute must be in between 0 - 59", "Error")
    }
    let w = (week.toString().length === 1) ? ("0" + week).slice(-2) : week;
    let d = (day.toString().length === 1) ? ("0" + day).slice(-2) : day;
    let h = (hours.toString().length === 1) ? ("0" + hours).slice(-2) : hours;
    let m = (minutes.toString().length === 1) ? ("0" + minutes).slice(-2) : minutes;

    this.duration = w + ':' + d + ':' + h + ':' + m
    if (this.duration === "00:00:00:00") {
      this.toasterMessageService.error(this.vcr, "Please provide duraiton", "Error")
    }
  }

  public savePrice(value) {
    let data = value[0]
    data['childRetailRate'] = (data['childRetailRate'] === '' || data['childRetailRate'] === null || data['childRetailRate'] === undefined) ? data['retailRate'] : data['childRetailRate']
    data['seniorRetailRate'] = (data['seniorRetailRate'] === '' || data['seniorRetailRate'] === null || data['seniorRetailRate'] === undefined) ? data['retailRate'] : data['seniorRetailRate']
    data['infantRetailRate'] = (data['infantRetailRate'] === '' || data['infantRetailRate'] === null || data['infantRetailRate'] === undefined) ? data['retailRate'] : data['infantRetailRate']

    data['childWholesaleRate'] = (data['childWholesaleRate'] === '' || data['childWholesaleRate'] === null || data['childWholesaleRate'] === undefined) ? data['wholesaleRate'] : data['childWholesaleRate']
    data['seniorWholesaleRate'] = (data['seniorWholesaleRate'] === '' || data['seniorWholesaleRate'] === null || data['seniorWholesaleRate'] === undefined) ? data['wholesaleRate'] : data['seniorWholesaleRate']
    data['infantWholesaleRate'] = (data['infantWholesaleRate'] === '' || data['infantWholesaleRate'] === null || data['infantWholesaleRate'] === undefined) ? data['wholesaleRate'] : data['infantWholesaleRate']

    data['childSalesRate'] = (data['childSalesRate'] === '' || data['childSalesRate'] === null || data['childSalesRate'] === undefined) ? data['salesRate'] : data['childSalesRate']
    data['seniorSalesRate'] = (data['seniorSalesRate'] === '' || data['seniorSalesRate'] === null || data['seniorSalesRate'] === undefined) ? data['salesRate'] : data['seniorSalesRate']
    data['infantSalesRate'] = (data['infantSalesRate'] === '' || data['infantSalesRate'] === null || data['infantSalesRate'] === undefined) ? data['salesRate'] : data['infantSalesRate']

    data['duration'] = this.duration
    return this.listingPriceService.addPricing(data).subscribe((response: ListingPricing) => { })
  }

  public onClick() {
    this.dialogRef.close();
  }

}

