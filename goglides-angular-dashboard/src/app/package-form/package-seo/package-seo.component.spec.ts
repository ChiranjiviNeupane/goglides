import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageSeoComponent } from './package-seo.component';

describe('PackageSeoComponent', () => {
  let component: PackageSeoComponent;
  let fixture: ComponentFixture<PackageSeoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageSeoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageSeoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
