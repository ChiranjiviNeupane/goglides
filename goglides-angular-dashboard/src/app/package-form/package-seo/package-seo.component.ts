import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-package-seo',
  templateUrl: './package-seo.component.html',
  styleUrls: ['./package-seo.component.scss']
})
export class PackageSeoComponent implements OnInit {

  seoForm: FormGroup; 

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private data: any)
     { }

  ngOnInit() {
    this.seoForm = this.fb.group({
      metaTitle: [(this.data['listing'])?this.data['listing']['metaTitle']: '',Validators.required],
      metaKeyword: [(this.data['listing'])?this.data['listing']['metaKeyword']: '', Validators.required],
      metaDescription: [(this.data['listing'])?this.data['listing']['metaDescription']: '', Validators.required],
    })
    
  }

}
