import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-feature-view',
  templateUrl: './feature-view.component.html',
  styleUrls: ['./feature-view.component.scss']
})
export class FeatureViewComponent implements OnInit {
  constructor(
    public dialogRef : MatDialogRef<FeatureViewComponent>,
    @Inject( MAT_DIALOG_DATA) public data: any,
  ) {
   console.log(data)
  }

  ngOnInit() {
  }

}
