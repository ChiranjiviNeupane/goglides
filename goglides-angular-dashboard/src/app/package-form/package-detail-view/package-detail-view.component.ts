import { ReviewService } from './../../service/review.service';
import { DataSource } from '@angular/cdk/table';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '../../service/common.service';
import { IncludeExclude } from '../../model/include-exclude';
import { ListingAddonsService } from '../../service/listing-addons.service';
import { ListingPriceService } from '../../service/listing-price.service';
import { Category } from 'aws-sdk/clients/support';
import { BusinessDirectory } from '../../model/business-directory';
import { TransportRoute } from '../../model/transport-route';
import { ListingTransportRouteService } from '../../service/listing-transport-route.service';
import { BusinessDirService } from '../../service/business-dir.service';
import { ItineraryUpdateComponent } from '../itinerary/itinerary-update/itinerary-update.component';
import { ItineraryAddComponent } from '../itinerary/itinerary-add/itinerary-add.component';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { GeneralUpdateComponent } from '../general/general-update/general-update.component';
import { MatDialogRef, MatDialog, MatMenu, MatTableDataSource } from '@angular/material';
import { ListingDestination } from '../../model/listingDestination';
import { ListingFact } from '../../model/listingFact';
import { ListingFactService } from '../../service/listing-fact.service';
import { CategoryService } from '../../service/category.service';
import { ListingItinerary } from '../../model/listing-itinerary';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from "ngx-image-gallery";
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { ActivatedRoute } from '@angular/router';
import { Listing } from '../../model/listing';
import { ListingService } from '../../service/listing.service';
import { ListingItineraryService } from '../../service/listing-itinerary.service';
import { ListingDestinationService } from '../../service/listing-destination.service';
import { CheckListService } from '../../service/check-list.service';
import { ListingCheckList } from '../../model/listing-checklist';
import { ListingChecklistService } from '../../service/listing-checklist.service';
import { ListingPricing } from '../../model/listing-pricing';
import { ListingAddons } from '../../model/listing-addons';
import { ListingIncludeService } from '../../service/listing-include.service';

import { BannerEditComponent } from './banner-edit/banner-edit.component';
import { BannerViewComponent } from './banner-view/banner-view.component';
import { FeatureViewComponent } from './feature-view/feature-view.component';
import { FeatureEditComponent } from './feature-edit/feature-edit.component';

import { PricingUpdateComponent } from '../pricing/pricing-update/pricing-update.component';
import { PricingAddComponent } from '../pricing/pricing-add/pricing-add.component';
import { AddOnsUpdateComponent } from '../add-ons/add-ons-update/add-ons-update.component';
import { AddOnsAddComponent } from '../add-ons/add-ons-add/add-ons-add.component';
import { FaqUpdateComponent } from '../faq/faq-update/faq-update.component';
import { PolicyUpdateComponent } from '../policy/policy-update/policy-update.component';
import { PolicyAddComponent } from '../policy/policy-add/policy-add.component';
import { FaqAddComponent } from '../faq/faq-add/faq-add.component';
import { InclusionAddComponent } from '../inclusion/inclusion-add/inclusion-add.component';
import { InclusionUpdateComponent } from '../inclusion/inclusion-update/inclusion-update.component';
import { ChecklistUpdateComponent } from '../checklist/checklist-update/checklist-update.component';
import { ChecklistAddComponent } from '../checklist/checklist-add/checklist-add.component';
import { DestinationUpdateComponent } from '../destination/destination-update/destination-update.component';
import { DestinationAddComponent } from '../destination/destination-add/destination-add.component';
import { FactsUpdateComponent } from '../facts/facts-update/facts-update.component';
import { PickupDropoffAddComponent } from '../facts/pickup-dropoff-add/pickup-dropoff-add.component';
import { PickupDropoffUpdateComponent } from '../facts/pickup-dropoff-update/pickup-dropoff-update.component';

import { ListingFaq } from '../../model/listing-faq';
import { ListingFaqService } from '../../service/listing-faq.service';
import { ListingMultimediaService } from '../../service/listing-multimedia.service';
import { ListingMultimeda } from '../../model/listing-multimedia';
import { DestinationDeleteComponent } from '../destination/destination-delete/destination-delete.component';
import { PickupDropoffDeleteComponent } from '../facts/pickup-dropoff-delete/pickup-dropoff-delete.component';
import { PricinDeleteComponent } from '../pricing/pricin-delete/pricin-delete.component';
import { MediaDeleteComponent } from '../media/media-delete/media-delete.component';
import { ItineraryDeleteComponent } from '../itinerary/itinerary-delete/itinerary-delete.component';
import { FaqDeleteComponent } from '../faq/faq-delete/faq-delete.component';
import { MediaAddComponent } from '../media/media-add/media-add.component';
import { MediaComponent } from '../media/media.component';
import { IMyDpOptions } from 'mydatepicker';
import { StatusService } from '../../service/status.service';
import { ListingSetting } from '../../model/listing-setting';
import { ListingSettingService } from '../../service/listing-setting.service';
import { SettingComponent } from '../setting/setting.component';
import { UserService } from '../../service/user.service';
import { DatePipe } from '@angular/common';
import { SeoSettingComponent } from '../../settings/seo-setting/seo-setting.component';
import { PackageSeoComponent } from '../package-seo/package-seo.component';
import { MapUpdateComponent } from '../media/map-update/map-update.component';
import {animate, state, style, transition, trigger} from '@angular/animations';
export interface option {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-package-detail-view',
  templateUrl: './package-detail-view.component.html',
  styleUrls: ['./package-detail-view.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PackageDetailViewComponent implements OnInit {
  public highlight: boolean = false;
  public description: boolean = false;
  calendarOptions: Options;
  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
  businessId: number = 0 // lstingid
  selected = '1';
  statusList
  busId: number // businessDirectoryId
  listing: Listing
  listingItinerary: ListingItinerary
  listingReview: any
  category
  categoryCode: any
  featureImage: string
  featureImageId: number
  bannerImage: string
  bannerImageId: number
  videos: any
  map: any
  listingFact: ListingFact
  listingDestination: ListingDestination[]
  transpostRoutes: TransportRoute[]
  businessDir: BusinessDirectory
  listingAddonsPrices: ListingAddons[]
  listingPrices: ListingPricing[]
  listingFaqs: ListingFaq[]
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
  };

  public featuredStart: any = { date: { year: null, month: null, day: null } };
  public featuredEnd: any = { date: { year: null, month: null, day: null } };

  checklist: {
    "bring": [{}],
    "provide": [{}]
  }
  includeExcludes: {
    "include": [{}],
    "exclude": [{}]
  }

  rows = [];
  displayedColumns = [ 'userName','star', 'updatedAt', 'action'];
  dataSource
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;

  // banner edit dialog
  openDialog(url): void {
    const dialogRef = this.dialog.open(BannerEditComponent, {
      width: '800px',
      data: {
        fileUrl: url,
        busId: this.busId,
        listingId: this.businessId
      }
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // banner view dialog
  openDialog1(url): void {
    const dialogRef = this.dialog.open(BannerViewComponent, {
      width: '800px',
      data: { fileUrl: url }
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // featured edit dialog
  openDialog2(url): void {
    console.log("BusinessId: " + this.busId)
    const dialogRef = this.dialog.open(FeatureEditComponent, {
      width: '800px',
      data: {
        fileUrl: url,
        busId: this.busId,
        listingId: this.businessId
      }
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // featured view dialog
  openDialog3(url): void {
    const dialogRef = this.dialog.open(FeatureViewComponent, {
      // width: '800px',
      data: { fileUrl: url }

    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  pricingDelete(id): void {
    const dialogRef = this.dialog.open(PricinDeleteComponent, {
      // width: '800px',
      // height: '400px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      this.listingPricingService.deletePricing(result).subscribe(data=>{
        this.toasterMessageService.success(this.vcr, "Pricing Deleted !!!", "SUCCESS")
        this.listingPrices = []
        this.getListingPricing(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }
  // pricing update dialog
  pricingUpdate(i): void {
    const dialogRef = this.dialog.open(PricingUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {
        pricing: this.listingPrices[i]
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.toasterMessageService.success(this.vcr, "Pricing Updated !!!", "SUCCESS")
        this.listingPrices = []
        this.getListingPricing(this.listing.businessDirectory.id, this.businessId)
      }
    });
  }

  // pricing add dialog
  pricingAdd(): void {
    const dialogRef = this.dialog.open(PricingAddComponent, {
      width: '800px',
      height: '400px',
      data: {
        businessId: this.busId,
        listingId: this.businessId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null) {
        console.log()
        this.toasterMessageService.success(this.vcr, "Pricing For " + result[0]['nationality'] + " Added !!!", "SUCCESS")
        setTimeout(() => {
          this.listingPrices = []
          this.getListingPricing(this.listing.businessDirectory.id, this.businessId)
        }, 2000);
      }
    });
  }

  // addons update dialog
  addonsUpdate(addons): void {
    const dialogRef = this.dialog.open(AddOnsUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {
        businessId: this.busId,
        listingId: this.businessId,
        addons: addons
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result['type'] = (result['type']) ? 1 : 0;
        this.listingAddonsService.updateAddons(addons['id'], result).subscribe((data: ListingAddons) => {
          this.toasterMessageService.success(this.vcr, "Addons Updated !!!", "SUCCESS")
          this.listingAddonsPrices = []
          this.getListingAddonsPricing(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  // addons add dialog
  addonsAdd(): void {
    const dialogRef = this.dialog.open(AddOnsAddComponent, {
      width: '800px',
      height: '400px',
      data: {
        businessId: this.busId,
        listingId: this.businessId,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result['type'] = (result['type'] === true) ? 1 : 0;
        this.listingAddonsService.saveAddons(result).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Addons " + result['title'] + " Added !!!", "SUCCESS")
          this.listingAddonsPrices = []
          this.getListingAddonsPricing(this.listing.businessDirectory.id, this.businessId)
        });
      }
    });
  }

  mediaDelete(id): void {
    const dialogRef = this.dialog.open(MediaDeleteComponent, {
      // width: '800px',
      // height: '400px',
      data: {
        id: id
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null || result !== undefined || result !== '' || !result) {
        this.listingMultimediaService.deleteMedia(id).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Media Deleted !!!", "SUCCESS")
          this.images = []
          this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  mediaAdd(): void {
    const dialogRef = this.dialog.open(MediaAddComponent, {
      width: '800px',
      height: '400px',
      data: {
        businessId: this.busId,
        listingId: this.businessId,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.images = []
        this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
      }
    });
  }

  faqDelete(id): void {
    const dialogRef = this.dialog.open(FaqDeleteComponent, {
      // width: '800px',
      // height: '400px',
      data: {
        id: id
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listingFaqService.deleteFaq(result).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "FAQ Deleted !!!", "SUCCESS")
          this.listingFaqs = []
          this.getListingFaq(this.listing.businessDirectory.id, this.businessId)
        }, (error) => {
          this.toasterMessageService.error(this.vcr, error.errors.message, "Error")
        })
      }
    });
  }
  // faq update dialog
  faqUpdate(faq): void {
    const dialogRef = this.dialog.open(FaqUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {
        faq: faq
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listingFaqService.updateFaq(faq['id'], result).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "FAQ Updated !!!", "SUCCESS")
          this.listingFaqs = []
          this.getListingFaq(this.listing.businessDirectory.id, this.businessId)
        }, (error) => {
          this.toasterMessageService.error(this.vcr, error.errors.message, "Error")
        })
      }
    });
  }

  // faq add dialog
  faqAdd(): void {
    const dialogRef = this.dialog.open(FaqAddComponent, {
      width: '800px',
      height: '400px',
      data: {
        businessId: this.busId,
        listingId: this.businessId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.lisitngFaqService.saveFaq(result).subscribe((data: ListingFaq) => {
          this.toasterMessageService.success(this.vcr, "Faq Saved !!!", "SUCCESS")
          this.listingFaqs = []
          this.getListingFaq(this.listing.businessDirectory.id, this.businessId)
        }, (error) => {
          this.toasterMessageService.error(this.vcr, error.errors.message, "Error")
        })
      }
    });
  }

  // policy update dialog
  policyUpdate(): void {
    const dialogRef = this.dialog.open(PolicyUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // policy add dialog
  policyAdd(): void {
    const dialogRef = this.dialog.open(PolicyAddComponent, {
      width: '800px',
      height: '400px',
      data: {}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // inclusion add dialog
  inclusionAdd(): void {
    const dialogRef = this.dialog.open(InclusionAddComponent, {
      width: '800px',
      height: '400px',
      data: {}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // inclusion update dialog
  inclusionUpdate(): void {
    const dialogRef = this.dialog.open(InclusionUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {
        include: this.includeExcludes['include'],
        exclude: this.includeExcludes['exclude'],
        businessId: this.busId,
        listingId: this.businessId
      }
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // checklist add dialog
  checklistAdd(): void {
    const dialogRef = this.dialog.open(ChecklistAddComponent, {
      width: '800px',
      height: '400px',
      data: {}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // checklist update dialog
  checklistUpdate(): void {
    const dialogRef = this.dialog.open(ChecklistUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {
        businessId: this.busId,
        listingId: this.businessId,
        bring: this.checklist['bring'],
        provide: this.checklist['provide']
      }
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  // destination add dialog
  destinationAdd(): void {
    const dialogRef = this.dialog.open(DestinationAddComponent, {
      width: '800px',
      height: '400px',
      data: {
        businessId: this.busId,
        listingId: this.businessId,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        for (let data of result) {
          this.listindDestinationService.saveListingDestination(data).subscribe((response: Response) => {
            this.toasterMessageService.success(this.vcr, "Destination Added !!!", "SUCCESS")
            this.listingDestination = [];
            this.getPackageDestination(this.listing.businessDirectory.id, this.businessId)
          })
        }
      }
    });
  }

  destinationDelete(id): void {
    const dialogRef = this.dialog.open(DestinationDeleteComponent, {
      data: {
        id: id
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listingDestinationService.deleteDestination(result).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Destination Deleted !!!", "SUCCESS")
          this.listingDestination = [];
          this.getPackageDestination(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  // destination update dialog
  destinationUpdate(destination): void {
    const dialogRef = this.dialog.open(DestinationUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {
        destination: destination
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listingDestinationService.updateDestination(destination['id'], result).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Faq Saved !!!", "SUCCESS")
          this.listingDestination = [];
          this.getPackageDestination(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  // facts update dialog
  factsUpdate(fact: ListingFact, listing: Listing): void {
    this.categoryService.getCategoryById(listing['category']['id']).subscribe((category: Category)=>{
      const dialogRef = this.dialog.open(FactsUpdateComponent, {
        width: '800px',
        height: '400px',
        data: {
          fact: fact,
          listing: listing,
          code: category['code']
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          if(result['id'] === null || result['id'] === undefined || result['id'] === ''){
            this.listingFactService.addListingFact(result).subscribe(data => {
              this.toasterMessageService.success(this.vcr, "Fact Saved", "SUCCESS")
              this.listingFact = null
              this.getPackageFacts(this.listing.businessDirectory.id, this.businessId)
            })
          }else{
            this.listingFactService.updateFact(fact['id'], result).subscribe(data => {
              this.toasterMessageService.success(this.vcr, "Fact Updated", "SUCCESS")
              this.listingFact = null
              this.getPackageFacts(this.listing.businessDirectory.id, this.businessId)
            })
          }
        }
      });
    });
  }

  // pickupDropoff add dialog
  pickupDropoffAdd(): void {
    const dialogRef = this.dialog.open(PickupDropoffAddComponent, {
      width: '800px',
      height: '400px',
      data: {
        transpostRoutes: this.transpostRoutes,
        businessId: this.busId,
        listingId: this.businessId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        for (let data of result['location_detail']) {
          data['pickUp'] = (data['pickUp'] !== true || data['pickUp'] === "" || data['pickUp'] == undefined || data['pickUp'] == null) ? 0 : 1;
          data['dropOff'] = (data['dropOff'] !== true || data['dropOff'] === "" || data['dropOff'] == undefined || data['dropOff'] == null) ? 0 : 1;
          this.lisitngRouteService.addListingTransportRoute(data).subscribe(data => {
            this.toasterMessageService.success(this.vcr, "Route Added !!!", "SUCCESS")
            this.transpostRoutes = []
            this.getPackageTransportRoute(this.listing.businessDirectory.id, this.businessId)
          })
        }
      }
    });
  }

  // pickupDropoff update dialog
  pickupDropoffUpdate(route): void {
    const dialogRef = this.dialog.open(PickupDropoffUpdateComponent, {
      width: '800px',
      height: '400px',
      data: {
        route: route
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let data = result[0]
        data['pickUp'] = (data['pickUp'] !== true || data['pickUp'] === "" || data['pickUp'] == undefined || data['pickUp'] == null) ? 0 : 1;
        data['dropOff'] = (data['dropOff'] !== true || data['dropOff'] === "" || data['dropOff'] == undefined || data['dropOff'] == null) ? 0 : 1;
        this.listingRouteService.update(route['id'], data).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Route Updated !!!", "SUCCESS")
          this.transpostRoutes = []
          this.getPackageTransportRoute(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  pickupDropoffDelete(id): void {
    const dialogRef = this.dialog.open(PickupDropoffDeleteComponent, {
      data: {
        id: id
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listingRouteService.deleteRoute(result).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Route Deleted !!!", "SUCCESS")
          this.transpostRoutes = []
          this.getPackageTransportRoute(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  public updateListingGeneralInfo(listing) {
    const dialogRef = this.dialog.open(GeneralUpdateComponent, {
      width: "800px",
      height: "400px",
      data: { listing: listing },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        return this.listingService.updateListingGeneralInfo(listing['id'], result).subscribe((data) => {
          this.toasterMessageService.success(this.vcr, "Sucessfully Updated... ", "SUCCESS");
          this.getPackage(this.businessId);
        })
      }
    })
  }

  addNewListingItinerary(listing) {
    const dialogRef = this.dialog.open(ItineraryAddComponent, {
      width: "800px",
      height: "400px",
      data: { listing: listing }
    }
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.listingItineraryService.addListingItinerary(result).subscribe((data) => {
          this.toasterMessageService.success(this.vcr, "Sucessfully Added... ", "SUCCESS");
          this.listingItinerary = null
          this.getItinery(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  updateListingItinerary(itinerary) {
    console.log(itinerary)
    const dialogRef = this.dialog.open(ItineraryUpdateComponent, {
      width: "800px",
      height: "400px",
      data: { itinerary: itinerary }
    }
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.listingItineraryService.updateListingItinerary(itinerary['id'], result).subscribe((data) => {
          this.toasterMessageService.success(this.vcr, "Sucessfully updated..... ", "SUCCESS");
          this.listingItinerary = null
          this.getItinery(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }

  itineraryDelete(id): void {
    const dialogRef = this.dialog.open(ItineraryDeleteComponent, {
      // width: '800px',
      // height: '400px',
      data: {
        id: id
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listingItenaryService.deleteItinerary(result).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Itinerary Deleted !!!", "SUCCESS");
          this.listingItinerary = null
          this.getItinery(this.listing.businessDirectory.id, this.businessId)
        })
      }
    });
  }


  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };

  // gallery images
  images: GALLERY_IMAGE[] = [


  ];

  constructor(
    private router: ActivatedRoute,
    private listService: ListingService,
    private listingItenaryService: ListingItineraryService,
    private listingFactService: ListingFactService,
    private listingDestinationService: ListingDestinationService,
    private categoryService: CategoryService,
    private dialog: MatDialog,
    private toasterMessageService: ToasterMessageService,
    private vcr: ViewContainerRef,
    private listingTransportrouteService: ListingTransportRouteService,
    private listingCheckListService: ListingChecklistService,
    private listingIncExcService: ListingIncludeService,
    private businessDirService: BusinessDirService,
    private listingPriceService: ListingPriceService,
    private listingAddonsPriceService: ListingAddonsService,
    private listingFaqService: ListingFaqService,
    private listingMultimediaService: ListingMultimediaService,
    private commonService: CommonService,
    private statusService: StatusService,
    private listingSettingService: ListingSettingService,
    private listingService: ListingService,
    private userService: UserService,
    private datePipe: DatePipe,
    private listingAddonsService: ListingAddonsService,
    private listindDestinationService: ListingDestinationService,
    private lisitngRouteService: ListingTransportRouteService,
    private listingRouteService: ListingTransportRouteService,
    private lisitngFaqService: ListingFaqService,
    private listingItineraryService: ListingItineraryService,
    private listingPricingService: ListingPriceService,
    private reviewService: ReviewService
  ) { }

  ngOnInit() {
    this.calendarOptions = {
      editable: true,
      eventLimit: false,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
      },
    };
    this.statusList = this.statusService.getStatusList();
    this.businessId = this.router.snapshot.params['id']
    this.getPackage(this.businessId)
  }

  getPackage(businessId: number) {
    this.listService.getDetailPackageList(businessId).subscribe((data: Listing) => {
      this.listing = data
      this.setFeatureStartEnd(this.listing)
      this.busId = this.listing.businessDirectory.id
      this.getItinery(this.listing.businessDirectory.id, this.businessId)
      this.getPackageFacts(this.listing.businessDirectory.id, this.businessId)
      this.getPackageDestination(this.listing.businessDirectory.id, this.businessId)
      this.getCheckList(this.listing.businessDirectory.id, this.businessId)
      this.getIncExc(this.listing.businessDirectory.id, this.businessId)
      this.getPackageTransportRoute(this.listing.businessDirectory.id, this.businessId)
      this.getCategoryTitle(this.listing.category.id)
      this.getBusinessName(this.listing.businessDirectory.id)
      this.getListingPricing(this.listing.businessDirectory.id, this.businessId)
      this.getListingAddonsPricing(this.listing.businessDirectory.id, this.businessId)
      this.getListingFaq(this.listing.businessDirectory.id, this.businessId)
      this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
      this.getReview(this.listing.businessDirectory.id, this.businessId)
      this.categoryService.getCategoryById(data.category.id).subscribe((data) => {
        this.categoryCode = data['code'];

      });
    })
  }
  data =[] 
  getReview(businessId: number, listingBusinessId: number){
    this.reviewService.getReviews(businessId, listingBusinessId)
      .subscribe(data => {
        for(let i in data){
          let element = data[i]
          this.rows.push(element, {detailRow: true, element})
        }
        this.dataSource =  this.rows
      })
  }
  getItinery(businessId: number, listingBusinessId: number) {
    this.listingItenaryService.getListingItinerary(businessId, listingBusinessId)
      .subscribe((itineryData: ListingItinerary) => {
        this.listingItinerary = itineryData
      })
  }
  getCategoryTitle(categoryId: number) {
    this.categoryService.getCategoryById(categoryId).subscribe((data: Category) => {
      this.category = data

    })
  }
  getPackageFacts(businessId, PackageId) {
    this.listingFactService.getListingFactByPackage(businessId, PackageId).subscribe((data) => {
      this.listingFact = data[0]
    })
  }
  getPackageDestination(businessId, PackageId) {
    this.listingDestinationService.getListingDestByPackage(businessId, PackageId).subscribe((data: ListingDestination[]) => {
      this.listingDestination = data
    })
  }

  getPackageTransportRoute(businessId, packageId) {
    this.listingTransportrouteService.getTransportRoute(businessId, packageId).subscribe((data: TransportRoute[]) => {
      this.transpostRoutes = data
    })
  }

  getCheckList(businessId, packageId) {
    this.listingCheckListService.viewCheckList(businessId, packageId).subscribe((data: any) => {
      this.checklist = data
    })
  }
  getIncExc(businessId, packageId) {
    this.listingIncExcService.viewIncExc(businessId, packageId).subscribe((data: any) => {
      this.includeExcludes = data
    })
  }

  getBusinessName(businessId: number) {
    this.businessDirService.getSingleBusiness(businessId).subscribe((data: BusinessDirectory) => {
      this.businessDir = data
    })
  }
  getListingPricing(businessId: number, listingId: number) {
    this.listingPriceService.getListingPrice(businessId, listingId).subscribe((data: ListingPricing[]) => {
      this.listingPrices = data
    })
  }
  getListingAddonsPricing(businessId: number, listingId: number) {
    this.listingAddonsPriceService.getListingAddonPrice(businessId, listingId).subscribe((data: ListingAddons[]) => {
      this.listingAddonsPrices = data
    })
  }
  getListingFaq(businessId: number, listingId: number) {
    this.listingFaqService.getListingFaq(businessId, listingId).subscribe((data: ListingFaq[]) => {
      this.listingFaqs = data
    })
  }
  getListingMultimedia(businessId: number, listingId: number) {
    this.listingMultimediaService.getListingMultimediaGallery(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        let imgObj = {
          url: element.fileUrl,
          thumbnailUrl: element.fileUrl,
          id: element.id
        }
        this.images.push(imgObj)
      }
      // console.log(data)
    })
    this.listingMultimediaService.getListingMultimediaFeature(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.featureImage = data[0]['fileUrl']
      this.featureImageId = data[0]['id']
    })
    this.listingMultimediaService.getListingMultimediaBanner(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.bannerImage = data[0]['fileUrl']
      this.bannerImageId = data[0]['id']
    })

    this.listingMultimediaService.getListingMultimediaVideo(businessId, listingId).subscribe((data: ListingMultimeda) => {
      this.videos = data
    })

    this.listingMultimediaService.getListingMultimediaMap(businessId, listingId).subscribe((data: ListingMultimeda) => {
      this.map = data[0]
    })
  }


  openGallery(index: number = 0) {
    this.ngxImageGallery.open(index);
  }

  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }

  // next image in gallery


  /**************************************************/

  // EVENTS
  // callback on gallery opened
  galleryOpened(index) {
    // console.info('Gallery opened at index ', index);
  }

  // callback on gallery closed
  galleryClosed() {
    // console.info('Gallery closed.');
  }

  // callback on gallery image clicked
  galleryImageClicked(index) {
    // console.info('Gallery image clicked with index ', index);
  }

  // callback on gallery image changed
  galleryImageChanged(index) {
    // console.info('Gallery image changed to index ', index);
  }

  // callback on user clicked delete button
  deleteImage(index) {
    // console.info('Delete image at index ', index);
  }



  public checked: Boolean = true
  public disable: Boolean = false
  public stateLabel: any = ""
  public publishStatus(id, status, i) {
    let data = {
      status: status
    }
    this.listingPriceService.updatePricing(id, data).subscribe(data => {
      if (data['status'] === "PUBLISH") {
        this.toasterMessageService.success(this.vcr, "Price Published !!!", "SUCCESS")
        this.listingPrices[i]['status'] = 'PUBLISH'
      }
    })
  }

  public draftAddonStatus(id, status, i) {
    let data = {
      status: status
    }
    this.listingAddonsService.updateAddons(id, data).subscribe((data: ListingAddons) => {
      if (data['status'] === "DRAFT") {
        this.toasterMessageService.success(this.vcr, "Addons Price Drafted !!!", "SUCCESS")
        this.listingPrices[i]['status'] = 'DRAFT'
      }
    })
  }
  public publishAddonStatus(id, status, i) {
    let data = {
      status: status
    }
    this.listingAddonsService.updateAddons(id, data).subscribe(data => {
      if (data['status'] === "PUBLISH") {
        this.toasterMessageService.success(this.vcr, "Addons Price Published !!!", "SUCCESS")
        this.listingPrices[i]['status'] = 'PUBLISH'
      }
    })
  }

  public draftStatus(id, status, i) {
    let data = {
      status: status
    }
    this.listingPriceService.updatePricing(id, data).subscribe((data: ListingAddons) => {
      if (data['status'] === "DRAFT") {
        this.toasterMessageService.success(this.vcr, "Price Drafted !!!", "SUCCESS")
        this.listingPrices[i]['status'] = 'DRAFT'
      }
    })
  }

  deleteAddons(id) {
    this.listingAddonsPriceService.deleteAddons(id).subscribe(data => {
      this.toasterMessageService.success(this.vcr, "Addons Deleted !!!", "SUCCESS")
      this.listingAddonsPrices = []
      this.getListingAddonsPricing(this.listing.businessDirectory.id, this.businessId)
    })
  }

  public setFeatureStartEnd(listing: Listing) {
    if (this.listing) {
      if (this.listing['featuredStart']) {
        let start = new Date(this.listing['featuredStart'])
        this.featuredStart = {
          date: {
            year: start.getFullYear(),
            month: (start.getMonth() + 1),
            day: (start.getDate())
          }
        }
      }
      if (this.listing['featuredEnd']) {
        let end = new Date(this.listing['featuredEnd'])
        this.featuredEnd = {
          date: {
            year: end.getFullYear(),
            month: (end.getMonth() + 1),
            day: (end.getDate())
          }
        }
      }
    }
  }

  public listingStatusFxn(listing: Listing, status: any) {
    if (listing['status'] !== status) {
      listing['status'] = status
      this.listingService.updateListingGeneralInfo(listing['id'], listing).subscribe(data => {
        this.toasterMessageService.success(this.vcr, "Status Updated To " + status + " !!!", "SUCCESS")
      }, (error) => {
        this.toasterMessageService.error(this.vcr, error.message, "ERROR")
      })
    }
  }

  public enabled = true
  public disabled = false
  public visibilityState(listing: Listing, visibilityEvent) {
    listing['isVisible'] = (listing['isVisible'] === 1) ? 0 : 1
    this.listingService.updateListingGeneralInfo(listing['id'], listing).subscribe((data: Listing) => {
      let string = (listing['isVisible'] === 1) ? "Enabled" : "Disabled"
      this.toasterMessageService.success(this.vcr, "Visibility " + string + " !!!", "SUCCESS")
      this.listing = null;
      this.getPackage(this.businessId)
      this.images = []
      this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
    }, (error) => {
      this.toasterMessageService.error(this.vcr, error.message, "ERROR")
    })
  }

  public instantBookingFxn(listing: Listing) {
    listing['instantBooking'] = (listing['instantBooking'] === 1) ? 0 : 1
    this.listingService.updateListingGeneralInfo(listing['id'], listing).subscribe((data: Listing) => {
      let string = (listing['instantBooking'] === 1) ? "Enabled" : "Disabled"
      this.toasterMessageService.success(this.vcr, "Instant Booking " + string + " !!!", "SUCCESS")
      this.listing = null;
      this.getPackage(this.businessId)
      this.images = []
      this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
    }, (error) => {
      this.toasterMessageService.error(this.vcr, error.message, "ERROR")
    })
  }

  public featuredOn(listing: Listing, featureStatus) {
    if (listing['isFeatured'] === 0) {
      const dialogRef = this.dialog.open(SettingComponent, {
        width: "800px",
        height: "400px",
        data: {
          featuredStart: this.featuredStart,
          featuredEnd: this.featuredEnd,
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        listing['isFeatured'] = 1
        if (result['startDate']['formatted'] !== null || result['endDate']['formatted'] !== null) {
          listing['featuredStart'] = result['startDate']['formatted']
          listing['featuredEnd'] = result['endDate']['formatted']
        }
        this.listingService.updateListingGeneralInfo(listing['id'], listing).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Feature Enabled !!!", "SUCCESS")
          this.listing = null;
          this.getPackage(this.businessId)
          this.images = []
          this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
        }, error => {
          this.toasterMessageService.error(this.vcr, error.message, "SUCCESS")
        });
      })
    }
  }

  public featuredOff(listing: Listing) {
    listing['isFeatured'] = 0
    listing['featuredStart'] = null
    listing['featuredEnd'] = null
    this.listingService.updateListingGeneralInfo(listing['id'], listing).subscribe(data => {
      this.featuredStart = { date: { year: null, month: null, day: null } };
      this.featuredEnd = { date: { year: null, month: null, day: null } };
      this.toasterMessageService.success(this.vcr, "Feature Disabled !!!", "SUCCESS")
      this.listing = null;
      this.getPackage(this.businessId)
      this.images = []
      this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
    }, error => {
      this.toasterMessageService.error(this.vcr, error.message, "SUCCESS")
    });
  }

  public updateFeatured(listing) {
    const dialogRef = this.dialog.open(SettingComponent, {
      width: "800px",
      height: "400px",
      data: {
        featuredStart: this.featuredStart,
        featuredEnd: this.featuredEnd,
      },
    }); dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result['startDate']['formatted']) {
          listing['featuredStart'] = result['startDate']['formatted']
          listing['featuredEnd'] = this.datePipe.transform(listing['featuredEnd'], 'yyyy-MM-dd');
        }
        if (result['endDate']['formatted']) {
          listing['featuredEnd'] = result['endDate']['formatted']
          listing['featuredStart'] = this.datePipe.transform(listing['featuredStart'], 'yyyy-MM-dd');
        }
        this.listingService.updateListingGeneralInfo(listing['id'], listing).subscribe(data => {
          this.toasterMessageService.success(this.vcr, "Feature Date Updated !!!", "SUCCESS")
          this.listing = null;
          this.getPackage(this.businessId)
          this.images = []
          this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
        }, error => {
          this.toasterMessageService.error(this.vcr, error.message, "SUCCESS")
        });
      }
    })
  }

  public seoAdd(listing: Listing) {
    console.log(listing)
    const dialogRef = this.dialog.open(PackageSeoComponent, {
      width: "800px",
      height: "400px",
      data: {
      },
    }); dialogRef.afterClosed().subscribe(data => {
      if (data) {
        data['metaTitle'] = (data['metaTitle']) ? data['metaTitle'] : listing['title'];
        this.listingService.updateListingGeneralInfo(listing['id'], data).subscribe((data: Listing) => {
          this.toasterMessageService.success(this.vcr, "SEO Saved !!!", "SUCCESS")
          this.listing = null;
          this.getPackage(this.businessId)
          this.images = []
          this.getListingMultimedia(listing.businessDirectory.id, this.businessId)
        })
      }
    })
  }

  public seoEdit(listing: Listing) {
    const dialogRef = this.dialog.open(PackageSeoComponent, {
      width: "800px",
      height: "400px",
      data: {
        listing: listing
      },
    }); dialogRef.afterClosed().subscribe(data => {
      if (data) {
        data['metaTitle'] = (data['metaTitle']) ? data['metaTitle'] : listing['title'];
        this.listingService.updateListingGeneralInfo(listing['id'], data).subscribe((data: Listing) => {
          this.toasterMessageService.success(this.vcr, "SEO Saved !!!", "SUCCESS")
          this.listing = null;
          this.getPackage(this.businessId)
          this.images = []
          this.getListingMultimedia(listing.businessDirectory.id, this.businessId)
        })
      }
    })
  }

  public mapUpdate(map: ListingMultimeda, listing: Listing) {
    const dialogRef = this.dialog.open(MapUpdateComponent, {
      width: "800px",
      height: "400px",
      data: {
        map: map,
        listing: this.businessId,
        business: this.busId
      },
    });
  }

  public isVerifiedState(listing: Listing) {
    listing['isVerified'] = (listing['isVerified'] === 1) ? 0 : 1
    this.listingService.updateListingGeneralInfo(listing['id'], listing).subscribe((data: Listing) => {
      let string = (listing['isVerified'] === 1) ? "Verified" : "Not-verified"
      this.toasterMessageService.success(this.vcr,  string + " !!!", "SUCCESS")
      this.listing = null;
      this.getPackage(this.businessId)
      this.images = []
      this.getListingMultimedia(this.listing.businessDirectory.id, this.businessId)
    }, (error) => {
      this.toasterMessageService.error(this.vcr, error.message, "ERROR")
    })
  }
  publishReview(id, reviewStatus){
    console.log(reviewStatus)

    let data = {
      status: (reviewStatus === "PENDING")?"COMPLETED": "PENDING"
    }
    this.reviewService.updateReview(id, data).subscribe(data=>{
      console.log(data)
      let msg = "Review "
      msg += (data['status'] === 'PENDING')?'status changed to PENDING': 'published'
      this.toasterMessageService.success(this.vcr, msg, "SUCCESS !!!")
      this.dataSource = []
      this.rows = []
      this.getReview(this.listing.businessDirectory.id, this.businessId)
    })
  }
}
