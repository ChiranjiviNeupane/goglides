import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UploadSerivce } from '../../../service/file.upload.service';
import { ListingMultimediaService } from '../../../service/listing-multimedia.service';
import { UserService } from '../../../service/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-feature-edit',
    templateUrl: './feature-edit.component.html',
    styleUrls: ['./feature-edit.component.scss']
})
export class FeatureEditComponent implements OnInit {
    public featureProgressPercentage: number
    public userId: number
    public businessId: number
    public listingId: number


    constructor(
        public dialogRef: MatDialogRef<FeatureEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public fileUploaderService: UploadSerivce,
        private vcr: ViewContainerRef,
        private listingMultimediaService: ListingMultimediaService,
        private userService: UserService,
        private activateRouter: ActivatedRoute
    ) {
        console.log(data)
    }

    ngOnInit() {
        this.getIds()
    }

    ngDoCheck() {
        this.featureProgressValue()
    }

    //image crop
    imageChangedEvent: any = [''];
    featureCropedImage: any = [''];
    featureStatus: Boolean = false

    public getIds() {
        this.userId = this.userService.userRef().id
        this.businessId = this.data['busId']
        this.listingId = this.data['listingId']
    }

    private dataURItoBlob(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: 'image/jpeg'
        });
    }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(image: string) {
        this.featureCropedImage = image;
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }

    public listingMediaJson(type): Object {
        return {
            "id": null,
            "user": {
                "id": this.userId,
            },
            "businessDirectory": {
                "id": this.businessId,
            },
            "listing": {
                "id": this.listingId,
            },
            "status": "PUBLISH",
            "type": type,
            "isApproved": 1,
            "fileUrl": ""
        }
    }


    public chooseFeatureImage(): void {
        var myFile: Blob = this.dataURItoBlob(this.featureCropedImage);
        if (myFile !== null) {
            let object = {
                id: this.data['fileUrl'],
                fileUrl: ""
            }
            this.featureStatus = true
            if (object['id'] === null || object['id'] === undefined || object['id'] === "") {
                this.fileUploaderService.uploadFeatureAndBannerImage(myFile, this.listingMediaJson("FEATURE"), "Feature", this.vcr)
            } else {
                this.fileUploaderService.uploadFeatureAndBannerImage(myFile, object, "Feature", this.vcr)
            }
        }
    }

    public featureProgressValue(): void {
        this.listingMultimediaService.featureProgressValue.subscribe(data => {
            this.featureProgressPercentage = Math.round(data['loaded'] / data['total'] * 100)
            if(this.featureProgressPercentage === 100){
                setTimeout(() => {
                    window.location.reload();
                }, 3000);
            }
        })
    }

}
