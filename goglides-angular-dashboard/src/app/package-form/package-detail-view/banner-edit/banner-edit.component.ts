import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UploadSerivce } from '../../../service/file.upload.service';
import { ListingMultimediaService } from '../../../service/listing-multimedia.service';
import { UserService } from '../../../service/user.service';

@Component({
    selector: 'app-banner-edit',
    templateUrl: './banner-edit.component.html',
    styleUrls: ['./banner-edit.component.scss']
})
export class BannerEditComponent implements OnInit {

    public bannerStatus: Boolean = false
    public bannerProgressPercentage: number

        public userId : number
        public businessId : number
        public listingId : number

    constructor(
        public dialogRef: MatDialogRef<BannerEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public fileUploaderService: UploadSerivce,
        private vcr: ViewContainerRef,
        private listingMultimediaService: ListingMultimediaService,
        private userService: UserService
    ) {
        console.log(data)
    }

    ngOnInit() {
        this.getIds();
    }

    ngDoCheck(){
        this.bannerProgressValue()
      }

      public getIds() {
        this.userId = this.userService.userRef().id
        this.businessId = this.data['busId']
        this.listingId = this.data['listingId']
    }

    //image crop
    imageChangedEvent: any = [''];
    bannerCroppedImage: any = [''];

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(image: string) {
        this.bannerCroppedImage = image;
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }

    public listingMediaJson(type): Object {
        return {
            "id": null,
            "user": {
                "id": this.userId,
            },
            "businessDirectory": {
                "id": this.businessId,
            },
            "listing": {
                "id": this.listingId,
            },
            "status": "PUBLISH",
            "type": type,
            "isApproved": 1,
            "fileUrl": ""
        }
    }

    private dataURItoBlob(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var array = [];
        for (var i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
          type: 'image/jpeg'
        });
      }

    public chooseBannerImage(): void{
        var myFile: Blob = this.dataURItoBlob(this.bannerCroppedImage);
        console.log(myFile)
        if(myFile !== null){
            let object ={
                id: this.data['fileUrl'],
                fileUrl: ""
            }
          this.bannerStatus = true

          console.log(object)
          if (object['id'] === null || object['id'] === undefined || object['id'] === "") {
            this.fileUploaderService.uploadFeatureAndBannerImage(myFile, this.listingMediaJson("BANNER"), "Banner", this.vcr)
          }else{
            this.fileUploaderService.uploadFeatureAndBannerImage(myFile, object, "Banner", this.vcr)
          }
        }
      }

      public bannerProgressValue(): void{
        this.listingMultimediaService.bannerProgressValue.subscribe(data=>{
          this.bannerProgressPercentage = Math.round(data['loaded']/data['total'] *100)
          if(this.bannerProgressPercentage === 100){
              setTimeout(() => {
                  window.location.reload();
              }, 3000);
          }
        })
    }

}
