import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-banner-view',
  templateUrl: './banner-view.component.html',
  styleUrls: ['./banner-view.component.scss']
})
export class BannerViewComponent implements OnInit {

  constructor(
    public dialogRef : MatDialogRef<BannerViewComponent>,
    @Inject( MAT_DIALOG_DATA) public data: any,
  ) {
   console.log(data)
  }

  ngOnInit() {
  }

}
