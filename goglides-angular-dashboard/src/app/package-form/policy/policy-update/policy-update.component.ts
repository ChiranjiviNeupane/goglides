import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';

@Component({
  selector: 'app-policy-update',
  templateUrl: './policy-update.component.html',
  styleUrls: ['./policy-update.component.scss']
})
export class PolicyUpdateComponent implements OnInit {
  policyForm: FormGroup; 

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.policyForm = this.fb.group({
      goglidesPolicy: ['', ],
      customPolicy: ['',  ],
    })
  }

}
