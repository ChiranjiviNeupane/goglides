import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';

@Component({
  selector: 'app-policy-add',
  templateUrl: './policy-add.component.html',
  styleUrls: ['./policy-add.component.scss']
})
export class PolicyAddComponent implements OnInit {

  policyForm: FormGroup; 

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.policyForm = this.fb.group({
      goglidesPolicy: ['', ],
      customPolicy: ['',  ],
    })
  }
}
