import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss']
})
export class PolicyComponent implements OnInit {

  policyForm: FormGroup; 

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.policyForm = this.fb.group({
      goglidesPolicy: ['', ],
      customPolicy: ['',  ],
    })
  }

}
