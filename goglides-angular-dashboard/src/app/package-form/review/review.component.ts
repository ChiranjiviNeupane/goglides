import { Observable } from 'rxjs';
import { Component, OnInit, Input, Output, ElementRef } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatTableDataSource } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import { of } from 'rxjs/observable/of';
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ReviewComponent implements OnInit {
  constructor() {
   
   }
  
  displayedColumns = [ 'name', 'date', 'action'];
  dataSource = new ExampleDataSource();

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  ngOnInit() {
  //   console.log(this.ELEMENT_DATA)
  //   this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
  }
}

export interface Element {
  name: string;
  position: number;
  date: number;
  symbol: string;
}

export class ExampleDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  data: Element[] = [
    { position: 1, name: 'Hydrogen', date: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', date: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', date: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', date: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', date: 10.811, symbol: 'B' },
  ];

  
  connect(): Observable<Element[]> {
    console.log(this.data)
    const rows = [];
    this.data.forEach(element => rows.push(element, { detailRow: true, element }));
    console.log(rows);
    return of(rows);
  }

  disconnect() { }
}
