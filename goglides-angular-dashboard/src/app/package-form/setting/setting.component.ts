import { Component, OnInit, Inject } from '@angular/core';

import {MatChipInputEvent, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {ENTER, COMMA} from '@angular/cdk/keycodes';
import { IMyDpOptions } from 'mydatepicker';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  featureForm: FormGroup
  show:boolean=false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<SettingComponent>,
    private fb: FormBuilder
  ) { console.log(data)}

  ngOnInit() {
    this.featureForm = this.fb.group({
      startDate: [( this.data['featuredStart']['date']['year'] === null)? '':this.data['featuredStart']],
      endDate: [( this.data['featuredEnd']['date']['year']===null)?'':this.data['featuredEnd']]
    })
  }

  toggle(){
    this.show=!this.show;
  }

  //status value
  packageStatus = [
    {value: '0', viewValue: 'Approved'},
    {value: '1', viewValue: 'Rejected'},
    {value: '2', viewValue: 'Draft'},
    {value: '3', viewValue: 'Published'},
  ];

  //chips
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;

  // Enter, comma
  separatorKeysCodes = [ENTER, COMMA];

  city = [
    { name: 'Adventure' },
    { name: 'Holiday' },
    { name: 'Family' },
  ];


  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // Add
    if ((value || '').trim()) {
      this.city.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(fruit: any): void {
    let index = this.city.indexOf(fruit);

    if (index >= 0) {
      this.city.splice(index, 1);
    }
  }

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
  };


}
