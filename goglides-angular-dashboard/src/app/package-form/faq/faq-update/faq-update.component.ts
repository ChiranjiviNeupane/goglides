import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListingFaqService } from '../../../service/listing-faq.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-faq-update',
  templateUrl: './faq-update.component.html',
  styleUrls: ['./faq-update.component.scss']
})
export class FaqUpdateComponent implements OnInit {

  faqForm: FormGroup; 

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<FaqUpdateComponent>,
    private fb: FormBuilder,
    private listingFaqService: ListingFaqService,
  ) {
    console.log(data)
  }

  ngOnInit() {
    this.faqForm = this.fb.group({
      question: [this.data['faq']['question'], Validators.required],
      answer: [this.data['faq']['answer'], Validators.required],
    })
  }


onNoClick(): void {
  this.dialogRef.close();
}

  public updateFaq(value){
  }

}
