import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListingPriceService } from '../../../service/listing-price.service';
import { ListingFaqService } from '../../../service/listing-faq.service';

@Component({
  selector: 'app-faq-delete',
  templateUrl: './faq-delete.component.html',
  styleUrls: ['./faq-delete.component.scss']
})
export class FaqDeleteComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<FaqDeleteComponent>,
    private listingFaqService: ListingFaqService
  ) { }

  ngOnInit() {
  }
  
  public onNoClick(): void{
    this.dialogRef.close();
  }

}
