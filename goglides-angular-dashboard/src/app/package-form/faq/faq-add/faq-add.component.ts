import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { UserService } from '../../../service/user.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { ListingFaqService } from '../../../service/listing-faq.service';
import { ListingFaq } from '../../../model/listing-faq';

@Component({
  selector: 'app-faq-add',
  templateUrl: './faq-add.component.html',
  styleUrls: ['./faq-add.component.scss']
})
export class FaqAddComponent implements OnInit {

  faqForm: FormGroup;
  public userId: number
  public businessId: number
  public listingId: number

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<FaqAddComponent>,
    private fb: FormBuilder,
    private userService: UserService,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    private lisitngFaqService: ListingFaqService
  ) { }

  ngOnInit() {
    this.getIds()
    this.faqFormBuilder()
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = parseInt(this.data['listingId'])
  }

  public faqFormBuilder() {
    this.faqForm = this.fb.group({
      question: '',
      answer: '',
      listing: this.fb.group({
        id: [this.listingId]
      }),
      businessDirectory: this.fb.group({
        id: this.businessId
      }),
      user: this.fb.group({
        id: this.userId
      })
    })
  }

  
  public onClick() {
    this.dialogRef.close();
  }

}
