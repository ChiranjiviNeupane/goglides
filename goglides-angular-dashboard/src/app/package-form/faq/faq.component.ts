import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../service/user.service';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { ListingFaqService } from '../../service/listing-faq.service';
import { ListingFaq } from '../../model/listing-faq';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  public faqForm: FormGroup; 
  public userId: number;
  public businessId: number;
  public listingId: number;
  public faqArray = [];

  constructor(
    private fb: FormBuilder,
    private vcr: ViewContainerRef,
    private activateRouter: ActivatedRoute,
    private userService: UserService,
    private toasterMessageService: ToasterMessageService,
    private router: Router,
    private lisitngFaqService: ListingFaqService
  ) { }

  ngOnInit() {
    this.getIds();
    this.faqFormBuilder()
  }

  public getIds(){
    this.userId = this.userService.userRef().id
    this.businessId = this.activateRouter.snapshot.params['businessId'];
    this.listingId = this.activateRouter.snapshot.params['listingId'];
  }

  public faqFormBuilder(){
    this.faqForm = this.fb.group({
      question: '',
      answer: '',
      listing:this.fb.group({
        id: [this.listingId]
      }),
      businessDirectory:this.fb.group({
        id: this.businessId
      }),
      user:this.fb.group({
        id: this.userId
      })
    })
  }

  // faqTitle(): FormGroup {
  //   return this.fb.group({
  //     question: '',
  //     answer: '',
  //   });
  // }
  
  // get faqForms() {
  //   return this.faqForm.get('faq_info') as FormArray
  // }
  
  // addTitle() {
  //   this.faqForms.push(this.faqTitle());
  // }
  
  // deleteTitle(i) {
  //   this.faqForms.removeAt(i)
  // }

  public addFaq(): void{
    this.faqArray.push(this.faqForm.value);
  }

  public deleteFaq(i: number): void{
    this.faqArray.splice(i, 1);
  }

  public saveFaqs(){
    let counter = 0;
    if( this.faqArray.length !== 0){
      for(let data of this.faqArray){
        this.lisitngFaqService.saveFaq(data).subscribe((data: ListingFaq)=>{
          counter++;
          if(counter === this.faqArray.length){
            this.toasterMessageService.success(this.vcr, "Faq Saved !!!", "Success")
            setTimeout(() => {
              this.redirectTo()
            }, 3000);
          }
        },(error)=>{
          this.toasterMessageService.error(this.vcr, error.error.message, "Error")
        })
      }
    }else{
      this.redirectTo()
    }
  }

  public redirectTo(): void{
    this.router.navigate(['/packages/detailview/', this.listingId]);
  }
}
