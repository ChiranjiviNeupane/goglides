import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';

import { MatChipInputEvent, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { UserService } from '../../../service/user.service';
import { CommonService } from '../../../service/common.service';
import { BusinessDirectory } from '../../../model/business-directory';
import { ListingDestination } from '../../../model/listingDestination';
import { ListingDestinationService } from '../../../service/listing-destination.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';

@Component({
  selector: 'app-destination-add',
  templateUrl: './destination-add.component.html',
  styleUrls: ['./destination-add.component.scss']
})
export class DestinationAddComponent implements OnInit {
  public userId: number
  public businessId: number
  public listingId: number
  
  businessDirectoryId: number
  destinationForm: FormGroup;
  cities: string = ""
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<DestinationAddComponent>,
    private fb: FormBuilder, public userService: UserService,
    public commonService: CommonService,
    public listindDestinationService: ListingDestinationService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService,
    private routerActivate: ActivatedRoute

  ) { }

  ngOnInit() {
    this.getIds()
    this.destinationForm = this.fb.group({

      destination_detail: this.fb.array([
        this.destinations()
      ]),
    })
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = parseInt(this.data['listingId'])
  }

  destinations(): FormGroup {
    return this.fb.group({
      country: ["", Validators.required],
      city: ["", Validators.required],
      user: this.fb.group({
        id: [this.userService.userRef().id]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessId]
      }),
      listing: this.fb.group({
        id: [this.listingId]
      })
    });
  }

  get destinationDetailForms() {
    return this.destinationForm.get('destination_detail') as FormArray
  }

  addDestination() {
    this.destinationDetailForms.push(this.destinations());
  }

  deleteDestination(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.destinationDetailForms.removeAt(i)
    }

  }

  close(){
    this.dialogRef.close();
  }


}
