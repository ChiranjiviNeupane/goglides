import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListingDestination } from '../../../model/listingDestination';
import { ListingDestinationService } from '../../../service/listing-destination.service';

@Component({
  selector: 'app-destination-update',
  templateUrl: './destination-update.component.html',
  styleUrls: ['./destination-update.component.scss']
})
export class DestinationUpdateComponent implements OnInit {

  businessDirectoryId:number
  destinationForm: FormGroup; 
  cities:string=""
    constructor(
      private fb: FormBuilder,
      @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<DestinationUpdateComponent>,
    private listingDestinationService: ListingDestinationService
    ) {console.log(data) }

  ngOnInit() {
    this.destinationForm = this.fb.group({
      country: [this.data['destination']['country'], Validators.required],
      city: [this.data['destination']['city'], Validators.required],
      user: this.fb.group({
        id:[this.data['destination']['user']['id']]
      }),
      businessDirectory: this.fb.group({
        id:[this.data['destination']['businessDirectory']['id']]
      }),
      listing: this.fb.group({
        id:[this.data['destination']['listing']['id']]
      })
    })
  }

}
