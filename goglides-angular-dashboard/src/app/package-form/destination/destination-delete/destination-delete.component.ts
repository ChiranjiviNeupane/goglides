import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListingDestinationService } from '../../../service/listing-destination.service';

@Component({
  selector: 'app-destination-delete',
  templateUrl: './destination-delete.component.html',
  styleUrls: ['./destination-delete.component.scss']
})
export class DestinationDeleteComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<DestinationDeleteComponent>,
    private listingDestinationService: ListingDestinationService
  ) { }

  ngOnInit() {
  }


  public onNoClick():void{
    this.dialogRef.close();
  }

}
