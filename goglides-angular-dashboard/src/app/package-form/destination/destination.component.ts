import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';

import { MatChipInputEvent } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { UserService } from '../../service/user.service';
import { CommonService } from '../../service/common.service';
import { BusinessDirectory } from '../../model/business-directory';
import { ListingDestination } from '../../model/listingDestination';
import { ListingDestinationService } from '../../service/listing-destination.service';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { Route } from 'aws-sdk/clients/ec2';

@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.scss']
})
export class DestinationComponent implements OnInit {
  public listingId: number
  public businessDirectoryId: number
  public destinationForm: FormGroup;
  public cities: string = ""
  constructor(private fb: FormBuilder, public userService: UserService,
    public commonService: CommonService,
    public listindDestinationService: ListingDestinationService,
    public vcr: ViewContainerRef,
    public toasterMessageService: ToasterMessageService,
    private routerActivate: ActivatedRoute,
    private router: Router

  ) { }

  ngOnInit() {
    this.listingId = this.routerActivate.snapshot.params['listingId']
    this.businessDirectoryId = this.routerActivate.snapshot.params['businessId']
    this.destinationForm = this.fb.group({
      destination_detail: this.fb.array([
        this.destinations()
      ]),
    })
  }

  destinations(): FormGroup {
    return this.fb.group({
      country: ["", Validators.required],
      city: ["", Validators.required],
      user: this.fb.group({
        id: [this.userService.userRef().id]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessDirectoryId]
      }),
      listing: this.fb.group({
        // id:[this.commonService.getListingId()]
        id: [this.listingId]
      })
    });
  }

  get destinationDetailForms() {
    return this.destinationForm.get('destination_detail') as FormArray
  }

  addDestination() {
    this.destinationDetailForms.push(this.destinations());
  }

  deleteDestination(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.destinationDetailForms.removeAt(i)
    }

  }

  saveDestination(): void {
    let counter: number = 0
    for (let data of this.destinationForm.value.destination_detail) {
      this.listindDestinationService.saveListingDestination(data).subscribe((response: Response) => {
        counter++
        if (counter === this.destinationForm.value.destination_detail.length) {
          this.toasterMessageService.success(this.vcr, "Listing Tour Destination Saved !!!", "Success")
          setTimeout(() => {
            this.redirectTo();
          }, 3000);
        }
      })
    }
  }

  public redirectTo() {
    this.router.navigate(['/packages/pricing/', this.businessDirectoryId, this.listingId]);
  }

}
