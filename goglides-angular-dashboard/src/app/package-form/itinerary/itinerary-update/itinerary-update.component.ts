import { ListingItineraryService } from '../../../service/listing-itinerary.service';
import { UserService } from '../../../service/user.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-itinerary-update',
  templateUrl: './itinerary-update.component.html',
  styleUrls: ['./itinerary-update.component.scss']
})
export class ItineraryUpdateComponent implements OnInit {

  updateItineraryForm: FormGroup
  businessId: number
  listingId: number
  public listingItinerary=[]
  itineraryDays: number;
  days:String
  itinerayId: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<ItineraryUpdateComponent>,

    private fb: FormBuilder,
    private userService: UserService,

) { }


public confirmUpdate(updateItinerary): void{
}

onNoClick(): void {
  this.dialogRef.close();
}


  ngOnInit() {

    this.itinerayId=this.data.itinerary.id
    this.updateItineraryForm = this.fb.group({
      title:[this.data.itinerary.title, Validators.required],
      description:[this.data.itinerary.description,Validators.required]
    
    });
  }


 
 //editor
 editorConfig2 = {
  editable: true,
  spellcheck: false,
  height: '10rem',
  minHeight: '5rem',
  placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
  translate: 'no',
  toolbar: [
    ['undo', 'redo'],
    ['orderedList', 'unorderedList'],
    ['removeFormat'],
  ]
};

htmlContent2 = '';
}
