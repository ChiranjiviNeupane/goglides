import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItineraryUpdateComponent } from './itinerary-update.component';

describe('ItineraryUpdateComponent', () => {
  let component: ItineraryUpdateComponent;
  let fixture: ComponentFixture<ItineraryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItineraryUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItineraryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
