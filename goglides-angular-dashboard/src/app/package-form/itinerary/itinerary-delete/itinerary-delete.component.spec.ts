import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItineraryDeleteComponent } from './itinerary-delete.component';

describe('ItineraryDeleteComponent', () => {
  let component: ItineraryDeleteComponent;
  let fixture: ComponentFixture<ItineraryDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItineraryDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItineraryDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
