import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListingItineraryService } from '../../../service/listing-itinerary.service';

@Component({
  selector: 'app-itinerary-delete',
  templateUrl: './itinerary-delete.component.html',
  styleUrls: ['./itinerary-delete.component.scss']
})
export class ItineraryDeleteComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<ItineraryDeleteComponent>,
    public listingItenaryService: ListingItineraryService
  ) {}

  ngOnInit() {
  }

 

  public onNoClick(): void{
    this.dialogRef.close();
  }
}
