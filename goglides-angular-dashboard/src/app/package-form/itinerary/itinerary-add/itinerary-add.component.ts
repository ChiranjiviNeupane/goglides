import { ListingItineraryService } from '../../../service/listing-itinerary.service';
import { UserService } from '../../../service/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-itinerary-add',
  templateUrl: './itinerary-add.component.html',
  styleUrls: ['./itinerary-add.component.scss']
})
export class ItineraryAddComponent implements OnInit {

  addItineraryForm: FormGroup
  businessId: number
  listingId: number
  public listingItinerary=[]
  itineraryDays: number;
  days:String

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<ItineraryAddComponent>,
    private fb: FormBuilder,
    private userService: UserService,
    private listingItineraryService: ListingItineraryService,

) { }


onNoClick(): void {
  this.dialogRef.close();
}


  ngOnInit() {
    this.listingId=this.data.listing.id
    this.businessId=this.data.listing.businessDirectory.id

    this.getAllListingItinerary(this.businessId,this.listingId);

    this.addItineraryForm = this.fb.group({
      title:["", Validators.required],
      description:["",Validators.required],
      user: this.fb.group({
        id: [this.userService.userRef().id, Validators.required]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessId, Validators.required]
      }),
      listing: this.fb.group({
        id: [this.listingId, Validators.required]
      })
    });
  }

  getAllListingItinerary(businessId,listingId){
    this.listingItinerary=[]
    this.listingItineraryService.getListingItineraryByListingId(businessId,listingId).subscribe((data) =>{
    this.listingItinerary =data   
        this.itineraryDays= this.listingItinerary.length+1   
        this.days="Days "+this.itineraryDays+" :"
    });
  }
 
 //editor
 editorConfig2 = {
  editable: true,
  spellcheck: false,
  height: '10rem',
  minHeight: '5rem',
  // placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
  translate: 'no',
  toolbar: [
    ['undo', 'redo'],
    ['orderedList', 'unorderedList'],
    ['removeFormat'],
  ]
};

htmlContent2 = '';

}
