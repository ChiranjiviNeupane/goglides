import { UserService } from '../../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Listing } from '../../model/listing';
import { Component, OnInit, AfterViewInit, DoCheck, OnChanges, AfterContentChecked, AfterContentInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { ListingItineraryService } from '../../service/listing-itinerary.service';
import { ListingItinerary } from '../../model/listing-itinerary';
import { User } from '../../model/user';
import { CommonService } from '../../service/common.service';

@Component({
  selector: 'app-itinerary',
  templateUrl: './itinerary.component.html',
  styleUrls: ['./itinerary.component.scss']
})
export class ItineraryComponent implements OnInit, AfterViewInit {

  listingId: number;
  itineraryDays: number;
  days:String
  businessId:number;


  ngAfterViewInit(){
    //stuff that doesn't do view changes
  }
  shows=false;

  itineraryForm: FormGroup; 
  show:boolean=false;
  
  public listingItinerary=[]

  // setting user

  public saveItinerary(itinerary){
    this.itineraryDays=this.itineraryDays+1;
    this.days="Days "+this.itineraryDays+ ": "
    this.listingItineraryService.addListingItinerary(itinerary).subscribe((data) => {
      this.getAllListingItinerary(this.businessId,this.listingId);
      this.resetFormData();
    });
  }

  public gotoFacts(){
    console.log("clicked")
    this.router.navigate(['/packages/facts',this.businessId, this.listingId]);
  }

  constructor(private fb: FormBuilder,
     private listingItineraryService: ListingItineraryService, 
     private commonService: CommonService,
     private activeRouter: ActivatedRoute,
     private userService: UserService,
     private router: Router,
    ) { }

  getAllListingItinerary(businessId,listingId){
    this.listingItinerary=[]
    this.listingItineraryService.getListingItineraryByListingId(businessId,listingId).subscribe((data) =>{
    this.listingItinerary =data   
        this.itineraryDays= this.listingItinerary.length+1   
        this.days="Days "+this.itineraryDays+" :"
    });

  }
  ngOnInit() {  
    
    this.listingId=this.activeRouter.snapshot.params['listingId'];
    this.businessId=this.activeRouter.snapshot.params['businessId'];
    this.getAllListingItinerary(this.businessId,this.listingId);
 
    this.resetFormData();

    } 

    public resetFormData(){
      this.itineraryForm = this.fb.group({
        title:["", Validators.required],
        description:["",Validators.required],
        user: this.fb.group({
          id: [this.userService.userRef().id, Validators.required]
        }),
        businessDirectory: this.fb.group({
          id: [this.businessId, Validators.required]
        }),
        listing: this.fb.group({
          id: [this.listingId, Validators.required]
        })
      });
    }

 
 //editor
  editorConfig = {
    editable: true,
    spellcheck: false,
    height: '15rem',
    minHeight: '8rem',
    // placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
    translate: 'no',
    toolbar: [
      ['undo', 'redo'],
      ['orderedList', 'unorderedList'],
      ['removeFormat'],
    ]
  };

  // htmlContent2 = '';


}
