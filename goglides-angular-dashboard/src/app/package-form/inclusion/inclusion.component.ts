import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { Location } from '@angular/common';
import { IncludeExcludeService } from '../../service/include-exclude.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { ListingIncludeService } from '../../service/listing-include.service';
import { UserService } from '../../service/user.service';
import { integer } from 'aws-sdk/clients/cloudfront';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { ListingService } from '../../service/listing.service';
import { CategoryService } from '../../service/category.service';
import { Listing } from '../../model/listing';
import { setTimeout } from 'timers';

@Component({
  selector: 'app-inclusion',
  templateUrl: './inclusion.component.html',
  styleUrls: ['./inclusion.component.scss']
})
export class InclusionComponent implements OnInit {

  public inclusionForm: FormGroup;
  public exclusionForm: FormGroup;

  public businessId: number;
  public listingId: number;
  public userId: number;
  public categoryId: number

  public includeFilterOption: string
  public excludeFilterOption: string

  public includeCheckListArray: any
  public excludeCheckListArray: any
  public filterArray: any

  public includeArray: any = []
  public excludeArray: any = []

  constructor(private fb: FormBuilder,
    private includeExcludeService: IncludeExcludeService,
    private activateRouter: ActivatedRoute,
    private listingingIncludeExclude: ListingIncludeService,
    public userService: UserService,
    public toasterMessageService: ToasterMessageService,
    private vcr: ViewContainerRef,
    private listingService: ListingService,
    private categoryService: CategoryService,
    private commonService: CommonService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getIds()
    this.inclusionFormBuilder();
    this.exclusionFormBuilder();
  }
  

  public getIds(){
    this.userId = this.userService.userRef().id
    this.businessId = this.activateRouter.snapshot.params['businessId'];
    this.listingId = this.activateRouter.snapshot.params['listingId'];
    this.listingService.getListingById(this.listingId).subscribe((data:Listing)=>{
      this.categoryService.getCategoryById(data.category.id).subscribe((data) => {
       this.categoryId = data['id'];
       this.inclusionForm.value.category.id = this.categoryId
        this.exclusionForm.value.category.id = this.categoryId
       this.commonService.setCategoryId(this.categoryId);
       this.pushInIncludeExcludeArray();
      }); 
    })
  }

  public initialize(){
    this.pushInIncludeExcludeArray();
  }

  public inclusionFormBuilder() {
      this.inclusionForm = this.fb.group({
        title: '',
        status: 'AWAITING_APPROVAL',
        user: this.fb.group({
          id: this.userId
        }),
        category: this.fb.group({
          id: this.categoryId
        })
      });
  }

  public exclusionFormBuilder(): void {
      this.exclusionForm = this.fb.group({
        title: '',
        status: 'AWAITING_APPROVAL',
        user: this.fb.group({
          id: this.userId
        }),
        category: this.fb.group({
          id: this.categoryId
        })
      })
  }

  public pushInIncludeExcludeArray(): void {
    this.includeExcludeService.getIncludeAndExclude(this.categoryId).subscribe((response) => {
      this.includeCheckListArray = response;
      this.excludeCheckListArray = response;
      this.filterArray = response
    })
  }

  public pushInIncExcArray(name, event): void {
    if (name === 'include') {
      this.includeArray.push(event.value)
      this.inclusionForm.reset();
      this.inclusionFormBuilder()
    } else {
      this.excludeArray.push(event.value)
      this.exclusionForm.reset();
      this.exclusionFormBuilder()
    }
  }

  public popFromListArray(i, name, event): void {
    if (name === 'include') {
      this.includeArray.splice(i, 1);
    } else {
      this.excludeArray.splice(i, 1);
    }
  }

  public addInIncludeExclude(name, includeAutoComplete): void {
    if (name == 'include') {
      this.includeExcludeService.addIncludeExclude(this.inclusionForm.value).subscribe((data) => {
        this.toasterMessageService.success(this.vcr, "Inclusion Saved !!!","Success")
        this.includeArray.push(data)
        this.inclusionForm.reset();
        this.inclusionFormBuilder()
      })
    } else {
      this.includeExcludeService.addIncludeExclude(this.exclusionForm.value).subscribe((data) => {
        this.toasterMessageService.success(this.vcr, "Exclusion Saved !!!","Success")
        this.excludeArray.push(data)
        this.exclusionForm.reset();
        this.exclusionFormBuilder()
      });
    }
  }

  public listingIncludeExcludeJson(data, type): Object {
    return {
      "user": {
        "id": this.userService.userRef().id,
      },
      "businessDirectory": {
        "id": this.businessId,
      },
      "listing": {
        "id": this.listingId,
      },
      "includeExclude": {
        "id": data['id'],
      },
      "type": type
    }
  }

  public saveIncludeAndExclude(): void {
    let includeCounter: number = 0;
    let excludeCounter: number = 0;
    for (let data of this.includeArray) {
      let listingIncludeExclude = this.listingIncludeExcludeJson(data, "INCLUDE")
      this.listingingIncludeExclude.saveIncludeAndExclude(listingIncludeExclude).subscribe(data => {
          includeCounter++
          if(includeCounter === this.includeArray.length){
            this.toasterMessageService.success(this.vcr, "Listing Inclusion Saved !!!","Success")
            this.includeArray = []
          }
      })
    }

    for (let data of this.excludeArray) {
      let listingIncludeExclude = this.listingIncludeExcludeJson(data, "EXCLUDE")
      this.listingingIncludeExclude.saveIncludeAndExclude(listingIncludeExclude).subscribe(data => {
          excludeCounter++
          if(excludeCounter === this.excludeArray.length){
            this.toasterMessageService.success(this.vcr, "Listing Exclusion Saved !!!","Success")
            this.excludeArray = []  
          }
      })
    }

    setTimeout(() => {
      if(!this.includeArray.length && !this.excludeArray.length)    {
        console.log("include array")
        this.redirectTo();
      }
    }, 5000);
  }

  public includeFilterAutoComplete(): void {
    let array = [];
    let option = this.includeFilterOption;
    let currentLength: number = this.includeFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.includeCheckListArray = array
    } else {
      this.pushInIncludeExcludeArray();
    }
  }

  public excludeFilterAutoComplete(): void {
    let array = [];
    let option = this.excludeFilterOption;
    let currentLength: number = this.excludeFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.excludeCheckListArray = array
    } else {
      this.pushInIncludeExcludeArray();
    }
  }

  public redirectTo(){
    console.log("clicked")
    this.router.navigate(['/packages/checklist',this.businessId, this.listingId]);
  }
}
