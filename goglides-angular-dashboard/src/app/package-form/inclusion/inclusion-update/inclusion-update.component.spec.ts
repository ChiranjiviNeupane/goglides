import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InclusionUpdateComponent } from './inclusion-update.component';

describe('InclusionUpdateComponent', () => {
  let component: InclusionUpdateComponent;
  let fixture: ComponentFixture<InclusionUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InclusionUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InclusionUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
