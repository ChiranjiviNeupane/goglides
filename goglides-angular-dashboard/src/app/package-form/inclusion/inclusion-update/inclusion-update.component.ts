import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListingIncludeService } from '../../../service/listing-include.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../../../service/user.service';
import { ListingService } from '../../../service/listing.service';
import { CategoryService } from '../../../service/category.service';
import { IncludeExcludeService } from '../../../service/include-exclude.service';
import { Listing } from '../../../model/listing';
import { Category } from 'aws-sdk/clients/support';

@Component({
  selector: 'app-inclusion-update',
  templateUrl: './inclusion-update.component.html',
  styleUrls: ['./inclusion-update.component.scss']
})
export class InclusionUpdateComponent implements OnInit {

  public includeArray = []
  public excludeArray = []

  public inclusionForm
  public exclusionForm

  public includeFilterOption: any
  public excludeFilterOption: any

  public includeCheckListArray
  public excludeCheckListArray
  public filterArray


  public listingId: number
  public businessId: number
  public categoryId: number
  public userId: number

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private listingIncExcService: ListingIncludeService,
    private vcr: ViewContainerRef,
    private toasterMessageService: ToasterMessageService,
    private fb: FormBuilder,
    private userService: UserService,
    private listingService: ListingService,
    private categoryService: CategoryService,
    private includeExcludeService: IncludeExcludeService,
    private dialogRef: MatDialogRef<InclusionUpdateComponent>,
  ) { console.log(data) }

  ngOnInit() {
    this.getIds()
    this.inclusionFormBuilder();
    this.exclusionFormBuilder();
    this.setIncludeExclude();
  }

  public getIds() {
    this.userId = this.userService.userRef().id;
    this.businessId = this.data['businessId']
    this.listingId = this.data['listingId']
    this.listingService.getListingById(this.listingId).subscribe((data: Listing) => {
      this.categoryService.getCategoryById(data.category.id).subscribe((data: Category) => {
        this.categoryId = data['id'];
        this.inclusionForm.value.category.id = this.categoryId
        this.exclusionForm.value.category.id = this.categoryId
        this.pushInIncludeExcludeArray();
      });
    })
  }

  public pushInIncludeExcludeArray(): void {
    this.includeExcludeService.getIncludeAndExclude(this.categoryId).subscribe((response) => {
      this.includeCheckListArray = response;
      this.excludeCheckListArray = response;
      this.filterArray = response
    })
  }

  public initialize() {
    this.pushInIncludeExcludeArray();
  }

  public inclusionFormBuilder() {
    this.inclusionForm = this.fb.group({
      title: '',
      status: 'AWAITING_APPROVAL',
      user: this.fb.group({
        id: this.userId
      }),
      category: this.fb.group({
        id: this.categoryId
      })
    });
  }

  public exclusionFormBuilder(): void {
    this.exclusionForm = this.fb.group({
      title: '',
      status: 'AWAITING_APPROVAL',
      user: this.fb.group({
        id: this.userId
      }),
      category: this.fb.group({
        id: this.categoryId
      })
    })
  }

  public setIncludeExclude(): void {
    this.includeArray = this.data['include']
    this.excludeArray = this.data['exclude']
  }

  public popFromListArray(i, name, id): void {
    console.log(id)
    if (name === 'include') {
      this.listingIncExcService.deleteIncExc(id).subscribe(data => {
        this.includeArray.splice(i, 1);
        this.toasterMessageService.success(this.vcr, "Inclusion Deleted !!!", "SUCCESS")
      })
    } else {
      this.listingIncExcService.deleteIncExc(id).subscribe(data => {
        this.excludeArray.splice(i, 1);
        this.toasterMessageService.success(this.vcr, "Exclusion Deleted !!!", "SUCCESS")
      });
    }
  }

  public pushInIncExcArray(name, event): void {
    if (name === 'include') {
      this.listingIncExcService.saveIncludeAndExclude(this.listingIncludeExcludeJson(event.value, "INCLUDE")).subscribe(data => {
        this.includeArray.push(event.value)
        this.inclusionForm.reset();
        this.inclusionFormBuilder()
      })
    } else {
      this.listingIncExcService.saveIncludeAndExclude(this.listingIncludeExcludeJson(event.value, "EXCLUDE")).subscribe(data => {
        this.excludeArray.push(event.value)
        this.exclusionForm.reset();
        this.exclusionFormBuilder()
      });
    }
  }

  public includeFilterAutoComplete(): void {
    let array = [];
    let option = this.includeFilterOption;
    let currentLength: number = this.includeFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.includeCheckListArray = array
    } else {
      this.pushInIncludeExcludeArray();
    }
  }

  public excludeFilterAutoComplete(): void {
    let array = [];
    let option = this.excludeFilterOption;
    let currentLength: number = this.excludeFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.excludeCheckListArray = array
    } else {
      this.pushInIncludeExcludeArray();
    }
  }

  public listingIncludeExcludeJson(data, type): Object {
    return {
      "user": {
        "id": this.userService.userRef().id,
      },
      "businessDirectory": {
        "id": this.businessId,
      },
      "listing": {
        "id": this.listingId,
      },
      "includeExclude": {
        "id": data['id'],
      },
      "type": type
    }
  }

  public addInIncludeExclude(name): void {
    let incExc = {
      "id": "",
      title: ""
    };
    if (name == 'include') {
      this.includeExcludeService.addIncludeExclude(this.inclusionForm.value).subscribe((data) => {
        incExc['title'] = data['title']
        let listingIncludeExclude = this.listingIncludeExcludeJson(data, "INCLUDE")
        this.listingIncExcService.saveIncludeAndExclude(listingIncludeExclude).subscribe(data => {
          incExc['id'] = data['id']
          this.toasterMessageService.success(this.vcr, "Listing Include/Exclude Saved !!!", "Success")
          this.includeArray.push(incExc)
          this.inclusionForm.reset();
          this.inclusionFormBuilder()
        })
      })
    }
    else {
      this.includeExcludeService.addIncludeExclude(this.exclusionForm.value).subscribe((data) => {
        incExc['title'] = data['title']
        let listingIncludeExclude = this.listingIncludeExcludeJson(data, "EXCLUDE")
        this.listingIncExcService.saveIncludeAndExclude(listingIncludeExclude).subscribe(data => {
          incExc['id'] = data['id']
          this.toasterMessageService.success(this.vcr, "Listing Include/Exclude Saved !!!", "Success")
          this.excludeArray.push(incExc)
          this.exclusionForm.reset();
          this.exclusionFormBuilder()
        });
      });
    }
  }

  public onClick() {
    this.dialogRef.close();
  }
}
