import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InclusionAddComponent } from './inclusion-add.component';

describe('InclusionAddComponent', () => {
  let component: InclusionAddComponent;
  let fixture: ComponentFixture<InclusionAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InclusionAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InclusionAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
