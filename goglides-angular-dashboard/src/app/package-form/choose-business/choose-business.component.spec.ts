import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseBusinessComponent } from './choose-business.component';

describe('ChooseBusinessComponent', () => {
  let component: ChooseBusinessComponent;
  let fixture: ComponentFixture<ChooseBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
