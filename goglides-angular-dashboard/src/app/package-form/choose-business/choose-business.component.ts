import { BusinessDirectory } from '../../model/business-directory';
import { BusinessDirService } from '../../service/business-dir.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
export interface option {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-choose-business',
  templateUrl: './choose-business.component.html',
  styleUrls: ['./choose-business.component.scss']
})
export class ChooseBusinessComponent implements OnInit {
  business:BusinessDirectory[]
  constructor(
    private businessDirService :BusinessDirService,
     private router: Router,
     public businessDirectoryModel: CommonService) { }

  ngOnInit() {
    this.initBusiness()
  }
  initBusiness(){
    this.businessDirService.getAllBusiness().subscribe((data:BusinessDirectory[])=>{
      this.business=data
      // console.log(data)
    })

  }
  selectBusiness(value){
    let busId = value.value
    console.log(busId);
    this.businessDirectoryModel.setBusinessDirectoryId(busId);
    this.router.navigate(['packages/general/',busId])
    // console.log(value.value)
  }

}
