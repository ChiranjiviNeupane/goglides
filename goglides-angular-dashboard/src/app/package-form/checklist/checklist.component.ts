import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { CommonService } from '../../service/common.service';
import { ListingChecklistService } from '../../service/listing-checklist.service';
import { CheckListService } from '../../service/check-list.service';
import { ListingCheckList } from '../../model/listing-checklist';
import { UserService } from '../../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { ListingService } from '../../service/listing.service';
import { CategoryService } from '../../service/category.service';
import { Listing } from '../../model/listing';


@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.scss']
})
export class ChecklistComponent implements OnInit {


  public check: Boolean = true;
  public provideCheckListForm: FormGroup;
  public bringCheckListForm: FormGroup;
  public provideList = []
  public bringList = []
  public provideCheckListArray: any
  public bringCheckListArray: any
  public filterArray: any
  public businessId: number
  public listingId: number
  public provideStatus: boolean = false
  public bringStatus: boolean = false
  public provideFilterOption: string;
  public bringFilterOption: string;
  public userId: number
  public categoryId: number;

  constructor(
    private fb: FormBuilder,
    public commonService: CommonService,
    public listingCheckListService: ListingChecklistService,
    public checkListService: CheckListService,
    public userService: UserService,
    public activeRoute: ActivatedRoute,
    public toasterMessageServce: ToasterMessageService,
    public vcr: ViewContainerRef,
    public activateRouter: ActivatedRoute,
    public listingService: ListingService,
    public categoryService: CategoryService,
    public toasterMessageService: ToasterMessageService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getIds()
    this.bringListFormBuilder();
    this.provideListFormBuilder();
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = parseInt(this.activateRouter.snapshot.params['businessId']);
    this.listingId = parseInt(this.activateRouter.snapshot.params['listingId']);
    this.listingService.getListingById(this.listingId).subscribe((data: Listing) => {
      this.categoryService.getCategoryById(data.category.id).subscribe((data) => {
        this.categoryId = data['id'];
        this.provideCheckListForm.value.category.id = this.categoryId
        this.bringCheckListForm.value.category.id = this.categoryId
        this.commonService.setCategoryId(this.categoryId);
        this.pushCheckListAutoCompleteArray();
      });
    })
  }

  bringListFormBuilder() {
    this.bringCheckListForm = this.fb.group({
      title: '',
      status: '',
      user: this.fb.group({
        id: [this.userId]
      }),
      category: this.fb.group({
        id: [this.categoryId]
      })
    })
  }

  provideListFormBuilder() {
    this.provideCheckListForm = this.fb.group({
      title: '',
      status: '',
      user: this.fb.group({
        id: [this.userId]
      }),
      category: this.fb.group({
        id: [this.categoryId]
      })
    })
  }

  pushCheckListAutoCompleteArray() {
    this.listingCheckListService.getProvideAndBring(this.categoryId).subscribe((response) => {
      this.provideCheckListArray = response;
      this.bringCheckListArray = response;
      this.filterArray = response
    })
  }

  pushInListArray(name, event) {
    if (name === 'provide') {
      this.provideList.push(event.value)
      this.provideCheckListForm.reset();
      this.provideListFormBuilder()
    } else {
      this.bringList.push(event.value)
      this.bringCheckListForm.reset();
      this.bringListFormBuilder()
    }
  }

  addInCheckList(name, event) {
    if (name == 'provide') {
      this.checkListService.addCheckList(this.provideCheckListForm.value).subscribe((data) => {
        this.toasterMessageServce.success(this.vcr, "Checklist Saved !!!","Success")
        this.provideList.push(data);
        this.provideCheckListForm.reset();
        this.provideListFormBuilder();
      })
    } else {
      this.checkListService.addCheckList(this.bringCheckListForm.value).subscribe((data) => {
        this.toasterMessageServce.success(this.vcr, "Checklist Saved !!!","Success")
        this.bringList.push(data);
        this.bringCheckListForm.reset();
        this.bringListFormBuilder();
      });
    }
  }

  popFromListArray(i: number, name, event) {
    if (name == 'provide') {
      this.provideList.splice(i, 1);
    } else {
      this.bringList.splice(i, 1);
    }
  }

  listingCheckListJson(data, type) {
    return {
      "user": {
        "id": this.userService.userRef().id,
      },
      "businessDirectory": {
        "id": this.businessId,
      },
      "listing": {
        "id": this.listingId,
      },
      "checkList": {
        "id": data['id'],
      },
      "type": type
    }
  }

  saveData(): void {
    let provideCounter: number =0;
    let bringCounter: number =0;
    for (let data of this.provideList) {
      let listingCheckList = this.listingCheckListJson(data, "PROVIDE")
      this.listingCheckListService.saveCheckList(listingCheckList).subscribe(data => {
        provideCounter++
          if(provideCounter === this.provideList.length){
            this.toasterMessageService.success(this.vcr, "Listing Provide Saved !!!","Success")
            this.provideList = []
          }
      }, (error) => {
        console.log(error)
        this.toasterMessageService.error(this.vcr, error.message+" !!!","Success")
        return false
      })
    }

    for (let data of this.bringList) {
      let listingCheckList = this.listingCheckListJson(data, "BRING")
      this.listingCheckListService.saveCheckList(listingCheckList).subscribe(data => {
        bringCounter++
        if(bringCounter === this.bringList.length){
          this.toasterMessageService.success(this.vcr, "Listing Bring Saved !!!","Success")
            this.bringList = []
        }
      }, (error) => {
        console.log(error)
        this.toasterMessageService.error(this.vcr, error.message+" !!!","Success")
        return false
      })
    }
    setTimeout(() => {
      if(!this.provideList.length && !this.bringList.length)    {
        this.redirectTo();
      }
    }, 5000);
  }

  public redirectTo(){
    this.router.navigate(['/packages/destination/',this.businessId, this.listingId]);
  }

  provideFilterAutoComplete() {
    let array = [];
    let option = this.provideFilterOption;
    let currentLength: number = this.provideFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.provideCheckListArray = array
    } else {
      this.pushCheckListAutoCompleteArray();
    }
  }

  bringFilterAutoComplete() {
    let array = [];
    let option = this.bringFilterOption;
    let currentLength: number = this.bringFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.bringCheckListArray = array
    } else {
      this.pushCheckListAutoCompleteArray();
    }
  }
}
