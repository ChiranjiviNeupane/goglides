import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecklistUpdateComponent } from './checklist-update.component';

describe('ChecklistUpdateComponent', () => {
  let component: ChecklistUpdateComponent;
  let fixture: ComponentFixture<ChecklistUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecklistUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecklistUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
