import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { CategoryService } from '../../../service/category.service';
import { ListingService } from '../../../service/listing.service';
import { UserService } from '../../../service/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ListingChecklistService } from '../../../service/listing-checklist.service';
import { Listing } from '../../../model/listing';
import { CheckListService } from '../../../service/check-list.service';
import { ListingCheckList } from '../../../model/listing-checklist';

@Component({
  selector: 'app-checklist-update',
  templateUrl: './checklist-update.component.html',
  styleUrls: ['./checklist-update.component.scss']
})
export class ChecklistUpdateComponent implements OnInit {

  public userId: number
  public businessId: number
  public listingId: number
  public categoryId: number

  public provideList = []
  public bringList = []

  public provideCheckListForm: FormGroup;
  public bringCheckListForm: FormGroup;

  public provideCheckListArray: any
  public bringCheckListArray: any
  public filterArray: any

  public provideFilterOption: string;
  public bringFilterOption: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<ChecklistUpdateComponent>,
    private fb: FormBuilder,
    private userService: UserService,
    private listingService: ListingService,
    private categoryService: CategoryService,
    public vcr: ViewContainerRef,
    private toasterMessageService: ToasterMessageService,
    private listingCheckListService: ListingChecklistService,
    private checkListService: CheckListService
  ) { console.log(data)}

  ngOnInit() {
    this.getIds()
    this.bringListFormBuilder();
    this.provideListFormBuilder();
    this.setCheckList();
    
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = this.data['listingId']
    this.listingService.getListingById(this.listingId).subscribe((data: Listing) => {
      this.categoryService.getCategoryById(data.category.id).subscribe((data) => {
        this.categoryId = data['id'];
        this.provideCheckListForm.value.category.id = this.categoryId
        this.bringCheckListForm.value.category.id = this.categoryId
        this.pushCheckListAutoCompleteArray();
      });
    })
  }

  bringListFormBuilder() {
    this.bringCheckListForm = this.fb.group({
      title: ['', Validators.required],
      status: '',
      user: this.fb.group({
        id: [this.userId]
      }),
      category: this.fb.group({
        id: [this.categoryId]
      })
    })
  }

  provideListFormBuilder() {
    this.provideCheckListForm = this.fb.group({
      title: ['', Validators.required],
      status: '',
      user: this.fb.group({
        id: [this.userId]
      }),
      category: this.fb.group({
        id: [this.categoryId]
      })
    })
  }

  pushCheckListAutoCompleteArray() {
    this.listingCheckListService.getProvideAndBring(this.categoryId).subscribe((response) => {
      this.provideCheckListArray = response;
      this.bringCheckListArray = response;
      this.filterArray = response
    })
  }

  public setCheckList(): void{
    this.provideList = this.data['provide']
    this.bringList = this.data['bring']
  }

  pushInListArray(name, event) {
    if (name === 'provide') {
      this.listingCheckListService.saveCheckList(this.listingCheckListJson(event.value, "PROVIDE")).subscribe(data =>{
        this.provideList.push(event.value)
        this.provideCheckListForm.reset();
        this.provideListFormBuilder()
      })
    } else {
      this.listingCheckListService.saveCheckList(this.listingCheckListJson(event.value, "BRING")).subscribe(data =>{
        this.bringList.push(event.value)
        this.bringCheckListForm.reset();
        this.bringListFormBuilder()
      })
    }
  }

  popFromListArray(i: number, name, id) {
    if (name == 'provide') {
      this.listingCheckListService.deleteCheckList(id).subscribe(data=>{
        this.provideList.splice(i, 1);
        this.toasterMessageService.success(this.vcr, "Checklist Deleted !!!", "SUCCESS")
      })
      
    } else {
      this.listingCheckListService.deleteCheckList(id).subscribe(data=>{
        this.bringList.splice(i, 1);
        this.toasterMessageService.success(this.vcr, "Checklist Deleted !!!", "SUCCESS")
      })
    }
  }

  provideFilterAutoComplete() {
    let array = [];
    let option = this.provideFilterOption;
    let currentLength: number = this.provideFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.provideCheckListArray = array
    } else {
      this.pushCheckListAutoCompleteArray();
    }
  }

  bringFilterAutoComplete() {
    let array = [];
    let option = this.bringFilterOption;
    let currentLength: number = this.bringFilterOption.length;
    if (currentLength !== 0) {
      for (let data of this.filterArray) {
        if (data['title'].toLowerCase().includes(option)) {
          array.push(data);
        }
      }
      this.bringCheckListArray = array
    } else {
      this.pushCheckListAutoCompleteArray();
    }
  }

  listingCheckListJson(data, type) {
    return {
      "user": {
        "id": this.userService.userRef().id,
      },
      "businessDirectory": {
        "id": this.businessId,
      },
      "listing": {
        "id": this.listingId,
      },
      "checkList": {
        "id": data['id'],
      },
      "type": type
    }
  }


  public addInCheckList(name, value): void{
    let checkList = {
      "id": "",
      'title': ""
    };
    if (name == 'provide') { 
      this.checkListService.addCheckList(value).subscribe((data) => {
        checkList['title'] = data['title'] 
        let listingCheckList = this.listingCheckListJson(data, "PROVIDE")
        this.listingCheckListService.saveCheckList(listingCheckList).subscribe(data=>{
          checkList['id'] = data['id']
          this.toasterMessageService.success(this.vcr, "Listing Provide/Bring Saved !!!","Success")
          this.provideList.push(checkList)
          this.provideCheckListForm.reset();
          this.provideListFormBuilder()
        })
      })
    } 
    else {
      this.checkListService.addCheckList(value).subscribe((data) => {
        checkList['title'] = data['title'] 
        let listingCheckList = this.listingCheckListJson(data, "BRING")
        this.listingCheckListService.saveCheckList(listingCheckList).subscribe(data=>{
          checkList['id'] = data['id']
          this.toasterMessageService.success(this.vcr, "Listing Provide/Bring Saved !!!","Success")
          this.bringList.push(checkList)
          this.bringCheckListForm.reset();
          this.bringListFormBuilder()
        });
      });
    }
  }

  public onClick() {
    this.dialogRef.close();
  }

}
