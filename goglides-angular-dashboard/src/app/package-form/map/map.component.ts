import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  locationForm: FormGroup; 

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.locationForm = this.fb.group({
      address: '',
      latitude: '',
      longitude: '',
    })
    
  }

  // locationTitle(): FormGroup {
  //   return this.fb.group({
  //     address: '',
  //     latitude: '',
  //     longitude: '',
  //   });
  // }
  
  // get locationForms() {
  //   return this.locationForm.get('location_info') as FormArray
  // }
  
  // addTitle() {
  //   this.locationForms.push(this.locationTitle());
  // }
  
  // deleteTitle(i) {
  //   this.locationForms.removeAt(i)
  // }

}
