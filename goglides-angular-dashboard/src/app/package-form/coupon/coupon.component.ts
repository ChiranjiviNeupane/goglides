import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.scss']
})
export class CouponComponent implements OnInit {

  couponForm: FormGroup; 

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.couponForm = this.fb.group({
      status: [''],
      type: [''],
      startDate: [''],
      endDate: [''],
      number: ['', Validators.min(1)],
      minimumAmount: [''],
      discount: [''],
      assign: [''],
    })
    
  }

  //status value
  status = [
    {value: '0', viewValue: 'Enable'},
    {value: '1', viewValue: 'Disable'},
  ];

}
