import { CategoryService } from '../../service/category.service';
import { Listing } from '../../model/listing';
import { ListingService } from '../../service/listing.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import { BusinessDirService } from '../../service/business-dir.service';

@Component({
  selector: 'app-package-list',
  templateUrl: './package-list.component.html',
  styleUrls: ['./package-list.component.scss']
})
export class PackageListComponent implements OnInit {
  /*---------------------Data table----------------------------*/
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef
  ELEMENT_DATA = []
  displayedColumns = ['position', 'title', 'category','code', 'business', 'status'];
  dataSource
  /*---------------------Data table----------------------------*/

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  listing:Object
  packagelist=[{}]
   packages = {
     id:'',
     code:'',
     title:'',
     status:'',
     category:'',
     businessId:'',
     businessName:'',
     businessLogo:''
   }

  constructor(private listService:ListingService,
     private router : Router,
     private categoryService:CategoryService,
    private businessService:BusinessDirService) { }

  ngOnInit() {
    let packages ={}
    let packageList = []
    this.listService.getAllListing().subscribe((data=>{
      for (let i in data) {
        this.categoryService.getCategoryById(data[i]['category']['id']).subscribe(cat => {
          this.businessService.getBusinessById(data[i]['businessDirectory']['id']).subscribe(bus=>{
            packages = {
              "id": data[i]['id'],
              "code": data[i]['code'],
              "title": data[i]['title'],
              "status": data[i]['status'],
              "category": cat['title'],
              "businessId": bus['id'],
              "businessName": bus['businessName'],
              "businessLogo": bus['logoUrl'],
              // "business":{
              //   name:bus['businessName'],
              //   logo: bus['logoUrl'],
              // }
             }
             packageList.push(packages);
             this.listService.listingSubject.next(packageList)
          })
        })
      }
      this.listService.listingSubject.subscribe(listData => {
        this.ELEMENT_DATA = listData;
        // console.log(this.ELEMENT_DATA)
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })


      console.log(this.packagelist)
      this.listing=data
      console.log(data)
    }))
  }
  viewPackage(id:number){
    this.router.navigate(['packages/detailview',id])
  }
  viewBusiness(id:number){
    this.router.navigate(['businessdirectory',id])
  }

}

// export interface Element {
//   business: string;
//   position: number;
//   title: string;
//   category: string;
//   code: string;
// }

// const ELEMENT_DATA: Element[] = [
//   {position: 1, business: '', title: 'Fly into The Himalayas', category: 'Paragliding', code: 'GC-101012457154'},
//   {position: 2, business: '', title: 'Kathmandu Paragliding and Hang Gliding Pvt ltd.', category: 'Paragliding', code: 'GC-100000000012'},
// ];
