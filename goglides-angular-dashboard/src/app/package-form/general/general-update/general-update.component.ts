import { ToasterMessageService } from '../../../service/toaster-message.service';
import { ListingService } from '../../../service/listing.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralSettingComponent } from '../../../settings/general-setting/general-setting.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-general-update',
  templateUrl: './general-update.component.html',
  styleUrls: ['./general-update.component.scss']
})
export class GeneralUpdateComponent implements OnInit {

  updateGeneralForm: FormGroup


  onNoClick(): void {
    this.dialogRef.close();
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) private data:any,
    private dialogRef: MatDialogRef<GeneralSettingComponent>,
    private fb: FormBuilder,
    private listingService: ListingService,
    private toasterMessage: ToasterMessageService,
    private vcr: ViewContainerRef,
    
  ) { }

  ngOnInit() {
  this.updateGeneralForm = this.fb.group({
      title: [this.data.listing.title, Validators.required ],
      tripCode: [this.data.listing.tripCode,  ],
      description:  [this.data.listing.description, Validators.required ],
      highlight: [this.data.listing.highlight, Validators.required ],
     
    });
  }


    //editor
    editorConfig = {
      editable: true,
      spellcheck: false,
      height: '10rem',
      minHeight: '5rem',
      // placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
      translate: 'no',
      toolbar: [
        ['undo', 'redo'],
        ['orderedList', 'unorderedList'],
        ['removeFormat'],
      ]
    };
    //editor1
    editorConfig1 = {
      editable: true,
      spellcheck: false,
      height: '10rem',
      minHeight: '5rem',
      // placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
      translate: 'no',
      toolbar: [
        ['undo', 'redo'],
        ['orderedList', 'unorderedList'],
        ['removeFormat'],
      ]
    };
}
