import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralUpdateComponent } from './general-update.component';

describe('GeneralUpdateComponent', () => {
  let component: GeneralUpdateComponent;
  let fixture: ComponentFixture<GeneralUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
