import { UserService } from '../../service/user.service';
import { Listing } from '../../model/listing';
import { ListingService } from '../../service/listing.service';
import { Category } from '../../model/category';
import { CategoryService } from '../../service/category.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { CommonService } from '../../service/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { IncludeExcludeService } from '../../service/include-exclude.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {


  generalForm: FormGroup; 
  public categories = [];
  public listing: Listing
  shows=false
  businessDirectoryId: number


  constructor
  ( private fb: FormBuilder,
    private categoryService :CategoryService,
    private listingService: ListingService,
    private commonService: CommonService,
    private router: Router,
    private userService :UserService,
    private routerActivate :ActivatedRoute,
  public includeExcludeService: IncludeExcludeService) { }

  public saveListingGeneral(data:object){
    console.log(data)
    this.commonService.setCategoryId(data['category'])
   this.listingService.addListingGeneral(data).
   subscribe((data) =>{
     console.log(data)
     this.commonService.setListingId(data['id']);
     this.commonService.setBusinessDirectoryId(this.routerActivate.snapshot.params['id']);
    
    this.router.navigate(['/packages/itinerary',this.businessDirectoryId,data.id]);
    
   });

  }

  //editor
  editorConfig = {
    editable: true,
    spellcheck: false,
    height: '15rem',
    minHeight: '8rem',
    // placeholder: 'Type your tour description here.',
    translate: 'no',
    toolbar: [
      ['undo', 'redo'],
      ['orderedList', 'unorderedList'],
      ['removeFormat'],
    ]
  };
  //editor1
  // editorConfig1 = {
  //   editable: true,
  //   spellcheck: false,
  //   height: '10rem',
  //   minHeight: '6rem',
  //   placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
  //   translate: 'no',
  //   toolbar: [
  //     ['undo', 'redo'],
  //     ['orderedList', 'unorderedList'],
  //   ]
  // };



  ngOnInit() {
    this.businessDirectoryId = this.routerActivate.snapshot.params['id']
    this.generalForm = this.fb.group({
      title: ['', Validators.required ],
      tripCode: ['',  ],
      category:  ['', Validators.required ],
      description:  ['', Validators.required ],
      highlight: ['', Validators.required ],
      user:this.fb.group({
        id:this.userService.userRef().id
      }),
      businessDirectory: this.fb.group({
        id:this.businessDirectoryId
      }),
    });
    // fetch lising category from category table
    this.categoryService.getAllCategory().subscribe(data => this.categories = data)
  }


}
