import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from "ngx-image-gallery";
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { UploadSerivce } from '../../service/file.upload.service';
import { ListingMultimediaService } from '../../service/listing-multimedia.service';
import { CommonService } from '../../service/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ListingService } from '../../service/listing.service';
import { Listing } from '../../model/listing';
import { UserService } from '../../service/user.service';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { stat } from 'fs';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {

  public mediaForm: FormGroup; 
  public fileList: FileList;
  public fileSelect:Boolean = false

  public galleryProgressPercentage: number = 0
  public featureProgressPercentage: number = 0
  public bannerProgressPercentage: number = 0
  public mapProgressPercentage: number = 0

  public fileName: string
  public progressBar: any = 0
  public featureStatus: Boolean = false
  public bannerStatus: Boolean = false
  public mapImageStatus: Boolean = false

  private businessId: number;
  private listingId: number;
  private categoryId: number;
  private userId: number;
  

  public galleryStatus: Boolean = false
  constructor(
    private fb: FormBuilder,
    private fileUploadeService: UploadSerivce,
    private listingMultimediaService: ListingMultimediaService,
    public commonService: CommonService,
    private vcr: ViewContainerRef,
    private activateRouter: ActivatedRoute,
    private userService: UserService,
    private toasterMessageService: ToasterMessageService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getIds();
    this.mediaForm = this.fb.group({
      featured: ['', Validators.required ],
      banner: ['',  ],
      gallery:  ['', Validators.required ],
      video:  ['', Validators.required ],
      map: ['', Validators.required ]
    })
  }

  public getIds(){
    this.userId = this.userService.userRef().id
    this.businessId = this.activateRouter.snapshot.params['businessId'];
    this.listingId = this.activateRouter.snapshot.params['listingId'];
  }

  //image crop
  featureImageChangedEvent: any = [''];
  bannerImageChangedEvent: any = [''];
  croppedImage: any = [''];
  featureCropedImage: any = ['']
  bannerCropedImage: any = ['']

  fileChangeEvent(event: any, name): void {
    if(name === 'feature')
      this.featureImageChangedEvent = event;
    else
    this.bannerImageChangedEvent = event;
    }
  imageCropped(image: string, name) {
    if(name === 'feature')
      this.featureCropedImage = image;
    else
      this.bannerCropedImage = image
  }
  imageLoaded() {
      // show cropper
  }
  loadImageFailed() {
      // show message
  }

  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };
	
  // gallery images
  images: GALLERY_IMAGE[] = [
    {
      url: "https://cdn-s.goglides.com/listing/feature/70726f64756374696f6e/e4da3b7fbbce2345d7772b0674a318d5/0fa203d7-1974-414a-87d3-1f81c8181b31.jpg", 
      // altText: 'woman-in-black-blazer-holding-blue-cup', 
      // title: 'woman-in-black-blazer-holding-blue-cup',
      thumbnailUrl: "https://cdn-s.goglides.com/listing/feature/70726f64756374696f6e/e4da3b7fbbce2345d7772b0674a318d5/0fa203d7-1974-414a-87d3-1f81c8181b31.jpg"
    }
  ];

  openGallery(index: number = 0) {
    this.ngxImageGallery.open(index);
  }
	
  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }
	
  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }
	
  // next image in gallery

	
  /**************************************************/
	
  // EVENTS
  // callback on gallery opened
  galleryOpened(index) {
    console.info('Gallery opened at index ', index);
  }

  // callback on gallery closed
  galleryClosed() {
    console.info('Gallery closed.');
  }

  // callback on gallery image clicked
  galleryImageClicked(index) {
    console.info('Gallery image clicked with index ', index);
  }
  
  // callback on gallery image changed
  galleryImageChanged(index) {
    console.info('Gallery image changed to index ', index);
  }

  // callback on user clicked delete button
  deleteImage(index) {
    console.info('Delete image at index ', index);
  }


  public choosedPhoto(event: any): void{
    this.fileList = event.target.files;
    if(this.fileList !== null){
      this.galleryStatus = true
    }
  }

  public listingMediaGalleyJson(type): Object{
    return {
      "user": {
        "id": this.userId,
      },
      "businessDirectory": {
        "id": this.businessId,
      },
      "listing": {
        "id": this.listingId,
      },
      "status": "PUBLISH",
      "type": type,
      "isApproved":1,
      "fileUrl":""
    }
  }

  public uploadGalleryPhotos(): void{
    if(this.fileList !== null){
      let files: File = [].slice.call(this.fileList);
      this.fileUploadeService.mulipleUpload(files, this.listingMediaGalleyJson("GALLERY"),"Gallery", this.vcr);
    }
  }

  ngDoCheck(){
    this.progressBarValue();
    this.featureProgressBar();
    this.bannerProgressBar();
    this.mapProgressBar();
  }

  public progressBarValue(): void{
    this.listingMultimediaService.galleryProgressValue.subscribe(data=>{
      if(data){
        this.galleryProgressPercentage = Math.round(data['loaded']/data['total'] *100)
        if(this.galleryProgressPercentage !== 100 ) {
          this.progressBar = 1
          this.fileName = data['key']
        }
        else{
          this.progressBar = 2
        }
      }
    })
  }

  public featureProgressBar(){
    this.listingMultimediaService.featureProgressValue.subscribe(data=>{
      this.featureProgressPercentage = Math.round(data['loaded']/data['total'] *100)
    })
  }

  public bannerProgressBar(){
    this.listingMultimediaService.bannerProgressValue.subscribe(data=>{
      this.bannerProgressPercentage = Math.round(data['loaded']/data['total'] *100)
    })
  }

 private dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/jpeg'
    });
  }

  public chooseFeatureImage(event: any): void{
    var myFile: Blob = this.dataURItoBlob(this.featureCropedImage);
    if(myFile !== null){
      this.featureStatus = true
      this.fileUploadeService.uploadFeatureAndBannerImage(myFile,this.listingMediaGalleyJson("FEATURE"), "Feature", this.vcr)
    }
  }

  public chooseBannerImage(event: any): void{
    var myFile: Blob = this.dataURItoBlob(this.bannerCropedImage);
    if(myFile !== null){
      this.bannerStatus = true;
      this.fileUploadeService.uploadFeatureAndBannerImage(myFile,this.listingMediaGalleyJson("BANNER"), "Banner", this.vcr)
    }
  }

  public redirectTo(): void{
      this.router.navigate(['/packages/faq/', this.businessId, this.listingId]);
  }

  public mapImage: any 
  public chooseMap(event){
    this.fileList = event.target.files;
    if(this.fileList !== null){
      this.mapImageStatus = true;
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => this.mapImage = reader.result;
      reader.readAsDataURL(file);
    } 
  }

  public mapUpload(){
    if(this.fileList !== null){
      let files: File = [].slice.call(this.fileList);
      this.fileUploadeService.mulipleUpload(files, this.listingMediaGalleyJson("MAP"),"RouteMap", this.vcr);
    }
  }

  public mapProgressBar(){
    this.listingMultimediaService.mapProgressValue.subscribe(data=>{
      this.mapProgressPercentage = Math.round(data['loaded']/data['total'] *100)
    })
  }

  public saveVideo(event){
    let videoData = this.listingMediaGalleyJson("VIDEO")
    videoData['fileUrl'] = event.value;
    console.log(videoData)
    this.listingMultimediaService.saveMultiMedia(videoData).subscribe(data=>{
      this.toasterMessageService.success(this.vcr, "Video Added !!!", "SUCCESS");
    })
  }
}
