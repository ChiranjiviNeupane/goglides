import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListingMultimediaService } from '../../../service/listing-multimedia.service';
import { UploadSerivce } from '../../../service/file.upload.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-map-update',
  templateUrl: './map-update.component.html',
  styleUrls: ['./map-update.component.scss']
})
export class MapUpdateComponent implements OnInit {

  public userId: number
  public businessId: number
  public listingId: number

  mapImage: any
  mapImageStatus: Boolean = false
  public fileList: FileList;
  public mapProgressPercentage: number = 0
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<MapUpdateComponent>,
    private fileUploadeService: UploadSerivce,
    private listingMultimediaService: ListingMultimediaService,
    private vcr: ViewContainerRef,
    private userService: UserService
  ) {
   }

  ngOnInit() {
    this.getIds();
    this.mapImage = this.data['map']['fileUrl']
  }

  public getIds(){
    this.userId = this.userService.userRef().id
    this.businessId = this.data['listing']
    this.listingId = parseInt(this.data['business'])
  }

  public chooseMap(event){
    this.fileList = event.target.files;
    if(this.fileList !== null){
      this.mapImageStatus = true
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => this.mapImage = reader.result;
      reader.readAsDataURL(file);
    } 
  }

  public listingMediaJson(type): Object{
    return {
      "user": {
        'id': this.userId
      },
      "businessDirectory": {
        "id": this.businessId
      },
      "listing": {
        "id": this.listingId
      },
      "status": "PUBLISH",
      "type": type,
      "isApproved":1,
      "fileUrl":""
    }
  }

  public mapUpload(){
    if(this.fileList !== null){
      let files: File = [].slice.call(this.fileList);
      if(this.data['map'] === null || this.data['map'] === undefined || this.data['map'] === ""){
        this.fileUploadeService.mulipleUpload(files, this.listingMediaJson("MAP"),"Map", this.vcr);
      }else{
        this.fileUploadeService.mulipleUpload(files,this.data['map'],"Map", this.vcr);
      }
    }
  }

  ngDoCheck() {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
   this.mapProgressBar(); 
  }

  public mapProgressBar(){
    this.listingMultimediaService.mapProgressValue.subscribe(data=>{
      this.mapProgressPercentage = Math.round(data['loaded']/data['total'] *100)
      if(this.mapProgressPercentage === 100){
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      }
    })
  }

}
