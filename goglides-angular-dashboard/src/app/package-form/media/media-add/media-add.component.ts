import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { UserService } from '../../../service/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UploadSerivce } from '../../../service/file.upload.service';
import { ListingMultimediaService } from '../../../service/listing-multimedia.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';

@Component({
  selector: 'app-media-add',
  templateUrl: './media-add.component.html',
  styleUrls: ['./media-add.component.scss']
})
export class MediaAddComponent implements OnInit {

  public userId: number
  public businessId: number
  public listingId: number

  public fileList: FileList;
  public fileSelect:Boolean = false

  public galleryStatus: Boolean = false
  public progressBar: any = 0
  public galleryProgressPercentage: number = 0
  public fileName: string
  constructor(
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<MediaAddComponent>,
    private fileUploadeService: UploadSerivce,
    public vcr: ViewContainerRef,
    private listingMultimediaService: ListingMultimediaService,
    private toasterMessageService: ToasterMessageService
  ) {}

  ngOnInit() {
    this.getIds();
  }

  public getIds(){
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = parseInt(this.data['listingId'])
  }

  public listingMediaGalleyJson(type): Object {
    return {
      "user": {
        "id": this.userId,
      },
      "businessDirectory": {
        "id": this.businessId,
      },
      "listing": {
        "id": this.listingId,
      },
      "status": "PUBLISH",
      "type": type,
      "isApproved": 1,
      "fileUrl": ""
    }
  }

  public choosedPhoto(event: any): void{
    this.fileList = event.target.files;
    if(this.fileList !== null){
      this.galleryStatus = true
    }
  }

  public uploadGalleryPhotos(): void{
    if(this.fileList !== null){
      let files: File = [].slice.call(this.fileList);
      this.fileUploadeService.mulipleUpload(files, this.listingMediaGalleyJson("GALLERY"),"Gallery", this.vcr);
    }
  }

  ngDoCheck(){
    this.progressBarValue();
  }

  public progressBarValue(): void{
    this.listingMultimediaService.galleryProgressValue.subscribe(data=>{
      if(data){
        this.galleryProgressPercentage = Math.round(data['loaded']/data['total'] *100)
        if(this.galleryProgressPercentage !== 100 ) {
          this.progressBar = 1
          this.fileName = data['key']
        }
        else{
          this.progressBar = 2
        }
      }
    })
  }

  addVideo(event){
    let videoData = this.listingMediaGalleyJson("VIDEO")
    videoData['fileUrl'] = event.value;
    this.listingMultimediaService.saveMultiMedia(videoData).subscribe(data=>{
      this.toasterMessageService.success(this.vcr, "Video Added !!!", "SUCCESS");
    })
  }

  public onClick(): void{
    this.dialogRef.close();
  }
  
}
