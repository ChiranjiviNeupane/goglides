import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListingMultimediaService } from '../../../service/listing-multimedia.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { CommonService } from '../../../service/common.service';

@Component({
  selector: 'app-media-delete',
  templateUrl: './media-delete.component.html',
  styleUrls: ['./media-delete.component.scss']
})
export class MediaDeleteComponent implements OnInit {
  public status: Boolean = false
  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<MediaDeleteComponent>,
    private listingMediaService: ListingMultimediaService,
    private toasterMessageService: ToasterMessageService,
    private vcr: ViewContainerRef,
    private commonService: CommonService
  ) {}

  ngOnInit() {
  }

  public confirmDelete(): void{
    this.listingMediaService.deleteMedia(this.data['id']).subscribe(data=>{
        this.commonService.dialogStatus.next(true);
        // setTimeout(() => {
        //   this.onNoClick();
        // }, 2000);
    },(error)=>{
      this.commonService.dialogStatus.next(false);
      this.toasterMessageService.error(this.vcr, error.error.message + " !!!", "ERROR")
    })
  }

  public onNoClick(): void{
    this.dialogRef.close();
  }

}
