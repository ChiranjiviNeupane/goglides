import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListingTransportRouteService } from '../../../service/listing-transport-route.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-pickup-dropoff-add',
  templateUrl: './pickup-dropoff-add.component.html',
  styleUrls: ['./pickup-dropoff-add.component.scss']
})
export class PickupDropoffAddComponent implements OnInit {
  locationForm: FormGroup;
  public pdArray = []
  public userId: number
  public businessId: number
  public listingId: number
  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<PickupDropoffAddComponent>,
    private lisitngRouteService: ListingTransportRouteService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getIds()
    this.locationForm = this.fb.group({

      location_detail: this.fb.array([
        this.locations()
      ]),
    })
  }

  public getIds() {
    this.userId = this.userService.userRef().id
    this.businessId = this.data['businessId']
    this.listingId = parseInt(this.data['listingId'])
  }

  //pickup/dropoff
  locations(): FormGroup {
    return this.fb.group({
      address: [''],
      pickUp: [],
      dropOff: [],
      listing: this.fb.group({
        id: [this.listingId]
      }),
      user: this.fb.group({
        id: [this.userId]
      }),
      businessDirectory: this.fb.group({
        id: [this.businessId]
      }),
    });
  }

  get locationDetailForms() {
    return this.locationForm.get('location_detail') as FormArray
  }

  addLocation() {
    this.locationDetailForms.push(this.locations());

  }

  deleteLocation(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.locationDetailForms.removeAt(i)
    }
  }

  public popFromPd(i) {
    this.pdArray.splice(i, 1);
  }

  public saveData(value): any {
   
  }

}
