import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupDropoffAddComponent } from './pickup-dropoff-add.component';

describe('PickupDropoffAddComponent', () => {
  let component: PickupDropoffAddComponent;
  let fixture: ComponentFixture<PickupDropoffAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickupDropoffAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupDropoffAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
