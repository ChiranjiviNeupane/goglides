import { ListingTransportRouteService } from '../../service/listing-transport-route.service';
import { ListingFactService } from '../../service/listing-fact.service';
import { BusinessDirectory } from '../../model/business-directory';
import { UserService } from '../../service/user.service';
import { Listing } from '../../model/listing';
import { CategoryService } from '../../service/category.service';
import { ListingService } from '../../service/listing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { max } from 'rxjs/operator/max';
import { ToasterMessageService } from '../../service/toaster-message.service';

@Component({
  selector: 'app-facts',
  templateUrl: './facts.component.html',
  styleUrls: ['./facts.component.scss']
})
export class FactsComponent implements OnInit {

  businessId: number;
  listingId: number;
  category: any;
  listing: any;
  pickup: number;
  dropOff: number;
  //standard
  stars = [
    {value: '1', viewValue: '1 Star'},
    {value: '2', viewValue: '2 Star'},
    {value: '3', viewValue: '3 Star'},
    {value: '4', viewValue: '4 Star'},
    {value: '5', viewValue: '5 Star'}
  ];

  grade = [
    {value: '1', viewValue: '2 Star'},
    {value: '2', viewValue: '3 Star'},
    {value: '3', viewValue: '4 Star'},
    {value: '4', viewValue: '5 Star'}
  ];

  //formarray
  locationForm: FormGroup;
  factsForm: FormGroup;

  listingTransport(address,pickup,dropoff){
    return {
      "address":address,
      "pickUp":pickup,
      "dropOff":dropoff,
      "user":{
        "id": this.userService.userRef().id
      },
      "businessDirectory": {
        "id": this.businessId
      },
      "listing": {
        "id": this.listingId
      }
    }
  }

  constructor(private fb: FormBuilder,
  private activateRouter: ActivatedRoute,
  private listingService: ListingService,
  private categoryService: CategoryService,
  private userService: UserService,
  private listingFactService: ListingFactService,
  private listingTransportRouteService: ListingTransportRouteService,
  private toasterMessageService: ToasterMessageService,
  private vcr: ViewContainerRef,
  private router: Router,
  ) { }


  public savePackageFacts(facts:any, location){
    for(let data of this.locationForm.value.location_detail){
      if(data['address']!==""){  
        let pick=( data['pickup'] === false || data['pickup'] === "" ) ? 0 : 1
        let drop=( data['dropoff'] === false || data['dropoff'] === "") ? 0 : 1
        this.listingTransportRouteService.addListingTransportRoute(this.listingTransport(data['address'],pick,drop)).subscribe((data)=> {
          
        },(error)=> {
          this.toasterMessageService.error(this.vcr,"Uff Something is wrong in Location","ERROR");
        });
      }
  
    }
    this.listingFactService.addListingFact(facts).subscribe((data)=> {
      // console.log(data)
      this.toasterMessageService.success(this.vcr,"Sucessfully Added ","SUCCESS")
      this.router.navigate(['/packages/inclusion',this.businessId, this.listingId]);
      },(error)=> {
        console.log(error)
        this.toasterMessageService.error(this.vcr,error.error.message,"ERROR");
      });
  }

  //editor
  editorConfig = {
    editable: true,
    spellcheck: false,
    height: '15rem',
    width:'100%',
    minHeight: '8rem',
    translate: 'no',
    toolbar: [
      ['undo', 'redo'],
      ['orderedList', 'unorderedList'],
      ['removeFormat'],
    ]
  };

  ngOnInit() {
    
    this.businessId = this.activateRouter.snapshot.params['businessId'];
    this.listingId = this.activateRouter.snapshot.params['listingId'];

    this.listingService.getListingById(this.listingId).subscribe((data:Listing)=>{
      this.listing=data;
     this.categoryService.getCategoryById(data.category.id).subscribe((data) => {
       console.log(data)
      this.category=data['code'];
      });
    })
    

    this.locationForm = this.fb.group({

      location_detail: this.fb.array([
        this.locations()
      ]),
    })

    this.factsForm = this.fb.group({
      daysCount: ['1',[Validators.required, Validators.min(1)]],
      nightCount: ['0',[ Validators.min(0)]],
      minAge: ['0', [Validators.required,Validators.max(100), Validators.min(1)]],
      maxAge: ['0', [Validators.max(100), Validators.min(1)]],
      minWeight: ['0', [Validators.required,Validators.max(100), Validators.min(1)]],
      maxWeight: ['0', [Validators.required,Validators.max(100), Validators.min(1)]],
      minPax: ['0', [Validators.required,Validators.min(1)]],
      maxPax: ['0', [Validators.required,Validators.min(1)]],
      availableCapacity: ['',[Validators.required, Validators.min(1)]],
      tripStandard: ['0'],
      startElevation: ['0'],
      lowestElevation: ['0'],
      highestElevation: ['0'],
      startPoint: [''],
      endPoint: [''],
      bestSeason: [''],
      tripGrade: ['0'],
      bridgeHeight: ['0'],
      riverDistance: ['0'],
      healthCondition:[''],
      user: this.fb.group({
        id: [this.userService.userRef().id,Validators.required]
      }),
      businessDirectory: this.fb.group({
        id:[this.businessId, Validators.required]
      }),
      listing: this.fb.group({
        id: [this.listingId, Validators.required]
      })
    })

  }
  
  //pickup/dropoff
  locations(): FormGroup {
    return this.fb.group({
      address: [''],
      pickup: [''],
      dropoff: [''],
    });
  }
  
  get locationDetailForms() {
    return this.locationForm.get('location_detail') as FormArray
  }
  
  addLocation() {
    this.locationDetailForms.push(this.locations());
  }
  
  deleteLocation(i) {
    const check = confirm("Are you sure to delete?")
    if(check){
    this.locationDetailForms.removeAt(i)
    }
  }

}
