import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupDropoffUpdateComponent } from './pickup-dropoff-update.component';

describe('PickupDropoffUpdateComponent', () => {
  let component: PickupDropoffUpdateComponent;
  let fixture: ComponentFixture<PickupDropoffUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickupDropoffUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupDropoffUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
