import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListingTransportRouteService } from '../../../service/listing-transport-route.service';

@Component({
  selector: 'app-pickup-dropoff-update',
  templateUrl: './pickup-dropoff-update.component.html',
  styleUrls: ['./pickup-dropoff-update.component.scss']
})
export class PickupDropoffUpdateComponent implements OnInit {

  locationForm: FormGroup;
  
  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<PickupDropoffUpdateComponent>,
    private listingRouteService: ListingTransportRouteService
  ) {console.log(data) }

  ngOnInit() {
    this.locationForm = this.fb.group({
      location_detail: this.fb.array([
        this.locations()
      ]),
    })
  }
  
  locations(): FormGroup {
    return this.fb.group({
      address: [this.data['route']['address']],
      pickUp: [(this.data['route']['pickUp']===1)?true:false],
      dropOff: [(this.data['route']['dropOff']===1)?true:false],
    });
  }
  
  get locationDetailForms() {
    return this.locationForm.get('location_detail') as FormArray
  }
  
  addLocation() {
    this.locationDetailForms.push(this.locations());
  }
  
  deleteLocation(i) {
    const check = confirm("Are you sure to delete?")
    if(check){
    this.locationDetailForms.removeAt(i)
    }
  }

  public onClick(){
    this.dialogRef.close();
  }
}
