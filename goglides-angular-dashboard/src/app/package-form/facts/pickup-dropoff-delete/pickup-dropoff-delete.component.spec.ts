import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupDropoffDeleteComponent } from './pickup-dropoff-delete.component';

describe('PickupDropoffDeleteComponent', () => {
  let component: PickupDropoffDeleteComponent;
  let fixture: ComponentFixture<PickupDropoffDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickupDropoffDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupDropoffDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
