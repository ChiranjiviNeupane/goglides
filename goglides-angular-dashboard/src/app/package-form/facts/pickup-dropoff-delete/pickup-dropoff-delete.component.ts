import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListingTransportRouteService } from '../../../service/listing-transport-route.service';

@Component({
  selector: 'app-pickup-dropoff-delete',
  templateUrl: './pickup-dropoff-delete.component.html',
  styleUrls: ['./pickup-dropoff-delete.component.scss']
})
export class PickupDropoffDeleteComponent implements OnInit {

  constructor(

    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<PickupDropoffDeleteComponent>,
    
  ) { }

  ngOnInit() {
  }

  public onClickClose():void{
    this.dialogRef.close()
  }

}
