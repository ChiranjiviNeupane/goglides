import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListingFactService } from '../../../service/listing-fact.service';

@Component({
  selector: 'app-facts-update',
  templateUrl: './facts-update.component.html',
  styleUrls: ['./facts-update.component.scss']
})
export class FactsUpdateComponent implements OnInit {
  category: any;
  factsForm: FormGroup;

  stars = [
    {value: 1, viewValue: '1 Star'},
    {value: 2, viewValue: '2 Star'},
    {value: 3, viewValue: '3 Star'},
    {value: 4, viewValue: '4 Star'},
    {value: 5, viewValue: '5 Star'}
  ];

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private dialogRef: MatDialogRef<FactsUpdateComponent>,
    private listingFactService: ListingFactService
  ) {}

  ngOnInit() {
    this.factFormbuilder();
  }

  factFormbuilder(){
    if(this.data['fact'] === null || this.data['fact'] === undefined){
      this.factsForm = this.fb.group({
        daysCount: ['1',[Validators.required, Validators.min(1)]],
        nightCount: ['0',[ Validators.min(0)]],
        minAge: ['0', [Validators.required,Validators.max(100), Validators.min(1)]],
        maxAge: ['0', [Validators.max(100), Validators.min(1)]],
        minWeight: ['0', [Validators.required,Validators.max(100), Validators.min(1)]],
        maxWeight: ['0', [Validators.required,Validators.max(100), Validators.min(1)]],
        minPax: ['0', [Validators.required,Validators.min(1)]],
        maxPax: ['0', [Validators.required,Validators.min(1)]],
        availableCapacity: ['0',[Validators.required, Validators.min(1)]],
        tripStandard: ['0'],
        startElevation: ['0'],
        lowestElevation: ['0'],
        highestElevation: ['0'],
        startPoint: [''],
        endPoint: [''],
        bestSeason: [''],
        tripGrade: ['0'],
        bridgeHeight: ['0'],
        riverDistance: ['0'],
        healthCondition:[''],
        user: [this.data['listing']['user']],
        businessDirectory: [this.data['listing']['businessDirectory'], Validators.required],
        listing: [this.data['listing']['id'], Validators.required]
      })
    } else{
      this.factsForm = this.fb.group({
        id:[(this.data['fact']['id'] !== null || this.data['fact']['id'] !== undefined)?this.data['fact']['id']:''],
        daysCount: [(this.data['fact']['daysCount'] !== null || this.data['fact']['daysCount'] !== undefined)?this.data['fact']['daysCount']:'1',[Validators.required, Validators.min(1)]],
        nightCount: [(this.data['fact']['nightCount'] !== null || this.data['fact']['nightCount'] !== undefined)?this.data['fact']['nightCount']:'0',[ Validators.min(0)]],
        minAge: [(this.data['fact']['minAge'] || this.data['fact']['minAge'] !== undefined)?this.data['fact']['minAge']:'0', [Validators.required,Validators.max(100), Validators.min(1)]],
        maxAge: [(this.data['fact']['maxAge'] || this.data['fact']['maxAge'] !==undefined)?this.data['fact']['maxAge']:'0', [Validators.max(100), Validators.min(1)]],
        minWeight: [(this.data['fact']['minWeight']  !== null || this.data['fact']['minWeight'] !==undefined)?this.data['fact']['minWeight']:'0', [Validators.required,Validators.max(100), Validators.min(1)]],
        maxWeight: [(this.data['fact']['maxWeight']  !== null || this.data['fact']['maxWeight'] !==undefined)?this.data['fact']['maxWeight']:'0', [Validators.required,Validators.max(100), Validators.min(1)]],
        minPax: [(this.data['fact']['minPax']  !== null || this.data['fact']['minPax'] !==undefined)?this.data['fact']['minPax']:'0', [Validators.required,Validators.min(1)]],
        maxPax: [(this.data['fact']['maxPax']  !== null || this.data['fact']['maxPax'] !==undefined)?this.data['fact']['maxPax']:'0', [Validators.required,Validators.min(1)]],
        availableCapacity: [(this.data['fact']['availableCapacity']  !== null || this.data['fact']['availableCapacity'] !== undefined )?this.data['fact']['availableCapacity']:'0',[Validators.required, Validators.min(1)]],
        tripStandard: [(this.data['fact']['tripStandard']  !== null || this.data['fact']['tripStandard'] !== undefined )?this.data['fact']['tripStandard']:'0'],
        startElevation: [(this.data['fact']['startElevation']  !== null || this.data['fact']['startElevation'] !== undefined )?this.data['fact']['startElevation']:'0'],
        lowestElevation: [(this.data['fact']['lowestElevation']  !== null || this.data['fact']['lowestElevation'] !== undefined )?this.data['fact']['lowestElevation']:'0'],
        highestElevation: [(this.data['fact']['highestElevation']  !== null || this.data['fact']['highestElevation'] !== undefined )?this.data['fact']['highestElevation']:'0'],
        startPoint: [(this.data['fact']['startPoint']  !== null || this.data['fact']['startPoint'] !== undefined )?this.data['fact']['startPoint']:''],
        endPoint: [(this.data['fact']['endPoint']  !== null || this.data['fact']['endPoint']!== undefined  )?this.data['fact']['endPoint']:''],
        bestSeason: [(this.data['fact']['bestSeason']  !== null || this.data['fact']['bestSeason'] !== undefined )?this.data['fact']['bestSeason']:''],
        tripGrade: [(this.data['fact']['tripGrade']  !== null || this.data['fact']['tripGrade'] !== undefined )?this.data['fact']['tripGrade']:'0'],
        bridgeHeight: [(this.data['fact']['bridgeHeight'] !== null  || this.data['fact']['bridgeHeight'] !== undefined )?this.data['fact']['bridgeHeight']:'0'],
        riverDistance: [(this.data['fact']['riverDistance'] || this.data['fact']['riverDistance'] !== undefined )?this.data['fact']['riverDistance']:'0'],
        healthCondition:[(this.data['fact']['healthCondition']  !== null || this.data['fact']['healthCondition'] !== undefined )?this.data['fact']['healthCondition']:''],
      })
    }
  }
 

  public onClose(){
    this.dialogRef.close();
  }


  editorConfig = {
    editable: true,
    spellcheck: false,
    height: '15rem',
    width:'100%',
    minHeight: '8rem',
    translate: 'no',
    toolbar: [
      ['undo', 'redo'],
      ['orderedList', 'unorderedList'],
      ['removeFormat'],
    ]
  };
}
