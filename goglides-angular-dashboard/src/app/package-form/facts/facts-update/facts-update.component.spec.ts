import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactsUpdateComponent } from './facts-update.component';

describe('FactsUpdateComponent', () => {
  let component: FactsUpdateComponent;
  let fixture: ComponentFixture<FactsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
