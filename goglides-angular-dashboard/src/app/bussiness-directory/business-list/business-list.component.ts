import { BusinessDirService } from '../../service/business-dir.service';
import { Http } from '@angular/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-business-list',
  templateUrl: './business-list.component.html',
  styleUrls: ['./business-list.component.scss']
})
export class BusinessListComponent implements OnInit,OnDestroy {
  private getAllBusinessSubs:Subscription
  businessDir:object[]
  constructor(private businesService: BusinessDirService, public router: Router) { }

  ngOnInit() {
   this.getAllBusinessSubs = this.businesService.getAllBusiness().subscribe((data:object[])=>
    this.businessDir=data
  )
  
  }
  view(id:number){
    this.router.navigate(['businessdirectory',id])
  }
  ngOnDestroy(): void {
    this.getAllBusinessSubs.unsubscribe()
  }
 

}
