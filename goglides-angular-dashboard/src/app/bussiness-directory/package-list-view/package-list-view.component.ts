import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {MatTableDataSource, MatPaginator, MatSort} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ListingService } from '../../service/listing.service';
import { CategoryService } from '../../service/category.service';
import { BusinessDirService } from '../../service/business-dir.service';

@Component({
  selector: 'app-package-list-view',
  templateUrl: './package-list-view.component.html',
  styleUrls: ['./package-list-view.component.scss']
})
export class PackageListViewComponent implements OnInit {
  // length
  // pageSize
  // pageSizeOptions
  // pageEvent
  // displayedColumns = ['position', 'name', 'code', 'category', 'action'];
  // dataSource = new MatTableDataSource(ELEMENT_DATA);

  public businessId: number = 0;

  /*---------------------Data table----------------------------*/
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef
  ELEMENT_DATA = []
  displayedColumns = ['position', 'title', 'category','code', 'business', 'status'];
  dataSource
  /*---------------------Data table----------------------------*/

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private listService: ListingService,
    private categoryService: CategoryService,
    private businessService: BusinessDirService
  ) { }

  ngOnInit() {
    this.getIds();
  }

  public getIds(): void{
    this.businessId = this.activatedRoute.snapshot.params['businessId']
    this.getPakages(this.businessId)
  }

  public getPakages(businessId): void{
    let packages ={}
    let packageList = []
    this.listService.getListingByBusinessId(businessId).subscribe((data=>{
      for (let i in data) {
        this.categoryService.getCategoryById(data[i]['category']['id']).subscribe(cat => {
          this.businessService.getBusinessById(data[i]['businessDirectory']['id']).subscribe(bus=>{
            packages = {
              "id": data[i]['id'],
              "code": data[i]['code'],
              "title": data[i]['title'],
              "status": data[i]['status'],
              "category": cat['title'],
              "businessId": bus['id'],
              "businessName": bus['businessName'],
              "businessLogo": bus['logoUrl'],
             }
             packageList.push(packages);
             this.listService.listingSubject.next(packageList)
          })
        })
      }
      this.listService.listingSubject.subscribe(listData => {
        this.ELEMENT_DATA = listData;
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    }))
  }
}
