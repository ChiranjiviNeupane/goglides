import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessPolicyComponent } from './business-policy.component';

describe('BusinessPolicyComponent', () => {
  let component: BusinessPolicyComponent;
  let fixture: ComponentFixture<BusinessPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
