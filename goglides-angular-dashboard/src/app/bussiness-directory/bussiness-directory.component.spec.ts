import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BussinessDirectoryComponent } from './bussiness-directory.component';

describe('BussinessDirectoryComponent', () => {
  let component: BussinessDirectoryComponent;
  let fixture: ComponentFixture<BussinessDirectoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BussinessDirectoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BussinessDirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
