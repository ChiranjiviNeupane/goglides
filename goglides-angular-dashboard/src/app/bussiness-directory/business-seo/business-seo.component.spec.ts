import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessSeoComponent } from './business-seo.component';

describe('BusinessSeoComponent', () => {
  let component: BusinessSeoComponent;
  let fixture: ComponentFixture<BusinessSeoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessSeoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSeoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
