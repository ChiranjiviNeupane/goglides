import { ContactPersonAddComponent } from '../contact-person/contact-person-add/contact-person-add.component';
import { ContactPersonUpdateComponent } from '../contact-person/contact-person-update/contact-person-update.component';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { BusinessLogoUpdateComponent } from '../basic-info/business-logo-update/business-logo-update.component';
import { BusinessDirectoryContactPerson } from '../../model/business-directory-contact-person';
import { BusinessContactPersonService } from '../../service/business-contact-person.service';
import { LocationUpdateComponent } from '../location/location-update/location-update.component';
import { BasicInfoUpdateComponent } from '../basic-info/basic-info-update/basic-info-update.component';
import { MatDialog } from '@angular/material';
import { StatusService } from '../../service/status.service';
import { BusinessDirService } from '../../service/business-dir.service';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessDirectory } from '../../model/business-directory';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from "ngx-image-gallery";

import { VerifiedComponent } from '../certificates/verified/verified.component';
import { BusinessCertificateService } from '../../service/business-certificate.service';
import { businessCertificate } from '../../model/business-certificate';
import { CertificatesComponent } from '../certificates/certificates.component';

export interface option {
  value: string;
  viewValue: string;

}
// export interface DialogData {
//   animal: string;
//   name: string;
// }

@Component({
  selector: 'app-business-view',
  templateUrl: './business-view.component.html',
  styleUrls: ['./business-view.component.scss']
})
export class BusinessViewComponent implements OnInit {
  // animal: string;
  // name: string;

  selected = '1';
  packageCount: number = 0;
  statusList
  certChecked = false
  certPanChecked = false
  certVatChecked = false
  certTpinChecked = false

  public id: number
  public business: BusinessDirectory
  public businessContactPerson: BusinessDirectoryContactPerson[]
  public contactPersons: Object[]
  businessCertificate: businessCertificate
  businessCertificate_Pan: businessCertificate
  businessCertificate_Vat: businessCertificate
  businessCertificate_Tipn: businessCertificate

  constructor(
    public routerActivate: ActivatedRoute,
    public businessService: BusinessDirService,
    private contactInfoService: BusinessContactPersonService,
    private statusService: StatusService,
    private busCertService: BusinessCertificateService,
    public dialog: MatDialog,
    private router: Router,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,


  ) { }

  ngOnInit() {
    this.id = this.routerActivate.snapshot.params['businessId']
    this.businessService.getSingleBusiness(this.id).subscribe((data: BusinessDirectory) => {
      this.business = data
      this.getContactPersonInfo(this.business.id)
      console.log(this.business)
    });
    this.statusList = this.statusService.getStatusList();

    this.getCertificate(this.id)
    console.log(this.statusList)
    this.publishedPakages(this.id)
  }
  addPackage(id){
    this.router.navigate(['packages/general/',id])
  }

  getCertificate(id) {
    this.busCertService.getCertificate(id).subscribe((data) => {
      for (var cert in data) {
        switch (data[cert]['fileType']) {
          case '_certificate':
            this.businessCertificate = data[cert];

            if (this.businessCertificate.isApproved === 0) { this.certChecked = false }
            else { this.certChecked = true }
            // console.log(this.businessCertificate)
            // alert('certificate')
            break;
          case '_pan':
            this.businessCertificate_Pan = data[cert];
            if (this.businessCertificate_Pan.isApproved === 0) { this.certPanChecked = false }
            else { this.certPanChecked = true }
            // console.log(this.businessCertificate_Pan)
            // alert('pan')
            break;
          case '_vat':
            this.businessCertificate_Vat = data[cert];
            if (this.businessCertificate_Vat.isApproved === 0) { this.certVatChecked = false }
            else { this.certVatChecked = true }
            // console.log(this.businessCertificate_Vat)
            // alert('pan')
            break;
          case '_tipn':
            this.businessCertificate_Tipn = data[cert];
            if (this.businessCertificate_Tipn.isApproved === 0) { this.certTpinChecked = false }
            else { this.certTpinChecked = true }
            // console.log(this.businessCertificate_Tipn)
            // alert('pan')
            break;
        }
      }
    })
  }

  busCert(value, id: number) {
    if (value.checked == true) {
      let result = confirm('are you sure?')
      if (result) {
        // alert("confirm" + result + " id " + id)
        let data = { "isApproved": 1 }
        this.busCertService.verifiedCert(this.id, data, id).subscribe((data) => {
          // console.log(data)
        })
      }
    }
    else{
      let result = confirm('are you sure?')
      if (result) {
        // alert("confirm" + result + " id " + id)
        let data = { "isApproved": 0 }
        this.busCertService.verifiedCert(this.id, data, id).subscribe((data) => {
          // console.log(data)
        })
      }

    }
  }



  public updateBusinessDirBasicInfo(businessInfo) {

    const dailogRef = this.dialog.open(BasicInfoUpdateComponent, {
      width: '800px',
      height: '400px',

      data: { businessInfo: businessInfo }
    });

    dailogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.business = this.businessService.getBusinessDirInfo();
      }
    })
  }

  public updateBusinessDirLocationInfo(businessInfo) {

    const dailogRef = this.dialog.open(LocationUpdateComponent, {
      width: '800px',
      height: '400px',

      data: { businessInfo: businessInfo }
    });

    dailogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.business = this.businessService.getBusinessDirInfo();
      }
    })
  }

  public getContactPersonInfo(id) {
    this.contactInfoService.listByBusinessId(id).subscribe((data: Object[]) => {
      this.contactPersons = data
    })
  }


  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };

  // gallery images
  images: GALLERY_IMAGE[] = [
    {
      url: "http://resources.mynewsdesk.com/image/upload/dpr_1.0,f_auto,pg_1,q_auto,w_670/rya9djfb6cvne5hd6bkv.jpg",
      thumbnailUrl: "http://resources.mynewsdesk.com/image/upload/dpr_1.0,f_auto,pg_1,q_auto,w_670/rya9djfb6cvne5hd6bkv.jpg"
    },
    {
      url: "https://isimangaliso.com/wp-content/uploads/2014/09/Background-Information-iSimangaliso-GEF-project-1.jpg",
      thumbnailUrl: "https://isimangaliso.com/wp-content/uploads/2014/09/Background-Information-iSimangaliso-GEF-project-1.jpg"
    },
  ];

  openGallery(index: number = 0) {
    this.ngxImageGallery.open(index);
  }

  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }

  // next image in gallery


  /**************************************************/

  // EVENTS
  // callback on gallery opened
  galleryOpened(index) {
    console.info('Gallery opened at index ', index);
  }

  // callback on gallery closed
  galleryClosed() {
    console.info('Gallery closed.');
  }

  // callback on gallery image clicked
  galleryImageClicked(index) {
    console.info('Gallery image clicked with index ', index);
  }

  // callback on gallery image changed
  galleryImageChanged(index) {
    console.info('Gallery image changed to index ', index);
  }

  // callback on user clicked delete button
  deleteImage(index) {
    console.info('Delete image at index ', index);
  }

  public updateBusinessLogo(businessInfo) {

    const dialogRef = this.dialog.open(BusinessLogoUpdateComponent, {
      data: { business: businessInfo },
      width:"800px",
      // height: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.router.navigate(['businessdirectory', businessInfo.id]);
      }
    });
  }

  public updateBusinessDirStatus(businessId, status){ 
    this.business.status=status;
    this.businessService.updateBusinessDirStatus(businessId,this.business).subscribe((data) => {
      this.toasterMessageService.success(this.vcr, "Sucessfully Updated!!!","SUCCESS");
    });
  }

  public updateBusinessDirContactPerson(businessContactPersonId, businessId){
    const dialogRef = this.dialog.open(ContactPersonUpdateComponent,
    {
      width: '800px',
      height: '400px',

      data: {businessContactPersonId: businessContactPersonId, businessId: businessId}
  
  } );
  dialogRef.afterClosed().subscribe((result) => {
    if(result==1){
      this.contactPersons=[];
        this.toasterMessageService.success(this.vcr, "ContactPerson Updated !", "Success");
        this.getContactPersonInfo(this.business.id);
    }
  })
  }

  public addNewBusinessdirContactPerson(businessId){
    const dialogRef =this.dialog.open(ContactPersonAddComponent,{
      width: '800px',
      height: '400px',
      data: {businessId: businessId}
    }
    );
    dialogRef.afterClosed().subscribe((result) => {
      if(result==1){
      this.toasterMessageService.success(this.vcr, "ContactPerson Added !", "Success");
      }
    })

  }

  public publishedPakages(id){
    this.packageCount = 89;
    this.businessService.countPackages(id).subscribe(data=>{
      console.log(data);
      this.packageCount = data['count']
    })
  }

  enabled: boolean = true
  disabled: boolean = false

  isVerifiedTrue(business){
    business['isVerified'] = 1
    let id = business['id']
    this.businessService.updateBusiness(id, business).subscribe((data:any)=>{
      this.toasterMessageService.success(this.vcr,"SUCCESS", "Business Verified On" )
      this.business['isVerified'] = 1
    })
  }

  isVerifiedFalse(business){
    business['isVerified'] = 0
    let id = business['id']
    this.businessService.updateBusiness(id, business).subscribe((data:any)=>{
      this.toasterMessageService.success(this.vcr,"SUCCESS", "Business Verified Off")
      this.business['isVerified'] = 0
    })
  }

}
