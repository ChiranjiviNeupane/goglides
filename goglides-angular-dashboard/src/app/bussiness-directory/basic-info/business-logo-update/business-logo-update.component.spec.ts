import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessLogoUpdateComponent } from './business-logo-update.component';

describe('BusinessLogoUpdateComponent', () => {
  let component: BusinessLogoUpdateComponent;
  let fixture: ComponentFixture<BusinessLogoUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessLogoUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessLogoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
