import { UserService } from '../../../service/user.service';
import { UploadSerivce } from '../../../service/file.upload.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { BusinessCertificateService } from '../../../service/business-certificate.service';

@Component({
  selector: 'app-business-logo-update',
  templateUrl: './business-logo-update.component.html',
  styleUrls: ['./business-logo-update.component.scss']
})
export class BusinessLogoUpdateComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  businessDirectoryId: number;
  userId:number;
  public logoProgressPercentage: number
    public logoBarStaus: number = 0

  constructor(
    public fileupload: UploadSerivce,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef :MatDialogRef<BusinessLogoUpdateComponent>,
    private userService: UserService,
    public vcr: ViewContainerRef,
    private businessCertificateService: BusinessCertificateService
  
  ) { }

  ngOnInit() {
    this.businessDirectoryId=this.data.business.id
    this.userId=this.userService.userRef().id

  }
  upload() {
    var myFile: Blob = this.dataURItoBlob(this.croppedImage);
    this.fileupload.uploadfile(myFile,this.businessDirectoryId,this.userId, this.vcr);
  }


  //convert base64 to image/type
  dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/png'
    });
  }



  //image crop
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(image: string) {
    this.croppedImage = image;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }

  ngDoCheck(){
    this.progressBarValue()
  }
  public progressBarValue(): void{
    this.businessCertificateService.logoProgressPercentage.subscribe(data=>{
      if(data){
        this.logoProgressPercentage = Math.round(data['loaded']/data['total'] *100)
        if(this.logoProgressPercentage !== 100 ) {
          this.logoBarStaus = 1
        }else{
          setTimeout(() => {
              window.location.reload();
          }, 3000);
        }
      }
    })
  }

}
