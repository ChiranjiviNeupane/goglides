import { CommonService } from '../../service/common.service';
import { BusinessDirService } from '../../service/business-dir.service';
import { UserService } from '../../service/user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { MatFormFieldControl } from '@angular/material';

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent implements OnInit {
  phoneValidation : Boolean = false;
  basicForm: FormGroup;

  public business = [
    { name: 'private' },
    { name: 'public' }
  ]

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService, private businessDirectoryService: BusinessDirService, private commonService: CommonService) { }

  public saveBusinessGeneralInfo(data) {
    this.businessDirectoryService.savaBusinessDirectoryGeneralInfo(data).subscribe((data: any) => {
      console.log(data)
      this.commonService.setBusinessDirectoryId(data.id);
      this.router.navigate(['businessdirectory/contact', data.id]);

    }
    );


  }
  ngOnInit() {
    this.basicForm = this.fb.group({
      businessName: ['', Validators.required],
      registrationNumber: ['',],
      website: [''],
      email: ['', Validators.email],
      phone1: ['+977', Validators.required],
      phone2: ['+977',],
      businessType: ['', Validators.required],
      description: ['',],
      user: this.fb.group({
        id: [this.userService.userRef().id, Validators.required]
      })
    })
    console.log("basic info on init")
    this.getPhoneValidationError();
  }

  //editor
  editorConfig21 = {
    editable: true,
    spellcheck: false,
    height: '15rem',
    minHeight: '8rem',
    // placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
    translate: 'no',
    toolbar: [
      ['undo', 'redo'],
      ['orderedList', 'unorderedList'],
      ['removeFormat'],
    ]
  };

  htmlContent21 = '';


  getPhoneValidationError(){
    if(this.basicForm.value.phone1.length < 10){
      this.phoneValidation = true;
      this.basicForm.controls.phone1.setErrors({'inValid':true})
    }else{
      this.phoneValidation = false;
    }
  }

}

