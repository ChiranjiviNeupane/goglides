import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicInfoUpdateComponent } from './basic-info-update.component';

describe('BasicInfoUpdateComponent', () => {
  let component: BasicInfoUpdateComponent;
  let fixture: ComponentFixture<BasicInfoUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInfoUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
