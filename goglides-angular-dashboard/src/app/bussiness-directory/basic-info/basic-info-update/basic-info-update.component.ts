import { BusinessDirectory } from '../../../model/business-directory';
import { BusinessDirService } from '../../../service/business-dir.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-basic-info-update',
  templateUrl: './basic-info-update.component.html',
  styleUrls: ['./basic-info-update.component.scss']
})
export class BasicInfoUpdateComponent implements OnInit {


  updateBusinessDirBasicInfo: FormGroup
  public phoneValidation: Boolean = true;
  public formValidation: Boolean = true;
  public message: string = "";

  public business = [
    { name: 'private' },
    { name: 'public' }
  ]

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<BasicInfoUpdateComponent>,
    private formBuilder: FormBuilder,
    private businessDirService: BusinessDirService
  ) {
    console.log(data)
   }

  ngOnInit() {
    this.updateBusinessDirBasicInfo = this.formBuilder.group({
      businessName: ['', Validators.required],
      registrationNumber: [''],
      website: ['', Validators.required],
      email: ['', Validators.required],
      phone1: ['+977'],
      phone2: ['+977'],
      businessType: ['', Validators.required],
      description: [this.data.businessInfo.description]
    })
  }

  editorConfig21 = {
    editable: true,
    spellcheck: false,
    height: '10rem',
    minHeight: '5rem',
    translate: 'no',
    toolbar: [
      ['undo', 'redo'],
      ['orderedList', 'unorderedList'],
      ['removeFormat'],
    ]
  };

  public confirmBusinessDirUpdate(businessDirBasicInfo) {
    this.businessDirService.updateBusinessDirBasicInfo(this.data.businessInfo.id, businessDirBasicInfo);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getPhoneValidationError(event) {
    console.log(event.value)
    if (event.value.length < 10) {
      this.phoneValidation = false;
      this.formValidation = false;
      this.message = "Please provide valid number"
    } else {
      this.message = ""
      this.phoneValidation = true;
      this.formValidation = true;
    }
  }

  setFormInvalid() {
    console.log(this.updateBusinessDirBasicInfo.valid);
    // console.log(this.phoneValidation)
    if (this.phoneValidation) {
      this.formValidation = true;
    } if (!this.updateBusinessDirBasicInfo.valid) {
      this.formValidation = false;
    }
  }
}
