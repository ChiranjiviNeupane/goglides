import { BusinessDirectory } from '../../model/business-directory';
import { BusinessDirService } from '../../service/business-dir.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  locationForm: FormGroup; 
  businessDirectoryId: number

  constructor(private fb: FormBuilder, private commonService: CommonService,
    private activeRoute: ActivatedRoute,
    private businessDirectoryService: BusinessDirService,
    private router: Router
  ) { }

  ngOnInit() {
    this.businessDirectoryId= this.activeRoute.snapshot.params['id'];
    this.locationForm = this.fb.group({
      street: ['',],
      city: ['',  ],
      state:  ['', ],
      country:  ['', ]
    })
    console.log("location on init")
    console.log("Business Directory Id "+this.commonService.getBusinessDirectoryId())
    
  }

  saveLocationInfo(businessLocationInfo){
    this.businessDirectoryService.saveBusinessDirectoryLocation(this.businessDirectoryId,businessLocationInfo).subscribe((data: BusinessDirectory) => {
      console.log("sucessfully added businesslocation Info");
      this.router.navigate(['businessdirectory/certificates',this.businessDirectoryId])
    })
  }

}
