import { BusinessDirService } from '../../../service/business-dir.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-location-update',
  templateUrl: './location-update.component.html',
  styleUrls: ['./location-update.component.scss']
})
export class LocationUpdateComponent implements OnInit {

  updateBusinessDirLocation: FormGroup

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private dialogRef: MatDialogRef<LocationUpdateComponent>,
  private businessDirService: BusinessDirService,
  private formBuilder: FormBuilder
) { }

  ngOnInit() {
    this.updateBusinessDirLocation= this.formBuilder.group({
      street:[""],
      city: [""],
      state: [""],
      country: [""],
      longitude: ["0.0"],
      latitude: ["0.01"],

    })
  }
  public confirmBusinessDirUpdate(businessDirBasicInfo){
    this.businessDirService.updateBusinessDirLocationInfo(this.data.businessInfo.id, businessDirBasicInfo); 
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}