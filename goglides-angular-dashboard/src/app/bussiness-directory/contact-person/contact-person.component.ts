import { ActivatedRoute, Router } from '@angular/router';
import { BusinessContactPersonService } from '../../service/business-contact-person.service';
import { BusinessDirService } from '../../service/business-dir.service';
import { UserService } from '../../service/user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { CommonService } from '../../service/common.service';

@Component({
  selector: 'app-contact-person',
  templateUrl: './contact-person.component.html',
  styleUrls: ['./contact-person.component.scss']
})
export class ContactPersonComponent implements OnInit {

  contactForm: FormGroup;
  businessId: number
  public phoneValidation: Boolean = false
  public formValidation: Boolean = false
  public message: string = ""
  
  public saveBusinessContactPersonInfo(data) {
    this.businessContactPersonService.saveBusinessContactPerson(data).subscribe((data: any) => {
      this.router.navigate(['businessdirectory/location', this.businessId]);

    })
  }
  constructor(private fb: FormBuilder,
    private commonService: CommonService,
    private userService: UserService,
    private businessDirectoryService: BusinessDirService,
    private businessContactPersonService: BusinessContactPersonService,
    private activeRouter: ActivatedRoute,
    private router: Router,

  ) { }

  ngOnInit() {

    this.businessId = this.activeRouter.snapshot.params['id'];
    this.contactForm = this.fb.group({
      contact_info: this.fb.array([
        this.Title(),
      ]),
      user: this.fb.group({
        id: [this.userService.userRef().id, Validators.required]
      }),
      businessDirectory: this.fb.group({
        id: [this.activeRouter.snapshot.params['id'], Validators.required]
      }),
    });
  }


  Title(): FormGroup {
    return this.fb.group({
      contactPersonFirstName: ['', Validators.required],
      contactPersonLastName: ['', Validators.required],
      contactPersonEmail: ['', Validators.email],
      contactPersonJobTitle: ['', Validators.required],
      contactPersonPrimaryPhone: ['+977', Validators.required],
      contactPersonSecondaryPhone: ['+977',],
      contactPersonMobile: ['+977',],
    });
  }

  get contactForms() {
    return this.contactForm.get('contact_info') as FormArray
  }

  addTitle() {
    this.contactForms.push(this.Title());
  }

  deleteTitle(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.contactForms.removeAt(i)
    }
  }

  getPhoneValidationError(event) {
    if (event.value.length < 10) {
      this.phoneValidation = false;
      this.formValidation = false;
      this.message = "Please provide valid number"
    } else {
      this.message = ""
      this.phoneValidation = true;
      this.formValidation = true;
    }
  }

  setFormInvalid() {
    if (this.phoneValidation) {
      this.formValidation = true;
    } if (!this.contactForm.valid) {
      this.formValidation = false;
    }
  }
}
