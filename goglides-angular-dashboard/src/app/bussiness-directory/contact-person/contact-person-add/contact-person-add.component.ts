import { BusinessContactPersonService } from '../../../service/business-contact-person.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from '../../../service/user.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-contact-person-add',
  templateUrl: './contact-person-add.component.html',
  styleUrls: ['./contact-person-add.component.scss']
})
export class ContactPersonAddComponent implements OnInit {

  addBusinessContactPeronForm:FormGroup;
  contactForm: FormGroup;
  public phoneValidation: Boolean = false
  public formValidation: Boolean = false
  public message: string = ""

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dailogRef: MatDialogRef<ContactPersonAddComponent>,
    private businessContactPersonService: BusinessContactPersonService,

  ) { }

  ngOnInit() {


    this.contactForm = this.formBuilder.group({
      contact_info: this.formBuilder.array([
        this.Title(),
      ]),
      user: this.formBuilder.group({
        id: [this.userService.userRef().id, Validators.required]
      }),
      businessDirectory: this.formBuilder.group({
        id: [this.data.businessId, Validators.required]
      }),
    });
  }


  Title(): FormGroup {
    return this.formBuilder.group({
      contactPersonFirstName: ['', Validators.required],
      contactPersonLastName: ['', Validators.required],
      contactPersonEmail: ['', Validators.email],
      contactPersonJobTitle: ['', Validators.required],
      contactPersonPrimaryPhone: ['+977', Validators.required],
      contactPersonSecondaryPhone: ['+977',],
      contactPersonMobile: ['+977',],
    });
  }

  get contactForms() {
    return this.contactForm.get('contact_info') as FormArray
  }

 onNoClick(): void {
  this.dailogRef.close();
}

public addNewContactInfo(contactPersonInfo){
 this.businessContactPersonService.addBusinessContactPerson(contactPersonInfo);
}

public addNewContactPerson(data) {
  this.businessContactPersonService.saveBusinessContactPerson(data).subscribe((data: any) => {
    console.log("Sucessfully added ");
  });
}

getPhoneValidationError(event) {
  if (event.value.length < 10) {
    this.phoneValidation = false;
    this.formValidation = false;
    this.message = "Please provide valid number"
  } else {
    this.message = ""
    this.phoneValidation = true;
    this.formValidation = true;
  }
}

setFormInvalid() {
  if (this.phoneValidation) {
    this.formValidation = true;
  } if (!this.contactForm.valid) {
    this.formValidation = false;
  }
}

}
