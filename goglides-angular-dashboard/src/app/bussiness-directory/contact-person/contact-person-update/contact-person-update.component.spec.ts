import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactPersonUpdateComponent } from './contact-person-update.component';

describe('ContactPersonUpdateComponent', () => {
  let component: ContactPersonUpdateComponent;
  let fixture: ComponentFixture<ContactPersonUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactPersonUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
