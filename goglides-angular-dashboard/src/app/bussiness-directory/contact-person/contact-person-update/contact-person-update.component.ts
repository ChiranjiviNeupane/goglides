import { ToasterMessageService } from '../../../service/toaster-message.service';
import { UserService } from '../../../service/user.service';
import { BusinessDirectoryContactPerson } from '../../../model/business-directory-contact-person';
import { BusinessContactPersonService } from '../../../service/business-contact-person.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-contact-person-update',
  templateUrl: './contact-person-update.component.html',
  styleUrls: ['./contact-person-update.component.scss']
})
export class ContactPersonUpdateComponent implements OnInit {

  updateBusinessContactPeronForm: FormGroup
  
  businessId:number;
  public phoneValidation: Boolean = false
  public formValidation: Boolean = false
  public message: string = ""
  
  constructor(private formBuilder: FormBuilder,
  @Inject(MAT_DIALOG_DATA) private data: any ,
  private dialogRef: MatDialogRef<ContactPersonUpdateComponent>,
  private businessContactPersonService: BusinessContactPersonService,
  private userService: UserService,
  private toasterMessageService: ToasterMessageService,
  public vcr: ViewContainerRef,
) { }

  ngOnInit() {

    this.initUpdate("","","","","","","","");
    this.businessId=this.data.businessId
    this.businessContactPersonService.getBusinessContactPerson(this.data.businessContactPersonId).subscribe((data: BusinessDirectoryContactPerson) => {
            
      this.initUpdate(data.contactPersonFirstName, data.contactPersonLastName,data.contactPersonEmail,
      data.contactPersonJobTitle,data.contactPersonPrimaryPhone,data.contactPersonSecondaryPhone,data.contactPersonMobile,this.businessId)
      
    });
  }

  
  initUpdate(firstName,lastName,email,jobTitle,primaryPhone,secondaryPhone,mobile, businesId){
    this.updateBusinessContactPeronForm= this.formBuilder.group({
      contactPersonFirstName: [firstName, Validators.required],
      contactPersonLastName: [lastName, Validators.required  ],
      contactPersonEmail:  [email, Validators.email],
      contactPersonJobTitle: [jobTitle, Validators.required ],
      contactPersonPrimaryPhone: [primaryPhone, Validators.required],
      contactPersonSecondaryPhone: [secondaryPhone, Validators.required],
      contactPersonMobile: [mobile, Validators.required],
      user: this.formBuilder.group({
        id: [this.userService.userRef().id,Validators.required]
      }),
      businessDirectory: this.formBuilder.group({
        id: [businesId, Validators.required]
      })
    });
  }

 onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmUpdateContactPerson(contactPersonInfo){
   this.businessContactPersonService.updateBusinessContactPerson(this.data.businessContactPersonId,contactPersonInfo).subscribe((data)=> {
    console.log("Sucessfully update")
    console.log(data);
  });
  }

  getPhoneValidationError(event) {
    if (event.value.length < 10) {
      this.phoneValidation = false;
      this.formValidation = false;
      this.message = "Please provide valid number"
    } else {
      this.message = ""
      this.phoneValidation = true;
      this.formValidation = true;
    }
  }
  

  setFormInvalid() {
    if (this.phoneValidation) {
      this.formValidation = true;
    } if (!this.updateBusinessContactPeronForm.valid) {
      this.formValidation = false;
    }
  }
}
