import { UserService } from '../../service/user.service';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { UploadSerivce } from '../../service/file.upload.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';
import { CommonService } from '../../service/common.service';
import { BusinessCertificateService } from '../../service/business-certificate.service';

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.scss']
})
export class CertificatesComponent implements OnInit {

  imageChangedEvent: any = '';
  croppedImage: any = '';
  selectedFiles: FileList;
  documentsForm: FormGroup;
  businessDirectoryId: number;
  userId: number;
  imageUrl: string
  certificateInfo =false;
  businessCertificate="";
  businessPan="";
  businessDocument: any
  moreDocumentsForm: FormGroup; 
  public logoProgressPercentage: number
  public businessProgressPercentage: number
  public panProgressPercentage: number
  public logoBarStaus: number = 0
  public businessCertificateStatus: number = 0
  public panCertificateStatus: number = 0
  public fileName: string = ""
 

  constructor(
  private fb: FormBuilder, 
  private fileupload: UploadSerivce,
  private activeRoute: ActivatedRoute,
  private router: Router,
  private userService : UserService,
  private commonService : CommonService,
  private businessCertificateService: BusinessCertificateService,
  public vcr: ViewContainerRef,
  ) { }

  ngOnInit() {
    this.businessDirectoryId=this.activeRoute.snapshot.params['id'];
    this.userId=this.userService.userRef().id
    this.documentsForm = this.fb.group({
      logo: ['', Validators.required],
      documents: ['',],
      pan: ['',],
      affiliation: ['',],
    })
    console.log("certificate on init"+this.businessDirectoryId)

    this.moreDocumentsForm = this.fb.group({

      document_detail: this.fb.array([
        this.documents()
      ]),
    })

  }

  documents(): FormGroup {
    return this.fb.group({
      file: ["", Validators.required],
      description: ["", Validators.required],
    });
  }
  
  get documentDetailForms() {
    return this.moreDocumentsForm.get('document_detail') as FormArray
  }
  
  addDocument() {
    this.documentDetailForms.push(this.documents());
  }
  
  deleteDocument(i) {
    const check = confirm("Are you sure to delete?")
    if(check){
    this.documentDetailForms.removeAt(i)
    }  
  }


  upload() {
    var myFile: Blob = this.dataURItoBlob(this.croppedImage);
    console.log(myFile)
    
    this.fileupload.uploadfile(myFile,this.businessDirectoryId,this.userId, this.vcr);

  }

  //convert base64 to image/type
  dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/png'
    });
  }

  selectBusinessCertificateFile(fileInput: any) {
    const file = fileInput.target.files[0];
    this.businessDocument= file
   
  }

  uploadBusinessCertificate(){
    this.fileupload.uploadBusinessCertificate(this.businessDocument,this.businessDirectoryId, this.userId, this.vcr);
   this.businessDocument=''; 
  //  console.log("upload message"+this.commonService.getStatus())

}

  uploadBusinessPan(){
    this.fileupload.uploadBusinessPan(this.businessDocument,this.businessDirectoryId, this.userId, this.vcr);
    this.businessDocument=''; 
  }

  selectBusiness(fileInput: any){
    const file = fileInput.target.files[0];
    console.log(file)
  }


  //image crop
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(image: string) {
    this.croppedImage = image;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }
  public saveBusinessDirectoryCertificate(){
    this.router.navigate(['businessdirectory/socialmedia',this.businessDirectoryId])
  }

  ngDoCheck(){
    this.progressBarValue()
  }

  public progressBarValue(): void{
    this.businessCertificateService.logoProgressPercentage.subscribe(data=>{
      if(data){
        this.logoProgressPercentage = Math.round(data['loaded']/data['total'] *100)
        if(this.logoProgressPercentage !== 100 ) {
          this.logoBarStaus = 1
          this.fileName = data['key']
        }
      }
    })

    this.businessCertificateService.businessProgressPercentage.subscribe(data=>{
      if(data){
        this.businessProgressPercentage = Math.round(data['loaded']/data['total'] *100)
        if(this.businessProgressPercentage !== 100){
          this.businessCertificateStatus = 1
        }
      }
    })

    this.businessCertificateService.panProgressPercentage.subscribe(data=>{
      if(data){
        this.panProgressPercentage = Math.round(data['loaded']/data['total'] *100)
        if(this.panProgressPercentage !== 100){
          this.panCertificateStatus = 1
        }
      }
    })
  }
}
