import { BusinessDirService } from '../../service/business-dir.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule, ReactiveFormsModule,FormArray} from '@angular/forms';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss']
})
export class SocialMediaComponent implements OnInit {

  socialmediaForm: FormGroup; 
  businessDirectoryId:number;
  constructor(private fb: FormBuilder, 
    private activeRoute: ActivatedRoute,
    private businessDirectoryService: BusinessDirService,
    private router: Router
  ) { }

  ngOnInit() {
    this.businessDirectoryId=this.activeRoute.snapshot.params['id'];
    this.socialmediaForm = this.fb.group({
      facebookUrl: ['', Validators.pattern],
      twitterUrl: ['',Validators.pattern  ],
      googlePlusUrl:  ['', Validators.pattern],
      linkedinUrl: ['', Validators.pattern],
      instagramUrl: ['', Validators.pattern],
      youtubeUrl: ['', Validators.pattern],
      tumblerUrl: ['', Validators.pattern],
    })
    console.log("contact on init")
    
  }
  public saveSocialMedia(socialMediaInformation){
    this.businessDirectoryService.saveBusinessDirSocialMedia(this.businessDirectoryId, socialMediaInformation)
    .subscribe(data => {
      console.log("Sucessfully Updated.........");
      this.router.navigate(['businessdirectory',this.businessDirectoryId])

    })
  }

}
