import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookingCartService } from '../../service/booking-cart.service';
import { Router } from '@angular/router';

export interface DialogData {
  businessDirectory: string;
  listing: number;
  user: number;
  bookingDate: string;
  clientName: string;
  clientContact: string;
  clientEmail: string;
  paxInfo: Object[];
 
}
@Component({
  selector: 'app-client-info',
  templateUrl: './client-info.component.html',
  styleUrls: ['./client-info.component.scss']
})
export class ClientInfoComponent implements OnInit {
  public contactInfo:FormGroup
  close=1
  constructor(public dialogRef: MatDialogRef<ClientInfoComponent>,
    // private status: StatusService,
    public vcr: ViewContainerRef,
    public bookingCartService: BookingCartService,
    private router: Router,
    public toasterService: ToasterMessageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder) { }

  ngOnInit() {
    console.log(this.data)
    this.contactInfo=this.fb.group({
      clientName:["",Validators.required],
      clientContact:["",Validators.required],
      clientEmail:['', Validators.required]

    })
  }
  book(data){
    this.data.clientName=data.clientName
    this.data.clientContact=data.clientContact
    this.data.clientEmail = data.clientEmail
    console.log(this.data)
    this.bookingCartService.addCart(this.data).subscribe((response: any) => {
      this.toasterService.success(this.vcr, "Booking Success :", "Success")
      this.router.navigate(['bookings/paxinfo',response.id])
    })
  }

}
