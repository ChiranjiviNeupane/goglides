import { Booking } from '../model/booking';
import { BookingCartService } from '../service/booking-cart.service';
import { Listing } from '../model/listing';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { BusinessDirService } from '../service/business-dir.service';
import { Router } from '@angular/router';
import { BusinessDirectory } from '../model/business-directory';
import { ListingService } from '../service/listing.service';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, AfterViewInit {
  business:BusinessDirectory[]
  packages:Listing[]

  selection = new SelectionModel<Element>(true, []);
     /*---------------------Data table----------------------------*/
     @ViewChild(MatSort) sort: MatSort;
     @ViewChild(MatPaginator) paginator: MatPaginator;
     @ViewChild('filter') filter: ElementRef
     ELEMENT_DATA = []
      displayedColumns = ['select', 'position', 'code', 'package', 'name', 'number','date', 'status'];
     dataSource
     /*---------------------Data table----------------------------*/
  

     applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }


  /**
   * Set the sort after the view init since this component will
   * be able to query its view for the initialized sort.
   */

  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
    this.dataSource.sort = this.sort;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  constructor(
    private businessDirService :BusinessDirService,
    private listingService :ListingService,
    private router: Router,
    private bookingService:BookingCartService
  ) { }

  ngOnInit() {
    let bookings ={}
    let bookingList =[]
    this.initBusiness()
    this.bookingService.viewAll().subscribe((data:any)=>{
      // console.log(data[0].listing.id)
      for(let i in data){
        // console.log(data[i]['clientName'])
        this.listingService.getListingById(data[i].listing.id).subscribe((listing:Listing)=>{
          console.log(listing['title'])
          bookings ={
            "id":data[i]['id'],
            "package":listing['title'],
            "clientName":data[i]['clientName'],
            "clientContact":data[i]['clientContact'],
            "bookedDate":data[i]['createdAt'],
            "bookingStatus":data[i]['bookingStatus'],
         
          }
          // console.log(bookings)
          bookingList.push(bookings);
          this.listingService.bookingSubject.next(bookingList)
        })
      }
      this.listingService.bookingSubject.subscribe(bookingData=>{
        this.ELEMENT_DATA=bookingData
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
        this.dataSource.paginator =this.paginator
        this.dataSource.sort=this.sort;
      })
     
    })
  }
  initBusiness(){
    this.businessDirService.getAllBusiness().subscribe((data:BusinessDirectory[])=>{
      this.business=data
      // console.log(data)
    })

  }
  getByCode(code:number){
    console.log(code)
    this.listingService.getListingByCode(code).subscribe((data:Listing)=>{
    this.router.navigate(['bookings/bookingdetail',data.id])

      console.log(data)
    })
  }
  initPackages(busId:number){
    console.log(busId)
    this.listingService.getAllListingByBusiness(busId).subscribe((data:Listing[])=>{
      this.packages=data
      console.log(data)
    })
  }
  selectPackage(value){
    let packageId=value.value
    this.router.navigate(['bookings/bookingdetail',packageId])

  }
  selectBusiness(value){
    let busId = value.value
    console.log(busId)
    this.initPackages(busId)
     }
    
     getBookingPax(bookingId){
     this.router.navigate(['bookings/paxinfo',bookingId])
 
     }

}
