import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { BookingCartService } from '../../service/booking-cart.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Booking } from '../../model/booking';
import { ListingService } from '../../service/listing.service';
import { CategoryService } from '../../service/category.service';
import { ListingMultimediaService } from '../../service/listing-multimedia.service';
import { Listing } from '../../model/listing';
import { Category } from '../../model/category';
import { ListingMultimeda } from '../../model/listing-multimedia';
import { ToastContainer } from 'ng2-toastr';
import { ToasterMessageService } from '../../service/toaster-message.service';

@Component({
  selector: 'app-other-info',
  templateUrl: './other-info.component.html',
  styleUrls: ['./other-info.component.scss']
})
export class OtherInfoComponent implements OnInit {
  bookingId: number
  packageId: number
  businessId: number
  pickup = []
  dropoff = []
  otherInfo: FormGroup

  listing: Listing
  featureImage
  bannerImage
  category
  listItem: any[]
  paymodes = [
    { value: 'cash', option: 'Cash' },
    { value: 'card', option: 'Credit Card' },
  ];

  booking: Booking

  public show: boolean = false;
  toggle() {
    this.show = !this.show;
  }

  constructor(
    public vcr: ViewContainerRef,
    public toasterService: ToasterMessageService,
    public bookingCartService: BookingCartService,
    private router: Router,
    public routerActivate: ActivatedRoute,
    private fb: FormBuilder,

    private listService: ListingService,

    private categoryService: CategoryService,
    private listingMultimediaService: ListingMultimediaService,



  ) { }
  checkPayMode(event) {
    if (event.value == 'card') {
      this.show = true
    }
    else {
      this.show = false
    }
  }
  ngOnInit() {
    this.otherInfo = this.fb.group({
      paymentMode: ['', Validators.required],
      pickup: ['', Validators.required],
      dropOff: ['', Validators.required],
      emergencyContactName: ['', Validators.required],
      emergencyContactNumber: ['', Validators.required],
      comment: ['', Validators.required],
    })
    this.bookingId = this.routerActivate.snapshot.params['id']
    this.bookingCartService.view(this.bookingId).subscribe((data: any) => {
      this.booking = data.booking
      this.packageId = this.booking.listing.id
      this.businessId = this.booking.businessDirectory.id
      this.getPackage(this.packageId)
      this.bookingCartService.getTransportRout(this.businessId, this.packageId).subscribe((data: any) => {
        console.log(data)
        this.pickup = data.pickup
        this.dropoff = data.dropoff
      })

    })

  }
  getPackage(listingId: number) {
    this.listService.getDetailPackageList(listingId).subscribe((data: Listing) => {
      this.listing = data
      this.getListingMultimedia(this.listing.businessDirectory.id, this.packageId)
      this.getCategoryTitle(this.listing.category.id)

    })
  }
  getListingMultimedia(businessId: number, listingId: number) {
    this.listingMultimediaService.getListingMultimediaFeature(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.featureImage = data[0]['fileUrl']
      console.log(this.featureImage)
    })
    this.listingMultimediaService.getListingMultimediaBanner(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.bannerImage = data[0]['fileUrl']
      console.log(this.bannerImage)
    })
  }
  getCategoryTitle(categoryId: number) {
    this.categoryService.getCategoryById(categoryId).subscribe((data: Category) => {
      // console.log(data)
      this.category = data

    })
  }
  book(bookingValue){
    console.log(bookingValue)
    this.bookingCartService.saveOtherInfo(bookingValue,this.bookingId).subscribe((data)=>{
      console.log("saved")
      this.toasterService.success(this.vcr, "Booking Success :", "Success")
      this.router.navigate(['bookings/bookingView', this.booking.id])

    })
  }

}
