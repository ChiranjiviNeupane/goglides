import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { IMyDpOptions } from 'mydatepicker';
import { FormGroupName, FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { BookingService } from '../../service/booking.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking-filter',
  templateUrl: './booking-filter.component.html',
  styleUrls: ['./booking-filter.component.scss']
})
export class BookingFilterComponent implements OnInit {
  public show: boolean = false;
  bookingFilterForm:FormGroup;
  bookingAdvanceFilterForm:FormGroup;
/*---------------------Data table----------------------------*/
@ViewChild(MatSort) sort: MatSort;
@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild('filter') filter: ElementRef
ELEMENT_DATA = []
displayedColumns = ['ref', 'inv','operator', 'user', 'package', 'tourDate', 'paxNo', 'bookedDate', 'status', 'paymentStatus'];
dataSource
/*---------------------Data table----------------------------*/
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
  };

  public bookedfrom: any = { date: { year: null, month: null, day: null } };
  public bookedto: any = { date: { year: null, month: null, day: null } };

 



  constructor(private fb: FormBuilder,public datepipe: DatePipe, private bookingService : BookingService,private router :Router) { }



  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
    this.dataSource.sort = this.sort;
  }

  toggle() {
    this.show = !this.show;
  }


  ngOnInit() {
    console.log(this.datepipe.transform(new Date(),"yy-MM-dd"))
    this.bookingFilterForm = this.fb.group({
      searchField:[""],
      searchType:[""],
    })
    this.bookingAdvanceFilterForm = this.fb.group({
      startDate:[this.datepipe.transform(new Date(),"yyyy-MM-dd")],
      finishDate:[this.datepipe.transform(new Date(),"yyyy-MM-dd")],
      payementStatus:[],
      bookingStatus:[],
    })
    this.searchRecent()
   
    
  }
 
  onDateChanged(event) {
    // Set today date using the patchValue function
    let date = new Date();
    this.bookingAdvanceFilterForm.patchValue({startDate: 
      event.formatted});
}
onDateChangedFinished(event) {
    // Set today date using the patchValue function
    let date = new Date();
    this.bookingAdvanceFilterForm.patchValue({finishDate: 
      event.formatted});
}
// onDateChanged(event){
//   this.bookingAdvanceFilterForm 
// }
clearDate(): void {
    // Clear the date using the patchValue function
    this.bookingAdvanceFilterForm.patchValue({myDate: null});
}
searchBooking(value){
  console.log(value)
  if(value.searchField===null&&value.searchType===null ){
    this.searchRecent()
  }
  else{
    this.bookingService.searchBooking(value.searchField,value.searchType).subscribe((data:any)=>{
      console.log(data)
      this.ELEMENT_DATA=data
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
      this.dataSource.paginator =this.paginator
      this.dataSource.sort=this.sort;
   })
  }
 
  // console.log(value)
}
resetForm(){
  this.bookingFilterForm.reset();
}
goToTicket(bookinId){
  this.router.navigate(['bookings/bookingView', bookinId])
}
searchRecent(){
  this.bookingService.searchRecentBooking().subscribe((data:any)=>{
    // console.log(data)
    this.ELEMENT_DATA=data
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
    this.dataSource.paginator =this.paginator
    this.dataSource.sort=this.sort;
  })
}
advanceFilter(value){
  console.log(value)
  // ,value.bookingStatus,value.payementStatus
  this.bookingService.advanceFilter(value.startDate,value.finishDate).subscribe((data:any)=>{
    console.log(data)
    this.ELEMENT_DATA=data
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
    this.dataSource.paginator =this.paginator
    this.dataSource.sort=this.sort;
  })

}

}

