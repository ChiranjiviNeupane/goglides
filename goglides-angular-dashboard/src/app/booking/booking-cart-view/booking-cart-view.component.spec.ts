import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingCartViewComponent } from './booking-cart-view.component';

describe('BookingCartViewComponent', () => {
  let component: BookingCartViewComponent;
  let fixture: ComponentFixture<BookingCartViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingCartViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingCartViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
