import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../service/listing.service';
import { Listing } from '../../model/listing';

@Component({
  selector: 'app-booking-cart-view',
  templateUrl: './booking-cart-view.component.html',
  styleUrls: ['./booking-cart-view.component.scss']
})
export class BookingCartViewComponent implements OnInit {
  cartLists=[]
  json:any
  listing: any;
  constructor(private http :HttpClient, private listService :ListingService) { }

  ngOnInit() {
  // this.cartLists=(JSON.parse(localStorage.getItem("cart")))
  this.http.get("http://localhost:8082/v1/manage/bookingCart").subscribe((data:any)=>{
    this.json=data
    this.cartLists=JSON.parse((this.json[0]['cart']))
    this.getPackage(this.cartLists[0]['packageId'])

    console.log(this.json[0]['cart'])
    console.log(JSON.parse(this.json[0]['cart']))
  })
  }
  getPackage(businessId: number) {
    this.listService.getDetailPackageList(businessId).subscribe((data: Listing) => {
      this.listing = data
      console.log(this.listing.title)
     
    })
  }

}
