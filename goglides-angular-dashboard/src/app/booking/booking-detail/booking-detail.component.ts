import { BookingCart } from '../../model/bookingCart';
import { BookingCartService } from '../../service/booking-cart.service';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { Cart } from '../../model/cart';
import { CommonService } from '../../service/common.service';
import { BookingService } from '../../service/booking.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListingService } from '../../service/listing.service';
import { Listing } from '../../model/listing';
import { ListingMultimediaService } from '../../service/listing-multimedia.service';
import { ListingMultimeda } from '../../model/listing-multimedia';
import { CategoryService } from '../../service/category.service';
import { Category } from '../../model/category';
import { ListingPriceService } from '../../service/listing-price.service';
import { ListingPricing } from '../../model/listing-pricing';
import { BookingOptions } from 'aws-sdk/clients/workmail';
import { HttpClient } from 'selenium-webdriver/http';
import { UserService } from '../../service/user.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { ClientInfoComponent } from '../client-info/client-info.component';
import { IMyDpOptions, IMyOptions } from 'mydatepicker';

@Component({
  selector: 'app-booking-detail',
  templateUrl: './booking-detail.component.html',
  styleUrls: ['./booking-detail.component.scss']
})
export class BookingDetailComponent implements OnInit {
  packageId: number
  businessId: number
  listing: Listing
  featureImage
  bannerImage
  category
  bookingDate:Date
  public model: any 
  seniorQty: number[] = [0]
  adultQty: number[] = [0]
  childQty: number[] = [0]
  infantQty: number[] = [0]
  seniortotal: number[] = [0]
  adulttotal: number[] = [0]
  childtotal: number[] = [0]
  infanttotal: number[] = [0]
  total: number[] = [0]
  state: number[] = [0]
  listingPrices: ListingPricing[]
  bookingOption: {
    "type": [""],
    "schedule": [""],
    "duration": [""],
  }
  cartList = []
  userId: number;
  bookingCart: BookingCart





  constructor(
    public routerActivate: ActivatedRoute,
    private listService: ListingService,
    private listingMultimediaService: ListingMultimediaService,
    private categoryService: CategoryService,
    private listingPriceService: ListingPriceService,
    private bookingService: BookingService,
    private commonService: CommonService,
    private toasterService: ToasterMessageService,
    public vcr: ViewContainerRef,
    public userService: UserService,
    public bookingCartService: BookingCartService,
    public dialog: MatDialog


  ) { }

  //client info dialog
  openDialog(data): void {
    const dialogRef = this.dialog.open(ClientInfoComponent, {
      width: '800px',
      data: data
    });
  }


  ngOnInit() {
    this.packageId = this.routerActivate.snapshot.params['id']
    this.getPackage(this.packageId)
    this.userId = this.userService.userRef().id


  }
  myDatePickerOptions: IMyDpOptions = {
    todayBtnTxt: 'Today',
    dateFormat: 'yyyy-mm-dd',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    inline: false,
    disableUntil: {year: 2016, month: 8, day: 10}
};

  calcSenior(id, qty, rate) {
    this.seniortotal[id] = qty.target.value * rate
    this.seniorQty[id] = qty.target.value
    console.log(this.seniorQty[id])
    this.calcTotal(id)

  }
  calcAdult(id, qty, rate) {
    this.adulttotal[id] = qty.target.value * rate
    this.adultQty[id] = qty.target.value


    console.log(this.adultQty[id])
    this.calcTotal(id)


  }
  calcChild(id, qty, rate) {
    this.childtotal[id] = qty.target.value * rate
    this.childQty[id] = qty.target.value

    console.log(this.childQty[id])
    this.calcTotal(id)


  }
  calcInfant(id, qty, rate) {
    this.infanttotal[id] = qty.target.value * rate
    this.infantQty[id] = qty.target.value

    console.log(this.infantQty[id])
    this.calcTotal(id)

  }
  calcTotal(id) {
    console.log(id)
    let seniorTotal = 0
    let adultTotal = 0
    let childTotal = 0
    let infantTotal = 0
    seniorTotal = isNaN(this.seniortotal[id]) ? 0 : this.seniortotal[id]
    adultTotal = isNaN(this.adulttotal[id]) ? 0 : this.adulttotal[id]
    childTotal = isNaN(this.childtotal[id]) ? 0 : this.childtotal[id]
    infantTotal = isNaN(this.infanttotal[id]) ? 0 : this.infanttotal[id]

    this.total[id] = seniorTotal + adultTotal + childTotal + infantTotal
    console.log(this.total[id])
  }
  saveBooking(){
    let booking={
      "businessDirectory":this.businessId,
      "listing":Number(this.packageId),
      "user":this.userId,
      "bookingDate":this.bookingDate,
      "clientName":"",
      "categories":"",
      "clientContact":"",
      "clientEmail":"",
      "paxInfo":[{}]
    }
    booking.paxInfo=this.cartList
    // let jsonData = JSON.stringify(this.cartList)
    // booking.categories=jsonData
    this.cartList.forEach(element => {
        booking.categories+=element.category+"-"+element.totalQty+"-"+element.type+"-"+element.schedule+"-"+element.duration+","
    });
    console.log(booking)
    this.openDialog(booking)
    // this.bookingCartService.addCart(booking).subscribe((response: any) => {
    //   console.log("Res")
    //   console.log(response)
    //   this.toasterService.success(this.vcr, "Booking Success :", "Success")

    // })
  }
  addtoCart(item) {
    // this.http.get
    // this.bookingCartService.checkCart(this.userId).subscribe((data: any) => {
    //   if (!data.cart) {
    
    let i = item.id
    let seniorTotal = 0
    let adultTotal = 0
    let childTotal = 0
    let infantTotal = 0
    let seniorQty = 0
    let adultQty = 0
    let childQty = 0
    let infantQty = 0
    seniorTotal = isNaN(this.seniortotal[item.id]) ? 0 : this.seniortotal[item.id]
    adultTotal = isNaN(this.adulttotal[item.id]) ? 0 : this.adulttotal[item.id]
    childTotal = isNaN(this.childtotal[item.id]) ? 0 : this.childtotal[item.id]
    infantTotal = isNaN(this.infanttotal[item.id]) ? 0 : this.infanttotal[item.id]
    seniorQty = isNaN(this.seniorQty[item.id]) ? 0 : Number(this.seniorQty[item.id])
    adultQty = isNaN(this.adultQty[item.id]) ? 0 : Number(this.adultQty[item.id])
    childQty = isNaN(this.childQty[item.id]) ? 0 : Number(this.childQty[item.id])
    infantQty = isNaN(this.infantQty[item.id]) ? 0 : Number(this.infantQty[item.id])
    let totalQty: number = 0
    totalQty = seniorQty + adultQty + childQty + infantQty
    console.log(totalQty)
    
    if (seniorQty == 0 && adultQty == 0 && childQty == 0 && infantQty == 0) {
      this.toasterService.warning(this.vcr, "At Least 1 PAX must be Seleted", "Waring")

    }
    
    else {
      let cart = {
        // "id": item.id,
        "listing": Number(this.packageId),
        "listingPrice": Number(item.id),
        "user":this.userId,
        "businessDirectory": this.businessId,
        "category": item.nationality,
        "type": item.type,
        "schedule": item.schedule,
        "duration": item.duration,
        "bookingDate": this.bookingDate,
        "seniorRate": item.seniorRetailRate,
        "adultRate": item.retailRate,
        "childRate": item.childRetailRate,
        "infantRate": item.infantRetailRate,
        "seniorQty": seniorQty,
        "adultQty": adultQty,
        "childQty": childQty,
        "infantQty": infantQty,
        "totalQty": totalQty,
        "seniorTotal": seniorTotal,
        "adultTotal": adultTotal,
        "childTotal": childTotal,
        "infantTotal": infantTotal,
        "total": this.total[item.id],
      }
      // console.log(this.adultQty[item.id])
      // console.log(item.id)
      this.cartList.push(cart)
      this.commonService.cartCount.next(this.cartList.length)
      this.state[item.id] = 1
      // console.log(JSON.stringify(this.cartList))
      // this.bookingCart= new BookingCart()
      // this.bookingCart.cart=JSON.stringify(this.cartList)
      // this.bookingCart.user=this.userId
        // console.log(cart)
        // console.log("Res")
        // console.log(response)
        this.toasterService.success(this.vcr, "Nationality :" + cart.category, "Added to Cart")

      // localStorage.setItem("cart", JSON.stringify(this.cartList))
      console.log(this.cartList)

    }
  }
  // else{
  //   this.toasterService.error(this.vcr, "Please Process the Previous Cart", "Warning")
  // }

  // })






getPackage(businessId: number) {
  this.listService.getDetailPackageList(businessId).subscribe((data: Listing) => {
    this.listing = data
    this.businessId = this.listing.businessDirectory.id
    this.getListingMultimedia(this.listing.businessDirectory.id, this.packageId)
    this.getCategoryTitle(this.listing.category.id)
    // this.getListingPricing(this.listing.businessDirectory.id,this.packageId)
    this.getFilterOption(this.listing.businessDirectory.id, this.packageId)

  })
}
getListingMultimedia(businessId: number, listingId: number) {
  this.listingMultimediaService.getListingMultimediaFeature(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
    this.featureImage = data[0]['fileUrl']
    console.log(this.featureImage)
  })
  this.listingMultimediaService.getListingMultimediaBanner(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
    this.bannerImage = data[0]['fileUrl']
    console.log(this.bannerImage)
  })
}
getCategoryTitle(categoryId: number) {
  this.categoryService.getCategoryById(categoryId).subscribe((data: Category) => {
    // console.log(data)
    this.category = data

  })
}
// getListingPricing(businessId:number,listingId:number){
//   this.listingPriceService.getListingPrice(businessId,listingId).subscribe((data:ListingPricing[])=>{
//     this.listingPrices=data
//     console.log(data)
//   })
// }
getFilterOption(businessId: number, packageId: number) {
  this.bookingService.getFilterOption(businessId, packageId).subscribe((data: any) => {
    this.bookingOption = data
  })
}
search(value) {

  this.seniortotal = [0]
  this.adulttotal = [0]
  this.childtotal = [0]
  this.infanttotal = [0]
  this.total = [0]
  this.bookingDate=value.bookingDate.formatted
  let data = {
    
    busId: this.businessId,
    packageId: this.packageId,
    type: value.type,
    schedule: value.schedule,
    duration: value.duration,
    bookingDate: this.bookingDate,
  }
  console.log(this.bookingDate)
  // console.log(value.bookingDate.formatted)
  this.bookingService.getPricing(data).subscribe((data: ListingPricing[]) => {
    this.listingPrices = data
    console.log(data)
  })

}

viewCart() {

}

}
