import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddonsInfoComponent } from './addons-info.component';

describe('AddonsInfoComponent', () => {
  let component: AddonsInfoComponent;
  let fixture: ComponentFixture<AddonsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddonsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddonsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
