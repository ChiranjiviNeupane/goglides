import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StatusService } from '../../../service/status.service';
import { DialogData } from '../../client-info/client-info.component';
import { BookingCartService } from '../../../service/booking-cart.service';
import { ListingAddons } from '../../../model/listing-addons';
import { UserService } from '../../../service/user.service';
import { ToasterMessageService } from '../../../service/toaster-message.service';
import { TrimPipe } from 'ngx-pipes';
import { CommonService } from '../../../service/common.service';

@Component({
  selector: 'app-addons-info',
  templateUrl: './addons-info.component.html',
  styleUrls: ['./addons-info.component.scss'],
  providers: [TrimPipe]
})
export class AddonsInfoComponent implements OnInit {
  businessId: number
  packageId: number
  paxId: number
  paxRefId:string

  addons: ListingAddons[]
  objectList = []
  object = {
    user: 0,
    paxId: 0,
    addonsList: []
  }
  constructor(
    public dialogRef: MatDialogRef<AddonsInfoComponent>,
    public bookingCartService: BookingCartService,
    private userService: UserService,
    public toasterService: ToasterMessageService,
    public vcr: ViewContainerRef,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private trimPipe: TrimPipe,
    private commonService: CommonService
  ) {
    // console.log(data)
  }

  ngOnInit() {
    this.businessId = this.data.busId
    this.packageId = this.data.listingId
    this.paxId = this.data.paxId
    this.bookingCartService.getAddons(this.businessId, this.packageId).subscribe((data: ListingAddons[]) => {
      console.log(data)
      this.addons = data
    })
  }

  plusAddons(event, object, category) {
    // console.log(event)
    // console.log(object)
    let customObj = {
      addonsId: object.id,
      retailRate: object.retailRate,
      businessId: object.businessDirectory.id,
      listingId: object.listing.id
    }

    if (event.checked) {
      // console.log(event.checked)
      this.objectList.push(customObj)
      // this.addonsCartBuilder(this.data['index'], customObj)
      this.addonCountBuilder(this.data['index'],this.data['category'], event )

    }
    else {
      // console.log(event.checked)
      // console.log("uncheked")
      let index = this.objectList.findIndex(k => k.addonsId == object.id)
      // console.log(index)
      if (index == 0) {
        this.removeAddons(event)
        this.objectList.splice(0, 1)
      }
      else {
        // console.log("splice"+index)
        this.removeAddons(event)
        this.objectList.splice(index, 1)
      }

    }

    console.log(this.objectList)
  }
  remove(index) {
    this.objectList.splice(index, 1)
  }
  saveAddons() {
    this.object.user = this.userService.userRef().id
    this.object.paxId = this.paxId
    this.object.addonsList = this.objectList
    console.log(this.object )
    // this.bookingCartService.saveAddons(this.object).subscribe((respond: Response) => {
    //   // console.log("saved")
    //   this.toasterService.success(this.vcr, "Addons Added Sucessfully!:", "Success")
    //   if(localStorage.getItem('addonData')){
    //     // this.trimInfo = this.trimPipe.transform(localStorage.getItem('addonData'), '/')
    //     // this.addonData = JSON.parse(this.trimInfo)
    //     // let data = null
    //     // this.buildAddonsJson(this.jsonBuilder(this.addons.length),this.addonData,data )
    //   }
    //   this.dialogRef.close();
    //   // this.router.navigate(['bookings/otherinfo', this.booking.id])
    // })
  }

  innterText
  addonData = []
  trimInfo
  addonsJson
  addonCountBuilder(index,category, event){
  let data = JSON.stringify(event['source']['_elementRef']['nativeElement']['innerText'])
  data = data.split('\\n')[1]
    // if(addonsJson)
  if (localStorage.getItem('addonData')) {
        this.trimInfo = this.trimPipe.transform(localStorage.getItem('addonData'), '/')
        this.addonData = JSON.parse(this.trimInfo)
      }
      if(parseInt(localStorage.getItem('index')) !== index && localStorage.getItem('index') !== null){
        this.commonService.buildAddonsJson(this.commonService.jsonBuilder(this.addons.length), this.addonData, data)
        this.addonData = []
      }
    this.addonData.push(data)
    localStorage.setItem("addonData", JSON.stringify(this.addonData))
    localStorage.setItem("size", this.addons.length.toString())
    localStorage.setItem("index", index)
    this.addonData.sort();
  }

  removeAddons(event){
    let data = event['source']['_elementRef']['nativeElement']['innerText']
    let index = this.addonData.indexOf(data)
    this.addonData.splice(index, 1)
    localStorage.setItem("addonData", JSON.stringify(this.addonData))
  }
}
