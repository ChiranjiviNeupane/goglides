import { element } from 'protractor';
import { BookingItem } from './../../model/bookingItem';
import { Booking } from './../../model/booking';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';

import { AddonsInfoComponent } from '../../booking/pax-info/addons-info/addons-info.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingCartService } from '../../service/booking-cart.service';
import { ListingService } from '../../service/listing.service';
import { Listing } from '../../model/listing';
import { CategoryService } from '../../service/category.service';
import { ListingMultimediaService } from '../../service/listing-multimedia.service';
import { Category } from '../../model/category';
import { CommonService } from '../../service/common.service';
import { ListingMultimeda } from '../../model/listing-multimedia';
import { TrimPipe, RangePipe } from 'ngx-pipes';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { AddOnsAddComponent } from '../../package-form/add-ons/add-ons-add/add-ons-add.component';
import { BookingService } from '../../service/booking.service';


export interface option {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-pax-info',
  templateUrl: './pax-info.component.html',
  styleUrls: ['./pax-info.component.scss'],
  providers: [TrimPipe, RangePipe],
})
export class PaxInfoComponent implements OnInit {
  ind: number =0

  bookingId: number
  booking: Booking
  packageId: number
  businessId: number
  listing: Listing
  featureImage
  bannerImage
  category
  listItem: any[]
  bookingItem: BookingItem[]
  filterbookingItem: BookingItem[]
  paxCategories = []
  categoryDetail = {
    category: "",
    totalPax: "",
    type: "",
    schedule: "",
    duration: "",
  }
  trimInfo
  raw: any
  formatData: any
  prodData = {
    items:[

    ]
  };
  productionForm: FormGroup;

  public show: boolean = false;
  // toggle() {
  //   if(this.show == true){
  //     this.dialog.open(AddOnsAddComponent)
  //   }
  //   // this.show = !this.show;
  // }

  addOns(value, paxId, i, category) {
    console.log(i)
    let data = {
      busId: this.businessId,
      listingId: this.packageId,
      paxId: paxId,
      index: i,
      category: category
    }
    if (value.value == 1) {
      const dialogRef = this.dialog.open(AddonsInfoComponent, {
        width: '800px',
        data: data
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log("closed")
      });
      console.log(data)

    }

    console.log(value.value)
  }

  value: option[] = [
    { value: 'Mr', viewValue: 'Mr' },
    { value: 'Mrs', viewValue: 'Mrs' },
    { value: 'Ms', viewValue: 'Ms' }
  ];

  type: option[] = [
    { value: 'Liscense', viewValue: 'Liscense' },
    { value: 'Passport', viewValue: 'Passport' },
  ];
  addons = [
    { value: 1, viewValue: 'Yes' },
    { value: 0, viewValue: 'No' },
  ];
  num: any;


  constructor(
    public vcr: ViewContainerRef,
    public toasterService: ToasterMessageService,
    private listService: ListingService,
    private trim: TrimPipe,
    private range: RangePipe,
    public dialog: MatDialog,
    public routerActivate: ActivatedRoute,
    public bookingCartService: BookingCartService,
    private categoryService: CategoryService,
    private commonService: CommonService,
    private fb: FormBuilder,
    private listingMultimediaService: ListingMultimediaService,
    private router: Router,
    private trimPipe: TrimPipe,
    private bookingService: BookingService
  ) { }
  add(){
    this.ind++
  }
  
  resetCount(){
  this.ind = 0
  }
  ngOnInit() {


    this.bookingId = this.routerActivate.snapshot.params['id']
    this.bookingCartService.view(this.bookingId).subscribe((data: any) => {
      this.booking = data.booking
      this.packageId = this.booking.listing.id
      this.businessId = this.booking.businessDirectory.id
      this.getPackage(this.packageId)
      console.log(this.booking)
       this.prodData.items = data.items
      this.initForm()



    })


  }
  initForm() {
    this.productionForm = this.fb.group({
      paxInfo: this.fb.array(
        this.prodData
          // for each...
          .items
          .reduce((acc, group) => [
            ...acc,
            // ...product of each group
            ...group.products.map(product =>
              // create a form group
              this.fb.group({
                id: [product.id, Validators.required],
                ageGroup: [product.ageGroup, Validators.required],
                title: [product.title,],
                fullName: [product.fullName,],
                nationality: [product.nationality,],
                email: [product.email,],
                contactNo: [product.contactNo,],
                weight: [product.weight,],
                identificationNo: [product.identificationNo,],
                identificationType: [product.identificationType,],
                addAddon: [product.addAddon,],
              })
            )
          ], [])
      )
    })
  }


  getPackage(listingId: number) {
    this.listService.getDetailPackageList(listingId).subscribe((data: Listing) => {
      this.listing = data
      this.getListingMultimedia(this.listing.businessDirectory.id, this.packageId)
      this.getCategoryTitle(this.listing.category.id)

    })
  }
  getListingMultimedia(businessId: number, listingId: number) {
    this.listingMultimediaService.getListingMultimediaFeature(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.featureImage = data[0]['fileUrl']
      console.log(this.featureImage)
    })
    this.listingMultimediaService.getListingMultimediaBanner(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.bannerImage = data[0]['fileUrl']
      console.log(this.bannerImage)
    })
  }
  getCategoryTitle(categoryId: number) {
    this.categoryService.getCategoryById(categoryId).subscribe((data: Category) => {
      // console.log(data)
      this.category = data

    })
  }

  // addons add dialog
  addonsAdd(): void {
    const dialogRef = this.dialog.open(AddonsInfoComponent, {
      width: '800px',
      height: '400px',
      data: {}
    });


    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.viewValue = result;
    // });
  }

  addonData = []
  addonsCart = []
  saveBooking(data) {
    this.bookingCartService.saveBooking(this.booking.id, data).subscribe((data) => {
      this.toasterService.success(this.vcr, "Booking Success :", "Success")
      if(localStorage.getItem('addonData')){
        this.trimInfo = this.trimPipe.transform(localStorage.getItem('addonData'), '/')
        this.addonData = JSON.parse(this.trimInfo)
        this.commonService.buildAddonsJson(this.commonService.jsonBuilder(parseInt(localStorage.getItem('size'))), this.addonData, null)
        let data ={
          addonsCart : localStorage.getItem('addonsCart')
        }
        this.bookingService.updateBooking(this.booking.id,data).subscribe((data: Listing)=>{
          localStorage.removeItem('index')
          localStorage.removeItem('size')
          localStorage.removeItem('addonsCart')
        })
      }
      this.router.navigate(['bookings/otherinfo', this.booking.id])
    })
  }

}