import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PaxinfoEditComponent } from '../../booking/booking-view/paxinfo-edit/paxinfo-edit.component';
import { MoreOptionComponent } from '../../booking/booking-view/more-option/more-option.component';
import { ToasterMessageService } from '../../service/toaster-message.service';
import { ListingService } from '../../service/listing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingCartService } from '../../service/booking-cart.service';
import { CategoryService } from '../../service/category.service';
import { CommonService } from '../../service/common.service';
import { FormBuilder } from '@angular/forms';
import { ListingMultimediaService } from '../../service/listing-multimedia.service';
import { Booking } from '../../model/booking';
import { Listing } from '../../model/listing';
import { BookingItem } from '../../model/bookingItem';
import { ListingMultimeda } from '../../model/listing-multimedia';
import { Category } from '../../model/category';
import { StatusService } from '../../service/status.service';
import { BookingService } from '../../service/booking.service';
import { BusinessCertificateService } from '../../service/business-certificate.service';
import { businessCertificate } from '../../model/business-certificate';
import { BusinessDirService } from '../../service/business-dir.service';
import { BusinessDirectory } from '../../model/business-directory';
import { isNullOrUndefined } from 'util';
import { TrimPipe } from 'ngx-pipes';

@Component({
  selector: 'app-booking-view',
  templateUrl: './booking-view.component.html',
  styleUrls: ['./booking-view.component.scss'],
  providers: [TrimPipe]
})
export class BookingViewComponent implements OnInit {
  bookingId: number
  booking: Booking
  packageId: number
  businessId: number
  listing: Listing
  business: BusinessDirectory
  featureImage
  bannerImage
  category
  listItem: any[]
  bookingItem: BookingItem[]
  prodData = {
    items: []
  };
  counter:number=0
  generateTickeStatus: Boolean = false
  bookingStatus = [];
  paymentStatus = []
  constructor(public dialog: MatDialog,

    public toasterService: ToasterMessageService,
    private listService: ListingService,
    public vcr: ViewContainerRef,
    public routerActivate: ActivatedRoute,
    public bookingCartService: BookingCartService,
    private categoryService: CategoryService,
    private commonService: CommonService,
    private fb: FormBuilder,
    private listingMultimediaService: ListingMultimediaService,
    private router: Router,
    private statusService: StatusService,
    private bookingService: BookingService,
    private listingService: ListingService,
    private businessService: BusinessDirService,
    private trimPipe : TrimPipe

  ) { }


  // paxinfo edit dialog
  paxinfoEdit(): void {
    const dialogRef = this.dialog.open(PaxinfoEditComponent, {
      width: '800px',
      data: {}
    });
  }

  //more option dialog
  moreOption(): void {
    const dialogRef = this.dialog.open(MoreOptionComponent, {
      width: '800px',
      height: '400px',
      data: {}
    });
  }

  ngOnInit() {
    this.bookingId = this.routerActivate.snapshot.params['id']
    this.getProdData(this.bookingId)
    console.log(this.prodData)
    this.getBookingAndPaymentStatus();
  }

  getProdData(id){
    this.bookingCartService.view(id).subscribe((data: any) => {
      this.booking = data.booking
      this.packageId = this.booking.listing.id
      this.businessId = this.booking.businessDirectory.id
      this.getPackage(this.packageId)
      this.getBusiness(this.businessId);
      this.prodData.items=data.items
        this.prodData.items[0]['total'] = 0;
        for(let pro of this.prodData.items[0]['products']){
          let individualRate = pro['retailRate']
          let addonsTotal = 0
          pro['addons'].forEach(element => {
              addonsTotal+=element.retailRate
            });

          let individualTotal = individualRate+addonsTotal
          this.prodData.items[0].products[this.counter]['individualTotal']=individualTotal
          this.counter++
        }
    })
  }

  getPackage(listingId: number) {
    this.listService.getDetailPackageList(listingId).subscribe((data: Listing) => {
      this.listing = data
      this.generateInvoice(this.listing)
      this.getListingMultimedia(this.listing.businessDirectory.id, this.packageId)
      this.getCategoryTitle(this.listing.category.id)

    })
  }
  getListingMultimedia(businessId: number, listingId: number) {
    this.listingMultimediaService.getListingMultimediaFeature(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.featureImage = data[0]['fileUrl']
    })
    this.listingMultimediaService.getListingMultimediaBanner(businessId, listingId).subscribe((data: ListingMultimeda[]) => {
      this.bannerImage = data[0]['fileUrl']
    })
  }
  getCategoryTitle(categoryId: number) {
    this.categoryService.getCategoryById(categoryId).subscribe((data: Category) => {
      this.category = data

    })
  }

  public getBusiness(id){
      this.businessService.getBusinessById(id).subscribe((data: BusinessDirectory)=>{
        this.business = data
      })
  }

  printComponent(ticket) {
    // let printContents = document.getElementById(ticket).innerHTML;
    // let originalContents = document.body.innerHTML;

    // document.body.innerHTML = printContents;

    // window.print();

    // document.body.innerHTML = originalContents;

    let printContents, popupWin;
    printContents = document.getElementById(ticket).innerHTML;
    let originalContents = document.body.innerHTML;
    popupWin = window.open('', 'top=0,left=0,height=100%,width=auto');

    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
               @media print and (color) {
                * {
                  -webkit-print-color-adjust: exact; 
                  print-color-adjust: exact;
                  color-adjust: exact;
                  -webkit-print-color-adjust: economy;
                  -webkit-print-color-adjust: exact;
                  }
                }
                @page{ margin: auto;  size: all; }
                @media print {
                  body {font-family:arial, sans-serif; font-size: 12px; color: #000; width: 100%; height: 100%; overflow: hidden;}
                  table { page-break-inside:auto; page-break-after:avoid; width: 100%; border:0 !important;}
                  tr { page-break-inside:auto; page-break-after:auto; width:100%; }
                  a[href]:after {content: none !important;}
                  .text-right{text-align:right; padding:0 30px; border-top:1px solid #ddd; border-bottom:1px solid #ddd; margin:10px 0; font-size:16px;}
                  h5.title,h6.pax-head{ margin:15px 0; padding-bottom:10px;}
                  th{text-align:left;}
                  td{ vertical-align:middle;}
                  li{ line-height:1.5;}
                  td.ticket-logo-placeholder{ float:left; display:inline-block;}
                  td.ticket-logo-placeholder{ width:20% !important;}
                  td.ticket-heading{width:78% !important; margin-top:20px;}
                  img.logo-bus{margin-top:10px;}
                  .ml-5{ margin-left:25px;}
                  .title{
                    background: #07575b !important; 
                    padding: 5px 10px 0; 
                    color: #fff !important; 
                    margin-bottom: 0;
//                     font-size: 13px;
                  }
                  .pax-head{
                    background: #aaa !important; 
                    padding: 6px 10px; 
                    margin: 0;
//                     font-size:12px;
                  }
                  table{
//                     font-size: 12px;
                    font-weight:normal !imporatnt;
                    text-shadow:none;
                  }
                 .material-icons{display:none;}
                }
               </style>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
          <link rel="stylesheet" href="assets/css/demo.css">
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();

  }


  public getBookingAndPaymentStatus() {
    this.paymentStatus = this.statusService.getPaymentStatus();
    this.bookingStatus = this.statusService.getBookingStatus();
  }

  public onClickPaymentStatus(event, booking): void {
    console.log(booking)
    let data = {
      status: event.value,
      type: "PAYMENT_STATUS",
      id: booking
    }
    this.bookingService.updateBookingStatus(data).subscribe((data: Booking) => {
      this.toasterService.success(this.vcr, "Payment Status Updated !!!", "SUCCESS")
      this.booking['paymentStatus'] = data['paymentStatus']
    })
  }

  public onClickBookingStatus(event, booking): void {
    let data = {
      status: event.value,
      type: "BOOKING_STATUS",
      id: booking
    }
    this.bookingService.updateBookingStatus( data).subscribe((data: Booking) => {
      this.toasterService.success(this.vcr, "Booking Status Updated !!!", "SUCCESS")
      this.booking['bookingStatus'] = data['bookingStatus']
    })
  }

// public bookingStatus(): void{
  
// }

public issueTicket(booking): void{
  this.bookingService.issueTicke(booking['id']).subscribe((data: Listing)=>{
    this.prodData = {
      items : []
    }
    this.toasterService.success(this.vcr, "Ticket Generated !!!","SUCCESS")
    this.getProdData(booking['id'])
  })
  }

  trimInfo 
  public invoices: any
  public addonsCarts: any
  generateInvoice(booking){
    this.trimInfo = this.trimPipe.transform(this.booking.cart, '/')
      this.invoices = JSON.parse(this.trimInfo)
      this.trimInfo = this.trimPipe.transform(this.booking.addonsCart, '/')
      this.addonsCarts = JSON.parse(this.trimInfo)
      let sub_total = 0
    for(let i in this.invoices){
      this.invoices['sub_total'] = 0
      this.invoices[i]['listingTitle'] = this.listing.title 
      this.invoices[i]['addons'] = this.addonsCarts[i]
      this.invoices[i]['total'] += this.invoices[i]['addons']['total']
      sub_total += parseInt(this.invoices[i]['total'])
      this.invoices['sub_total'] +=sub_total
    }
  }
}
