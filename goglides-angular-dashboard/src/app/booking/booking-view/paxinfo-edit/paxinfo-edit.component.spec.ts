import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaxinfoEditComponent } from './paxinfo-edit.component';

describe('PaxinfoEditComponent', () => {
  let component: PaxinfoEditComponent;
  let fixture: ComponentFixture<PaxinfoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaxinfoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaxinfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
