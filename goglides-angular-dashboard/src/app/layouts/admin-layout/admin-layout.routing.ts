import { Routes } from '@angular/router';

// import { DashboardComponent } from '../../dashboard/dashboard.component';

import { PackageFormComponent } from '../../package-form/package-form.component';
import { GeneralComponent } from '../../package-form/general/general.component';
import { ItineraryComponent } from '../../package-form/itinerary/itinerary.component';
import { FactsComponent } from '../../package-form/facts/facts.component';
import { InclusionComponent } from '../../package-form/inclusion/inclusion.component';
import { ChecklistComponent } from '../../package-form/checklist/checklist.component';
import { DestinationComponent } from '../../package-form/destination/destination.component';
import { PricingComponent } from '../../package-form/pricing/pricing.component';
import { AddOnsComponent } from '../../package-form/add-ons/add-ons.component';
import { MediaComponent } from '../../package-form/media/media.component';
import { CouponComponent } from '../../package-form/coupon/coupon.component';
import { FullcalendarComponent } from '../../package-form/fullcalendar/fullcalendar.component';
// import { MapComponent } from '../../package-form/map/map.component';
import { FaqComponent } from '../../package-form/faq/faq.component';
import { PolicyComponent } from '../../package-form/policy/policy.component';
import { SettingComponent } from '../../package-form/setting/setting.component';
import { PackageSeoComponent } from '../../package-form/package-seo/package-seo.component';
import { PackageListComponent } from '../../package-form/package-list/package-list.component';
import { PackageDetailViewComponent } from '../../package-form/package-detail-view/package-detail-view.component';
import { ChooseBusinessComponent } from '../../package-form/choose-business/choose-business.component';

import { BussinessDirectoryComponent } from '../../bussiness-directory/bussiness-directory.component';
import { BusinessListComponent } from '../../bussiness-directory/business-list/business-list.component';
import { BasicInfoComponent } from '../../bussiness-directory/basic-info/basic-info.component';
import { LocationComponent } from '../../bussiness-directory/location/location.component';
import { ContactPersonComponent } from '../../bussiness-directory/contact-person/contact-person.component';
import { CertificatesComponent } from '../../bussiness-directory/certificates/certificates.component';
import { SocialMediaComponent } from '../../bussiness-directory/social-media/social-media.component';
import { BusinessPolicyComponent } from '../../bussiness-directory/business-policy/business-policy.component';
import { BusinessSeoComponent } from '../../bussiness-directory/business-seo/business-seo.component';
import { BusinessViewComponent } from '../../bussiness-directory/business-view/business-view.component';
import { PackageListViewComponent } from '../../bussiness-directory/package-list-view/package-list-view.component';

import { CategoryComponent } from '../../catalog/category/category.component';
import { CatalogComponent } from '../../catalog/catalog.component';
import { ChecklistsComponent } from '../../catalog/checklists/checklists.component';
import { IncExcComponent } from '../../catalog/inc-exc/inc-exc.component';
import { ForexComponent } from '../../catalog/forex/forex.component';

import { SettingsComponent } from '../../settings/settings.component';
import { SettingListComponent } from '../../settings/setting-list/setting-list.component';
import { GeneralSettingComponent } from '../../settings/general-setting/general-setting.component';
import { SocialSettingComponent } from '../../settings/social-setting/social-setting.component';
import { SeoSettingComponent } from '../../settings/seo-setting/seo-setting.component';
import { PaymentSettingComponent } from '../../settings/payment-setting/payment-setting.component';
import { KeyclockSettingComponent } from '../../settings/keyclock-setting/keyclock-setting.component';
import { ApiSettingComponent } from '../../settings/api-setting/api-setting.component';

import { HomeComponent } from '../../home/home.component';

import { BookingComponent } from '../../booking/booking.component';
import { BookingDetailComponent } from '../../booking/booking-detail/booking-detail.component';
import { PaxInfoComponent } from '../../booking/pax-info/pax-info.component';
import { OtherInfoComponent } from '../../booking/other-info/other-info.component';
import { BookingCartViewComponent } from '../../booking/booking-cart-view/booking-cart-view.component';
import { BookingViewComponent } from '../../booking/booking-view/booking-view.component';
import { BookingFilterComponent } from './../../booking/booking-filter/booking-filter.component';
import { NotificationComponent } from '../../notification/notification.component';


export const AdminLayoutRoutes: Routes = [

    // { path: 'dashboard', component: DashboardComponent },
    { path: 'dashboard', component: HomeComponent },
    { path: 'choosebusiness', component: ChooseBusinessComponent},
    { path: 'packages/list', component: PackageListComponent},
    {
        path: 'notification',
        component:NotificationComponent
      },
    { path: 'packages', component: PackageFormComponent,
       children: [
        { path: 'general/:id', component: GeneralComponent},
        { path: 'itinerary/:businessId/:listingId', component: ItineraryComponent},
        { path: 'facts/:businessId/:listingId', component: FactsComponent},
        { path: 'inclusion/:businessId/:listingId', component: InclusionComponent},
        { path: 'checklist/:businessId/:listingId', component: ChecklistComponent},
        { path: 'destination/:businessId/:listingId', component: DestinationComponent,},
        { path: 'pricing/:businessId/:listingId', component: PricingComponent},
        { path: 'addons/:businessId/:listingId', component: AddOnsComponent},
        { path: 'media/:businessId/:listingId', component: MediaComponent},
        { path: 'coupon', component: CouponComponent},
        { path: 'calendar', component: FullcalendarComponent},
        // { path: 'location', component: MapComponent},
        { path: 'faq/:businessId/:listingId', component: FaqComponent},
        { path: 'policy/:businessId/:listingId', component: PolicyComponent},
        { path: 'setting', component: SettingComponent},
        { path: 'seo', component: PackageSeoComponent},
       ]
    },
    { path: 'packages/detailview/:id', component: PackageDetailViewComponent},
    // { path: 'packages/detailview', component: PackageDetailViewComponent},
    { path: 'businessdirectory', component: BussinessDirectoryComponent,
        children: [
            { path: 'basicinfo', component: BasicInfoComponent},
            { path: 'contact/:id', component: ContactPersonComponent},
            { path: 'location/:id', component: LocationComponent}, 
            { path: 'certificates/:id', component: CertificatesComponent},
            { path: 'socialmedia/:id', component: SocialMediaComponent},
            { path: 'businesspolicy', component: BusinessPolicyComponent},
            { path: 'seo', component: BusinessSeoComponent},
        ]
    },
    { path: 'businessdirectory/list', component: BusinessListComponent},
    { path: 'businessdirectory/view', component: BusinessViewComponent},
    { path: 'businessdirectory/viewpackages/:businessId', component: PackageListViewComponent},
    { path: 'businessdirectory/:businessId', component: BusinessViewComponent},
    { path: 'catalog', component: CatalogComponent },
    { path: 'category', component: CategoryComponent },
    { path: 'include', component: IncExcComponent },
    { path: 'checklists', component: ChecklistsComponent },
    { path: 'forex', component: ForexComponent },
    // { path: 'catalog', component: CatalogComponent,
    //     children: [
    //         { path: 'category', component: CategoryComponent },
    //         { path: 'include', component: IncExcComponent },
    //         { path: 'checklists', component: ChecklistsComponent },
    //         { path: 'forex', component: ForexComponent },
    //         { path: 'catalogview', component: CatalogViewComponent }
    //     ]
    // },

    { path: 'settings/list', component: SettingListComponent},
    { path: 'settings', component: SettingsComponent,
        children: [
            { path: 'generalsetting', component: GeneralSettingComponent },
            { path: 'socialsetting', component: SocialSettingComponent },
            { path: 'seosetting', component: SeoSettingComponent },
            { path: 'paymentsetting', component: PaymentSettingComponent },
            { path: 'keyclocksetting', component: KeyclockSettingComponent },
            { path: 'apisetting', component: ApiSettingComponent },
        ] 
    },
    
    { path: 'bookings/bookingdetail/:id', component: BookingDetailComponent},
    { path: 'bookings/paxinfo/:id', component: PaxInfoComponent},
    { path: 'bookings/otherinfo/:id', component: OtherInfoComponent},
    { path: 'bookings/viewcart', component: BookingCartViewComponent},
    { path: 'bookings/bookingView/:id', component: BookingViewComponent},
    { path: 'bookings/bookingFilter', component: BookingFilterComponent},
    
    { path: 'bookings', component: BookingComponent,
        // children: [
        //     { path: 'bookingdetail', component: BookingDetailComponent },
        // ] 
    },
    
];
