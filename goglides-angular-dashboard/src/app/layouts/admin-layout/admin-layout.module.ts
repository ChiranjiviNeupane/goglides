import { ItineraryUpdateComponent } from '../../package-form/itinerary/itinerary-update/itinerary-update.component';
import { ItineraryAddComponent } from '../../package-form/itinerary/itinerary-add/itinerary-add.component';
import { GeneralUpdateComponent } from '../../package-form/general/general-update/general-update.component';
import { ContactPersonAddComponent } from '../../bussiness-directory/contact-person/contact-person-add/contact-person-add.component';
import { BusinessLogoUpdateComponent } from '../../bussiness-directory/basic-info/business-logo-update/business-logo-update.component';
import { IncExcDeleteComponent } from '../../catalog/inc-exc/inc-exc-delete/inc-exc-delete.component';
import { CheckListDeleteComponent } from '../../catalog/checklists/check-list-delete/check-list-delete.component';
import { CategoryDeleteComponent } from '../../catalog/category/category-delete/category-delete.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { PackageFormComponent } from '../../package-form/package-form.component';
import { GeneralComponent } from '../../package-form/general/general.component';
import { ItineraryComponent } from '../../package-form/itinerary/itinerary.component';
import { FactsComponent } from '../../package-form/facts/facts.component';
import { InclusionComponent } from '../../package-form/inclusion/inclusion.component';
import { ChecklistComponent } from '../../package-form/checklist/checklist.component';
import { DestinationComponent } from '../../package-form/destination/destination.component';
import { PricingComponent } from '../../package-form/pricing/pricing.component';
import { AddOnsComponent } from '../../package-form/add-ons/add-ons.component';
import { MediaComponent } from '../../package-form/media/media.component';
import { CouponComponent } from '../../package-form/coupon/coupon.component';
import { FullcalendarComponent } from '../../package-form/fullcalendar/fullcalendar.component';
import { MapComponent } from '../../package-form/map/map.component';
import { FaqComponent } from '../../package-form/faq/faq.component';
import { PolicyComponent } from '../../package-form/policy/policy.component';
import { SettingComponent } from '../../package-form/setting/setting.component';
import { BussinessDirectoryComponent } from '../../bussiness-directory/bussiness-directory.component';
import { BusinessListComponent } from '../../bussiness-directory/business-list/business-list.component';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { NgxEditorModule } from 'ngx-editor';
import { HttpClientModule } from '@angular/common/http';
import { FullCalendarModule } from 'ng-fullcalendar';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatTooltipModule,
  MatSelectModule,
  MatStepperModule,
  MatExpansionModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRadioModule,
  MatChipsModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatTableModule,
  MatDialogModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatListModule,
  MatSortModule,
  MatFormFieldControl,
  MatAutocompleteModule,
  MatMenuModule
  
} from '@angular/material';
import { CatalogComponent } from '../../catalog/catalog.component';
import { CategoryComponent } from '../../catalog/category/category.component';
import { IncExcComponent } from '../../catalog/inc-exc/inc-exc.component';
import { ChecklistsComponent } from '../../catalog/checklists/checklists.component';
import { BasicInfoComponent } from '../../bussiness-directory/basic-info/basic-info.component';
import { LocationComponent } from '../../bussiness-directory/location/location.component';
import { ContactPersonComponent } from '../../bussiness-directory/contact-person/contact-person.component';
import { CertificatesComponent } from '../../bussiness-directory/certificates/certificates.component';
import { SocialMediaComponent } from '../../bussiness-directory/social-media/social-media.component';
import { PackageListComponent } from '../../package-form/package-list/package-list.component';
import { CategoryUpdateComponent } from '../../catalog/category/category-update/category-update.component';
import { CheckListUpdateComponent } from '../../catalog/checklists/check-list-update/check-list-update.component';
import { IncExcUpdateComponent } from '../../catalog/inc-exc/inc-exc-update/inc-exc-update.component';
import { BusinessViewComponent } from '../../bussiness-directory/business-view/business-view.component';
import { PackageListViewComponent } from '../../bussiness-directory/package-list-view/package-list-view.component';
import { PackageDetailViewComponent } from '../../package-form/package-detail-view/package-detail-view.component';

import { BasicInfoUpdateComponent } from '../../bussiness-directory/basic-info/basic-info-update/basic-info-update.component';
import { LocationUpdateComponent } from '../../bussiness-directory/location/location-update/location-update.component';
import { ContactPersonUpdateComponent } from '../../bussiness-directory/contact-person/contact-person-update/contact-person-update.component';
import { InternationalPhoneModule } from 'ng4-intl-phone';

import { VerifiedComponent } from '../../bussiness-directory/certificates/verified/verified.component';
import { ForexComponent } from '../../catalog/forex/forex.component';
import { CatalogViewComponent } from '../../catalog/catalog-view/catalog-view.component';
import { ChooseBusinessComponent } from '../../package-form/choose-business/choose-business.component';
import { ForexUpdateComponent } from '../../catalog/forex/forex-update/forex-update.component';
import { ForexDeleteComponent } from '../../catalog/forex/forex-delete/forex-delete.component';

import { SettingsComponent } from '../../settings/settings.component';
import { SettingListComponent } from '../../settings/setting-list/setting-list.component';
import { GeneralSettingComponent } from '../../settings/general-setting/general-setting.component';
import { SocialSettingComponent } from '../../settings/social-setting/social-setting.component';
import { SeoSettingComponent } from '../../settings/seo-setting/seo-setting.component';
import { PaymentSettingComponent } from '../../settings/payment-setting/payment-setting.component';
import { KeyclockSettingComponent } from '../../settings/keyclock-setting/keyclock-setting.component';
import { ApiSettingComponent } from '../../settings/api-setting/api-setting.component';

import { PackageSeoComponent } from '../../package-form/package-seo/package-seo.component';
import { BusinessPolicyComponent } from '../../bussiness-directory/business-policy/business-policy.component';
import { BusinessSeoComponent } from '../../bussiness-directory/business-seo/business-seo.component';

import { BannerEditComponent } from '../../package-form/package-detail-view/banner-edit/banner-edit.component';
import { BannerViewComponent } from '../../package-form/package-detail-view/banner-view/banner-view.component';
import { FeatureViewComponent } from '../../package-form/package-detail-view/feature-view/feature-view.component';
import { FeatureEditComponent } from '../../package-form/package-detail-view/feature-edit/feature-edit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from '../../home/home.component';
import { PricingUpdateComponent } from '../../package-form/pricing/pricing-update/pricing-update.component';
import { AddOnsUpdateComponent } from '../../package-form/add-ons/add-ons-update/add-ons-update.component';
import { PricingAddComponent } from '../../package-form/pricing/pricing-add/pricing-add.component';
import { FaqUpdateComponent } from '../../package-form/faq/faq-update/faq-update.component';
import { PolicyUpdateComponent } from '../../package-form/policy/policy-update/policy-update.component';
import { PolicyAddComponent } from '../../package-form/policy/policy-add/policy-add.component';
import { FaqAddComponent } from '../../package-form/faq/faq-add/faq-add.component';
import { AddOnsAddComponent } from '../../package-form/add-ons/add-ons-add/add-ons-add.component';
import { InclusionAddComponent } from '../../package-form/inclusion/inclusion-add/inclusion-add.component';
import { InclusionUpdateComponent } from '../../package-form/inclusion/inclusion-update/inclusion-update.component';
import { ChecklistUpdateComponent } from '../../package-form/checklist/checklist-update/checklist-update.component';
import { ChecklistAddComponent } from '../../package-form/checklist/checklist-add/checklist-add.component';
import { DestinationUpdateComponent } from '../../package-form/destination/destination-update/destination-update.component';
import { DestinationAddComponent } from '../../package-form/destination/destination-add/destination-add.component';
import { FactsUpdateComponent } from '../../package-form/facts/facts-update/facts-update.component';
import { PickupDropoffAddComponent } from '../../package-form/facts/pickup-dropoff-add/pickup-dropoff-add.component';
import { PickupDropoffUpdateComponent } from '../../package-form/facts/pickup-dropoff-update/pickup-dropoff-update.component';

import { BookingComponent } from '../../booking/booking.component';
import { BookingDetailComponent } from '../../booking/booking-detail/booking-detail.component';
import { PaxInfoComponent } from '../../booking/pax-info/pax-info.component';
import { OtherInfoComponent } from '../../booking/other-info/other-info.component';
import { AddonsInfoComponent } from '../../booking/pax-info/addons-info/addons-info.component';
import { BookingCartViewComponent } from '../../booking/booking-cart-view/booking-cart-view.component';
import { DestinationDeleteComponent } from '../../package-form/destination/destination-delete/destination-delete.component';
import { PickupDropoffDeleteComponent } from '../../package-form/facts/pickup-dropoff-delete/pickup-dropoff-delete.component';
import { FaqDeleteComponent } from '../../package-form/faq/faq-delete/faq-delete.component';
import { ItineraryDeleteComponent } from '../../package-form/itinerary/itinerary-delete/itinerary-delete.component';
import { MediaDeleteComponent } from '../../package-form/media/media-delete/media-delete.component';
import { PricinDeleteComponent } from '../../package-form/pricing/pricin-delete/pricin-delete.component';
import { MediaAddComponent } from '../../package-form/media/media-add/media-add.component';
import { ClientInfoComponent } from '../../booking/client-info/client-info.component';
import { MyDatePickerModule } from 'mydatepicker';
import { NgPipesModule, NgObjectPipesModule } from 'ngx-pipes';
import { MapUpdateComponent } from '../../package-form/media/map-update/map-update.component';
import { BookingViewComponent } from '../../booking/booking-view/booking-view.component';
import { PaxinfoEditComponent } from '../../booking/booking-view/paxinfo-edit/paxinfo-edit.component';
import { MoreOptionComponent } from '../../booking/booking-view/more-option/more-option.component';
import { BookingFilterComponent } from '../../booking/booking-filter/booking-filter.component';
import { NotificationComponent } from '../../notification/notification.component';
import { NotificationService } from '../../service/notification.service';
import { ReviewComponent } from '../../package-form/review/review.component';
import { ReviewService } from '../../service/review.service';

@NgModule({
  imports: [
    MyDatePickerModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatInputModule,
    MatTooltipModule,
    MatSelectModule,
    MatStepperModule,
    MatExpansionModule,
    MatCardModule,
    MatCheckboxModule,
    NgxImageGalleryModule,
    NgxEditorModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatChipsModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatListModule,
    MatSortModule,
    NgPipesModule,
    ReactiveFormsModule,
    FullCalendarModule,
    ImageCropperModule,
    InternationalPhoneModule,
    MatAutocompleteModule,
    AmazingTimePickerModule,
    NgbModule,
    MatMenuModule,
  ],
  declarations: [
    DashboardComponent,
    NotificationComponent,
    PackageFormComponent,
    GeneralComponent,
    ItineraryComponent,
    FactsComponent,
    InclusionComponent,
    ChecklistComponent,
    DestinationComponent,
    PricingComponent,
    AddOnsComponent,
    MediaComponent,
    CouponComponent,
    FullcalendarComponent,
    MapComponent,
    FaqComponent,
    PolicyComponent,
    SettingComponent,

    BussinessDirectoryComponent,
    BusinessListComponent,
    BasicInfoComponent,
    LocationComponent,
    ContactPersonComponent,
    CertificatesComponent,
    SocialMediaComponent,
    CatalogComponent,
    CategoryComponent,
    IncExcComponent,
    ChecklistsComponent,
    PackageListComponent,
    CategoryUpdateComponent,
    CategoryDeleteComponent,
    CheckListUpdateComponent,
    CheckListDeleteComponent,
    IncExcDeleteComponent,
    IncExcUpdateComponent,
    BusinessViewComponent,
    PackageListViewComponent,
    PackageDetailViewComponent,
    BasicInfoUpdateComponent,
    LocationUpdateComponent,
    ContactPersonUpdateComponent,
    BusinessLogoUpdateComponent,
    ContactPersonAddComponent,
    VerifiedComponent,
    ForexComponent,
    CatalogViewComponent,
    ChooseBusinessComponent,
    ForexUpdateComponent,
    ForexDeleteComponent,
    SettingsComponent,
    GeneralSettingComponent,
    PackageSeoComponent,
    BusinessPolicyComponent,
    BusinessSeoComponent,
    GeneralUpdateComponent,
    ItineraryAddComponent,
    ItineraryUpdateComponent,
    BannerEditComponent,
    BannerViewComponent,
    FeatureViewComponent,
    FeatureEditComponent,
    HomeComponent,
    PricingUpdateComponent,
    AddOnsUpdateComponent,
    PricingAddComponent,
    FaqUpdateComponent,
    PolicyUpdateComponent,
    PolicyAddComponent,
    FaqAddComponent,
    AddOnsAddComponent,
    InclusionAddComponent,
    InclusionUpdateComponent,
    ChecklistUpdateComponent,
    ChecklistAddComponent,
    DestinationUpdateComponent,
    DestinationAddComponent,
    SettingListComponent,
    SocialSettingComponent,
    SeoSettingComponent,
    PaymentSettingComponent,
    KeyclockSettingComponent,
    ApiSettingComponent,
    BookingComponent,
    BookingDetailComponent,
    PaxInfoComponent,
    OtherInfoComponent,
    FactsUpdateComponent,
    PickupDropoffAddComponent,
    PickupDropoffUpdateComponent,
    AddonsInfoComponent,
    BookingCartViewComponent,
    DestinationDeleteComponent,
    PickupDropoffDeleteComponent,
    FaqDeleteComponent,
    ItineraryDeleteComponent,
    MediaDeleteComponent,
    PricinDeleteComponent,
    MediaAddComponent,
    ClientInfoComponent,
    BookingViewComponent,
    MapUpdateComponent,
    PaxinfoEditComponent,
    MoreOptionComponent,
    BookingFilterComponent,
    ReviewComponent,
    
  ],
  entryComponents: [
  CategoryUpdateComponent,
  CategoryDeleteComponent,
  CheckListUpdateComponent,
  CheckListDeleteComponent,
  IncExcDeleteComponent,
  IncExcUpdateComponent,
  BasicInfoUpdateComponent,
  LocationUpdateComponent,
  BusinessLogoUpdateComponent,
  ContactPersonUpdateComponent,
  ContactPersonAddComponent,
  VerifiedComponent,
  ForexDeleteComponent,
  ForexUpdateComponent,
  GeneralUpdateComponent,
  ItineraryAddComponent,
  ItineraryUpdateComponent,
  BannerEditComponent,
  BannerViewComponent,
  FeatureEditComponent,
  FeatureViewComponent,
  PricingUpdateComponent,
  AddOnsUpdateComponent,
  AddOnsAddComponent,
  PricingAddComponent,
  FaqUpdateComponent,
  PolicyUpdateComponent,
  PolicyAddComponent,
  FaqAddComponent,
  InclusionAddComponent,
  InclusionUpdateComponent,
  ChecklistUpdateComponent,
  ChecklistAddComponent,
  DestinationUpdateComponent,
  DestinationAddComponent,
  FactsUpdateComponent,
  PickupDropoffAddComponent,
  PickupDropoffUpdateComponent,
  AddonsInfoComponent,
  DestinationDeleteComponent,
  PickupDropoffDeleteComponent,
  PricinDeleteComponent,
  FaqDeleteComponent,
  ItineraryDeleteComponent,
  MediaDeleteComponent,
  MediaAddComponent,
  ClientInfoComponent,
  SettingComponent,
  MapUpdateComponent,
  PaxinfoEditComponent,
  MoreOptionComponent
],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [],
})

export class AdminLayoutModule { }
