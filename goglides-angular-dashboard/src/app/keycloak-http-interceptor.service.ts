import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import { KeycloakService } from './keycloak-service/keycloak.service';
@Injectable()
export class KeycloakHttpInterceptorService {

  constructor(public _keycloakService: KeycloakService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    if(this._keycloakService.authenticated()){
      return this.handleSecureRequest(request, next);
    } else {
      return next.handle(request)
    }
  }

  handleSecureRequest(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const tokenPromise: Promise<string> = this._keycloakService.getToken();
    const tokenObservable: Observable<string> = Observable.fromPromise(tokenPromise);

    return tokenObservable.map(token => {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`
          }
        });

        return request;
      }).concatMap(req => next.handle(req));
  }

}
