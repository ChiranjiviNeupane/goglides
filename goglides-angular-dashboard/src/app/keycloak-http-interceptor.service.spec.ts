import { TestBed, inject } from '@angular/core/testing';

import { KeycloakHttpInterceptorService } from './keycloak-http-interceptor.service';

describe('KeycloakHttpInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KeycloakHttpInterceptorService]
    });
  });

  it('should be created', inject([KeycloakHttpInterceptorService], (service: KeycloakHttpInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
