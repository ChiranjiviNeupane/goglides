import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyclockSettingComponent } from './keyclock-setting.component';

describe('KeyclockSettingComponent', () => {
  let component: KeyclockSettingComponent;
  let fixture: ComponentFixture<KeyclockSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyclockSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyclockSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
