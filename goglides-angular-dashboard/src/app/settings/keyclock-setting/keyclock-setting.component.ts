import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-keyclock-setting',
  templateUrl: './keyclock-setting.component.html',
  styleUrls: ['./keyclock-setting.component.scss']
})
export class KeyclockSettingComponent implements OnInit {

  keyclockSettingForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.keyclockSettingForm = this.fb.group({
      serverUrl: [''],
      realmId: [''],
      clientId: [''],
      clientSecret: [''],
      uname: [''],
      password: ['']
    });
  }

}
