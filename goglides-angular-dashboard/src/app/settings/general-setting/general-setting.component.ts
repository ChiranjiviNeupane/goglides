import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
export interface option {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-general-setting',
  templateUrl: './general-setting.component.html',
  styleUrls: ['./general-setting.component.scss']
})
export class GeneralSettingComponent implements OnInit {
  generalSettingForm: FormGroup

  value: option[] = [
    {value: '0', viewValue: 'NRP'},
    {value: '1', viewValue: 'USD'},
    {value: '2', viewValue: 'INR'}
  ];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.generalSettingForm = this.fb.group({
      siteName: ['', Validators.required],
      siteUrl: ['', Validators.required],
      tagline: [''],
      adminEmail: ['', Validators.required],
      copyright: [''],
      businessHour: [],
      primaryAdd: ['',Validators.required],
      secondaryAdd:[],
      primaryContact:["",Validators.required],
      secondaryContact: [''],
      fax1: [''],
      fax2:[''],
      map: [''],
      logo: [''],
      siteMode: [''],
      currency: ['']
    });
  }

}
