import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-api-setting',
  templateUrl: './api-setting.component.html',
  styleUrls: ['./api-setting.component.scss']
})
export class ApiSettingComponent implements OnInit {

  apiSettingForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.apiSettingForm = this.fb.group({
      apiKey1: [''],
    });
  }

}
