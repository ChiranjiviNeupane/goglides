import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiSettingComponent } from './api-setting.component';

describe('ApiSettingComponent', () => {
  let component: ApiSettingComponent;
  let fixture: ComponentFixture<ApiSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
