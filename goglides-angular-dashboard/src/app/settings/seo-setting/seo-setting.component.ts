import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-seo-setting',
  templateUrl: './seo-setting.component.html',
  styleUrls: ['./seo-setting.component.scss']
})
export class SeoSettingComponent implements OnInit {

  seoSettingForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.seoSettingForm = this.fb.group({
      title: [''],
      keyword: [''],
      description: [''],
    });
  }

}
