import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-payment-setting',
  templateUrl: './payment-setting.component.html',
  styleUrls: ['./payment-setting.component.scss']
})
export class PaymentSettingComponent implements OnInit {

  paymentSettingForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.paymentSettingForm = this.fb.group({
      title: ["", Validators.required],
      status: ["", Validators.required],
      code: ['', Validators.required],
      user: this.fb.group({
        id: []
      })
    });
  }

}
