import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-social-setting',
  templateUrl: './social-setting.component.html',
  styleUrls: ['./social-setting.component.scss']
})
export class SocialSettingComponent implements OnInit {
  generalSettingForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.generalSettingForm = this.fb.group({
      fb: [''],
      gplus: [''],
      twitter: [''],
      youtube: [''],
      tumblr: [''],
      insta: ['']
    });
  }

}
