import { ToasterMessageService } from './../service/toaster-message.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  notifications:any
  constructor(
    private notificationService: NotificationService,
    private toasterMessageService: ToasterMessageService,
    public vcr: ViewContainerRef,
    ) { }

  ngOnInit() {
   this.getNotification()
    
  }

  deleteNotification(id: number){
    this.notificationService.deleteNotification(id).subscribe(data=>{
      this.toasterMessageService.success(this.vcr, "Notification Deleted !", "SUCCESS")
      this.getNotification()
    })
  }

  getNotification(){
    this.notificationService.getNotification().subscribe((data:any)=>{
      this.notifications=data
    })
  }

}
