/*
 * Copyright 2017 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Injectable} from '@angular/core';
import {KeycloakLoginOptions} from './keycloak';

// If using a local keycloak.js, uncomment this import.  With keycloak.js fetched
// from the server, you get a compile-time warning on use of the Keycloak()
// method below.  I'm not sure how to fix this, but it's certainly cleaner
// to get keycloak.js from the server.
// 
import * as Keycloak from './keycloak';
import { LogService } from '../service/log.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

export type KeycloakClient = Keycloak.KeycloakInstance;
type InitOptions = Keycloak.KeycloakInitOptions;

@Injectable()
export class KeycloakService {
    static keycloakAuth: KeycloakClient;
    // private url = 
    // static router: any;
    static url: any=environment.siteURL
    /**
     * Configure and initialize the Keycloak adapter.
     *
     * @param configOptions Optionally, a path to keycloak.json, or an object containing
     *                      url, realm, and clientId.
     * @param adapterOptions Optional initiaization options.  See javascript adapter docs
     *                       for details.
     * @returns {Promise<T>}
     */
    // static init(configOptions?: string|{}, initOptions?: InitOptions): Promise<any> {
    //     KeycloakService.keycloakAuth = Keycloak(configOptions);

    //     return new Promise((resolve, reject) => {
    //         KeycloakService.keycloakAuth.init(initOptions)
    //             .success(() => {
    //                 resolve();
    //             })
    //             .error((errorData: any) => {
    //                 reject(errorData);
    //             });
    //     });
    // }

    constructor(router :Router){}

    static init(configOptions?: string|{}, initOptions?: InitOptions): Promise<any> {

        KeycloakService.keycloakAuth = Keycloak(configOptions);
       
        return new Promise((resolve, reject) => {
            KeycloakService.keycloakAuth.init(initOptions)
                .success(() => {
                    KeycloakService.keycloakAuth = this.keycloakAuth;
                    if(KeycloakService.keycloakAuth.authenticated) {
                        LogService.check(KeycloakService.keycloakAuth.subject).then((checked) => {
                            if(checked == null) {
                              LogService.save(KeycloakService.keycloakAuth.subject).then((saved) => {
                                // LogService.setLoggedUserInfo(
                                //     saved['id'],
                                //     saved['referenceId'],
                                //     KeycloakService.auth.authz.tokenParsed.preferred_username,
                                //     KeycloakService.auth.authz.tokenParsed.name,
                                //     KeycloakService.keycloakAuth.authenticated);
                                console.log("respond"+saved)
                                resolve();
                              }).catch((error) => { alert('Opps...Something went wrong!!! \n[ Error: While login]')});
                            }
                            if(checked != null) {
                                console.log(checked)
                                console.log(KeycloakService.keycloakAuth.tokenParsed)
                                console.log(KeycloakService.keycloakAuth.authenticated)
                               var roles = KeycloakService.keycloakAuth.tokenParsed.realm_access.roles
                               

                               let name = KeycloakService.keycloakAuth.tokenParsed['given_name'] + " " + KeycloakService.keycloakAuth.tokenParsed['family_name']
                               console.log(name)
                               console.log(KeycloakService.keycloakAuth.tokenParsed.realm_access.roles)
                               roles.forEach(element=>{
                                  console.log("ROLES:" +element)
                                  if(element !=="ROLE_ADMIN"){
                                      if(element ==="ROLE_USER"){
                                        // alert("user")
                                        // window.location.href="www.stage.goglides.com/asdhasdk"
                                        window.location.href=this.url+"404"
                                      }
                                     
                                    //   window.location.href="http://localhost:4200/asdasdasdbasdasdt"
                                  }
                               })
                                LogService.setLoggedUserInfo(
                                    checked['id'],
                                    checked['referenceId'],
                                 KeycloakService.keycloakAuth.tokenParsed['given_name'],
                                   KeycloakService.keycloakAuth.tokenParsed['preferred_username'],
                                    KeycloakService.keycloakAuth.authenticated,
                                    KeycloakService.keycloakAuth.tokenParsed.realm_access.roles);
                                
                                resolve();
                            }
                          }).catch((error) => { alert('Opps...Something went wrong!!! \n[ Error: While login]')});;
                    } else { resolve(); }
                })
                .error((errorData: any) => {
                    reject(errorData);
                });
        });
    }
    
    /**
     * Expose the underlying Keycloak javascript adapter.
     */
    client(): KeycloakClient {
        return KeycloakService.keycloakAuth;
    }

    authenticated(): boolean {
        return KeycloakService.keycloakAuth.authenticated;
    }

    login(options?: KeycloakLoginOptions) {
        KeycloakService.keycloakAuth.login(options);
    }

    logout(redirectUri?: string) {
        KeycloakService.keycloakAuth.logout({redirectUri: redirectUri});
    }

    account() {
        KeycloakService.keycloakAuth.accountManagement();
    }
    
    authServerUrl(): string {
        return KeycloakService.keycloakAuth.authServerUrl;
    }
    
    realm(): string {
        return KeycloakService.keycloakAuth.realm;
    }

    getToken(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            if (KeycloakService.keycloakAuth.token) {
                KeycloakService.keycloakAuth
                    .updateToken(5)
                    .success(() => {
                        resolve(<string>KeycloakService.keycloakAuth.token);
                    })
                    .error(() => {
                        reject('Failed to refresh token');
                    });
            } else {
                reject('Not loggen in');
            }
        });
    }

    loadProfile(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
          if (KeycloakService.keycloakAuth.token) {
            KeycloakService.keycloakAuth
              .loadUserProfile()
              .success(data => {
                resolve(<any>data);
              })
              .error(() => {
                reject('Failed to load profile');
              });
          } else {
            reject('Not loggen in');
          }
        })
      }

      getRoles(){
       return KeycloakService.keycloakAuth.tokenParsed.realm_access.roles
      }
}
