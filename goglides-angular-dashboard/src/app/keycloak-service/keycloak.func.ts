import { environment } from "environments/environment";

export class User {
    private apiUrl = environment.Url
    

    checkUser(ref: string){
        var data = {ref:ref};
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", this.apiUrl + "user/checkUser", true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
            xhr.onload = () => {if(xhr.status === 200){resolve((JSON.parse(xhr.responseText))['user']);}else{resolve(null);}};
            xhr.onerror = () => reject(xhr.statusText);
            xhr.send(JSON.stringify(data));
        });
    }
  
    saveUser(ref: string){
        var data = {ref:ref};
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", this.apiUrl + "user/saveUser", true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
            xhr.onload = () => {if(xhr.status === 200){resolve((JSON.parse(xhr.responseText))['user']);}else{resolve(null);}};
            xhr.onerror = () => reject(xhr.statusText);
            xhr.send(JSON.stringify(data));
        });
    }
  
    getUser(id: number) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", this.apiUrl + "user/getUser?id=" + id, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
            xhr.onload = () => {if(xhr.status === 200){resolve((JSON.parse(xhr.responseText))['user']);}else{resolve(null);}};
            xhr.onerror = () => reject(xhr.statusText);
            xhr.send();
        });
    }
}