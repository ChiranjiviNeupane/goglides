import { Component, OnInit } from '@angular/core';
import { RangePipe } from 'ngx-pipes';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [RangePipe]
})
export class HomeComponent implements OnInit {
  items
  constructor(private ragePipe :RangePipe) { }

  ngOnInit() {
    this.items = this.ragePipe.transform(1, 5)
  }

}
