package com.goglides

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class WishListServiceSpec extends Specification {

    WishListService wishListService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new WishList(...).save(flush: true, failOnError: true)
        //new WishList(...).save(flush: true, failOnError: true)
        //WishList wishList = new WishList(...).save(flush: true, failOnError: true)
        //new WishList(...).save(flush: true, failOnError: true)
        //new WishList(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //wishList.id
    }

    void "test get"() {
        setupData()

        expect:
        wishListService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<WishList> wishListList = wishListService.list(max: 2, offset: 2)

        then:
        wishListList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        wishListService.count() == 5
    }

    void "test delete"() {
        Long wishListId = setupData()

        expect:
        wishListService.count() == 5

        when:
        wishListService.delete(wishListId)
        sessionFactory.currentSession.flush()

        then:
        wishListService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        WishList wishList = new WishList()
        wishListService.save(wishList)

        then:
        wishList.id != null
    }
}
