package com.goglides

import grails.gorm.services.Service

@Service(BusinessInsurance)
interface BusinessInsuranceService {

    BusinessInsurance get(Serializable id)

    List<BusinessInsurance> list(Map args)

    Long count()

    void delete(Serializable id)

    BusinessInsurance save(BusinessInsurance businessInsurance)

}