package com.goglides

import com.goglides.common.CommonStatus

class ReviewApiService {

    UserService userService
    def countTotal(listing , star){
        def cnt = Review.withCriteria() {
            createAlias("listing", "l")
            eq("status", "COMPLETED")
            eq("l.id", listing)
            eq("star", star)
            
            projections {
                countDistinct("id")
            }
        }
        return cnt
    }


    def multimedia(Long id){
        def mulId = Review.get(id)
        def mul = []
        def multimedia = Multimedia.createCriteria().list(){
            eq("review",mulId)
        }
        multimedia.each{
            mul.add(
                'id':it?.id,
                'url':it?.fileUrl
            )
        }
        return mul
    }


    def count(listingId){
        def total = [:]
        def oneStar =countTotal(listingId,1)
        def twoStar=countTotal(listingId,2)
        def threeStar=countTotal(listingId,3)
        def fourStar=countTotal(listingId,4)
        def fiveStar=countTotal(listingId,5)
        def cntTotal = Review.withCriteria() {
            createAlias("listing", "l")
            eq("l.id", listingId)
            eq("status", "COMPLETED")
            order("id", "desc")
            projections {
                countDistinct("id")
            }
        }
        if(!cntTotal[0])
        {
            total = ['total':cntTotal[0],
                    'average':0,
                    'one':oneStar[0],
                    'two':twoStar[0],
                    "three":threeStar[0],
                    "four":fourStar[0],
                    "five":fiveStar[0]
            ]
        return total
        }
        else{
            def add = Review.withCriteria() {
                createAlias("listing", "l")
                eq("l.id", listingId)
                eq("status", "COMPLETED")
                projections {
                    sum("star")
                }
            }   
            def average = [add[0]/cntTotal[0]]    

            total = ['total':cntTotal[0],
                    'average':average[0],
                    'one':oneStar[0],
                    'two':twoStar[0],
                    "three":threeStar[0],
                    "four":fourStar[0],
                    "five":fiveStar[0]
            ]
            return total
        }
    }




    def countAverageTotal(listingId){
        def total = []
        def cntTotal = Review.withCriteria() {
            createAlias("listing", "l")
            eq("l.id", listingId)
            eq("status", "COMPLETED")
            order("id", "desc")
            projections {
                countDistinct("id")
            }
        }
        if(!cntTotal[0])
        {
            total = ['total':cntTotal[0],
                    'average':0
            ]
        return total
        }
        else{
            def add = Review.withCriteria() {
                createAlias("listing", "l")
                eq("l.id", listingId)
                eq("status", "COMPLETED")
                projections {
                    sum("star")
                }
            }   
            def average = [add[0]/cntTotal[0]]    

            total = ['total':cntTotal[0],
                    'average':average[0]
            ]
            return total
        }
    }

    def userById(id) {
        def user = User.get(id)
        return([user: userService.mapUser(user)])
    }
}