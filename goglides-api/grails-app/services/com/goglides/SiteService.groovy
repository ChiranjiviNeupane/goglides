package com.goglides

import com.goglides.common.CommonStatus


class SiteService {

    def category() {
        def result = []
        def categoryList = Category.createCriteria().list {
            eq('isVisible', 1)
        }
        if (categoryList) {
            categoryList.each { cat ->
                def count = Listing.countByCategoryAndIsVisibleAndIsVerifiedAndStatus(cat, 1, 1, CommonStatus.STATUS_PUBLISH.getStatus()) ?: 0
                if (count > 0) {
                    result.add(
                            'referenceId': cat?.referenceId,
                            'code': cat?.code,
                            'title': cat?.title,
                            'slug': cat?.slug,
                            'count': count
                    )
                }
            }
        }
        return result
    }
}