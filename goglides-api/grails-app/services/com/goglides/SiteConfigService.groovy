package com.goglides

class SiteConfigService {

    static siteUrl() {
        return SiteSetting.findBySlug('site-url')?.value ?: ''
    }

    def siteName() {
        return SiteSetting.findBySlug('site-name')?.value ?: ''
    }

    def tagline() {
        return SiteSetting.findBySlug('tagline')?.value ?: ''
    }

    def adminEmail() {
        return SiteSetting.findBySlug('admin-email')?.value ?: ''
    }

    def address1() {
        return SiteSetting.findBySlug('address1')?.value ?: ''
    }

    def address2() {
        return SiteSetting.findBySlug('address2')?.value ?: ''
    }

    def phone1() {
        return SiteSetting.findBySlug('phone1')?.value ?: ''
    }

    def phone2() {
        return SiteSetting.findBySlug('phone2')?.value ?: ''
    }

    def fax1() {
        return SiteSetting.findBySlug('fax1')?.value ?: ''
    }

    def fax2() {
        return SiteSetting.findBySlug('fax2')?.value ?: ''
    }

    def copyright() {
        return SiteSetting.findBySlug('copyright')?.value ?: ''
    }

    def facebook() {
        return SiteSetting.findBySlug('facebook')?.value ?: ''
    }

    def twitter() {
        return SiteSetting.findBySlug('twitter')?.value ?: ''
    }

    def instagram() {
        return SiteSetting.findBySlug('instagram')?.value ?: ''
    }

    def googlePlus() {
        return SiteSetting.findBySlug('google-plus')?.value ?: ''
    }

    def linkedin() {
        return SiteSetting.findBySlug('linkedin')?.value ?: ''
    }

    def googleMap() {
        return SiteSetting.findBySlug('google-map')?.value ?: ''
    }

    def googleAPIKey() {
        return SiteSetting.findBySlug('google-api-key')?.value ?: ''
    }

    def businessHour() {
        return SiteSetting.findBySlug('business-hour')?.value ?: ''
    }

    def siteMode() {
        return SiteSetting.findBySlug('site-mode')?.value ?: ''
    }

    def metaTitle() {
        return SiteSetting.findBySlug('meta-title')?.value ?: ''
    }

    def metaKeyword() {
        return SiteSetting.findBySlug('meta-keyword')?.value ?: ''
    }

    def metaDescription() {
        return SiteSetting.findBySlug('meta-description')?.value ?: ''
    }

    static keycloakAuthServerUrl() {
        return SiteSetting.findBySlug('keycloak-auth-server-url')?.value ?: ''
    }

    static keycloakRealmID() {
        return SiteSetting.findBySlug('keycloak-realm-id')?.value ?: ''
    }

    static keycloakClientID() {
        return SiteSetting.findBySlug('keycloak-client-id')?.value ?: ''
    }

    static keycloakClientSecret() {
        return SiteSetting.findBySlug('keycloak-client-secret')?.value ?: ''
    }

    static keycloakUsername() {
        return SiteSetting.findBySlug('keycloak-username')?.value ?: ''
    }

    static keycloakPassword() {
        return SiteSetting.findBySlug('keycloak-password')?.value ?: ''
    }

    static paypalMode() {
        return SiteSetting.findBySlug('paypal-mode')?.value ?: 'sandbox'
    }

    static paypalClientId() {
        def paypalMode = paypalMode()
        if (paypalMode == 'sandbox')
            return SiteSetting.findBySlug('paypal-client-id-sandbox')?.value ?: ''
        else
            return SiteSetting.findBySlug('paypal-client-id')?.value ?: ''
    }


    static paypalClientSecret() {
        def paypalMode = paypalMode()
        if (paypalMode == 'sandbox')
            return SiteSetting.findBySlug('paypal-client-secret-sandbox')?.value ?: ''
        else
            return SiteSetting.findBySlug('paypal-client-secret')?.value ?: ''
    }

    static paypalEndpoint() {
        def paypalMode = paypalMode()
        if (paypalMode == 'sandbox')
            return SiteSetting.findBySlug('paypal-endpoint-sandbox')?.value ?: ''
        else
            return SiteSetting.findBySlug('paypal-endpoint')?.value ?: ''
    }

    static paypalCurrencyCode() {
        return SiteSetting.findBySlug('paypal-currency-code')?.value ?: ''
    }

    static paypalReturnUrl() {
        return SiteSetting.findBySlug('paypal-return-url')?.value ?: ''
    }

    static paypalCancelUrl() {
        return SiteSetting.findBySlug('paypal-cancel-url')?.value ?: ''
    }

    static khaltiEndpoint() {
        return SiteSetting.findBySlug('khalti-endpoint-url')?.value ?: ''
    }

    static khaltiMode() {
        return SiteSetting.findBySlug('khalti-mode').value ?: ''
    }

    static khaltiSecretKey() {
        def khaltiMode = khaltiMode()
        if (khaltiMode == 'test') {
            return SiteSetting.findBySlug('khalti-secret-key-test').value ?: ''
        } else {
            return SiteSetting.findBySlug('khalti-secret-key').value ?: ''
        }
    }

    static khaltiPublicKey() {
        def khaltiMode = khaltiMode()
        if (khaltiMode == 'test') {
            return SiteSetting.findBySlug('khalti-public-key-test').value ?: ''
        } else {
            return SiteSetting.findBySlug('khalti-public-key').value ?: ''
        }
    }

}