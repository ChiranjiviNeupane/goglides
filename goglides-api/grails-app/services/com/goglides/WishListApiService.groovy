package com.goglides

import com.goglides.common.CommonStatus
import grails.gorm.transactions.Transactional

@Transactional
class WishListApiService {
    ListingApiService listingApiService
    BusinessApiService businessApiService
   def myWishList(userId, max, offset){
       def myWishList = []
       def result = [:]
       def user = User.findById(userId)
       def data = WishList.createCriteria().list (max: max, offset: offset) {
           eq("user", user)
           order("id", "desc")
           projections {
               property('listing')
           }
       }
       data.each {
           d->
            myWishList.add(
                    'referenceId': d?.referenceId,
                    'code': d?.code,
                    'featureImageUrl': listingApiService?.listingFeatureImage(d),
                    'title': d?.title,
                    'slug': d?.slug,
                    'address': BusinessDirectory?.findById(d?.businessDirectory?.id)?.address,
                    'city': BusinessDirectory?.findById(d?.businessDirectory?.id)?.city,
                    'country': BusinessDirectory?.findById(d?.businessDirectory?.id)?.country,
                    'tripCode': d?.tripCode,
                    'metaTitle': d?.metaTitle,
                    'metaKeyword': d?.metaKeyword,
                    'metaDescription': d?.metaDescription,
                    'type': ListingPrice?.findByListing(d)?.type,
                    'duration': ListingPrice?.findByListing(d)?.duration,
                    'price':listingApiService.rate(d)
            )
       }
       result['myWishList'] = myWishList
       result['count'] = WishList.countByUser(user)
       return result
   }
}
