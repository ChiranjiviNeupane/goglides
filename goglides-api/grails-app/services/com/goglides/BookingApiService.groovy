package com.goglides

import com.goglides.common.CommonStatus
import com.goglides.common.BookingStatus
import grails.validation.ValidationException

class BookingApiService {
    ListingApiService listingApiService

    def save(bookingInfo) {
        def result = []

        def listingPrice = ListingPrice.findByReferenceIdAndStatus(bookingInfo?.referenceId, CommonStatus.STATUS_PUBLISH.getStatus())
        if (!listingPrice) {
            result.add('Invalid cart data.')
            return result
        }
        def booking = new Booking()
        booking.businessDirectory = BusinessDirectory.get(listingPrice?.businessDirectory?.id)
        booking.listing = Listing.get(listingPrice?.listing?.id)
        booking.user = User.get(bookingInfo?.user)
        booking.bookingDate = Date.parse("yyyy-MM-dd", bookingInfo?.bookingDate)
        booking.type = bookingInfo?.type ?: ''
        booking.schedule = bookingInfo?.schedule ?: ''
        booking.duration = bookingInfo?.duration ?: ''
        booking.currencyCode = bookingInfo?.currencyCode ?: ''
        booking.categories = bookingInfo?.categories ?: ''
        booking.bookingStatus = BookingStatus.STATUS_PENDING.getStatus()
        booking.paxTotalCount = bookingInfo?.paxTotalCount ?: 0
        booking.paxTotal = bookingInfo?.paxTotal ?: 0
        booking.addonTotal = 0
        booking.total = bookingInfo?.total ?: 0
        booking.grandTotal = bookingInfo?.grandTotal ?: 0
        booking.cart = bookingInfo?.paxInfo ?: ""
        booking.clientName = bookingInfo?.clientName ?: ""
        booking.clientContact = bookingInfo?.clientContact ?: ""
        booking.clientEmail = bookingInfo?.clientEmail ?: ""
        booking.bookingMode = 'SELF'
        if (booking == null) {
            result.add('Cart cannot be empty.')
            return result
        }

        try {
            booking.save(flush: true)
            if (bookingInfo?.paxInfo) {
                bookingInfo?.paxInfo.each { item ->
                    saveBookingItem(booking, item, item?.ageGroup, listingPrice)
                }
            }
            result.add('referenceId': booking?.referenceId)
        } catch (ValidationException e) {
            return booking.errors
        }

        return result
    }

    def saveBookingItem(booking, item, ageGroup, listingPrice) {
        def rate
        switch (ageGroup) {
        case 'ADT':
            rate = listingPrice?.retailRate
            break
        case 'SNR':
            rate = listingPrice?.seniorRetailRate
            break
        case 'CLD':
            rate = listingPrice?.childRetailRate
            break
        case 'INF':
            rate = listingPrice?.infantRetailRate
        }
        def bookingItem = new BookingItem()
        bookingItem.businessDirectory = BusinessDirectory.get(booking?.businessDirectory?.id)
        bookingItem.listing = Listing.get(booking?.listing?.id)
        bookingItem.listingPrice = ListingPrice.get(listingPrice?.id)
        bookingItem.booking = Booking.get(booking?.id)
        bookingItem.user = User.get(booking?.user?.id)
        bookingItem.ageGroup = ageGroup
        bookingItem.retailRate = rate ?: listingPrice?.retailRate
        bookingItem.bookingDate = Date.parse("yyyy-MM-dd", item?.bookingDate)
        bookingItem.category = item?.category
        bookingItem.type = item?.type
        bookingItem.schedule = item?.schedule
        bookingItem.duration = item?.duration
        bookingItem.title = item?.title
        bookingItem.fullName = item?.fullName
        bookingItem.contactNo = item?.contactNo
        bookingItem.email = item?.email
        bookingItem.nationality = item?.nationality
        bookingItem.identificationType = item?.identificationType
        bookingItem.identificationNo = item?.identificationNo
        bookingItem.weight = item?.weight
        if (!bookingItem.validate()) {
            bookingItem.errors.allErrors.each {
            }
        }
        bookingItem.save(flush: true)
    }

    def view(referenceId, user) {
        def result = []
        def booking = []
        def listing = []
        def paxList = []
        def addonList = []
        def data = Booking.findByReferenceIdAndUser(referenceId, User.get(user))
        if (data) {
            listing.add(
                    'referenceId': data?.listing?.referenceId,
                    'slug': data?.listing?.slug
            )
            booking.add(
                    'referenceId': data?.referenceId,
                    'code': data?.code,
                    'invoiceNo': data?.invoiceNo,
                    'bookingDate': data?.bookingDate,
                    'type': data?.type,
                    'schedule': data?.schedule,
                    'duration': data?.duration,
                    'bookingMode': data?.bookingMode,
                    'paymentMode': data?.paymentMode,
                    'paymentStatus': data?.paymentStatus,
                    'bookingStatus': data?.bookingStatus,
                    'currencyCode': data?.currencyCode,
                    'currencyRate': data?.currencyRate,
                    'paxTotalCount': data?.paxTotalCount,
                    'paxTotal': data?.paxTotal ?: 0,
                    'addonTotal': data?.addonTotal ?: 0,
                    'total': data?.total ?: 0,
                    'grandTotal': data?.grandTotal ?: 0,
                    'emergencyContactName': data?.emergencyContactName,
                    'emergencyContactNumber': data?.emergencyContactNumber,
                    'clientName': data?.clientName,
                    'clientContact': data?.clientContact,
                    'clientEmail': data?.clientEmail,
                    'comment': data?.comment,
                     'createdAt': data?.createdAt,
                    'pickup': data?.pickup,
                    'dropOff': data?.dropOff,
                    'transactionId': data?.transactionId,
                    'payerId': data?.payerId,
                    'payerToken': data?.payerToken,
                    'pickupAt': data?.pickupAt,
                    'cart': data?.cart,
                    'addonsCart': data?.addonsCart
            )
            def items = BookingItem.createCriteria().list {
                eq('booking', data)
            }
            if (items) {
                items.each { item ->
                    if (item?.addAddon) {
                        def addons = BookingItemAddon.createCriteria().list {
                            eq('bookingItem', item)
                        }
                        if (addons) {
                            addons.each {
                                addonList.add(
                                        'referenceId': it?.referenceId,
                                        'title': it?.listingAddon?.title,
                                        'retailRate': it?.retailRate
                                )
                            }
                        }
                    }
                    paxList.add(
                            'referenceId': item?.referenceId,
                            'bookingDate': item?.bookingDate,
                            'schedule': item?.schedule,
                            'duration': item?.duration,
                            'type': item?.type,
                            'category': item?.category,
                            'ageGroup': item?.ageGroup,
                            'retailRate': item?.retailRate,
                            'ticketNo': item?.ticketNo,
                            'title': item?.title,
                            'fullName': item?.fullName,
                            'contactNo': item?.contactNo,
                            'email': item?.email,
                            'nationality': item?.nationality,
                            'identificationType': item?.identificationType,
                            'identificationNo': item?.identificationNo,
                            'weight': item?.weight,
                            'addAddon': item?.addAddon,
                            'addOn': addonList
                    )
                    addonList = []
                }
            }
        }
        result.add('listing': listing, 'booking': booking, 'paxList': paxList)
        return result
    }

    def saveAddons(bookingInfo) {
        def item = BookingItem.findByReferenceId(bookingInfo.referenceId)
        def booking = Booking.get(item?.bookingId)
        def addonList = []
        if (bookingInfo?.addAddon) {
            bookingInfo?.addOnList.each { addon ->
                if (!BookingItemAddon.findByListingAddonAndBookingItem(ListingAddon.findByReferenceId(addon?.referenceId), item)) {
                    def paxAddon = new BookingItemAddon()
                    paxAddon.user = User.get(bookingInfo?.user)
                    paxAddon.businessDirectory = BusinessDirectory.get(item?.businessDirectory?.id)
                    paxAddon.listing = Listing.get(item?.listing?.id)
                    paxAddon.listingAddon = ListingAddon.findByReferenceId(addon?.referenceId)
                    paxAddon.booking = booking
                    paxAddon.bookingItem = item
                    paxAddon.retailRate = addon.retailRate ?: ListingAddon.findByReferenceId(addon?.referenceId)?.retailRate
                    if (!paxAddon.validate()) {
                        paxAddon.errors.allErrors.each {
                            //println it
                        }
                    }
                    paxAddon.save(flush: true)
                    booking.addonTotal = booking?.addonTotal + paxAddon.retailRate
                    booking.total = booking?.total + paxAddon.retailRate
                    booking.grandTotal = booking?.grandTotal + paxAddon.retailRate
                    booking.save(flush: true)
                }
            }
            item.addAddon = bookingInfo?.addAddon
            item.save(flush: true)
            def addons = BookingItemAddon.createCriteria().list {
                eq('bookingItem', item)
                eq('booking', booking)
            }
            addons.each {
                addonList.add('referenceId': it?.referenceId, 'title': it?.listingAddon?.title, 'rate': it?.retailRate, 'fullName': it?.bookingItem?.fullName)
            }
        }

        return addonList
    }

    def removeAddon(bookingInfo) {
        def result = []
        def bookingItemAddon = BookingItemAddon.findByReferenceId(bookingInfo?.referenceId)
        def booking = Booking.get(bookingItemAddon?.bookingId)
        def bookingItem = BookingItem.get(bookingItemAddon?.bookingItemId)
        if (bookingItemAddon) {
            booking.addonTotal = booking?.addonTotal - bookingItemAddon?.retailRate
            booking.total = booking?.total - bookingItemAddon?.retailRate
            booking?.grandTotal = booking?.grandTotal - bookingItemAddon?.retailRate
            booking.save()

            bookingItemAddon.delete(flush: true)
            def count = BookingItemAddon.createCriteria().list {
                eq('booking', booking)
                eq('bookingItem', bookingItem)
                projections {
                    count()
                }
            }
            if (!count[0]) {
                bookingItem.addAddon = 0
                bookingItem.save(flush: true)
            }
            result.add('Deleted')
        } else {
            result.add('Something went wrong')
        }
        return result
    }

    def removeAllAddon(bookingInfo) {
        def result = []
        def item = BookingItem.findByReferenceId(bookingInfo.referenceId)
        def booking = Booking.get(item?.bookingId)
        if (item) {
            def addonList = BookingItemAddon.createCriteria().list {
                eq('booking', booking)
            }
            if (addonList) {
                addonList.each {
                    booking.addonTotal = booking?.addonTotal - it?.retailRate
                    booking.total = booking?.total - it?.retailRate
                    booking?.grandTotal = booking?.grandTotal - it?.retailRate
                    booking.save(flush: true)

                    it.delete(flush: true)
                    def count = BookingItemAddon.createCriteria().list {
                        eq('booking', booking)
                        eq('bookingItem', item)
                        projections {
                            count()
                        }
                    }
                    if (!count[0]) {
                        item.addAddon = 0
                        item.save(flush: true)
                    }
                }
            }
        }
        return result
    }

    def saveBooking(bookingInfo) {
        def result = []
        def booking = Booking.findByReferenceId(bookingInfo?.referenceId)
        booking.properties = bookingInfo
        if (booking.save(flush: true)) {
            result.add('booking': booking?.referenceId)
        }
        return result
    }

    def myBooking(id) {
        def result = [:]
        def myBookingList = []
        def user = User.get(id)
        def bookings = Booking.findAllByUserAndBookingMode(user, 'SELF')
        bookings.each {
            booking ->
            def listingTitle = Listing.findById(booking?.listing?.id)?.title
            def businessAddress = BusinessDirectory.findById(booking?.businessDirectory?.id)
            def bookingItem = BookingItem.findByBooking(booking)
            myBookingList.add(
                bookingDate: booking?.bookingDate,
                clientContact: booking?.clientContact,
                code: booking?.code,
                total: booking?.total,
                paxTotal: booking?.paxTotal,
                grandTotal: booking?.grandTotal,
                cart: booking?.cart,
                bookingStatus: booking?.bookingStatus,
                referenceId: booking?.referenceId,
                clientEmail: booking?.clientEmail,
                addonsCart: booking?.addonsCart,
                categories: booking?.categories,
                paxTotalCount: booking?.paxTotalCount,
                clientName: booking?.clientName,
                paymentStatus: booking?.paymentStatus,
                lisitngTitle: listingTitle,
                businessAddress: businessAddress?.address,
                businessCity: businessAddress?.city,
                businessStreet: businessAddress?.street,
                schedule: bookingItem?.schedule,
                type: bookingItem?.type,
                duration: bookingItem?.duration,
            )
        }
        return myBookingList
    }

}