package com.goglides

import com.goglides.common.PaymentStatus
import com.paypal.api.payments.Details
import com.paypal.base.Constants
import groovy.json.JsonSlurper
import com.goglides.common.BookingStatus

class PaymentApiService {

    def paypalService

    def approvePaypalPayment(def booking) {
        String clientId = SiteConfigService.paypalClientId()
        String clientSecret = SiteConfigService.paypalClientSecret()
        String endpoint = SiteConfigService.paypalEndpoint()
        Map sdkConfig = [(Constants.CLIENT_ID)    : clientId,
                         (Constants.CLIENT_SECRET): clientSecret,
                         (Constants.ENDPOINT)     : endpoint]
        def accessToken = paypalService.getAccessToken(clientId, clientSecret, sdkConfig)
        def apiContext = paypalService.getAPIContext(accessToken, sdkConfig)


        String total = booking?.grandTotal as BigDecimal

        def details = new Details()
        details.setSubtotal(total)
        def currencyCode = SiteConfigService.paypalCurrencyCode()
        def amount = paypalService.createAmount(currency: currencyCode, total: total, details: details)
        def description = booking?.listing?.title + ' ' + currencyCode + ' ' + total + ' REF. CODE:' + booking?.referenceId

        def transaction = paypalService.createTransaction(amount: amount, description: description, item_number: booking?.referenceId, details: details)
        def transactions = [transaction]

        def payer = paypalService.createPayer(paymentMethod: 'paypal')
        def cancelUrl = SiteConfigService.paypalCancelUrl() + '/' + booking?.referenceId
        def returnUrl = SiteConfigService.paypalReturnUrl()

        def redirectUrls = paypalService.createRedirectUrls(cancelUrl: cancelUrl, returnUrl: returnUrl)

        def payment
        try {
            payment = paypalService.createPayment(
                    payer: payer, intent: 'sale',
                    transactionList: transactions,
                    redirectUrls: redirectUrls,
                    apiContext: apiContext)
        }
        catch (e) {
            log.error("Payment Error : " + e.getMessage())
            return [responseStatus: 'error', message: e.getMessage()]
        }

        def approvalUrl = ""
        def retUrl = ""
        for (links in payment?.links) {
            if (links?.rel == "approval_url") {
                approvalUrl = links.href
            }
            if (links?.rel == "return_url") {
                retUrl = links.href
            }
        }

        return [responseStatus: 'success', url: approvalUrl, method: 'POST']
    }

    def executePaypalPayment(params) {
        String clientId = SiteConfigService.paypalClientId()
        String clientSecret = SiteConfigService.paypalClientSecret()
        String endpoint = SiteConfigService.paypalEndpoint()
        Map sdkConfig = [:]
        sdkConfig[Constants.CLIENT_ID] = clientId
        sdkConfig[Constants.CLIENT_SECRET] = clientSecret
        sdkConfig[Constants.ENDPOINT] = endpoint
        def accessToken = paypalService.getAccessToken(clientId, clientSecret, sdkConfig)
        def apiContext = paypalService.getAPIContext(accessToken, sdkConfig)
        def paypalPayment = paypalService.createPaymentExecution(paymentId: params.paymentId, payerId: params.PayerID, apiContext)
        def map = new JsonSlurper().parseText(paypalPayment.toString())
        def itemDesc = map.transactions.description.toString()
        def (value1, value2) = itemDesc.tokenize(':')
        def referenceId = value2.replace(']', '')
        def state = map.state.toString()
        def transactionId = map.id.toString()

        try {
            if (state == 'approved') {
                def booking = Booking.findByReferenceId(referenceId)
                booking.paymentStatus = PaymentStatus.STATUS_PAID.getStatus()
                booking.bookingStatus = BookingStatus.STATUS_CONFIRMED.getStatus()
                booking.transactionId = transactionId
                if (booking.save(flush: true)) {
                    issueTicket(booking)
                }

                return [responseStatus: 'success', booking: booking]
            }
        } catch (Exception e) {
            log.error "Error: ${e.message}", e
        }
        return [responseStatus: 'error']
    }

    def issueTicket(booking) {
        def items = BookingItem.createCriteria().list {
            eq("booking", booking)
        }

        if (items) {
            items.each { item ->
                if (!item.ticketNo) {
                    def ticketNo = generateTicket()
                    item.ticketNo = ticketNo
                    item.save(flush: true)
                }
            }
        }
        return
    }

    def generateTicket() {
        def ticket
        def ticketNo = BookingItem.createCriteria().get {
            projections {
                max "ticketNo"
            }
        }
        println(ticketNo);
        if (!ticketNo) {
            ticket = 1000000001
        } else {
            ticketNo
            ticket = ticketNo.toInteger() + 1
        }

        return ticket
    }


}