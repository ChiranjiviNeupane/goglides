package com.goglides

import grails.gorm.services.Service

@Service(BusinessContactPerson)
interface BusinessContactPersonService {

    BusinessContactPerson get(Serializable id)

    List<BusinessContactPerson> list(Map args)

    Long count()

    void delete(Serializable id)

    BusinessContactPerson save(BusinessContactPerson businessContactPerson)

}