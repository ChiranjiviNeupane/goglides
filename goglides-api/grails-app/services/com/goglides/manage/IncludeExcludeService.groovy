package com.goglides.manage

import com.goglides.IncludeExclude
import grails.gorm.services.Service

@Service(IncludeExclude)
interface IncludeExcludeService {

    IncludeExclude get(Serializable id)

    List<IncludeExclude> list(Map args)

    Long count()

    void delete(Serializable id)

    IncludeExclude save(IncludeExclude includeExclude)

}