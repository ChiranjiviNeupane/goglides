package com.goglides.manage

import com.goglides.Listing
import grails.gorm.services.Service

@Service(Listing)
interface ListingService {

    Listing get(Serializable id)

    List<Listing> list(Map args)

    Long count()

    void delete(Serializable id)

    Listing save(Listing listing)

}