package com.goglides.manage

import com.goglides.ListingTransportRoute
import grails.gorm.services.Service

@Service(ListingTransportRoute)
interface ListingTransportRouteService {

    ListingTransportRoute get(Serializable id)

    List<ListingTransportRoute> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingTransportRoute save(ListingTransportRoute listingTransportRoute)

}