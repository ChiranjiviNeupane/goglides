package com.goglides.manage

import com.goglides.BusinessCertificate
import grails.gorm.services.Service

@Service(BusinessCertificate)
interface BusinessCertificateService {

    BusinessCertificate get(Serializable id)

    List<BusinessCertificate> list(Map args)

    Long count()

    void delete(Serializable id)

    BusinessCertificate save(BusinessCertificate businessCertificate)

}