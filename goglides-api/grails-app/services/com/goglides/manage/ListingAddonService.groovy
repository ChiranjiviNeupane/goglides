package com.goglides.manage

import com.goglides.ListingAddon
import grails.gorm.services.Service

@Service(ListingAddon)
interface ListingAddonService {

    ListingAddon get(Serializable id)

    List<ListingAddon> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingAddon save(ListingAddon listingAddon)

}