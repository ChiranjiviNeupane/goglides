package com.goglides.manage

import com.goglides.ListingPrice
import grails.gorm.services.Service

@Service(ListingPrice)
interface ListingPriceService {

    ListingPrice get(Serializable id)

    List<ListingPrice> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingPrice save(ListingPrice listingPrice)

}