package com.goglides.manage

import com.goglides.BusinessStaff
import grails.gorm.services.Service

@Service(BusinessStaff)
interface BusinessStaffService {

    BusinessStaff get(Serializable id)

    List<BusinessStaff> list(Map args)

    Long count()

    void delete(Serializable id)

    BusinessStaff save(BusinessStaff businessStaff)

}