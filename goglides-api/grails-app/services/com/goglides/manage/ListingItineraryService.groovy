package com.goglides.manage

import com.goglides.ListingItinerary
import grails.gorm.services.Service

@Service(ListingItinerary)
interface ListingItineraryService {

    ListingItinerary get(Serializable id)

    List<ListingItinerary> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingItinerary save(ListingItinerary listingItinerary)

}