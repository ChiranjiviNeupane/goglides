package com.goglides.manage

import com.goglides.Forex
import grails.gorm.services.Service

@Service(Forex)
interface ForexService {

    Forex get(Serializable id)

    List<Forex> list(Map args)

    Long count()

    void delete(Serializable id)

    Forex save(Forex forex)

}