package com.goglides.manage

import com.goglides.SiteSetting
import grails.gorm.services.Service

@Service(SiteSetting)
interface SiteSettingService {

    SiteSetting get(Serializable id)

    List<SiteSetting> list(Map args)

    Long count()

    void delete(Serializable id)

    SiteSetting save(SiteSetting siteSetting)

}