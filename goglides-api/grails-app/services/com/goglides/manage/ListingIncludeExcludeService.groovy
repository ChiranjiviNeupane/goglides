package com.goglides.manage

import com.goglides.ListingIncludeExclude
import grails.gorm.services.Service

@Service(ListingIncludeExclude)
interface ListingIncludeExcludeService {

    ListingIncludeExclude get(Serializable id)

    List<ListingIncludeExclude> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingIncludeExclude save(ListingIncludeExclude listingIncludeExclude)

}