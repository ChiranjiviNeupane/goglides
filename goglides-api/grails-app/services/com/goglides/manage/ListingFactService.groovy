package com.goglides.manage

import com.goglides.ListingFact
import grails.gorm.services.Service

@Service(ListingFact)
interface ListingFactService {

    ListingFact get(Serializable id)

    List<ListingFact> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingFact save(ListingFact listingFact)

}