package com.goglides.manage

import com.goglides.ListingFaq
import grails.gorm.services.Service

@Service(ListingFaq)
interface ListingFaqService {

    ListingFaq get(Serializable id)

    List<ListingFaq> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingFaq save(ListingFaq listingFaq)

}