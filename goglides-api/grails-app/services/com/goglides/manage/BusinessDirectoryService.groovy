package com.goglides.manage

import com.goglides.BusinessDirectory
import grails.gorm.services.Service

@Service(BusinessDirectory)
interface BusinessDirectoryService {

    BusinessDirectory get(Serializable id)

    List<BusinessDirectory> list(Map args)

    Long count()

    void delete(Serializable id)

    BusinessDirectory save(BusinessDirectory businessDirectory)

}