package com.goglides.manage

import com.goglides.ListingMultimedia
import grails.gorm.services.Service

@Service(ListingMultimedia)
interface ListingMultimediaService {

    ListingMultimedia get(Serializable id)

    List<ListingMultimedia> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingMultimedia save(ListingMultimedia listingMultimedia)

}