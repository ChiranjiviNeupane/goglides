package com.goglides.manage

import com.goglides.ListingChecklist
import grails.gorm.services.Service

@Service(ListingChecklist)
interface ListingChecklistService {

    ListingChecklist get(Serializable id)

    List<ListingChecklist> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingChecklist save(ListingChecklist listingChecklist)

}