package com.goglides.manage

import com.goglides.ListingDestination
import grails.gorm.services.Service

@Service(ListingDestination)
interface ListingDestinationService {

    ListingDestination get(Serializable id)

    List<ListingDestination> list(Map args)

    Long count()

    void delete(Serializable id)

    ListingDestination save(ListingDestination listingDestination)

}