package com.goglides

import grails.gorm.transactions.Transactional

@Transactional
class ForexAPIService {

    def forex() {
        def forex = []
        def forexData = Forex.createCriteria().list {
            eq('status', "PUBLISH")
        }

        forexData.each {
            f ->
                forex.add(
                    countryCode: f?.countryCode,
                    currencyCode: f?.currencyCode,
                )
        }
        return forex
    }

    def getRate(currencyCode){
        def result = Forex.findByCurrencyCode(currencyCode.toString())
        return result
    }
}
