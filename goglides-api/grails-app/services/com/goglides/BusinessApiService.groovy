package com.goglides

import com.goglides.common.CommonStatus
import grails.gorm.transactions.Transactional

@Transactional
class BusinessApiService {

    ListingApiService listingApiService
    ReviewApiService reviewApiService

    def getBuisness(slug, id) {
        def result = [:]
        def businessInfo = [:]
        def businessListing = []
        def miltimedia = []
        def totalAverageRating = 0
        def countList = 0
        def averageRating
        def totalNumberRating = 0
        def rating = [:]

        def business = BusinessDirectory.findBySlug(slug)
        if (business)
            businessInfo=[
                    businessName: business?.businessName,
                    businessType: business?.businessType,
                    logoUrl: business?.logoUrl,
                    email: business?.email,
                    website: business?.website,
                    description: business?.description,
                    phone1: business?.phone1,
                    isVerified: business?.isVerified,
                    city: business?.city,
                    country: business?.country,
                    state: business?.state,
                    street: business?.street,
                    phone2: business?.phone2,
                    facebookUrl: business?.facebookUrl,
                    twitterUrl: business?.twitterUrl,
                    linkedInUrl: business?.linkedInUrl,
                    youtubeUrl: business?.youtubeUrl,
                    tumblerUrl: business?.tumblerUrl,
                    instagramUrl: business?.instagramUrl,
            ]


        def listingList = Listing.createCriteria().list {
            eq('businessDirectory', business)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            eq('isVisible', 1)
            eq('isVerified', 1)
            order('id', 'desc')
        }

        listingList.each {
            listing ->
                businessListing.add(
                        referenceId:listing?.referenceId,
                        title: listing?.title,
                        description: listing?.description,
                        'price': listingPrice(listing)[0],
                        "slug": listing?.slug,
                        "code": listing?.code,
                        "address": null,
                        "city": null,
                        "country": null,
                        "tripCode": null,
                        "metaTitle": listing?.metaTitle,
                        "metaKeyword": listing?.metaKeyword,
                        "metaDescription": listing?.metaDescription,
                        "featureImageUrl": featureImage(listing),
                        "isFeatured": listing?.isFeatured,
                        "category": Category.findById(listing?.category?.id)?.title,
                        "wishList": listingApiService.getWishList(id, listing),
                        'review':(reviewApiService.countAverageTotal(listing?.id))
                )
                totalAverageRating = totalAverageRating + reviewApiService.countAverageTotal(listing?.id)?.average
                if(reviewApiService.countAverageTotal(listing?.id)?.average>0){
                    countList = countList + 1
                }
                totalNumberRating = totalNumberRating + reviewApiService.countAverageTotal(listing?.id)?.total
        }
        if(countList == 0){
            averageRating = 0
         }
         else{
            averageRating = totalAverageRating / countList
         }

        rating=[
            'averageRating':averageRating,
            'totalReviewsCount': totalNumberRating
        ]
        
        result['business'] = businessInfo
        result['rating'] = rating
        result['listing'] = businessListing
        result['gallery'] = businessMultimediaGallery(business)
        result['video'] = businessMultimediaVideo(business)
        return result
    }


    def listingPrice(listing) {
        def listingPricing = ListingPrice.createCriteria().list {
            eq('listing', listing)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                min "retailRate"
            }
        }
        return listingPricing
    }

    def featureImage(listing){
        def bannerImage = ListingMultimedia.findWhere(listing: listing, type: 'FEATURE')
        return bannerImage?.fileUrl
    }

    def businessMultimediaGallery(business){
        def multimedias = ListingMultimedia.createCriteria().list {
            eq("businessDirectory", business)
            eq("status", "PUBLISH")
            eq("type", "GALLERY")
        }
        def multimediaData = []
        multimedias.each {
            multimedia->
                multimediaData.add(
                        "fileUrl": multimedia?.fileUrl
                )
        }
        return multimediaData
    }

    def businessMultimediaVideo(business){
        def multimedias = ListingMultimedia.createCriteria().list {
            eq("businessDirectory", business)
            eq("status", "PUBLISH")
            eq("type", "VIDEO")
        }
        def multimediaData = []
        multimedias.each {
            multimedia->
                if(multimedia?.fileUrl){
                    multimediaData.add(
                            "fileUrl": 'https://www.youtube.com/embed/'+multimedia?.fileUrl
                    )
                }
        }
        return multimediaData
    }
}
