package com.goglides

import com.goglides.common.CommonStatus
import grails.gorm.transactions.Transactional
import groovy.sql.Sql

@Transactional
class SearchApiService {

    def dataSource

    def search(data){
        def result
        def destinaton = data?.destination
        def category = data?.category
        def date = data?.date
        def rating = data?.rating
        def duration = data?.duration
        def priceMin = data?.priceMin
        def priceMax = data?.priceMax
        def tourDestination = ListingDestination.findByCity(destinaton)
        result = searchResult(category, duration, priceMin, priceMax, tourDestination);
        return result
    }

    def searchResult(category, duration, priceMin, priceMax, tourDestination){
        def listing
        def query
        def sql
        if(category) {
            listing= Listing.createCriteria().list {
                eq('category', Category.findBySlug(category))
                eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
                eq('isVisible', 1)
                eq('isVerified', 1)
                order('id', 'desc')
            }
        }
        if(category && duration){
           query = "SELECT * from listing l, listing_price lp, listing_destination ld, category c\n" +
                   "WHERE\n" +
                   "l.id = lp.listing_id AND\n" +
                   "l.id = ld.listing_id AND\n" +
                   "c.id = l.category_id AND\n" +
                   "c.status = \"PUBLISH\" AND\n" +
                   "l.status = \"PUBLISH\" AND\n" +
                   "l.is_verified = '1' AND\n" +
                   "l.is_visible = '1' AND\n" +
                   "c.title =\"paragliding\" AND\n" +
                   "lp.duration = '${duration}'"
            sql= new Sql(dataSource);
            listing = sql.rows(query);
        }
        if(category && duration && priceMax && priceMin){
                query = "SELECT * from listing l, listing_price lp, listing_destination ld, category c\n" +
                    "WHERE\n" +
                    "l.id = lp.listing_id AND\n" +
                    "l.id = ld.listing_id AND\n" +
                    "c.id = l.category_id AND\n" +
                    "c.status = \"PUBLISH\" AND\n" +
                    "l.status = \"PUBLISH\" AND\n" +
                    "l.is_verified = '1' AND\n" +
                    "l.is_visible = '1' AND\n" +
                    "c.title =\"paragliding\" AND\n" +
                    "lp.duration = '${duration}' AND\n" +
                    "lp.retail_rate >= '${priceMin}' AND\n" +
                    "lp.retail_rate <= '${priceMax}'"
            sql = new Sql(dataSource)
            listing = sql.rows(query);

        }
        return listing
    }
}
