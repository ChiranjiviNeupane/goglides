package com.goglides

import com.goglides.common.CommonStatus

class ListingApiService {
    BusinessApiService businessApiService
    ReviewApiService reviewApiService

    def feature(id) {
        def result = []
        def listing = []
        def listingPrice = []
        def category = []
        def business = []
        def review
        def msg
        def listings = Listing.createCriteria().list {
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            eq('isFeatured', 1)
            eq('isVisible', 1)
            eq('isVerified', 1)
            maxResults(4)
            order('id', 'desc')
        }
        if (listings) {
            listings.each {
                def rate = rate(it)
                review = (reviewApiService.countAverageTotal(it?.id))
                // println(review)
                listing.add(
                        'referenceId': it?.referenceId,
                        'code': it?.code,
                        'featureImageUrl': listingFeatureImage(it),
                        'title': it?.title,
                        'slug': it?.slug,
                        'address': it?.address,
                        'city': it?.city,
                        'country': it?.country,
                        'tripCode': it?.tripCode,
                        'metaTitle': it?.metaTitle,
                        'metaKeyword': it?.metaKeyword,
                        'metaDescription': it?.metaDescription,
                        'wishList': getWishList(id, it)
                )
                category.add(
                        'referenceId': it?.category?.referenceId,
                        'code': it?.category?.code,
                        'title': it?.category?.title,
                        'slug': it?.category?.slug,
                )
                business.add(
                        'referenceId': it?.businessDirectory?.referenceId,
                        'businessName': it?.businessDirectory?.businessName,
                        'slug': it?.businessDirectory?.slug,
                        'logoUrl': it?.businessDirectory?.logoUrl,
                        'address': it?.businessDirectory?.address,
                        'city': it?.businessDirectory?.city,
                        'street': it?.businessDirectory?.street,
                        'state': it?.businessDirectory?.state,
                        'country': it?.businessDirectory?.country,
                        'latitude': it?.businessDirectory?.latitude,
                        'longitude': it?.businessDirectory?.longitude,
                )

                result.add('listing': listing, 'listingPrice': rate, 'category': category, 'business': business, 'review': review)
                listing = []
                listingPrice = []
                category = []
                business = []
                review = []
            }
        } else {
            msg = "No record found."
            result.add('msg': msg)
        }
        return result
    }

    def rate(listing) {
        def result = ListingPrice.createCriteria().list {
            eq('listing', listing)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                min "retailRate"
            }
        }
        return result[0] ?: 0
    }


    def review(listing){
        def result = []
        def rating
        def review
        return result
    }

    def listingByCategory(id) {
        def result = []
        def listings = []
        def listing = []
        def listingPrice = []
        def category = []
        def business = []
        def review
        def msg

        def categoryList = Category.createCriteria().list {
            eq('isVisible', 1)
        }
        if (categoryList) {
            categoryList.each { cat ->

                def listingList = Listing.createCriteria().list {
                    eq('category', cat)
                    eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
                    ne('isFeatured', 1)
                    eq('isVisible', 1)
                    eq('isVerified', 1)
                    maxResults(4)
                    order('id', 'desc')
                }
                if (listingList) {
                    category.add(
                            'referenceId': cat?.referenceId,
                            'code': cat?.code,
                            'title': cat?.title,
                            'slug': cat?.slug,
                            'count': Listing.countByCategoryAndIsVisibleAndIsVerifiedAndStatus(cat, 1, 1, CommonStatus.STATUS_PUBLISH.getStatus()) ?: 0
                    )
                    listingList.each {
                        def rate = rate(it)
                        review = (reviewApiService.countAverageTotal(it?.id))
                        listing.add(
                                'referenceId': it?.referenceId,
                                'code': it?.code,
                                'title': it?.title,
                                'slug': it?.slug,
                                'address': it?.address,
                                'city': it?.city,
                                'country': it?.country,
                                'tripCode': it?.tripCode,
                                'metaTitle': it?.metaTitle,
                                'metaKeyword': it?.metaKeyword,
                                'metaDescription': it?.metaDescription,
                                'featureImageUrl': listingFeatureImage(it),
                                'wishList': getWishList(id, it)
                        )
                        business.add(
                                'referenceId': it?.businessDirectory?.referenceId,
                                'businessName': it?.businessDirectory?.businessName,
                                'slug': it?.businessDirectory?.slug,
                                'logoUrl': it?.businessDirectory?.logoUrl,
                                'address': it?.businessDirectory?.address,
                                'city': it?.businessDirectory?.city,
                                'street': it?.businessDirectory?.street,
                                'state': it?.businessDirectory?.state,
                                'country': it?.businessDirectory?.country,
                                'latitude': it?.businessDirectory?.latitude,
                                'longitude': it?.businessDirectory?.longitude,
                        )

                        listings.add('listing': listing, 'listingPrice': rate, 'business': business, 'review': review)
                        listing = []
                        listingPrice = []
                        business = []
                        review = []
                    }
                    result.add('category': category, 'list': listings)
                } else {
                    msg = "No record found."

                }
                listings = []
                category = []
            }
        } else {
            msg = "No record found."
            result.add('msg': msg)
        }

        return result
    }

    def listingByReference(referenceId) {
        return Listing.findByReferenceId(referenceId)
    }

    def listingBySlug(slug) {
        return Listing.findBySlugAndStatus(slug, CommonStatus.STATUS_PUBLISH.getStatus())
    }
    def listingFeatureImage(listing) {
        def result
        def data = ListingMultimedia.findWhere(
                'listing': listing,
                'type': 'FEATURE',
                'status': CommonStatus.STATUS_PUBLISH.getStatus()
        )
        if (data) {
            result = data?.fileUrl
        }
        return result
    }

    def listingBannerImage(listing) {
        def result
        def data = ListingMultimedia.findWhere(
                'listing': listing,
                'type': 'BANNER',
                'status': CommonStatus.STATUS_PUBLISH.getStatus()
        )
        if (data) {
            result = data?.fileUrl
        }
        return result
    }

    def listingGallery(listing) {
        def result = []
        def data = ListingMultimedia.createCriteria().list {
            eq('listing', listing)
            eq('type', 'GALLERY')
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
        }
        if (data) {
            data.each {
                result.add(it?.fileUrl)
            }
        }
        return result
    }

    def listingMedia(listing) {
        def result = [:]
        def map = []
        def video = []
        def data = ListingMultimedia.createCriteria().list {
            eq('listing', listing)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
        }
        if (data) {
            data.each {
                if (it?.type == 'MAP')
                map.add(it?.fileUrl)
                if (it?.type == 'VIDEO'){
                    if(it?.fileUrl.contains('youtube')){
                        def fileUrl = (it?.fileUrl?.contains('v='))? it?.fileUrl?.split('v=')[1]:it?.fileUrl
                        video.add("https://www.youtube.com/embed/"+fileUrl)
                    }
                }
            }
        }
        result['map'] = map
        result['video'] = video
        return result
    }

    def listing(slug, id) {
        def result = [:]
        def listing = []
        def business = []
        def category = []
        def data = listingBySlug(slug)
        if (data) {
            listing.add(
                    'referenceId': data?.referenceId,
                    'code': data?.code,
                    'tripCode': data?.tripCode,
                    'title': data?.title,
                    'slug': data?.slug,
                    'address': data?.address,
                    'city': data?.city,
                    'country': data?.country,
                    'latitude': data?.latitude,
                    'longitude': data?.longitude,
                    'highlight': data?.highlight,
                    'description': data?.description,
                    'metaTitle': data?.metaTitle,
                    'metaKeyword': data?.metaKeyword,
                    'metaDescription': data?.metaDescription,
                    'featureImageUrl': listingFeatureImage(data),
                    'bannerImageUrl': listingBannerImage(data),
                    'gallery': listingGallery(data),
                    "wishList": getWishList(id, data)
            )
            category.add(
                    'referenceId': data?.category?.referenceId,
                    'code': data?.category?.code,
                    'title': data?.category?.title,
                    'slug': data?.category?.slug
            )
            business.add(
                    'referenceId': data?.businessDirectory?.referenceId,
                    'businessName': data?.businessDirectory?.businessName,
                    'slug': data?.businessDirectory?.slug,
                    'logoUrl': data?.businessDirectory?.logoUrl,
                    'address': data?.businessDirectory?.address,
                    'city': data?.businessDirectory?.city,
                    'street': data?.businessDirectory?.street,
                    'state': data?.businessDirectory?.state,
                    'country': data?.businessDirectory?.country,
                    'latitude': data?.businessDirectory?.latitude,
                    'longitude': data?.businessDirectory?.longitude,
            )
            result['listing'] = listing
            result['category'] = category
            result['business'] = business
            result['destination'] = listingDestination(data)
            result['itinerary'] = listingItinerary(data)
            result['includeExclude'] = listingIncludeExclude(data)
            result['Checklist'] = listingChecklist(data)
            result['fact'] = listingFact(data)
            result['faq'] = listingFaq(data)
            result['transportRoute'] = listingTransportRoute(data)
            result['media'] = listingMedia(data)
            result['listingPrice'] = rate(data)
            result['searchComponent'] = priceSearchComponent(data)
            result['review'] = 0
        }
        return result
    }

    def listingIncludeExclude(listing) {
        def result = [:]
        def include = []
        def exclude = []
        def data = ListingIncludeExclude.createCriteria().list {
            eq('listing', listing)
        }
        if (data) {
            data.each {
                if (it?.type == 'INCLUDE') {
                    include.add(it?.includeExclude?.title)
                }
                if (it?.type == 'EXCLUDE')
                exclude.add(it?.includeExclude?.title)
            }
        }
        result['include'] = include
        result['exclude'] = exclude
        return result
    }

    def listingChecklist(listing) {
        def result = [:]
        def bring = []
        def provide = []
        def data = ListingChecklist.createCriteria().list {
            eq('listing', listing)
            ne('status', CommonStatus.STATUS_TRASH.getStatus())
        }
        if (data) {
            data.each {
                if (it?.type == 'BRING') {
                    bring.add(it?.checkList?.title)
                }
                if (it?.type == 'PROVIDE') {
                    provide.add(it?.checkList?.title)
                }
            }
        }

        result['bring'] = bring
        result['provide'] = provide
        return result
    }

    def listingDestination(listing) {
        def result = []
        def data = ListingDestination.createCriteria().list {
            eq('listing', listing)
        }
        if (data) {
            data.each {
                result.add('country': it?.country, 'city': it?.city)
            }
        }
        return result
    }

    def listingItinerary(listing) {
        def result = []
        def data = ListingItinerary.createCriteria().list {
            eq('listing', listing)
        }
        if (data) {
            data.each {
                result.add('title': it?.title, 'description': it?.description)
            }
        }
        return result
    }

    def listingFact(listing) {
        def result = []
        def data = ListingFact.findWhere(
                'listing': listing
        )
        if (data) {
            result.add(
                    'tripGrade': data?.tripGrade,
                    'tripStandard': data?.tripStandard,
                    'bestSeason': data?.bestSeason,
                    'startPoint': data?.startPoint,
                    'endPoint': data?.endPoint,
                    'startElevation': data?.startElevation,
                    'lowestElevation': data?.lowestElevation,
                    'highestElevation': data?.highestElevation,
                    'daysCount': data?.daysCount,
                    'nightCount': data?.nightCount,
                    'maxCapacity': data?.maxCapacity,
                    'availableCapacity': data?.availableCapacity,
                    'minPax': data?.minPax,
                    'maxPax': data?.maxPax,
                    'minWeight': data?.minWeight,
                    'maxweight': data?.maxWeight,
                    'minAge': data?.minAge,
                    'maxAge': data?.maxAge,
                    'bridgeHeight': data?.bridgeHeight,
                    'riverDistance': data?.riverDistance,
                    'healthCondition': data?.healthCondition
            )
        }
        return result
    }

    def listingFaq(listing) {
        def result = []
        def data = ListingFaq.createCriteria().list {
            eq('listing', listing)
        }
        if (data) {
            data.each {
                result.add('question': it?.question, 'answer': it?.answer)
            }
        }
        return result
    }

    def listingTransportRoute(listing) {
        def result = []
        def data = ListingTransportRoute.createCriteria().list {
            eq('listing', listing)
        }
        if (data) {
            data.each {
                result.add('address': it?.address, 'pickUp': it?.pickUp, 'dropOff': it?.dropOff)
            }
        }
        return result
    }

    def listingPriceList(listing, type, duration, schedule) {
        def result = []
        def data = ListingPrice.createCriteria().list {
            eq('listing', listing)
            if (type) {
                eq('type', type)
            }
            if (duration) {
                eq('duration', duration)
            }
            if (schedule) {
                eq('schedule', schedule)
            }
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
        }
        if (data) {
            data.each {
                result.add(
                        'referenceId': it?.referenceId,
                        'type': it?.type,
                        'duration': it?.duration,
                        'schedule': it?.schedule,
                        'nationality': it?.nationality,
                        'ageGroupRate': it?.ageGroupRate,
                        'stockUnit': it?.stockUnit,
                        'availableStockUnit': it?.stockUnit,
                        'enableSale': it?.enableSale,
                        'retailRate': it?.retailRate,
                        'seniorRetailRate': it?.seniorRetailRate,
                        'childRetailRate': it?.childRetailRate,
                        'infantRetailRate': it?.infantRetailRate,
                        'salesRate': it?.salesRate,
                        'seniorSalesRate': it?.seniorSalesRate,
                        'childSalesRate': it?.childSalesRate,
                        'infantSalesRate': it?.infantSalesRate
                )
            }
        }
        return result
    }

    def priceSearchComponent(listing) {
        def list = [:]
        def type = ListingPrice.createCriteria().list {
            eq('listing', listing)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                groupProperty('type')
            }
        }
        def schedule = ListingPrice.createCriteria().list {
            eq('listing', listing)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                groupProperty('schedule')
            }
        }
        def duration = ListingPrice.createCriteria().list {
            eq('listing', listing)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                groupProperty('duration')
            }
        }

        list['type'] = type
        list['schedule'] = schedule
        list['duration'] = duration
        return list
    }

    def more(category_slug, usrId) {
        def result = [:]
        def category = Category.findBySlug(category_slug)
        def listing = Listing.createCriteria().list {
            eq("category", category)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            eq('isVisible', 1)
            eq('isVerified', 1)
        }

        def listingData = []
        listing.each {
            listingValue ->
            listingData.add(
                        'referenceId': listingValue?.referenceId,
                        'code': listingValue?.code,
                        'featureImageUrl': listingFeatureImage(listingValue),
                        'title': listingValue?.title,
                        'slug': listingValue?.slug,
                        'address': BusinessDirectory.findById(listingValue?.businessDirectory?.id)?.street,
                        'city': BusinessDirectory.findById(listingValue?.businessDirectory?.id)?.city,
                        'country': BusinessDirectory.findById(listingValue?.businessDirectory?.id)?.country,
                        'tripCode': listingValue?.tripCode,
                        'metaTitle': listingValue?.metaTitle,
                        'metaKeyword': listingValue?.metaKeyword,
                        'metaDescription': listingValue?.metaDescription,
                        'price': businessApiService?.listingPrice(listingValue)[0],
                        'businessUrl': BusinessDirectory.findById(listingValue?.businessDirectory?.id)?.logoUrl,
                        'businessSlug': BusinessDirectory.findById(listingValue?.businessDirectory?.id)?.slug,
                        'businessName': BusinessDirectory.findById(listingValue?.businessDirectory?.id)?.businessName,
                        'wishList': getWishList(usrId, listingValue),
                        'review':(reviewApiService.countAverageTotal(listingValue?.id))
            )
        }
        result['category'] = category?.title
        result['listing'] = listingData
        return result
    }

    def listingPrice(listingPrice) {
        def result = []
        def price = ListingPrice.findByReferenceIdAndStatus(listingPrice, CommonStatus.STATUS_PUBLISH.getStatus())

        if (price) {
            result.add(
                    'referenceId': price?.referenceId,
                    'type': price?.type,
                    'duration': price?.duration,
                    'schedule': price?.schedule,
                    'nationality': price?.nationality,
                    'ageGroupRate': price?.ageGroupRate,
                    'stockUnit': price?.stockUnit,
                    'availableStockUnit': price?.stockUnit,
                    'enableSale': price?.enableSale,
                    'retailRate': price?.retailRate,
                    'seniorRetailRate': price?.seniorRetailRate,
                    'childRetailRate': price?.childRetailRate,
                    'infantRetailRate': price?.infantRetailRate,
                    'salesRate': price?.salesRate,
                    'seniorSalesRate': price?.seniorSalesRate,
                    'childSalesRate': price?.childSalesRate,
                    'infantSalesRate': price?.infantSalesRate
            )
            def business = []
            business.add(
                    'referenceId': price?.businessDirectory?.referenceId,
                    'slug': price?.businessDirectory?.slug
            )
            def listing = []
            listing.add(
                    'referenceId': price?.listing?.referenceId,
                    'slug': price?.listing?.slug
            )
            result.add('businessDirectory': business, 'listing': listing)
        }
        return result
    }

    def addonList(referenceId) {
        def result = []
        def listing = listingByReference(referenceId)
        if (listing) {
            def addons = ListingAddon.createCriteria().list {
                eq('listing', listing)
                eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            }
            if (addons) {
                addons.each {
                    result.add('referenceId': it?.referenceId, 'type': it?.type, 'title': it?.title, 'rate': it?.retailRate)
                }
            }
        }
        return result
    }

    def similarListing(referenceId, id) {
        def result = []
        def listing = []
        def listingPrice = []
        def category = []
        def business = []
        def review = []
        def msg
        def listings = Listing.createCriteria().list {
            eq('category', Category.findByReferenceId(referenceId))
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            eq('isVisible', 1)
            eq('isVerified', 1)
            sqlRestriction "1=1 ORDER BY RAND()"
            maxResults(4)
        }
        if (listings) {
            listings.each {
                def rate = rate(it)
                 review = (reviewApiService.countAverageTotal(it?.id))
                listing.add(
                        'referenceId': it?.referenceId,
                        'code': it?.code,
                        'featureImageUrl': listingFeatureImage(it),
                        'title': it?.title,
                        'slug': it?.slug,
                        'address': it?.address,
                        'city': it?.city,
                        'country': it?.country,
                        'tripCode': it?.tripCode,
                        'metaTitle': it?.metaTitle,
                        'metaKeyword': it?.metaKeyword,
                        'metaDescription': it?.metaDescription,
                        'wishList': getWishList(id, it)
                )
                category.add(
                        'referenceId': it?.category?.referenceId,
                        'code': it?.category?.code,
                        'title': it?.category?.title,
                        'slug': it?.category?.slug,
                )
                business.add(
                        'referenceId': it?.businessDirectory?.referenceId,
                        'businessName': it?.businessDirectory?.businessName,
                        'slug': it?.businessDirectory?.slug,
                        'logoUrl': it?.businessDirectory?.logoUrl,
                        'address': it?.businessDirectory?.address,
                        'city': it?.businessDirectory?.city,
                        'street': it?.businessDirectory?.street,
                        'state': it?.businessDirectory?.state,
                        'country': it?.businessDirectory?.country,
                        'latitude': it?.businessDirectory?.latitude,
                        'longitude': it?.businessDirectory?.longitude,
                )

                result.add('listing': listing, 'listingPrice': rate, 'category': category, 'business': business, 'review': review)
                listing = []
                listingPrice = []
                category = []
                business = []
                review = []
            }
        } else {
            msg = "No record found."
            result.add('msg': msg)
        }
        return result
    }

    def getWishList(id, it){
        def wishList
        def wishListData
        if(id == 0){
            wishList = false
        }else{
            def user = User.findById(id)
            wishListData = WishList.findByListingAndUser(it, user)
            if(wishListData){
                wishList =  true
            }else{
                wishList =  false
            }
        }
        return  wishList
    }
}