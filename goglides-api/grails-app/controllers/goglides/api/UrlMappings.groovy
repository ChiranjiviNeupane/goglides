package goglides.api

class UrlMappings {

    static mappings = {
        delete "/v1/$namespace/$controller/$id(.$format)?"(action: "delete")
        get "/v1/$namespace/$controller(.$format)?"(action: "index")
        get "/v1/$namespace/$controller/$id(.$format)?"(action: "show")
        post "/v1/$namespace/$controller(.$format)?"(action: "save")
        put "/v1/$namespace/$controller/$id(.$format)?"(action: "update")
        patch "/v1/$namespace/$controller/$id(.$format)?"(action: "patch")


        delete "/v1/$controller/$id(.$format)?"(action: "delete")
        get "/v1/$controller(.$format)?"(action: "index")
        get "/v1/$controller/$id(.$format)?"(action: "show")
        post "/v1/$controller(.$format)?"(action: "save")
        put "/v1/$controller/$id(.$format)?"(action: "update")
        patch "/v1/$controller/$id(.$format)?"(action: "patch")


        "/v1/$namespace/$controller/$action/$id?"()
        "/v1/$namespace/$controller/$action?"()
        "/v1/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        delete "/v1/$controller/$id(.$format)?"(action: "delete")
        get "/v1/$controller(.$format)?"(action: "index")
        get "/v1/$controller/$id(.$format)?"(action: "show")
        post "/v1/$controller(.$format)?"(action: "save")
        put "/v1/$controller/$id(.$format)?"(action: "update")
        patch "/v1/$controller/$id(.$format)?"(action: "patch")

        group("/v1/$namespace") {
            "/businessDirectory"(resources: 'businessDirectory') {
                "/businessContactPerson"(resources: 'businessContactPerson')
                "/businessCertificate"(resources: 'businessCertificate')
                group "/listing"(resources: 'listing') {
                    group "/listingIncludeExclude"(resources: 'listingIncludeExclude') {
                        "/includeExclude"(resources: 'includeExclude')
                    }
                    group "/listingChecklist"(resources: 'listingChecklist') {
                        "/checkList"(resources: 'checkList')
                    }
                    "/listingFact"(resources: 'listingFact')
                    "/listingItinerary"(resources: 'listingItinerary')
                    "/listingDestination"(resources: 'listingDestination')
                    "/listingTransportRoute"(resources: 'listingTransportRoute')
                    "/listingPrice"(resources: 'listingPrice')
                    "/listingAddon"(resources: 'listingAddon')
                    "/listingMultimedia"(resources: 'listingMultimedia')
                    "/listingSetting"(resources: 'listingSetting')
                    "/listingFaq"(resources: 'listingFaq')
                }
            }
        }

        "/v1/listing/feature/$usrId"(controller: 'listing', action: 'feature') {
            constraints {

            }
        }
        "/v1/listing/category/$usrId"(controller: 'listing', action: 'category') {
            constraints {

            }
        }
        "/v1/listing/search"(controller: 'listing', action: 'search') {
            constraints {

            }
        }

        "/v1/listing/listingPrice/$listingPrice"(controller: 'listing', action: 'listingPrice') {
            constraints {

            }
        }

        "/v1/listing/updateListingPrice"(controller: 'listing', action: 'updateListingPrice') {
            constraints {

            }
        }

        "/v1/listing/updateListingAddon"(controller: 'listing', action: 'updateListingAddon') {
            constraints {

            }
        }

        "/v1/listing/$slug/$usrId"(controller: 'listing', action: 'index') {
            constraints {
                slug matches: /^[a-z0-9-]+$/
            }
        }

        "/v1/more/$slug/$usrId"(controller: 'listing', action: 'more') {
            constraints {
                slug matches: /^[a-z0-9-]+$/
            }
        }

        "/v1/business/$slug/$usrId"(controller: 'business', action: 'index') {
            constraints {
                slug matches: /^[a-z0-9-]+$/
            }
        }
        "/v1/booking"(controller: 'booking', action: 'save') {
            constraints {
            }
        }
        "/v1/booking/view/$referenceId/$user"(controller: 'booking', action: 'view') {
            constraints {
                referenceId matches: /^[a-z0-9-]+$/
                user matches: /^[a-z0-9-]+$/
            }
        }
        "/v1/booking/$user"(controller: 'booking', action: 'index') {
            constraints {
                user matches: /^[a-z0-9-]+$/
            }
        }
        "/v1/listing/addonList/$referenceId"(controller: 'listing', action: 'addonList') {
            constraints {
                referenceId matches: /^[a-z0-9-]+$/
            }
        }
        "/v1/listing/similar/$referenceId/$usrId"(controller: 'listing', action: 'similar') {
            constraints {
                referenceId matches: /^[a-z0-9-]+$/
            }
        }
        "/v1/booking/saveAddon"(controller: 'booking', action: 'saveAddon') {
            constraints {}
        }
        "/v1/booking/removeAddon"(controller: 'booking', action: 'removeAddon') {
            constraints {}
        }
        "/v1/booking/removeAllAddon"(controller: 'booking', action: 'removeAllAddon') {
            constraints {}
        }
        "/v1/booking/processPayment/$referenceId"(controller: 'booking', action: 'processPayment') {
            constraints {
                referenceId matches: /^[a-z0-9-]+$/
            }
        }
        "/v1/booking/executePaypalPayment"(controller: 'booking', action: 'executePaypalPayment') {
            constraints {}
        }
        "/v1/booking/cancelPaypalPayment"(controller: 'booking', action: 'cancelPaypalPayment') {
            constraints {}
        }
        "/v1/contact"(controller: 'contact', action: 'save') {
            constraints {

            }
        }

        "/v1/banner"(controller: 'banner', action: 'save') {
            constraints {

            }
        }
		"/v1/banner/status"(controller: 'banner', action: 'status') {
            constraints {

            }
        }

        "/v1/myWishList/$usrId/$max/$offset"(controller: 'wishList', action: 'myWishList') {
            constraints {

            }
        }

        "/v1/wishlist/$refId/$usrId"(controller: 'wishList', action: 'remove') {
            constraints {

            }
        }
        "/v1/reviews/$referenceId/$star/$offset"(controller: 'review', action: 'reviews') {
            constraints {

            }
        }

        "/v1/reviews/status/$referenceId/$userId"(controller: 'review', action: 'status') {
            constraints {

            }
        }
        "/v1/reviews/myReview/$userId"(controller: 'review', action: 'myReview') {
            constraints {

            }
        }

        "/v1/reviews/count/$referenceId/$offset"(controller: 'review', action: 'count') {
            constraints {

            }
        }
        "/v1/quote"(controller: 'quote', action: 'save') {
            constraints {

            }
        }

        "/v1/forex/rate/$code"(controller: 'forex', action: 'rate') {
            constraints {

            }
        }
        "/v1/booking/executeKhaltiPayment"(controller: 'booking', action: 'executeKhaltiPayment') {
            constraints {}
        }
        "/"(view: '/notFound')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
