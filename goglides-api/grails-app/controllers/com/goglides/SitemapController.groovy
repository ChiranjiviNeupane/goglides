package com.goglides


import grails.rest.*
import grails.converters.*
import com.goglides.Listing
import com.goglides.SiteConfigService

class SitemapController {
	static responseFormats = ['json', 'xml']
    //SiteConfigService siteconfig
	
    def index() { 
        def results=[:]
        def urlXML = []
        def header="<?xml version='1.0' encoding='UTF-8'?><urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>"
        def footer="</urlset>"
        def url=''
        def listings = Listing.findAllByStatus("PUBLISH")
        listings.each{
            listing->
            url="<ul> <loc>"+SiteConfigService.siteUrl()+"/listing/"+listing.slug+"</loc>"+
                "<changefreq>weekly</changefreq>"+
                "<priority>0.8</priority>"+
                "</ul>"    
            urlXML.add(url)
        }
        results['header']=header
        results['ulLoc']=urlXML
        results['footer']=footer
        respond results
    }
}
