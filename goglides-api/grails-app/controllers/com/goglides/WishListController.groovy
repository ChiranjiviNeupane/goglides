package com.goglides

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class WishListController {

    WishListService wishListService
    ListingApiService   listingApiService
    WishListApiService wishListApiService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def details(Long id) {
       def wishList=[]
            def wish = WishList.createCriteria().list {
                user {
                    eq('id', id)
                } 
            }
            wish.each {
                    wishList.add(
                        'id': it?.id,
                        'listing_title': it?.listing?.title,
                        'listing_address': it?.listing?.address,
                        'listing_code': it?.listing?.code,
                        'listing_slug': it?.listing?.slug,
                        'listing_price': listingApiService.rate(it?.listing)
                    )
                }
        respond wishList
    }
    
    def index(User user){
        def list=WishList.findAll()
        respond list
    }

    def show(Long id) {
        respond wishListService.get(id)
    }

    def save() {
        def wishListData = request.JSON
        if (wishListData == null) {
            render status: NOT_FOUND
            return
        }else{
            def listing = Listing?.findByReferenceId(wishListData?.listing)
            def user = User?.findById(wishListData?.user)
            def wishList = WishList.findByListingAndUser(listing, user)
            if(wishList){
                respond status:"ALREADY_EXIST"
                return
            }else{
                wishList = new WishList()
                wishList?.listing =listing
                wishList?.user=user
                try {
                    wishListService.save(wishList)
                } catch (ValidationException e) {
                    respond wishList.errors, view:'create'
                    return
                }
                respond (msg: "saved")
            }

        }
    }

    def update(WishList wishList) {
        if (wishList == null) {
            render status: NOT_FOUND
            return
        }

        try {
            wishListService.save(wishList)
        } catch (ValidationException e) {
            respond wishList.errors, view:'edit'
            return
        }

        respond wishList, [status: OK, view:"show"]
    }

    def delete() {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        wishListService.delete(id)

        render status: NO_CONTENT
    }

    def myWishList(){
        def userId = params?.usrId
        def max = params?.max
        def offset = params?.offset
        def result = wishListApiService.myWishList(userId, max, offset)
        respond result
    }



    def remove(){
        def user = User?.findById(params?.usrId)
        def listing = Listing?.findByReferenceId(params?.refId)
        def wishList = WishList.findByListingAndUser(listing, user)?.id
        if(!wishList){
             render status: NOT_FOUND
            return
        }
        wishListService.delete(wishList)
        respond (msg:"deleted")
    }
}
