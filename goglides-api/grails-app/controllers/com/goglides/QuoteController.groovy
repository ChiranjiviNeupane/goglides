package com.goglides

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class QuoteController {

    QuoteService quoteService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond quoteService.list(params), model:[quoteCount: quoteService.count()]
    }

    def show(Long id) {
        respond quoteService.get(id)
    }

    def save(Quote quote) {
        if (quote == null) {
            render status: NOT_FOUND
            return
        }

        try {
            quoteService.save(quote)
        } catch (ValidationException e) {
            respond quote.errors, view:'create'
            return
        }

        respond quote, [status: CREATED, view:"show"]
    }

    def update(Quote quote) {
        if (quote == null) {
            render status: NOT_FOUND
            return
        }

        try {
            quoteService.save(quote)
        } catch (ValidationException e) {
            respond quote.errors, view:'edit'
            return
        }

        respond quote, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        quoteService.delete(id)

        render status: NO_CONTENT
    }
}
