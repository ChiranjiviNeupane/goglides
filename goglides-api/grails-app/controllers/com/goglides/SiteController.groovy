package com.goglides

import static org.springframework.http.HttpStatus.*

class SiteController {
    static responseFormats = ['json', 'xml']

    SiteService siteService

    def category() {
        respond siteService.category()
    }
}