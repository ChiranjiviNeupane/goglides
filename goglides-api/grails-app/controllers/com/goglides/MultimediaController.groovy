package com.goglides


import grails.rest.*
import grails.converters.*

class MultimediaController extends RestfulController {
    static responseFormats = ['json', 'xml']
    MultimediaController() {
        super(Multimedia)
    }
}
