package com.goglides


import grails.rest.*
import grails.converters.*

class ForexController {
	static responseFormats = ['json', 'xml']

    ForexAPIService forexAPIService

    def index() {
        def result = forexAPIService.forex()
        respond result
    }

    def rate(){
        def currencyCode = params?.code
        def result = forexAPIService.getRate(currencyCode)
        respond result
    }
}
