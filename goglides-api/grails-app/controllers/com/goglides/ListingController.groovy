package com.goglides

import static org.springframework.http.HttpStatus.*

class ListingController {
    static responseFormats = ['json', 'xml']

    ListingApiService listingApiService

    def feature() {
        def usrId = params?.usrId
        def result = [:]
        result['feature'] = listingApiService.feature(usrId)
        respond result
    }

    def category() {
        def usrId = params?.usrId
        def result = listingApiService.listingByCategory(usrId)
        respond result
    }

    def index() {
        if (params?.slug == null) {
            render status: NOT_FOUND
            return
        }

        def result = listingApiService.listing(params?.slug,params?.usrId)
        respond result
    }

    def search() {
        if (params?.listing == null) {
            render status: NOT_FOUND
            return
        }
        def listing = listingApiService.listingByReference(params?.listing)
        if (!listing) {
            render status: NOT_FOUND
            return
        }
        def type = params?.type ?: ''
        def duration = params?.duration ?: ''
        def schedule = params?.schedule ?: ''
        def result = listingApiService.listingPriceList(listing, type, duration, schedule)
        respond result
    }

    def listingPrice() {
        if (params?.listingPrice == null) {
            render status: NOT_FOUND
            return
        }

        def result = listingApiService.listingPrice(params?.listingPrice)
        respond result
    }

    def updateListingPrice() {
        def listingPrice = ListingPrice.createCriteria().list {
        }
        if (listingPrice) {
            listingPrice.each {
                if (!it?.referenceId) {
                    it?.referenceId = ListingPrice.generateReferenceId(9)
                    it.save(flush: true)
                }
            }
        }
        respond('t': 'done')
    }

    def updateListingAddon() {
        def listingPrice = ListingAddon.createCriteria().list {
        }
        if (listingPrice) {
            listingPrice.each {
                if (!it?.referenceId) {
                    it?.referenceId = ListingAddon.generateReferenceId(9)
                    it.save(flush: true)
                }
            }
        }
        respond('t': 'done')
    }

    def more() {
        def category_slug = params?.slug
        def usrId = params?.usrId
        def result = listingApiService.more(category_slug, usrId)
        respond result
    }

    def addonList() {
        if (params?.referenceId == null) {
            render status: NOT_FOUND
            return
        }
        def result = listingApiService.addonList(params?.referenceId)
        respond result
    }

    def similar() {
        if (params?.referenceId == null) {
            render status: NOT_FOUND
            return
        }

        def result = listingApiService.similarListing(params?.referenceId, params?.usrId)
        respond result
    }
}