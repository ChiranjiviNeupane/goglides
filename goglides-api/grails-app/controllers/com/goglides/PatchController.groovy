package com.goglides

import com.goglides.manage.ListingMultimediaService
import grails.rest.*
import grails.converters.*

class PatchController {
    static responseFormats = ['json', 'xml']
    ListingMultimediaService listingMultimediaService

    def s3UrlPatch() {
        def listingMultimedias = ListingMultimedia.createCriteria().list {
            ne("type", "VIDEO")
        }

        listingMultimedias.each {
            listingMultimedia ->
                if (listingMultimedia?.fileUrl.contains('test/')) {
                    def url = listingMultimedia?.fileUrl.split('test/')[1]
                    url = "https://stage-goglides-media.s3.us-west-2.amazonaws.com/original/listing/" + url
                    listingMultimedia?.fileUrl = url
                    if (listingMultimedia) {
                        listingMultimediaService.save(listingMultimedia)
                    }
                }
        }
        respond listingMultimedias
    }

    def defaultFeatureImage() {
        def listing = Listing.createCriteria().list {}
        if (listing) {
            listing.each {
                def media = ListingMultimedia.findByListingAndType(it, 'FEATURE')
                if (media == null) {
                    def feature = new ListingMultimedia()
                    feature.listing = it
                    feature.businessDirectory = BusinessDirectory.get(it?.businessDirectory?.id)
                    feature.user = User.get(listing?.user?.id)
                    feature.isApproved = 1
                    feature.type = 'FEATURE'
                    feature.fileUrl = 'https://s3-us-west-2.amazonaws.com/stage-goglides-media/default/default-image.png'
                    feature.status = 'PUBLISH'
                    if (!feature.save(flush: true)) {
                        feature.errors.allErrors.each {
                            // println it
                        }
                    }
                }
            }
        }
        respond('t': 'Patch feature image.')
    }
}
