package com.goglides.manage

import com.goglides.BusinessDirectory
import com.goglides.Listing
import com.goglides.ListingIncludeExclude
import com.goglides.common.CommonStatus
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingIncludeExcludeController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ListingIncludeExcludeService listingIncludeExcludeService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)


        if (params?.businessDirectoryId && params?.listingId) {
            def include = []
            def exclude = []
            def list = [:]
            def criteria = ListingIncludeExclude.createCriteria()
            def result = criteria.list {
                eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
                eq('listing', Listing.get(params?.listingId))
            }
            if (result) {
                result.each {
                    if (it?.type == 'INCLUDE') {
//                        include.add(it?.includeExclude?.title)
                        // include[it?.id] = it?.includeExclude?.title
                        include.add('id':it?.id, 'title':it?.includeExclude?.title)

                    }
                    if (it?.type == 'EXCLUDE')
                        exclude.add('id':it?.id, 'title':it?.includeExclude?.title)
                }
            }

            list['include'] = include
            list['exclude'] = exclude
            respond list
            return
        }
        respond listingIncludeExcludeService.list(params), model: [listingIncludeExcludeCount: listingIncludeExcludeService.count()]
    }

    def show(Long id) {
        respond listingIncludeExcludeService.get(id)
    }

    def save(ListingIncludeExclude listingIncludeExclude) {
        if (listingIncludeExclude == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingIncludeExcludeService.save(listingIncludeExclude)
        } catch (ValidationException e) {
            respond listingIncludeExclude.errors, view: 'create'
            return
        }

        respond listingIncludeExclude, [status: CREATED, view: "show"]
    }

    def update(ListingIncludeExclude listingIncludeExclude) {
        if (listingIncludeExclude == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingIncludeExcludeService.save(listingIncludeExclude)
        } catch (ValidationException e) {
            respond listingIncludeExclude.errors, view: 'edit'
            return
        }

        respond listingIncludeExclude, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingIncludeExcludeService.delete(id)

        render status: NO_CONTENT
    }
}
