package com.goglides.manage

import com.goglides.SiteSetting
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class SiteSettingController {

    SiteSettingService siteSettingService
    static namespace = "manage"

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        respond siteSettingService.list(params), model:[siteSettingCount: siteSettingService.count()]
    }

    def show(Long id) {
        respond siteSettingService.get(id)
    }

    def save(SiteSetting siteSetting) {
        if (siteSetting == null) {
            render status: NOT_FOUND
            return
        }

        try {
            siteSettingService.save(siteSetting)
        } catch (ValidationException e) {
            respond siteSetting.errors, view:'create'
            return
        }

        respond siteSetting, [status: CREATED, view:"show"]
    }

    def update(SiteSetting siteSetting) {
        if (siteSetting == null) {
            render status: NOT_FOUND
            return
        }

        try {
            siteSettingService.save(siteSetting)
        } catch (ValidationException e) {
            respond siteSetting.errors, view:'edit'
            return
        }

        respond siteSetting, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        siteSettingService.delete(id)

        render status: NO_CONTENT
    }
}
