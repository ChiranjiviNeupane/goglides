package com.goglides.manage

import com.goglides.BusinessDirectory
import com.goglides.Listing
import com.goglides.ListingTransportRoute
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingTransportRouteController {

    ListingTransportRouteService listingTransportRouteService

    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            respond ListingTransportRoute.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingTransportRouteService.list(params), model:[listingTransportRouteCount: listingTransportRouteService.count()]
    }

    def show(Long id) {
        respond listingTransportRouteService.get(id)
    }

    def save(ListingTransportRoute listingTransportRoute) {
        if (listingTransportRoute == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingTransportRouteService.save(listingTransportRoute)
        } catch (ValidationException e) {
            respond listingTransportRoute.errors, view:'create'
            return
        }

        respond listingTransportRoute, [status: CREATED, view:"show"]
    }

    def update(ListingTransportRoute listingTransportRoute) {
        if (listingTransportRoute == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingTransportRouteService.save(listingTransportRoute)
        } catch (ValidationException e) {
            respond listingTransportRoute.errors, view:'edit'
            return
        }

        respond listingTransportRoute, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingTransportRouteService.delete(id)

        render status: NO_CONTENT
    }
	
	def routeList(){
		def dropoff = []
		def pickup = []
		def list = [:]
		def criteria = ListingTransportRoute.createCriteria()
		def result = criteria.list {
			eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
			eq('listing', Listing.get(params?.listingId))
		}
		if (result) {
			result.each {
				if (it?.pickUp) {
					pickup.add(it?.address)

				}
				if (it?.dropOff)
					dropoff.add(it?.address)
			}
		}

		list['pickup'] = pickup
		list['dropoff'] = dropoff
		respond list
		return
	}
}
