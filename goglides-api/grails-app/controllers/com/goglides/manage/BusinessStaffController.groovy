package com.goglides.manage

import com.goglides.BusinessStaff
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BusinessStaffController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    BusinessStaffService businessStaffService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        respond businessStaffService.list(params), model: [businessStaffCount: businessStaffService.count()]
    }

    def show(Long id) {
        respond businessStaffService.get(id)
    }

    def save(BusinessStaff businessStaff) {
        if (businessStaff == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessStaffService.save(businessStaff)
        } catch (ValidationException e) {
            respond businessStaff.errors, view: 'create'
            return
        }

        respond businessStaff, [status: CREATED, view: "show"]
    }

    def update(BusinessStaff businessStaff) {
        if (businessStaff == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessStaffService.save(businessStaff)
        } catch (ValidationException e) {
            respond businessStaff.errors, view: 'edit'
            return
        }

        respond businessStaff, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        businessStaffService.delete(id)

        render status: NO_CONTENT
    }
}
