package com.goglides.manage

import com.goglides.ListingPrice
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingPriceController {

    ListingPriceService listingPriceService
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            respond ListingPrice.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingPriceService.list(params), model:[listingPriceCount: listingPriceService.count()]
    }

    def show(Long id) {
        respond listingPriceService.get(id)
    }

    def save(ListingPrice listingPrice) {
        if (listingPrice == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingPriceService.save(listingPrice)
        } catch (ValidationException e) {
            respond listingPrice.errors, view:'create'
            return
        }

        respond listingPrice, [status: CREATED, view:"show"]
    }

    def update(ListingPrice listingPrice) {
        if (listingPrice == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingPriceService.save(listingPrice)
        } catch (ValidationException e) {
            respond listingPrice.errors, view:'edit'
            return
        }

        respond listingPrice, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingPriceService.delete(id)

        render status: NO_CONTENT
    }
}
