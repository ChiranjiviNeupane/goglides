package com.goglides.manage

import com.goglides.User
import com.goglides.UserService

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

class UserController {
    static responseFormats = ['json', 'xml']
    static namespace = "manage"
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    UserService userService

    def index() {
        println("reached.......................")
        respond(message: "hello")
    }


    def getUser(params) {
        def user = User.get(params?.id)
        respond([user: userService.mapUser(user)])
    }

    def getUserDetails(id) {
        def user = User.get(id)
        respond([user: userService.mapUser(user)])
    }

    def checkUser() {
        def data = request.JSON
        def user = User.findByReferenceId(data.ref)
        if (user != null) {
            respond([user: [id: user.id, referenceId: user.referenceId]])
        } else {
            respond([user: null])
        }
    }

    def saveUser() {
        def data = request.JSON
        if (!User.findByReferenceId(data?.ref)) {
            def user = new User()
            user.referenceId = data?.ref
            user.save(flush: true)
        }
        respond([user: [id: user.id, referenceId: user.referenceId]])
    }

    def updateKcUser() {
        def data = request.JSON
        def userRef = User.get(data.id)
        if (userRef != null) {
            def user = userService.mapUser(userRef)
            if (user?.profile != null && user?.profile != "") {
                //def bucket = amazonS3Service.sourceBucket()
                //println("profile Image deleted")
                //amazonS3Service.s3Delete(bucket, user.profile)
            }
            respond([user: userService.update(userRef, data)])
        } else {
            respond([user: null])
        }
    }

    def reset() {
        def data = request.JSON
        def userRef = User.get(data.id)
        if(!userRef && !data?.ps){
            render status: NOT_ACCEPTABLE
            return
        }else {
            if (userService.changePassword(userRef, data.ps))
                respond status: true
        }
    }

    def deleteProfileImage() {
        def data = request.JSON
        data.profile = "deleted"
        def userRef = User.get(data.id)
        if (userRef != null) {
            def user = userService.mapUser(userRef)
            if (user?.profile != null && user?.profile != "") {
                def bucket = amazonS3Service.sourceBucket()
                //amazonS3Service.s3Delete(bucket, user.profile)
                //println("profile Image deleted")
                def updateStat = userService.update(userRef, data)
                if (updateStat != null) {
                    respond([stat: "200"])
                }
            }
        }
        respond([user: null])
    }

    /******* User APIs *********/
    //For Review

    def getUserProfileById() {
        def userRef = User.get(params.id)
        def user = userService.mapUser(userRef)
        respond([url: user.profile])
    }

    def getUserNameById() {
        def userRef = User.get(params.id)
        def user = userService.mapUser(userRef)
        respond([firstname: user.firstname, lastname: user.lastname])
    }

    /******* End of User APIs *********/

//    def saveAndUpdateUserCurrencyPreference(UserPreferences data){
//        def user = User.findById(data.user.id)
//        def u = UserPreferences.findByUser(user)
//        if(u != null){
//            u['currencyCode'] = data['currencyCode']
//            u.save(flush:true)
//            respond([message: "updated!"])
//        }else {
//            data.save()
//            respond ([message : "saved!"])
//        }
//    }

    def getUserCurrencyPreference() {
        def user = User.findByReferenceId(params?.rid)
        def currencyPref = UserPreferences.findWhere(user: user)
        if (currencyPref == null) {
            respond([id: "1", currencyCode: "USD", user: [id: null]])
        } else
            respond(currencyPref)
    }
}
