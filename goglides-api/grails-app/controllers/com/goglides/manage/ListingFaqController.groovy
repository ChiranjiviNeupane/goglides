package com.goglides.manage

import com.goglides.ListingFaq
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingFaqController {

    ListingFaqService listingFaqService
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            respond ListingFaq.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingFaqService.list(params), model: [listingFaqCount: listingFaqService.count()]
    }

    def show(Long id) {
        respond listingFaqService.get(id)
    }

    def save(ListingFaq listingFaq) {
        if (listingFaq == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingFaqService.save(listingFaq)
        } catch (ValidationException e) {
            respond listingFaq.errors, view: 'create'
            return
        }

        respond listingFaq, [status: CREATED, view: "show"]
    }

    def update(ListingFaq listingFaq) {
        if (listingFaq == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingFaqService.save(listingFaq)
        } catch (ValidationException e) {
            respond listingFaq.errors, view: 'edit'
            return
        }

        respond listingFaq, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingFaqService.delete(id)

        render status: NO_CONTENT
    }
}
