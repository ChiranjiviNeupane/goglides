package com.goglides.manage

import com.goglides.Booking
import com.goglides.BookingItem
import com.goglides.BookingItemAddon
import com.goglides.BookingService
import com.goglides.BusinessDirectory
import com.goglides.Listing
import com.goglides.ListingAddon
import com.goglides.ListingPrice
import com.goglides.User
import com.goglides.common.BookingStatus
import com.goglides.common.CommonStatus
import grails.validation.ValidationException

import java.awt.List
import java.awt.print.Book
import java.lang.reflect.Array
import java.sql.Date

import static org.springframework.http.HttpStatus.*

class BookingController {

    BookingService bookingService
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId) {
            def busId = params.businessDirectoryId
            respond Booking.where { businessDirectory.id == busId }.list()
            return
        }
        respond bookingService.list(params), model: [bookingCount: bookingService.count()]
    }

    def show(Long id) {
        def result = [:]
        def booking = bookingService.get(id)
        def items = bookingItemList(booking)
        result['booking'] = booking
        result['items'] = items
        respond result
    }

    def save() {
        def bookingInfo = request.JSON

        def paxTotalCount = 0
        def paxTotal = 0
        def addonTotal = 0
        def total = 0
        def grandTotal = 0

        def booking = new Booking()
        booking.businessDirectory = BusinessDirectory.get(bookingInfo?.businessDirectory)
        booking.listing = Listing.get(bookingInfo?.listing)
        booking.user = User.get(bookingInfo?.user)
        booking.bookingDate = Date.parse("yyyy-MM-dd", bookingInfo?.bookingDate)
        booking.clientName = bookingInfo?.clientName ?: ''
        booking.clientContact = bookingInfo?.clientContact ?: ''
        booking.clientEmail = bookingInfo?.clientEmail ?: ''
        booking.categories = bookingInfo?.categories ?: ''
        booking.bookingStatus = BookingStatus.STATUS_RESERVED.getStatus()
        booking.paxTotalCount = bookingInfo?.paxTotalCount ?: 0
        booking.paxTotal = bookingInfo?.paxTotal ?: 0
        booking.total = bookingInfo?.total ?: 0
        booking.grandTotal = bookingInfo?.grandTotal ?: 0
        booking.cart = bookingInfo?.paxInfo ?: ""
        if (booking == null) {
            render status: NOT_FOUND
            return
        }

        try {
            bookingService.save(booking)
            if (bookingInfo?.paxInfo) {
                bookingInfo?.paxInfo.each { item ->
                    bookingItem(booking, item)
                    paxTotalCount += item?.totalQty
                    paxTotal += item?.total
                }
                total = paxTotal + addonTotal
                grandTotal = total
                booking.paxTotalCount = paxTotalCount
                booking.paxTotal = paxTotal
                booking.total = total
                booking.grandTotal = grandTotal
                booking.save(flush: true)
            }
        } catch (ValidationException e) {
            respond booking.errors, view: 'create'
            return
        }

        respond booking, [status: CREATED, view: "show"]
    }

    def update(Booking booking) {
        if (booking == null) {
            render status: NOT_FOUND
            return
        }

        try {
            bookingService.save(booking)
        } catch (ValidationException e) {
            respond booking.errors, view: 'edit'
            return
        }

        respond booking, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        bookingService.delete(id)

        render status: NO_CONTENT
    }

    def packageSearch() {
        def c = ListingPrice.createCriteria()
        def result = c.list {
            eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
            eq('listing', Listing.get(params?.listingId))
            if (params?.type)
                eq('type', params?.type)
            if (params?.schedule)
                eq('schedule', params?.schedule)
            if (params?.duration)
                eq('duration', params?.duration)
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
        }
        respond result
        return
    }

    def packageByCode() {
        def listing = Listing.findByCode(params?.code)
        respond(listing)
    }

    def packageSearchComponent() {
        def list = [:]
        def type = ListingPrice.createCriteria().list {
            eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
            eq('listing', Listing.get(params?.listingId))
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                groupProperty('type')
            }
        }
        def schedule = ListingPrice.createCriteria().list {
            eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
            eq('listing', Listing.get(params?.listingId))
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                groupProperty('schedule')
            }
        }
        def duration = ListingPrice.createCriteria().list {
            eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
            eq('listing', Listing.get(params?.listingId))
            eq('status', CommonStatus.STATUS_PUBLISH.getStatus())
            projections {
                groupProperty('duration')
            }
        }

        list['type'] = type
        list['schedule'] = schedule
        list['duration'] = duration
        respond list
    }

    def bookingItem(booking, item) {
        def adult = item?.adultQty
        def senior = item?.seniorQty
        def child = item?.childQty
        def infant = item?.infantQty
        if (adult > 0) {
            for (def i = 0; i < adult; i++) {
                saveBookingItem(booking, item, 'ADT')
            }
        }
        if (senior > 0) {
            for (def i = 0; i < adult; i++) {
                saveBookingItem(booking, item, 'SNR')
            }
        }
        if (child > 0) {
            for (def i = 0; i < adult; i++) {
                saveBookingItem(booking, item, 'CLD')
            }
        }
        if (infant > 0) {
            for (def i = 0; i < adult; i++) {
                saveBookingItem(booking, item, 'INF')
            }
        }
    }

    def saveBookingItem(booking, item, ageGroup) {
        def retailRate
        switch (ageGroup) {
            case 'ADT':
                retailRate = item?.adultRate
                break
            case 'SNR':
                retailRate = item?.seniorRate
                break
            case 'CLD':
                retailRate = item?.childRate
                break
            case 'INF':
                retailRate = item?.infantRate
        }
        def bookingItem = new BookingItem()
        bookingItem.businessDirectory = BusinessDirectory.get(item?.businessDirectory)
        bookingItem.listing = Listing.get(item?.listing)
        bookingItem.listingPrice = ListingPrice.get(item?.listingPrice)
        bookingItem.booking = Booking.get(booking?.id)
        bookingItem.user = User.get(item?.user)
        bookingItem.ageGroup = ageGroup
        bookingItem.retailRate = retailRate
        bookingItem.bookingDate = Date.parse("yyyy-MM-dd", item?.bookingDate)
        bookingItem.category = item?.category
        bookingItem.type = item?.type
        bookingItem.schedule = item?.schedule
        bookingItem.duration = item?.duration
        if (!bookingItem.validate()) {
        }
        bookingItem.save(flush: true)
    }

    def bookingItemList(booking) {
        def itemList = []
        def category
        def duration
        def schedule
        def type
        def paxList
        def paxInfo = []
        def addonList = []
        def items = BookingItem.createCriteria().list {
            eq('booking', booking)
            projections {
                distinct('category')
                property('duration')
                property('schedule')
                property('type')
            }
        }


        if (items) {
            items.each { item ->
                category = item[0]
                duration = item[1]
                schedule = item[2]
                type = item[3]
                paxList = BookingItem.createCriteria().list {
                    eq('booking', booking)
                    if (category)
                        eq('category', category)
                    if (duration)
                        eq('duration', duration)
                    if (schedule)
                        eq('schedule', schedule)
                    if (type)
                        eq('type', type)
                }
                if (paxList) {
                    paxList.each { pax ->
                        def paxAddonList
                        if (pax?.addAddon) {
                            paxAddonList = BookingItemAddon.createCriteria().list {
                                eq('bookingItem', BookingItem.get(pax?.id))
                            }

                            if (paxAddonList) {
                                paxAddonList.each { addon ->
                                    addonList.add('id': addon?.id, 'title': addon?.listingAddon?.title, 'retailRate': addon?.retailRate)
                                }
                            }
                        }
                        paxInfo.add(
                                'id': pax?.id,
                                'businessDirectoryId': pax?.businessDirectoryId,
                                'listingId': pax?.listingId,
                                'bookingId': pax?.bookingId,
                                'bookingDate': pax?.bookingDate,
                                'schedule': pax?.schedule,
                                'duration': pax?.duration,
                                'type': pax?.type,
                                'category': pax?.category,
                                'ageGroup': pax?.ageGroup,
                                'ticketNo': pax?.ticketNo ?: '',
                                'title': pax?.title ?: '',
                                'fullName': pax?.fullName ?: '',
                                'contactNo': pax?.contactNo ?: '',
                                'email': pax?.email ?: '',
                                'nationality': pax?.nationality ?: '',
                                'identificationType': pax?.identificationType ?: '',
                                'identificationNo': pax?.identificationNo ?: '',
                                'weight': pax?.weight ?: '',
                                'addAddon': pax?.addAddon ?: 0,
                                'retailRate': pax?.retailRate ?: 0,
                                'addons': addonList
                        )
                        addonList = []
                    }
                }
                itemList.add('category': category, 'duration': duration, 'schedule': schedule, 'type': type, 'products': paxInfo)
                paxInfo = []
            }
            if (paxList) {
                paxList.each { pax ->
                    paxInfo.add(
                            'id': pax?.id,
                            'businessDirectoryId': pax?.businessDirectoryId,
                            'listingId': pax?.listingId,
                            'bookingId': pax?.bookingId,
                            'bookingDate': pax?.bookingDate,
                            'schedule': pax?.schedule,
                            'duration': pax?.duration,
                            'type': pax?.type,
                            'category': pax?.category,
                            'ageGroup': pax?.ageGroup,
                            'ticketNo': pax?.ticketNo ?: '',
                            'title': pax?.title ?: '',
                            'fullName': pax?.fullName ?: '',
                            'contactNo': pax?.contactNo ?: '',
                            'email': pax?.email ?: '',
                            'nationality': pax?.nationality ?: '',
                            'identificationType': pax?.identificationType ?: '',
                            'identificationNo': pax?.identificationNo ?: '',
                            'weight': pax?.weight ?: '',
                            'addAddon': pax?.addAddon ?: 0
                    )
                }
            }
            itemList.add('category': category, 'duration': duration, 'schedule': schedule, 'type': type, 'products': paxInfo)
            paxInfo = []
        }
        return itemList
    }

    def updateBookingItem() {
        try {
            def bookingInfo = request.JSON
            if (bookingInfo?.paxInfo) {
                bookingInfo?.paxInfo.each { item ->
                    def bookingItem = BookingItem.get(item?.id)
                    if (bookingItem) {
                        bookingItem.properties = item
                        if (bookingItem.hasErrors()) {
                            bookingItem.errors.allErrors.each {
                                //println it
                            }
                        }
                        bookingItem.save(flush: true)
                    }
                }
            }
        } catch (ValidationException e) {
            render status: NOT_FOUND
            return
        }

        respond('status': OK)
    }

    def saveBookingAddon() {
        def bookingInfo = request.JSON

        if (bookingInfo) {
            def pax = BookingItem.get(bookingInfo?.paxId)
            def paxAddons = BookingItemAddon.createCriteria().list {
                eq("bookingItem", pax)
            }
            paxAddons*.delete(flush: true)
            if (pax) {
                bookingInfo?.addonsList.each { item ->
                    def paxAddon = BookingItemAddon.findWhere(bookingItem: pax, listingAddon: ListingAddon.get(item?.addonsId))
                    if (!paxAddon) {
                        def itemAddon = new BookingItemAddon()
                        itemAddon.businessDirectory = BusinessDirectory.get(pax?.businessDirectoryId)
                        itemAddon.listing = Listing.get(pax?.listingId)
                        itemAddon.booking = Booking.get(pax?.bookingId)
                        itemAddon.bookingItem = pax
                        itemAddon.listingAddon = ListingAddon.get(item?.addonsId)
                        itemAddon.user = User.get(bookingInfo?.user)
                        itemAddon.retailRate = item?.retailRate
                        if (itemAddon.validate()) {
                            itemAddon.save(flush: true)
                        } else {
                            if (itemAddon.hasErrors()) {
                                itemAddon.errors.allErrors.each {

                                }
                            }
                        }
                    }
                }
                respond('status': OK)
                return
            }
        }
        render status: NOT_FOUND
    }

    def issueTicket() {
        if (params?.bookingId) {
            def items = BookingItem.createCriteria().list {
                eq("booking", Booking.get(params?.bookingId))
            }

            if (items) {
                items.each { item ->
                    if (!item.ticketNo) {
                        def ticketNo = generateTicket()
                        item.ticketNo = ticketNo
                        item.save(flush: true)
                    }
                }
            }
            respond('status': OK)
            return
        } else {
            render status: NOT_FOUND
        }
    }

    def generateTicket() {
        def ticket
        def ticketNo = BookingItem.createCriteria().get {
            projections {
                max "ticketNo"
            }
        }
        if (!ticketNo) {
            ticket = 1000000001
        } else {
            ticket = ticketNo.toInteger() + 1
        }

        return ticket
    }


    def invoiceAndTicket() {

    }

    def addons() {
        def id = params?.id
        def data = []
        def bookingItems = [:]
        def booking = Booking.findById(id)
        def values = new ArrayList()
        def categories = BookingItem.createCriteria().list {
            eq('booking', booking)
            projections {
                distinct('category')
            }
        }

        categories.each {
            category ->
//                category?.findIndexOf(category)
                bookingItems = BookingItem.createCriteria().list {
                    eq('category', category)
                }
                bookingItems.each {
                    bookingItem ->
                        values.add(BookingItemAddon.findAllByBookingItem(bookingItem))
                        data.push(values)
                }
        }
        respond values
    }
}