package com.goglides.manage

import com.goglides.ListingFact
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingFactController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ListingFactService listingFactService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)

        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            respond ListingFact.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingFactService.list(params), model: [listingFactCount: listingFactService.count()]
    }

    def show(Long id) {
        respond listingFactService.get(id)
    }

    def save(ListingFact listingFact) {
        if (listingFact == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingFactService.save(listingFact)
        } catch (ValidationException e) {
            respond listingFact.errors, view: 'create'
            return
        }

        respond listingFact, [status: CREATED, view: "show"]
    }

    def update(ListingFact listingFact) {
        if (listingFact == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingFactService.save(listingFact)
        } catch (ValidationException e) {
            respond listingFact.errors, view: 'edit'
            return
        }

        respond listingFact, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingFactService.delete(id)

        render status: NO_CONTENT
    }
}
