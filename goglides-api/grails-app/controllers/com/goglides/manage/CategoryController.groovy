package com.goglides.manage

import com.goglides.Category
import com.goglides.common.CommonStatus
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class CategoryController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    CategoryService categoryService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        respond categoryService.list(params), model: [categoryCount: categoryService.count()]
    }

    def show(Long id) {
        respond categoryService.get(id)
    }

    def save(Category category) {
        if (category == null) {
            render status: NOT_FOUND
            return
        }

        try {
            categoryService.save(category)
        } catch (ValidationException e) {
            respond category.errors, view: 'create'
            return
        }

        respond category, [status: CREATED, view: "show"]
    }

    def update(Category category) {
        if (category == null) {
            render status: NOT_FOUND
            return
        }

        try {
            categoryService.save(category)
        } catch (ValidationException e) {
            respond category.errors, view: 'edit'
            return
        }

        respond category, [status: OK, view: "show"]
    }
    def countData(){

        def result

        def data
        try{
            def criteria = Category.createCriteria()
            result = criteria.list {
                projections {
                    count()
                }

            }
            data=["count":result[0]]


        }catch (Exception e){
            log.info(e.printStackTrace())
        }

        respond data, status: OK
    }
    def countStatus(params){

        def result
        def data
        try{
            def criteria = Category.createCriteria()
             result = criteria.list {
                projections {
                    count()
                }
                 eq ('status', params.status)

            }
            data=["count":result[0]]


        }catch (Exception e){
            log.info(e.printStackTrace())
        }

        respond data, status: OK
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        def category
        try{
        category =Category.get(id);
            if(category){
                category.status= CommonStatus.STATUS_TRASH.getStatus();
                category.save(flush : true, failOnError : true)
            }

        }catch (Exception e){
            log.info(e.printStackTrace())
        }

        respond category, status: OK
    }

}
