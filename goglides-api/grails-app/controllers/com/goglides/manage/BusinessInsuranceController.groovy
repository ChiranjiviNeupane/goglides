package com.goglides.manage

import com.goglides.BusinessInsurance
import com.goglides.BusinessInsuranceService
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BusinessInsuranceController {

    BusinessInsuranceService businessInsuranceService
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId) {
            def busId = params.businessDirectoryId
            respond BusinessInsurance.where { businessDirectory.id == busId }.list()
            return
        }
        respond businessInsuranceService.list(params), model: [businessInsuranceCount: businessInsuranceService.count()]
    }

    def show(Long id) {
        respond businessInsuranceService.get(id)
    }

    def save(BusinessInsurance businessInsurance) {
        if (businessInsurance == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessInsuranceService.save(businessInsurance)
        } catch (ValidationException e) {
            respond businessInsurance.errors, view: 'create'
            return
        }

        respond businessInsurance, [status: CREATED, view: "show"]
    }

    def update(BusinessInsurance businessInsurance) {
        if (businessInsurance == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessInsuranceService.save(businessInsurance)
        } catch (ValidationException e) {
            respond businessInsurance.errors, view: 'edit'
            return
        }

        respond businessInsurance, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        businessInsuranceService.delete(id)

        render status: NO_CONTENT
    }
}
