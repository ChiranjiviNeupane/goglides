package com.goglides.manage

import com.goglides.ListingChecklist
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import com.goglides.Listing
import com.goglides.BusinessDirectory
import com.goglides.common.CommonStatus

class ListingChecklistController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ListingChecklistService listingChecklistService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def bring = []
            def provide = []
            def list = [:]
            def criteria = ListingChecklist.createCriteria()
            def result = criteria.list {
                eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
                eq('listing', Listing.get(params?.listingId))
                ne('status', CommonStatus.STATUS_TRASH.getStatus())
            }
            if (result) {
                result.each {
                    if (it?.type == 'BRING') {
                        //bring.add(it?.checkList?.title)
                        bring.add('id': it?.id,'title': it?.checkList?.title)
                    }
                    if (it?.type == 'PROVIDE') {
                        provide.add('id': it?.id,'title': it?.checkList?.title)
                    }
                }
            }

            list['bring'] = bring
            list['provide'] = provide
            respond list
            return
        }
        respond listingChecklistService.list(params), model: [listingChecklistCount: listingChecklistService.count()]
    }

    def show(Long id) {
        respond listingChecklistService.get(id)
    }

    def save(ListingChecklist listingChecklist) {
        if (listingChecklist == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingChecklistService.save(listingChecklist)
        } catch (ValidationException e) {
            respond listingChecklist.errors, view: 'create'
            return
        }

        respond listingChecklist, [status: CREATED, view: "show"]
    }

    def update(ListingChecklist listingChecklist) {
        if (listingChecklist == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingChecklistService.save(listingChecklist)
        } catch (ValidationException e) {
            respond listingChecklist.errors, view: 'edit'
            return
        }

        respond listingChecklist, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingChecklistService.delete(id)

        render status: NO_CONTENT
    }
}
