package com.goglides.manage

import com.goglides.BusinessCertificate
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BusinessCertificateController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    BusinessCertificateService businessCertificateService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
		if (params?.businessDirectoryId) {
            def busId = params.businessDirectoryId
            respond BusinessCertificate.where { businessDirectory.id == busId }.list()
            return
        }
        respond businessCertificateService.list(params), model: [businessCertificateCount: businessCertificateService.count()]
    }

    def show(Long id) {
        respond businessCertificateService.get(id)
    }

    def save(BusinessCertificate businessCertificate) {
        if (businessCertificate == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessCertificateService.save(businessCertificate)
        } catch (ValidationException e) {
            log.error(e.printStackTrace())
            respond businessCertificate.errors, view: 'create'
            return
        }

        respond businessCertificate, [status: CREATED, view: "show"]
    }

    def update(BusinessCertificate businessCertificate) {
        if (businessCertificate == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessCertificateService.save(businessCertificate)
        } catch (ValidationException e) {
            respond businessCertificate.errors, view: 'edit'
            return
        }

        respond businessCertificate, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        businessCertificateService.delete(id)

        render status: NO_CONTENT
    }
}
