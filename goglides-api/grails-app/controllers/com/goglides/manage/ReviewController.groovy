package com.goglides.manage


import grails.rest.*
import grails.converters.*
import com.goglides.Review

class ReviewController extends RestfulController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    ReviewController() {
        super(Review)
    }
}
