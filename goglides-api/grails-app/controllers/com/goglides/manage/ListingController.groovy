package com.goglides.manage

import com.goglides.BusinessDirectory
import com.goglides.Listing

import grails.validation.ValidationException

import java.text.DateFormat
import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*

class ListingController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ListingService listingService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId) {
            def busId = params?.businessDirectoryId
            respond Listing.where { businessDirectory.id == busId }.list(sort: "title", order: "desc")
            return
        }
        respond Listing.list(params), model: [listingCount: listingService.count()]
    }

    def show(Long id) {
        respond listingService.get(id)
    }

    def save(Listing listing) {
        if (listing == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingService.save(listing)
        } catch (ValidationException e) {
            respond listing.errors, view: 'create'
            return
        }
        respond listing, [status: CREATED, view: "show"]
    }

    def update() {
        def id = params?.id
        def listingValue = request.JSON
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd")
        def listing
        if(listingValue?.featuredStart == null || listingValue?.featuredEnd == null){
            listing = Listing.findById(id)
            listing.properties = listingValue
        }else {
            listingValue.featuredStart = (!listingValue?.featuredStart) ? null : date.parse(listingValue?.featuredStart)
            listingValue.featuredEnd = (!listingValue?.featuredEnd) ? null : date.parse(listingValue?.featuredEnd)
            listing = Listing.findById(id)
            listing.properties = listingValue
        }
        if (listing == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingService.save(listing)
        } catch (ValidationException e) {
            respond listing.errors, view: 'edit'
            return
        }

        respond listing, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingService.delete(id)

        render status: NO_CONTENT
    }

    def countStatus(params) {
        def result
        def data
        try {
            def criteria = Listing.createCriteria()
            result = criteria.list {
                eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
                projections {
                    count()
                }
                eq('status', params.status)
            }
            data = ["count": result[0]]
        } catch (Exception e) {
            log.info(e.printStackTrace())
        }
        respond data, status: OK
    }
}
