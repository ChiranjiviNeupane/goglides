package com.goglides.manage

import com.goglides.Category
import com.goglides.IncludeExclude
import com.goglides.common.CommonStatus
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class IncludeExcludeController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    IncludeExcludeService includeExcludeService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        respond includeExcludeService.list(params), model: [includeExcludeCount: includeExcludeService.count()]
    }

    def show(Long id) {
        respond includeExcludeService.get(id)
    }

    def save(IncludeExclude includeExclude) {
        if (includeExclude == null) {
            render status: NOT_FOUND
            return
        }

        try {
            includeExcludeService.save(includeExclude)
        } catch (ValidationException e) {
            respond includeExclude.errors, view: ''
            return
        }

        respond includeExclude, [status: CREATED, view: "show", namespace: "manage"]
    }

    def update(IncludeExclude includeExclude) {
        if (includeExclude == null) {
            render status: NOT_FOUND
            return
        }

        try {
            includeExcludeService.save(includeExclude)
        } catch (ValidationException e) {
            respond includeExclude.errors
            return
        }

        respond includeExclude, [status: OK, view: "show"]
    }

    def countData() {

        def result
        def data
        try {
            def criteria = IncludeExclude.createCriteria()
            result = criteria.list {
                projections {
                    count()
                }

            }
            data = ["count": result[0]]


        } catch (Exception e) {
            log.info(e.printStackTrace())
        }

        respond data, status: OK
    }

    def countStatus(params) {

        def result
        def data
        try {
            def criteria = IncludeExclude.createCriteria()
            result = criteria.list {
                projections {
                    count()
                }
                eq('status', params.status)

            }
            data = ["count": result[0]]


        } catch (Exception e) {
            log.info(e.printStackTrace())
        }

        respond data, status: OK
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        def includeExclude
        try {
            includeExclude = IncludeExclude.get(id)
            if (includeExclude) {
                includeExclude.status = CommonStatus.STATUS_TRASH.getStatus()
                includeExclude.save(failOnError: true, flush: true)
            }

        } catch (Exception e) {
            log.error(e.printStackTrace())
        }


        respond includeExclude, status: OK
    }

    def includeAndExclude(params) {
        def category = Category.findById(params.id)
        respond(IncludeExclude.findAllByStatusAndCategory("PUBLISH", category));
    }
}
