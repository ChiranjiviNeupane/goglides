package com.goglides.manage

import com.goglides.Forex
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ForexController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ForexService forexService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        respond forexService.list(params), model: [forexCount: forexService.count()]
    }

    def show(Long id) {
        respond forexService.get(id)
    }

    def save(Forex forex) {
        if (forex == null) {
            render status: NOT_FOUND
            return
        }

        try {
            forexService.save(forex)
        } catch (ValidationException e) {
            respond forex.errors, view: 'create'
            return
        }

        respond forex, [status: CREATED, view: "show"]
    }

    def update(Forex forex) {
        if (forex == null) {
            render status: NOT_FOUND
            return
        }

        try {
            forexService.save(forex)
        } catch (ValidationException e) {
            respond forex.errors, view: 'edit'
            return
        }

        respond forex, [status: OK, view: "show"]
    }

    def countData(){

        def result
        def data
        try{
            def criteria = Forex.createCriteria()
            result = criteria.list {
                projections {
                    count()
                }

            }
            data=["count":result[0]]


        }catch (Exception e){
            log.info(e.printStackTrace())
        }

        respond data, status: OK
    }
    def countStatus(params){

        def result
        def data
        try{
            def criteria = Forex.createCriteria()
            result = criteria.list {
                projections {
                    count()
                }
                eq ('status', params.status)

            }
            data=["count":result[0]]


        }catch (Exception e){
            log.info(e.printStackTrace())
        }

        respond data, status: OK
    }
    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        forexService.delete(id)

        render status: NO_CONTENT
    }
}
