package com.goglides.manage

import com.goglides.BusinessDirectory
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BusinessDirectoryController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    BusinessDirectoryService businessDirectoryService

    def index(Integer max) {

        params.max = Math.min(max ?: 300, 500)
        respond businessDirectoryService.list(params), model: [businessDirectoryCount: businessDirectoryService.count()]
    }

    def show(Long id) {
        respond businessDirectoryService.get(id)
    }

    def save(BusinessDirectory businessDirectory) {
        if (businessDirectory == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessDirectoryService.save(businessDirectory)
        } catch (ValidationException e) {
            log.error(e.printStackTrace())
            respond businessDirectory.errors, view: 'create'
            return
        }

        respond businessDirectory, [status: CREATED, view: "show"]
    }

    def update(BusinessDirectory businessDirectory) {
        if (businessDirectory == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessDirectoryService.save(businessDirectory)
        } catch (ValidationException e) {
            respond businessDirectory.errors, view: 'edit'
            return
        }

        respond businessDirectory, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        businessDirectoryService.delete(id)

        render status: NO_CONTENT
    }
}
