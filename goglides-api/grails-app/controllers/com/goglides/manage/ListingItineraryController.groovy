package com.goglides.manage

import com.goglides.ListingItinerary
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import java.util.*

class ListingItineraryController {

    ListingItineraryService listingItineraryService

    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            respond ListingItinerary.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingItineraryService.list(params), model:[listingItineraryCount: listingItineraryService.count()]
    }

    def show(Long id) {
        respond listingItineraryService.get(id)
    }

    def save(ListingItinerary listingItinerary) {
        if (listingItinerary == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingItineraryService.save(listingItinerary)
        } catch (ValidationException e) {
            respond listingItinerary.errors, view:'create'
            return
        }

        respond listingItinerary, [status: CREATED, view:"show"]
    }

    def update(ListingItinerary listingItinerary) {
        if (listingItinerary == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingItineraryService.save(listingItinerary)
        } catch (ValidationException e) {
            respond listingItinerary.errors, view:'edit'
            return
        }

        respond listingItinerary, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingItineraryService.delete(id)

        render status: NO_CONTENT
    }

    def getByListingId(){
        log.INFO("reached.................")
        def listingItinerary=ListingItinerary.createCriteria().list{

        }
        respond listingItinerary

    }
}
