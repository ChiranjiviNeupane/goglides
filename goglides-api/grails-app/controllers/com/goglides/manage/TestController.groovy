package com.goglides.manage

import com.goglides.Test
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class TestController {
    static namespace = 'manage'

    TestService testService
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond testService.list(params), model: [testCount: testService.count()], status: OK
    }

    def show(Long id) {
        respond testService.get(id), status: OK
    }

    def save(Test test) {
        if (test == null) {
            render status: NOT_FOUND
            return
        }

        try {
            testService.save(test)
        } catch (ValidationException e) {
            respond test.errors, view: 'create'
            return
        }

        respond test, [status: CREATED, view: "show"]
    }

    def update(Test test) {
        if (test == null) {
            render status: NOT_FOUND
            return
        }

        try {
            testService.save(test)
        } catch (ValidationException e) {
            respond test.errors, view: 'edit'
            return
        }

        respond test, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        testService.delete(id)

        render status: NO_CONTENT
    }
}
