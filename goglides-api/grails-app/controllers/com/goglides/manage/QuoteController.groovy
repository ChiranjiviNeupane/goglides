package com.goglides.manage


import grails.rest.*
import grails.converters.*
import com.goglides.Quote

class QuoteController extends RestfulController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    QuoteController() {
        super(Quote)
    }
}
