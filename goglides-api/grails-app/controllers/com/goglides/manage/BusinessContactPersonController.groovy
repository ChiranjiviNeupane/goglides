package com.goglides.manage

import com.goglides.BusinessContactPerson
import com.goglides.BusinessContactPersonService
import com.goglides.BusinessDirectory
import com.goglides.User
import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.*



class BusinessContactPersonController {

    BusinessContactPersonService businessContactPersonService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static namespace = "manage"

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)


        if (params?.businessDirectoryId) {
            def busId = params.businessDirectoryId
            respond BusinessContactPerson.where { businessDirectory.id == busId }.list()
            return
        }
        respond businessContactPersonService.list(params), model: [businessContactPersonCount: businessContactPersonService.count()]
    }

    def show(Long id) {
        respond businessContactPersonService.get(id)
    }

    def save() {
        try{
        def allRequest=request.JSON
        def contactInfo=allRequest.contact_info
        def userId=allRequest.user.id
        def businessDirectoryId=allRequest.businessDirectory.id
        def user=User.get(userId);
        def businessDirectory= BusinessDirectory.get(businessDirectoryId)

    
        for(BusinessContactPerson b: contactInfo){
        def businessContactInfo=new BusinessContactPerson();
            businessContactInfo.user=user
            businessContactInfo.businessDirectory=businessDirectory
            businessContactInfo.contactPersonFirstName=b.contactPersonFirstName
            businessContactInfo.contactPersonLastName=b.contactPersonLastName
            businessContactInfo.contactPersonEmail=b.contactPersonEmail
            businessContactInfo.contactPersonJobTitle=b.contactPersonJobTitle
            businessContactInfo.contactPersonPrimaryPhone=b.contactPersonPrimaryPhone
            businessContactInfo.contactPersonSecondaryPhone=b.contactPersonSecondaryPhone
            businessContactInfo.contactPersonMobile=b.contactPersonMobile
            businessContactInfo.save(flush: true, failOnError : true)
        }
        }catch(Exception e){
            log.info("Exception "+e)
        }

        respond "created", [status: CREATED, view:"show"]
    }

    def update(BusinessContactPerson businessContactPerson) {
        if (businessContactPerson == null) {
            render status: NOT_FOUND
            return
        }

        try {
            businessContactPersonService.save(businessContactPerson)
        } catch (ValidationException e) {
            respond businessContactPerson.errors, view:'edit'
            return
        }

        respond businessContactPerson, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        businessContactPersonService.delete(id)

        render status: NO_CONTENT
    }
}
