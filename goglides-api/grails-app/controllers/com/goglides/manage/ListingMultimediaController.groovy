package com.goglides.manage

import com.goglides.BusinessDirectory
import com.goglides.Listing
import com.goglides.ListingMultimedia
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingMultimediaController {

    ListingMultimediaService listingMultimediaService
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            if (params?.type) {
                def criteria = ListingMultimedia.createCriteria()
                def result = criteria.list {
                    eq('businessDirectory', BusinessDirectory.get(params?.businessDirectoryId))
                    eq('listing', Listing.get(params?.listingId))
                    eq('type', params?.type)
                }
                respond result
                return
            }
            respond ListingMultimedia.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingMultimediaService.list(params), model: [listingMultimediaCount: listingMultimediaService.count()]
    }

    def show(Long id) {
        respond listingMultimediaService.get(id)
    }

    def save(ListingMultimedia listingMultimedia) {
        if (listingMultimedia == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingMultimediaService.save(listingMultimedia)
        } catch (ValidationException e) {
            respond listingMultimedia.errors, view: 'create'
            return
        }

        respond listingMultimedia, [status: CREATED, view: "show"]
    }

    def update(ListingMultimedia listingMultimedia) {
        if (listingMultimedia == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingMultimediaService.save(listingMultimedia)
        } catch (ValidationException e) {
            respond listingMultimedia.errors, view: 'edit'
            return
        }

        respond listingMultimedia, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingMultimediaService.delete(id)

        render status: NO_CONTENT
    }
}
