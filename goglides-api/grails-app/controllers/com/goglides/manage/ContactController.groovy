package com.goglides.manage


import grails.rest.*
import grails.converters.*
import com.goglides.Contact

class ContactController extends RestfulController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    ContactController() {
        super(Contact)
    }
}
