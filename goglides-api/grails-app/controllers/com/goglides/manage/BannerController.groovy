package com.goglides.manage


import grails.rest.*
import grails.converters.*
import com.goglides.Banner
import com.goglides.Quote

class BannerController extends RestfulController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    BannerController() {
        super(Banner)
    }

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def status() {
        def result=[:]
        def status= []
        def statusAdd = []
        def quote = []

        def banners =Banner.findAll() 
        banners.each{
            banner->
            if(banner?.status){
                status.add(
                    banner
                )
            }
            if(banner?.statusAdd){
                statusAdd.add(
                    banner
                )
            }
            if(banner?.showQuote){
               def keywords = Quote.findAll('from Quote where status = 1 order by rand()', [max: 1])
                quote.add('quote':keywords.description[0],
                            'source':keywords.source[0],
                            'user':keywords.user.id[0]
                            )
            }
        }
        result['banner'] = status
        result['bannerBottom']= statusAdd
        result['quote']= quote
        respond result
    }
}
