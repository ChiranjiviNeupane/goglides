package com.goglides.manage

import com.goglides.ListingAddon
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingAddonController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ListingAddonService listingAddonService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            respond ListingAddon.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingAddonService.list(params), model: [listingAddonCount: listingAddonService.count()]
    }

    def show(Long id) {
        respond listingAddonService.get(id)
    }

    def save(ListingAddon listingAddon) {
        if (listingAddon == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingAddonService.save(listingAddon)
        } catch (ValidationException e) {
            respond listingAddon.errors, view: 'create'
            return
        }

        respond listingAddon, [status: CREATED, view: "show"]
    }

    def update(ListingAddon listingAddon) {
        if (listingAddon == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingAddonService.save(listingAddon)
        } catch (ValidationException e) {
            respond listingAddon.errors, view: 'edit'
            return
        }

        respond listingAddon, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingAddonService.delete(id)

        render status: NO_CONTENT
    }
}
