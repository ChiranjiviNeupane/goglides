package com.goglides.manage

import com.goglides.ListingDestination
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListingDestinationController {
    static namespace = "manage"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ListingDestinationService listingDestinationService

    def index(Integer max) {
        params.max = Math.min(max ?: 300, 500)
        if (params?.businessDirectoryId && params?.listingId) {
            def busId = params?.businessDirectoryId
            def listing = params.listingId
            respond ListingDestination.where { listing.id == listing; businessDirectory.id == busId }.list()
            return
        }
        respond listingDestinationService.list(params), model: [listingDestinationCount: listingDestinationService.count()]
    }

    def show(Long id) {
        respond listingDestinationService.get(id)
    }

    def save(ListingDestination listingDestination) {
        if (listingDestination == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingDestinationService.save(listingDestination)
        } catch (ValidationException e) {
            respond listingDestination.errors, view: 'create'
            return
        }

        respond listingDestination, [status: CREATED, view: "show"]
    }

    def update(ListingDestination listingDestination) {
        if (listingDestination == null) {
            render status: NOT_FOUND
            return
        }

        try {
            listingDestinationService.save(listingDestination)
        } catch (ValidationException e) {
            respond listingDestination.errors, view: 'edit'
            return
        }

        respond listingDestination, [status: OK, view: "show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        listingDestinationService.delete(id)

        render status: NO_CONTENT
    }
}
