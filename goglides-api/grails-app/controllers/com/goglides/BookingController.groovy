package com.goglides

import com.goglides.common.BookingStatus
import com.goglides.common.PaymentStatus

import static org.springframework.http.HttpStatus.*
import com.paypal.base.Constants
import com.paypal.api.payments.Details
import groovyx.net.http.*
import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*

class BookingController {
    static responseFormats = ['json', 'xml']

    BookingApiService bookingApiService
    PaymentApiService paymentApiService
    SiteConfigService siteConfigService

    def save() {
        def bookingInfo = request.JSON
        if (bookingInfo == null) {
            render status: NOT_FOUND
            return
        }
        if (!bookingInfo?.referenceId) {
            render status: NOT_FOUND
            return
        }
        def result = bookingApiService.save(bookingInfo)
        respond result

    }

    def saveAddon() {
        def bookingInfo = request.JSON
        if (bookingInfo == null) {
            render status: NOT_FOUND
            return
        }
        def result = bookingApiService.saveAddons(bookingInfo)
        respond result
    }

    def removeAddon() {
        def bookingInfo = request.JSON
        if (bookingInfo == null) {
            render status: NOT_FOUND
            return
        }
        def result = bookingApiService.removeAddon(bookingInfo)
        respond result
    }

    def removeAllAddon() {
        def bookingInfo = request.JSON
        if (bookingInfo == null) {
            render stage: NOT_FOUND
            return
        }
        def result = bookingApiService.removeAllAddon(bookingInfo)
        respond result
    }

    def saveBooking() {
        def bookingInfo = request.JSON
        if (bookingInfo == null) {
            render status: NOT_FOUND
            return
        }

        def result = bookingApiService.saveBooking(bookingInfo)
        if (result.booking) {
            def booking = Booking.findByReferenceId(result?.booking)
            def payment = paymentProcessing(booking)

            result.add('url': payment?.url)
        }
        respond result
    }

    def view() {
        if (params?.referenceId == null || params?.user == null) {
            render status: NOT_FOUND
            return
        }
        def result = bookingApiService.view(params?.referenceId, params?.user)
        respond result
    }

    def index() {
        if (params?.user == null) {
            render status: NOT_FOUND
            return
        }
        def id = params?.user
        def result = bookingApiService.myBooking(id)
        respond result
    }

    def cancellation() {
        def refId = params?.id
        def data = request.JSON
        if (!refId && !data) {
            render status: NOT_FOUND
            return
        }
        def booking = Booking.findByReferenceId(refId)
        booking?.bookingStatus = data?.bookingStatus
        booking?.msgCancellation = data?.msgCancellation
        respond booking.save(flush: true)
    }

    def paymentProcessing(booking) {
        def result = []
        switch (booking?.paymentMode) {
            case 'PAYPAL':
                def res = paymentApiService.approvePaypalPayment(booking)
                if (res.responseStatus == 'error') {
                    result.add('An error has occoured.')
                } else {
                    result.add(res)
                }
        }
        return result
    }

    def executePaypalPayment() {
        def execute = paymentApiService.executePaypalPayment(params)
        if (execute?.booking) {
            redirect(url: siteConfigService.siteUrl() + '/checkout/ticket/' + execute?.booking?.referenceId)
        } else {
            render status: NOT_FOUND
            return
        }
    }

    def cancelPaypalPayment() {
        def result = []
        if (params?.id == null) {
            render status: NOT_FOUND
            return
        }
        def booking = Booking.findByReferenceId(params?.id)
        if (booking) {
            booking.paymentStatus = PaymentStatus.STATUS_CANCELLED.getStatus()
            booking.bookingStatus = BookingStatus.STATUS_CANCELLED.getStatus()
            if (!booking.save(flush: true)) {
                result.add('Request cannot be processed.')
                redirect(url: siteConfigService.siteUrl() + '/checkout/ticket/' + booking?.referenceId)
            } else {
                redirect(url: siteConfigService.siteUrl() + '/checkout/ticket/' + booking?.referenceId)
            }
        } else {
            result.add('Invalid data')
        }
        respond result
    }

    def executeKhaltiPayment() {
        def result = []
        def obj = request.JSON
        def http = new HTTPBuilder(SiteConfigService.khaltiEndpoint())
        def booking = Booking.findWhere(referenceId: obj.referenceId)
        http.request(POST) {
            requestContentType = URLENC
            body = [token: obj.token, amount: obj.amount]
            headers.'Authorization' = "Key " + SiteConfigService.khaltiSecretKey()
            headers.'Accept' = 'application/json'
            headers.'Access-Control-Allow-Origin' = '*'
            response.success = { resp, json ->
                booking.payerId = obj.mobile
                booking.payerToken = obj.token
                booking.transactionId = json.idx
                booking.paymentStatus = PaymentStatus.STATUS_PAID.getStatus()
                booking.bookingStatus = BookingStatus.STATUS_CONFIRMED.getStatus()
                if (booking.save(flush: true)) {
                    result.add('referenceId': booking?.referenceId)
                    paymentApiService.issueTicket(booking)
                }
            }
            if (!response.success) {
                result.add('Transaction cannot be processed.')
            }
        }
        result.add(url: siteConfigService.siteUrl() + '/checkout/ticket/' + booking?.referenceId)
        respond result
    }

    def routeList() {
        def dropoff = []
        def pickup = []
        def list = [:]
        def criteria = ListingTransportRoute.createCriteria()
        def result = criteria.list {
            eq('listing', Listing.findByReferenceId(params?.id))
        }
        if (result) {
            result.each {
                if (it?.pickUp) {
                    pickup.add(it?.address)

                }
                if (it?.dropOff)
                    dropoff.add(it?.address)
            }
        }

        list['pickup'] = pickup
        list['dropoff'] = dropoff
        respond list
        return
    }

}