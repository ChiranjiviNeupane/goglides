package com.goglides

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BannerController {

    BannerService bannerService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bannerService.list(params), model:[bannerCount: bannerService.count()]
    }

    def show(Long id) {
        respond bannerService.get(id)
    }

    def save(Banner banner) {
        if (banner == null) {
            render status: NOT_FOUND
            return
        }

        try {
            bannerService.save(banner)
        } catch (ValidationException e) {
            respond banner.errors, view:'create'
            return
        }

        respond banner, [status: CREATED, view:"show"]
    }

    def update(Banner banner) {
        if (banner == null) {
            render status: NOT_FOUND
            return
        }

        try {
            bannerService.save(banner)
        } catch (ValidationException e) {
            respond banner.errors, view:'edit'
            return
        }

        respond banner, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        bannerService.delete(id)

        render status: NO_CONTENT
    }
	
	def status() {
        def result=[:]
        def status= []
        def statusAdd = []
        def quote = []

        def banners = Banner.findAll() 
        banners.each{
            banner->
            if(banner?.status){
                status.add(
                    banner
                )
            }
            if(banner?.statusAdd){
                statusAdd.add(
                    banner
                )
            }
            if(banner?.showQuote){
               def keywords = Quote.findAll('from Quote where status = 1 order by rand()', [max: 1])
                quote.add('quote':keywords.description[0],
                            'source':keywords.source[0],
                            'user':keywords.user.id[0]
                            )
            }
        }
        result['banner'] = status
        result['bannerBottom']= statusAdd
        result['quote']= quote
        respond result
    }
}
