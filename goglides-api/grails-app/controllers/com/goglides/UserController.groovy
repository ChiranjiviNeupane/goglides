package com.goglides


import grails.rest.*
import grails.converters.*
import com.goglides.User
class UserController {
    static responseFormats = ['json', 'xml']

    UserService userService
    def checkUser() {
        def data = request.JSON
        def user = User.findByReferenceId(data?.ref)
        if (user != null) {
            respond([user: [id: user?.id, referenceId: user?.referenceId]])
        } else {
            respond([user: null])
        }
    }

    def saveUser() {
        def data = request.JSON
        if (!User.findByReferenceId(data?.ref)) {
            def newUser = new User()
            newUser.referenceId = data?.ref
            newUser.save(flush: true)
            respond([user: [id: newUser?.id, referenceId: newUser?.referenceId]])
        } else {
            def user = User.findByReferenceId(data?.ref)
            respond([user: [id: user?.id, referenceId: user?.referenceId]])
        }
    }
    
    def getUser(params) {
        def user = User.get(params?.id)
        respond([user: userService.mapUser(user)])
    }


}
