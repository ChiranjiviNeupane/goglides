package com.goglides

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ContactController {

    ContactService contactService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond contactService.list(params), model:[contactCount: contactService.count()]
    }

    def show(Long id) {
        respond contactService.get(id)
    }

    def save(Contact contact) {
        if (contact == null) {
            render status: NOT_FOUND
            return
        }

        try {
            contactService.save(contact)
        } catch (ValidationException e) {
            respond contact.errors, view:'create'
            return
        }

        respond contact, [status: CREATED, view:"show"]
    }

    def update(Contact contact) {
        if (contact == null) {
            render status: NOT_FOUND
            return
        }

        try {
            contactService.save(contact)
        } catch (ValidationException e) {
            respond contact.errors, view:'edit'
            return
        }

        respond contact, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        contactService.delete(id)

        render status: NO_CONTENT
    }
}
