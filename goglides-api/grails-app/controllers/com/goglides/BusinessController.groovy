package com.goglides

import static org.springframework.http.HttpStatus.NOT_FOUND

class BusinessController {
	static responseFormats = ['json', 'xml']

    BusinessApiService businessApiService
    def index() {
        def slug = params?.slug
        def id = params?.usrId
        if (params?.slug == null) {
            render status: NOT_FOUND
            return
        }

        def business = businessApiService.getBuisness(slug, id)
        respond business
    }
}
