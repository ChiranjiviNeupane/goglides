package com.goglides


import grails.rest.*
import grails.converters.*

class SearchController {
	static responseFormats = ['json', 'xml']
    SearchApiService searchApiService
    def index(params) {
        def searchResult = searchApiService.search(params)
        respond searchResult
    }
}
