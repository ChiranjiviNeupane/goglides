package com.goglides

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ReviewController {

    ReviewService reviewService
    ReviewApiService reviewApiService
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond reviewService.list(params), model:[reviewCount: reviewService.count()]
    }

    def show(Long id) {
        respond reviewService.get(id)
    }

    def save() {
        def review = request.JSON
        if(review == null){
            render status: NOT_FOUND
            return
        }
        def reviewData = new Review()
        reviewData?.listing = Listing?.findByReferenceId(review?.listing)
        reviewData?.user = User?.get(review?.user)
        reviewData?.status = review?.status
        reviewData?.star = review?.star
        reviewData?.description = review?.description
        reviewData?.videoUrl = review?.videoUrl
        reviewData?.blogUrl = review?.blogUrl
        reviewData?.userEmail = review?.userEmail
        try{
            reviewData.save(flush: true)
        }catch(ValidationException e){
            respond review.errors, view:'create'
            return
        }
       respond(msg: reviewData)
    }

    def update(Review review) {
        if (review == null) {
            render status: NOT_FOUND
            return
        }

        try {
            reviewService.save(review)
        } catch (ValidationException e) {
            respond review.errors, view:'edit'
            return
        }
        respond review, [status: OK, view:"show"]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        reviewService.delete(id)

        render status: NO_CONTENT
    }


    def status(String referenceId , Long userId){
        def listing = Listing.findByReferenceId(referenceId)
        def details = [:]
            def detail = Review.createCriteria().list() {
                createAlias("listing", "l")
                createAlias("user", "u")
                eq("l.id", listing?.id)
                eq("u.id", userId)
                order('id', 'desc')
            }
                detail.each {
                    def mul = reviewApiService.multimedia(it?.id)
                    details = [
                            'id':it?.id,
                            'userName':reviewApiService.userById(it?.user?.id)?.user?.firstname +" "+reviewApiService.userById(it?.user?.id)?.user?.lastname,
                            'userPhoto':reviewApiService.userById(it?.user?.id)?.user?.profile,
                            'star': it?.star,
                            'description': it?.description,
                            'videoUrl': it?.videoUrl,
                            'blogUrl': it?.blogUrl,
                            'updatedAt': it?.updatedAt,
                            'status': it?.status,
                            'multimedia': mul
                    ]
                }
            respond details
    }

    def count(String referenceId, Integer offset){
        def listing = Listing.findByReferenceId(referenceId)
        
        def result = [:]
         def total = reviewApiService.count(listing?.id)

            def details = []
            def detail = Review.createCriteria().list(max:3, offset:offset) {
                createAlias("listing", "l")
                eq("l.id",listing?.id)
                eq("status", "COMPLETED")

            }
                detail.each {
                    def mul = reviewApiService.multimedia(it?.id)
                    details.add(
                        'userName':reviewApiService.userById(it?.user?.id)?.user?.firstname +" "+reviewApiService.userById(it?.user?.id)?.user?.lastname,
                        'userPhoto':reviewApiService.userById(it?.user?.id)?.user?.profile,
                        'star':it?.star,
                        'description': it?.description,
                        'videoUrl': it?.videoUrl,
                        'blogUrl': it?.blogUrl,
                        'updatedAt':it?.updatedAt,
                        'multimedia':mul
                    )
                }
            // respond details
            result['count']=total
            result['reviews']=details
            respond result
        
    }



    def reviews(String referenceId ,Integer star,Integer offset){
        def listing = Listing.findByReferenceId(referenceId)
        def comment=[]
            def comments = Review.createCriteria().list(max:3, offset:offset) {
                createAlias("listing", "l")
                eq("l.id",listing?.id)
                eq("star",star)
                eq("status", "COMPLETED")
            }
            comments.each {
                def mul = reviewApiService.multimedia(it?.id)
                    comment.add(
                            'userName':reviewApiService.userById(it?.user?.id)?.user?.firstname +" "+reviewApiService.userById(it?.user?.id)?.user?.lastname,
                            'userPhoto':reviewApiService.userById(it?.user?.id)?.user?.profile,
                        'description': it?.description,
                        'videoUrl': it?.videoUrl,
                        'blogUrl': it?.blogUrl,
                        'updatedAt':it?.updatedAt,
                        'star':it?.star,
                        'multimedia':mul
                    )
                }
        respond comment
    }

    def myReview(Long userId){
        def list=[]
            def userList = Review.createCriteria().list() {
                createAlias("user", "u")
                eq("u.id",userId)
            }
            userList.each {
                    def mul = reviewApiService.multimedia(it?.id)
                    list.add(
                        'description': it?.description,
                        'videoUrl': it?.videoUrl,
                        'blogUrl': it?.blogUrl,
                        'createdAt':it?.createdAt,
                        'updatedAt':it?.updatedAt,
                        'star':it?.star,
                        'status':it?.status,
                        'listingSlug':it?.listing?.slug,
                        'listingTitle':it?.listing?.title,
                        'multimedia':mul
                    )
                }
        respond list
    }


}
