package goglides.api

import com.goglides.SiteSetting
import com.goglides.manage.SiteSettingService

class BootStrap {

    def init = { servletContext ->
        /**
         * Check whether site settings exists.
         * If not then insert setting parameters and default values.
         */
        if (!SiteSetting.findBySlug('site-url')) {
            new SiteSetting(title: 'Site Url', slug: 'site-url').save(flush: true)
        }
        if (!SiteSetting.findBySlug('site-name')) {
            new SiteSetting(title: 'Site Name', slug: 'site-name', value: 'Goglides').save(flush: true)
        }
        if (!SiteSetting.findBySlug('tagline')) {
            new SiteSetting(title: 'Tagline', slug: 'tagline', value: 'Thrill with gliding experience').save(flush: true)
        }
        if (!SiteSetting.findBySlug('admin-email')) {
            new SiteSetting(title: 'Admin Email', slug: 'admin-email', value: 'info@goglides.com').save(flush: true)
        }
        if (!SiteSetting.findBySlug('address1')) {
            new SiteSetting(title: 'Address 1', slug: 'address1').save(flush: true)
        }
        if (!SiteSetting.findBySlug('address2')) {
            new SiteSetting(title: 'Address 2', slug: 'address2').save(flush: true)
        }
        if (!SiteSetting.findBySlug('phone1')) {
            new SiteSetting(title: 'Primary Contact No.', slug: 'phone1', value: '+977-14219235').save(flush: true)
        }
        if (!SiteSetting.findBySlug('phone2')) {
            new SiteSetting(title: 'Secondary Contact No.', slug: 'phone2', value: '+977-14263722').save(flush: true)
        }
        if (!SiteSetting.findBySlug('fax1')) {
            new SiteSetting(title: 'Fax 1', slug: 'fax1').save(flush: true)
        }
        if (!SiteSetting.findBySlug('fax2')) {
            new SiteSetting(title: 'Fax 2', slug: 'fax2').save(flush: true)
        }
        if (!SiteSetting.findBySlug('copyright')) {
            new SiteSetting(title: 'Copyright', slug: 'copyright', value: 'GoGlides. All rights reserved').save(flush: true)
        }
        if (!SiteSetting.findBySlug('facebook')) {
            new SiteSetting(title: 'Facebook', slug: 'facebook').save(flush: true)
        }
        if (!SiteSetting.findBySlug('twitter')) {
            new SiteSetting(title: 'Twitter', slug: 'twitter').save(flush: true)
        }
        if (!SiteSetting.findBySlug('instagram')) {
            new SiteSetting(title: 'Instagram', slug: 'instagram').save(flush: true)
        }
        if (!SiteSetting.findBySlug('google-plus')) {
            new SiteSetting(title: 'Google +', slug: 'google-plus').save(flush: true)
        }
        if (!SiteSetting.findBySlug('linkedin')) {
            new SiteSetting(title: 'Linkedin', slug: 'linkedin').save(flush: true)
        }
        if (!SiteSetting.findBySlug('google-map')) {
            new SiteSetting(title: 'Google Map', slug: 'google-map').save(flush: true)
        }
        if (!SiteSetting.findBySlug('google-api-key')) {
            new SiteSetting(title: 'Google API Key', slug: 'google-api-key').save(flush: true)
        }
        if (!SiteSetting.findBySlug('business-hour')) {
            new SiteSetting(title: 'Business Hours', slug: 'business-hour').save(flush: true)
        }
        if (!SiteSetting.findBySlug('site-mode')) {
            new SiteSetting(title: 'Site Mode', slug: 'site-mode').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-mode')) {
            new SiteSetting(title: 'Paypal Mode', slug: 'paypal-mode').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-client-id-sandbox')) {
            new SiteSetting(title: 'PayPal Client Id Sandbox', slug: 'paypal-client-id-sandbox').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-client-id')) {
            new SiteSetting(title: 'PayPal Client Id', slug: 'paypal-client-id').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-client-secret-sandbox')) {
            new SiteSetting(title: 'PayPal Client Secret Sandbox', slug: 'paypal-client-secret-sandbox').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-client-secret')) {
            new SiteSetting(title: 'PayPal Client Secret', slug: 'paypal-client-secret').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-endpoint-sandbox')) {
            new SiteSetting(title: 'PayPal Endpoint Sandbox', slug: 'paypal-endpoint-sandbox').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-endpoint')) {
            new SiteSetting(title: 'PayPal Endpoint', slug: 'paypal-endpoint').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-currencyCode')) {
            new SiteSetting(title: 'Paypal Currency Code', slug: 'paypal-currency-code', value: 'USD').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-return-url')) {
            new SiteSetting(title: 'Paypal Return Url', slug: 'paypal-return-url').save(flush: true)
        }
        if (!SiteSetting.findBySlug('paypal-cancel-url')) {
            new SiteSetting(title: 'Paypal Cancel Url', slug: 'paypal-cancel-url').save(flush: true)
        }
        if (!SiteSetting.findBySlug('meta-title')) {
            new SiteSetting(title: 'Meta Title', slug: 'meta-title').save(flush: true)
        }
        if (!SiteSetting.findBySlug('meta-keyword')) {
            new SiteSetting(title: 'Meta Keyword', slug: 'meta-keyword').save(flush: true)
        }
        if (!SiteSetting.findBySlug('meta-description')) {
            new SiteSetting(title: 'Meta Description', slug: 'meta-description').save(flush: true)
        }
        if (!SiteSetting.findBySlug('keycloak-auth-server-url')) {
            new SiteSetting(title: 'Auth Server Url', slug: 'keycloak-auth-server-url').save(flush: true)
        }
        if (!SiteSetting.findBySlug('keycloak-realm-id')) {
            new SiteSetting(title: 'Realm ID', slug: 'keycloak-realm-id').save(flush: true)
        }
        if (!SiteSetting.findBySlug('keycloak-client-id')) {
            new SiteSetting(title: 'Client ID', slug: 'keycloak-client-id').save(flush: true)
        }
        if (!SiteSetting.findBySlug('keycloak-client-secret')) {
            new SiteSetting(title: 'Client Secret', slug: 'keycloak-client-secret').save(flush: true)
        }
        if (!SiteSetting.findBySlug('keycloak-username')) {
            new SiteSetting(title: 'Username', slug: 'keycloak-username').save(flush: true)
        }
        if (!SiteSetting.findBySlug('keycloak-password')) {
            new SiteSetting(title: 'Password', slug: 'keycloak-password').save(flush: true)
        }
        if (!SiteSetting.findBySlug('khalti-endpoint-url')) {
            new SiteSetting(title: 'Khalti API Endpoint URL', slug: 'khalti-endpoint-url').save(flush: true)
        }
        if (!SiteSetting.findBySlug('khalti-mode')) {
            new SiteSetting(title: 'Khalti Mode', slug: 'khalti-mode', value: 'test').save(flush: true)
        }
        if (!SiteSetting.findBySlug('khalti-secret-key')) {
            new SiteSetting(title: 'Khalti Live Secret Key', slug: 'khalti-secret-key').save(flush: true)
        }
        if (!SiteSetting.findBySlug('khalti-secret-key-test')) {
            new SiteSetting(title: 'Khalti Test Secret Key', slug: 'khalti-secret-key-test').save(flush: true)
        }
        if (!SiteSetting.findBySlug('khalti-public-key')) {
            new SiteSetting(title: 'Khalti Public Key', slug: 'khalti-public-key').save(flush: true)
        }
        if (!SiteSetting.findBySlug('khalti-public-key-test')) {
            new SiteSetting(title: 'Khalti Test Public Key', slug: 'khalti-public-key-test').save(flush: true)
        }
    }
    def destroy = {
    }
}
