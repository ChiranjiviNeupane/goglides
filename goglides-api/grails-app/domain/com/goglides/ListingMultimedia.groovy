package com.goglides

import com.goglides.common.CommonStatus

class ListingMultimedia {
    User user
    BusinessDirectory businessDirectory
    Listing listing

    String type
    String fileUrl
    int isApproved
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing]

    static constraints = {
        type blank: true, nullable: true
        fileUrl blank: true, nullable: true
        isApproved blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'listing_multimedia'

        isApproved column: "is_approved", sqlType: "tinyint", length: 6
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }
}
