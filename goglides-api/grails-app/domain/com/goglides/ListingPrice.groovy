package com.goglides

import com.goglides.common.CommonStatus

class ListingPrice {
    User user
    BusinessDirectory businessDirectory
    Listing listing
	
	String referenceId
    String type
    String duration
    String schedule
    String nationality
    int ageGroupRate
    int stockUnit
    int availableStockUnit
    int enableSale
    BigDecimal wholesaleRate
    BigDecimal retailRate
    BigDecimal salesRate
    BigDecimal seniorWholesaleRate
    BigDecimal seniorRetailRate
    BigDecimal seniorSalesRate
    BigDecimal childWholesaleRate
    BigDecimal childRetailRate
    BigDecimal childSalesRate
    BigDecimal infantWholesaleRate
    BigDecimal infantRetailRate
    BigDecimal infantSalesRate
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing]

    static constraints = {
		referenceId blank: false, nullable: false
        ageGroupRate blank: true, nullable: true
        nationality blank: true, nullable: true
        type blank: true, nullable: true
        duration blank: true, nullable: true
        schedule blank: true, nullable: true
        stockUnit blank: true, nullable: true
        availableStockUnit blank: true, nullable: true
        wholesaleRate blank: true, nullable: true
        retailRate blank: true, nullable: true
        salesRate blank: true, nullable: true
        seniorWholesaleRate blank: true, nullable: true
        seniorRetailRate blank: true, nullable: true
        seniorSalesRate blank: true, nullable: true
        childWholesaleRate blank: true, nullable: true
        childRetailRate blank: true, nullable: true
        childSalesRate blank: true, nullable: true
        infantWholesaleRate blank: true, nullable: true
        infantRetailRate blank: true, nullable: true
        infantSalesRate blank: true, nullable: true
        enableSale blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }
    static mapping = {
        table 'listing_price'

		referenceId column: "reference_id", length: 255
        enableSale column: "enable_sale", sqlType: "tinyint", length: 6, defaultValue: 0
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        if (!schedule) {
            schedule = '00:00'
        }
        if (!duration) {
            duration = '00:00:00:00'
        }
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
		if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }
	
	def static generateReferenceId(int length) {
        def unique = false
        def refId
        def listingPrice
        while (!unique) {
            refId = UUID.randomUUID().toString()
            listingPrice = ListingPrice.findByReferenceId(refId)
            if (!listingPrice)
                unique = true
        }
        return refId
    }
}
