package com.goglides

class Banner {
    User user
        
    String title
    String path
    String description
    String listingSlug
    String userName
    String userEmail
    boolean status = false
    boolean statusAdd = false
    boolean showQuote = false
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User]  

    static constraints = {
        title blank:true , nullable:true
        userName blank:true , nullable:true
        userEmail blank:true , nullable:true
        path blank:true , nullable:true
        createdAt nullable:true
        updatedAt nullable:true
        deletedAt nullable:true
        description blank:true , nullable:true
        link blank:true , nullable:true
    }
    
    static mapping={
        table 'banner'

        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
        description column: "description", sqlType: "text"
    }
}
