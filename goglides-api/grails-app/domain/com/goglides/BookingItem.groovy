package com.goglides

class BookingItem {
    BusinessDirectory businessDirectory
    Listing listing
    ListingPrice listingPrice
    Booking booking
    User user

    String referenceId
    Date bookingDate
    String schedule
    String duration
    String type
    String category
    String ageGroup
    BigDecimal retailRate
    int ticketNo
    String title
    String fullName
    String contactNo
    String email
    String nationality
    String identificationType
    String identificationNo
    String weight
    int addAddon

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing, listingPrice: ListingPrice, booking: Booking]
    static hasMany = [bookingAddon: BookingItemAddon]

    static constraints = {
        schedule blank: true, nullable: true
        duration blank: true, nullable: true
        category blank: true, nullable: true
        ageGroup blank: true, nullable: true
        retailRate blank: true, nullable: true
        ticketNo blank: true, nullable: true
        title blank: true, nullable: true
        fullName blank: true, nullable: true
        contactNo blank: true, nullable: true
        email blank: true, nullable: true
        nationality blank: true, nullable: true
        identificationType blank: true, nullable: true
        identificationNo blank: true, nullable: true
        weight blank: true, nullable: true
        addAddon blank: true, nullable: true
    }

    static mapping = {
        table 'booking_item'

        bookingDate column: "booking_date", sqlType: "date"
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId()
        }
    }

    def static generateReferenceId() {
        def unique = false
        def refId
        def item
        while (!unique) {
            refId = UUID.randomUUID().toString()
            item = BookingItem.findByReferenceId(refId)
            if (!item)
                unique = true
        }
        return refId
    }


    def generateInvoice() {
        def unique = false
        def listing
        def prefix = 'GIVN-'
        def count = Booking.count()
        if (!count) {
            count = 1
        } else {
            count = count + 1
        }
        count = count.toString()
        def code
        while (!unique) {
            code = prefix + count.padLeft(10, '0')
            listing = Booking.findByInvoiceNo(code)
            if (!listing) {
                unique = true
            }
            count = count + 1
        }
        return code
    }

    def issueInvoice(booking) {
        booking.invoiceNo = generateInvoice()
        booking.save(flush: true)
    }



}