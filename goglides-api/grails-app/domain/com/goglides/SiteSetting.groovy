package com.goglides

import org.apache.commons.lang.RandomStringUtils

class SiteSetting {
    def commonService
    String referenceId
    String title
    String slug
    String value
    Date createdAt
    Date updatedAt
    Date deletedAt

    static constraints = {
        referenceId blank: false, nullable: false
        title blank: false, nullable: false, unique: true
        slug blank: false, nullable: false
        value blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'site_setting'

        referenceId column: "reference_id", length: 255
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"

    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!slug) {
            slug = commonService.slug(title)
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def siteSetting
        while (!unique) {
            refId = UUID.randomUUID().toString()
            siteSetting = SiteSetting.findByReferenceId(refId)
            if (!siteSetting)
                unique = true
        }
        return refId
    }


}
