package com.goglides

class BookingItemAddon {
    BusinessDirectory businessDirectory
    Listing listing
    ListingAddon listingAddon
    Booking booking
    BookingItem bookingItem
    BigDecimal retailRate
    User user

    String referenceId

    static belongsTo = [businessDirectory: BusinessDirectory, listing: Listing, user: User, listingAddon: ListingAddon, booking: Booking, bookingItem: BookingItem]

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId()
        }
    }

    def static generateReferenceId() {
        def unique = false
        def refId
        def addon
        while (!unique) {
            refId = UUID.randomUUID().toString()
            addon = BookingItemAddon.findByReferenceId(refId)
            if (!addon)
                unique = true
        }
        return refId
    }
}