package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class ListingFact {
    String referenceId
    Long parentId
    User user
    BusinessDirectory businessDirectory
    Listing listing

    String tripGrade
    int tripStandard
    String bestSeason
    String startPoint
    String endPoint
    String startElevation
    String lowestElevation
    String highestElevation
    int daysCount
    int nightCount
    int maxCapacity
    int availableCapacity
    int minPax
    int maxPax
    int minWeight
    int maxWeight
    int minAge
    int maxAge
    String bridgeHeight
    String riverDistance
    String healthCondition
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing]

    static constraints = {
        referenceId blank: true, nullable: true
        parentId blank: true, nullable: true
        daysCount blank: true, nullable: true
        nightCount blank: true, nullable: true
        minAge blank: true, nullable: true
        maxAge blank: true, nullable: true
        minWeight blank: true, nullable: true
        maxWeight blank: true, nullable: true
        minPax blank: true, nullable: true
        maxPax blank: true, nullable: true
        maxCapacity blank: true, nullable: true
        availableCapacity blank: true, nullable: true   
        tripStandard blank: true, nullable: true
        highestElevation blank: true, nullable: true
        lowestElevation blank: true, nullable: true
        startElevation blank: true, nullable: true
        startPoint blank: true, nullable: true
        endPoint blank: true, nullable: true
        tripGrade blank: true, nullable: true
        bestSeason blank: true, nullable: true
        status blank: true, nullable: true
        healthCondition blank: true, nullable: true
        riverDistance blank: true, nullable: true
        bridgeHeight blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'listing_fact'

        referenceId column: "reference_id", length: 255
        parentId column: "parent_id", length: 20
        tripStandard column: "trip_standard", sqlType: "tinyInt", length: 6, defaultValue: 0
        daysCount column: "days_count", sqlType: "tinyInt", length: 6, defaultValue: 0
        nightCount column: "night_count", sqlType: "tinyInt", length: 6, defaultValue: 0
        maxCapacity column: "max_capacity", sqlType: "tinyInt", length: 6, defaultValue: 0
        availableCapacity column: "available_capacity", sqlType: "tinyInt", length: 6, defaultValue: 0
        minPax column: "min_pax", sqlType: "tinyInt", length: 6, defaultValue: 0
        maxPax column: "max_pax", sqlType: "tinyInt", length: 6, defaultValue: 0
        minWeight column: "min_weight", sqlType: "tinyInt", length: 6, defaultValue: 0
        bestSeason column: "best_season", sqlType: "text"
        healthCondition column: "health_condition", sqlType: "text"
        maxWeight column: "max_Weight", sqlType: "tinyInt", length: 6, defaultValue: 0
        minAge column: "min_age", sqlType: "tinyInt", length: 6, defaultValue: 0
        maxAge column: "max_age", sqlType: "tinyInt", length: 6, defaultValue: 0
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def listingFact
        while (!unique) {
            refId = UUID.randomUUID().toString()
            listingFact = ListingFact.findByReferenceId(refId)
            if (!listingFact)
                unique = true
        }
        return refId
    }
}
