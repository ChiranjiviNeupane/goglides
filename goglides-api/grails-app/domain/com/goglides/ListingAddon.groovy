package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class ListingAddon {
    User user
    BusinessDirectory businessDirectory
    Listing listing

	String referenceId
    int type
    String title
    BigDecimal wholesaleRate
    BigDecimal retailRate
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing]

    static constraints = {
		referenceId blank: false, nullable: false
        title blank: false, nullable: false
        type blank: true, nullable: true
        wholesaleRate blank: true, nullable: true
        retailRate blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'listing_addons'
		
		referenceId column: "reference_id", length: 255
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
		if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

	def static generateReferenceId(int length) {
        def unique = false
        def refId
        def listingAddon
        while (!unique) {
            refId = UUID.randomUUID().toString()
            listingAddon = ListingAddon.findByReferenceId(refId)
            if (!listingAddon)
                unique = true
        }
        return refId
    }

}
