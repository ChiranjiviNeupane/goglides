package com.goglides

class User {
    String referenceId

    static constraints = {
        referenceId blank: false, nullable: false
    }
    
    static mapping = {
        table 'users'

        reference column: "reference_id", length: 255
    }
}
