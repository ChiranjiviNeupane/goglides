package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class Listing {
    def commonService
    User user
    BusinessDirectory businessDirectory
    Category category
    String referenceId
    Long parentId
    String title
    String slug
    String code
    String tripCode
    String highlight
    String description
    String address
    String city
    String country
    double latitude
    double longitude
    String nearestCity
    String nearestAirport
    int isVisible
    int isVerified
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt
    int isFeatured
    Date featuredStart
    Date featuredEnd
    int policyType
    int instantBooking
    String metaTitle
    String metaKeyword
    String metaDescription

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, category: Category]
    static hasMany = [listingAddon: ListingAddon, listingChecklist: ListingChecklist, listingDestination: ListingDestination, listingFact: ListingFact, listingIncludeExclude: ListingIncludeExclude, listingItinerary: ListingItinerary, listingMultimedia: ListingMultimedia, listingPrice: ListingPrice, listingTransportRoute: ListingTransportRoute, listingFaq: ListingFaq, booking: Booking, contact: Contact, wishList: WishList,review:Review]

    static constraints = {
        referenceId blank: false, nullable: false
        parentId nullable: true
        title blank: false, nullable: false, unique: true
        slug blank: true, nullable: true
        code blank: true, nullable: true
        tripCode blank: true, nullable: true
        address blank: true, nullable: true
        city blank: true, nullable: true
        country blank: true, nullable: true
        latitude nullable: true
        longitude nullable: true
        nearestCity blank: true, nullable: true
        nearestAirport blank: true, nullable: true
        description blank: false, nullable: true
        highlight blank: false, nullable: true
        isVerified nullable: true
        isVisible nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
        isFeatured nullable: true, blank: true
        featuredStart nullable: true, blank: true
        featuredEnd nullable: true, blank: true
        policyType nullable: true, blank: true
        instantBooking nullable: true, blank: true
        metaTitle nullable: true, blank: true
        metaKeyword nullable: true, blank: true
        metaDescription nullable: true, blank: true

    }

    static mapping = {
        table 'listing'

        referenceId column: "reference_id", length: 255
        parentId column: "parent_id", length: 20, defaultValue: 0
        highlight column: "highlight", sqlType: "text"
        description column: "description", sqlType: "text"
        latitude column: "latitude", sqlType: "double"
        longitude column: "longitude", sqlType: "double"
        isVerified column: "is_verified", sqlType: "tinyInt", length: 6, defaultValue: 0
        isVisible column: "is_visible", sqlType: "tinyInt", length: 6, defaultValue: 0
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
        isFeatured column: "is_featured", sqlType: "tinyInt", length: 6, defaultValue: 0
        featuredStart column: "featured_start", sqlType: "datetime"
        featuredEnd column: "featured_end", sqlType: "datetime"
        policyType column: "policy_type", sqlType: "tinyInt", length: 6, defaultValue: 0
        instantBooking column: "instant_booking", sqlType: "tinyInt", length: 6, defaultValue: 0
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!code) {
            code = generateCode()
        }
        if (!slug) {
            slug = commonService.slug(title)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def listing
        while (!unique) {
            refId = UUID.randomUUID().toString()
            listing = Listing.findByReferenceId(refId)
            if (!listing)
                unique = true
        }
        return refId
    }

    def generateCode() {
        def unique = false
        def listing
        def prefix = 'GC-'
        def count = Listing.count()
        if (!count) {
            count = 1
        } else {
            count = count + 1
        }
        count = count.toString()
        def code
        while (!unique) {
            code = prefix + count.padLeft(10, '0')
            listing = Listing.findByCode(code)
            if (!listing) {
                unique = true
            }
            count = count + 1
        }
        return code
    }
}
