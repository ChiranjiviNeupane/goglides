package com.goglides

class ListingIncludeExclude {

    Long parentId
    User user
    BusinessDirectory businessDirectory
    Listing listing

    String type
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [businessDirectory: BusinessDirectory, listing: Listing, user: User, includeExclude: IncludeExclude]
    static constraints = {
        parentId blank: true, nullable: true
        type blank: false, nullable: false
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }
    static mapping = {
        table 'listing_includes_excludes'

        parentId column: "parent_id", defaultValue: 0
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
    }
}
