package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class Forex {
    String referenceId
    String countryCode
    String currencyCode
    Double rate
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static constraints = {
        referenceId blank: false,nullable: true
        rate blank: false,nullable: true
        countryCode blank: false,nullable: true, unique: true
        currencyCode blank: false, nullable: true, unique: true
        status blank: false, nullable: true
        createdAt blank: false, nullable: true
        updatedAt blank: false, nullable: true
        deletedAt blank: false, nullable: true


    }

    static mapping = {
        table 'forex'

        referenceId column: "reference_id", length: 255
        countryCode column: "country_code", length: 60
        currencyCode column: "currency_code", length: 60
        status column: "status", length: 255
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def forex
        while (!unique) {
            refId = UUID.randomUUID().toString()
            forex = Page.findByReferenceId(refId)
            if (!forex)
                unique = true
        }
        return refId
    }
}
