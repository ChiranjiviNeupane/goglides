package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class ListingChecklist {
    User user
    BusinessDirectory businessDirectory
    Listing listing
    CheckList checkList

    String referenceId
    String type
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing, checkList: CheckList]

    static constraints = {
        type blank: false, nullable: false
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'listing_check_list'

        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }


    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def listingCheckList
        while (!unique) {
            refId = UUID.randomUUID().toString()
            listingCheckList = ListingChecklist.findByReferenceId(refId)
            if (!listingCheckList)
                unique = true
        }
        return refId
    }
}
