package com.goglides

class WishList {
    User user
    Listing listing
    Date createdAt
    Date updatedAt

    static belongsTo = [user: User, listing: Listing]

    static constraints = {
        createdAt nullable: true
        updatedAt nullable: true

    }
    static mapping = {
        table 'wish_list'

        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
    }
    def beforeInsert() {
        createdAt = new Date()
    }
}
