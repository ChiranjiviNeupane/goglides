package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class IncludeExclude {
    def commonService
    User user

    String referenceId
    String title
    String slug
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [category: Category ,user: User]
    static hasMany = [listingIncludeExclude: ListingIncludeExclude]

    static constraints = {
        referenceId blank: false, nullable: false
        title blank: false, nullable: false
        slug blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'include_exclude'

        referenceId column: "reference_id", length: 255
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!slug) {
            slug = commonService.slug(title)
        }
        if (!status) {

            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def includeExclude
        while (!unique) {
            refId = UUID.randomUUID().toString()
            includeExclude = IncludeExclude.findByReferenceId(refId)
            if (!includeExclude)
                unique = true
        }
        return refId
    }
}
