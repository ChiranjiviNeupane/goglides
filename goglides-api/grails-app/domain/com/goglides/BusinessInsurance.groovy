package com.goglides

import com.goglides.common.CommonStatus

class BusinessInsurance {

    BusinessDirectory businessDirectory
    User user

    String referenceId
    String insuranceCompany
    String coverageAmount
    String expiryDate
    String fileUrl
    int isApproved
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [businessDirectory: BusinessDirectory, user: User]

    static constraints = {
        isApproved nullable: false
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'business_insurance'

        referenceId column: "reference_id", length: 255
        isApproved defaultValue: 0, column: "is_approved", sqlType: "tinyint", length: 6
        fileUrl column: "file_url", length: 500
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }


    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def business
        while (!unique) {
            refId = UUID.randomUUID().toString()
            business = BusinessDirectory.findByReferenceId(refId)
            if (!business)
                unique = true
        }
        return refId
    }

}