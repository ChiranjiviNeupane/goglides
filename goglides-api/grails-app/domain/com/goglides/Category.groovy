package com.goglides

import com.goglides.common.CommonStatus

class Category {
    def commonService
    User user

    String referenceId
    String code
    String title
    String slug
    String bannerUrl
    int isVisible
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User]
    static hasMany = [checklist: CheckList, includeExclude: IncludeExclude]

    static constraints = {
        user blank: true, nullable: true
        referenceId blank: true, nullable: true
        code unique: true
        title blank: true, nullable: true, unique: true
        slug blank: true, nullable: true
        bannerUrl blank: true, nullable: true
        isVisible blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'category'

        referenceId column: "reference_id", length: 255
        isVisible column: "is_visible", sqlType: "tinyInt", length: 6, defaultValue: 0
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!slug) {
            slug = commonService.slug(title)
        }
        if (!code) {
            code = generateCode()
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def category
        while (!unique) {
            refId = UUID.randomUUID().toString()
            category = Category.findByReferenceId(refId)
            if (!category)
                unique = true
        }
        return refId
    }

    def generateCode() {
        def unique = false
        def prefix = 'GCAT-'
        def count = Category.count()
        if (!count) {
            count = 1
        } else {
            count = count + 1
        }
        def code
        while (!unique) {
            count = count.toString()
            code = prefix + count.padLeft(10, '0')
            def category = Category.findByCode(code)
            if (!category) {
                unique = true
            }
            count = count.toInteger() + 1
        }
        return code
    }
}