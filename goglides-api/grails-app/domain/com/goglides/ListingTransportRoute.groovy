package com.goglides

import com.goglides.common.CommonStatus

class ListingTransportRoute {
    User user
    BusinessDirectory businessDirectory
    Listing listing

    int pickUp
    int dropOff
    String address
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing]

    static constraints = {
        pickUp blank: true, nullable: true
        dropOff blank: true, nullable: true
        address blank: false, nullable: true
        status blank: false, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'listing_transport_route'

        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }
}
