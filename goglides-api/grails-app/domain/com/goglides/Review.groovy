package com.goglides

class Review {
    User user
    Listing listing
    int star
    String description
    String videoUrl
    String blogUrl
    String status
    String userEmail
    Date createdAt
    Date updatedAt
    Date deletedAt
    static belongsTo = [user: User, listing: Listing]
    static hasMany = [multimedia:Multimedia]

    static constraints = {
        videoUrl blank:true , nullable:true
        blogUrl blank:true , nullable:true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable:true
        userEmail nullable:true , blank:true
    }
    static mapping = {
        table 'review'
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        createdAt column: "created_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
        description column: "description", sqlType: "text"
        status column: "status" , sqlType: "varchar"
    }
    
    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

     def beforeUpdate() {
        updatedAt = new Date()
    }
}
