package com.goglides.common

public enum PaymentStatus {
    STATUS_PENDING('PENDING'),
    STATUS_PAID('PAID'),
    STATUS_REVERSED('REVERSED'),
    STATUS_REFUNDED('REFUNDED'),
    STATUS_FAILED('FAILED'),
    STATUS_CANCELLED('CANCELLED')

    private final String status

    PaymentStatus(String status) {
        this.status = status
    }

    public String getStatus() {
        return this.status
    }

}
