package com.goglides.common

public enum BookingStatus {

    STATUS_PENDING('PENDING'),
    STATUS_TRASH('TRASH'),
    STATUS_AWAITING_APPROVAL('AWAITING_APPROVAL'),
    STATUS_DECLINED('DECLINED'),
    STATUS_CLOSED('CLOSED'),
    STATUS_COMPLETED('COMPLETED'),
    STATUS_CANCELLATION_REQUEST('CANCELLATION_REQUEST'),
    STATUS_CANCELLED('CANCELLED'),
    STATUS_CONFIRMED('CONFIRMED'),
    STATUS_RESERVED('RESERVED')

    private final String status

    BookingStatus(String status) {
        this.status = status
    }

    public String getStatus() {
        return this.status
    }

}
