package com.goglides.common

public enum CommonStatus {

    STATUS_DRAFT('DRAFT'),
    STATUS_PUBLISH('PUBLISH'),
    STATUS_TRASH('TRASH'),
    STATUS_AWAITING_APPROVAL('AWAITING_APPROVAL'),
    STATUS_DECLINED('DECLINED'),
    STATUS_CLOSED('CLOSED')

    private final String status

    CommonStatus(String status) {
        this.status = status
    }

    public String getStatus() {
        return this.status
    }

}
