package com.goglides

class BusinessContactPerson {

    def commonService
    User user
    BusinessDirectory businessDirectory
    String contactPersonJobTitle
    String contactPersonFirstName
    String contactPersonLastName
    String contactPersonEmail
    String contactPersonMobile
    String contactPersonPrimaryPhone
    String contactPersonSecondaryPhone
    Date createdAt
    Date updatedAt
    Date deletedAt
   static belongsTo = [businessDirectory: BusinessDirectory, user: User]
    static constraints = {
        contactPersonFirstName blank: true, nullable: false
        contactPersonLastName blank: true, nullable: false
        contactPersonJobTitle blank: true, nullable: true
        contactPersonEmail blank: true, nullable: true
        contactPersonMobile blank: true, nullable: true
        contactPersonPrimaryPhone blank: true, nullable: true
        contactPersonSecondaryPhone blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }
    static mapping = {
        table 'business_contact_person'
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }
    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }
    def beforeUpdate() {
        updatedAt = new Date()

    }

}
