package com.goglides


class Test {
    String testName
    String checkVersion

    static constraints = {
        testName blank:false
    }
}
