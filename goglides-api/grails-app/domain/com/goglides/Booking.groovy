package com.goglides

import com.goglides.common.BookingStatus
import com.goglides.common.PaymentStatus
import org.apache.commons.lang.RandomStringUtils

class Booking {

    BusinessDirectory businessDirectory
    Listing listing
    User user

    String referenceId
    String code
    String invoiceNo
    Date bookingDate
    String type
    String schedule
    String duration
    String bookingMode
    String paymentMode
    String paymentStatus
    String bookingStatus
    String currencyCode
    String categories
    BigDecimal currencyRate
    int paxTotalCount
    BigDecimal paxTotal
    BigDecimal addonTotal
    BigDecimal total
    BigDecimal grandTotal
    String emergencyContactName
    String emergencyContactNumber
    String clientName
    String clientContact
    String clientEmail
    String comment
    String pickup
    String dropOff
    String transactionId
    String payerId
    String payerToken
    Date pickupAt
    Date createdAt
    Date updatedAt
    Date deletedAt
    String cart
    String addonsCart
    String msgCancellation

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing]
    static hasMany = [bookingItem: BookingItem]

    static constraints = {
        code blank: true, nullable: true
        invoiceNo blank: true, nullable: true
        type blank: true, nullable: true
        schedule blank: true, nullable: true
        duration blank: true, nullable: true
        paymentMode blank: true, nullable: true
        paymentStatus blank: true, nullable: true
        bookingStatus blank: true, nullable: true
        currencyCode blank: true, nullable: true
        categories blank: true, nullable: true
        currencyRate blank: true, nullable: true
        paxTotalCount blank: true, nullable: true
        paxTotal blank: true, nullable: true
        addonTotal blank: true, nullable: true
        total blank: true, nullable: true
        grandTotal blank: true, nullable: true
        emergencyContactName blank: true, nullable: true
        emergencyContactNumber blank: true, nullable: true
        clientName blank: true, nullable: true
        clientContact blank: true, nullable: true
        clientEmail blank: true, nullable: true
        comment blank: true, nullable: true
        pickup blank: true, nullable: true
        dropOff blank: true, nullable: true
        transactionId blank: true, nullable: true
        bookingMode blank: true, nullable: true
        payerId blank: true, nullable: true
        payerToken blank: true, nullable: true
        pickupAt blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
        cart blank: true, nullable: true
        addonsCart blank: true, nullable: true
        msgCancellation blank: true, nullable: true
    }

    static mapping = {
        table 'booking'

        bookingDate column: "booking_date", sqlType: "date"
        paxTotalCount column: "total_pax", sqlType: "tinyInt", length: 6, defaultValue: 0
        pickupAt column: "pickup_at", sqlType: "datetime"
        comment column: "comment", sqlType: "text"
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
        cart column: "cart", sqlType: "text"
        addonsCart column: "addons_cart", sqlType: "text"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (bookingStatus == BookingStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
        if (paymentStatus == PaymentStatus.STATUS_PAID.getStatus() && invoiceNo == null) {
            invoiceNo = generateInvoiceNo()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId()
        }
        if (!code) {
            code = generateCode(6)
        }
        if (!bookingStatus) {
            bookingStatus = BookingStatus.STATUS_PENDING.getStatus()
        }
        if (!paymentStatus) {
            paymentStatus = PaymentStatus.STATUS_PENDING.getStatus()
        }
    }

    def static generateReferenceId() {
        def unique = false
        def refId
        def listing
        while (!unique) {
            refId = UUID.randomUUID().toString()
            listing = Listing.findByReferenceId(refId)
            if (!listing)
                unique = true
        }
        return refId
    }

    def generateCode(int length) {
        def unique = false
        while (!unique) {
            String charset = (('a'..'z') + ('0'..'9')).join()
            code = RandomStringUtils.random(length, charset.toCharArray())
            def booking = Booking.findByCode(code)
            if (!booking) {
                unique = true
            }
        }
        return code
    }

    def generateInvoiceNo() {
        def unique = false
        def prefix = 'INV-'
        def count = Booking.countByInvoiceNoIsNotNull()
        if (!count) {
            count = 1
        } else {
            count = count + 1
        }
        def invoiceNo = ''
        while (!unique) {
            invoiceNo = count.toString()
            invoiceNo = prefix + invoiceNo.padLeft(10, '0')
            def booking = Booking.findByInvoiceNo(invoiceNo)
            if (!booking) {
                unique = true
            }
            count = count + 1
        }
        return invoiceNo
    }
}