package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class BusinessDirectory {

    def commonService
    User user
    String referenceId
    Long parentId
    String businessName
    String registrationNumber
    String slug
    String businessType
    String email
    String phone1
    String phone2
    String fax
    String address
    String city
    String street
    String state
    String country
    double latitude
    double longitude
    String description
    String logoUrl
    String website
    String metaTitle
    String metaKeyword
    String metaDescription
    int isVerified
    int isClaimable
    int isVisible
    String facebookUrl
    String twitterUrl
    String googlePlusUrl
    String linkedInUrl
    String instagramUrl
    String youtubeUrl
    String tumblerUrl
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static hasMany = [user: User, businessCertificate: BusinessCertificate, businessStaff: BusinessStaff, businessContactPerson: BusinessContactPerson, businessInsurance: BusinessInsurance, booking: Booking,contact: Contact]

    static constraints = {
        referenceId blank: false, nullable: false
        parentId nullable: true
        businessName blank: false, nullable: false, unique: true
        slug blank: false, nullable: false
        businessType blank: false, nullable: false
        email blank: false, nullable: false
        phone1 blank: false, nullable: true
        phone2 blank: true, nullable: true
        address blank: false, nullable: true
        city blank: false, nullable: true
        street blank: false, nullable: true
        state blank: false, nullable: true
        fax blank: false, nullable: true
        country blank: false, nullable: true
        description blank: false, nullable: false
        logoUrl blank: true, nullable: true
        website blank: true, nullable: true
        metaTitle blank: true, nullable: true
        metaKeyword blank: true, nullable: true
        metaDescription blank: true, nullable: true
        isVerified blank: true, nullable: true
        isClaimable blank: true, nullable: true
        isVisible blank: true, nullable: true

        facebookUrl blank: true, nullable: true
        twitterUrl blank: true, nullable: true
        googlePlusUrl blank: true, nullable: true
        linkedInUrl blank: true, nullable: true
        instagramUrl blank: true, nullable: true
        youtubeUrl blank: true, nullable: true
        tumblerUrl blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }


    static mapping = {
        table 'business_directory'

        parentId column: "parent_id", length: 255, defaultValue: "0"
        longitude column: "longitude", sqlType: "double"
        description column: "description", sqlType: "text"
        latitude column: "latitude", sqlType: "double"
        isVerified column: "is_verified", sqlType: "tinyInt", length: 6, defaultValue: 0
        isClaimable column: "is_claimable", sqlType: "tinyInt", length: 6, defaultValue: 0
        isVisible column: "is_visible", sqlType: "tinyInt", length: 6, defaultValue: 0
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }


    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!slug) {

            slug = commonService.slug(businessName)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def business
        while (!unique) {
            refId = UUID.randomUUID().toString()
            business = BusinessDirectory.findByReferenceId(refId)
            if (!business)
                unique = true
        }
        return refId
    }

}
