package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class ListingItinerary {
    User user
    BusinessDirectory businessDirectory
    Listing listing
    String referenceId
    Long parentId
    String title
    String description
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory, listing: Listing]

    static constraints = {
        referenceId blank: true, nullable: true
        parentId blank: true, nullable: true
        title blank: true, nullable: true
        description blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'listing_itinerary'
        referenceId column: "reference_id", length: 255
        parentId column: "parent_id", length: 255, defaultValue: 0
        description column: "description", sqlType: "text"
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def listingItinerary
        while (!unique) {
            refId = UUID.randomUUID().toString()
            listingItinerary = ListingItinerary.findByReferenceId(refId)
            if (!listingItinerary)
                unique = true
        }
        return refId
    }
}
