package com.goglides

class Contact {
    Listing listing
    BusinessDirectory businessDirectory
    String fullName
    String email
    String details
    Date createdAt
    Date updatedAt
    Date deletedAt
    boolean type = false

    static belongsTo = [listing: Listing,businessDirectory: BusinessDirectory]  
    static constraints = {
        fullName blank:true , nullable:true
        email blank:true , nullable:true
        details blank:true , nullable:true
        listing blank:true , nullable:true
        businessDirectory blank:true , nullable:true
        createdAt nullable:true
        updatedAt nullable:true
        deletedAt nullable:true

    }

    static mapping={
        table 'contact'
        details column: "details", sqlType: "text"
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }
    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }
}
