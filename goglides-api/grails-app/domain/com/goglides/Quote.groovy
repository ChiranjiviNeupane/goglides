package com.goglides

class Quote {
    User user
    String description
    String userName
    String userEmail
    boolean status = false
    String source
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User]

    static constraints = {
        source nullable:true , blank:true
        createdAt nullable:true
        deletedAt nullable:true
        updatedAt nullable:true
        userName nullable:true , blank:true
        userEmail nullable:true , blank:true
        user nullable:true , blank:true 
    }
    static mapping={
        table 'quote'
        description column: "description", sqlType: "text"
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }
    def beforeInsert() {
        createdAt = new Date()
    }
}
