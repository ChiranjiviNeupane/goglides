package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils

class Page {
    def commonService
    User user

    String referenceId
    String title
    String slug
    String content
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static constraints = {
        referenceId blank: false, nullable: true
        title blank: false, nullable: true
        slug blank: true, nullable: true
        content blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true

    }

    static mapping = {
        table 'page'

        referenceId column: "reference_id", length: 255
        content column: "content", sqlType: 'Text'
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!slug) {
            slug = commonService.slug(title)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def page
        while (!unique) {
            refId = UUID.randomUUID().toString()
            page = Page.findByReferenceId(refId)
            if (!page)
                unique = true
        }
        return refId
    }
}
