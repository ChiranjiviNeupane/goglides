package com.goglides

import com.goglides.common.CommonStatus
import org.apache.commons.lang.RandomStringUtils


class BusinessCertificate {
    BusinessDirectory businessDirectory
    User user

    String referenceId
    Long parentId
    String fileType
    String fileUrl
    int isApproved
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [businessDirectory: BusinessDirectory, user: User]

    static constraints = {
        referenceId blank: false, nullable: false
        parentId blank: true, nullable: true
        fileType blank: false, nullable: true
        fileUrl blank: false, nullable: true
        isApproved nullable: false
        status blank: false, nullable: false
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'business_certificate'

        referenceId column: "reference_id", length: 255
        parentId column: "parent_id", length: 20
        isApproved defaultValue: 0, column: "is_approved", sqlType: "tinyint", length: 6
        fileType column: "file_type", length: 60
        fileUrl column: "file_url", length: 500
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
        updatedAt=new Date()
    }

    def beforeValidate() {
        if (!referenceId) {
            referenceId = generateReferenceId(9)
        }
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }

    def static generateReferenceId(int length) {
        def unique = false
        def refId
        def business
        while (!unique) {
            refId = UUID.randomUUID().toString()
            business = BusinessCertificate.findByReferenceId(refId)
            if (!business)
                unique = true
        }
        return refId
    }
}