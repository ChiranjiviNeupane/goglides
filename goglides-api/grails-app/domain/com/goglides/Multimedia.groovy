package com.goglides

class Multimedia {
    User user
    Review review
    String fileUrl
    String type
    Date createdAt
    Date updatedAt
    Date deletedAt
    static belongsTo = [user: User,review: Review]  

    static constraints = {
        fileUrl nullable:true , blank:true
        type nullable:true , blank:true
        createdAt nullable:true
        updatedAt nullable:true
        deletedAt nullable:true  
    }

    static mapping={
        table 'multimedia'

        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

     def beforeUpdate() {
        updatedAt = new Date()
    }
}
