package com.goglides

import com.goglides.common.CommonStatus

class BusinessStaff {
    User user
    BusinessDirectory businessDirectory

    String role
    int isOwner
    String status
    Date createdAt
    Date updatedAt
    Date deletedAt

    static belongsTo = [user: User, businessDirectory: BusinessDirectory]

    static constraints = {
        role blank: true, nullable: true
        isOwner blank: false, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
        table 'business_staff'

        role column: "role", length: 255
        isOwner column: "is_owner", sqlType: "tinyint", length: 6, defaultValue: 0
        createdAt column: "created_at", sqlType: "datetime"
        updatedAt column: "updated_at", sqlType: "datetime"
        deletedAt column: "deleted_at", sqlType: "datetime"
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
        if (status == CommonStatus.STATUS_TRASH.getStatus()) {
            deletedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!status) {
            status = CommonStatus.STATUS_DRAFT.getStatus()
        }
    }
}
