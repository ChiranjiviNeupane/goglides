import com.goglides.Test
import grails.rest.render.json.JsonRenderer

// Place your Spring DSL code here
beans = {
    testRenderer(JsonRenderer,Test) {
        excludes = ['id']
    }
}
