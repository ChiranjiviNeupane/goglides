import { HomeComponent } from './components/home/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KeycloakGuard } from './keycloak-service/keycloak.guard';
import { BannerUploadFormComponent } from './components/home/banner-upload-form/banner-upload-form.component';
import { Error404Component } from './components/pages/404/404.component';
const routes: Routes = [
  {
    path:'', component:HomeComponent
  },
  {
    path:'banner',component:BannerUploadFormComponent
  },
  
  {
    path:'listing',loadChildren:'./components/pages/detail-page-lazy.module#DetailPageLazyModule'
  },
  {
    path:'checkout',loadChildren:'./components/pages/booking/booking.module#BookingModule',canActivate: [KeycloakGuard]
  },
  {
    path:'business',loadChildren:'./components/pages/business-page-lazy.module#BusinessPageLazyModule'
  },
  {
    path:'user-profile',loadChildren:'./components/pages/user-profile.module#UserProfileModule',canActivate: [KeycloakGuard]
  },
  {
    path:'notification',loadChildren:'./components/pages/notification.module#NotificationModule',canActivate: [KeycloakGuard]
  },
  {
    path:'about-us',loadChildren:'./components/layouts/footer/about-us/about.module#AboutModule'
  },
  {
    path:'privacy-policy',loadChildren:'./components/layouts/footer/privacy-policy/privacy.module#PrivacyModule'
  },
  {
    path:'contact-us',loadChildren:'./components/layouts/footer/contact-us/contactus.module#ContactusModule'
  },
  {
    path:'terms-and-condition',loadChildren:'./components/layouts/footer/terms-and-conditions/terms.module#TermsModule'
  },
  {
    path:'careers',loadChildren:'./components/layouts/footer/career/career.module#CareerModule'
  },
  {
    path:'category',loadChildren:'./components/pages/category-listing.module#CategoryListingModule'
  },
  {
    path:'mybooking',loadChildren:'./components/pages/mybooking.module#MybookingModule',canActivate: [KeycloakGuard]
  },
  {
    path:'wish-list',loadChildren:'./components/pages/wishlist/wishlist.module#WishlistModule',canActivate: [KeycloakGuard]
  },
  {
    path:'myreviews',loadChildren:'./components/pages/my-reviews/my-reviews.module#MyReviewsModule',canActivate: [KeycloakGuard]
  },
  {
    path:'search',loadChildren:'./components/pages/search/search.module#SearchModule'
  },
  {
    path:'**',component:Error404Component
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  
exports: [RouterModule]
})
export class AppRoutingModule { }
