import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-listing',
  templateUrl: './categories-listing.component.html',
  styleUrls: ['./categories-listing.component.scss']
})
export class CategoriesListingComponent implements OnInit {
  @Input() listings
  constructor(
    private router: Router
  ) { }
  ngOnInit() {
    window.scroll(0,0)
  }

  loadMore(slug){
      this.router.navigate(['category',slug])
  }
}
