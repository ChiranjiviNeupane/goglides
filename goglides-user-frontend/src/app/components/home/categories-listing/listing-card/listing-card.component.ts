import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { ForexService } from '../../../../services/forex.service';
import { HomeService } from '../../../../services/home.service';
import { ListingService } from '../../../../services/listing.service';
import { KeycloakService } from '../../../../keycloak-service/keycloak.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-listing-card',
  templateUrl: './listing-card.component.html',
  styleUrls: ['./listing-card.component.scss']
})
export class ListingCardComponent implements OnInit {
@Input() list
max: number = 10;
public rate: number = 7;
isReadonly: boolean = true;
wishList : boolean = false
heart: boolean = true
  constructor(
    private router: Router,
    public forexService: ForexService,
    private loginService : HomeService,
    private listingService: ListingService,
    private keyClokService: KeycloakService,
    private snotifyService: SnotifyService
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.rate=this.list.review
    this.wishList=this.list.listing[0].wishList
  }

  detailPage(value){
    this.router.navigate(['listing',value])
  }
  businessPage(value){
    this.router.navigate(['business',value])
  }

  wishlist(listing){
    this.heart = false
    if(this.keyClokService.authenticated()){
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.listingService.saveWishList(data).subscribe((data:any)=>{
      this.wishList = true
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Added to WishList",{
        showProgressBar:true
      })
    })
  }else{
    this.keyClokService.login()
  }
  }

  RemoveWishlist(listing){
    this.heart = false
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.listingService.deleteWishList(data).subscribe((data:any)=>{
      this.wishList = false
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Removed from WishList",{
        showProgressBar:true
      })
    })
  }

}
