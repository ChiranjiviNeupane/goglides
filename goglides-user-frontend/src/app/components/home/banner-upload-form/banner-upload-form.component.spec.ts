import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerUploadFormComponent } from './banner-upload-form.component';

describe('BannerUploadFormComponent', () => {
  let component: BannerUploadFormComponent;
  let fixture: ComponentFixture<BannerUploadFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerUploadFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerUploadFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
