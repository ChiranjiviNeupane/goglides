import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HomeService } from '../../../services/home.service';
import { KeycloakService } from '../../../keycloak-service/keycloak.service';

@Component({
  selector: 'app-banner-upload-form',
  templateUrl: './banner-upload-form.component.html',
  styleUrls: ['./banner-upload-form.component.scss']
})
export class BannerUploadFormComponent implements OnInit {
  bannerForm: FormGroup
  quoteForm: FormGroup
  loggedUser: any
  constructor( 
    private fb: FormBuilder,
    private keyCloakService: KeycloakService,
    private loggedUserService: HomeService
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.formBuilder()
  }

  formBuilder(){
    this.loggedUser = this.keyCloakService.loadProfile().then(user=>{
      let usrId = this.loggedUserService.userRef().id
        this.bannerForm = this.fb.group({
      title:['', [Validators.required]],
      description:['', [Validators.required]],
      path:['',[Validators.required]],
      fullName:[user['firstName']+' '+user['lastName'], [Validators.required]],
      userEmail:[user['email'], [Validators.required]],
      user:[usrId,]
    })
    this.quoteForm = this.fb.group({
      description:['', [Validators.required]],
      path:['',[Validators.required]],
      fullName:[user['firstName']+' '+user['lastName'], [Validators.required]],
      userEmail:[user['email'], [Validators.required]],
      user:[usrId],
      source: ['']
    })
    })
  }

  //smooth scrolling
  scrollToElement($element): void {
    console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }
  // ends

}
