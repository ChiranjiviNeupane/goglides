import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotDealsBlockComponent } from './hot-deals-block.component';

describe('HotDealsBlockComponent', () => {
  let component: HotDealsBlockComponent;
  let fixture: ComponentFixture<HotDealsBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotDealsBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotDealsBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
