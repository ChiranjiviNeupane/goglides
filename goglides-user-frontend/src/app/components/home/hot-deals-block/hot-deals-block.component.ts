import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ForexService } from '../../../services/forex.service';
import { KeycloakService } from '../../../keycloak-service/keycloak.service';
import { HomeService } from '../../../services/home.service';
import { ListingService } from '../../../services/listing.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-hot-deals-block',
  templateUrl: './hot-deals-block.component.html',
  styleUrls: ['./hot-deals-block.component.scss']
})
export class HotDealsBlockComponent implements OnInit {
  @Input() deals

  wishList: boolean = false
  heart: boolean = true
  constructor(private router : Router,
    public forexService: ForexService,
    private keyClokService: KeycloakService,
    private loginService: HomeService,
    private listingService: ListingService,
    private snotifyService: SnotifyService
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    // console.log(this.deals)
    this.wishList=this.deals.listing[0].wishList
  }
  detailPage(value){
    this.router.navigate(['listing',value])
 
  }
  businessPage(value){
    this.router.navigate(['business',value])
  }

  wishlist(listing){
    this.heart = false
    if(this.keyClokService.authenticated()){
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.listingService.saveWishList(data).subscribe((data:any)=>{
      this.wishList = true
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Added to WishList",{
        showProgressBar:true
      })
    })
  }else{
    this.keyClokService.login()
  }
  }

  RemoveWishlist(listing){
    this.heart = false
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.listingService.deleteWishList(data).subscribe((data:any)=>{
      this.wishList = false
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Removed from WishList",{
        showProgressBar:true
      })
    })
  }

}
