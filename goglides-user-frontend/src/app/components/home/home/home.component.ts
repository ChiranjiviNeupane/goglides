import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators'
import { HomeService } from '../../../services/home.service';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { FromObservable } from 'rxjs/observable/FromObservable';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],

})
export class HomeComponent implements OnInit {
 
  data=[1,2,2,3]
  categories:[{}]
  categoriesList:[{}]
  public hotDeals:any
  bannerImages = []
  addsImages = []
  quotes =[]
  value:boolean=false
  baseUrl = environment.baseUrl
  seachForm : FormGroup
  constructor(
    private homeService:HomeService,
    private _http: HttpClient,
    private router: Router,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    window.scroll(0,0)
    this.value=false
   this.getHotDeals();
   this.getCategoriesList()
   this.getCategoriesListing()
   this.seachForm=this.fb.group({
     category:['paragliding']
   })
   console.log(this.seachForm.value)


   this._http.get(this.baseUrl+'banner/status')
        // .pipe(map((images: Array<{id: number}>) => this._randomImageUrls(images)))
        // .subscribe(images => this.images = images);
        // .pipe(map((images: Array<{id: number}>) => this._randomImageUrls(images)))
        .subscribe((images: any)=> {
          this.bannerImages = images['banner']
          this.addsImages = images['bannerBottom']
          this.quotes=images['quote']
        });
  }
  search(){
    console.log(this.seachForm.value)
  }
  navigate(){
    this.router.navigate(['category', this.seachForm.value.category]); 
  }
  getHotDeals(){
    let usrId = (this.homeService.userRef().id === null ||this.homeService.userRef().id === undefined)?0:this.homeService.userRef().id
    this.homeService.getHotDeals(usrId).subscribe((data:any)=>{
      this.hotDeals=data['feature']
    })
  }
  
  getCategoriesList(){
    this.homeService.getCategorieslist().subscribe((data:any)=>{
      this.categories=data
    })
  }
  banner(){
    this.router.navigate(['banner'])
  }

  getCategoriesListing(){
    let usrId = (this.homeService.userRef().id === null ||this.homeService.userRef().id === undefined)?0:this.homeService.userRef().id
    this.homeService.getCategoiesListing(usrId).subscribe((data:any)=>{
      this.categoriesList=data
      
    })
  }

  private _randomImageUrls(images: Array<{id: number}>): Array<string> {
    return [1, 2, 3].map(() => {
      const randomId = images[Math.floor(Math.random() * images.length)].id;
      return `https://picsum.photos/900/500?image=${randomId}`;
    });
  }


  loadMore(slug){
    this.router.navigate(["category", slug])
  }

  redirect(link){
    console.log(link)
    if(link){
      window.location.href = link
    }
  }
}
