import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../../../../services/contact.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  public contactusForm

  constructor(private fb: FormBuilder,
    private contactService: ContactService,
    private snotifyService:SnotifyService
  ) { }

  ngOnInit() {
    window.scroll(0, 0)
    this.contactusFormBuilder()
  }

  private contactusFormBuilder(){
    this.contactusForm = this.fb.group({
      fullname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      details: ['', [Validators.required]],
      type:[false]
    })
  }

  get f() { return this.contactusForm.controls; }

  resolved(event){
    console.log(event)
  }


  onSubmit(value){
    value['type'] = 0
    console.log(value)
    this.contactService.saveQuery(value).subscribe((data:any)=>{
      console.log("Saved!!!!")
      this.snotifyService.success("Sucessfully, Sent Your Message")
    })
  }
}
