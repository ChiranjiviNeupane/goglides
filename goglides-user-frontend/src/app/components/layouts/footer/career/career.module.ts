import { CareerComponent } from './career.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes,  RouterModule} from '@angular/router';
const routes:Routes =[
  {path:'', component:CareerComponent},
 
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CareerComponent]
})
export class CareerModule { }
