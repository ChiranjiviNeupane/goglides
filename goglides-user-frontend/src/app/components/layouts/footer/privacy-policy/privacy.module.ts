import { PrivacyPolicyComponent } from './privacy-policy.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes,  RouterModule} from '@angular/router';
const routes:Routes =[
  {path:'', component:PrivacyPolicyComponent},
 
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PrivacyPolicyComponent]
})
export class PrivacyModule { }
