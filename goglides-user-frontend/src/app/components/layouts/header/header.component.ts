import { Component, OnInit } from '@angular/core';
import { KeycloakService } from '../../../keycloak-service/keycloak.service';
import { ForexService } from '../../../services/forex.service';
import { environment } from '../../../../environments/environment';
import { HomeService } from '../../../services/home.service';
import { NotificationService } from '../../../services/notification.service';
declare var $:any

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public agentProfile: any = {};
  roles:any
  currency: any
  forex: any
  navbarItem: boolean = false
  siteUrl=environment.siteUrl
  AdminSiteUrl=this.siteUrl+"manage"
  userId:any
  notificationCount:number=0
  constructor(public kcSvc: KeycloakService,
    public forexService: ForexService,
    private notificationService : NotificationService,
    public homeService: HomeService
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.kcSvc.loadProfile().then(user => {
      this.agentProfile = user;
      console.log(this.agentProfile)
      let data = JSON.stringify(this.agentProfile)
      localStorage.setItem("userProfile",data)
      // let  returnData = JSON.parse(localStorage.getItem("userProfile"))
      // console.log("local:"+returnData.username)
      this.roles= this.kcSvc.getRoles()
      for (var value of this.roles) {
       if(value ==="ROLE_ADMIN"){
          this.roles=value
        // alert(value)
       }
     }
  })
  this.userId = this.homeService.userRef().id
  this.notificationService.getNotificaitonCount(this.userId).subscribe((data:any)=>{
    console.log(data)
    this.notificationCount=data['count']
  })


  this.getCurrencyCode()
  this.currencyValue();

  //sticky header
  window.onscroll = function() {scrollFunction()};
  function scrollFunction() {
      if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
          document.getElementById("navbar").classList.add('sticky');
      } else {
          document.getElementById("navbar").classList.remove('sticky');
      }
  }
  //ends

  // menu toggle
  $('.nav-link').on('click',function() {
    $('.navbar-collapse').collapse('hide');
  });
  // ends

  }

  //smooth scrolling
  scrollToElement($element): void {
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }
  // ends

  getCurrencyCode(){
    this.forexService.getCurrecyCode().subscribe((data:any)=>{
      this.forex = data
    })
  }
  logOut(){
    localStorage.removeItem("userProfile")
    this.kcSvc.logout(this.siteUrl)
  }
  currencyValue(){
    if(localStorage.getItem('currencyCode')){
      this.currency = localStorage.getItem('currencyCode')
      localStorage.setItem('currencyCode', this.currency)
      this.setCurrency(this.currency)
    }else{
      this.currency = "NPR"
      localStorage.setItem('currencyCode', this.currency)
      this.setCurrency(this.currency)
    }
  }

  currencyChange(currency){
    console.log(currency)
    localStorage.setItem('currencyCode', currency)
    this.setCurrency(currency)
  }

  setCurrency(currency){
    this.forexService.setCurrency()
  }

  logout(){
    this.kcSvc.logout();
}

navbarCollapsed(){
  console.log(this.navbarItem)
  this.navbarItem = !this.navbarItem
}
}
