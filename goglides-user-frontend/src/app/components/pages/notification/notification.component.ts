import { NotificationService } from './../../../services/notification.service';
import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../../services/home.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  userId:any
  notifications:any
  constructor(
    private notificationService : NotificationService,
     public homeService: HomeService) { }

  ngOnInit() {
    window.scroll(0,0)
    this.userId = this.homeService.userRef().id
    this.getNotification()
    
  }
  getNotification(){
    this.notificationService.getNotificaiton(this.userId).subscribe((data:any)=>{
      console.log(data)
      this.notifications=data
    })
  }
  readNotification(refId){
    this.notificationService.readNotificaitonCount(refId).subscribe((data:any)=>{
      this.getNotification()
    })
  }

}
