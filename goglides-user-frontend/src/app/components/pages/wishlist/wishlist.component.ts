import { Component, OnInit } from '@angular/core';
import { WishlistService } from '../../../services/wishlist.service';
import { HomeService } from '../../../services/home.service';
import { Router } from '@angular/router';
import { BookingService } from '../../../services/booking.service';
import { ListingService } from '../../../services/listing.service';
import { SnotifyService } from 'ng-snotify';
import { ForexService } from '../../../services/forex.service';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {
  public  myWishListData: any
  public myWishLists: any 
  paginationMax: any = 5
  paginationSteps: any
  steps: any = []
  dataLength :number
  page: any = 1
  offset: number = 0

  constructor(
    private wishListService: WishlistService,
    private loginService: HomeService,
    private router: Router,
    private listingService: ListingService,
    private snotifyService: SnotifyService,
    public forexService: ForexService
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.getWishList(this.offset)
  }

  getWishList(offset){
    let userId = this.loginService.userRef().id
    this.wishListService.getWishList(userId, this.paginationMax, offset).subscribe((data:any)=>{
      this.myWishLists = data['myWishList']
      this.dataLength = data['count']
    })
  }

  listingPage(slug){
    this.router.navigate(['listing',slug])
  }

  onPageChange(page){
    this.offset = (this.page-1)*this.paginationMax
    this.getWishList(this.offset)
  }

  wishListRemove(refId){
    let data ={
      listing : refId,
      user: this.loginService.userRef().id
    }
    this.snotifyService.info("Processing...", {
      timeout: 0,
      showProgressBar: true,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.listingService.deleteWishList(data).subscribe((data:any)=>{
      this.snotifyService.remove()
      this.snotifyService.success("WishList Removed", {
        showProgressBar: true
      })
      this.getWishList(0)
    },error=>{
      this.snotifyService.remove()

      this.snotifyService.error("Something Wrong Try Again",{
        showProgressBar:true
      })
    })

  }

}
