import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WishlistComponent } from './wishlist.component';
import { RouterModule, Routes } from '@angular/router';
import { WishlistService } from '../../../services/wishlist.service';
import { NgbPaginationModule, NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedMaterialModule } from '../shared-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes:Routes =[
  {path:'', component:WishlistComponent},
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbPaginationModule,NgbRatingModule,
    SharedMaterialModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [WishlistComponent,],
  providers: [WishlistService]
})
export class WishlistModule { }
