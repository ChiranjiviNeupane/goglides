import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploaderService } from '../../../../services/file-uploader.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.scss']
})
export class ProfileImageComponent implements OnInit {
    @Input() data;
    image : boolean =false
  constructor(
    public activeModal: NgbActiveModal,
    private fileUploaderService: FileUploaderService,
    private modalService: NgbModal,
    private snotityService: SnotifyService
  ) { }

  ngOnInit() {
    window.scroll(0,0)
}

  imageChangedEvent: any = '';
croppedImage: any = '';

private dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
        type: 'image/jpeg'
    });
}

fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
}
imageCropped(image: string) {
    this.croppedImage = image;
}
imageLoaded() {
    // show cropper
}
loadImageFailed() {
    // show message
}

uploadImage(){
    this.image = true
    var myFile: Blob = this.dataURItoBlob(this.croppedImage);
    if(myFile){
        this.fileUploaderService.uploadProfilePicture(myFile, "PROFILE", this.data, this.snotityService)
    }
}

cancelImage(){
    this.imageChangedEvent = ''
    this.croppedImage = ''
}
}
