import { ViewImageComponent } from './view-image/view-image.component';
import { KeycloakService } from '../../../keycloak-service/keycloak.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HomeService } from '../../../services/home.service';
import { LogService } from '../../../services/log.service';
import { SnotifyService } from 'ng-snotify';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfileImageComponent } from './profile-image/profile-image.component';
import { ForexService } from '../../../services/forex.service';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  userRef: any;
  user: any
  public profileForm: FormGroup
  profileImage: string = "assets/images/user.png";
  name: string = null;
  email: string = null;
  currencyList: any = []
  uploadFile: any;
  checkTot: number = 0;
  checkInt: any;
  agentProfile: any
  loading: boolean = false
  imagePath: any = ''
  resetPassword: FormGroup
  resetPasswordState: boolean = false
  resetMessage: string = ''
  currencyCode = []
  roles: any
  defaultImagePath: string = 'assets/images/employees.svg'

  cdnurl ='https://qa-com-goglides-media.s3.amazonaws.com/'
  constructor(private _fb: FormBuilder,
    private loginService: HomeService,
    public kcSvc: KeycloakService,
    private snotifyService: SnotifyService,
    private modalService: NgbModal,
    private forexService: ForexService

  ) {
    this.userRef = this.loginService.userRef();
    this.user = this.loginService.getLoggedUser();
  }
  getProfile() {
    this.kcSvc.loadProfile().then(user => {
      this.agentProfile = user;
      localStorage.removeItem("userProfile")
      let data = JSON.stringify(this.agentProfile)
      localStorage.setItem("userProfile",data)
      // console.log(this.agentProfile)
      //  = ()?this.agentProfile['attributes']['profile'][0]:
      if(Object.keys(this.agentProfile['attributes']).length ){
        if(!this.agentProfile['attributes']['profile'][0]){
          this.imagePath = this.defaultImagePath
         }
        else if(this.agentProfile['attributes']['profile'][0].indexOf('PROFILE')>0){
          this.imagePath = this.agentProfile['attributes']['profile'][0]
        }else{
          this.imagePath =  this.cdnurl+this.agentProfile['attributes']['profile'][0]
        }
      }else{
        this.imagePath = this.defaultImagePath
      }
       
      this.roles = this.kcSvc.getRoles()
      for (var value of this.roles) {
        if (value == "ROLE_ADMIN") {
          this.roles = value
        }
      }
      // this.imagePath = (Object.keys(this.agentProfile['attributes']).length)? this.agentProfile['attributes']['profile'][0]:this.defaultImagePath
      this.profileFormBuilder(user)
    })
  }

  ngOnInit() {
    window.scroll(0,0)
    this.getProfile()
    this.getCurrecyCode()
  }


  profileFormBuilder(agentProfile){
    console.log(this.agentProfile['attributes'])
    this.profileForm = this._fb.group({
      id: [this.userRef['id'],],
      referenceId: [this.userRef['referenceId'],],
      email: [(agentProfile['email'])?agentProfile['email']:''],
      username: [(agentProfile['username'])?agentProfile['username']:''],
      firstname: [(agentProfile['firstName'])?agentProfile['firstName']:''],
      lastname: [(agentProfile['lastName'])?agentProfile['lastName']:''],
      
      gender: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['gender'][0] : ''],
      phone: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['phone'][0] : '',],
      country: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['country'][0] : ''],
      address: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['address'][0] : '',],
      mobile: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['mobile'][0] : '',],
      profile: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['profile'][0] : '',],
      state: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['state'][0] : '',],
      zip_code: [(Object.keys(agentProfile['attributes']).length ) ? agentProfile['attributes']['zip_code'][0] : '',]
    })

    this.resetPassword = this._fb.group({
      id:[this.userRef['id'],],
      ps: ['']
    })
  }


  onSave(value) {
    this.snotifyService.info("Processing...", {
      timeout: 0,
      showProgressBar: true,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.loginService.updateUserProfile(value).subscribe((data: any) => {

      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Updated Profile", {
        showProgressBar: true
      })
      this.getProfile()
      
    },error=>{
      this.snotifyService.remove()

      this.snotifyService.error("Something Wrong Try Again",{
        showProgressBar:true
      })
    })

  }

  open(content) {

  }

  openModal() {
    const modalRef = this.modalService.open(ProfileImageComponent,  { centered: true, size:'lg', windowClass: 'dark-modal' })
    modalRef.componentInstance.data = this.profileForm.value
  }

  confirmPassword(event) {
    let cps = event.target.value
    let psLength = this.resetPassword.controls.ps.value
    this.resetMessage = ""
      if (cps !== this.resetPassword.controls.ps.value) {
        this.resetMessage = "Make sure your New-password and Confirm-password are same"
        this.resetPasswordState = false
    } if (cps === this.resetPassword.controls.ps.value) {
      this.resetMessage = ""
      this.resetPasswordState = true
    }
  }

  resetPasswordFxn(value){
    this.snotifyService.info("Processing...", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.loginService.resetPassword(value).subscribe((data:any)=>{
      this.snotifyService.remove()
      this.snotifyService.success("Password Changed", {
        showProgressBar: true
      })
    }, error => {
      this.snotifyService.error("Something Wrong Try Again", {
        showProgressBar: true
      })
    })
  }

  getCurrecyCode(){
    this.forexService.getCurrecyCode().subscribe((data:any)=>{
      this.currencyCode = data
    })
  }

  viewPic(){
    const modalRef = this.modalService.open(ViewImageComponent, { centered: true, windowClass: 'dark-modal' })
    modalRef.componentInstance.img = this.agentProfile['attributes']['profile'][0]
  }
}
