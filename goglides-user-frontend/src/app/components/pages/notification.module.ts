import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeService } from '../../services/home.service';
import { NotificationService } from '../../services/notification.service';
import { SharedMaterialModule } from './shared-material.module';
const routes:Routes =[
  {path:'', component:NotificationComponent}
]
@NgModule({
  imports: [
    CommonModule,
    SharedMaterialModule,
    RouterModule.forChild(routes),
  ],
  declarations: [NotificationComponent],
  providers: [HomeService, NotificationService],
  
})
export class NotificationModule { }
