import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailPageComponent } from './detail-page/detail-page.component';
import { DetailPageIndexComponent } from './detail-page-index/detail-page-index.component';
import {Routes,  RouterModule} from '@angular/router';
import { NgbAccordionModule, NgbTabsetModule, NgbRatingModule, NgbTooltipModule, NgbProgressbarModule, NgbPaginationModule,NgbModalModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { ListingService } from '../../services/listing.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { BookingService } from '../../services/booking.service';
import { ReviewService } from '../../services/review.service';
import { SharedMaterialModule } from './shared-material.module';


const routes:Routes =[
  {path:':slug', component:DetailPageIndexComponent}
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgbAccordionModule,
    NgbTabsetModule,
    NgbRatingModule,
    NgbTooltipModule,
    NgbProgressbarModule,
    MyDatePickerModule,
    NgxImageGalleryModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    SharedMaterialModule,
    NgbModalModule,
  ],
  declarations: [DetailPageComponent, DetailPageIndexComponent],
  providers: [ListingService, BookingService,ReviewService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DetailPageLazyModule { }
