import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MybookingComponent } from './mybooking/mybooking.component';
import { Routes, RouterModule } from '@angular/router';
import { BookingService } from '../../services/booking.service';

import {NgPipesModule, TrimPipe} from 'ngx-pipes';
import { ContactService } from '../../services/contact.service';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CancelBookingComponent } from './mybooking/cancel-booking/cancel-booking.component';

const routes:Routes =[
  {path:'', component:MybookingComponent},
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgPipesModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [MybookingComponent],
  providers: [BookingService],
})
export class MybookingModule { }
