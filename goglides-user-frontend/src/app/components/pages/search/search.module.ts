import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

const routes:Routes =[
  {path:'', component:SearchComponent}
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbRatingModule,
  ],
  declarations: [SearchComponent]
})
export class SearchModule { }
