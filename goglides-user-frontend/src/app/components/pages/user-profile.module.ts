import { KeycloakService } from '../../keycloak-service/keycloak.service';
import { HomeService } from '../../services/home.service';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes,  RouterModule} from '@angular/router';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploaderService } from '../../services/file-uploader.service';
import { ProfileImageComponent } from './user-profile/profile-image/profile-image.component';

const routes:Routes =[
  {path:'', component:UserProfileComponent}
]
@NgModule({
  imports: [CommonModule,
  RouterModule.forChild(routes),
  NgbRatingModule,
  FormsModule,ReactiveFormsModule,
  ],
  declarations:[
    UserProfileComponent, 
    ],
  providers: [HomeService, FileUploaderService],
  
})
export class UserProfileModule { }