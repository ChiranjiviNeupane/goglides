import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyReviewsComponent } from './my-reviews.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbRatingModule, NgbPaginationModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ReviewService } from '../../../services/review.service';
import { ReactiveFormsModule } from '@angular/forms';

const routes:Routes =[
  {path:'', component:MyReviewsComponent},
]
@NgModule({
  imports: [
  CommonModule,
  RouterModule.forChild(routes),
  NgbRatingModule,NgbPaginationModule,
  NgbModalModule,
  ReactiveFormsModule,
  ],
  declarations: [MyReviewsComponent],
  providers:[ReviewService]
})
export class MyReviewsModule { }
