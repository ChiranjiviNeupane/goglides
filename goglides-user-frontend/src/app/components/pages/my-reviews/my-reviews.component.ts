import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../../../services/review.service';
import { HomeService } from '../../../services/home.service';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SnotifyService } from 'ng-snotify';
import { FileUploaderService } from '../../../services/file-uploader.service';

@Component({
  selector: 'app-my-reviews',
  templateUrl: './my-reviews.component.html',
  styleUrls: ['./my-reviews.component.scss']
})
export class MyReviewsComponent implements OnInit {

  currentRate: number = 4
  page: number = 0
  userId: number
  paginationMax: number = 5
  offset: number
  myReviews 
  dataLength: number
  mr: NgbModalRef
  editModalForm: FormGroup
  multimedia

  files: FileList 

  constructor(
    private reviewService: ReviewService,
    private loginService: HomeService,
    public router :Router,
    public modalService: NgbModal,
    public fb: FormBuilder,
    private snotifyService: SnotifyService,
    private fileUploaderService: FileUploaderService
  ) { }

  ngOnInit() {
    this.offset = 0
    this.getMyReview(this.offset)
  }

  getMyReview(offset){
    this.userId = this.loginService.userRef().id
    this.reviewService.getMyReviews(this.userId,this.paginationMax, offset).subscribe(data=>{
      this.myReviews = data['myReview']
      this.dataLength = data['count']
    })
  }

  onPageChange(page){
    this.offset = (this.page-1)*this.paginationMax
    this.getMyReview(this.offset)
  }

  redirectTo(slug){
    this.router.navigate(['/listing/',slug])
  }

  editReview(content, myreview){
    this.multimedia = myreview['multimedia']
    this.editModalForm = this.fb.group({
      id:[myreview.id],
      star:[myreview.star, Validators.required],
      description:[myreview.description, Validators.required],
      videoUrl:[myreview.videoUrl,],
      blogUrl:[myreview.blogUrl, ],
     })  
    this.mr = this.modalService.open(content, { size: 'lg' });
  }

  deleteReviewImage(i, id){
    this.multimedia.splice(i,1)
    this.reviewService.deleteReviewImage(id).subscribe((data:any)=>{
      this.snotifyService.success("Review Image Deleted")
    })
  }

  updateFileLength: number
  uploadImageChange(event, multimedia){
    this.files = event.target.files
    this.updateFileLength = multimedia.length + this.files.length
    if(this.updateFileLength > 5){
      this.snotifyService.error("Please select all together 5 photos",{
        showProgressBar:true
      })
    }
  }

  mulmediaJsonBuilder(reviewId){
    return {
      review:{
        id : reviewId,
      },
      user:{
        id: this.loginService.userRef().id
      },
      type: 'REVIEW',
      fileUrl:''
    }
  }

  update(value, multimedia){
    if( (multimedia.length+this.files)>4){
      this.snotifyService.error("Please select only 5 photos",{
        showProgressBar:true
      })
      return
    }
    this.reviewService.udateReview(value).subscribe((data:any)=>{
      if(this.files !== undefined){
        this.fileUploaderService.mulipleUpload(this.files, this.mulmediaJsonBuilder(data['id']), "REVIEW", this.reviewService )
      }else{
        this.snotifyService.success("Review is updated for review",{
          showProgressBar:true
        })
        this.mr.close();
        this.getMyReview(this.offset)
      }
    })
  }

}
