import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-500',
  templateUrl: './500.component.html',
  styleUrls: ['./500.component.scss']
})
export class Error500Component implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scroll(0,0)
  }

}
