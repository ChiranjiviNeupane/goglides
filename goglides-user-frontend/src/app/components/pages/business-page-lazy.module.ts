import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessPageComponent } from './business-page/business-page.component';
import {Routes,  RouterModule} from '@angular/router';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { NgbRatingModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';


const routes:Routes =[
  {path:':slug', component:BusinessPageComponent}
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxImageGalleryModule,
    NgbRatingModule,
    NgbTabsetModule
  ],
  declarations: [BusinessPageComponent]
})
export class BusinessPageLazyModule { }
