import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsAndConditionsComponent } from '../layouts/footer/terms-and-conditions/terms-and-conditions.component';
import { PrivacyPolicyComponent } from '../layouts/footer/privacy-policy/privacy-policy.component';
import { ContactUsComponent } from '../layouts/footer/contact-us/contact-us.component';
import { CareerComponent } from '../layouts/footer/career/career.component';
import { AboutUsComponent } from '../layouts/footer/about-us/about-us.component';
import {Routes,  RouterModule} from '@angular/router';

const routes:Routes =[
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class SharedModule { }
