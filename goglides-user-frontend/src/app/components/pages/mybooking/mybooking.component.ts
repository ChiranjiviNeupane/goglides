import { Component, OnInit, Input } from '@angular/core';
import { HomeService } from '../../../services/home.service';
import { BookingService } from '../../../services/booking.service';
import { TrimPipe } from 'ngx-pipes';
import { ForexService } from '../../../services/forex.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CancelBookingComponent } from './cancel-booking/cancel-booking.component';

@Component({
  selector: 'app-mybooking',
  templateUrl: './mybooking.component.html',
  styleUrls: ['./mybooking.component.scss'],
  providers: [TrimPipe]
})
export class MybookingComponent implements OnInit {

 private userId: number
 public mybookings: any
 referenceId: any
 trimInfo: any
  constructor(
    public homeService: HomeService,
    private bookingService: BookingService,
    public forexService: ForexService,
    private trimPipe: TrimPipe,
    private router: Router,
    private fb: FormBuilder,
    private modalService: NgbModal
  
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.userId = this.homeService.userRef().id
    this.myBooking(this.userId)
  }

  private myBooking(id){
    this.bookingService.getMyBooking(id).subscribe((data:any)=>{
      this.mybookings = data
    })
  }

  viwTicket(refId){
    this.router.navigate(["checkout/ticket",refId])
  }

openModal(refId){
  const modalRef = this.modalService.open(CancelBookingComponent)
  modalRef.componentInstance.referenceId = refId
  modalRef.result.then((response)=>{
    if(response){
      this.myBooking(this.userId)
    }
  })
}

}
