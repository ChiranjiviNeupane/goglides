import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookingService } from '../../../../services/booking.service';
import { HomeService } from '../../../../services/home.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-cancel-booking',
  templateUrl: './cancel-booking.component.html',
  styleUrls: ['./cancel-booking.component.scss']
})
export class CancelBookingComponent implements OnInit {
@Input() referenceId
public cancellationForm: FormGroup
  constructor(
    private bookingService: BookingService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private snotifyService: SnotifyService
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.cancellationFormBuilder()
  }

  cancellationFormBuilder(){ 
    this.cancellationForm = this.fb.group({
    msgCancellation: [null,[Validators.required]],
    bookingStatus: ['CANCELLATION_REQUEST'],
  })
}

cancellationProcess(){ 
  this.snotifyService.info("Processing...", {
    timeout: 0,
    showProgressBar: true,
    closeOnClick: false,
    icon: 'assets/images/Spinner.svg'
  })
  this.bookingService.makeCancellationReq(this.referenceId, this.cancellationForm.value).subscribe((data:any)=>{
    console.log("cancellation request sent")
    this.snotifyService.remove()
    this.snotifyService.success("Sucessfully Sent Cancellation Request", {
      showProgressBar: true
    })
    setTimeout(() => {
      this.activeModal.close(true);  
    }, 2000);
    
  },error=>{
    this.snotifyService.remove()

    this.snotifyService.error("Something Wrong Try Again",{
      showProgressBar:true
    })
  })
 }

 d(){
   this.activeModal.close()
 }

}
