import { ForexService } from './../../../../services/forex.service';
import { HomeService } from './../../../../services/home.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../../../../services/booking.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SnotifyService } from 'ng-snotify';
import { ListingService } from '../../../../services/listing.service';
@Component({
  selector: 'app-addons',
  templateUrl: './addons.component.html',
  styleUrls: ['./addons.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AddonsComponent implements OnInit {
  refId: any
  booking: any
  paxList: any
  addonsList: any = [{}]
  paxRefId: string
  enableAddon: number = 0
  paxCounter: number = 1
  infactPax: number = 0
  childPax: number = 0
  adultPax: number = 0
  seniorPax: number = 0
  infactPaxRate: number = 0
  childPaxRate: number = 0
  adultPaxRate: number = 0
  seniorPaxRate: number = 0
  mr: NgbModalRef
  listing:any
  pricing:any
  total: number = 0
  findCounter: number = 0



  individualAddonsList = []


  postAddons = []

  constructor(private activateRoute: ActivatedRoute,
    private bookingService: BookingService,
    private modalService: NgbModal,
    public homeService: HomeService,
    private snotifyService: SnotifyService,
    public forexService: ForexService,
    public loginService: HomeService,
    private lilstingService: ListingService,


    private router: Router) { }

  ngOnInit() {
    window.scroll(0,0)
    this.refId = this.activateRoute.snapshot.params['refNo']
    console.log(this.refId)
    this.bookingService.viewBooking(this.refId, this.homeService.userRef().id).subscribe((data: any) => {
      console.log(data)
      this.booking = data[0].booking[0]
      this.paxList = data[0].paxList
      this.total = data[0].booking[0].grandTotal
      this.pricing=data[0]
      this.filterPax()
      let usrId = (this.loginService.userRef().id === null ||this.loginService.userRef().id === undefined)?0:this.loginService.userRef().id
      this.lilstingService.getListingDetail(data[0]['listing'][0]['slug'], usrId).subscribe((data:any)=>{
        this.listing=data
        console.log(this.listing)
      })
      this.bookingService.getAddonsList(data[0].listing[0].referenceId).subscribe((data: any) => {
        console.log(data)
        this.addonsList = data
      })
    })

  }
  filterPax() {
    this.paxList.forEach(element => {
      if (element.ageGroup === "CHD") {
        this.childPax++
        this.childPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
      else if (element.ageGroup === "INF") {
        this.infactPax++
        this.infactPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
      else if (element.ageGroup === "ADT") {
        this.adultPax++
        this.adultPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
      else if (element.ageGroup === "SNR") {
        this.seniorPax++
        this.seniorPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
    });
  }
  reloadBookingView() {

    this.bookingService.viewBooking(this.refId, this.homeService.userRef().id).subscribe((res) => {
      console.log(res)
      this.total = res[0].booking[0].grandTotal
    })
  }
  addonEvent(event, refId, content) {

    this.paxRefId = ""
    if (event.checked) {
      this.enableAddon = 1
      this.paxRefId = refId
      this.openLg(content)
      this.postAddons = []

      console.log(refId)
    }
    console.log(event.checked)

  }
  removeAddons(name, refId) {
    this.snotifyService.info("Processing...", {
      timeout: 0,
      showProgressBar: true,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    // alert(refId)
    let data={
      "referenceId":refId
    }
    
    this.bookingService.deleteAddons(data).subscribe((res)=>{
      let index = this.individualAddonsList.findIndex(k => k.name == name)
      let addons = this.individualAddonsList[index]
      let itemIndex = addons.addons.findIndex(key => key.refId = refId)
      // alert(itemIndex)
      this.reloadBookingView()
      addons.addons.splice(0, 1)
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Removed Addons!!", {
        showProgressBar: true
      })
    },error=>{
      this.snotifyService.remove()
      this.snotifyService.error("Something Wrong Try Again", {
        showProgressBar: true
      })
    })
    
    

  }
  plusAddons(event, item) {
    let customObj = {
      "retailRate": "",
      "referenceId": ""
    }
    if (event.checked) {
      customObj.referenceId = item['referenceId']
      customObj.retailRate = item['rate']
      this.postAddons.push(customObj)
    }
    else {
      let index = this.postAddons.findIndex(k => k.referenceId == item.referenceId)
      if (index == 0) {
        this.postAddons.splice(0, 1)
      }
      else {
        this.postAddons.splice(index, 1)
      }

    }
    console.log(this.postAddons)
    // alert(this.paxRefId)
    console.log(event.checked)
  }
  next() {
    this.router.navigate(['checkout/otherinfo/', this.refId])
  }





  openLg(content) {
    this.mr = this.modalService.open(content, { centered: true });
  }
  save() {
    this.snotifyService.info("Processing...", {
      timeout: 0,
      showProgressBar: true,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let total = 0;
    this.postAddons.forEach(element => {
      total += element.retailRate
    })
    let data = {
      "addonTotal": total,
      "addAddon": this.enableAddon,
      "user": this.homeService.userRef().id,
      "addOnList": this.postAddons,
      "referenceId": this.paxRefId
    }
    console.log(data)
    // data.addOnList.forEach(element => {
    //   this.postAddons.forEach(addelement =>{
    //     console.log(addelement)
    //       if(element.referenceId==addelement.referenceId){
    //         this.findCounter++
    //       }
    //   })
    // });
    // console.log(this.findCounter)
    // if(data.addAddon)
    this.bookingService.saveAddons(data).subscribe((response: object[]) => {
      let name
      //  let addons={
      //    "title":"",
      //    "rate":"",
      //    "refId":""
      //  }
      let individualAddons = {
        "name": "",
        "addons": []

      }
      response.forEach(element => {
        let addons = {
          "title": "",
          "rate": "",
          "refId": ""
        }
        name = element['fullName']
        addons.title = element['title']
        addons.rate = element['rate']
        addons.refId = element['referenceId']
        // console.log(element)
        individualAddons.name = name
        individualAddons.addons.push(addons)
        // console.log(individualAddons)
      });
      console.log(response)
      // this.individualAddons.name.refId=this.paxRefId
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Added Addons!!", {
        showProgressBar: true
      })
      this.individualAddonsList.push(individualAddons)
      this.reloadBookingView()

      this.mr.close()



      // this.c('Close click')

      // console.log(this.individualAddonsList)
    }, error => {
      this.snotifyService.remove()

      this.snotifyService.error("Something Wrong Try Again", {
        showProgressBar: true
      })
    })
    // console.log(data)
  }



}
