import { KeycloakService } from './../../../../keycloak-service/keycloak.service';
import { HomeService } from './../../../../services/home.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../../../../services/booking.service';
import { ListingService } from '../../../../services/listing.service';
import * as KhaltiCheckout from 'khalti-web';
import { ForexService } from '../../../../services/forex.service';

@Component({
  selector: 'app-booking-page',
  templateUrl: './booking-page.component.html',
  styleUrls: ['./booking-page.component.scss']
})
export class BookingPageComponent implements OnInit {
 refId:any
 listing:any
 pricing:any
 baseRate:number
 paxCounter:number=1
 infactPax:number=0
 childPax:number=0
 adultPax:number=1
 seniorPax:number=0
 infactPaxRate:number=0
 childPaxRate:number=0
 adultPaxRate:number=0
 seniorPaxRate:number=0
 total:number=0
 roles:any
 agentProfile:any ={}
 clientMobile:any
 clientPhone:any
 bookingDate:any
 localProfile:any
  public bookingForm: FormGroup;
  constructor(private fb: FormBuilder, 
    private activateRoute: ActivatedRoute,
    public bookingService: BookingService,
    private lilstingService: ListingService,
    public homeService : HomeService,
    public router : Router,
    private kcSvc: KeycloakService,
    private loginService: HomeService,
    public forexService:ForexService
  ) { 
  }
  ageGroupChange(){
   this.caclPrice( )
  }
  ngOnInit() {
    window.scroll(0,0)
    if(this.kcSvc.authenticated){
      let  localProfile = JSON.parse(localStorage.getItem("userProfile"))
      //  console.log(localProfile)
       this.localProfile=localProfile
      this.bookingDate = localStorage.getItem("bookingDate")
      localStorage.removeItem("bookingDate")
      if(!this.bookingDate){
        this.router.navigate(['/'])
      }
      this.loadProfile()
      this.refId=this.activateRoute.snapshot.params['refNo']
      this.bookingService.getBookingDetail(this.refId).subscribe((data:any)=>{
        this.pricing=data[0]
        let usrId = (this.loginService.userRef().id === null ||this.loginService.userRef().id === undefined)?0:this.loginService.userRef().id
        this.lilstingService.getListingDetail(data[1]['listing'][0]['slug'], usrId).subscribe((data:any)=>{
          this.listing=data
          this.total=this.pricing.retailRate
        })
      })
      this.initializeForm()
    }
    else{
      this.kcSvc.login()
    }
    
  }


  khalti(){
    let config = {
      // replace this key with yours
      "publicKey": "test_public_key_dc74e0fd57cb46cd93832aee0a390234",
      "productIdentity": "1234567890",
      "productName": "Drogon",
      "productUrl": "http://gameofthrones.com/buy/Dragons",
      "eventHandler": {
          onSuccess (payload) {
              // hit merchant api for initiating verfication
              console.log(payload);
          },
          // onError handler is optional
          onError (error) {
              // handle errors
              console.log(error);
          },
          onClose () {
              console.log('widget is closing');
          }
      }
  };
  
  let checkout = new KhaltiCheckout(config);
  
      checkout.show({amount: 1000});
  
  }
  loadProfile(){
    this.kcSvc.loadProfile().then(user => {
      this.agentProfile = user;
      console.log(this.agentProfile)
      this.roles= this.kcSvc.getRoles()
      for (var value of this.roles) {
       if(value =="ROLE_ADMIN"){
        this.roles=value
       }
     }
     this.clientMobile=this.agentProfile.attributes.mobile[0]
     this.clientPhone=this.agentProfile.attributes.phone[0]
      // this.agentProfile.attributes.mobile[0] ? this.agentProfile.attributes.mobile[0] : this.agentProfile.attributes.phone[0]  

  })
  }
  initializeForm(){
    this.bookingForm = this.fb.group({
      paxInfo:this.fb.array([
        this.initPax()
      ]),
      user:[""],
      referenceId:[""],
      bookingDate:[""],
      clientName:[""],
      clientEmail:[""],
      clientContact:[""],
      total:[""],
      paxTotal:[""],
      bookingMode:[""],
      grandTotal:[""],
      refenceId:[""],
      paxTotalCount:[""],
      schedule:[""],
      duration:[""],
      type:[""],
      category:[""],
      currencyCode:[""]
    })
    
  }
  initPax() {
   return this.fb.group({
     bookingDate:[this.bookingDate],
     schedule:[""],
     duration:[""],
     type:[""],
     category:[""],
     addAddon:[0],
     title:["Mr",Validators.required],
     fullName:["",Validators.required],
     contactNo:["",Validators.required],
     email:["",Validators.required],
     identificationNo:["",Validators.required],
     identificationType:["",Validators.required],
     nationality:["",Validators.required],
     weight:["",Validators.required],
     ageGroup:["ADT",Validators.required],
     rate:[0,Validators.required],
   })
  
  }
  get paxInfoForms() {
    return this.bookingForm.get('paxInfo') as FormArray
  }

  addPax() {
    this.paxInfoForms.push(this.initPax());
    this.paxCounter++
    this.caclPrice()
  }

  delPax(i) {
    const check = confirm("Are you sure to delete?")
    if (check) {
      this.paxInfoForms.removeAt(i)
      this.paxCounter--;
      this.caclPrice()
    }

  }
  setBookigData(){
    let paxArray= this.bookingForm.controls['paxInfo'].value
    paxArray.forEach(element => {
      element.duration=this.pricing.duration
      element.schedule=this.pricing.schedule
      element.type=this.pricing.type
      element.category=this.pricing.nationality
      element.duration=this.pricing.duration

      if(element.ageGroup==="INF"){
        element.rate=this.pricing.infantRetailRate
      }
      else if(element.ageGroup==="CHD"){
        element.rate=this.pricing.childRetailRate
      }
      else if(element.ageGroup==="ADT"){
        element.rate=this.pricing.retailRate
      }
      else if(element.ageGroup==="SNR"){
        element.rate=this.pricing.seniorRetailRate
      }
    });
    // console.log(paxArray)
    // let = localStorage.getItem("userProfile")
   
     
      // console.log("local:"+localProfile.username)
    return{
      "paxInfo": paxArray,
      "duration":this.pricing.duration,
      "schedule":this.pricing.schedule,
      "type":this.pricing.type,
      "category":this.pricing.nationality,
      "user": this.homeService.userRef().id,
      "referenceId": this.pricing.referenceId,
      "bookingDate": this.bookingDate,
      "clientName":this.localProfile.firstName+" "+this.localProfile.lastName,
      "clientEmail":this.localProfile.email,
      "clientContact":(Object.keys(this.localProfile.attributes).length!==0) ? this.localProfile.attributes.phone[0] : "N/A",
      "total": this.total,
      "paxTotal": "",
      "bookingMode": "",
      "grandTotal": this.total,
      "refenceId": "",
      "paxTotalCount": this.paxCounter,
      "currencyCode": this.forexService.currencyCode
    }
  }

  caclPrice(){
    this.infactPax=this.childPax=this.adultPax=this.seniorPax=this.total=0
    this.bookingForm.value.paxInfo.forEach(element => {
      if(element.ageGroup=="INF"){
        this.infactPax++
        this.total+=this.pricing.infantRetailRate
        this.infactPaxRate=this.pricing.infantRetailRate
        console.log(this.pricing)
        console.log(this.pricing.infantRetailRate)
        // alert("infant")
      }
      else if(element.ageGroup=="CHD"){
        this.childPax++
        this.total+=this.pricing.childRetailRate
         this.childPaxRate=this.pricing.childRetailRate
        console.log(this.pricing.childRetailRate)
        // alert("child")
      } 
      else if(element.ageGroup=="ADT"){
        this.adultPax++
        this.total+=this.pricing.retailRate
        this.adultPaxRate=this.pricing.retailRate

        console.log(this.pricing.retailRate)
        // alert("Adult")
      }
      else if(element.ageGroup=="SNR"){
        this.seniorPax++
        this.total+=this.pricing.seniorRetailRate
        this.seniorPaxRate=this.pricing.seniorRetailRate

        console.log(this.pricing.seniorRetailRate)
        // alert("Senior")
      }
      console.log("infat_pax:"+this.infactPax+"child_pax:"+this.childPax+"adult_pax:"+this.adultPax+"senior_pax:"+this.seniorPax)
      console.log(this.total)
      console.log(this.setBookigData())
    });
    // console.log(this.bookingForm.value.paxInfo)
  }


  saveBooking(){
    this.bookingService.saveBooking(this.setBookigData()).subscribe((data:any)=>{
      // alert(data[0].referenceId)
      this.router.navigate(['checkout/addons/',data[0].referenceId])
      console.log(data)
    })
  }
  // add(){
  //   this.bookingForm.push
  // }

}
