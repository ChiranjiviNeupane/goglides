import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingPageComponent } from './booking-page/booking-page.component';
import {Routes,  RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookingService } from '../../../services/booking.service';
import { HomeService } from '../../../services/home.service';
import { ListingService } from '../../../services/listing.service';
import { AddonsComponent } from './addons/addons.component';
import { OtherInfoComponent } from './other-info/other-info.component';
import { NgbButtonsModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TicketComponent } from './ticket/ticket.component';
import { PaymentCancellationComponent } from './payment-cancellation/payment-cancellation.component';
import { SharedMaterialModule } from '../shared-material.module';

const routes:Routes =[
      {path:'ticket/:refNo', component:TicketComponent},
  {path:'',
  children:[
  
     {path:':refNo', component:BookingPageComponent},

    {path:'otherinfo/:refNo', component:OtherInfoComponent},
    {path:'addons/:refNo', component:AddonsComponent,},
    {path:'payment-cancellation', component:PaymentCancellationComponent,},
  ]
}

 
]
@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routes),
    FormsModule,ReactiveFormsModule,NgbButtonsModule,NgbTooltipModule,
    SharedMaterialModule
  ],
  declarations: [BookingPageComponent, AddonsComponent, OtherInfoComponent, TicketComponent, PaymentCancellationComponent],
  providers:[BookingService,HomeService,ListingService]
})
export class BookingModule { }
