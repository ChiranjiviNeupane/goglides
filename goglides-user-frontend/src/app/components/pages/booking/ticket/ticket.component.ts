import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../../../../services/booking.service';
import { ListingService } from '../../../../services/listing.service';
import { HomeService } from '../../../../services/home.service';
import { KeycloakService } from '../../../../keycloak-service/keycloak.service';
import { ForexService } from '../../../../services/forex.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  refId:any
  booking:any
  listing:any
  paxList:any
  package:any
  busnissDir:any
  constructor(private activateRoute: ActivatedRoute,
    private bookingService: BookingService,
    private lilstingService: ListingService,
    public homeService : HomeService,
    public router : Router,
    public forexService : ForexService,
    private kcSvc: KeycloakService) { }

  ngOnInit() {
    window.scroll(0,0)
    this.refId=this.activateRoute.snapshot.params['refNo']
    this.bookingService.viewBooking(this.refId,this.homeService.userRef().id).subscribe((data:any)=>{
      console.log(data)
      this.booking=data[0].booking[0]
      this.listing=data[0].listing[0]
      this.paxList=data[0].paxList
      this.lilstingService.getListingDetail(  data[0]['listing'][0]['slug'], this.homeService.userRef().id).subscribe((data:any)=>{
        console.log(data)
        this.package=data

      })
    })
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('printSection').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
       <html>
           <head>
               <style>
               @media print and (color) {
                * {
                  -webkit-print-color-adjust: exact; 
                  print-color-adjust: exact;
                  color-adjust: exact;
                  -webkit-print-color-adjust: economy;
                  -webkit-print-color-adjust: exact;
                  }
                }
                @page{ margin: 6mm 8mm 6mm 6mm;  size: all; }
                @media print {
                  body {font-family:arial, sans-serif; font-size: 12px; color: #000; width: 100%; height: 100%; overflow: hidden;}
                  table { page-break-inside:auto; page-break-after:avoid; width: 100%; border:0 !important;}
                  tr { page-break-inside:auto; page-break-after:auto; width:100%; }
                  a[href]:after {content: none !important;}
                  .text-right{text-align:right; padding:0 30px; border-top:1px solid #ddd; border-bottom:1px solid #ddd; margin:10px 0; font-size:16px;}
                  h5.title,h6.pax-head{ margin:15px 0; padding-bottom:10px;}
                  th{text-align:left;}
                  td{ vertical-align:middle;}
                  li{ line-height:1.5;}
                  td.ticket-logo-placeholder{ float:left; display:inline-block;}
                  td.ticket-logo-placeholder{ width:20% !important;}
                  td.ticket-heading{width:78% !important; margin-top:20px;}
                  img.logo-bus{margin-top:10px;}
                  .ml-5{ margin-left:25px;}
                  .title{
                    background: #07575b !important; 
                    padding: 5px 10px 0; 
                    color: #fff !important; 
                    margin-bottom: 0;
                    font-size: 13px;
                  }
                  .pax-head{
                    background: #aaa !important; 
                    padding: 6px 10px; 
                    margin: 0;
                    font-size:12px;
                  }
                  table{
                    font-size: 12px;
                    font-weight:normal !imporatnt;
                    text-shadow:none;
                    color:#000;
                  }
                }
               </style>
           </head>
           <body onload="window.print();window.close()">${printContents}
           </body>
       </html>`
    );
    popupWin.document.close();
 }

}
