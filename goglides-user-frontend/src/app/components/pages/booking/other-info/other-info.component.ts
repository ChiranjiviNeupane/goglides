import { error } from 'util';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../../../../services/booking.service';
import { ListingService } from '../../../../services/listing.service';
import { HomeService } from '../../../../services/home.service';
import { KeycloakService } from '../../../../keycloak-service/keycloak.service';
import * as KhaltiCheckout from 'khalti-web';
import { SnotifyService } from 'ng-snotify';
import { ForexService } from '../../../../services/forex.service';
@Component({
  selector: 'app-other-info',
  templateUrl: './other-info.component.html',
  styleUrls: ['./other-info.component.scss']
})
export class OtherInfoComponent implements OnInit {
  public otherInfoForm: FormGroup;
  refId: any
  listing: any
  pricing: any
  pickup = []
  dropOff = []
  booking:any
  paxList: any
  total: number = 0
  paxCounter: number = 1
  infactPax: number = 0
  childPax: number = 0
  adultPax: number = 0
  seniorPax: number = 0
  infactPaxRate: number = 0
  childPaxRate: number = 0
  adultPaxRate: number = 0
  seniorPaxRate: number = 0
  paymode: object[] = [
    {"paymode":"CASH",
      "iconUrl":"assets/images/cash.png"},
    {"paymode":"PAYPAL",
    "iconUrl":"assets/images/pp.png"},
    {"paymode":"KHALTI",
      "iconUrl":"assets/images/khalti.png"},
  ];
  favoriteSeason: string;

  constructor(private fb: FormBuilder,
    private activateRoute: ActivatedRoute,
    private bookingService: BookingService,
    private lilstingService: ListingService,
    public homeService: HomeService,
    public router: Router,
    public forexService:ForexService,
    private snotifyService: SnotifyService,

    private kcSvc: KeycloakService) { }

  ngOnInit() {
    window.scroll(0,0)
    this.refId = this.activateRoute.snapshot.params['refNo']
    this.bookingService.viewBooking(this.refId, this.homeService.userRef().id).subscribe((data: any) => {
      this.pricing = data[0]
      this.booking =data[0].booking[0]
      this.paxList = data[0].paxList
      this.total = data[0].booking[0].grandTotal

      this.filterPax()

      this.total = data[0].booking[0].grandTotal
      console.log(this.pricing)

      this.lilstingService.getListingDetail(this.pricing['listing'][0]['slug'], this.homeService.userRef().id).subscribe((data: any) => {
        console.log(data)
        this.listing = data
        // this.listing.listing[0].referenceId
        // "34dc9800-b21a-4a4f-9a71-e0716f57104c"
        this.bookingService.getPickDropLocation(this.listing.listing[0].referenceId).subscribe((data: any) => {
          console.log(data)
          this.pickup = data['pickup']
          this.dropOff = data['dropoff']
          console.log(this.pickup)
        })

      })

    })
    this.otherInfoForm = this.fb.group({
      referenceId: [this.refId, Validators.required],
      paymentMode: ["", Validators.required],
      emergencyContactName: ["", Validators.required],
      emergencyContactNumber: ["", Validators.required],
      pickup: [""],
      dropOff: [""],
      comment: [""],
      condition: ["", Validators.requiredTrue],

    })
  }
  filterPax() {
    this.paxList.forEach(element => {
      if (element.ageGroup === "CHD") {
        this.childPax++
        this.childPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
      else if (element.ageGroup === "INF") {
        this.infactPax++
        this.infactPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
      else if (element.ageGroup === "ADT") {
        this.adultPax++
        this.adultPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
      else if (element.ageGroup === "SNR") {
        this.seniorPax++
        this.seniorPaxRate = (element.retailRate === null || element.retailRate === undefined) ? 0 : element.retailRate
      }
    });
  }
  saveBooking(value) {
    this.snotifyService.info(" Wait !!  We are Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.bookingService.saveOtherInfo(value).subscribe((data: any) => {
      console.log(data)
      if (value.paymentMode === "KHALTI") {
        this.snotifyService.remove()
        this.kalti()
      } else if (value.paymentMode === "PAYPAL") {
        this.snotifyService.remove()
        console.log(data[1]['url'][0])
        let url = data[1]['url'][0]
        location.href = url
      }
      else {
        this.snotifyService.remove()
        this.snotifyService.success("Sucessfully Booked", {
          showProgressBar: true
        })
        this.router.navigate(['checkout/ticket/', data[0].booking])

      }
    },error =>{
      this.snotifyService.remove()
      this.snotifyService.error("Sorry! Something gone Wrong. Please Try Again", {
        showProgressBar: true
      })
    }
  )
  }
  kalti() {

    let bookingService = this.bookingService
    let refId = this.refId
    let snotify= this.snotifyService
    let router=this.router
    let config = {
      // replace this key with yours
      "publicKey": "live_public_key_84913ed9347343339a4a440565aebe18",
      // "productIdentity": "abc",
      // "productName": "test",
      // "productUrl": "http://goglides.com/listing/",
      "productIdentity": this.listing.listing[0].code,
      "productName": this.listing.listing[0].title,
      "productUrl": "http://goglides.com/listing/" + this.listing.listing[0].slug,
      "eventHandler": {
        onSuccess(payload) {
          snotify.info(" Wait !!  We are Processing.....", {
            timeout: 0,
            showProgressBar: false,
            closeOnClick: false,
            icon: 'assets/images/Spinner.svg'
          })
          // hit merchant api for initiating verfication
          console.log(payload);
          let data = {
            "amount": payload.amount,
            "token": payload.token,
            "referenceId": refId
          }
          bookingService.khaltiPayement(data).subscribe((data: any) => {
            
            snotify.remove()
            snotify.success("Sucessfully Booked", {
              showProgressBar: true
            })
            router.navigate(['checkout/ticket/', data[0].referenceId])

          },error=>{
            snotify.remove()
            snotify.error("Sorry! Something gone Wrong. Please Try Again", {
              showProgressBar: true
            })
          })
        },
        // onError handler is optional
        onError(error) {
          // handle errors
          snotify.remove()
          snotify.error("Sorry! Something gone Wrong. Please Try Again", {
            showProgressBar: true
          })
          console.log(error);
        },
        onClose() {
          console.log('widget is closing');
        }
      }
    };
    console.log(this.pricing.booking[0].grandTotal)

    let checkout = new KhaltiCheckout(config);
    checkout.show({ amount: this.pricing.booking[0].grandTotal*100*100 });
    // checkout.show({ amount: 1000 });
  }

}
