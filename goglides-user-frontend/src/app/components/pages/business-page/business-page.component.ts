import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { BusinessPageService } from '../../../services/business-page.service';
import { CommonService } from '../../../services/common.service';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from "ngx-image-gallery";
import { HomeService } from '../../../services/home.service';
import { ListingService } from '../../../services/listing.service';
import { KeycloakService } from '../../../keycloak-service/keycloak.service';
import { SnotifyService } from 'ng-snotify';
import { ForexService } from '../../../services/forex.service';
@Component({
  selector: 'app-business-page',
  templateUrl: './business-page.component.html',
  styleUrls: ['./business-page.component.scss']
})
export class BusinessPageComponent implements OnInit {

  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;

  public business: any
  public listing: any
  public galleries: any
  public rating : any
  public videos = []
  currentRate = 0
  wishList: boolean = false

  images: GALLERY_IMAGE[] = [];

  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };
  heart: boolean = true

  constructor(
    private activateRoute: ActivatedRoute,
    private businessPageService: BusinessPageService,
    private router: Router,
    private commonService: CommonService,
    private loginService: HomeService,
    private listingService: ListingService,
    private keyClokService: KeycloakService,
    private snotifyService: SnotifyService,
    private forexService:ForexService

  ) { 
  }

  ngOnInit() {
    window.scroll(0,0)
    this.getBusiness()
  }


  public getBusiness(){
    let slug = this.activateRoute.snapshot.params['slug']
    let usrId = (this.loginService.userRef().id === null ||this.loginService.userRef().id === undefined)?0:this.loginService.userRef().id
    this.businessPageService.getBusinessBySlug(slug, usrId).subscribe((data:any)=>{
      this.business = data['business']
      this.listing = data['listing']
      this.galleries = data['gallery']
      this.rating = data['rating']
      this.videos = data['video']
    })
  }

  route(slug){
    this.router.navigate(['listing', slug])
  }

  transFormUrl(url){
    return this.commonService.transFormUrl(url)
  }


  openGallery(index: number = 0) {
    for(let i in this.galleries){
      let data = {
        url:this.galleries[i]['fileUrl'],
        thumbnailUrl:this.galleries[i]['fileUrl'],
      }
      this.images.push(data)
    }
    this.ngxImageGallery.open(index);
  }
    
  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }
    
  // set new active(visible) image in gallerykeyClokService
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }
    
  // next image in gallery
  // nextImage(index: number = 0) {
  //   this.ngxImageGallery.next(index);
  // }
    
  // // prev image in gallery
  // prevImage(index: number = 0) {
  //   this.ngxImageGallery.prev(index);
  // }
    
  /**************************************************/
    
  // EVENTS
  // callback on gallery opened
  // galleryOpened(index) {
  //   console.info('Gallery opened at index ', index);
  // }
 
  // // callback on gallery closed
  galleryClosed() {
    this.images = []
    console.log(this.images)
  }
 
  // // callback on gallery image clicked
  // galleryImageClicked(index) {
  //   console.info('Gallery image clicked with index ', index);
  // }
  
  // // callback on gallery image changed
  // galleryImageChanged(index) {
  //   console.info('Gallery image changed to index ', index);
  // }
 
  // // callback on user clicked delete button
  // deleteImage(index) {
  //   console.info('Delete image at index ', index);
  // }



  wishlist(listing, i){
    this.heart = false
    if(this.keyClokService.authenticated()){
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.listingService.saveWishList(data).subscribe((data:any)=>{
      this.listing[i]['wishList'] = true
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Added to WishList",{
        showProgressBar:true
      })
    })
  }else{
    this.keyClokService.login()
  }
  }

  RemoveWishlist(listing, i){
    this.heart = false
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.listingService.deleteWishList(data).subscribe((data:any)=>{
      this.listing[i]['wishList'] = false
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Removed from WishList",{
        showProgressBar:true
      })
    })
  }
}
