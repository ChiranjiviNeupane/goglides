import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../../services/listing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { KeycloakService } from '../../../../keycloak-service/keycloak.service';
import { HomeService } from '../../../../services/home.service';
import { ForexService } from '../../../../services/forex.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  public currentRate = 4
  public category:any
  public listing:any
  heart: boolean = true
  constructor(
    private listingService: ListingService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private keyClokService: KeycloakService,
    private loginService: HomeService,
    private forexService: ForexService,
    private snotifyService: SnotifyService
  ) { }

  ngOnInit() {
    window.scrollTo(0,0)
    this.getAllList()
  }

  getAllList(){
    let slug = (this.activeRoute.snapshot.params['slug'])
    let usrId = (this.loginService.userRef().id === null ||this.loginService.userRef().id === undefined)?0:this.loginService.userRef().id
    this.listingService.getAllListingBySlug(slug, usrId).subscribe((data:any)=>{
      this.category = data['category'],
      this.listing = data['listing']
    })

  }

  detailPage(slug){
    this.router.navigate(["listing",slug])
  }

  businessPage(businessSlug){
    this.router.navigate(["business",businessSlug])
  }

  wishlist(listing, i){
    this.heart = false
    if(this.keyClokService.authenticated()){
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.listingService.saveWishList(data).subscribe((data:any)=>{
      this.listing[i]['wishList'] = true
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Added to WishList",{
        showProgressBar:true
      })
    })
  }else{
    this.keyClokService.login()
  }
  }

  removeWishList(listing, i){
    this.heart = false
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.listingService.deleteWishList(data).subscribe((data:any)=>{
      this.listing[i]['wishList'] = false
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Removed from WishList",{
        showProgressBar:true
      })
    })
  }

}
