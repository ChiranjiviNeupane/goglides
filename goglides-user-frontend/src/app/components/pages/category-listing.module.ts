import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryListComponent } from './category-wise-list/category-list/category-list.component';
import {Routes,  RouterModule} from '@angular/router';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { ListingService } from '../../services/listing.service';

const routes:Routes =[
  {path:':slug', component:CategoryListComponent},
]
@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routes),
    NgbRatingModule,
  ],
  declarations: [CategoryListComponent],
  providers: [ListingService],
})
export class CategoryListingModule { }
