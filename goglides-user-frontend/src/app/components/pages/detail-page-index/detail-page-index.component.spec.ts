import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPageIndexComponent } from './detail-page-index.component';

describe('DetailPageIndexComponent', () => {
  let component: DetailPageIndexComponent;
  let fixture: ComponentFixture<DetailPageIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPageIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPageIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
