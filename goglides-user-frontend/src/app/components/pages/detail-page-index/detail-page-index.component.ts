import { HomeService } from '../../../services/home.service';
import { ListingService } from '../../../services/listing.service';
import { Component, OnInit, ViewChild, ViewEncapsulation,  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ForexService } from '../../../services/forex.service';
import {IMyDpOptions} from 'mydatepicker';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from 'ngx-image-gallery';
import { CommonService } from '../../../services/common.service';
import { KeycloakService } from '../../../keycloak-service/keycloak.service';
import { SnotifyService } from 'ng-snotify';
import { BookingService } from '../../../services/booking.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReviewService } from '../../../services/review.service';
import { NgbModal, NgbModalRef, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploaderService } from '../../../services/file-uploader.service';
@Component({
  selector: 'app-detail-page-index',
  templateUrl: './detail-page-index.component.html',
  styleUrls: ['./detail-page-index.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DetailPageIndexComponent implements OnInit {

  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;

  images: GALLERY_IMAGE[] = [];
  mr: NgbModalRef
  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };
  paginationMax: any = 3
  page = 1;
  offset: number = 0
  currentRate
  slug:string
  test="sadas"
  public listingDetail:any
  bookingInfo={
    category:"",
    type:null,
    schedule:"",
    duration:"",
    bookingDate:this.formatDate(new Date()),
    listing:"",
  }
  myreview:any
  pricing:any
  dangerousVideoUrl: string;
  videoUrl: any;
  isReviewAvailale:boolean=false
  categoryReferenceId : any
  reviews=[]
  closeResult: string;
  count:any

  similarListings:any
  heart: boolean = true
  reviewForm: FormGroup

  userProfile: any

  constructor( 
    public router :Router, public routerActivate: ActivatedRoute,
    private listingService:ListingService, 
    private homeService:HomeService, 
    public sanitizer: DomSanitizer,
    public forexService: ForexService,
    private commonService: CommonService,
    private loginService: HomeService,
    private keyClokService: KeycloakService,
    private snotifyService: SnotifyService,
    private bookingService: BookingService,
    private reviewService: ReviewService,
    public modalService: NgbModal,
    private fb : FormBuilder,
    private fileUploaderService: FileUploaderService
  ) { }

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    todayBtnTxt: 'Today',
    dateFormat: 'yyyy-mm-dd',
    disableUntil: {year:new Date().getFullYear(), month:new Date().getMonth()+1, day: new Date().getDate()-1},
    showClearDateBtn: false
};

  public model: any = { date: { year: new Date().getFullYear(), month: new Date().getMonth()+1, day: new Date().getDate() } };


  ngOnInit() {
    window.scroll(0,0)
    this.currentRate = 2
    this.slug = this.routerActivate.snapshot.params['slug']
    let usrId = (this.loginService.userRef().id === null ||this.loginService.userRef().id === undefined)?0:this.loginService.userRef().id
    this.getListingDetail(this.slug, usrId)
    this.updateVideoUrl('7GERhDNHpMA') 
  }

  loadProfile(){
    if(this.keyClokService.authenticated())
    this.reviewFormBuilder(this.listingDetail['listing'][0]['referenceId'])
  }

  //smooth scrolling
  scrollToElement($element): void {
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }
  // ends

  updateVideoUrl(id: string) {
    // Appending an ID to a YouTube URL is safe.
    // Always make sure to construct SafeValue objects as
    // close as possible to the input data so
    // that it's easier to check if the value is safe.
    this.dangerousVideoUrl = 'https://www.youtube.com/embed/' + id;
    this.videoUrl =
        this.sanitizer.bypassSecurityTrustResourceUrl(this.dangerousVideoUrl);
  }

  getListingDetail(slug, usrId){
    this.listingService.getListingDetail(slug, usrId).subscribe((data:any)=>{
      this.listingDetail=data
      this.categoryReferenceId = data['category'][0]['referenceId']
      this.getSimilarListing(this.categoryReferenceId,usrId )
    this.bookingInfo.listing=this.listingDetail.listing[0].referenceId
    this.loadProfile()
    this.checkReviewStatus(this.listingDetail.listing[0].referenceId,usrId)
    this.getReviews(this.listingDetail.listing[0].referenceId,0)
    })
  }

  search(){
    // this.snotifyService.error("Something Wrong Try Again", {
    //   showProgressBar: true
    // })
    if(this.bookingInfo.type===null || this.bookingInfo.type===""){
      this.snotifyService.error("Please Select Type", {
        showProgressBar: true
      })
    }
    this.listingService.getPackagePrice(this.bookingInfo).subscribe((data:any)=>{
      this.pricing=data
    })
  }

  bookNow(referenceId){
    localStorage.setItem("bookingDate", this.bookingInfo.bookingDate);
    this.router.navigate(['/checkout/',referenceId])
  }


  businessPage(slug){
    this.router.navigate(["business",slug])
  }

  listingPage(slug){
    // this.router.navigate(['listing', slug])
    this.router.navigateByUrl('/listing/' + slug);
    let usrId = (this.loginService.userRef().id === null ||this.loginService.userRef().id === undefined)?0:this.loginService.userRef().id
    this.getListingDetail(slug, usrId )
    window.scroll(0,0)
  }

  onDateChanged(event){
    this.bookingInfo.bookingDate = event.formatted
  }

  formatDate(date){
    let value = new Date(date)
    let year =  value.getFullYear()
    let month = ((value.getMonth()+1).toString.length ===1)?0+(value.getMonth()+1).toString():value.getMonth()+1
    let day = value.getDate()
    // this.myDatePickerOptions['disableUntil']['year'] = 
    // this.myDatePickerOptions['disableUntil']['month'] = 
    // this.myDatePickerOptions['disableUntil']['day'] = -
    let  today = year+'-'+ month +'-'+ day
    return today
  }

  getSimilarListing(referenceId,usrId){
    this.listingService.getSimilarListing(referenceId,usrId).subscribe((data:any)=>{
      this.similarListings = data
    })
  }


  openGallery(index: number = 0) {
    let galleries = this.listingDetail['listing'][0]['gallery']
    for(let i in galleries){
      let data = {
        url: galleries[i],
        thumbnailUrl: galleries[i]
      }
      this.images.push(data)
    }
    this.ngxImageGallery.open(index);
  }
    
  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }
    
  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }

  galleryClosed() {
    this.images = []
  }

  transFormUrl(url){
    return this.commonService.transFormUrl(url)
  }

  wishlist(listing, i){
    this.heart = false
    if(this.keyClokService.authenticated()){
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    this.listingService.saveWishList(data).subscribe((data:any)=>{
      this.similarListings[i]['listing'][0]['wishList'] = true
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Added to WishList",{
        showProgressBar:true
      })
    })
  }else{
    this.keyClokService.login()
  }
  }

  RemoveWishlist(listing, i){
    this.heart = false
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.listingService.deleteWishList(data).subscribe((data:any)=>{
      this.similarListings[i]['listing'][0]['wishList'] = false
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Removed from WishList",{
        showProgressBar:true
      })
    })
  }

  addToFavorite(listing){
    this.heart = false
    if(this.keyClokService.authenticated()){
      let data = {
        listing: listing.referenceId,
        user: this.loginService.userRef().id
      }
      this.snotifyService.info("Processing.....", {
        timeout: 0,
        showProgressBar: false,
        closeOnClick: false,
        icon: 'assets/images/Spinner.svg'
      })
      this.listingService.saveWishList(data).subscribe((data:any)=>{
        this.listingDetail.listing[0]['wishList'] = true
        this.heart = true
        this.snotifyService.remove()
        this.snotifyService.success("Sucessfully Added to WishList",{
          showProgressBar:true
        })
      })
    }else{
      this.keyClokService.login()
    }
  }

  removeFromFavorite(listing){
    this.heart = false
    this.snotifyService.info("Processing.....", {
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let data = {
      listing: listing.referenceId,
      user: this.loginService.userRef().id
    }
    this.listingService.deleteWishList(data).subscribe((data:any)=>{
      this.listingDetail.listing[0]['wishList'] = false
      this.heart = true
      this.snotifyService.remove()
      this.snotifyService.success("Sucessfully Removed from WishList",{
        showProgressBar:true
      })
    })
  }


  reviewFormBuilder(refId){
    this.keyClokService.loadProfile().then(data=>{
      this.reviewForm = this.fb.group({
        user : [this.loginService.userRef().id],
        listing :[refId],
        star:[this.currentRate, Validators.required],
        description:['', Validators.required],
        videoUrl:[null,],
        blogUrl:[null, ],
        status:['PENDING',Validators.required],
        userEmail:[(data.email)?data.email:''],
        termsAndCondition:[false,Validators.required]
      })  
    })
  }
  getReviews(refId,offset){
    this.count=null;
    this.reviewService.getReviews(refId,offset).subscribe((data:any)=>[
      this.count=data.count,
      this.reviews=data.reviews
    ])
  }

  checkReviewStatus(refId,userId){
    this.reviewService.checkStatus(refId,userId).subscribe((data:any)=>{
      if(Object.keys(data).length == 0){
        this.isReviewAvailale=false
      }
      else{
        this.isReviewAvailale=true
        this.myreview=data
      }
      // alert(this.isReviewAvailale)
    })
  }

  getStarWiseReview(refid,star,offset){
    this.reviewService.getratingReviews(refid,star,offset).subscribe((data:any)=>{
      this.reviews=data
    })
  }
  onPageChange(){
    let paginationMax=3
    let no = this.page-1
    this.offset = no * paginationMax
    this.getReviews(this.listingDetail.listing[0].referenceId,this.offset)
  }

  saveReview(value){
    this.snotifyService.info("Processing...",{
      showProgressBar:true
    })
    this.reviewService.postReview(value).subscribe((data:any)=>{
      if(this.files !== undefined){

        console.log(this.files)
        this.fileUploaderService.mulipleUpload(this.files, this.mulmediaJsonBuilder(data['msg']['id']), "REVIEW", this.reviewService )
      }else{
        console.log(this.files)
        this.snotifyService.remove();
        this.snotifyService.success("Review is submitted for review",{
          showProgressBar:true
        })
      }
      let usrId = this.loginService.userRef().id
      this.checkReviewStatus(this.listingDetail['listing'][0]['referenceId'],usrId )
      this.getReviews(this.listingDetail['listing'][0]['referenceId'],usrId )
    })
  }
  
  editModalForm: FormGroup
  openLg(content, myreview) {
    this.editModalForm = this.fb.group({
      id:[myreview.id],
      star:[myreview.star, Validators.required],
      description:[myreview.description, Validators.required],
      videoUrl:[myreview.videoUrl,],
      blogUrl:[myreview.blogUrl, ],
     })  
    this.mr = this.modalService.open(content, { size: 'lg' });
  }

  update(value){
    if( (this.myreview.multimedia.length+this.files)>4){
      this.snotifyService.error("Please select only 5 photos",{
        showProgressBar:true
      })
      return
    }
    this.reviewService.udateReview(value).subscribe((data:any)=>{
      if(this.files !== undefined){
        this.fileUploaderService.mulipleUpload(this.files, this.mulmediaJsonBuilder(data['id']), "REVIEW", this.reviewService )
      }else{
        this.snotifyService.success("Review is updated for review",{
          showProgressBar:true
        })
        this.mr.close();
        let usrId = this.loginService.userRef().id
        this.checkReviewStatus(this.listingDetail['listing'][0]['referenceId'],usrId )
        this.getReviews(this.listingDetail['listing'][0]['referenceId'],usrId )
      }
    })
  }

  files: FileList 


  mulmediaJsonBuilder(reviewId){
    return {
      review:{
        id : reviewId,
      },
      user:{
        id: this.loginService.userRef().id
      },
      type: 'REVIEW',
      fileUrl:''
    }
  }

  
  onImageSelect(event){
    this.files = event.target.files
    if(this.files.length >5){
      this.snotifyService.error("Please select only 5 photos",{
        showProgressBar:true
      })
    }
  }

  deleteReviewImage(i, id){
    this.myreview['multimedia'].splice(i,1)
    this.reviewService.deleteReviewImage(id).subscribe((data:any)=>{
      this.snotifyService.success("Review Image Deleted")
    })
  }

  updateFileLength
  uploadImageChange(event){
    this.files = event.target.files
    this.updateFileLength = this.myreview.multimedia.length + this.files.length
    if(this.updateFileLength > 5){
      this.snotifyService.error("Please select all together 5 photos",{
        showProgressBar:true
      })
    }
  }

  openTermsAndCondition(content){
    this.mr = this.modalService.open(content, { size: 'lg' });
  }
}
