import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-contents',
  templateUrl: './contents.component.html',
  styleUrls: ['./contents.component.scss']
})
export class ContentsComponent implements OnInit {

  constructor(private snotifyService: SnotifyService) { }

  ngOnInit() {
    window.scroll(0,0)
  
  }
  onSucess(){
    this.snotifyService.success('Example body content', 'Example title', {
      timeout: 2000,
      showProgressBar: false,
      closeOnClick: false,
      pauseOnHover: true
    });
    // this.snotifyService.success('Example body content', {timeout:2000});
  }

}
