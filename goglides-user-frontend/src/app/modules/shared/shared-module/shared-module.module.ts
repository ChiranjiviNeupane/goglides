import { HomeComponent } from '../../../components/home/home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HotDealsBlockComponent } from '../../../components/home/hot-deals-block/hot-deals-block.component';
import { CategoriesListingComponent } from '../../../components/home/categories-listing/categories-listing.component';
import { ListingCardComponent } from '../../../components/home/categories-listing/listing-card/listing-card.component';
import { SharedMaterialModule } from '../../../components/pages/shared-material.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    SharedMaterialModule
  ],
  declarations: 
  [HomeComponent, 
    HotDealsBlockComponent,
    CategoriesListingComponent,
    ListingCardComponent,],
  exports:[FormsModule,HomeComponent,]
})
export class SharedModuleModule { }
