import { NotificationService } from './services/notification.service';
;
import { KeycloakService } from './keycloak-service/keycloak.service';
import { SharedModuleModule } from './modules/shared/shared-module/shared-module.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layouts/header/header.component';
import { FooterComponent } from './components/layouts/footer/footer.component';
import { ContentsComponent } from './components/contents/contents.component';
import { HomeService } from './services/home.service';
import { LibraryModule } from './modules/shared/library/library.module';
import { KeycloakGuard } from './keycloak-service/keycloak.guard';
import { BusinessPageService } from './services/business-page.service';
import { CommonService } from './services/common.service';
import { ForexService } from './services/forex.service';
import { BannerUploadFormComponent } from './components/home/banner-upload-form/banner-upload-form.component';
import { ListingService } from './services/listing.service';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { ProfileImageComponent } from './components/pages/user-profile/profile-image/profile-image.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { FileUploaderService } from './services/file-uploader.service';
import { NgPipesModule } from 'ngx-pipes';
import { DatePipe } from '@angular/common';

import { CancelBookingComponent } from './components/pages/mybooking/cancel-booking/cancel-booking.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewImageComponent } from './components/pages/user-profile/view-image/view-image.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SharedMaterialModule } from './components/pages/shared-material.module';
import { Error500Component } from './components/pages/500/500.component';
import { Error404Component } from './components/pages/404/404.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContentsComponent,
    BannerUploadFormComponent,
    ProfileImageComponent,
    CancelBookingComponent,
    ViewImageComponent,
    Error500Component, Error404Component,
  ],
  imports: [
    LibraryModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModuleModule,
    SnotifyModule,
    ImageCropperModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SharedMaterialModule
  ],
  providers: [HomeService,NotificationService, KeycloakService,KeycloakGuard, BusinessPageService, CommonService, ForexService,ListingService,
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService,
    FileUploaderService,DatePipe

    // {
    //   provide: RECAPTCHA_SETTINGS,
    //   useValue: { siteKey: '6LeaxBoUAAAAAJyWICHwU4kfZKMu7MsxyAlRZ1mC' } as RecaptchaSettings
    // }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  
  entryComponents:[ProfileImageComponent, CancelBookingComponent, ViewImageComponent]
})
export class AppModule { }
