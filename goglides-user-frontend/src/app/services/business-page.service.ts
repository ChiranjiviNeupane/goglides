import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class BusinessPageService {

  private url = environment.baseUrl
  constructor(
    private http: HttpClient
  ) { }
// localhost:8082/v1/business?slug=dgmates-llc

  getBusinessBySlug(slug, usrId){
    return this.http.get(this.url+"business/"+slug+"/"+usrId)
  }
}
