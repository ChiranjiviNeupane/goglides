import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ReviewService {
  baseUrl = environment.baseUrl
  constructor(private http : HttpClient) { }

  checkStatus(refId,userId){
    return this.http.get(this.baseUrl+"reviews/status/"+refId+"/"+userId)
  }
  getReviews(refId,offset){
    return this.http.get(this.baseUrl+"reviews/count/"+refId+"/"+offset)
  }
  getratingReviews(refId,star,offset){
    return this.http.get(this.baseUrl+"reviews/"+refId+"/"+star+"/"+offset)
  }
  postReview(data){
    return this.http.post(this.baseUrl+"review",data)
  }

  udateReview(data){
  return this.http.put(this.baseUrl+"review/"+data['id'], data)
  }

  multimediaSave(data){
    return this.http.post(this.baseUrl+"multimedia", data);
  }

  deleteReviewImage(id){
    return this.http.delete(this.baseUrl+"multimedia/"+id)
  }

  getMyReviews(userId, max, offset){
    return this.http.get(this.baseUrl+"review/"+userId+"/"+max+"/"+offset)
  }
 

}