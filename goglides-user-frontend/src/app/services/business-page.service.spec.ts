import { TestBed, inject } from '@angular/core/testing';

import { BusinessPageService } from './business-page.service';

describe('BusinessPageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusinessPageService]
    });
  });

  it('should be created', inject([BusinessPageService], (service: BusinessPageService) => {
    expect(service).toBeTruthy();
  }));
});
