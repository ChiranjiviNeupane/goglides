import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class CommonService {

  
  constructor(
    private sanitizer: DomSanitizer
  ) { }


  transFormUrl(url){
    return this.sanitizer.bypassSecurityTrustResourceUrl(url)
  }

}
