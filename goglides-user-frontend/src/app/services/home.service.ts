
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {  HttpClient } from '@angular/common/http';
import { KeycloakService } from '../keycloak-service/keycloak.service';
import { LogService } from './log.service';
import { Observable } from 'rxjs';


@Injectable()
export class HomeService {
  baseUrl= environment.baseUrl
  private userReference: {id: number, referenceId: string, username: string, fullname: string, authenticated: boolean, roles: string[]};
  private loggedUser: {username: string, firstname: string, lastname: string, email: string, mobile: number, phone: number, gender: string, profile: string, address: string, state: string, zipCode: number, country: string, id: number };
  constructor(private http:HttpClient, private kc: KeycloakService) { this.userReference = LogService.userRef;
    this.loggedUser = LogService.user;}
  getHotDeals(usrId){
    return this.http.get(this.baseUrl+'listing/feature/'+usrId)

  }
  getCategoiesListing(usrId){
    return this.http.get(this.baseUrl+'listing/category/'+usrId)
  }
  getCategorieslist(){
    return this.http.get(this.baseUrl + 'site/category')
  }
  login(params?: any){
    return this.kc.login(params);
  }

  logout(params?: any) {
    return this.kc.logout(params);
  }

  /** loggedUser Info fxn **/
  userRef() {
    return this.userReference;
  }

  setLoggedUser(user){
    this.loggedUser=user;
  }

  getLoggedUser(){
    return this.loggedUser;
  }
  /** loggedUser Info fxn **/
 
  formatDuration(duration:any){
    let formated = " "
    let dur:any = duration.split(':') 
    if(dur[0]!= '00'){
      formated += dur[0] +" weeks "
    }
    if(dur[1]!= '00'){
      formated += dur[1]+" days "
     }
     if(dur[2]!= '00'){
    formated += dur[2] +" hours "
     }
     if(dur[3]!= '00' ){
       formated += dur[3] + " minutes "
    }
    return(formated) 
  }

  formatSchedule(schdeule:any){
    let formated = " "
    let schl:any = schdeule.split(':')
    if(schl[0]>=0 && schl[0]<12){
      formated = schl[0] + ":" +schl[1] + " AM" 
    }
    if(schl[0]==24 ){
      schl[0]='00'
      formated = schl[0] + ":" +schl[1] + " AM" 
    }
    else if(schl[0]>12){
      schl[0]=schl[0]-12
      formated = schl[0] + ":" +schl[1] + " PM" 
    }
    return(formated)
    // alert(formated)
  }

  updateUserProfile(data){
    return this.http.post(this.baseUrl+"user/updateKcUser", data)
  }

  resetPassword(data){
    return this.http.post(this.baseUrl+"user/reset",data)
  }
}
