import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ForexService {

  forexSubject = new Subject<any>()
  private baseUrl = environment.baseUrl
  public currencyCode: any
  public rate: number
  constructor(
    private http: HttpClient
  ) { }

  getCurrecyCode(){
    return this.http.get(this.baseUrl+"forex")
  }

  setCurrency(){
    if(localStorage.getItem("currencyCode")){
      this.currencyCode = localStorage.getItem("currencyCode")
      this.http.get(this.baseUrl+"forex/rate/"+localStorage.getItem("currencyCode")).subscribe((data:any)=>{
        this.rate = parseFloat(data['rate'])
      })
    }
  }

  calculateRate(amount: number){
    amount = amount * this.rate
    return amount
    // console.log(amount)
  }
}
