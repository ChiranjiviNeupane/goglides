import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import * as S3 from 'aws-sdk/clients/s3';
import { DatePipe } from "@angular/common";
import { HomeService } from './home.service';
import { SnotifyService } from 'ng-snotify';
import { ReviewService } from './review.service';

@Injectable()
export class FileUploaderService {
  constructor(
    private datePipe: DatePipe,
    private homeService: HomeService,
    // private reviewService: ReviewService,
    private snotifyService: SnotifyService
  ) { }

  uploadProfilePicture(file, imageType, object, snotifyService) {
    snotifyService.info(" Processing...", {
      timeout: 0,
      showProgressBar: true,
      icon: 'assets/images/Spinner.svg'
    })
    let bucket = new S3(environment.aws);
    let date = new Date();
    let name = this.datePipe.transform(date, "yyyyMMddHHmmss")
    const params = {
      Bucket: environment.container,
      Key: "original/profile/" + imageType + "/" + name + ".jpeg",
      Body: file,
      ContentType: 'image/jpeg',
      ACL: 'public-read',
    };

    let homeService = this.homeService
    // let photo = object['profile']
    // let status = this.deleteImage(photo)
    bucket.upload(params, function (err, data) {
      if (err) {
        snotifyService.error("Something Wrong Try Again", {
          showProgressBar: true
        })
        setTimeout(() => {
          window.location.reload();
        }, 3000);
        return false
      }
      if (data['Location']) {
        object['profile'] = data['Location']
        
        homeService.updateUserProfile(object).subscribe((data: any) => {
          snotifyService.remove();
          snotifyService.success("Profile Image Updated", {
            showProgressBar: true
          })
          setTimeout(() => {
            window.location.reload();
          }, 3000);
        }, error => {
          snotifyService.error("Something Wrong Try Again", {
            showProgressBar: true
          })
          setTimeout(() => {
            window.location.reload();
          }, 3000);
        })
      }
    });
  }


  mulipleUpload(files, object, imageType, reviewService) {
    this.snotifyService.info("Processing...",{
      timeout: 0,
      showProgressBar: false,
      closeOnClick: false,
      icon: 'assets/images/Spinner.svg'
    })
    let length = files.length
    let counter = 0;
    const bucket = new S3(environment.aws);
    let file: any;
    let date = new Date();
    if (files !== null) {
      for (let i = 0; i < files.length; i++) {
        file = files[i]
        let name = this.datePipe.transform(date, "yyyyMMddHHmmss")
        const params = {
          Bucket: environment.container,
          Key: "original/listing/" + imageType + "/" + name + i + ".jpeg",
          Body: file,
          ContentType: 'image/jpeg',
          ACL: 'public-read',
        };
        let multimediaService = reviewService
        let snotifyService = this.snotifyService
        bucket.upload(params, function (err, data) {
          if (err) {
            console.log(err)
            return false
          }
          object['fileUrl'] = data['Location']
          multimediaService.multimediaSave(object).subscribe((data:any) => {
            counter++;        
            if (counter == length) {
              snotifyService.remove()
              snotifyService.success("Review are submitted for review",{
                showProgressBar:true
              })
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            }
          });
        })
      }
    }
  }
}
