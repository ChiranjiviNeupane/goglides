import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class WishlistService {

  private baseUrl = environment.baseUrl
  constructor(
    private http: HttpClient
  ) { }

  saveWishList(data){
    this.http.post(this.baseUrl+"wishlist/save", data)
  }
  
  getWishList(id, max, offset){
    return this.http.get(this.baseUrl+"myWishList/"+id+"/"+max+"/"+offset);
  }
}
