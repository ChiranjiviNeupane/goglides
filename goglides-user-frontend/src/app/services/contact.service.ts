import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ContactService {

  baseUrl =  environment.baseUrl
  constructor(
    private http: HttpClient
  ) { }


  saveQuery(value){
    return this.http.post(this.baseUrl+"contact", value)
  }
}
