import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';

@Injectable()
export class ListingService {
  baseUrl = environment.baseUrl
  constructor(private http : HttpClient) { }

  getListingDetail(slug, usrId){
    return this.http.get(this.baseUrl + 'listing/'+slug+"/"+usrId)
  }
  getPackagePrice(object){
    return this.http.get(this.baseUrl + 'listing/search?listing='+object.listing + '&type='+object.type+'&schedule='+object.schedule+'&duration='+object.duration)
  }

  getAllListingBySlug(slug, usrId){
    return this.http.get(this.baseUrl+"more/"+slug+"/"+usrId)
  }

  getSimilarListing(slug, usrId){
    return this.http.get(this.baseUrl+"listing/similar/"+slug+"/"+usrId)
  }

  saveWishList(data){
    return this.http.post(this.baseUrl+"wishList",data)
  }

  deleteWishList(data){
    return this.http.get(this.baseUrl+"wishlist/"+data.listing+"/"+data.user)
  }

}
