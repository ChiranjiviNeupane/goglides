import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';

@Injectable()
export class BookingService {
  public date = new Subject<any>();
  baseUrl=environment.baseUrl
  constructor(private http:HttpClient) { }

  getBookingDetail(refId){
    return this.http.get(this.baseUrl+"listing/listingPrice/"+refId)
  }
  saveBooking(data){
    return this.http.post(this.baseUrl+"booking",data)
  }
  viewBooking(refId,userId){
    return this.http.get(this.baseUrl+"booking/view/"+refId+"/"+userId)
  }
  getAddonsList(redId){
    return this.http.get(this.baseUrl +"listing/addonList/"+redId)
  }
  saveAddons(addons){
    return this.http.post(this.baseUrl +"booking/saveAddon",addons)
  }
  getMyBooking(id){
    return this.http.get(this.baseUrl+"booking/"+id)
  }
  saveOtherInfo(value){
    return this.http.post(this.baseUrl +"booking/saveBooking",value)
  }

  makeCancellationReq(refId, data){
    return this.http.post(this.baseUrl+"booking/cancellation/"+refId, data)
  }
  getPickDropLocation(refId){
    return this.http.get(this.baseUrl+"booking/routeList/"+refId)
  }
  khaltiPayement(data){
    return this.http.post(this.baseUrl+"booking/executeKhaltiPayment",data)
  }
  deleteAddons(refId){
    return this.http.post(this.baseUrl+"booking/removeAddon",refId)
  }

}
