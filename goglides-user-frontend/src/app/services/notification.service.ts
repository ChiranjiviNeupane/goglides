import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NotificationService {
  baseUrl=environment.baseUrl
  constructor(private http:HttpClient) { }
  getNotificaiton(useId){
    return this.http.get(this.baseUrl+"notification/list?user="+useId)
  }
  getNotificaitonCount(useId){
    return this.http.get(this.baseUrl+"notification/unReadNotification/?user="+useId)
  }
  readNotificaitonCount(refId){
    return this.http.get(this.baseUrl+"notification/readNotification/?reference="+refId)
  }
}
