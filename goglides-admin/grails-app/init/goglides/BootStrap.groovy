package goglides

import com.dgmates.goglides.BusinessDirectory
import com.dgmates.goglides.Category
import com.dgmates.goglides.ContactBook
import com.dgmates.goglides.Listing
import com.dgmates.goglides.ListingPayment
import com.dgmates.goglides.SiteSettings

class BootStrap {

    def init = { servletContext ->
        /**
         * Check whether categories exists.
         * If not then insert categories.
         */
        if (!Category.findByCategoryTitle('Paragliding')) {
            new Category(categoryTitle: 'Paragliding', slug: 'paragliding', status: 'publish').save(flush: true)
        }
        if (!Category.findByCategoryTitle('Handgliding')) {
            new Category(categoryTitle: 'Handgliding', slug: 'handgliding', status: 'publish').save(flush: true)
        }
        if (!Category.findByCategoryTitle('Ultra Light Flight')) {
            new Category(categoryTitle: 'Ultra Light Flight', slug: 'ultra-light-flight', status: 'publish').save(flush: true)
        }
        if (!Category.findByCategoryTitle('Powered Paragliding')) {
            new Category(categoryTitle: 'Powered Paragliding', slug: 'powered-paraglidig', status: 'publish').save(flush: true)
        }
        if (!Category.findByCategoryTitle('Bungy')) {
            new Category(categoryTitle: 'Bungy', slug: 'bungy', status: 'publish').save(flush: true)
        }
        if (!Category.findByCategoryTitle('Rafting/Kayak')) {
            new Category(categoryTitle: 'Rafting/Kayak', slug: 'rafting-kayak', status: 'publish').save(flush: true)
        }

        /**
         * Check whether site settings exists.
         * If not then insert setting parameters and default values.
         */
        if (!SiteSettings.findBySlug('site-title')) {
            new SiteSettings(title: 'Site Title', slug: 'site-title', defaultValue: 'GoGlides').save(flush: true)
        }
        if (!SiteSettings.findBySlug('tagline')) {
            new SiteSettings(title: 'Tagline', slug: 'tagline', defaultValue: 'Thrill with gliding experience').save(flush: true)
        }
        if (!SiteSettings.findBySlug('email')) {
            new SiteSettings(title: 'Email', slug: 'email', defaultValue: 'info@goglides.com').save(flush: true)
        }
        if (!SiteSettings.findBySlug('phone1')) {
            new SiteSettings(title: 'Phone1', slug: 'phone1', defaultValue: '+977-14219235').save(flush: true)
        }
        if (!SiteSettings.findBySlug('phone2')) {
            new SiteSettings(title: 'Phone2', slug: 'phone2', defaultValue: '14263722').save(flush: true)
        }
        if (!SiteSettings.findBySlug('Address1')) {
            new SiteSettings(title: 'Address1', slug: 'address1', defaultValue: 'JP MARG, THAMEL, KATHMANDU, NEPAL.').save(flush: true)
        }
        if (!SiteSettings.findBySlug('Address2')) {
            new SiteSettings(title: 'Address2', slug: 'address2').save(flush: true)
        }
        if (!SiteSettings.findBySlug('facebook')) {
            new SiteSettings(title: 'Facebook', slug: 'facebook', defaultValue: 'https://www.facebook.com/goglides').save(flush: true)
        }
        if (!SiteSettings.findBySlug('twitter')) {
            new SiteSettings(title: 'Twitter', slug: 'twitter', defaultValue: 'https://twitter.com/googlides').save(flush: true)
        }
        if (!SiteSettings.findBySlug('googleplus')) {
            new SiteSettings(title: 'GooglePlus', slug: 'googleplus', defaultValue: 'https://plus.google.com/b/104573181260446052319/104573181260446052319/about?gmbpt=true&hl=en').save(flush: true)
        }
        if (!SiteSettings.findBySlug('youtube')) {
            new SiteSettings(title: 'YouTube', slug: 'youtube', defaultValue: 'https://www.youtube.com/channel/UCgl6AILUwi4QQzgeaCnw6qw?guided_help_flow=3').save(flush: true)
        }
        if (!SiteSettings.findBySlug('copyright')) {
            new SiteSettings(title: 'Copyright', slug: 'copyright', defaultValue: 'GoGlides. All rights reserved').save(flush: true)
        }
        if (!SiteSettings.findBySlug('google-map')) {
            new SiteSettings(title: 'Google Map', slug: 'google-map').save(flush: true)
        }
        if (!SiteSettings.findBySlug('business-hours')) {
            new SiteSettings(title: 'Business Hours', slug: 'business-hours', defaultValue: '<p> <i class="glyphicon glyphicon-time"></i> <strong>Mon.-Fri.</strong> 9am-6pm </p> <p> <i class="glyphicon glyphicon-time"></i> <strong>Sat</strong> Closed </p> <p> <i class="glyphicon glyphicon-time"></i> <strong>Sun</strong> 9am-11am </p>').save(flush: true)
        }
        if (!SiteSettings.findBySlug('google-map-api-key')) {
            new SiteSettings(title: 'Google map API Key', slug: 'google-map-api-key', defaultValue: 'AIzaSyBdQX0EJBVmrCnTWG4LKIO52ncxChz_AN0').save(flush: true)
        }
        if (!SiteSettings.findBySlug('meta-keyword')) {
            new SiteSettings(title: 'Meta Keyword', slug: 'meta-keyword', defaultValue: 'paragliding').save(flush: true)
        }
        if (!SiteSettings.findBySlug('meta-description')) {
            new SiteSettings(title: 'Meta Description', slug: 'meta-description', defaultValue: 'paragliding').save(flush: true)
        }
        if (!SiteSettings.findBySlug('site-mode')) {
            new SiteSettings(title: 'Site Mode', slug: 'site-mode', defaultValue: 'test').save(flush: true)
        }
        if (!SiteSettings.findBySlug('paypal-mode')) {
            new SiteSettings(title: 'Paypal Mode', slug: 'paypal-mode', defaultValue: 'sandbox').save(flush: true)
        }
        if (!SiteSettings.findBySlug('keycloak-server-url')) {
            new SiteSettings(title: 'KeyCloak Server URL', slug: 'keycloak-server-url', defaultValue: '').save(flush: true)
        }
        if (!SiteSettings.findBySlug('keycloak-realm-id')) {
            new SiteSettings(title: 'KeyCloak Realm ID', slug: 'keycloak-realm-id', defaultValue: '').save(flush: true)
        }
        if (!SiteSettings.findBySlug('keycloak-client-id')) {
            new SiteSettings(title: 'KeyCloak Client ID', slug: 'keycloak-client-id', defaultValue: '').save(flush: true)
        }
        if (!SiteSettings.findBySlug('keycloak-client-secret')) {
            new SiteSettings(title: 'KeyCloak Client Secret', slug: 'keycloak-client-secret', defaultValue: '').save(flush: true)
        }
        if (!SiteSettings.findBySlug('keycloak-username')) {
            new SiteSettings(title: 'KeyCloak Username', slug: 'keycloak-username', defaultValue: '').save(flush: true)
        }
        if (!SiteSettings.findBySlug('keycloak-password')) {
            new SiteSettings(title: 'KeyCloak Password', slug: 'keycloak-password', defaultValue: '').save(flush: true)
        }

        ContactBook.search().createIndexAndWait()
        Listing.search().createIndexAndWait()
        BusinessDirectory.search().createIndexAndWait()
        ListingPayment.search().createIndexAndWait()
    }
    def destroy = {
    }
}
