package com.dgmates.goglides

class ListingAddonsPrice {

    Listing listing
    ListingPackage listingPackage

    String addonType
    String ageGroup
    BigDecimal wholesaleRate
    BigDecimal retailRate


    static constraints = {

    }

    static mapping = {

    }

    def beforeValidate() {

    }
}