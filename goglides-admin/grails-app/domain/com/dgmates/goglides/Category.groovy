package com.dgmates.goglides

class Category {

    String categoryTitle
    String slug
    String status
    Date createdAt
    Date modifiedAt
    Date trashedAt

    static constraints = {
        categoryTitle blank: false, nullable: false
        slug blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        modifiedAt nullable: true
        trashedAt nullable: true
    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        modifiedAt = new Date()
    }
}
