package com.dgmates.goglides

class Message {

    String fullname
    String email
    String query
    String type

    static constraints = {
        fullname blank: false, nullable: false
        email blank: false, nullable: false
        query blank: false, nullable: false
        type blank: false, nullable: false, inlist: ['contact', 'query']
    }

    static mapping = {

    }
}