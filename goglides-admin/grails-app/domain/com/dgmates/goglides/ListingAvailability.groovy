package com.dgmates.goglides

class ListingAvailability {

    Listing listing
    ListingPackage listingPackage

    String availability
    String availabilityType
    Date fromDate
    Date toDate
    String unAvailabilityType
    Date unAvailabilityFromDate
    Date unAvailabilityToDate

    static constraints = {
        availability blank: true, nullable: true
        availabilityType blank: false, nullable: false
        fromDate nullable: true
        toDate nullable: true
        unAvailabilityType true: true, nullable: true
        unAvailabilityFromDate nullable: true
        unAvailabilityToDate nullable: true
    }

    static mapping = {

    }

    def beforeValidate() {
        if (!availability) {
            availability = 'available'
        }
    }

}
