package com.dgmates.goglides
import org.apache.commons.lang.RandomStringUtils
import java.sql.Timestamp
class Event{
    def friendlyUrlService
    User user
    String title
    String location
    Date startDate
    Date endDate
    String startTime
    String endTime
    String description
    String eventType
    String status
    Date createdAt
    Date modifiedAt
    String slug
    String email
    String phone1
    String phone2
    String phone3
    String refNo
    String totalAvailableStock

    static constraints = {
        title blank: false, nullable: false, maxSize: 255
        location blank: false, nullable: false
        startDate blank: false, nullable: false
        startTime blank: false, nullable: false
        endDate blank: true, nullable: true
        endTime blank: true, nullable: true
        description blank: true, nullable: true, maxSize: 10000
        createdAt blank: true, nullable: true
        modifiedAt blank: true, nullable: true
        status blank: true, nullable: true
        eventType blank: true, nullable: true
        slug blank: true, nullable: true
        email blank: true, nullable: true
        phone1 blank: true, nullable: true
        phone2 blank: true, nullable: true
        phone3 blank: true, nullable: true
        refNo blank: true, nullable: true
        totalAvailableStock blank: true, nullable: true
    }

    static mapping = {

    }

    def beforeInsert(){
        createdAt = new Date()
        modifiedAt = new Date()
        refNo = generateUrl(10)
    }

    def beforeUpdate(){
        modifiedAt = new Date()
    }

    def beforeValidate() {

        if (!slug) {
            try {
                String uniqueTitle = generateUrl(3) + " " + friendlyUrlService.sanitizeWithDashes(title)
                slug = friendlyUrlService.sanitizeWithDashes(uniqueTitle)
            } catch (Exception e) {
                slug = generateUrl(9)
            }
        }
    }

    def static generateUrl(int length){
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        return RandomStringUtils.random(length, charset.toCharArray())
    }
}