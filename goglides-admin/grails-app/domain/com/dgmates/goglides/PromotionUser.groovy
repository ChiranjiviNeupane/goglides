package com.dgmates.goglides


class PromotionUser {
    User user
    Promotion promotion

    String isValidUser
    String winnerRank
    Date createdAt
    Date updatedAt
    Date trashedAt
    String userImage
    Boolean prizeClaim
    Boolean contactStatus

    static mapping = {}
    static constraints = {
        isValidUser nullable: true, blank: true
        winnerRank nullable: true, blank: true
        createdAt nullable: true
        updatedAt nullable: true
        trashedAt nullable: true
        prizeClaim blank: true, defaultValue: 0
        contactStatus blank: true, defaultValue: 0
        userImage nullable: true
    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
    }

}
