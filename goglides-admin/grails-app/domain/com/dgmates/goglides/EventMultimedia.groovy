package com.dgmates.goglides
class EventMultimedia {

	Event event
	String file

	static constraints = {
		file blank: false, nullable: false
	}

	static mapping = {
	}
}