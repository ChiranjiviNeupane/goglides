package com.dgmates.goglides

class Content {
    String title
    String slug
    String description
    String status
    Date createdAt
    Date modifiedAt
    Date trashedAt

    static constraints = {
        createdAt nullable: true
        modifiedAt nullable: true
        trashedAt nullable: true
    }

    static mapping = {
        description sqlType: 'longText'
    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        modifiedAt = new Date()
        if (status == 'trash') {
            trashedAt = new Date()
        }
    }
}