package com.dgmates.goglides

class WishList {
    def springSecurityService

    User user
    Listing listing
    Date createdAt

    static constraints = {
        createdAt nullable: true
    }

    static mapping = {

    }

    def beforeInsert() {
        createdAt = new Date()
    }

    def beforeUpdate() {}

    def beforeValidate() {
        if (!user) {
            user = ''
        }
    }
}