package com.dgmates.goglides

class ListingAgeGroup {

    Listing listing

    String infantMin = "below"
    String infantMax
    String childMin
    String childMax
    String adultMin
    String adultMax
    String seniorMin
    String seniorMax = "above"

    static constraints = {
        infantMin blank: true, nullable: true
        infantMax blank: true, nullable: true, defaultValue: '2'
        childMin blank: true, nullable: true, defaultValue: '3'
        childMax blank: true, nullable: true, defaultValue: '12'
        adultMin blank: true, nullable: true, defaultValue: '13'
        adultMax blank: true, nullable: true, defaultValue: '65'
        seniorMin blank: true, nullable: true, defaultValue: '66'
        seniorMax blank: true, nullable: true
    }

    static mapping = {

    }
}
