package com.dgmates.goglides

//import com.dgmates.helpnetwork.User
import org.apache.commons.lang.RandomStringUtils

class Promotion {

    def friendlyUrlService
    def springSecurityService

    User user

    String title
    String slug
    String banner
    String description
    Date startDate
    Date endDate
    Boolean hasPopUp
    String type
    Integer sort
    String status
    Date createdAt = new Date()
    Date modifiedAt = new Date()
    Date trashedAt
    String hasResultDrawn
    Date resultAnnouncementDate
    String bannerImage

    static constraints = {
        title blank: true, nullable: true
        slug blank: true, nullable: true
        banner blank: true, nullable: true
        description blank: true, nullable: true
        startDate nullable: true
        endDate nullable: true
        hasPopUp blank: true, defaultValue: 0
        type blank: true, nullable: true
        sort blank: true, nullable: true
        status blank: true, nullable: true
        createdAt nullable: true
        modifiedAt nullable: true
        trashedAt nullable: true
        hasResultDrawn blank: true, nullable: true
        resultAnnouncementDate nullable: true
        bannerImage nullable: true

    }

    def beforeValidate() {
        if (!slug) {
            try {
                String uniqueTitle = friendlyUrlService.sanitizeWithDashes(title)
                slug = friendlyUrlService.sanitizeWithDashes(uniqueTitle)
            }
            catch (Exception e) {
                slug = generateUrl(9)
            }
        }
    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        modifiedAt = new Date()
        if (status == 'trash') {
            trashedAt = new Date()
        }
    }

    def static generateUrl(int length) {
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        return RandomStringUtils.random(length, charset.toCharArray())
    }
}
