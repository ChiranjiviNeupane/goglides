package com.dgmates.goglides

class ListingDuration {

    Listing listing
    ListingPackage listingPackage

    String duration
    String durationUnit

    static constraints = {
        durationUnit nullable: true, blank: true
    }
}
