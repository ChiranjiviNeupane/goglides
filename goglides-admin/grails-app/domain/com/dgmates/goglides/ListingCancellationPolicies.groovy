package com.dgmates.goglides

class ListingCancellationPolicies {

    Listing listing

    String policyTitle
    String duration
    String durationUnit
    String refund
    String refundUnit
    String description

    static constraints = {
        policyTitle blank: true, nullable: true
        duration blank: true, nullable: true
        durationUnit blank: true, nullable: true
        refund blank: true, nullable: true
        refundUnit blank: true, nullable: true
        description blank: true, nullable: true
    }

    static mapping = {

    }

    def beforeValidate() {

    }
}
