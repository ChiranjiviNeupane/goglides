package com.dgmates.goglides

class Ticket {
    User user
    BusinessDirectory businessDirectory
    ListingPayment listingPayment

    String ticketCategory
    String status
    String createdBy
    String createdTo
    String ticketDescription
    String remark
    Date createdAt = new Date()
    Date modifiedAt = new Date()

    static constraints = {
        remark blank: true, nullable: true
        user nullable: true
        businessDirectory nullable: true
    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        modifiedAt = new Date()
    }
}
