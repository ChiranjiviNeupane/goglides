package com.dgmates.goglides

class ListingReview {
    def springSecurityService

    User user
    Listing listing
    BusinessDirectory businessDirectory

    String rating
    String comment
    String status
    String blogUrl
    Date createdAt
    Date modifiedAt
    Date trashedAt

    static hasMany = [multimedia: Multimedia]
    static belongsTo = [listing: Listing, businessDirectory: BusinessDirectory]

    static constraints = {
        rating blank: true, nullable: true
        comment blank: true, nullable: true, maxSize: 1000
        status blank: true, nullable: true
        createdAt blank: true, nullable: true
        modifiedAt blank: true, nullable: true
        businessDirectory blank: true, nullable: true
        trashedAt blank: true, nullable: true
        blogUrl blank: true, nullable: true
    }

    static mapping = {

    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
        status = 'draft'
    }

    def beforeUpdate() {
        modifiedAt = new Date()
    }

    def beforeValidate() {
        if (!user) {
            user = ''
        }
    }
}