package com.dgmates.goglides

class BookingHistory {

    User user
    ListingPayment listingPayment

    String action
    Date createdAt

    static constraints = {
        createdAt nullable: true
    }

    def beforeInsert() {
        createdAt = new Date()
    }
}