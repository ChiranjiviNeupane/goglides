package com.dgmates.goglides

class ListingGroupDiscount {

    Listing listing
    ListingPackage listingPackage
    String groupPolicy
    String discountFrom
    String discountTo
    String discountPercent


    static constraints = {

    }

    static mapping = {

    }

    def beforeValidate() {

    }
}