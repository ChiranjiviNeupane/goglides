package com.dgmates.goglides

class BusinessDirectoryTransferLog {

    BusinessDirectory businessDirectoryOld
    BusinessDirectory businessDirectoryNew

    User transferredBy
    User reversedBy
    Boolean isReversible
    Date transferredAt
    Date reversedAt

    static constraints = {
        isReversible blank: true, nullable: true
        transferredBy nullable: true
        transferredAt nullable: true
        reversedBy nullable: true
        reversedAt nullable: true
    }

    static mapping = {
    }

    def beforeInsert() {
        if (!transferredBy)
            transferredBy = ''
        if (!isReversible)
            isReversible = Boolean.TRUE
        transferredAt = new Date()
    }

    def beforeUpdate() {
        if (!reversedBy)
            reversedBy = ''
        isReversible = Boolean.FALSE
        reversedAt = new Date()
    }

}