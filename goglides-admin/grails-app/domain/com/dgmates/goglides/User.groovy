package com.dgmates.goglides

class User{

    String referenceId

    static constraints = {
        referenceId unique: true
    }

    static mapping = {

    }

    def beforeValidate() {

    }

    def beforeInsert() {
    }

    def beforeUpdate() {

    }

}
