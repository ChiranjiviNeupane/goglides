package com.dgmates.goglides
class QueryTicket {

    Listing listing
    Event event
    String name
    String email
    String phoneNumber
    String message
    String status
    Date createdAt=new Date()
    Date updatedAt=new Date()

    static constraints = {
        listing blank:true, nullable: true
        event blank:true, nullable: true
        email blank: true, nullable: true
        phoneNumber blank: true, nullable: true
        message blank: true, nullable: true
        status blank: true, nullable: true
        createdAt blank:true, nullable: true
        updatedAt blank:true, nullable: true
    }


    def beforeValidate() {
        createdAt = new Date()
        updatedAt = new Date()
    }

    def beforeUpdate() {
        updatedAt = new Date()
    }
}
