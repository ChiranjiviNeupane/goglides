package com.dgmates.goglides

class ListingPrice {

    Listing listing
    ListingPackage listingPackage

    String pricingType
    String type
    String ageGroup
    String duration
    String schedule
    String nationality
    String status
    BigDecimal wholeSaleRate
    BigDecimal retailRate
    BigDecimal transportationRate
    BigDecimal multimediaRate
    Date availabilityDate

    static belongsTo = [listing: Listing, listingPackage:ListingPackage]

    static constraints = {
        pricingType blank: true, nullable: true
        type blank: true, nullable: true
        ageGroup blank: true, nullable: true
        duration blank: true, nullable: true
        schedule blank: true, nullable: true
        nationality blank: true, nullable: true
        transportationRate nullable: true
        multimediaRate nullable: true
        status blank: true, nullable: true
        availabilityDate nullable: true
        wholeSaleRate nullable: true
        retailRate nullable: true
    }

    static mapping = {

    }

    def beforeValidate() {
        if (!pricingType) {
            pricingType = 'regular'
        }
    }

    def beforeInsert() {
        status = 'publish'
    }
}
