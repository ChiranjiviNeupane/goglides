package com.dgmates.goglides

class ListingAddons {

    Listing listing
    ListingPackage listingPackage

    String includedMultimedia
    String includedTransport
    String hasGroupDiscountPolicy
    String isDiscountApplicable
    String groupDiscountMultimedia
    String groupDiscountTransport

    static constraints = {
        includedMultimedia blank: true, nullable: true
        includedTransport blank: true, nullable: true
        hasGroupDiscountPolicy blank: true, nullable: true
        isDiscountApplicable blank: true, nullable: true
        groupDiscountMultimedia blank: true, nullable: true
        groupDiscountTransport blank: true, nullable: true
    }

    static mapping = {

    }

    def beforeValidate() {
        if (!groupDiscountMultimedia) {
            groupDiscountMultimedia = 'no'
        }
        if (!groupDiscountTransport) {
            groupDiscountTransport = 'no'
        }
        if (!isDiscountApplicable) {
            isDiscountApplicable = 'no'
        }
        if (!groupDiscountMultimedia) {
            groupDiscountMultimedia = 'no'
        }
        if (!groupDiscountTransport) {
            groupDiscountTransport = 'no'
        }
    }
}