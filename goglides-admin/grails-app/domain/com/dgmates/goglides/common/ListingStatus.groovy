package com.dgmates.goglides.common

public enum ListingStatus {

    PROCESSING('processing'),
    CONFIRMED('confirmed'),
    CANCELLED('cancelled'),
    TRASH('trash'),
    COMPLETED('completed')

    private final String description

    ListingStatus(String description) {
        this.description = description
    }

    public String description() {
        return this.description
    }

}
