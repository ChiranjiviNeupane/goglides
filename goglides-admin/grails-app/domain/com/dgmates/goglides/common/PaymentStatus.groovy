package com.dgmates.goglides.common

public enum PaymentStatus {

    PENDING('pending'),
    PAID('paid'),
    REVERSED('reversed'),
    REFUNDED('refunded'),
    FAILED('failed')

    private final String description

    PaymentStatus(String description) {
        this.description = description
    }

    public String description() {
        return this.description
    }

}
