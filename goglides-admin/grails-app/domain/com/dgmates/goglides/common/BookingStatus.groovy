package com.dgmates.goglides.common

public enum BookingStatus {

    PROCESSING('processing'),
    CONFIRMED('confirmed'),
    CANCELLED('cancelled'),
    TRASH('trash'),
    COMPLETED('completed'),
    CANCELLATIONREQUEST('cancellation_request')

    private final String description

    BookingStatus(String description) {
        this.description = description
    }

    public String description() {
        return this.description
    }

}
