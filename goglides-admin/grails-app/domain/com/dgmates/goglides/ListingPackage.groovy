package com.dgmates.goglides

import org.apache.commons.lang.RandomStringUtils

class ListingPackage {

    def friendlyUrlService

    Listing listing

    String tripTitle
    String slug
    String tripCode
    String referenceNo
    String packageCode
    String shortItinerary
    String detailItinerary
    String tripIncludes
    String tripExcludes
    String rateType
    String status
    Date createdAt
    Date modifiedAt
    Date trashedAt

    static hasMany = [listingprice: ListingPrice, listingAvailabilityDays: ListingAvailabilityDays]

    static constraints = {
        tripTitle blank: false, nullable: false
        slug blank: false, nullable: false
        tripCode blank: true, nullable: true
        referenceNo blank: true, nullable: true, unique: true
        packageCode blank: true, nullable: true, unique: true
        shortItinerary blank: true, nullable: true, maxSize: 10000
        detailItinerary blank: true, nullable: true, maxSize: 10000
        tripIncludes blank: true, nullable: true, maxSize: 10000
        tripExcludes blank: true, nullable: true, maxSize: 10000
        status blank: false, nullable: false
        createdAt nullable: true
        modifiedAt nullable: true
        trashedAt nullable: true
        rateType blank: true, nullable: true
    }

    static mapping = {

    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        modifiedAt = new Date()
        if (status == 'trash') {
            trashedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!slug) {
            try {
                String uniqueTitle = friendlyUrlService.sanitizeWithDashes(tripTitle) + " " + generateUrl(3)
                slug = friendlyUrlService.sanitizeWithDashes(uniqueTitle)
            }
            catch (Exception e) {
                slug = generateUrl(9)
            }
        }

        if (!referenceNo) {
            referenceNo = UUID.randomUUID().toString()
        }

        if (!packageCode) {
            packageCode = generateCode(8)
        }

        if (!status) {
            status = 'publish'
        }
    }

    def static generateUrl(int length) {
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        return RandomStringUtils.random(length, charset.toCharArray())
    }

    def static generateCode(int length) {
        def unique = false
        def ref_no
        def code
        while (!unique) {
            String charset = (('0'..'9')).join()
            ref_no = RandomStringUtils.random(length, charset.toCharArray())
            code = ListingPackage.findByPackageCode(ref_no)
            if (!code)
                unique = true
        }
        return 'PC' + ref_no
    }
}
