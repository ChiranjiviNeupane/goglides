package com.dgmates.goglides

class BusinessDirectoryMultimedia {

    BusinessDirectory businessDirectory

    String mediaType
    String file

    static belongsTo = [businessDirectory: BusinessDirectory]

    static constraints = {

    }

    static mapping = {

    }

}