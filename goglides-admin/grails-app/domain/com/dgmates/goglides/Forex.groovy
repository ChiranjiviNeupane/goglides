package com.dgmates.goglides

class Forex {
    String countryCode
    String currencyCode
    BigDecimal rate
    Date createdAt = new Date()
    Date updatedAt = new Date()
    Boolean status

    static constraints = {
        countryCode nullable: false, blank: false
        currencyCode nullable: false, blank: false
        rate nullable: false, blank: false

    }

    def beforeInsert() {
        createdAt = new Date()
        updatedAt = new Date()
        status = true
    }

    def beforeUpdate() {
        updatedAt = new Date()
    }
}
