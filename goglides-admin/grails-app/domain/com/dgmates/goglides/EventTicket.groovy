package com.dgmates.goglides
class EventTicket {
    EventBooking eventBooking
    String ticketNo
    String status


    static constraints = {
        status blank: true, nullable: true
        ticketNo blank: true ,nullable: true
    }
}
