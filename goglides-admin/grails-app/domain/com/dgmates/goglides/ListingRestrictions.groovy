package com.dgmates.goglides

class ListingRestrictions {

    Listing listing

    String minAge
    String maxAge
    String minWeight
    String maxWeight
    String maxPeople
    String minPeople
    String maxCapacity
    String availableCapacity
    String availabilityHour
    String availabilityUnit
    String bookingConfirmation

    static constraints = {
        minAge blank: true, nullable: true
        maxAge blank: true, nullable: true
        minWeight blank: true, nullable: true
        maxWeight blank: true, nullable: true
        maxPeople blank: true, nullable: true
        minPeople blank: true, nullable: true
        maxCapacity blank: true, nullable: true
        availableCapacity blank: true, nullable: true
        availabilityHour blank: true, nullable: true
        availabilityUnit blank: true, nullable: true
        bookingConfirmation blank: true, nullable: true
    }

    static mapping = {

    }

    def beforeValidate(){
        if(!bookingConfirmation){
            bookingConfirmation = 'instant'
        }
    }
}
