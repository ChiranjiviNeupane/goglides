package com.dgmates.goglides

class ListingAvailabilityDays {

    Listing listing
    ListingPackage listingPackage

    Boolean monday
    Boolean tuesday
    Boolean wednesday
    Boolean thursday
    Boolean friday
    Boolean saturday
    Boolean sunday

    static belongsTo = [listingPackage: ListingPackage]

    static constraints = {
        monday blank: true, nullable: true
        tuesday blank: true, nullable: true
        wednesday blank: true, nullable: true
        thursday blank: true, nullable: true
        friday blank: true, nullable: true
        saturday blank: true, nullable: true
        sunday blank: true, nullable: true
    }

    static mapping = {

    }

}
