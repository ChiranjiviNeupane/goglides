package com.dgmates.goglides

class ListingRecentlyViewed {
    User user
    Listing listing
    Date viewedAt
    Date modifiedAt

    static constraints = {
        user blank: true, nullable: true
        viewedAt nullable: true
        modifiedAt nullable: true
    }

    static mapping = {

    }

    def beforeInsert() {
        viewedAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        modifiedAt = new Date()
    }

    def beforeValidate() {
        if (!user) {
            user = null
        }
    }
}