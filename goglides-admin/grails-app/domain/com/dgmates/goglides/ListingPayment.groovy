package com.dgmates.goglides

class ListingPayment {

    Listing listing
    ListingPackage listingPackage
    User user
    BusinessDirectory businessDirectory

    Integer noOfGuest
    Integer guestSenior
    Integer guestAdult
    Integer guestChild
    Integer guestInfant
    Integer includeMultimedia
    Integer includeTransportation
    BigDecimal discount
    BigDecimal totalPrice
    BigDecimal grandTotal

    String paymentStatus
    String transactionId
    String payerId
    String payerToken
    String paymentType
    Date createdAt
    Date modifiedAt
    Date trashedAt
    String pickup
    String dropLocation
    String address
    String city
    String state
    String country
    String zipCode
    String lat
    String lng
    String customerComment
    String phone
    String bookingDate
    String bookingTime
    String duration

    String status
    String referenceNo
    String invoiceNo
    String confirmationCode
    String bookingType
    BigDecimal totalMultimedia
    BigDecimal totalTransport
    String emergencyContactPerson
    String emergencyContactNo
    String pickupTime
    String packageRate
    String appVersion

    static constraints = {
        noOfGuest nullable: true
        guestSenior nullable: true
        guestAdult nullable: true
        guestChild nullable: true
        guestInfant nullable: true
        discount nullable: true
        totalPrice nullable: true
        paymentStatus blank: true, nullable: true
        transactionId blank: true, nullable: true
        payerId blank: true, nullable: true
        payerToken blank: true, nullable: true
        paymentType blank: true, nullable: true
        createdAt nullable: true
        modifiedAt nullable: true
        trashedAt nullable: true
        pickup blank: true, nullable: true
        dropLocation blank: true, nullable: true
        address blank: true, nullable: true
        city blank: true, nullable: true
        state blank: true, nullable: true
        country blank: true, nullable: true
        zipCode blank: true, nullable: true
        lat blank: true, nullable: true
        lng blank: true, nullable: true
        customerComment blank: true, nullable: true, maxSize: 1000
        phone blank: true, nullable: true
        bookingDate blank: true, nullable: true
        bookingTime blank: true, nullable: true
        duration blank: true, nullable: true
        includeMultimedia blank: true, nullable: true
        includeTransportation blank: true, nullable: true
        status blank: true, nullable: true
        referenceNo blank: true, nullable: true
        invoiceNo blank: true, nullable: true
        confirmationCode blank: true, nullable: true
        user nullable: true
        businessDirectory nullable: true
        bookingType nullable: true, blank: true
        totalMultimedia nullable: true
        totalTransport nullable: true
        grandTotal nullable: true
        emergencyContactPerson blank: true, nullable: true
        emergencyContactNo blank: true, nullable: true
        pickupTime blank: true, nullable: true
        packageRate blank: true, nullable: true
        appVersion blank: true, nullable: true
    }

    static search = {
        paymentType index: 'yes'
        transactionId index: 'yes'
        paymentStatus index: 'yes'
        referenceNo index: 'yes'
        invoiceNo index: 'yes'
        bookingType index: 'yes'
    }


    def beforeValidate() {
        if (!bookingType) {
            bookingType = '_self'
        }
        if (!invoiceNo) {
            invoiceNo = generateInvoiceNo()
        }
    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
        if (!paymentStatus) {
            paymentStatus = 'pending'
        }
        if (!status) {
            status = 'processing'
        }
    }

    def beforeUpdate() {
        modifiedAt = new Date()
    }

    def generateInvoiceNo() {
        def invoiceNo

        def invoice = ListingPayment.createCriteria().get {
            projections {
                max "invoiceNo"
            }
        }

        if (!invoice)
            invoiceNo = '1000000001'
        else if (invoice) {
            invoiceNo = invoice.toInteger() + 1
        }

        return invoiceNo
    }
}
