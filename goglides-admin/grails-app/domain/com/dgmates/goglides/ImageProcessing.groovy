package com.dgmates.goglides

class ImageProcessing {

    String imageCategory
    int width
    int height
    int quality
    Date createdAt
    Date modifiedAt


    static constraints = {
        imageCategory nullable: true,blank: true
        width nullable: true, blank: true
        height nullable: true, blank: true
        createdAt nullable: true, blank: true
        modifiedAt nullable: true, blank :true
    }



    def beforeInsert(){
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate(){
        modifiedAt = new Date()
    }

}
