package com.dgmates.goglides

class ListingMultimedia {

    Listing listing

    String type
    String fileFormat
    String file

    static belongsTo = [listing: Listing]
    static constraints = {
        fileFormat blank: true, nullable: true
    }
}
