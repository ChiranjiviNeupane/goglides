package com.dgmates.goglides

class ListingPolicies {

    Listing listing

    String hasInsuranceCoverage
    String insuranceCompany
    String accidentalInsurance
    String deathInsurance
    String insuranceDocument
    String noInsuranceReason
    String cancellationPolicyType

    static constraints = {
        hasInsuranceCoverage blank: true, nullable: true
        insuranceCompany blank: true, nullable: true
        accidentalInsurance blank: true, nullable: true
        deathInsurance blank: true, nullable: true
        insuranceDocument blank: true, nullable: true
        noInsuranceReason blank: true, nullable: true
        cancellationPolicyType blank: true, nullable: true
    }

    static mapping = {

    }

    def beforeValidate() {

    }
}
