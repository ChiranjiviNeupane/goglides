package com.dgmates.goglides

import org.apache.commons.lang.RandomStringUtils

class SiteSettings {

    def friendlyUrlService

    String title
    String slug
    String value
    String defaultValue

    static constraints = {
        value blank: true, nullable: true, maxSize: 500
        defaultValue blank: true, nullable: true, maxSize: 500
    }

    def beforeValidate() {
        if (!slug) {
            try {
                String uniqueTitle = friendlyUrlService.sanitizeWithDashes(title)
                slug = friendlyUrlService.sanitizeWithDashes(uniqueTitle)
            } catch (Exception e) {
                slug = generateUrl(9)
            }
        }
    }

    def static generateUrl(int length) {
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        return RandomStringUtils.random(length, charset.toCharArray())
    }
}