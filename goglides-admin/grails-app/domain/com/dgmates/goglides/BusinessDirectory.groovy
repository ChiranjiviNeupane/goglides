package com.dgmates.goglides

import org.apache.commons.lang.RandomStringUtils

class BusinessDirectory {

    def friendlyUrlService
    def userService

    User user

    String businessName
    String businessId
    String slug
    String businessDescription
    String registration
    String businessType
    String website
    String email
    String phone1
    String phone2
    String phone3
    String addressLine1
    String addressLine2
    String geolocationCity
    String geolocationStateLong
    String geolocationCountryLong
    String geolocationPostcode
    String geolocationLat
    String geolocationLong
    String geolocationFormattedAddress
    String affiliations
    String approved
    Boolean transferred = 0
    Boolean emailVerification = 0
    Boolean registeredDocumentsVerification
    Boolean taxDocumentsVerification
    Boolean affiliationsVerification = 0
    String verificationCode
    String contactJobTitle
    String contactFirstName
    String contactMiddleName
    String contactLastName
    String contactEmail
    String contactPhone
    Boolean isClaimable = 0
    String status
    String ticketId
    Boolean ticket
    Date createdAt
    Date modifiedAt
    Date trashedAt

    static constraints = {
        businessName blank: false, nullable: false, maxSize: 255
        slug blank: true, nullable: true
        businessDescription blank: true, nullable: true, maxSize: 10000
        registration blank: false, nullable: false
        businessType blank: false
        website blank: true, nullable: true
        email blank: false, nullable: false, email: true, unique: true
        phone1 blank: true, nullable: true
        phone2 blank: true, nullable: true
        phone3 blank: true, nullable: true
        addressLine1 blank: true, nullable: true
        addressLine2 blank: true, nullable: true
        geolocationCity blank: true, nullable: true
        geolocationStateLong blank: true, nullable: true
        geolocationCountryLong blank: true, nullable: true
        geolocationPostcode blank: true, nullable: true
        geolocationLat blank: true, nullable: true
        geolocationLong blank: true, nullable: true
        geolocationFormattedAddress blank: true, nullable: true
        affiliations blank: true, nullable: true, maxSize: 10000
        approved blank: true, nullable: true
        transferred blank: true, nullable: true
        createdAt nullable: true
        modifiedAt nullable: true
        trashedAt nullable: true
        emailVerification blank: true, nullable: true, defaultValue: '0'
        registeredDocumentsVerification blank: true, nullable: true, defaultValue: '0'
        taxDocumentsVerification blank: true, nullable: true, defaultValue: '0'
        affiliationsVerification blank: true, nullable: true, defaultValue: '0'
        verificationCode blank: true, nullable: true
        contactJobTitle blank: true, nullable: true
        contactFirstName blank: true, nullable: true
        contactMiddleName blank: true, nullable: true
        contactLastName blank: true, nullable: true
        contactEmail blank: true, nullable: true
        contactPhone blank: true, nullable: true
        isClaimable blank: true, nullable: true
        ticketId blank: true, nullable: true
        ticket blank: true, nullable: true
        status blank: true, nullable: true
    }

    static mapping = {
        businessDescription type :'text'
    }

    static search = {
        analyzer = 'ngram'
        businessName index : 'yes'
        businessType index : 'yes'
        businessId index: 'yes'
        status index : 'yes'

    }

    def beforeInsert() {
        transferred = Boolean.FALSE
        geolocationFormattedAddress = addressLine1
        if (!approved)
            approved = 'pending'
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        geolocationFormattedAddress = addressLine1
        modifiedAt = new Date()
        if (status == 'trash') {
            trashedAt = new Date()
        }
    }

    def beforeValidate() {
        if (!user) {
            user = userService.currentUser()
        }
        if (!businessId) {
            businessId = generateId(8)
        }
        if (!slug) {
            try {
                String uniqueTitle = friendlyUrlService.sanitizeWithDashes(businessName)
                slug = friendlyUrlService.sanitizeWithDashes(uniqueTitle)
            } catch (Exception e) {
                slug = slug = generateUrl(9)
            }
        }
        if (!isClaimable) {
            isClaimable = 'not_claimable'
        }
    }

    def static generateUrl(int length) {
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        return RandomStringUtils.random(length, charset.toCharArray())
    }

    def static generateId(int length) {
        def unique = false
        def refNo
        def business
        while (!unique) {
            String charset = (('0'..'9')).join()
            refNo = RandomStringUtils.random(length, charset.toCharArray())
            business = BusinessDirectory.findWhere(businessId: refNo)
            if (!business)
                unique = true
        }
        return refNo
    }
}
