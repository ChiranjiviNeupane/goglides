package com.dgmates.goglides

class ContactBook {

    String businessType
    String companyName
    String streetAddress
    String city
    String country
    String phone1
    String phone2
    String phone3
    String companyEmail
    String website
    String facebook
    String contactPerson
    String jobPosition
    String mobile
    String email
    Date createdAt = new Date()
    Date updatedAt = new Date()
    Date modifiedAt = new Date()

    static constraints = {
        businessType blank: true, nullable: true
        companyName blank: true, nullable: true
        streetAddress blank: true, nullable: true
        city blank: true, nullable: true
        country blank: true, nullable: true
        phone1 blank: true, nullable: true
        phone2 blank: true, nullable: true
        phone3 blank: true, nullable: true
        companyEmail blank: true, nullable: true
        website blank: true, nullable: true
        facebook blank: true, nullable: true
        contactPerson blank: true, nullable: true
        jobPosition blank: true, nullable: true
        mobile blank: true, nullable: true
        email blank: true, nullable: true
        createdAt nullable: true
        updatedAt nullable: true
        modifiedAt nullable: true
    }

    static search = {
        analyzer = 'ngram'
        companyName index: 'yes'
        contactPerson index: 'yes'
        businessType index: 'yes'
        email index: 'yes'

    }


    def beforeValidate() {
        createdAt = new Date()
        modifiedAt = new Date()
    }

    def beforeUpdate() {
        modifiedAt = new Date()
    }
}
