package com.dgmates.goglides

class Multimedia {

    ListingReview listingReview
    String fileType
    String url
    String status

    static belongsTo = [listingReview: ListingReview]

    static constraints = {
        listingReview blank: false, nullable: false
        fileType blank: false, nullable: false, inList: ["_audio", "_video", "_image"]
        url blank: false, nullable: false
        status blank: false, nullable: false, inList: ["publish", "draft", "cancel"]
    }

    static mapping = {

    }

    def beforeValidate() {
        status = "publish"
    }
}