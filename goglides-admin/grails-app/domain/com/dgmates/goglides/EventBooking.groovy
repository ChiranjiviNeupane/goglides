package com.dgmates.goglides
class EventBooking {
    Event event
    EventCharge eventCharge
    String firstname
    String lastname
    String email
    String phone
    String quantity
    String totalAmount
    String paymentStatus
    String paymentRefunded
    String partialRefunded
    String status
    BigInteger invoiceNo
    String paymentMode
    String transactionId
    Date createdAt
    Date modifiedAt
    Date deletedAt

    static constraints = {
        eventCharge blank: true, nullable: true
        firstname blank: true , nullable: true
        lastname blank: true , nullable: true
        email blank: true , nullable: true
        phone blank: true , nullable: true
        quantity blank: true , nullable: true
        totalAmount blank: true , nullable: true
        paymentStatus blank: true , nullable: true
        paymentRefunded blank: true , nullable: true
        partialRefunded blank: true , nullable: true
        status blank: true , nullable: true
        invoiceNo blank: true , nullable: true
        paymentMode blank: true , nullable: true
        transactionId blank: true , nullable: true
        createdAt blank: true , nullable: true
        modifiedAt blank: true , nullable: true
        deletedAt blank: true , nullable: true
    }



    def beforeValidate() {
        if (!invoiceNo) {
            invoiceNo = generateInvoiceNo()
        }
    }


    def beforeInsert(){
        createdAt = new Date()
        modifiedAt = new Date()
        paymentStatus = 'pending'
    }

    def beforeUpdate(){
        modifiedAt = new Date()
    }



    def generateInvoiceNo() {
        def invoiceNo

        def invoice = EventBooking.createCriteria().get {
            projections {
                max "invoiceNo"
            }
        }

        if (!invoice)
            invoiceNo = 1000
        else if (invoice) {
            invoiceNo = invoice + 1
        }

        return invoiceNo
    }

}
