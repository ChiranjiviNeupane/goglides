package com.dgmates.goglides

class BookingItems {
    ListingPayment listingPayment
    ListingPrice listingPrice

    String ticketNo
    String type
    String ageGroup
    String duration
    String schedule
    String nationality
    BigDecimal amount
    BigDecimal transportationCharge
    BigDecimal multimediaCharge
    BigDecimal totalAmount
    String title
    String fullName
    String contactNumber
    String email
    String passportNumber
    String country
    String weight
    String addTransport
    String addMultimedia

    static constraints = {
        listingPrice blank: true, nullable: true
        ticketNo blank: true, nullable: true
        type blank: true, nullable: true
        ageGroup blank: true, nullable: true
        duration blank: true, nullable: true
        schedule blank: true, nullable: true
        nationality blank: true, nullable: true
        amount nullable: true
        totalAmount nullable: true
        transportationCharge blank: true, nullable: true
        multimediaCharge blank: true, nullable: true
        title blank: true, nullable: true
        fullName blank: true, nullable: true
        contactNumber blank: true, nullable: true
        email blank: true, nullable: true
        passportNumber blank: true, nullable: true
        country blank: true, nullable: true
        weight blank: true, nullable: true
        addTransport blank: true, nullable: true
        addMultimedia blank: true, nullable: true
    }




}