package com.dgmates.goglides

import org.apache.commons.lang.RandomStringUtils

class Listing {

    def friendlyUrlService
    def userService

    User user
    BusinessDirectory businessDirectory
    Category category

    String title
    String slug
    String metaTitle
    String metaKeyword
    String metaDescription
    String listingCode
    String description
    String highlights
    String nearestCity
    String nearestAirport
    String address
    String city
    String state
    String country
    String zipCode
    String lat
    String lng
    String status
    String availability
    String referenceNo
    String stepNo
    String stepPackageId
    String stepPackageNo
    String isApproved
    Boolean transferred = 0
    Boolean isFeatured = 0
    Date featuredStart
    Date featuredEnd
    Date createdAt
    Date modifiedAt
    Date trashedAt

    static hasMany = [listingPrice: ListingPrice, listingMultimedia: ListingMultimedia, listingReview: ListingReview]
    static belongsTo = [category: Category, businessDirectory: BusinessDirectory]

    static constraints = {
        title blank: false, nullable: false
        slug blank: true, nullable: true
        metaTitle blank: true, nullable: true, maxSize: 1000
        metaKeyword blank: true, nullable: true, maxSize: 1000
        metaDescription blank: true, nullable: true, maxSize: 1000
        listingCode blank: true, nullable: true
        description blank: true, nullable: true, maxSize: 10000
        highlights blank: true, nullable: true, maxSize: 10000
        nearestCity blank: true, nullable: true
        nearestAirport blank: true, nullable: true
        address blank: true, nullable: true
        city blank: true, nullable: true
        state blank: true, nullable: true
        country blank: true, nullable: true
        zipCode blank: true, nullable: true
        lat blank: true, nullable: true
        lng blank: true, nullable: true
        status blank: true, nullable: true
        transferred blank: true, nullable: true
        isFeatured blank: true, nullable: true
        featuredStart nullable: true
        featuredEnd nullable: true
        createdAt nullable: true
        modifiedAt nullable: true
        trashedAt nullable: true
        availability blank: true, nullable: true
        businessDirectory nullable: true
        referenceNo blank: true, nullable: true, unique: true
        stepNo blank: true, nullable: true
        stepPackageId blank: true, nullable: true
        stepPackageNo blank: true, nullable: true
        isApproved blank: true, nullable: true
        category nullable: true
    }

    static mapping = {
        description sqlType: "longText"
        highlights sqlType: "longText"
    }

    static search = {
        analyzer = 'ngram'
        title index: 'yes'
        referenceNo index: 'yes'
        status index: 'yes'
        listingCode index: 'yes'
    }

    def beforeInsert() {
        createdAt = new Date()
        modifiedAt = new Date()
        availability = 'available'
        status = 'draft'
    }

    def beforeUpdate() {
        modifiedAt = new Date()
    }

    def beforeValidate() {
        if (!user) {
            user = userService.currentUser()
        }

        if (!slug) {
            try {
                String uniqueTitle = friendlyUrlService.sanitizeWithDashes(title)
                slug = friendlyUrlService.sanitizeWithDashes(uniqueTitle)
            }
            catch (Exception e) {
                slug = generateUrl(9)
            }
        }

        if (!referenceNo) {
            referenceNo = UUID.randomUUID().toString()
        }

        if (!listingCode) {
            listingCode = generateId(8)
        }
    }

    def static generateUrl(int length) {
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        return RandomStringUtils.random(length, charset.toCharArray())
    }

    def static generateId(int length) {
        def unique = false
        def ref_no
        def listing
        while (!unique) {
            String charset = (('0'..'9')).join()
            ref_no = RandomStringUtils.random(length, charset.toCharArray())
            listing = Listing.findByListingCode(ref_no)
            if (!listing)
                unique = true
        }
        return 'LI' + ref_no
    }
}
