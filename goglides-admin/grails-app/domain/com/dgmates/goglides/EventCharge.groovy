
package com.dgmates.goglides
class EventCharge {
    Event event
    String ticketTitle
    String description
    String wholeSaleRate
    String slug
    String retailRate
    String status
    String availableStock
    String createdAt
    String deletedAt
    String modifiedAt


    static constraints = {
        ticketTitle blank: true, nullable: true
        description blank: true, nullable: true
        slug blank: true, nullable: true
        wholeSaleRate blank: true, nullable: true
        status blank: true, nullable: true
        retailRate blank: true, nullable: true
        availableStock blank: true, nullable: true
        createdAt blank: true, nullable: true
        modifiedAt blank: true, nullable: true
        deletedAt blank: true, nullable: true
    }

    static mapping = {

    }


    def beforeInsert(){
        createdAt = new Date()
        modifiedAt = new Date()
    }
    def beforeUpdate(){
        modifiedAt = new Date()
    }

    def beforeDelete(){
        deletedAt=new Date()
    }


}