package com.dgmates.goglides

import com.amazonaws.services.sns.model.MessageAttributeValue
import com.amazonaws.services.sns.model.PublishRequest
import com.amazonaws.services.sns.model.PublishResult
import com.dgmates.goglides.AmazonWebService
import grails.transaction.Transactional

@Transactional
class MobileMessagingService {

    AmazonWebService amazonWebService

    def sendSMS(String message, String phoneNumber) {

        try {
            Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>()

            smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                    .withStringValue("ggSenderID") //The sender ID shown on the device.
                    .withDataType("String"))
            smsAttributes.put("AWS.SNS.SMS.MaxPrice", new MessageAttributeValue()
                    .withStringValue("0.50") //Sets the max price to 0.50 USD.
                    .withDataType("Number"))
            smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
                    .withStringValue("Transactional") //Sets the type to promotional.
                    .withDataType("String"))

            def snsClient = amazonWebService.getSNS()

            PublishResult result = snsClient.publish(new PublishRequest()
                    .withMessage(message)
                    .withPhoneNumber(phoneNumber)
                    .withMessageAttributes(smsAttributes))

        } catch (Exception e) {
            print(e)
            return false
        }
        return true

    }

    def tokenGenerator() {
        Random rnd = new Random()
        100000 + rnd.nextInt(900000) as String
    }

}
