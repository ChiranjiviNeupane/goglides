package com.dgmates.goglides

import com.amazonaws.services.simpleemail.model.Body
import com.amazonaws.services.simpleemail.model.Content
import com.amazonaws.services.simpleemail.model.Destination
import com.amazonaws.services.simpleemail.model.Message
import com.amazonaws.services.simpleemail.model.SendEmailRequest
import grails.transaction.Transactional

@Transactional
class EmailMessagingService {

    def amazonWebService
    def siteSettingsService
    DirectoryService directoryService

    def sendEmail(String FROM, String TO, String SUBJECT, String BODY) {

        Destination destination = new Destination().withToAddresses(TO)

        Content subject = new Content().withData(SUBJECT)
        Content textBody = new Content().withData(BODY)
        Body body = new Body().withHtml(textBody)
        Message message = new Message().withSubject(subject).withBody(body)

        //        @TODO refactor this use instance AMI profile instead
        SendEmailRequest request = new SendEmailRequest().withSource(FROM).withDestination(destination).withMessage(message)

        def sesClient = amazonWebService.getSES('us-west-2')

        sesClient.sendEmail(request)

    }

    def fromEmail() {
        def siteMode = siteSettingsService.getSiteMode()
        switch (siteMode) {
            case 'live':
                return siteSettingsService.getEmail()
            default:
                return 'goglides.redmine@gmail.com'
        }
    }

    def adminEmail() {
        def siteMode = siteSettingsService.getSiteMode()
        switch (siteMode) {
            case 'live':
                return siteSettingsService.getEmail()
            default:
                return 'goglides.redmine@gmail.com'
        }
    }

    def agentEmail(User userInstance) {
        def siteMode = siteSettingsService.getSiteMode()
        switch (siteMode) {
            case 'live':
                def businessDirectory = directoryService.getBusinessDirectory(userInstance)
                def agentContactEmail = ''
                if (businessDirectory) {
                    agentContactEmail = User.get(businessDirectory?.userId.toInteger())?.email
                }
                return agentContactEmail
            default:
                return 'shreejalshakyas@gmail.com'
        }

    }
}
