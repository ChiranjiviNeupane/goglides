package com.dgmates.goglides

import grails.transaction.Transactional
import org.jsoup.Jsoup

import java.text.BreakIterator

@Transactional
class CommonService {

    AmazonS3Service amazonS3Service
    def grailsApplication

    def embedVideoUrl(String url) {
        def youtube_video_id = ''
        def value = ''
        def embed_url = ''

        value = url.split('embed/')

        if (value.size() > 1) {
            url = value[1]
            value = url.split('"')
            youtube_video_id = value[0]
        }

        value = url.split('.be/')
        if (value.size() > 1) {
            youtube_video_id = value[1]
        }

        value = url.split('v=')
        if (value.size() > 1) {
            youtube_video_id = value[1]
        }

        if (youtube_video_id != '') {
            embed_url = '<iframe width="560" height="315" src="https://www.youtube.com/embed/' + youtube_video_id + '" frameborder="0" allowfullscreen></iframe>'
        }

        return embed_url
    }

    def getStartEndDate(type) {
        def startDate = new Date().clearTime()
        def endDate = new Date().clearTime()
        switch (type) {
            case 'upcoming':
                startDate = new Date() + 1
                endDate = new Date() + 30
                break
            case 'today':
                startDate = startDate
                endDate = endDate + 1
                break
            case 'yesterday':
                startDate = new Date() - 1
                endDate = endDate - 1
                break
            case 'last 7 days':
                startDate = new Date() - 7
                endDate = endDate + 1
                break
            case 'last 30 days':
                startDate = new Date() - 30
                endDate = endDate + 1
                break
            default:
                startDate = startDate
                endDate = endDate + 1
                break
        }
        return [startDate: startDate, endDate: endDate]
    }


    def truncateString(String content) {
        def contentLength = 150
        def result

        if (content) {
            content = Jsoup.parse(content).text();
        }

        //Is content > than the contentLength?
        if (content != null && content.size() > contentLength) {
            BreakIterator bi = BreakIterator.getWordInstance()
            bi.setText(content)
            def first_after = bi.following(contentLength)

            //Truncate
            result = content.substring(0, first_after) + "..."
        } else {
            result = content
        }

        return result
    }

    def getImageUrl(String file) {
        def url = amazonS3Service.sourceServerUrl()
        if (file) {
            return url + file
        }
        return ''
    }


}