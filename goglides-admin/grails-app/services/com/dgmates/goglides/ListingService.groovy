package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class ListingService {
    def amazonS3Service
    def directoryService

    def getAllListings(userInstance, params) {
        def businessDirectory
        def listing = Listing.createCriteria()
        def listings = listing.list(max: params.max, offset: params.offset) {
            if (params?.state) {
                eq('status', params?.state)
            } else {
                ne('status', 'trash')
            }
            order("id", "desc")
            if (params.businessId) {
                businessDirectory = BusinessDirectory.findByBusinessId(params?.businessId)
                eq('businessDirectory', businessDirectory)
            } else if (!params?.businessId) {
                businessDirectory = directoryService.getBusinessDirectory(userInstance)
            }
        }

        return listings
    }

    def splitDuration(duration) {
        def durations = duration
        def (week, days, hours, minutes) = durations.split(':')
        return [w: week, d: days, h: hours, m: minutes]
    }

    def formatDuration(String duration) {
        def durations = duration
        def (week, days, hours, minutes) = durations.split(':')
        //return minutes
        def dur = ''
        if (week != '00') {
            dur = week + " weeks "
        }
        if (days != '00') {
            dur = dur + days + " days "
        }
        if (hours != '00') {
            dur = dur + hours + " hours "
        }
        if (minutes != '00') {
            dur = dur + minutes + " minutes "
        }
        return [d: dur, du: duration]
    }

    def formatSchedule(String schedule) {
        def schedules = schedule
        if(!schedules){
            return  schdl = '00:00' + ":" + '00:00' + " PM"
        }
        def (hr, min) = schedules.split(':')
        def hour = hr.toInteger()
        def minute = min.toInteger()

        def schdl = ' '
        if (hour > 0 && hour < 12) {
            schdl = hour + ":" + minute + " AM"
        }
        if (hour == 24) {
            hour = 00
            schdl = hour + ":" + minute + " AM"
        } else if (hour > 12) {
            hour = hour - 12
            schdl = hour + ":" + minute + " PM"
        }
        return schdl
    }

    /**
     * Set the total no of primary steps in the listing form.
     * @return
     */
    def totalSteps() {
        return 8
    }

    /**
     * Find the listing by listing referenceNo.
     * @param referenceNo
     * @return object
     */
    def getListingByReferenceNo(def referenceNo) {
        return Listing.findByReferenceNo(referenceNo)
    }

    /**
     * Find the feature image of listing and return the image path.
     * @param id
     * @return
     */
    def getListingFeatureImage(def id) {
        def listing = Listing.get(id)
        def featureImage = ListingMultimedia.findWhere(listing: listing, type: '_feature')
        def url
        if (featureImage) {
            url = amazonS3Service.sourceBucketCDN() + featureImage.file
        }
        return url
    }


    def getInsuranceDocument(def insuranceDocument){

        def insuranceDocuemt = ListingMultimedia.findWhere(file: insuranceDocument, type: '_insurance')
        def url
        if (insuranceDocuemt) {
            url = amazonS3Service.sourceBucketCDN() + insuranceDocuemt.file
        }
        return url
    }
    /**
     * Find the listing category.
     * @param id
     * @return
     */
    def getListingCategory(def id) {
        def listing = Listing.get(id)

        return listing?.category?.categoryTitle
    }

    /**
     * Calculate the average rating of the listing.
     * @param id
     * @return
     */
    def getAverageRating(int id) {
        def listing = Listing.get(id)
        def reviews = ListingReview.findAllByListing(listing)
        def review_count = reviews ? reviews.size : 0
        def total_rating = 0
        def avg_rating = 0

        if (reviews) {
            reviews.each { review ->
                total_rating = total_rating.toInteger() + review.rating.toInteger()
            }
            avg_rating = total_rating / review_count
        }
        return avg_rating.toInteger()
    }

    /**
     * Get the total reivew count of the listing.
     * @param id
     * @return
     */
    def getReviewCount(int id) {
        def listing = Listing.get(id)
        def reviews = ListingReview.findAllByListing(listing)
        def review_count = reviews.size

        return review_count
    }

    /**
     * Get the insurance company info.
     * @param listing
     * @return object
     */
    def getInsuranceCompany(Listing listing) {
        return ListingPolicies.findByListing(listing)?.insuranceCompany ?: 'N.A.'
    }
}