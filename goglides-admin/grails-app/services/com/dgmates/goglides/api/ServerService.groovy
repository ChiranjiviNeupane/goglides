package com.dgmates.goglides.api

import com.dgmates.goglides.*
import com.paypal.base.Constants
import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Transactional
class ServerService {

    def paypalService
    def grailsApplication
    SiteSettingsService siteSettingsService
    EmailMessagingService emailMessagingService
    DirectoryService directoryService
    def groovyPageRenderer
    UserService userService

    def processPaypalPayment(ListingPayment booking, Listing listing){
         String clientId = siteSettingsService.getPaypalClientId()
         String clientSecret = siteSettingsService.getPaypalClientSecret()
         String endpoint = siteSettingsService.getPaypalEndPoint()
        Map sdkConfig = [
                (Constants.CLIENT_ID): clientId,
                (Constants.CLIENT_SECRET): clientSecret,
                (Constants.ENDPOINT): endpoint
        ]

        def accessToken = paypalService.getAccessToken(clientId, clientSecret, sdkConfig)
        def apiContext = paypalService.getAPIContext(accessToken, sdkConfig)

        String total = booking?.grandTotal as BigDecimal
        def description = listing?.title + '-' + booking?.id

        def details = paypalService.createDetails(subtotal: total)
        def amount = paypalService.createAmount(currency: siteSettingsService.getPaypalCurrencyCode(), total: total, details: details)

        def transaction = paypalService.createTransaction(amount: amount, description: description, details: details)
        def transactions = [transaction]

        def payer = paypalService.createPayer(paymentMethod: 'paypal')
        def cancelUrl = siteSettingsService.getServerUrl() + grailsApplication.config.paypal.paymentCancelUrl + '/' + booking?.id
        def returnUrl = siteSettingsService.getServerUrl() + grailsApplication.config.paypal.paymentReturnUrl

        def redirectUrls = paypalService.createRedirectUrls(cancelUrl: cancelUrl, returnUrl: returnUrl)

        def payment
        try{
            payment = paypalService.createPayment(
                    payer: payer,
                    intent: 'sale',
                    transactionList: transactions,
                    redirectUrls: redirectUrls,
                    apiContext: apiContext
            )
        } catch(e) {
            log.error("Payment Error : " + e.getMessage())
            return[responseStatus: 'error', message : e.getMessage()]
        }

        def approvalUrl=""
        def retUrl = ""
        for(links in payment?.links){
            if(links?.rel == "approval_url") {
                approvalUrl = links.href
            }
            if(links?.rel == "return_url"){
                retUrl = links.href
            }
        }

        return [responseStatus: 'success', url: approvalUrl, method: 'POST', payment : payment]
    }

    def executePaypalPayment(params){
        String clientId = siteSettingsService.getPaypalClientId()
        String clientSecret = siteSettingsService.getPaypalClientSecret()
        String endpoint = siteSettingsService.getPaypalEndPoint()
        Map sdkConfig = [
                (Constants.CLIENT_ID) = clientId,
                (Constants.CLIENT_SECRET) = clientSecret,
                (Constants.ENDPOINT) = endpoint
        ]

        def accessToken = paypalService.getAccessToken(clientId, clientSecret, sdkConfig)
        def apiContext = paypalService.getAPIContext(accessToken, sdkConfig)

        def paypalPayment = paypalService.createPayentExecution(paymentId : params.paymentId, payerId: params.PayerId, apiContext)

        def map = new JsonSlurper().parseText(paypalPayment.toString())

        def itemDesc = map.transactions.description.toString()
        def (value1, value2) = itemDesc.tokenize('-')
        def listingPaymentId = value2.replace(']','')
        def state = map.state.toString()
        def transactionId = map.id.toString()

        try{
            if(state == 'approved') {
                def listingPayment = ListingPayment.findById(listingPaymentId)
                listingPayment.paymentStatus = 'paid'
                listingPayment.transactionId = transactionId
                listingPayment.save(flush: true)

                def listing = Listing.findById(listingPayment?.listingId.toInteger())
                return [responseStatus: 'success', booking: listingPayment]
            }
        } catch (Exception e){
            log.error("Error : " + e.getMessage())
        }
        return [responseStatus: 'error']
    }

    def sendNewOrderEmail(def booking){
        try {
            def listing = Listing.get(booking?.listingId)
            def bookingItem = BookingItems.findByListingPayment(booking)
            def userRef = User.get(booking?.userId)
            def user = userService.mapUser(userRef)
            def userEmail = user?.email
            def fromEmail = emailMessagingService.fromEmail()
            def adminEmail = emailMessagingService.adminEmail()
            def userInstance = User.findById(listing?.userId.toInteger())

            def agentContactEmail = ''
            agentContactEmail = emailMessagingService.agentEmail(userInstance)

            def subject = "GoGlides - New Order"

            def message = groovyPageRenderer.render(view: '/templates/mails/booking/_new_order_admin', model: [user: user, listing: listing, listingPrice: bookingItem, booking: booking])

            emailMessagingService.sendEmail(fromEmail, adminEmail, subject, message)

            // if (businessDirectory && agentContactEmail)
            if (agentContactEmail != '')
                emailMessagingService.sendEmail(fromEmail, agentContactEmail, subject, message)
        }
        catch (Exception e) {
            log.error "Error: ${e.message}", e
        }
        return true
    }


    def sendBookingEmail(def booking) {
        try {
            def listing = Listing.get(booking?.listingId)
            def bookingItem = BookingItems.findByListingPayment(booking)
            def userRef = User.get(booking?.userId)
            def user = userService.mapUser(userRef)
            def userEmail = user?.email
            def fromEmail = emailMessagingService.fromEmail()
            def adminEmail = emailMessagingService.adminEmail()
            def userInstance = User.findById(listing?.userId.toInteger())

            def agentContactEmail = ''
            agentContactEmail = emailMessagingService.agentEmail(userInstance)

            def subject = "GoGlides - New Order"

            def message = groovyPageRenderer.render(view: '/templates/mails/booking/_new_order_admin', model: [user: user, listing: listing, listingPrice: bookingItem, booking: booking])
            def userMessage = groovyPageRenderer.render(view: '/templates/mails/booking/_new_order_user', model: [user: user, listing: listing, listingPrice: bookingItem, booking: booking])

            emailMessagingService.sendEmail(fromEmail, adminEmail, subject, message)

            // if (businessDirectory && agentContactEmail)
            if (agentContactEmail != '')
                emailMessagingService.sendEmail(fromEmail, agentContactEmail, subject, message)

            emailMessagingService.sendEmail(fromEmail, userEmail, subject, userMessage)
        }
        catch (Exception e) {
            log.error "Error: ${e.message}", e
        }
        return true
    }

    def bookingCancelRequestEmail(def data){
        try{
            def agentEmail = emailMessagingService.agentEmail(user)
            def adminEmail = emailMessagingService.adminEmail()
            def subject = "GoGlides - Cancellation Request"
            def message = groovyPageRenderer.render(view: '/templates/mails/booking/_cancellation_request', model: [data:data[0]])
            emailMessagingService.sendEmail(emailMessagingService.fromEmail(), adminEmail, subject, message)
            emailMessagingService.sendEmail(emailMessagingService.fromEmail(), agentEmail, subject, message)
        }catch(Exception e){
            log.error "Error: ${e.message}", e
        }
    }
}
