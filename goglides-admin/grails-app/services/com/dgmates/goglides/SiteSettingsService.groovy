package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class SiteSettingsService {

    def grailsApplication

    def getSiteTitle() {
        def siteTitle = SiteSettings.findBySlug('site-title')
        return siteTitle?.value ?: siteTitle?.defaultValue
    }

    def getTagline() {
        def tagLine = SiteSettings.findBySlug('tagline')
        return tagLine?.value ?: tagLine?.defaultValue
    }

    def getEmail() {
        def email = SiteSettings.findBySlug('email')
        return email?.value ?: email?.defaultValue
    }

    def getPhone1() {
        def phone1 = SiteSettings.findBySlug('phone1')
        return phone1?.value ?: phone1?.defaultValue
    }

    def getPhone2() {
        def phone2 = SiteSettings.findBySlug('phone2')
        return phone2?.value ?: phone2?.defaultValue
    }

    def getAddress1() {
        def address1 = SiteSettings.findBySlug('address1')
        return address1?.value ?: address1?.defaultValue
    }

    def getAddress2() {
        def address2 = SiteSettings.findBySlug('address2')
        return address2?.value ?: address2?.defaultValue
    }

    def getFacebook() {
        def facebook = SiteSettings.findBySlug('facebook')
        return facebook?.value ?: facebook?.defaultValue
    }

    def getTwitter() {
        def twitter = SiteSettings.findBySlug('twitter')
        return twitter?.value ?: twitter?.defaultValue
    }

    def getGooglePlus() {
        def googlePlus = SiteSettings.findBySlug('googleplus')
        return googlePlus?.value ?: googlePlus?.defaultValue
    }

    def getYoutube() {
        def youtube = SiteSettings.findBySlug('youtube')
        return youtube?.value ?: youtube?.defaultValue
    }

    def getCopyright() {
        def copyright = SiteSettings.findBySlug('copyright')
        return copyright?.value ?: copyright?.defaultValue
    }

    def getGoogleMap() {
        def googleMap = SiteSettings.findBySlug('google-map')
        return googleMap?.value ?: googleMap?.defaultValue
    }

    def getBusinessHours() {
        def businessHours = SiteSettings.findBySlug('business-hours')
        return businessHours?.value ?: businessHours?.defaultValue
    }

    def getGoogleMapAPIKey() {
        def googleMapAPIKey = SiteSettings.findBySlug('google-map-api-key')
        return googleMapAPIKey?.value ?: googleMapAPIKey?.defaultValue
    }

    def getMetaKeyword() {
        def metaKeyword = SiteSettings.findBySlug('meta-keyword')
        return metaKeyword?.value ?: metaKeyword?.defaultValue
    }

    def getMetaDescription() {
        def metaDescription = SiteSettings.findBySlug('meta-description')
        return metaDescription?.value ?: metaDescription?.defaultValue
    }

    def getSiteMode() {
        def siteMode = SiteSettings.findBySlug('site-mode')
        return siteMode?.value ?: siteMode?.defaultValue
    }

    def getPaypalMode() {
        def paypalMode = SiteSettings.findBySlug('paypal-mode')
        return paypalMode?.value ?: paypalMode?.defaultValue
    }

    def getPaypalClientId() {
        def paypalMode = getPaypalMode()
        if (paypalMode == 'sandbox')
            return grailsApplication.config.paypal.sandbox.clientId
        else
            return grailsApplication.config.paypal.clientId
    }


    def getPaypalClientSecret() {
        def paypalMode = getPaypalMode()
        if (paypalMode == 'sandbox')
            return grailsApplication.config.paypal.sandbox.clientSecret
        else
            return grailsApplication.config.paypal.clientSecret
    }

    def getPaypalEndPoint() {
        def paypalMode = getPaypalMode()
        if (paypalMode == 'sandbox')
            return grailsApplication.config.paypal.sandbox.endpoint
        else
            return grailsApplication.config.paypal.endpoint
    }

    def getServerUrl() {
        def paypalMode = getPaypalMode()
        def serverUrl = ''
        if (paypalMode == 'live') {
            serverUrl = SiteSettings.findBySlug('server-url')
            return serverUrl?.defaultValue
        } else {
            serverUrl = SiteSettings.findBySlug('server-url')
            return serverUrl?.value
        }
    }

    def getPaypalCurrencyCode() {
        return grailsApplication.config.paypal.currencyCode
    }

    def getPaypalReturnUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.paypal.returnUrl
    }

    def getPaypalcancelUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.paypal.cancelUrl
    }

    def getEventPaypalReturnUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.paypal.eventReturnUrl
    }

    def getEventPaypalcancelUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.paypal.eventCancelUrl
    }

    def getPaypalredirectUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.paypal.redirectUrl
    }

//    ipay  service

    def getIpayMerchantId() {
        return grailsApplication.config.ipay.merchantId
    }

    def getIpayCurrencyCode() {
        return grailsApplication.config.ipay.currencyCode
    }

    def getIpayReturnUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.ipay.returnUrl
    }

    def getiPaycancelUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.ipay.cancelUrl
    }

    def getiPayredirectUrl() {
        return grailsApplication.config.grails.serverURL + grailsApplication.config.ipay.redirectUrl
    }

    static getKeyCloakServerURL() {
        def keyCloakServerURL = SiteSettings.findBySlug('keycloak-server-url')
        return keyCloakServerURL?.value ?: keyCloakServerURL?.defaultValue
    }

    static getKeyCloakRealmID() {
        def keyCloakRealmID = SiteSettings.findBySlug('keycloak-realm-id')
        return keyCloakRealmID?.value ?: keyCloakRealmID?.defaultValue
    }

    static getKeyCloakClientID() {
        def keyCloakClientID = SiteSettings.findBySlug('keycloak-client-id')
        return keyCloakClientID?.value ?: keyCloakClientID?.defaultValue
    }

    static getKeyCloakClientSecret() {
        def keyCloakClietSecret = SiteSettings.findBySlug('keycloak-client-secret')
        return keyCloakClietSecret?.value ?: keyCloakClietSecret?.defaultValue
    }

    static getKeyCloakUsername() {
        def keyCloakUsername = SiteSettings.findBySlug('keycloak-username')
        return keyCloakUsername?.value ?: keyCloakUsername?.defaultValue
    }

    static getKeyCloakPassword() {
        def keyCloakPassword = SiteSettings.findBySlug('keycloak-password')
        return keyCloakPassword?.value ?: keyCloakPassword?.defaultValue
    }
}


