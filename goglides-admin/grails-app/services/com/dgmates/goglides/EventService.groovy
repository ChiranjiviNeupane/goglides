package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class EventService {

def amazonS3Service



    def getEventSlug(Long id){
        def event=Event.get(id)
        if(event){
            return event.slug
        }
    }
}
