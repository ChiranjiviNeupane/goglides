package com.dgmates.goglides

import com.amazonaws.AmazonWebServiceClient
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.regions.ServiceAbbreviations
import com.amazonaws.services.ec2.AmazonEC2AsyncClient
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.regions.Region
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.AmazonS3EncryptionClient
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient
import com.amazonaws.services.sns.AmazonSNSClient
import grails.transaction.Transactional
import grails.util.Holders

@Transactional
class AmazonWebService {

    static final String DEFAULT_REGION = 'us-east-1'

    AmazonEC2AsyncClient getEc2Async(regionName = '') {
        getServiceClient('ec2', regionName, true) as AmazonEC2AsyncClient
    }

    AmazonEC2Client getEc2(String regionName = '') {
        getServiceClient('ec2', regionName) as AmazonEC2Client
    }

    AmazonS3Client getS3(String regionName = '') {
        getServiceClient('s3', regionName) as AmazonS3Client
    }

    AmazonSNSClient getSNS(String regionName = '') {
        getServiceClient('sns', regionName) as AmazonSNSClient
    }

    AmazonSimpleEmailServiceClient getSES(String regionName = '') {
        getServiceClient('ses', regionName) as AmazonSimpleEmailServiceClient
    }

    AmazonS3EncryptionClient getS3Encryption(String regionName = '') {
        getServiceClient('s3Encryption', regionName) as AmazonS3EncryptionClient
    }

    private AmazonWebServiceClient getServiceClient(String service, String regionName = '', Boolean async = false) {

        if (!regionName) {
            regionName = DEFAULT_REGION
        }

        Region region = RegionUtils.getRegion(regionName)
        String serviceName = getServiceAbbreviation(service)

        if (!region || !region.isServiceSupported(serviceName)) {
            if (!region) {
                log.warn "Region ${regionName} not found"
            } else if (!region.isServiceSupported(serviceName)) {
                log.warn "Service ${service} is not supported in region ${regionName}"
            }
            log.warn "Using default region ${DEFAULT_REGION}"
            regionName = DEFAULT_REGION
            region = RegionUtils.getRegion(regionName)
        }

        AmazonWebServiceClient client

        if (region) {
            def credentials = buildCredentials()

            switch (service) {
                case 'ec2':
                    client = async ? new AmazonEC2AsyncClient(credentials) : new AmazonEC2Client(credentials)
                    break
                case 's3':
                    if (async) throw new Exception("Sorry, there is no async client for AmazonS3")
                    client = new AmazonS3Client(credentials)
                    break
                case 'sns':
                    client = new AmazonSNSClient(credentials)
                    break
                case 'ses':
                    client = new AmazonSimpleEmailServiceClient(credentials)
                    break
                default:
                    throw new Exception("Sorry, no client found for service ${service}")
            }

            client.setRegion(region)

        }

        return client
    }

    private buildCredentials() {
        def access = Holders.grailsApplication.config.aws
        BasicAWSCredentials credentials = new BasicAWSCredentials(access.aws_access_key_id, access.aws_secret_access_key)
        credentials
    }

    private String getServiceAbbreviation(String service) {
        switch (service) {
            case 'cloudSearchDomain':
                ServiceAbbreviations.CloudSearch
                break
            case 'cloudWatch':
                ServiceAbbreviations.CloudWatch
                break
            case 'cognitoIdentity':
                ServiceAbbreviations.CognitoIdentity
                break
            case 'cognitoSync':
                ServiceAbbreviations.CognitoSync
                break
            case 'ses':
                ServiceAbbreviations.Email
                break
            case 's3Encryption':
                ServiceAbbreviations.S3
                break
            default:
                service.toLowerCase()
        }
    }
}
