package com.dgmates.goglides

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.util.BinaryUtils
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import grails.transaction.Transactional
import grails.util.Holders
import org.grails.web.util.WebUtils

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException

@Transactional
class AmazonS3Service {

    def grailsApplication
    def amazonWebService
    def siteSettingsService

    def uploadImage(def bucketName, def imageFile, def s3Path) {

        def s3Client = amazonWebService.getS3()
        s3Client.putObject(
                new PutObjectRequest(bucketName, s3Path, (File) imageFile)
                        .withCannedAcl(CannedAccessControlList.PublicRead))
        return s3Client.getResourceUrl(bucketName, s3Path)

    }

    def deleteImage(def s3Url) {
        String[] tokens = s3Url.replace("https://", "").split("/", 2)
        String key = tokens[1]
        println tokens[0]
        String bucketName = tokens[0].split("\\.", 2)[0]
        def s3Client = amazonWebService.getS3()
        s3Client.deleteObject(new DeleteObjectRequest(bucketName, key))
    }


    private String base64EncodePolicy(JsonElement jsonElement) throws UnsupportedEncodingException {
        String policyJsonStr = jsonElement.toString()
        String base64Encoded = BinaryUtils.toBase64(policyJsonStr.getBytes("UTF-8"))
        return base64Encoded
    }

    private String sign(String toSign) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        def access = Holders.grailsApplication.config.aws
        Mac hmac = Mac.getInstance("HmacSHA1")
        hmac.init(new SecretKeySpec(access.aws_secret_access_key.getBytes("UTF-8"), "HmacSHA1"))
        String signature = BinaryUtils.toBase64(hmac.doFinal(toSign.getBytes("UTF-8")))
        return signature
    }


    def s3Upload(String key) {
        def access = Holders.grailsApplication.config.aws
        def urlS3 = "https://" + access.s3_bucket + ".s3.amazonaws.com" + "/" + key
        return urlS3
    }

    def s3Delete(String bucket, String key) {
        def access = Holders.grailsApplication.config.aws
        AWSCredentials myCredentials = new BasicAWSCredentials(access.aws_access_key_id, access.aws_secret_access_key);
        AmazonS3 s3Client = new AmazonS3Client(myCredentials);
        s3Client.deleteObject(bucket, key);
    }

    def s3Signing() {
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()
        request.getReader()
        JsonParser jsonParser = new JsonParser()
        JsonElement contentJson = jsonParser.parse(request.getReader())
        JsonObject jsonObject = contentJson.getAsJsonObject()
        String signature
        JsonElement headers = jsonObject.get("headers")
        JsonObject response = new JsonObject()
        try {
            if (headers == null) {
                String base64Policy = base64EncodePolicy(contentJson)
                signature = sign(base64Policy)
                response.addProperty("policy", base64Policy)
            } else {
                signature = sign(headers.getAsString())
            }
            response.addProperty("signature", signature)

        }
        catch (Exception e) {
        println(e.fillInStackTrace())
        }

        return response
    }


    def sourceServerUrl(){
        def sourceServerUrl=grailsApplication.config.amazonS3.sourceEndPoint
        return sourceServerUrl
    }


    def sourceBucket(){
        def sourceBucket=grailsApplication.config.amazonS3.sourceBucket
        return sourceBucket
    }


    def sourceBucketCDN(){
        def sourceCDN=grailsApplication.config.amazonS3.sourceCDN
        return sourceCDN
    }

    def accessKey(){
        def accessKey=grailsApplication.config.amazonS3.accessKey
        return accessKey
    }


    def destinationServerUrl(){
        def destinationServerUrl=grailsApplication.config.amazonS3.destinationEndPoint
        return destinationServerUrl

    }


    def destinationBucket(){
        def destinationBucket=grailsApplication.config.amazonS3.destinationBucket
        return destinationBucket
    }


    def destinationBucketCDN(){
        def destinationCDN=grailsApplication.config.amazonS3.destinationCDN
        return destinationCDN
    }


}
