package com.dgmates.goglides
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.PutObjectRequest
import grails.transaction.Transactional
import org.apache.commons.io.FilenameUtils
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

@Transactional
class ImageProcessingService {

    def grailsApplication
    def amazonS3Service
    def amazonWebService

    //returning image format like .jpeg,png,gif
    def checkImageFormat(String imageUrl) {
        def imageType = FilenameUtils.getExtension(imageUrl)
        return imageType
    }

    //return ImageName withour extension
    def getImageName(String url) {
        File file = new File(url)
        return file.getName();

    }

    //return whole image name and path without extension
    def getImagePathWithOutExtension(String url){
        String imageUrl
        if(url.lastIndexOf(".")>0 && url){
            imageUrl=url.substring(0,url.lastIndexOf("."));
            return imageUrl;
        }
    }

    //call imageProxy server for imageProcessing, ImageProxy server process image and receive image and that image store local directory /images/temp/
    def imageProcessingHTTPAPI(String url) {
        def imageFormat = checkImageFormat(url);
        def imageName = getImageName(url);
        RestTemplate restTemplate = new RestTemplate();
        String imageUploadPath = grailsApplication.config.imageUpload.path
        HttpHeaders headers = new HttpHeaders()
        headers.set(" Content-Type", "images/*")
        HttpEntity httpEntity = new HttpEntity(headers)
        ResponseEntity<byte[]> responseEntity = null
        println("image url "+url)
        responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, byte[].class)
        Byte[] bytes = responseEntity.getBody();
        BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytes));
        ImageIO.write(img, imageFormat, new File("${imageUploadPath}/${imageName}"));
        return imageUploadPath + "/" + imageName
    }


    //define resied image property like height,widht,crop, resize and return whole imageurl which url should be used for ImageProxy Server HTTP API
    def defineImageProperties(String imageUrl, String imageProcessingType, int width, int quality) {
        def imageProxyServerUrl = grailsApplication.config.amazonS3.imageProxyServerUrl
        def url = imageProxyServerUrl + "/" + imageProcessingType + "?width=" + width + "&quality=" + quality + "&url=" + imageUrl
        return url

    }


    //gives resized iamge url with naming convension...
    def getResizeImageUrl(String s3BucketImageUrl, String imageProcessingType, int width,int quality) {
        def imageName=getImagePathWithOutExtension(s3BucketImageUrl);
        def imageFormat=checkImageFormat(s3BucketImageUrl)
        def resizeUrl = imageName+"_"+imageProcessingType +"-w"+width+"-q"+quality+"."+imageFormat;
        return resizeUrl
    }



    def imageProcessing(String imagePath, String attribute, int width, int quality){
        //defining a height,width of the image..geting from database..
        def sourceImageUrl=amazonS3Service.sourceServerUrl()+imagePath   //Get Original Image from Source bucket
       // def serverStatus=checkImageProxyServerStatus()
            //definiing attribute like height, width, image should be crop,resize... and getting imageUrl.That imageUrl should be called when image processing
            def processingImageUrl = defineImageProperties(sourceImageUrl, attribute, width, quality)
            //calling a method.That method send imageUrl to proxy image server and get resized image in a byte format.That byte format change into image and store that image into /images/temp directory
            def localImageUrl = imageProcessingHTTPAPI(processingImageUrl)
            //sending imageurl without domain of amazon ,height and width for making naming convension of resize image...Take a Image url with name convension like ..ImageName*crop*height*width
            def destinationImageUrl = getResizeImageUrl(imagePath, attribute, width,quality)
            //finally upload resized image into destion bucket and follow imageName like sameImageName*resize*height*width
            def imageuploadStatus= uploadImageInS3Bucket(localImageUrl, destinationImageUrl)



    }

/*

    def deleteResizeImageFromS3DestinationBucket( String url){
        def destinationImageUrl=getResizeImageUrl(url, imageProcessingType,height,width)
        amazonS3Service.s3Delete(amazonS3Service.imageProcessingBucket(),destinationImageUrl)
        println("Sucessfully deleted..")
    }

    def checkResizeImageInDestinationBucket(String resizeImageUrl){
        def amazonS3 = amazonWebService.getS3("us-west-2")
        def bucketName=amazonS3Service.imageProcessingBucket()
        boolean imageExist=  amazonS3.doesObjectExist(bucketName,resizeImageUrl)
        if(imageExist){
            return true
        }else {
            return false
        }
    }
*/

    //upload resized/processed image to destination bucket...
    def uploadImageInS3Bucket(String localImagePathLocation, String imageUrl) {
        File file=null;

        try {
            def amazonS3 = amazonWebService.getS3("us-west-2")
            def bucketName=amazonS3Service.destinationBucket()
            file = new File(localImagePathLocation);
            amazonS3.putObject(new PutObjectRequest(bucketName, imageUrl, file).withCannedAcl(CannedAccessControlList.PublicRead));
            def uploadStatus=amazonS3.getResourceUrl(bucketName, imageUrl)
            file.delete()
        } catch (Exception e) {
            file.delete();
            println(e.printStackTrace())

        }

    }


    def deleteResizeImageFromS3DestinationBucket( String url){
        try{
            def amazonS3 = amazonWebService.getS3("us-west-2")
            def bucketName=amazonS3Service.destinationBucket()
            def imageExist=  amazonS3.doesObjectExist(bucketName,url)
            if(imageExist){
                amazonS3Service.s3Delete(amazonS3Service.destinationBucket(),url)
            }
        }catch (Exception e){
            println(e.printStackTrace())
        }

    }


    //check image proxy server status.....
    def checkImageProxyServerStatus(){
        def serverAddress=grailsApplication.config.imageProxyServer.imageServerHost
        def serverPort=grailsApplication.config.imageProxyServer.imageServerPort
        SocketAddress socketAddress=new InetSocketAddress(serverAddress,serverPort);
        Socket socket=new Socket();
        try{
            socket.connect(socketAddress,serverPort);
            socket.close()
            return true
        }catch(Exception e){
            return false
        }
    }


    def checkImageType(String url, String imageCategory){
        def resizedImageUrl
        def imageProcessing=ImageProcessing.findByImageCategory(imageCategory)
        if(imageProcessing && url!=null){
            resizedImageUrl=getResizeImageUrl(url,"thumbnail",imageProcessing.width,imageProcessing.quality)
            return resizedImageUrl
        }
        return resizedImageUrl
    }


    def getEventImageDestionServerUrl(Long eventId, String imageType){
        def event=Event.get(eventId)
        def eventMultimedia=EventMultimedia.findByEvent(event)
        if(eventMultimedia){
            def reSizedImageUrl=checkImageType(eventMultimedia.file, imageType)
            return reSizedImageUrl
        }
        return null

    }

    def getEventImageSourceServerUrl(Long id){
        def event=Event.get(id)
        def eventMultimedia=EventMultimedia.findByEvent(event)
        if(eventMultimedia){
            return eventMultimedia.file
        }
        return null
    }



    def listingFeaturedDestionUrl(String imageUrl,String imageType){
        def reSizedImageUrl=checkImageType(imageUrl, imageType)
        println("resized "+reSizedImageUrl)
        return reSizedImageUrl

    }

    def listingGallerydDestionUrl(String imageUrl,String imageType){
        def resizedGalleryImage=checkImageType(imageUrl, imageType)
        return resizedGalleryImage
    }




}
