package com.dgmates.goglides

import grails.transaction.Transactional
import com.dgmates.goglides.common.ListingStatus
import com.dgmates.goglides.common.PaymentStatus
import com.dgmates.goglides.common.BookingStatus
import java.text.DecimalFormat

@Transactional
class DashboardService {
    DirectoryService directoryService

    def returnData = []

    def getLifetimeSales(User userInstance) {
        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        if (businessDirectory) {
            returnData = ListingPayment.createCriteria().list({
                eq('paymentStatus', PaymentStatus.PAID.description())
                eq('status', BookingStatus.COMPLETED.description())
                projections {
                    sum('totalPrice')
                }
            })
        }
        return decimalFormat(returnData?.getAt(1) ?: 0)
    }

    def getMonthlySales(User userInstance) {
        def startDate = getFirstDayOfMonth()
        def endDate = getLastDayOfMonth()

        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        if (businessDirectory) {
            returnData = ListingPayment.createCriteria().list({

                between('createdAt', startDate, endDate)
                eq('paymentStatus', PaymentStatus.PAID.description())
                eq('status', BookingStatus.COMPLETED.description())
                projections {
                    sum('totalPrice')
                }
            })
        }
        return decimalFormat(returnData?.getAt(1) ?: 0)
    }

    def getTotalListings(User userInstance) {
        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        if (businessDirectory) {
            returnData = Listing.createCriteria().list {
                ne('status', ListingStatus.TRASH.description())
                projections {
                    count()
                }
            }
        }
        return (returnData?.getAt(1) ?: 0)
    }

    def getTotalBookings(User userInstance) {
        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        if (businessDirectory) {
            returnData = ListingPayment.createCriteria().list {
                ne('status', BookingStatus.TRASH.description())
                projections {
                    count()
                }
            }
        }
        return (returnData?.getAt(1) ?: 0)
    }

    def decimalFormat(def value) {
        DecimalFormat df = new DecimalFormat("###,###.##")
        df.setMinimumFractionDigits(2)
        return df.format(value)
    }

    def getFirstDayOfMonth() {
        Calendar cal = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_MONTH, 1)
        Date firstDayOfMonth = cal.getTime()
        return firstDayOfMonth
    }

    def getLastDayOfMonth() {
        Calendar cal = Calendar.getInstance()
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE))
        Date lastDayOfMonth = cal.getTime()
        return lastDayOfMonth
    }
}