package com.dgmates.goglides

import grails.transaction.Transactional
import org.apache.commons.lang.RandomStringUtils

@Transactional
class BookingService {
    /**
     * List all bookings
     * @param userInstance
     * @param params
     */
    def bookings(userInstance, params) {
        def booking = ListingPayment.createCriteria()
        def bookings = booking.list(max: params.max, offset: params.offset) {
            if (params?.state) {
                eq('status', params?.state)
            } else {
                ne('status', 'trash')
            }
            if (params.businessId) {
               def businessDirectory = BusinessDirectory.findByBusinessId(params?.businessId)
                eq('businessDirectory', businessDirectory)
            }
            order("id", "desc")
        }
    }

    def static generateReferenceNo() {
        def length = 6
        def unique = false
        def ref_no
        def listing_payment

        while (!unique) {
            String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
            ref_no = RandomStringUtils.random(length, charset.toCharArray())
            listing_payment = ListingPayment.findWhere(referenceNo: ref_no)
            if (!listing_payment)
                unique = true
        }

        return ref_no
    }

    def static generateTicketNo() {
        def ticket_no

        def booking_item = BookingItems.createCriteria().get {
            projections {
                max "ticketNo"
            }
        }

        if (!booking_item)
            ticket_no = '1000000001'
        else if (booking_item) {
            ticket_no = booking_item.toInteger() + 1
        }

        return ticket_no
    }

}