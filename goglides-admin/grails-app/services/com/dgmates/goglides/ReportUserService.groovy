package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class ReportUserService {
    DirectoryService directoryService
    CommonService commonService

    def returnData

    def getUserList(userInstance, params) {
        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        def dates = commonService.getStartEndDate(params?.type)
        def startDate = dates?.startDate
        def endDate = dates?.endDate
        def keyword = params?.keyword ?: ""

        if (businessDirectory) {
            returnData = User.createCriteria().list(max: params?.max, offset: params?.offset) {
                if (keyword) {
                    or {
                        ilike('username', '%' + keyword + '%')
                    }
                } else {

                    if (startDate && endDate) {
                        between('dateCreated', startDate, endDate)
                    }
                }
                order('id', 'asc')
            }
        }
        return returnData[0] ?: 0
    }
}