package com.dgmates.goglides

import grails.transaction.Transactional
import groovy.time.*

@Transactional
class DirectoryService {

    AmazonS3Service amazonS3Service

    def getAgentsByParams(params, status = '') {
        def businessDirectory = BusinessDirectory.createCriteria()
        return businessDirectory.list(max: params?.max, offset: params?.offset) {
            if (status) {
                eq('status', status)
            }else{
                ne('status', 'trash')
            }
            order('id', 'desc')
        }
    }

    def getFileLocation(file, type = '') {
        def url = ''
        if (file) {
            url = amazonS3Service.sourceBucketCDN() + file
        }
        return url
    }

    def saveBusinessDirectory(User userInstance, params) {
        def businessDirectory = new BusinessDirectory(params)
        businessDirectory.user = userInstance
        businessDirectory.approved = 'approved'
        businessDirectory.save(flush: true)
        return businessDirectory
    }

    def saveBusinessStaff(User userInstance, BusinessDirectory businessDirectory) {
        def businessStaff = new BusinessStaff()
        return new BusinessStaff(user: userInstance, businessDirectory: businessDirectory, accountLocked: false).save(flush: true)
    }

    def getBusinessDirectoryByid(params) {
        try {

            def businessDirectory = BusinessDirectory.findByBusinessId(params.businessId)
            return businessDirectory
        }
        catch (Exception e) {

            log.error(e)
        }
    }

    def getBusinessDirectory(userInstance) {
        try {
            def businessStaff = BusinessStaff.findByUser(userInstance)
            def businessDirectory = BusinessDirectory.get(businessStaff?.businessDirectoryId)
            return businessDirectory
        }
        catch (Exception e) {
            //log.error(e)
        }
    }

    def saveTransferLog(def oldBusiness, def newBusiness) {
        try {
            new BusinessDirectoryTransferLog(
                    businessDirectoryOld: oldBusiness,
                    businessDirectoryNew: newBusiness
            ).save(failOnError: true)
        }
        catch (Exception e) {
            //log.error(e)
        }
        return true
    }

    def checkTransferDuration(def businessDirectory) {
        def directory = BusinessDirectory.get(businessDirectory.toInteger())
        def businessLog = BusinessDirectoryTransferLog.findWhere(businessDirectoryOld: directory)
        def timeStart = businessLog?.transferredAt
        def timeStop = new Date()
        if (timeStart) {
            TimeDuration duration = TimeCategory.minus(timeStop, timeStart)
            return duration.hours
        } else {
            return 73
        }
    }

    def revertBusinessTransfer(def businessDirectory) {
        def directory = BusinessDirectory.get(businessDirectory.toInteger())
        def businessLog = BusinessDirectoryTransferLog.findWhere(businessDirectoryOld: directory)
        def directoryNew = BusinessDirectory.get(businessLog?.businessDirectoryNewId)
        try {
            directory.transferred = false
            directory.save(failOnError: true)
            revertBusinessListing(businessLog)
            businessLog.isReversable = 'false'
            businessLog.save(failOnError: true)
            directoryNew.transferred = true
            directoryNew.save(flush: true)
        }
        catch (Exception e) {
            // log.error(e)
        }
        return true
    }

    def revertBusinessListing(def businessLog) {
        def oldListings = Listing.findAllWhere(businessDirectoryId: businessLog?.businessDirectoryOldId.toString())
        if (oldListings) {
            oldListings.each { listing ->
                listing.transferred = false
                listing.status = 'draft'
                listing.save(failOnError: true)
            }
        }
        def transferredListings = Listing.findAllWhere(businessDirectoryId: businessLog?.businessDirectoryNewId.toString())
        if (transferredListings) {
            transferredListings.each { listing ->
                listing.transferred = true
                listing.status = 'reverted'
                listing.save(failOnError: true)
            }
        }
        return true
    }

}