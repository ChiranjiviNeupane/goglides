package com.dgmates.goglides

import grails.transaction.Transactional
import groovyx.net.http.*

@Transactional
class PriceService {

    def data = [:]

    /**
     * Check the currency code and return the currency symbol.
     * @param currencyCode
     * @return string
     */
    def getCurrencySymbol(def currencyCode) {
        def symbol
        switch (currencyCode) {
            case 'GBP':
                symbol = '<i class="fa fa-gbp"></i>'
                break
            case 'USD':
                symbol = '<i class="fa fa-usd"></i>'
                break
            case 'EUR':
                symbol = '<i class="fa fa-euro"></i>'
                break
            case 'RMB':
                symbol = '<i class="fa fa-rmb"></i>'
                break
            case 'YEN':
                symbol = '<i class="fa fa-yen"></i>'
                break
            case 'RUB':
                symbol = '<i class="fa fa-ruble"></i>'
                break
            case 'WON':
                symbol = '<i class="fa fa-won"></i>'
                break
            case 'INR':
                symbol = '<i class="fa fa-rupee"></i>'
                break
            default:
                symbol = '<i class="fa fa-usd"></i>'
                break
        }
        return symbol
    }

    /**
     * Active price range of listing.
     * @param id
     * @return string price range
     */
    def getPriceRange(def id, def type = null) {
        def listing
        def listingPackage
        if (type) {
            listingPackage = ListingPackage.get(id)
        } else {
            listing = Listing.get(id)
        }
        def minPrice = ListingPrice.withCriteria {
            if (type) {
                eq('listingPackage', listingPackage)
            } else {
                eq('listing', listing)
            }
            eq('status', 'publish')
            projections {
                min 'retailRate'
            }
        }

        def maxPrice = ListingPrice.withCriteria {
            if (type) {
                eq('listingPackage', listingPackage)
            } else {
                eq('listing', listing)
            }
            eq('status', 'publish')
            projections {
                max 'retailRate'
            }
        }
        if (minPrice[0] == null || maxPrice[0] == null) {
            return "N.A."
        } else if (minPrice != maxPrice) {
            return getCurrencySymbol() + ' ' + minPrice[0].toInteger() + ' - ' + getCurrencySymbol() + ' ' + maxPrice[0].toInteger()
        } else if (minPrice == maxPrice) {
            return getCurrencySymbol() + ' ' + maxPrice[0].toInteger()
        } else {
            return "N.A."
        }
    }

    def getFormattedPrice(rate) {
        return getCurrencySymbol() + rate
    }


    def getPackagePrice(Listing listing, ListingPackage listingPackage, def params) {
        data['rate'] = true

        def seniorRate
        def adultRate
        def childRate
        def infantRate

        def seniorMultimedia
        def adultMultimedia
        def childMultimedia
        def infantMultimedia

        def seniorTransport
        def adultTransport
        def childTransport
        def infantTransport

        def subTotal = 0
        def totalMultimedia = 0
        def totalTransport = 0
        def total = 0
        def seniorTotal = 0
        def adultTotal = 0
        def childTotal = 0
        def infantTotal = 0

        adultRate = getPackageRateByAgeGroup(listingPackage, params, 'adult')
        seniorRate = getPackageRateByAgeGroup(listingPackage, params, 'senior')
        childRate = getPackageRateByAgeGroup(listingPackage, params, 'child')
        infantRate = getPackageRateByAgeGroup(listingPackage, params, 'infant')

        if (!adultRate) {
            data['rate'] = false
        }

        if (!seniorRate && data['rate'] == true) {
            seniorRate = adultRate
        }
        if (!childRate && data['rate'] == true) {
            childRate = adultRate
        }
        if (!infantRate && data['rate'] == true) {
            infantRate = adultRate
        }

        if (data['rate'] == true) {
            seniorTotal = (params?.senior.toInteger() * seniorRate?.retailRate[0])
            adultTotal = (params?.adult.toInteger() * adultRate?.retailRate[0])
            childTotal = (params?.child.toInteger() * childRate?.retailRate[0])
            infantTotal = (params?.infant.toInteger() * infantRate?.retailRate[0])

            subTotal = seniorTotal + adultTotal + childTotal + infantTotal
        }

        def listingAddon = ListingAddons.findByListingAndListingPackage(listing, listingPackage)

        if (listingAddon.includedMultimedia == 'no') {
            seniorMultimedia = getMultimediaRateByAgeGroup(listingPackage, 'senior')
            adultMultimedia = getMultimediaRateByAgeGroup(listingPackage, 'adult')
            childMultimedia = getMultimediaRateByAgeGroup(listingPackage, 'child')
            infantMultimedia = getMultimediaRateByAgeGroup(listingPackage, 'infant')

            if (!adultMultimedia) {
                adultMultimedia = 0
            }
            if (!seniorMultimedia) {
                seniorMultimedia = adultMultimedia
            }
            if (!childMultimedia) {
                childMultimedia = adultMultimedia
            }
            if (!infantMultimedia) {
                infantMultimedia = adultMultimedia
            }
            if (data['rate'] == true) {
                totalMultimedia = (params?.totalMultimedia.toInteger() * adultMultimedia.toInteger())
            }
        }

        if (listingAddon.includedTransport == 'no') {
            seniorTransport = getTransportRateByAgeGroup(listingPackage, 'senior')
            adultTransport = getTransportRateByAgeGroup(listingPackage, 'adult')
            childTransport = getTransportRateByAgeGroup(listingPackage, 'child')
            infantTransport = getTransportRateByAgeGroup(listingPackage, 'infant')

            if (!adultTransport) {
                adultTransport = 0
            }
            if (!seniorTransport) {
                seniorTransport = adultTransport
            }
            if (!childTransport) {
                childTransport = adultTransport
            }
            if (!infantTransport) {
                infantTransport = infantTransport
            }

            if (data['rate'] == true) {
                totalTransport = (params?.totalTransport.toInteger() * adultTransport.toInteger())
            }
        }

        if (data['rate'] == true)
            total = subTotal + totalMultimedia + totalTransport

        if (data['rate']) {
            seniorRate = seniorRate?.retailRate[0]
            adultRate = adultRate?.retailRate[0]
            childRate = childRate?.retailRate[0]
            infantRate = infantRate?.retailRate[0]
        }
        data['seniorRate'] = seniorRate
        data['adultRate'] = adultRate
        data['childRate'] = childRate
        data['infantRate'] = infantRate
        data['seniorTotal'] = seniorTotal
        data['adultTotal'] = adultTotal
        data['childTotal'] = childTotal
        data['infantTotal'] = infantTotal
        data['seniorMultimedia'] = seniorMultimedia ?: 0
        data['adultMultimedia'] = adultMultimedia ?: 0
        data['childMultimedia'] = childMultimedia ?: 0
        data['infantMultimedia'] = infantMultimedia ?: 0
        data['totalMultimedia'] = totalMultimedia
        data['totalMultimediaFormatted'] = getFormattedPrice(totalMultimedia)
        data['seniorTransport'] = seniorTransport ?: 0
        data['adultTransport'] = adultTransport ?: 0
        data['childTransport'] = childTransport ?: 0
        data['infantTransport'] = infantTransport ?: 0
        data['totalTransport'] = totalTransport
        data['totalTransportFormatted'] = getFormattedPrice(totalTransport)
        data['subTotal'] = subTotal
        data['subTotalFormatted'] = getFormattedPrice(subTotal.toInteger())
        data['total'] = total
        data['totalFormatted'] = getFormattedPrice(total.toInteger())

        return data
    }

    def getPackageRateByAgeGroup(ListingPackage listingPackage, def params, def ageGroup) {
        def rate = ListingPrice.createCriteria()
        return rate.list() {
            eq('listingPackage', listingPackage)
            eq('ageGroup', ageGroup)
            eq('status', 'publish')
            if (params?.type) {
                eq('type', params?.type)
            }
            if (params?.duration) {
                eq('duration', params?.duration)
            }
            if (params?.schedule && listingPackage?.rateType != 'no') {
                eq('schedule', params?.schedule)
            }
        }
    }

    def getMultimediaRateByAgeGroup(ListingPackage listingPackage, def ageGroup) {
        return ListingAddonsPrice.findByListingPackageAndAddonTypeAndAgeGroup(listingPackage, 'multimedia', ageGroup)?.retailRate
    }

    def getTransportRateByAgeGroup(ListingPackage listingPackage, def ageGroup) {
        return ListingAddonsPrice.findByListingPackageAndAddonTypeAndAgeGroup(listingPackage, 'transport', ageGroup)?.retailRate
    }

}