package com.dgmates.goglides

import grails.transaction.Transactional
import com.dgmates.goglides.common.PaymentStatus
import com.dgmates.goglides.common.BookingStatus

@Transactional
class ReportBookingService {
    def directoryService
    def commonService

    def returnData

    def getFlightList(User userInstance, def params) {
        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        def dates = commonService.getStartEndDate(params?.type)
        def startDate = dates?.startDate.format('yyyy-MM-dd')
        def endDate = dates?.endDate.format('yyyy-MM-dd')
        def keyword = params?.bookingKeyword ?: ""

        if (businessDirectory) {
            returnData = ListingPayment.createCriteria().list(max: params?.max, offset: params?.offset) {
                if (keyword) {
                    or {
                        ilike('referenceNo', '%' + keyword + '%')
                    }
                } else {
                    eq('paymentStatus', PaymentStatus.PAID.description())
                    eq('status', BookingStatus.CONFIRMED.description())

                    if (startDate && endDate) {
                        between('bookingDate', startDate, endDate)
                    }
                }
                order('bookingTime', 'asc')
            }
        }
        return returnData ? returnData[0] : 0
    }

    def getBookingList(User userInstance, def params) {
        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        def dates = commonService.getStartEndDate(params?.type)
        def startDate = dates?.startDate
        def endDate = dates?.endDate
        def keyword = params?.bookingKeyword ?: ""

        if (businessDirectory) {
            returnData = ListingPayment.createCriteria().list(max: params?.max, offset: params?.offset) {
                if (keyword) {
                    or {
                        ilike('referenceNo', '%' + keyword + '%')
                    }
                } else {
                    ne('status', BookingStatus.TRASH.description())
                    ne('status', BookingStatus.COMPLETED.description())

                    if (startDate && endDate) {
                        between('createdAt', startDate, endDate)
                    }
                }
                order('bookingTime', 'asc')
            }
        }
        return returnData ? returnData[0] : 0
    }
}