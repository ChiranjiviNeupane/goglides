package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class SiteDropdownService {

    def getUnitListOption() {
        return ['Day', 'Hour', 'Minute']
    }

    def getFilterOptions() {
        return ['upcoming', 'today', 'yesterday', 'last 7 days', 'last 30 days']
    }
}