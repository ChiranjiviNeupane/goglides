package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class ReportBusinessDirectoryService {
    def springSecurityService
    def directoryService
    def commonService

    def returnData

    def getDirectoryList(userInstance, params) {
        def businessDirectory = directoryService.getBusinessDirectory(userInstance)
        def dates = commonService.getStartEndDate(params?.type)
        def startDate = dates?.startDate
        def endDate = dates?.endDate
        def keyword = params?.keyword ?: ""

        if (businessDirectory) {
            returnData = BusinessDirectory.createCriteria().list(max: params?.max, offset: params?.offset) {
                if (keyword) {
                    or {
                        ilike('businessName', '%' + keyword + '%')
                    }
                } else {
                    if (startDate && endDate) {
                        between('createdAt', startDate, endDate)
                    }
                }
                order('id', 'desc')
            }
        }
        return returnData?.getAt(1) ?: 0
    }
}