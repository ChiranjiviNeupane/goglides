package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class WishListService {

    def springSecurityService

    /**
     * Checks if current logged in user has added the listing to wish-list
     * @params id [Integer] - listing id
     * @return boolean - true -> if added to wishlist, else false
     */
    def checkListing(int id) {
        def currentUser = ''
        def listing = Listing.get(id)

        def wishList = WishList.findWhere(user: currentUser, listing: listing)

        if (wishList)
            return true
        else
            return false
    }
}