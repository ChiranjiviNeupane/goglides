package com.dgmates.goglides

import grails.transaction.Transactional

@Transactional
class ListingPackageService {

    def listingPackageByCode(def packageCode) {
        return ListingPackage.findByPackageCode(packageCode)
    }

}