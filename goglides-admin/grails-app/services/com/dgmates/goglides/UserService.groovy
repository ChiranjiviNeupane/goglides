package com.dgmates.goglides

import org.jboss.resteasy.plugins.server.servlet.ServletSecurityContext
import org.keycloak.KeycloakPrincipal
import org.keycloak.KeycloakSecurityContext
import org.keycloak.OAuth2Constants
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.admin.client.resource.UserResource
import org.keycloak.admin.client.resource.UsersResource
import org.keycloak.representations.AccessToken
import org.keycloak.representations.idm.ClientRepresentation
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext
import java.security.Principal


class UserService {
    private serverUrl = SiteSettingsService.getKeyCloakServerURL()
    private realmId = SiteSettingsService.getKeyCloakRealmID()
    private clientId = SiteSettingsService.getKeyCloakClientID()
    private clientSecret = SiteSettingsService.getKeyCloakClientSecret()
    private username = SiteSettingsService.getKeyCloakUsername()
    private password = SiteSettingsService.getKeyCloakPassword()

    KeycloakClient keycloakClient = new KeycloakClient(serverUrl, realmId, clientId, clientSecret)

    Keycloak keycloak = keycloakClient.getKeycloakClientWithPasswordCredentials(username, password)

    /**
     * Fetch keycloak user id from logged in user session.
     * @return string keycloak user_id
     */

    def userReference() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()
        try {
            if (request.getUserPrincipal())
                return request.getUserPrincipal().getName()
            else {

            }
        }
        catch (Exception e) {
            log.error e.getMessage()
            throw e
        }
    }

    /**
     * getting role from keycloak

     */

    def getUserRoleFromKeyCloak() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()
        try {
            if (request.getUserPrincipal())
                return request.getUserPrincipal()
            else {

            }
        }
        catch (Exception e) {
            log.error e.getMessage()
            throw e
        }
    }


    /**
     * Fetch user id from user table with reference to keycloak user id.
     * @return object user
     */
    def currentUser() {
        def referenceId = userReference()
        def currentUser = User.findWhere(referenceId: referenceId)
        return currentUser
    }

    def getRole(){
        def userRole
        def referenceId = getUserRoleFromKeyCloak()
        ArrayList<String> rolesRef = getUserRoleFromKeyCloak().getProperties().get("authorities")
        for (int i = 0; i < rolesRef.size(); i++) {
          String  role=rolesRef.get(i)['role']
            if(role.substring(0,4)=='ROLE'){
                userRole=role
            }
        }
        return  userRole
    }

    def logoutUrl() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()
        request.logout()
        // return this.serverUrl + "/realms/" + this.realmId;
        // return keycloakClient.logout();
    }

    /**
     * Insert user data to keycloak realm and synchronize with grails user table.
     * @params string username , email, firstname, lastname
     * @return object user
     */
    def create(def userParams, def role = 'ROLE_USER') {
        UserRepresentation user = new UserRepresentation()
        user.setEnabled(true)
        user.setUsername(userParams?.username)
        user.setFirstName(userParams?.firstname)
        user.setLastName(userParams?.lastname)
        user.setEmail(userParams?.email)
        List<String> requiredActionList = new ArrayList<>()
        requiredActionList.add("UPDATE_PASSWORD")
        requiredActionList.add("VERIFY_EMAIL")

        user.setRequiredActions(requiredActionList)
        Map<String, List<String>> data = new HashMap<String, List<String>>()

        data.put("mobile", Arrays.asList(userParams?.mobile ?: ''))
        data.put("phone", Arrays.asList(userParams?.phone ?: ''))
        data.put("gender", Arrays.asList(userParams?.gender ?: ''))
        data.put("profile", Arrays.asList(userParams?.profile ?: ''))
        data.put("address", Arrays.asList(userParams?.address ?: ''))
        data.put("city", Arrays.asList(userParams?.city ?: ''))
        data.put("state", Arrays.asList(userParams?.state ?: ''))
        data.put("zip_code", Arrays.asList(userParams?.zipCode ?: ''))
        data.put("country", Arrays.asList(userParams?.country ?: ''))
        user.setAttributes(data)

        RealmResource realmResource = keycloak.realm(realmId)
        UsersResource userResource = realmResource.users()
        Response response = userResource.create(user)

        String uri = response.getLocation()
        String[] uidurl = uri.split('/')
        String userId = uidurl[uidurl.length - 1]
        def userInstance = new User()
        userInstance.referenceId = userId
        userInstance.save flush: true

        // Get realm role "tester" (requires view-realm role)
        RoleRepresentation testerRealmRole = realmResource.roles()
                .get(role).toRepresentation()

        userResource.get(userId).roles().realmLevel()
                .add(Arrays.asList(testerRealmRole))

        // Get client
        ClientRepresentation app1Client = realmResource.clients()
                .findByClientId(clientId).get(0)
        // Get client level role (requires view-clients role)
        RoleRepresentation userClientRole = realmResource.clients().get(app1Client.getId())
                .roles().get(role).toRepresentation()

        // Assign client level role to user
        userResource.get(userId).roles()
                .clientLevel(app1Client.getId()).add(Arrays.asList(userClientRole))
        return userInstance
    }

    /**
     * Edit user data in keycloak realm.
     * @return object user
     */
    def update(User userRef,def userParams) {
        // TODO: update user in keycloak realm
        RealmResource realmResource = keycloak.realm(realmId)
        UserResource userResource = realmResource.users().get(userRef.referenceId)
        UserRepresentation userRepresentation = userResource.toRepresentation();
        def received = user(userRef)
        userRepresentation.setFirstName(userParams?.firstname ? userParams?.firstname : received?.firstname);
        userRepresentation.setLastName(userParams?.lastname ? userParams?.lastname : received?.lastname);
        Map<String, List<String>> data = new HashMap<String, List<String>>()
        userParams?.mobile ? data.put("mobile", Arrays.asList(userParams?.mobile == null ? "" : userParams?.mobile)) : data.put("mobile", Arrays.asList(received?.mobile == null ? "" : received?.mobile))
        userParams?.phone ? data.put("phone", Arrays.asList(userParams?.phone == null ? "" : userParams?.phone)) : data.put("phone", Arrays.asList(received?.phone == null ? "" : received?.phone))
        userParams?.gender ? data.put("gender", Arrays.asList(userParams?.gender == null ? "" : userParams?.gender)) : data.put("gender", Arrays.asList(received?.gender == null ? "" : received?.gender))
        userParams?.profile ? data.put("profile", Arrays.asList(userParams?.profile == null ? "" : userParams?.profile)) : data.put("profile", Arrays.asList(received?.profile == null ? "" : received?.profile))
        userParams?.address ? data.put("address", Arrays.asList(userParams?.address == null ? "" : userParams?.address)) : data.put("address", Arrays.asList(received?.address == null ? "" : received?.address))
        userParams?.state ? data.put("state", Arrays.asList(userParams?.state == null ? "" : userParams?.state)) : data.put("state", Arrays.asList(received?.state == null ? "" : received?.state))
        userParams?.zip_code ? data.put("zip_code", Arrays.asList(userParams?.zip_code == null ? "" : userParams?.zip_code)) : data.put("zip_code", Arrays.asList(received?.zip_code == null ? "" : received?.zip_code))
        userParams?.country ? data.put("country", Arrays.asList(userParams?.country == null ? "" : userParams?.country)) : data.put("country", Arrays.asList(received?.country == null ? "" : received?.country))
        userRepresentation.setAttributes(data)

        Response response = userResource.update(userRepresentation)
        def userData = user(userRef)
        return userData
    }

    /**
     * Get user data from keycloak realm.
     * @params int id
     * @return object user
     */
    def user(User user) {
        if (!user) {
            return ['firstname': '', 'lastname': '']
        }
        def referenceId = user?.referenceId
        RealmResource realmResource = keycloak.realm(realmId)
        UserResource userResource = realmResource.users().get(referenceId)
        Map<String, List<String>> data = new HashMap<String, List<String>>()
        data = userResource.toRepresentation().getAttributes()
        if (data == null) {
            data = [
                    'mobile'  : null,
                    'phone'   : null,
                    'gender'  : null,
                    'profile' : null,
                    'address' : null,
                    'state'   : null,
                    'zip_code': null,
                    'country' : null,
                    'city'    : null
            ]
        }
        def info = [
                'referenceId': referenceId,
                'username'   : userResource.toRepresentation().username,
                'firstname'  : userResource.toRepresentation().firstName,
                'lastname'   : userResource.toRepresentation().lastName,
                'email'      : userResource.toRepresentation().email,
                'mobile'     : data.get("mobile") == null ? null : data.get("mobile").get(0),
                'phone'      : data.get("phone") == null ? null : data.get("phone").get(0),
                'gender'     : data.get("gender") == null ? null : data.get("gender").get(0),
                'profile'    : data.get("profile") == null ? null : data.get("profile").get(0),
                'address'    : data.get("address") == null ? null : data.get("address").get(0),
                'state'      : data.get("state") == null ? null : data.get("state").get(0),
                'zipCode'    : data.get("zip_code") == null ? null : data.get("zip_code").get(0),
                'country'    : data.get("country") == null ? null : data.get("country").get(0),
        ]
        return info
    }


    def changePassword(User user, String password) {
        RealmResource realmResource = keycloak.realm(realmId);
        UsersResource userRessource = realmResource.users();
        // Define password credential
        CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(false);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(password);

        // Set password credential
        userRessource.get(user.referenceId).resetPassword(passwordCred)
        return true
    }

    // def userRole() {
    //     String userId = userReference()
    //     println("user ID : " + userId)
    //     RealmResource realmResource = keycloak.realm(realmId)
    //     UserResource userResource = realmResource.users().get(userReference())
    //     // UserRepresentation userRepresentation = userResource.toRepresentation()
    //     println(userResource.get(userId))
    //     return userResource.get(userId).roles().realmLevel()
    // }

    /**
     * Get user profile picture.
     * @params string id
     * @return string profile
     */
    def profilePicture(def id) {
        def image
        return '/manage/assets/dashboard-2017/images/user.png'
    }

    /**
     * Migrate users to keycloak.
     * @return
     */
    def migrate() {
        def user = User.list()
        user.each { u ->
            String role = getRole(u)
            migrateUser(u, role)
        }
    }

    /**
     * Find role of user.
     * @param user
     * @return string user role
     */
    def String getRole(User user) {
        def role = UserRole.findByUser(user)
        if (!role) {
            return "ROLE_USER"
        }
        def authority = Role.get(role.roleId)
        if (authority) {
            role = (authority.authority == "ROLE_AGENT") ? 'ROLE_SUPPLIER' : authority.authority
        }
        return (!role) ? 'ROLE_USER' : role
    }

    /**
     * Save user to keycloak.
     * @param dbUser
     * @param role
     * @return
     */
    def migrateUser(dbUser, role) {
        if (dbUser.mobile == null) {
            dbUser.mobile = ""
        }
        if (dbUser.phone == null) {
            dbUser.phone = ""
        }
        if (dbUser.gender == null) {
            dbUser.gender = ""
        }
        if (dbUser.profilePicture == null) {
            dbUser.profilePicture = ""
        }
        if (dbUser.address == null) {
            dbUser.address = ''
        }
        if (dbUser.city == null) {
            dbUser.city = ''
        }
        if (dbUser.state == null) {
            dbUser.state = ''
        }
        if (dbUser.zipCode == null) {
            dbUser.zipCode = ''
        }
        if (dbUser.country == null) {
            dbUser.country = ''
        }
        if (role == null) {
            role = "ROLE_USER"
        }

        // Define user
        UserRepresentation user = new UserRepresentation()
        user.setEnabled(true)
        user.setUsername(dbUser.username)
        user.setFirstName(dbUser.firstname)
        user.setLastName(dbUser.lastname)
        user.setEmail(dbUser.email)
        List<String> requiredActionList = new ArrayList<>()
        requiredActionList.add("UPDATE_PASSWORD")
        requiredActionList.add("VERIFY_EMAIL")

        user.setRequiredActions(requiredActionList)
        Map<String, List<String>> data = new HashMap<String, List<String>>()
        data.put("mobile", Arrays.asList(dbUser.mobile))
        data.put("phone", Arrays.asList(dbUser.phone))
        data.put("gender", Arrays.asList(dbUser.gender))
        data.put("profile", Arrays.asList(dbUser.profilePicture))
        data.put("address", Arrays.asList(dbUser.address))
        data.put("city", Arrays.asList(dbUser.city))
        data.put("state", Arrays.asList(dbUser.state))
        data.put("zipCode", Arrays.asList(dbUser.zipCode))
        data.put("country", Arrays.asList(dbUser.country))
        user.setAttributes(data)
        RealmResource realmResource = keycloak.realm(realm)
        UsersResource userResource = realmResource.users()
        Response response = userResource.create(user)

        int status = response.getStatus()
        if (status == 201) {
            println("success")
        } else if (status == 409) {
            println("failure")
            return
        }

        String uri = response.getLocation()
        String[] uidurl = uri.split('/')
        String userId = uidurl[uidurl.length - 1]

        // Update user reference id
        def updateUser = User.get(dbUser.id)
        updateUser.referenceId = userId
        updateUser.save(flush: true)

        // Get realm role "tester" (requires view-realm role)
        RoleRepresentation testerRealmRole = realmResource.roles()
                .get(role).toRepresentation()

        userResource.get(userId).roles().realmLevel()
                .add(Arrays.asList(testerRealmRole))

        // Get client
        ClientRepresentation app1Client = realmResource.clients()
                .findByClientId(clientId).get(0)
        // Get client level role (requires view-clients role)
        RoleRepresentation userClientRole = realmResource.clients().get(app1Client.getId())
                .roles().get(role).toRepresentation()

        // Assign client level role to user
        userResource.get(userId).roles()
                .clientLevel(app1Client.getId()).add(Arrays.asList(userClientRole))

        // Set password credential
        List<String> requiredResetActionList = new ArrayList<>()
        requiredResetActionList.add("UPDATE_PASSWORD")
        requiredResetActionList.add("VERIFY_EMAIL")

        return ("  " + user.username + "     completed <br>")
    }

    def mapUser(User userRef) {
        def userData = user(userRef)
        userData.id = userRef.id
        return userData
    }

}

class KeycloakClient {
    private final String serverUrl
    private final String realmId
    private final String clientId
    private final String clientSecret

    KeycloakClient(String serverUrl, String realmId, String clientId, String clientSecret) {
        this.serverUrl = serverUrl
        this.realmId = realmId
        this.clientId = clientId
        this.clientSecret = clientSecret
    }

    Keycloak getKeycloakClientWithClientCredentials() {
        return newKeycloakBuilderWithClientCredentials().build()
    }

    Keycloak getKeycloakClientWithPasswordCredentials(String username, String password) {
        return newKeycloakBuilderWithPasswordCredentials(username, password).build()
    }

    private KeycloakBuilder newKeycloakBuilderWithClientCredentials() {
        return KeycloakBuilder.builder()
                .realm(realmId)
                .serverUrl(serverUrl)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
    }

    private KeycloakBuilder newKeycloakBuilderWithPasswordCredentials(String username, String password) {
        return newKeycloakBuilderWithClientCredentials()
                .username(username)
                .password(password)
                .grantType(OAuth2Constants.PASSWORD)
    }
}