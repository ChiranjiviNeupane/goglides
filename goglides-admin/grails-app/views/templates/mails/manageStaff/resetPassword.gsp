%{--Use ListingService--}%
<g:set var="listService" bean="listingService"/>
<div style="background-color: rgb(253, 253, 253); margin: 0; padding: 70px 0 70px 0; width: 100%">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center">
                <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color: #ffffff;">
                    <tr>
                        <td align="center" style="padding: 10px; border-bottom: 1px solid #e7eeee;">
                            <img src="${listService.getServerUrl()}${assetPath(src: '/v_0.2/images/logo-48x48.png')}" alt="" style="" />
                            <p style="margin-top: 5px; margin-bottom: 0; font-size: 16px; line-height: 28px; color: rgba(32, 80, 129, 0.97); font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                Thrill with Gliding Experience
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 10px;">
                            <p style="margin-top: 5px; font-size: 12px; line-height: 24px; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                <a target="_blank" href="http://facebook.com/goglides"><img src="${listService.getServerUrl()}${assetPath(src: '/v_0.2/images/facebook.png')}" alt="facebook" /></a>
                                <a target="_blank" href="https://twitter.com/googlides"><img src="${listService.getServerUrl()}${assetPath(src: '/v_0.2/images/twitter.png')}" alt="twitter" /></a>
                                <a target="_blank" href="https://plus.google.com/b/104573181260446052319/104573181260446052319/about?gmbpt=true&hl=en"><img src="${listService.getServerUrl()}${assetPath(src: '/v_0.2/images/googleplus.png')}" alt="googleplus" /></a>
                                <a target="_blank" href="https://www.youtube.com/channel/UCgl6AILUwi4QQzgeaCnw6qw?guided_help_flow=3"><img src="${listService.getServerUrl()}${assetPath(src: '/v_0.2/images/youtube.png')}" alt="youtube" /></a>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;">
                            <p style="margin-bottom: 15px; font-size: 16px; line-height: 28px; color: #040a48; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                Dear ${user?.firstname} ${user?.lastname},
                                <br>
                            </p>
                            <p style="margin-bottom: 15px; font-size: 16px; line-height: 28px; color: #040a48; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;"></p>
                            <p style="margin-bottom: 15px; font-size: 16px; line-height: 28px; color: #040a48; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                Welcome to Goglides.com
                            </p>
                            <p style="margin-bottom: 15px; font-size: 16px; line-height: 28px; color: #040a48; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                Goglides is the best home for adventure lovers. We're exicited you're joining us, but before you get going, please prove you are a real and beautiful human being by resetting your password.
                            </p>
                            <p style="margin-bottom: 15px; font-size: 16px; line-height: 28px; color: #040a48; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                To complete your password setup click <a href="${url}" style="color: rgb(0, 131, 193);">here</a>.
                            </p>
                            <p style="margin-bottom: 15px; font-size: 16px; line-height: 28px; color: #040a48; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                We look forward to working with you!
                            </p>
                            <p style="margin-bottom: 15px; font-size: 16px; line-height: 28px; color: #040a48; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                Kind regards,<br>
                                The Goglides<br> Team
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 10px; border-bottom: 1px solid #e7eeee; background-color: rgba(32, 80, 129, 0.97);">
                            <p style="font-size: 12px; line-height: 24px; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                Copyright &copy; 2017 Goglides.com. All rights reserved.<br>
                                This email was sent by Goglides.com<br>
                                JP Road, Thamel, Kathmandu, Nepal
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>