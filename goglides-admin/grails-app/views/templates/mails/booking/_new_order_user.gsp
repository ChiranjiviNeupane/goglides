%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email Template</title>

    <style type="text/css">
    @media only screen and (max-width: 480px) {

        table {
            width: 100%;
        }

        .main {
            padding: 3px !important;
        }

        .logo {
            width: 120px !important;
        }

        .social img {
            width: 20px;
            height: 20px;
        }

        .title {
            font-size: 28px !important;
        }

        .icons img {
            height: 32px;
            width: 32px;
        }

        .pilot img {
            height: 80px;
            width: 80px;
        }

        .nopadding td {
            padding: 0;
        }
    }

    </style>
</head>

<body style="margin: 0; font-family: arial; background:#e6e6e6; ">

<table bgcolor="#e6e6e6" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" class="main"
       style="padding: 15px; font-size: 12px;">

    <tr>
        <td align="center" valign="top">
            <table style="margin: 2px 0; color: #777;" bgcolor="#fff" border="0" cellpadding="10" cellspacing="0"
                   width="700">
                <tbody>
                <tr>
                    <td valign="middle" style="padding-top: 25px;">
                        <a href="index.html"><img src="http://mockup.goglides.com/html/template/logo.png" alt=""
                                                  class="logo" style="width:160px;"></a>
                    </td>
                    <td align="right" class="social">
                        <a target="_blank" href="http:facebook.com/goglides"><img
                                src="http://mockup.goglides.com/html/template/facebook.png"></a>
                        <a target="_blank"
                           href="https://plus.google.com/b/104573181260446052319/104573181260446052319/about?gmbpt=true&amp;hl=en">
                            <img src="http://mockup.goglides.com/html/template/google.png"></a>
                        <a target="_blank" href="https://twitter.com/googlides"><img
                                src="http://mockup.goglides.com/html/template/twitter.png"></a>
                        <a target="_blank"
                           href="https://www.youtube.com/channel/UCgl6AILUwi4QQzgeaCnw6qw?guided_help_flow=3">
                            <img src="http://mockup.goglides.com/html/template/ytb.png"></a>
                        <a target="_blank" href="https://goglides.tumblr.com/">
                            <img src="http://mockup.goglides.com/html/template/tumblr.png"></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <h1 style="margin: 15px 0; color: #66a5ad; font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;">Thank you for your booking</h1>

            <p style="margin-bottom: 15px; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">Your booking has been received and is now being processed.</p>

            <p>Package: ${listing?.title}</p>
            <table>
                <tr>
                    <td>Booking Date:</td><td><g:formatDate
                        date="${booking?.createdAt}" type="datetime" style="MEDIUM"/></td>
                </tr>
                <tr>
                    <td>Invoice No:</td><td>${booking?.invoiceNo}</td>
                </tr>
                <tr>
                    <td>Total no. of Guest:</td><td>${booking?.noOfGuest}</td>
                </tr>
                <tr>
                    <td>Total Rate:</td><td>${booking?.totalPrice}</td>
                </tr>
                <tr>
                    <td>Payment Type:</td><td>${booking?.paymentType}</td>
                </tr>
                <tr>
                    <td>Payment Status:</td><td>${booking?.paymentStatus}</td>
                </tr>
                <tr>
                    <td>Booking Status:</td><td>${booking?.status}</td>
                </tr>
            </table>

            <p>Please <a href="${createLink(uri: '/login')}">login</a> to our website to view the booking details.</p>
        </td>
    </tr>

    <tr>
        <td align="center" valign="top">
            <table style="margin-bottom:10px; text-align: center;" class="table" cellpadding="10" cellspacing="0"
                   width="700">
                <tbody>
                <tr>
                    <td align="center">
                        <p style="font-size: 11px; color: #898989;">Need help? Have feedback? Feel free to <a
                                href="${createLink(uri: '/contact-us', absolute: true)}"
                                style="color: #6ea4ad;">Contact Us</a></p>

                        <p style="font-size: 12px; color: #898989;">${siteSetting.getAddress1()}<br/>
                            Ph : ${siteSetting.getPhone1()}, ${siteSetting.getPhone2()}<br/>Email : <a
                                href="mailto:${siteSetting.getEmail()}"
                                style="color: #898989;">${siteSetting.getEmail()}</a></p>

                        <p style="font-size: 12px; color: #898989;">Copyright © <g:formatDate format="yyyy"
                                                                                              date="${new Date()}"/> ${siteSetting.getCopyright()}</p>
                    </td>
                </tr>

                </tbody>
            </table>

        </td>
    </tr>

</table>

</body>
</html>
