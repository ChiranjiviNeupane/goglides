
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Email Template</title>
</head>

<body style="margin: 0; font-family: Verdana; background:#e6e6e6; ">

    <table bgcolor="#e6e6e6" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="padding: 15px; font-size: 13px;">

           <tr>
                <td align="center" valign="top">
                    <table style="margin: 4px 0; color: #777;" class="table" bgcolor="#fff" border="0" cellpadding="10" cellspacing="0" width="700">
                       <tbody>
                       <tr>
                            <td valign="middle" style="padding-top: 25px;">
                                <a href="index.html"><img src="http://mockup.goglides.com/html/template/logo.png" alt=""
                                                  class="logo" style="width:160px;"></a>
                            </td> 
                            <td align="right">
                                <a target="_blank" href="http:facebook.com/goglides"><img
                                src="http://mockup.goglides.com/html/template/facebook.png"></a>
                        <a target="_blank"
                           href="https://plus.google.com/b/104573181260446052319/104573181260446052319/about?gmbpt=true&amp;hl=en">
                            <img src="http://mockup.goglides.com/html/template/google.png"></a>
                        <a target="_blank" href="https://twitter.com/googlides"><img
                                src="http://mockup.goglides.com/html/template/twitter.png"></a>
                        <a target="_blank"
                           href="https://www.youtube.com/channel/UCgl6AILUwi4QQzgeaCnw6qw?guided_help_flow=3">
                            <img src="http://mockup.goglides.com/html/template/ytb.png"></a>
                        <a target="_blank" href="https://goglides.tumblr.com/">
                            <img src="http://mockup.goglides.com/html/template/tumblr.png"></a>
                            </td>
                        </tr>
                       </tbody>
                    </table>
                </td>
            </tr>
    

            <tr>
                <td align="center" valign="top">
                    <table style="margin-bottom:15px; color: #777;" class="table" bgcolor="#fff" border="0" cellpadding="10" cellspacing="0" width="700">
                       <tbody>
                       <tr>
                           <td>
                             Dear ${userName},
            <br>
            <br>
            Welcome to Goglides.com
            <br>
            <br>
            Goglides is the best home for adventure lovers. We're exicited you're joining us.
            <br>
            Goglides team has been notified with your agent request. The goglides team will contact you soon.
            <br>
            <br>
            We look forward to working with you!
            <br>
			<br>
            Kind regards,<br>
            The Goglides<br> Team
                           </td>
                       </tr>

                       </tbody>
                    </table>
                </td>
            </tr>

            <tr>
              <td align="center" valign="top">
                  <p style="font-size: 10px; color: #898989;">Copyright © 2017 GoGlides. All rights reserved.</p> 
				  <p style="font-size: 10px; color: #898989;">This email was sent by Goglides.com, JP MARG, Thamel, Kathmandu, Nepal.</p>
              </td>
            </tr>

        </table>
  
</body>
</html>
