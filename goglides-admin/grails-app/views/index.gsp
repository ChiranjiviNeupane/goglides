%{--Use SiteSetting Service--}%
<g:set var="siteSettings" bean="siteSettingsService"/>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${siteSettings.getSiteTitle() + ' - ' + siteSettings.getTagline()}</title>
    <!-- Favicons-->
    <link rel="icon" href='${assetPath(src: '/dashboard-2017/images/favicon.ico')}' type="image/x-icon"/>
</head>

<body>
<section class="main">
    <div class="row">

    </div>
</section>
</body>
</html>
