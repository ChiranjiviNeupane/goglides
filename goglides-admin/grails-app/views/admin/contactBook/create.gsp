<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Contact BookInfo</h2>
    </div>

</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">


            <div class="x_content">
                <g:hasErrors bean="${this.contactBook}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.promotion}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
            </div>

            <g:form action="save" method="POST" class="form form-horizontal form-label-left">
              
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Business Type </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input id="businessType" type="text" name="businessType" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Company-Name">Company Name </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="companyName" type="text" name="companyName" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Street-Address">Street Address </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="streetAddress" type="text" name="streetAddress" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">City  </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="city" class="form-control col-md-7 col-xs-12" type="text" name="city">
                    </div>
                </div>

                <div class="form-group">
                   <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Country  </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="country" class="form-control col-md-7 col-xs-12" type="text" name="country">
                    </div>
                </div>

                 <div class="form-group">
                    <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Phone1 </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="phone1" class="form-control col-md-7 col-xs-12" type="text" name="phone1">
                    </div>
                </div>

                 <div class="form-group">
                    <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Phone2 </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="phone2" class="form-control col-md-7 col-xs-12" type="text" name="phone2">
                    </div>
                </div>

                 <div class="form-group">
                  <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Phone3 </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="phone3" class="form-control col-md-7 col-xs-12" type="text" name="phone3" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Email</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="companyEmail" name="companyEmail" class="form-control col-md-7 col-xs-12" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Website  </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="website" name="website" class="form-control col-md-7 col-xs-12" type="text">
                    </div>
                </div>

                 <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Facebook </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="facebook" name="facebook" class="form-control col-md-7 col-xs-12" type="text">
                    </div>
                </div>
  
                 <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Person <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="contactPerson" name="contactPerson" class="form-control col-md-7 col-xs-12" required="required" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Position
                 </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="jobPosition" name="jobPosition" class="form-control col-md-7 col-xs-12"  type="text"  >
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input name="mobile" class="form-control col-md-7 col-xs-12"  type="text" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Email </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input name="email" class="form-control col-md-7 col-xs-12"  type="text">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                        <fieldset class="buttons">
                            <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                        </fieldset>

                    </div>
                </div>

            </g:form>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        // validate the comment form when it is submitted
        $(".form").validate({
            rules: {
               
                contactPerson:{
                    required:true

                },
                mobile:{
                    number:true,
                    required:true
                },
                phone1:{
                   number:true
                } ,
                phone2:{
                    number:true
                },
                phone3:{
                        number:true
                },
                companyEmail:{
                    email:true
                },
                email:{
                    email:true
                }
                           
            },
            messages: {
                title:{
                    required:"please enter a contact person"
                },
                mobile: {
                    required: "please enter a mobile number",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                }

            }
        });
    });
</script>
</body>
</html>