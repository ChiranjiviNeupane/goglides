<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Manage Contact Book</h2>
    </div>
    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(action: 'create', namespace: 'admin')}" class="btn bttn">Add New Contact</a>
        </div>
    </div>
</div>
<div class="clearfix"></div>



<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Advanced Search</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
                <div class="x_content" style="display: none;">
                    <g:form url="[action: 'index', controller: 'contactBook']">
                        <div class="col-xs-6">
                            <input type="text" name="search" class="form-control" placeholder="Search for..." style="height: auto; padding: 7px 12px;">
                        </div>
                        <div class="col-xs-5">
                            <g:select name="searchAttribute" from="${["search by business Type", "search by company Name", "search by email","search by contact Person"]}"
                                      keys="${['businessType', 'companyName', 'email',"contactPerson"]}"
                                      value="${name}"
                                      noSelection="['': '-Choose Search Category-']" class="form-control" style="height: auto; padding: 7px 12px;"/>
                        </div>
                        <div class="col-xs-1 form-group">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </g:form>
                </div>
                <div class="clearfix"></div>
            </div>


        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_content">
                <div class="row">
                    <g:if test="${contactBook}">
                         <table id="table" class="table table-striped log-table">
                          <thead>
                            <tr>
                                <th>Business Type</th>
                                <th>Company Name</th>
                                <th>Country</th>
                                <th>Contact Person</th>
                                <th>Mobile</th>
                              
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${contactBook}" var="c">
                                <tr>
                                    <td>${c?.businessType}</td>
                                    <td>${c?.companyName}</td>
                                    <td>${c?.country}</td>
                                    <td>${c?.contactPerson}</td>
                                    <td>${c?.mobile}</td>
                                    <td>${c?.email}</td>

                                    <td>
                                        <a href="${createLink(uri: '/admin/contactBook/updates?id=' + c?.id)}"
                                           class="btn bttn">Edit</a>
                                        <a href="${createLink(uri: '/admin/contactBook/delete?id=' + c?.id)}"
                                           class="btn bttn" onClick="return confirm('Are you sure update')" >Delete</a>
                                    </td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                        <div class="pagination pull-right">
                            <g:paginate total="${contactBookSize ?: 0}" class="pagination"
                                        params="${[state: params?.state]}" action="index"
                                        next='&raquo;' prev='&laquo;' maxsteps="4"/>
                        </div>
                    </g:if>
                    <g:else>
                        <h1>Data not Found</h1>
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>