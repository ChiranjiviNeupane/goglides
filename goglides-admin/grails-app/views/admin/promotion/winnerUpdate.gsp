%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">
    <div class="row">
        <br>
        <div class="page-title">
            <div class="title_left">
                <div class="pull-left">
                    <g:form class="form-inline">
                        <div class="form-group">
                            <g:textField type="text" name="search" id="search" class="form-control col-sm-4" placeholder="Search user"/>
                        </div>
                    </g:form>
                </div>
            </div>
            <div class="title_right">
                <div class="pull-right">
                    <g:form class="form-inline">
                        <div class="form-group">
                            <g:select class="form-control col-sm-8"
                                      name="promotion"
                                      from="${promotion}"
                                      noSelection="['':'-Choose Promotion-']"
                                      optionValue="title"
                                        optionKey="id"
                                      id="promotion"
                            />
                        </div>
                    </g:form>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>User Lists </h3>
                    </div>

                    <div class="x_content">
                        <div class="row">
                            <table class="table table-hover table-striped" id="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th>FullName</th>
                                    <th>Phone No</th>
                                    <th id="tdRank">Rank</th>
                                    <th>Contact Status</th>
                                    <th>Prize Claim Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="tbody">

                                </tbody>
                            </table>
                        </div>
                        <div class="pagination">
                            <div id="content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <form class="form-horizontal" id="modalForm">
                        <div class="form-group">
                            <label class="col-md-3" for="id">User ID</label>
                            <div class="col-md-9">
                                <g:textField class="form-control" name="id" id="id" readonly=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3" for="fname">User name</label>
                            <div class="col-md-9">
                                <g:textField class="form-control" name="fname" id="fname" readonly=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3" for="phoneno">User Phoneno</label>
                            <div class="col-md-9">
                                <g:textField class="form-control" name="phoneno" id="phone" readonly=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3" for="phoneno">Rank</label>
                            <div class="col-md-9">
                                <g:select
                                        name="rank"
                                        from="${1..3}"
                                        value="rank"
                                        id="rank"
                                        class="form-control"
                                        noSelection="${['null':'Select Rank--']}"
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3" for="phoneno">Contact Status</label>
                            <div class="col-md-9">
                                <g:select id="contactStatus"
                                          name='contactStatus'
                                          value="contactStatus"
                                          class="form-control"
                                          from="${["true","false"]}"
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3" for="phoneno">Prize Claim</label>
                            <div class="col-md-9">
                                <g:select id="prizeClaim"
                                          name='prizeClaim'
                                          value="prizeClaim"
                                          class="form-control"
                                          from="${["true","false"]}"
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">User Image</label>
                            <div id="user-image"></div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="userImage" value="userImage" id="userImage"/>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveChanges">Save changes</button>
            </div>
        </div>
    </div>
</div>
<g:render template="/layouts/file_upload"/>

<script>
    $('#user-image').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "promotion", action: "multimediaUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                type: 'promotion'

            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key")}',
                    {
                        filename: filename,
                        directory: 'Promotion'
                    }).done(function (data) {
                    keyRetrieval.success(data,$("#idUserImage").ready(function() {
                        $("#userImage").empty();
                        $("#userImage").val(data);
                    }));
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "promotion", action: "multimediaDelete")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });
</script>
<script>
    $(document).ready(function(){
        $(".pagination").hide()
    })
    function ajax(url,data,method,callback){
        $.ajax({
            url:url,
            type:method,

            data:data,
            dataType:'json',
            async:false,
            cache:false,
            success:function(response){
                callback(response)
            },error:function(jqXHR, status){
                console.log(status)
            }
        })
    }

    $("#promotion").change(function(){
        $("#tbody").empty();
        $(".pagination").hide()
        var promotionId = $("#promotion").val()
        var url = "${createLink(uri:'/admin/promotion/getUserList')}"
        var d = {"pid": promotionId}
        ajax(url,d,"GET", function (data) {
            $(".pagination").show()
          setDataOnTable(data)
        })
    })

    $('#search').keyup(function(){
        var value = $(this).val().toLowerCase();
        if(value == ""){
            $('#tbody').show();
        } else {
            $('#tbody tr').each(function(){
                var text = $(this).text().toLowerCase();
                (text.indexOf(value) >= 0) ? $(this).show() : $(this).hide();
            });
        };
    });

    function btnClick(event){
        rankCount()
        var value = event.target.id;
        var arrayValue = value.split(',');
        var id = arrayValue[0];
        var fname = arrayValue[1]
        var phoneno = arrayValue[2]
        var contactStatus = arrayValue[3];
        var prizeClaim = arrayValue[4];
        var rank = arrayValue[5]
        if(id!="" && id!=null && id !="undefined" && fname!="" && fname!=null && fname !="undefined" && phoneno!="" && phoneno!=null && phoneno !="undefined"){
            $("#exampleModal").modal();
            $("#id").val(id);
            $("#fname").val(fname);
            $("#phone").val(phoneno);
            $("#contactStatus").val(contactStatus);
            $("#prizeClaim").val(prizeClaim);
            $("#rank").val(rank);
        }

        $("#saveChanges").click(function() {
            $("#exampleModal").modal('toggle');
            var id = $("#id").val();
            var fname = $("#fname").val();
            var phoneno = $("#phone").val();
            var rank = $("#rank").val();
            var contactStatus = $("#contactStatus").val();
            var prizeClaim = $("#prizeClaim").val();
            var userImage = $("#userImage").val();
            if(contactStatus == "true"){
                contactStatus = 1
            }else{
                contactStatus = 0
            }
            if(prizeClaim== "true"){
                prizeClaim = 1
            }else{
                prizeClaim= 0
            }
            var data = {
                "rank":rank,
                "id":id,
                "contactStatus":contactStatus,
                "prizeClaim":prizeClaim,
                "uImage":userImage,
            }
            var url = "${createLink(uri:'/admin/promotion/setWinnerRank', absolute: true)}"
            ajax(url,data,"GET",function(data){
                if(data !== null){
                    $("#exampleModal").find("input,textarea,select").val('').end();
                    $("#userImage").val()
                    $("#exampleModal").find("#user-image").html("");
                    var pid = $("#promotion").val()
                    var url = "${createLink(uri:'/admin/promotion/getUserList')}"
                    var d = {pid:pid}
                    ajax(url,d,"GET", function (data){
                        setDataOnTable(data)
                    })
                }
            });
        });
    }

    function rankCount(){
    var pid = $("#promotion").val()
    var url = "${createLink(uri:'/admin/promotion/rankCount')}"
    var data = {pid:pid}
        ajax(url,data,"GET",function(data) {
            console.log(data[0])
            if(data[0] >= 3){
                $("#modalForm").hide();
                $("#saveChanges").hide();
                $(".modal-body").empty();
                $(".modal-body").append("You cannot perform this action.")
            }
        })
    }
</script>
<script src="//rawgit.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
<script>
    $('.pagination').bootpag({
        total: 50,
        page: 1,
        maxVisible: 3,
        leaps: false,
        firstLastUse: false,
        first: 'First',
        last: 'Last',
        prev: 'Prev',
        next: 'Next',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(event, num){
        $("#content").html(num);
        $(event.target).click(function(){
            var page = $("#content").text()
            var pid = $("#promotion").val()
            var max = 30
            var offset = max * (page-1)
            var url = "${createLink(uri:'/admin/promotion/getUserList')}"
            var d = {offset:offset,max:max,pid:pid}
            ajax(url,d,"GET", function (data){
                setDataOnTable(data)
            })
        })
    });


    function setDataOnTable(data){
        var i = 0;
        var table = '<tr>';
        if(data == "" || data == null || data == "undefined"){
            $("#modalForm").hide();
            $("#saveChanges").hide();
            $(".modal-body").empty();
            $(".modal-body").append("No data available in database.")
            $("#exampleModal").modal("show");
        }else {
            $("#tbody").empty();
            $.each(data,function(k, v) {
                    table += '<td id="fullname">' + v.user.firstname + " " + v.user.lastname+ '</td>' +
                        '<td id="phoneno">' + v.user.mobile + '</td>' +
                        '<td id="idRank">' + v.promotionUser.winnerRank + '</td>' +
                        '<td id="">' + v.promotionUser.contactStatus + '</td>' +
                        '<td id="">' + v.promotionUser.prizeClaim + '</td>' +
                        '<td>' +
                        '<button type="button" ' +
                        'data-toggle="modal" ' +
                        'class="btn btn-info" ' +
                        'id="' + v.promotionUser.id + ","+v.user.firstname+","+v.user.mobile+","+ + v.promotionUser.contactStatus + "," + v.promotionUser.prizeClaim + "," + v.promotionUser.winnerRank + '" onclick="btnClick(event)">Update</button></td>' +
                        '</tr>'
                    i++;
                })
            $("#tbody").append(table);
        }
    }
</script>
</body>
</html>