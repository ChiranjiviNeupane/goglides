<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h3>Participant List</h3>
    </div>

    <div class="title_right">
        <p class="pull-right">Total Participants: ${participants.size()}</p>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_content">
                <div class="row">
                    <g:if test="${participants}">
                        <table class="table table-bordered table-condensed" id="mytable">
                            <tr>
                                <th>User ID</th>
                                <th>Name</th>
                                <th>Phone No.</th>
                            </tr>
                            <g:each in="${participants}" var="p">
                                <g:if test="${p?.user?.firstname !='' && p?.user?.lastname!=''}">
                                    <tr>
                                        <td>${p?.user?.id}</td>
                                        <td>${p?.user?.firstname + ' ' + p?.user?.lastname}</td>
                                        <td>${p?.user?.phone}</td>

                                    </tr>
                                </g:if>

                            </g:each>
                        </table>

                        <div class="pagination">
                            <g:paginate total="${promotion}"/>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(function () {
        $('#mytable tr').each(function() {
            var t;
            var customerId = $(this).not('thead tr').find("td").eq(2).html();
            if (typeof customerId === "undefined") {
            }else {
                const regex = /\d(?=\d{4})/mg;
                $(this).not('thead tr').find("td").eq(2).html(customerId.replace(regex,"#"));
            }
        });
    });
</script>
</body>
</html>