<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h3>Participant List</h3>
    </div>

    <div class="title_right">
        <p class="pull-right">Total Participants: ${participants.size()}</p>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Participant  - ${promotion?.title} #${promotion?.id}</h3>
            </div>

            <div class="x_content">
                <div class="row">
                    <g:if test="${participants}">
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone No.</th>
                                <th>Winner Rank</th>
                                <th>Created At</th>
                                <td>FeedBack</td>
                            </tr>
                            <g:each in="${participants}" var="p">
                                <tr class="${p?.winnerRank == '1' ? 'bg-success' : ''}">
                                    <td>${p?.user?.firstname + ' ' + p?.user?.lastname}</td>
                                    <td>${p?.user?.email}</td>
                                    <td>${p?.user?.phone}</td>
                                    <td>${p?.winnerRank}</td>
                                    <td><g:formatDate date="${p?.createdAt}" type="datetime" style="MEDIUM"/></td>
                                    <td>  <a href="${createLink(uri: '/admin/testimonial/create?id=' + p?.id)}"
                                             class="btn bttn">Add Feedback</a></td>
                                </tr>
                            </g:each>
                        </table>

                        <div class="pagination">
                            <g:paginate total="${promotion}"/>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>