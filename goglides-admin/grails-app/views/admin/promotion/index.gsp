%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Manage Promotion</h2>
    </div>
    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(action: 'create')}" class="btn bttn">Add New Promotion</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Promotion Lists</h3>
            </div>

            <div class="x_content">
                <div class="row">
                    <g:if test="${promotion}">
                        <table class="table">
                            <tr>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            <g:each in="${promotion}" var="p">
                                <tr>
                                    <td>${p?.title}</td>
                                    <td>${p?.status}</td>
                                    <td>
                                        <a href="${createLink(uri: '/admin/promotion/view?id=' + p?.id)}" class="btn bttn">View</a>
                                        <a href="${createLink(uri: '/admin/promotion/delete?id=' + p?.id)}"
                                           class="btn bttn">Delete</a>
                                    </td>
                                </tr>
                            </g:each>
                        </table>

                        <div class="pagination">
                            <g:paginate total="${promotion}"/>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>