%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Create Promotion</h2>
    </div>

</div>

<div class="clearfix"></div>
<asset:stylesheet href="standalone/css/fine-uploader-gallery.css"/>
<asset:javascript src="standalone/js/s3.jquery.fine-uploader.js"/>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">


            <div class="x_content">
                <g:hasErrors bean="${this.promotion}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.promotion}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
            </div>

            <g:form action="save" method="POST">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="">Title</label>
                        <input type="text" name="title" placeholder="Title" class="form-control"
                               id="title" value="${promotion?.title}">
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="">Descrition</label>
                        <textarea name="description" placeholder="Description" class="form-control" rows="6"></textarea>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="">Start Date</label>
                        <input type="text" name="startDate" placeholder="start date" class="form-control datepicker"
                               id="startDate" value="startDate">
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">End Date</label>
                        <input type="text" name="endDate" placeholder="end date" class="form-control datepicker"
                               id="endDate" value="endDate">
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Has pop up</label>
                        <g:radioGroup name="hasPopUp"
                                      labels="[' Yes ', ' No ']"
                                      values="[true, false]"
                                      required="required">
                            ${it.label} ${it.radio}
                        </g:radioGroup>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Status</label>
                        <g:select name="status12" from="${["Draft", "Publish", "Trash"]}"
                                  keys="${['draft', 'publish', 'trash']}"
                                  value="${name}"
                                  noSelection="['': '-Choose status-']" class="form-control"/>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Type</label>
                        <g:select name="type" from="${["Weekly", "Monthly", "Yearly"]}"
                                  keys="${['weekly', 'monthly', 'yearly']}"
                                  value="${name}"
                                  noSelection="['': '-Choose Type-']" class="form-control"/>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Sort</label>
                        <input type="number" name="sort" min="0" max="99" placeholder="0" class="form-control"
                               id="sort" value="${sort ?: 0}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="">Result Announcement Date</label>
                        <input type="text" name="resultAnnouncementDate" placeholder="Result Announcement Date" class="form-control datepicker"
                               id="resultAnnouncementDate" value="resultAnnouncementDate">
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Feature Image</label>
                        <div id="feature-image"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner Image</label>
                        <div id="banner-image"></div>
                    </div>
                    <div class="form-group">
                            <input type="hidden" name="bannerImage" value="bannerImage" id="bannerImage"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <fieldset class="buttons">
                            <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                        </fieldset>
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>

<g:render template="/layouts/file_upload"/>
<script>
    $('#feature-image').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "promotion", action: "multimediaUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                type: 'promotion'
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key")}',
                    {
                        filename: filename,
                        directory: 'Promotion'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "promotion", action: "multimediaDelete")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];
                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });
    $('#banner-image').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "promotion", action: "multimediaUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                type: 'promotion'
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key")}',
                    {
                        filename: filename,
                        directory: 'Promotion'
                    }).done(function (data) {
                    keyRetrieval.success(data,$("#idBannerImgae").ready(function() {
                        $("#bannerImage").empty();
                        $("#bannerImage").val(data);
                    }));
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "promotion", action: "multimediaDelete")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    // function ajax(data) {
    //     var startDate = $("#startDate").val();
    //     var endDate = $("#endDate").val();
    //     $.ajax({
    //         url : "http://localhost:8080/admin/promotion/insertBannerImage?bannerImage="+data+"&s="+startDate+"&e"+endDate,
    //         method : "PUT",
    //         success:function(response){
    //             alert(response)
    //         },error:function(response){
    //             alert(response)
    //         }
    //     });
    // }
</script>
</body>
</html>