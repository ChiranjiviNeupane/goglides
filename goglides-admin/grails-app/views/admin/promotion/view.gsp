<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>
<body>

<div class="page-title">
    <div class="title_left">
        <h2>Promotion ${promotion?.title}</h2>
    </div>
</div>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-lg-3">
                        <h3>Status: ${promotion?.status}</h3><br/>
                        <h4>Description: ${promotion?.description}</h4>
                        <h3>Action :</h3> <g:each in="${promotion}" var="p">
                        <a href="${createLink(uri: '/admin/promotion/edit?id=' + p?.id)}"
                           class="btn bttn">Edit</a> <br/><br/>
                        <a href="${createLink(uri: '/admin/promotion/viewParticipatedUser?id=' + p?.id)}" class="btn bttn">Print User</a><br/><br/>
                        <a href="${createLink(uri: '/admin/promotion/participants?id=' + p?.id)}" class="btn bttn">Participants</a><br/><br/>
                        %{--<a href="${createLink(uri: '/admin/promotion/winner-list?id=' + p?.id)}"--}%
                           %{--class="btn bttn">Draw Winner</a><br/><br/>--}%
                        %{--<a href="${createLink(uri: '/admin/promotion/rest-result?id=' + p?.id)}"--}%
                           %{--class="btn bttn btn-danger">Reset Winner</a><br/><br/>--}%
                        <a href="${createLink(action:'winnerUpdate')}"
                           class="btn bttn">Winner Update</a><br/><br/>

                    </g:each>
                    </div>
                    <div class="col-lg-9">
                        <canvas id="chart-wrap">
                        </canvas>
                    </div>
                </div>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js"></script>

                <script>
           var ctx = document.getElementById("chart-wrap");
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: ${raw(data.toString())},
                            datasets: [{
                                fill: false,
                                label: 'Total Register User: '+${participants.size()},
                                data: ${label},
                                backgroundColor: '#66a5ad',
                                borderColor: '#66a5ad',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    Label:{
                                        text: 'Participants'
                                    },
                                    ticks: {
                                        beginAtZero:true

                                    }
                                }]
                            }
                        }
                    });


 /*        var ctx = document.getElementById("chart-wrap").getContext("2d");
         var myLine = new Chart(ctx, {
             type: 'line',
            data: {
             labels: %{--${raw(data.toString())}--}%,
              datasets: [{
                 fill: false,
                 label: 'Total Register User: '+%{--${participants.size()}--}%,
                 data: %{--${label}--}%,
                 backgroundColor: '#66a5ad',
                 borderColor: '#66a5ad',
                 borderWidth: 1
             }]
         },
             showTooltips: false,

             options: {
                 animation: {

                     onComplete : function(){
                         var ctx = this.chart.ctx;
                         ctx.textAlign = "center";
                         ctx.textBaseline = "bottom";

                         this.datasets.forEach(function (dataset) {
                             dataset.points.forEach(function (points) {
                                 ctx.fillText(points.value, points.x, points.y - 10);
                             });
                         })
                     }
                 },
             }
         });*/
                </script>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</body>
</html>