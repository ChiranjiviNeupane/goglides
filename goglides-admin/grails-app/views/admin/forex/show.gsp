<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="dashboard-2017" />
        <g:set var="entityName" value="${message(code: 'forex.label', default: 'Forex')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div id="show-forex" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">
                        <label>Country Code:</label>
                    </div>
                    <div class="col-md-6">
                        <p>${forex?.countryCode}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label>Currency Code:</label>
                    </div>

                    <div class="col-md-6">
                        <p>${forex?.currencyCode}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>Rate:</label>
                    </div>

                    <div class="col-md-6">
                        <p>${forex?.rate}</p>
                    </div>
                </div>
            </div>
            <g:form resource="${this.forex}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit btn btn-default" action="edit" resource="${this.forex}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete btn btn-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    <g:link class="index btn btn-primary" action="index" resource="${this.forex}"><g:message code="default.button.index.label" default="Index" /></g:link>
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
