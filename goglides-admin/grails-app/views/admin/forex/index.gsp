<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'forex.label', default: 'Forex')}" />
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Manage Forex</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a class="btn bttn" href="${createLink(action: 'create')}"><i
                    class="fa fa-user-plus"></i> Add Forex</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Forex Details</h3>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <table class="table table-stripped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Country Code</th>
                        <th>Currency Code</th>
                        <th>Rate</th>
                        <th>Status</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    <g:each in="${forex}" status="i" var="it">
                        <tr>
                            <td><span class="text-info">${it?.countryCode}</span></td>
                            <td><span class="text-info">${it?.currencyCode}</span></td>
                            <td><span class="text-info">${it?.rate}</span></td>
                            <td><span class="text-info">${it?.status}</span></td>
                            <td><a
                                    href="${createLink(namespace:"admin", controller:"forex", action: "del", id:it?.id)}"
                                   class="btn btn-info"
                                    onclick="return confirm('Are you sure?')"
                            >
                                Delete
                            </a>
                            <a
                                    href="${createLink(namespace:"admin", controller:"forex", action:"edit", id:it?.id)}" class="btn btnn btn-default">
                                Edit
                            </a>
                            </td>
                        </tr>
                    </g:each>

                    </tbody>
                </table>

                <div class="pagination pagination-custom pull-right">
                    <g:paginate class="" total="${forexCount ?: 0}"/>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>