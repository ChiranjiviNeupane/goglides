<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="dashboard-2017" />
        <g:set var="entityName" value="${message(code: 'forex.label', default: 'Forex')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Create Forex Detail</h3>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <g:form resource="${this.forex}" method="POST" class="form">
                        <g:hasErrors bean="${this.forex}">
                            <ul class="alert alert-danger" role="alert">
                                <g:eachError bean="${this.forex}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                            error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>


                        <div class="text-muted"><label for="currency">Country Code</label></div>

                        <div class="form-group">
                            <input id="countryCode" class="form-control" type="text"
                                   name="countryCode" value="${forex?.countryCode}" placeholder="Country Code"
                                   required="required">
                        </div>
                        <div class="text-muted"><label for="currencyCode">currencyCode</label></div>

                        <div class="form-group">
                            <input id="currencyCode" class="form-control" type="text"
                                   name="currencyCode" value="${forex?.currencyCode}" placeholder="currencyCode "
                                   required="required">
                        </div>
                        <div class="text-muted"><label for="rate">Rate</label></div>

                        <div class="form-group">
                            <input id="rate" class="form-control" type="text"
                                   name="rate" value="${forex?.rate}" placeholder="rate "
                                   required="required">
                        </div>

                        <div class="text-muted"><label for="rate">Status</label></div>
                        <div class="form-group">
                            <select name="status" id="status" class="form-control">
                                <option value="false">false</option>
                                <option value="true">true</option>
                            </select>
                        </div>
                        <div class=" clearfix">
                            <input type="submit" class="btn bttn pull-right" value="Save">
                        </div>

                    </g:form>
                </div>
            </div>
        </div>
    </div>


    </body>
</html>
