%{--Use ListingService--}%
<g:set var="listService" bean="listingService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
%{--Use FriendlyUrlService--}%
<g:set var="friendlyUrlService" bean="friendlyUrlService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    %{-- Star rating --}%
    <asset:stylesheet href="standalone/js/star-rating/css/star-rating.min.css"/>
    <asset:javascript src="standalone/js/star-rating/js/star-rating.min.js"/>

    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>Listings Reviews</h2>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Review Details <small>list of reviews</small></h2>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="${createLink(namespace: 'admin', controller: 'listingReview', action: 'index')}"><i
                                class="fa fa-bars"></i> All </a>
                        <a class="btn btn-dark" href="${createLink(namespace: 'admin', controller: 'listingReview', action: 'index', params: [state: 'draft'])}"><i
                                class="fa fa-file-text"></i> Draft </a>
                        <a class="btn btn-success" href="${createLink(namespace: 'admin', controller: 'listingReview', action: 'index', params: [state: 'publish'])}"><i
                                class="fa fa-check"></i> Publish </a>
                        <a class="btn btn-danger" href="${createLink(namespace: 'admin', controller: 'listingReview', action: 'index', params: [state: 'trash'])}"><i
                                class="fa fa-trash-o"></i> Past </a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="row">
                        <div class="col-xs-12">
                            <g:if test="${reviews}">
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th>Business</th>
                                            <th>Listing</th>
                                            <th>User</th>
                                            <th>Rating</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <g:each in="${reviews}">
                                            <tr>
                                                <td>${it?.businessDirectory?.businessName}</td>
                                                <td>${it?.listing?.title}</td>
                                                <td>
                                                    <g:set var="user" value="${userService.user(it?.user)}"/>
                                                    ${user?.firstname} ${user?.lastname}
                                                </td>
                                                <td>
                                                    <g:each in="${1..5}" var="c">
                                                        <g:if test="${c <= it?.rating}">
                                                            <i class="fa fa-star"></i>
                                                        </g:if>
                                                        <g:else>
                                                            <i class="fa fa-star-o"></i>
                                                        </g:else>
                                                    </g:each>
                                                </td>
                                                <td class="text-capitalize">
                                                    ${friendlyUrlService.sanitizeWithSpaces(it?.status)}
                                                </td>
                                                <td>
                                                    <a href="${createLink(controller: 'listingReview', action: 'view', namespace: 'admin', id: it?.id)}"
                                                       class="" data-toggle="tooltip" title="View">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    %{--<a href="${createLink(controller: 'listingReview', action: 'delete', namespace: 'admin', id: it?.id)}"--}%
                                                       %{--class="text-danger" data-toggle="tooltip" title="Trash"--}%
                                                       %{--onclick="return confirm('Are you sure you want to trash the item?')">--}%
                                                        %{--<i class="fa fa-trash-o"></i>--}%
                                                    %{--</a>--}%
                                                </td>
                                            </tr>
                                        </g:each>
                                        </tbody>
                                    </table>
                                </div>

                                <div class=" pagination pull-right ">
                                    <g:paginate total="${reviews?.totalCount ?: 0}" class="pagination"
                                                params="${[state: params?.state]}"
                                                action="index"
                                                next='&raquo;' prev='&laquo;' maxsteps="4"/>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>