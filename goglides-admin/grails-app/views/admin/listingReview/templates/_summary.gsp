<div class="x_panel">
    <div class="x_title">
        <h2>Summary</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled">
                <li class="text-capitalize">
                    <i class="glyphicon glyphicon-briefcase"></i>
                    <g:if test="${review?.businessDirectory?.businessName}">
                        ${review?.businessDirectory?.businessName}
                    </g:if>
                </li>
                <li class="text-capitalize">
                    <i class="glyphicon glyphicon-list"></i>
                    <g:if test="${review?.listing?.title}">
                        ${review?.listing?.title}
                    </g:if>
                </li>
                <li class="text-capitalize">
                    <i class="glyphicon glyphicon-ok"></i> <label>Status:</label> ${review?.status}
                </li>
                <li><hr></li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Created:</label> <g:formatDate
                        date="${review?.createdAt}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Updated:</label> <g:formatDate
                        date="${review?.modifiedAt}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Trashed:</label> <g:formatDate
                        date="${review?.trashedAt}" type="date" style="MEDIUM"/>
                </li>
            </ul>
        </div>
    </div>
</div>