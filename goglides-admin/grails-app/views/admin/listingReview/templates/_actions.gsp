<div class="x_panel">
    <div class="x_title">
        <h2>Actions</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled action-wrap">
                <li>
                    <a class="btn btn-success col-xs-12" href="${createLink(action: 'publish', id: review?.id)}"
                       onclick="return confirm('The review will be published. Click OK to continue')">
                        <i class="fa fa-check"></i> Approve and Publish</a>
                </li>
                <li>
                    <a href="${createLink(action: 'edit', id: businessDirectory?.businessId)}"
                       class="btn btn-dark col-xs-12"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                </li>
                <li>
                    <a class="btn btn-danger col-xs-12" href="javascript:void(0);" data-toggle="modal"
                       data-target="#myModal">
                        <i class="fa fa-times"></i> Decline</a>
                </li>
                <li class="">
                    <a class="btn btn-danger col-xs-12"
                       href="${createLink(action: 'delete', id: businessDirectory?.businessId)}"
                       onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                        <i class="fa fa-trash-o"></i> Trash
                    </a>
                </li>

            </ul>
        </div>
    </div>
</div>