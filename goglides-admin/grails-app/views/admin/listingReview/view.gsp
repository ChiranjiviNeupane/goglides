%{--Use ListingService--}%
<g:set var="listService" bean="listingService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
%{--Use FriendlyUrlService--}%
<g:set var="friendlyUrlService" bean="friendlyUrlService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    %{-- Star rating --}%
    <asset:stylesheet href="standalone/js/star-rating/css/star-rating.min.css"/>
    <asset:javascript src="standalone/js/star-rating/js/star-rating.min.js"/>

    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>Listings Reviews</h2>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>${review?.businessDirectory?.businessName} <small>detail review of business directory</small>
                    </h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content listing-view">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <ul class="nav nav-tabs tabs-left">
                                <li class="active">
                                    <a href="#review" data-toggle="tab" aria-expanded="false">Review</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-xs-12 col-sm-9">
                            <div class="tab-content">
                                <div class="tab-pane active" id="review">
                                    <h4>Review
                                        <a class="pull-right" data-toggle="tooltip" title="Edit"
                                           href="${createLink(uri: '', params: [])}"><i
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                    </h4>

                                    <div><i class="fa fa-user"></i>
                                        <g:set var="user" value="${userService.user(review?.user)}"/>
                                        ${user?.firstname} ${user?.lastname}
                                    </div>
                                    <hr>

                                    <div><label>Rating</label></div>

                                    <div>
                                        <g:each in="${1..5}" var="c">
                                            <g:if test="${c <= review?.rating}">
                                                <i class="fa fa-star"></i>
                                            </g:if>
                                            <g:else>
                                                <i class="fa fa-star-o"></i>
                                            </g:else>
                                        </g:each>
                                    </div>
                                    <hr>

                                    <div><label>Comment</label></div>

                                    <div>
                                        ${review?.comment}
                                    </div>
                                    <hr>

                                    <div><label>Blog</label></div>

                                    <div>
                                        <g:if test="${review?.blogUrl}">
                                            <a href="${review?.blogUrl}" target="_blank"><i class="fa fa-link"></i> Blog
                                            </a>
                                        </g:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-3">
            <g:render template="templates/actions" model=""/>
            <g:render template="templates/summary" model=""/>
        </div>
    </div>
</div>
</body>
</html>