<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="frontend-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container banner-block">

    <div class="ticket-wrap" id="divID">
        <div class="print-btn">
            <a href="" class="btn bttn btn-lg" onclick="printTicket()">Print Ticket</a>
        </div>
        %{--<g:each in="${eventTicket}" var="c">--}%
            %{--<div style="width: 50%; float: left; text-align: center; padding: 15px; border: 1px dotted #ccc;"><p><img style="width: 180px;"--}%
                                                                                                                      %{--src="${assetPath(src: '/images/logo.png')}"></p>--}%

                %{--<p><strong>Ticket No</strong> : ${c?.ticketNo} <strong style="padding-left: 20px;">Register Date</strong> : <g:formatDate format="yyyy-MM-dd" date="${eventBooking?.event?.createdAt}"/>--}%
                %{--</p>--}%

                %{--<p><strong>Payment Mode</strong> : ${eventBooking?.paymentMode}</p>--}%

                %{--<p><strong class="title">${eventBooking?.event?.title}</strong></p>--}%

                %{--<p><strong><g:formatDate format="yyyy-MM-dd" date="${eventBooking?.event?.createdAt}"/></strong></p>--}%

                %{--<p>${eventBooking?.event?.location}</p>--}%

                %{--<p>Total Amount : <strong>Rs ${eventBooking?.eventCharge?.retailRate}</strong></p>--}%

                %{--<p><img src="images/barcode.png" class="code-img"></p>--}%
            %{--</div>--}%
        %{--</g:each>--}%
        <g:if test="${eventBooking?.event?.eventType=='free'}">
            <g:render template="/admin/event/templates/printFreeEvent"/>
        </g:if>
        <g:if test="${eventBooking?.event?.eventType=='fee'}">
            <g:render template="/admin/event/templates/printPaidEvent"/>
        </g:if>

    </div>
</div>


<script>
    function printTicket() {
        window.print()
    }
</script>

</body>
</html>