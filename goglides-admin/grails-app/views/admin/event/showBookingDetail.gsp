<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Event title:${event?.title}</h2>
    </div>

</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_content">
                <g:hasErrors bean="${this.promotion}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.promotion}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>

                <div class="row">
                    <g:if test="${eventBooking}">
                        <table class="table" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Quantity</th>
                                <th>Total Amount</th>
                                <th>Payment Status</th>
                                <th>Payment Mode</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <g:each in="${eventBooking}" var="e">
                            <tr>
                                <td>${e?.firstname +" "+e?.lastname}</td>
                                <td>${e?.phone}</td>
                                <td>${e?.email}</td>
                                <td>${e?.quantity}</td>
                                <td>${e?.totalAmount}</td>
                                <td>${e?.paymentStatus}</td>
                                <td>${e?.paymentMode}</td>
                                <td>${e?.status}</td>
                                <td >
                                    <a href="${createLink(uri: '/admin/event/editEventBooking?id=' + e?.id)}"
                                       ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="${createLink(uri: '/admin/event/printTicket?id=' + e?.id)}"
                                        ><i class="fa fa-print" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                            </g:each>
                            </thead>
                        </table>
                    </g:if>
                    <g:else>
                        this event has zero booking
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>