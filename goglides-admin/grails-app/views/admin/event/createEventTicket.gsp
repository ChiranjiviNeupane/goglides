<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Add Ticket</h2>
    </div>

</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">


            <div class="x_content">
                <g:form action="addTicket" method="post" class="form">
                    <input type="hidden" name="eventId" value="${event?.id}"/>

                    <div class="form-group col-sm-6">
                        <label for=""> Title</label>
                        <input type="text" name="ticketTitle" placeholder="Title" class="form-control"
                               id="title" value="" required>
                    </div>


                    <div class="form-group col-sm-6">
                        <label for="">Description</label>
                        <textarea id="description" name="description" rows=5 cols=5 class="form-control description"
                                  placeholder="Description" maxlength="500" required></textarea>
                    </div>


                        <div class="row" id="eventTypes">
                            <div class="form-group col-sm-6">
                                <label for="">WholeSale Rate</label>
                                <input type="text" name="wholeSaleRate" placeholder="WholeSale rate" class="form-control"
                                       id="wholeSaleRate" value="0" onkeydown="resetvalue(this)" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="">RetailRate</label>
                                <input type="text" name="retailRate" placeholder="Retail Rate" class="form-control"
                                       id="retailRate" value="0" required>
                            </div>
                        </div>

                    <div class="form-group col-sm-6">
                        <label for="">Available Stock</label>
                        <input type="text" name="availableStock" placeholder="Available Stock" class="form-control"
                               id="availableStock" value="" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Status</label>
                        <g:select name="status12" from="${["Draft", "Publish", "Trash"]}"
                                  keys="${['draft', 'publish', 'trash']}"
                                  noSelection="['': '-Choose status-']" class="form-control" id="status" required="required"/>
                    </div>

                    <div class="form-group col-sm-6">
                        <fieldset class="buttons">
                            <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                        </fieldset>
                    </div>

                </g:form>

            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function () {
    var eventType='${event.eventType}';
    if(eventType=='free'){
        $('#eventTypes').hide();
    }
    $('.form').validate({
        rules:{
            title:{
                required: true
            },
            description:{
                required:true
            },
            wholeSaleRate:{
                required:true,
                number:true
            },
            retailRate:{
                required:true,
                number:true
            },
            availableStock:{
                required:true,
                number:true
            }
        }

        }
    );


});

function resetvalue(ele) {
if(event.key=='Enter'){
    alert(ele.value)
}
}

</script>
</body>
</html>