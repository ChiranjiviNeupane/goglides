%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!-- Fine Uploader Gallery CSS file-->
<g:set var="imageProcessingService" bean="imageProcessingService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <asset:stylesheet href="standalone/css/fine-uploader-gallery.css"/>
    <asset:javascript src="standalone/js/s3.jquery.fine-uploader.js"/>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Event title:${event?.title}</h2>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_content">
                <g:hasErrors bean="${this.event}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.event}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>

                <div class="row">

                    <div class="col-lg-3">
                        <h3>Event Type:  ${event?.eventType}</h3>

                        <h3>Status: ${event?.status}</h3><br/>
                        <h4>Description:${raw(event?.description)}</h4>
                    </div>

                    <div class="col-lg-6">
                        <div class="col-sm-offset-1 col-sm-3">
                            <label>Preview</label>

                            <div class="preview">
                                <img src='${amazonS3Service.destinationBucketCDN()+imageProcessingService.checkImageType(eventMultimedia?.file,'feature')}' id="showImage" onerror="return imageNotFound('${amazonS3Service.destinationBucketCDN()+imageProcessingService.checkImageType(eventMultimedia?.file,"feature")}')" complete="complete"/>                                <span class="preview-hvr"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <h2>Action:</h2>
                        <a href="${createLink(uri: '/admin/event/delete?id=' + event?.id)}"
                           class="btn bttn">Delete</a><br/><br/>
                        <a href="${createLink(uri: '/admin/event/updateDescription?id=' + event?.id)}"
                           class="btn bttn">update Description</a><br/><br/>
                        <a href="${createLink(uri: '/admin/event/updateImage?id=' + event?.id)}"
                           class="btn bttn">update Image & Phone</a><br/><br/>
                        <a href="${createLink(uri: '/admin/event/addNewTicket?id=' + event?.id)}"
                           class="btn bttn">Add Ticket</a><br/><br/>
                        <a href="${createLink(uri: '/admin/event/bookingDetails?id=' + event?.id)}"
                           class="btn bttn">Booking Detail</a><br/><br/>
                    </div>
                </div>
                <br/><br/>
            </div>
            <g:if test="${eventCharge}">

                <div class="row eventTicket">
                    <h2>Ticket List</h2>
                    <g:if test="${eventCharge}">

                        <table class="table table-striped log-table" id="thetable">
                            <tr><th>Title</th>
                                <th>WholeSale Rate</th>
                                <th>Retail Rate</th>
                                <th>Available Stock</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>

                            <g:each in="${eventCharge}" var="e">
                                <tr>
                                    <td>${e?.id}</td>
                                    <td>${e?.ticketTitle}</td>
                                    <td>${e?.wholeSaleRate}</td>
                                    <td>${e?.retailRate}</td>
                                    <td>${e?.availableStock}</td>
                                    <td>${e?.status}</td>
                                    <td>
                                        <a href="#"
                                           class="btn bttn" onclick="deleteEvent()">Delete Ticket</a>
                                        <a href="<g:createLink controller="event" action="updateTicket"
                                                               params="[id: e?.id, eid: event?.id]"/>"
                                           class="btn bttn">update Ticket</a>

                                    </td>
                                </tr>
                            </g:each>
                        </table>

                    </g:if>

                </div>
            </g:if>
        </div>
    </div>
</div>
<script>

function imageNotFound(destinationUrl) {

    var imageUrl='${amazonS3Service.destinationBucketCDN()+eventMultimedia?.file}'

    if(imageUrl==destinationUrl){
        return false;
    }
    else {
        $('#showImage').attr('src','${amazonS3Service.sourceBucketCDN()+eventMultimedia?.file}')
        var URL="${createLink(controller:'imageProcessing',action:'createImage')}"
        $.ajax({
                url:URL,
                type:"POST",
                data:{id:'${event?.id}',
                    imageType:'feature',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             '

                },
                success: function (resp) {
                    console.log("resp "+resp)
                    if(resp=="success"){
                        console.log("success")
                    }else {
                        alert("fail...")
                    }
                }
            }
        );
    }
}



    $(document).ready(function () {
        $('#thetable').find('tr').find('td:first').hide();
    });

    function deleteEvent() {
        $('#thetable').find('tr').click(function () {
            var id = $(this).find('td:first').text();
            console.log("Id" + id)

            if (id != '') {
                id = parseInt(id);
                $.ajax({
                    url: "${createLink(action: 'deleteTicket',controller: 'event',namespace: 'admin')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            location.reload();
                        }

                    }
                });
            }

        })
    }


</script>
</body>
</html>
