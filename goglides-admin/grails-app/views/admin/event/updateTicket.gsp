<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Update Ticket</h2>
    </div>

</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">


            <div class="x_content">
                <g:form action="updateEventTickets" method="post" >
                    <input type="hidden" name="eventId" value="${event?.id}"/>
                    <input type="hidden" name="eventChargeId" value="${eventCharge?.id}"/>

                    <div class="form-group col-sm-6">
                        <label for=""> Title</label>
                        <input type="text" name="ticketTitle" placeholder="Title" class="form-control"
                               id="title" value="${eventCharge?.ticketTitle}" required>
                    </div>


                    <div class="form-group col-sm-6">
                        <label for="">Description</label>
                        <textarea id="description" name="description" rows=5 cols=5 class="form-control description"
                                  placeholder="Description" maxlength="500" required>${eventCharge?.description}</textarea>
                    </div>
                    <div class="row" id="eventTypes">
                    <div class="form-group col-sm-6">
                        <label for="">WholeSale Rate</label>
                        <input type="text" name="wholeSaleRate" placeholder="WholeSale rate" class="form-control"
                               id="" value="${eventCharge?.wholeSaleRate}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="">RetailRate</label>
                        <input type="text" name="retailRate" placeholder="Retail Rate" class="form-control"
                               id="" value="${eventCharge?.retailRate}" required>
                    </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="">Available Stock</label>
                        <input type="text" name="availableStock" placeholder="Available Rate" class="form-control"
                               id="" value="${eventCharge?.availableStock}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Status</label>
                        <g:select name="status12" from="${["Draft", "Publish", "Trash"]}"
                                  keys="${['draft', 'publish', 'trash']}"
                                  value="${eventCharge?.status}"
                                  noSelection="['': '-Choose status-']" class="form-control"/>
                    </div>

                    <div class="form-group col-sm-6">
                        <fieldset class="buttons">
                            <g:submitButton id="submit-btn" name="update" class="btn bttn"
                                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                        </fieldset>
                    </div>

                </g:form>

            </div>
        </div>
    </div>
</div>
<script>
    var eventType='${event.eventType}';
    if(eventType=='free'){
        $('#eventTypes').hide();
    }
</script>
</body>
</html>