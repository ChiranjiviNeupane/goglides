<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Event title: ${eventBooking?.event?.title}</h2>
    </div>

</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_content">
                <g:hasErrors bean="${this.promotion}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.promotion}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>


                <g:form method="POST" action="updateEventBooking">
                    <div class="row">
                        <input type="hidden" value="${eventBooking?.id}" name="id"/>
                    <div class="form-group col-sm-6">
                        <label for="">FirstName</label>
                        <input type="text" name="firstname" placeholder="First Name" class="form-control"
                               id="firstname" value="${eventBooking?.firstname}" required>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">LastName</label>
                        <input type="text" name="lastname" placeholder="Last Name" class="form-control"
                               id="lastname" value="${eventBooking?.lastname}" required>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Phone Number</label>
                        <input type="text" name="phone" placeholder="Phone Number" class="form-control"
                               id="phone" value="${eventBooking?.phone}" required>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Email</label>
                        <input type="text" name="email" placeholder="email " class="form-control"
                               id="email" value="${eventBooking?.email}" required>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Quantity</label>
                        <input type="text" name="quantity" placeholder="Quantity" class="form-control"
                               id="quantity" value="${eventBooking?.quantity}" required>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for=""> TicketRate</label>
                        <input type="text" name="" placeholder=" total amount " class="form-control"
                               id="ticketRate" value="${eventBooking?.eventCharge?.retailRate}" readonly>
                    </div>

                        <div class="form-group col-sm-6">
                            <label for="">Total Amount</label>
                            <input type="text" name="totalAmount" placeholder=" total amount " class="form-control"
                                   id="amount" value="${eventBooking?.totalAmount}" readonly>
                        </div>

                    <div class="form-group col-sm-6">
                        <label for="">Payment Status</label>
                        <g:select name="paymentStatus" from="['processing', 'pending', 'paid']"
                                  value="${eventBooking?.paymentStatus}"
                                  noSelection="['': '-Choose status-']" class="form-control" id="status"  />
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Payment Mode</label>
                        <g:select name="paymentMode" from="['cash', 'credit card']"
                                  value="${eventBooking?.paymentMode}"
                                  noSelection="['': '-Choose status-']" class="form-control" id="status"  />
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Status</label>
                        <g:select name="status12" from="['processing', 'confirmed', 'cancelled']"
                                  value="${eventBooking?.status}"
                                  noSelection="['': '-Choose status-']" class="form-control" id="status"  />
                    </div>
                    </div>

                    <div class="form-group col-sm-6">
                        <fieldset class="buttons">
                            <g:submitButton id="submit-btn" name="update" class="btn bttn"
                                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                        </fieldset>
                    </div>
                    </div>
                </g:form>


                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {

        var quantity;
        var price =${eventBooking?.eventCharge?.retailRate};

        if ($('#quantity').keypress) {
            $('#quantity').keyup(function () {
                quantity = $('#quantity').val();
                var total = price * quantity;
                $('#amount').val("")
                $('#amount').val(total)
            });
        }
        else {
            $('#amount').val(price)
        }
    });
</script>

</body>
</html>