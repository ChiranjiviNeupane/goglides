<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Create Event</h2>
    </div>

</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">

        <div class="x_content">
            <g:hasErrors bean="${this.promotion}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.promotion}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>
        </div>


        <g:form class="create-user-form form" method="post" action="eventDescriptionUpdate">

            <div class="row">
                <h3 class="col-md-10 wizard-header">Enter your event info</h3>
            </div>
            <input type="hidden" name="id" value="${event?.id}"/>

            <div class="form-group col-sm-6">
                <label for="">Title</label>
                <input type="text" name="title" placeholder="Title" class="form-control"
                       id="title" value="${event?.title}">
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">location</label>
                    <input type="text" name="location" placeholder="Enter location" class="form-control"
                           id="address" value="${event?.location}">
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Start Date</label>
                    <input id="startDate" type="text" name="startDate" class="form-control datepicker" required="required"
                           placeholder="Event Starting Date" value="${event?.startDate}"/>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">End Date</label>
                    <input id="endDate" type="text" name="endDate" class="form-control datepicker" required="required"
                           placeholder="" value="${event?.endDate}"/>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Event Starting Time</label>
                    <input id="" type="text" name="startTime" class="form-control timepicker" required="required"
                           placeholder="Event Starting Date" value="${event?.startTime}"/>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Event Ending Time</label>
                    <input id="" type="text" name="endTime" class="form-control timepicker" required="required"
                           placeholder="Event Ending Date" value="${event?.endTime}"/>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Description</label>
                    <textarea id="description" name="description" rows=5 cols=5 class="form-control description"
                              placeholder="Description" maxlength="7000">${raw(event?.description)}</textarea>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Status</label>
                    <g:select name="status12" from="${["Draft", "Publish", "Trash"]}"
                              keys="${['draft', 'publish', 'trash']}"
                              value="${event?.status}"
                              noSelection="['': '-Choose status-']" class="form-control"/>
                </div>

            </div>
            </div>



            <div class="form-group col-sm-6">
                <fieldset class="buttons">
                    <g:submitButton id="submit-btn" name="update" class="btn bttn"
                                    value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                </fieldset>
            </div>
        </g:form>

    </div>

</div>
<script>
    function initAutocomplete() {
// Create the autocomplete object, restricting the search to geographical
// location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['geocode']});

// When the user selects an address from the dropdown, populate the address
// fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
// Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

//for (var component in componentForm) {
//  document.getElementById(component).value = '';
// document.getElementById(component).disabled = false;
//}

// Get each component of the address from the place details
// and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                if (document.getElementById(addressType)) {

                    document.getElementById(addressType).value = val;
                }
            }
        }
    }

    function geolocate() {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                console.log(results[0].geometry.location.lat());
                console.log(results[0].geometry.location.lng());
                document.getElementById('geolocation_lat').value = results[0].geometry.location.lat();
                document.getElementById('geolocation_long').value = results[0].geometry.location.lng();

            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    if($('#startDate').length>0){
        $('#startDate').datetimepicker(
            {
                defaultDate:new Date(<g:formatDate format="yyyy-MM-dd" date="${event?.startDate}"/>),
                format: 'YYYY-MM-DD'
            }
        );
    }
    if($('#endDate').length>0){
        $('#endDate').datetimepicker(
            {
                defaultDate:new Date(<g:formatDate format="yyyy-MM-dd" date="${event?.endDate}"/>),
                format: 'YYYY-MM-DD'
            }
        );
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdQX0EJBVmrCnTWG4LKIO52ncxChz_AN0&libraries=places&callback=initAutocomplete"
        async defer></script>
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'description' );
    CKEDITOR.config.toolbar = [
        ['Styles','Format','Font','FontSize'],
        '/',
        ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-'],
        '/',
        ['NumberedList','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
    ] ;
    CKEDITOR.dataProcessor.writer.setRules( 'body',
        {
            indent : false,
            breakBeforeOpen : true,
            breakAfterOpen : false,
            breakBeforeClose : false,
            breakAfterClose : true
        });
</script>
</body>
</html>