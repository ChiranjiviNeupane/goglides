<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Manage Event</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(action: 'create')}" class="btn bttn">Add New Event</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Event Lists</h3>
            </div>

            <div class="x_content">
                <div class="row">
                    <g:if test="${events}">
                        <table class="table" id="mytable">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${events}" var="e">
                                <tr>
                                    <td>${e?.title}</td>
                                <td><g:formatDate format="yyyy-MM-dd" date="${e?.startDate}"/></td>
                                <td><g:formatDate format="yyyy-MM-dd" date="${e?.endDate}"/></td>
                                <td>${e?.status}    <td>
                                <a href="${createLink(uri: '/admin/event/view?id=' + e?.id)}" class="btn bttn">View</a>
                                <a href="${createLink(uri: '/admin/event/delete?id=' + e?.id)}"
                                   class="btn bttn">Delete</a>
                            </td></td>
                                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                        <div class="pagination pagination-custom pull-right">
                            <g:paginate class="" action="index" controller="event" total="${size ?: 0}"/>
                        </div>
                    </g:if>
                    <g:else>
                        No Event Available
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</body>
</html>