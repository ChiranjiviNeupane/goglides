<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Event   : ${event?.title}.</h2>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_content">
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <g:form class="create-user-form" method="post" action="saveSetting">
                <g:hasErrors bean="${this.event}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.event}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
                <div class="row">
                    <input type="hidden" name="id" value="${event?.id}">
                </div>
                <fieldset class="">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Feature Image</label>

                                <div id="feature-image"></div>
                            </div>

                            <div class="col-sm-offset-1 col-sm-3">
                                <label>Preview</label>

                                <div class="preview">
                                    <img src="${amazonS3Service?.sourceServerUrl() + eventMultimedia?.file}"
                                         class="img-responsive">
                                    <span class="preview-hvr"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>


                <div class="row">
                    <div class="col-md-4">
                        <label for="">Phone 1</label>
                        <input type="text" name="phone1" class="form-control required" value="${event?.phone1}"
                               placeholder="Phone 1" required="required">
                    </div>

                    <div class="col-md-4">
                        <label for="">Phone 2</label>
                        <input type="text" name="phone2" class="form-control" value="${event?.phone2}"
                               placeholder="Phone 2">
                    </div>

                    <div class="col-md-4">
                        <label for="">Phone 3</label>
                        <input type="text" name="phone3" class="form-control" value="${event?.phone3}"
                               placeholder="Phone 3">
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Email ID</label>
                    <input type="email" name="email" placeholder="Your email id" class="form-control"
                           id="email" value="${event?.email}">
                </div>

                </div><!-- end middle-wizard -->
                <div class="form-group col-sm-6">
                    <fieldset class="buttons">
                        <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                        value="${message(code: 'default.button.create.label', default: 'create')}"/>
                    </fieldset>
                </div>
            </g:form>
        </div><!-- end survey_container -->
    </div>
</div>
</div>
<g:render template="/layouts/file_upload" model=""/>

<style>
.preview-link {
    display: block;
    height: 100%;
    width: 100%;
}
</style>
<script type="text/javascript">
    $('#feature-image').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "event", action: "multimediaImagesUploadUpdateSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${event?.id},
                type: 'event'
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key", absolute: true)}',
                    {
                        filename: filename,
                        directory: 'event'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                });
                //                );
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "event", action: "multimediaImagesDelete")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 15000000,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    function delGallery(id, key) {
        $.ajax({
            url: "${createLink(action: 'multimediaImagesDelete')}",
            type: "post",
            data: {key: key, bucket: 'com-goglides-media'},
            success: function (data) {
                //console.log(data); //<-----this logs the data in browser's console
                $('.gallery-' + id).remove();
                if (data == 'success') {

                }
            },
            error: function (xhr) {
                // alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
    }
</script>
</body>
</html>
