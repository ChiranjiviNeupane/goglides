<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>User Query Ticket Info</h2>
    </div>

</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_content">
                <g:hasErrors bean="${this.queryTicket}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.queryTicket}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
            </div>

            <g:form action="save" method="POST" class="form form-horizontal form-label-left">

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Name</label>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" type="text" name="name" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Email</label>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="email" type="text" name="email" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Phone Number</label>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="phoneNumber" type="text" name="phoneNumber" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Description</label>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="message" name="message" rows=5 cols=5 class="form-control message"
                                  placeholder="message" maxlength="7000" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Status</label>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <g:select name="status" from="${["Draft", "Publish", "Trash"]}"
                                  keys="${['draft', 'publish', 'trash']}"
                                  value="${name}"
                                  noSelection="['': '-Choose status-']" class="form-control" id="status"
                                  required="required"/>
                    </div>
                </div>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                        <fieldset class="buttons">
                            <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                        </fieldset>

                    </div>
                </div>

            </g:form>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        // validate the comment form when it is submitted
        $(".form").validate({
            rules: {

                name: {
                    required: true

                },
                email: {
                    email: true,
                    required: true
                },
                phoneNumber: {
                    number: true,
                    required: true
                },
                message: {
                    required: true
                }

            },
            messages: {
                name: {
                    required: "please enter a name "
                },
                mobile: {
                    required: "please enter a mobile number",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                }

            }
        });
    });
</script>
</body>
</html>