%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Manage User Feedback/Query</h2>
    </div>
    <div class="title_right">
        <div class="pull-right">
                <a href="${createLink(action: 'create')}" class="btn bttn">Add User Query</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3> User Query Details</h3>
            </div>

            <div class="x_content">
                <div class="row">
                    <g:if test="${queryTicket}">
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <g:each in="${queryTicket}" var="q">
                            <tr>
                                <td>${q?.name}</td>
                                <td>${q?.email}</td>
                                <td>${q?.phoneNumber}</td>
                                <td>${q?.status}</td>
                                <td>
                                    <a href="${createLink(uri: '/admin/queryTicket/view?id=' + q?.id)}" class="btn bttn">Update</a>
                                    <a href="${createLink(uri: '/admin/queryTicket/delete?id=' + q?.id)}"
                                       class="btn bttn">Delete</a>
                                </td>
                            </tr>
                            </g:each>

                        </table>

                        <div class="pagination pagination-custom pull-right">
                            <g:paginate class="" action="index" controller="queryTicket" total="${size ?: 0}"/>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>