%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<div class="row">

    <div class="col-sm-12 col-xs-12">
        <div class="x_panel tile fixed_height_320 overflow_hidden">
            <div class="x_title title-bg">
                <g:form method="GET" controller="bookings" action="searchFlights" namespace="admin">
                    <div class="input-group">

                        <input type="text" name="bookingKeyword" value="${params?.bookingKeyword}"
                               placeholder="Search for..." class="form-control">

                        <div class="input-group-btn">

                            <g:select name="bookingSort" from="${siteDropdownService.getFilterOptions()}"
                                      noSelection="['': 'Flight Lists']"
                                      value="${params?.bookingSort}"
                                      class="text-capitalize form-control sort-opt"/>

                            <button type="submit" id="#" class="btn bttn"><i class="fa fa-filter"></i></button>
                        </div>

                    </div>
                </g:form>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:if test="${flightLists == 0}">
                    ${message(code: "default.norecords")}
                </g:if>
                <g:else>
                    <table id="table" class="table dash-table">
                        <thead>
                        <tr>
                            <th>Reference No.</th>
                            <th>Flight Date</th>
                            <th>Flight Time</th>
                            <th>Guests</th>
                            <th>Pickup Location</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${flightLists}">
                            <tr>
                                <td mdata-th="Reference No."><span class="bt-content">${it?.referenceNo}</span></td>
                                <td data-th="Flight Date"><span class="bt-content">${it?.bookingDate}</span></td>
                                <td data-th="Flight Time"><span class="bt-content">${it?.bookingTime}</span></td>
                                <td data-th="Guests"><span class="bt-content">${it?.noOfGuest}</span></td>
                                <td data-th="Location"><span class="bt-content">${it?.address}</span></td>
                                <td class="text-right" data-th="Action"><span class="bt-content">
                                    <a href="${createLink(uri: '/admin/bookings/edit?id=' + it?.id)}"><i
                                            class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="${createLink(uri: '/admin/bookings/view?id=' + it?.id)}"><i
                                            class="fa fa-eye" aria-hidden="true"></i></a>
                                </span></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:else>
            </div>
        </div>
    </div>
</div>