%{--Use CommonService--}%
<g:set var="commonService" bean="commonService"/>
%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<div class="col-sm-6 col-xs-12">
    <div class="x_panel tile fixed_height_320">
        <div class="x_title title-bg">
            <g:form method="GET" controller="businessDirectory" action="searchDirectory" namespace="admin">
                <div class="input-group">
                    <input type="text" name="keyword" value="" placeholder="Search for.." class="form-control">

                    <div class="input-group-btn">

                        <g:select name="sort" from="${siteDropdownService.getFilterOptions()}"
                                  noSelection="['': 'Latest Agents']"
                                  value="${params?.sort}"
                                  class="text-capitalize form-control sort-opt"/>

                        <button type="submit" id="#" class="btn bttn"><i class="fa fa-filter"></i></button>

                    </div>
                </div>
            </g:form>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <g:if test="${directoryLists == 0}">
                ${message(code: "default.norecords")}
            </g:if>
            <g:else>
                <table id="table" class="table order-table dash-table">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Agents</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${directoryLists}">
                        <tr>
                            <td><div class="img-box" style="background-image:url('');"></div></td>
                            <td>${it?.businessName}<br><small>${it?.addressLine1}</small></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </g:else>
        </div>
    </div>
</div>