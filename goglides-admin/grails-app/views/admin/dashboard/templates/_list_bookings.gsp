%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use CommonService--}%
<g:set var="commonService" bean="commonService"/>
%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<div class="col-sm-6 col-xs-12">
    <div class="x_panel tile fixed_height_320">
        <div class="x_title title-bg">
            <g:form method="GET" controller="bookings" action="searchBookings" namespace="admin">
                <div class="input-group">
                    <input type="text" name="keyword" value="" placeholder="Search for.." class="form-control">

                    <div class="input-group-btn">

                        <g:select name="sort" from="${siteDropdownService.getFilterOptions()}"
                                  noSelection="['': 'Latest Bookings']"
                                  value="${params?.sort}"
                                  class="text-capitalize form-control sort-opt"/>

                        <button type="submit" id="#" class="btn bttn"><i class="fa fa-filter"></i></button>

                    </div>
                </div>
            </g:form>
            <div class="clearfix"></div>
        </div>

        <div class="x_content" style="display: block;">
            <g:if test="${bookingLists == 0}">
                ${message(code: "default.norecords")}
            </g:if>
            <g:else>
                <table id="table" class="table order-table dash-table">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Package Name</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${bookingLists}">
                        <tr>
                            <td><div class="img-box"
                                     style="background-image:url('${listingService.getListingFeatureImage(it?.listing.id.toInteger())}');"></div>
                            </td>
                            <td><a href="/admin/bookings/view?id=${it?.id}">${it?.listing?.title}</a></td>
                            <td>${raw(priceService.getCurrencySymbol())}${it?.totalPrice}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </g:else>
        </div>
    </div>
</div>