%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use CommonService--}%
<g:set var="commonService" bean="commonService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="row top_tiles">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-cart-plus"></i></div>

                <div class="count">

                        ${stats?.lifetimeSales}

                </div>

                <h3>Lifetime Sales</h3>
            </div>
        </a>
    </div>

    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-shopping-cart"></i></div>

                <div class="count">
                        ${stats?.monthlySales}
                </div>

                <h3>This Month Sales</h3>
            </div>
        </a>
    </div>

    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-list-alt"></i></div>

                <div class="count">

                        ${stats?.totalListings}

                </div>

                <h3>Total Listing</h3>
            </div>
        </a>
    </div>

    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-plane"></i></div>

                <div class="count">

                        ${stats?.totalBookings}

                </div>

                <h3>Total Booking</h3>
            </div>
        </a>
    </div>
</div>

<div class="wrap-flight-data"></div>

<div class="row clearfix">
    <div class="wrap-booking-data"></div>
    <div class="wrap-agent-data"></div>
</div>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            $.ajax({
                url: "${createLink(uri: '/admin/dashboard/getFlightDetails')}",
                type: "post",
                success: function (data) {
                    $('.wrap-flight-data').html(data);
                },
                error: function (xhr) {
                    console.log(xhr.responseText); //<----when no data alert the err msg
                },
                complete: function (data) {

                }
            });
        }, 1000);

        setTimeout(function () {
            $.ajax({
                url: "${createLink(uri: '/admin/dashboard/getBookingDetails')}",
                type: "post",
                success: function (data) {
                    $('.wrap-booking-data').html(data);
                },
                error: function (xhr) {
                    console.log(xhr.responseText); //<----when no data alert the err msg
                },
                complete: function (data) {

                }
            });
        }, 2000);

        setTimeout(function () {
            $.ajax({
                url: "${createLink(uri: '/admin/dashboard/getBusinessDirectoryDetails')}",
                type: "post",
                success: function (data) {
                    $('.wrap-agent-data').html(data);
                },
                error: function (xhr) {
                    console.log(xhr.responseText); //<----when no data alert the err msg
                },
                complete: function (data) {

                }
            });
        }, 3000);
    });
</script>
</body>
</html>