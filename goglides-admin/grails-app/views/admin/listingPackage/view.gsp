%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use WishListService--}%
<g:set var="wishListSer" bean="wishListService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
<g:set var="pageTitle" value="${listingPackage?.tripTitle}"/>
<g:set var="pageSubtitle" value="detail view of listing package"/>
<!DOCTYPE html>
<head>
    <meta name="layout" content="dashboard-2017"/>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>${pageTitle}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(uri: '/admin/listing/create')}" class="btn bttn"><i
                    class="fa fa-plus"></i> Add New Listing</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="x_panel">
            <div class="x_title">
                <h2>${pageTitle} <small>${pageSubtitle}</small></h2>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab_content1" id="basic-tab" role="tab"
                               data-toggle="tab" aria-expanded="false">Basic</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content2" id="availability-tab" role="tab"
                               data-toggle="tab" aria-expanded="false">Availability</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content3" id="pricing-tab" role="tab"
                               data-toggle="tab" aria-expanded="false">Pricing</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content4" id="addons-tab" role="tab"
                               data-toggle="tab" aria-expanded="false">Addon Pricing</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content5" id="policy-tab" role="tab" data-toggle="tab"
                               aria-expanded="false">Policies</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="listing-view">
            <div id="myTabContent" class="tab-content listing-view">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                     aria-labelledby="basic-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Short Itinerary
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing-package/edit/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>

                            <div class="col-xs-12">${raw(listingPackage?.shortItinerary)}</div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Detail Itinerary
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing-package/edit/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>

                            <div class="col-xs-12">${raw(listingPackage?.detailItinerary)}</div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Includes
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing-package/edit/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>

                            <div class="col-xs-12">${raw(listingPackage?.tripIncludes)}</div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Excludes
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing-package/edit/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>

                            <div class="col-xs-12">${raw(listingPackage?.tripExcludes)}</div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                     aria-labelledby="availability-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Availability
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing-package/edit-package-options/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>

                            <div class="col-sm-12">
                                <label>Trip availability date</label>
                                <g:if test="${listingAvailability?.availabilityType == 'date-single'}">
                                    <p><g:formatDate date="${listingAvailability?.fromDate}" type="date"
                                                     style="MEDIUM"/></p>
                                </g:if>
                                <g:else>
                                    <p><g:formatDate date="${listingAvailability?.fromDate}" type="date"
                                                     style="MEDIUM"/> - <g:formatDate
                                            date="${listingAvailability?.toDate}" type="date" style="MEDIUM"/></p>
                                </g:else>
                            </div>

                            <div class="col-xs-12"><hr/></div>

                            <div class="col-sm-12">
                                <label>Trip availability day(s)</label>

                                <div class="table-responsive">
                                    <table id="" class="table table-bordered">
                                        <tr>
                                            <th class="text-center">Days</th>
                                            <th class="text-center">Monday</th>
                                            <th class="text-center">Tuesday</th>
                                            <th class="text-center">Wednesday</th>
                                            <th class="text-center">Thursday</th>
                                            <th class="text-center">Friday</th>
                                            <th class="text-center">Saturday</th>
                                            <th class="text-center">Sunday</th>
                                        </tr>
                                        <tr>
                                            <th data-th="Days" class="text-center">Availability</th>
                                            <td data-th="Mon"
                                                class="text-center">${raw(listingAvailabilityDays?.monday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                                            <td data-th="Tue"
                                                class="text-center">${raw(listingAvailabilityDays?.tuesday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                                            <td data-th="Wed"
                                                class="text-center">${raw(listingAvailabilityDays?.wednesday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                                            <td data-th="Thu"
                                                class="text-center">${raw(listingAvailabilityDays?.thursday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                                            <td data-th="Fri"
                                                class="text-center">${raw(listingAvailabilityDays?.friday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                                            <td data-th="Sat"
                                                class="text-center">${raw(listingAvailabilityDays?.saturday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                                            <td data-th="Sun"
                                                class="text-center">${raw(listingAvailabilityDays?.sunday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="col-xs-12"><hr></div>

                            <div class="col-sm-6">
                                <label>Trip Duration</label>
                                <ul>
                                    <g:if test="${listingDurations}">
                                        <g:each in="${listingDurations}">
                                            <g:set var="format" value="${listingService.formatDuration(it?.duration)}"/>
                                            <li>${format.d}</li>
                                        </g:each>
                                    </g:if>
                                </ul>
                            </div>

                            <div class="col-sm-6">
                                <label>Trip Schedule</label>
                                <g:if test="${listingSchedules}">
                                    <g:each in="${listingSchedules}">
                                        <li>${listingService.formatSchedule(it?.schedule)}</li>
                                    </g:each>
                                </g:if>
                            </div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                     aria-labelledby="availability-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Availability
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing-package/edit-package-price/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>
                        </div>
                        <g:if test="${packagePrice}">
                            <g:each in="${packagePrice}">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label>Age Group</label>

                                        <p>${it?.ageGroup}</p>
                                    </div>
                                    <g:if test="${it?.type}">
                                        <div class="col-sm-2">
                                            <label>Glide Type</label>

                                            <p>${it?.type}</p>
                                        </div>
                                    </g:if>
                                    <g:if test="${it?.duration}">
                                        <div class="col-sm-2">
                                            <label>Duration</label>
                                            <g:set var="format" value="${listingService.formatDuration(it?.duration)}"/>

                                            <p>${format.d}</p>
                                        </div>
                                    </g:if>
                                    <g:if test="${it?.schedule}">
                                        <div class="col-sm-2">
                                            <label>Schedule</label>

                                            <p>${it?.schedule}</p>
                                        </div>
                                    </g:if>
                                    <div class="col-sm-2">
                                        <label>Wholesale Rate</label>

                                        <p>${raw(priceService.getFormattedPrice(it?.wholeSaleRate))}</p>
                                    </div>

                                    <div class="col-sm-2">
                                        <label>Retail Rate</label>

                                        <p>${raw(priceService.getFormattedPrice(it?.retailRate))}</p>
                                    </div>
                                </div>
                            </g:each>
                        </g:if>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content4"
                     aria-labelledby="addons-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Add-on Services <a class="pull-right" data-toggle="tooltip" title="Edit"
                                                       href="${createLink(uri: '/admin/listing-package/edit-package-addons/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>

                            <div class="col-sm-6">
                                <label>Multimedia</label>
                                <g:if test="${listingAddons?.includedMultimedia == 'no'}">
                                    <g:each in="${listingAddonMultimedia}">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <strong>Age Group</strong>

                                                <p>${it?.ageGroup}</p>
                                            </div>

                                            <div class="col-sm-4">
                                                <strong>Wholesale Rate</strong>

                                                <p>${raw(priceService.getFormattedPrice(it?.wholesaleRate))}</p>
                                            </div>

                                            <div class="col-sm-3">
                                                <strong>Retail Rate</strong>

                                                <p>${raw(priceService.getFormattedPrice(it?.retailRate))}</p>
                                            </div>
                                        </div>
                                    </g:each>
                                </g:if>
                                <g:else>
                                    <p>Multimedia Cost included on above rate</p>
                                </g:else>
                            </div>

                            <div class="col-sm-6">
                                <label>Trasport</label>
                                <g:if test="${listingAddons?.includedTransport == 'no'}">
                                    <g:each in="${listingAddonTransport}">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <strong>Age Group</strong>

                                                <p>${it?.ageGroup}</p>
                                            </div>

                                            <div class="col-sm-4">
                                                <strong>Wholesale Rate</strong>

                                                <p>${raw(priceService.getFormattedPrice(it?.wholesaleRate))}</p>
                                            </div>

                                            <div class="col-sm-3">
                                                <strong>Retail Rate</strong>

                                                <p>${raw(priceService.getFormattedPrice(it?.retailRate))}</p>
                                            </div>
                                        </div>
                                    </g:each>
                                </g:if>
                                <g:else>
                                    <p>Transportation Cost included on above rate</p>
                                </g:else>
                            </div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="policy-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Policy <a class="pull-right" data-toggle="tooltip" title="Edit"
                                              href="${createLink(uri: '/admin/listing-package/edit-package-policy/', params: ['id': listingPackage?.packageCode])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>
                        </div>
                        <g:if test="${listingAddons?.isDiscountApplicable == 'yes'}">
                            <g:if test="${discountPolicies}">
                                <g:each in="${discountPolicies}" var="policy" status="i">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Group Policy</strong>

                                            <p>Policy ${i + 1}</p>
                                        </div>

                                        <div class="col-sm-3">
                                            <strong>Group Size</strong>

                                            <p>${policy?.discountFrom} - ${policy?.discountTo}</p>
                                        </div>

                                        <div class="col-sm-3">
                                            <strong>Discount in %</strong>

                                            <p>${policy?.discountPercent}</p>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </g:if>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <g:render template="/admin/listingPackage/templates/summary_actions"
                  model="['listingPackage': listingPackage]"/>
        <g:render template="/admin/listingPackage/templates/summary_listing" model="['listing': listing]"/>
        <g:render template="/admin/listingPackage/templates/summary_listingpackage"
                  model="['listingPackage': listingPackage]"/>
    </div>
</div>
</body>
</html>