%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use WishListService--}%
<g:set var="wishListSer" bean="wishListService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
<g:set var="pageTitle" value="${listingPackage?.tripTitle}"/>
<g:set var="pageSubtitle" value="detail view of listing package"/>
<!DOCTYPE html>
<head>
    <meta name="layout" content="dashboard-2017"/>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>${pageTitle}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(uri: '/admin/listing/create')}" class="btn bttn"><i
                    class="fa fa-plus"></i> Add New Listing</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="x_panel">
            <div class="x_title">
                <h2>${pageTitle} <small>${pageSubtitle}</small></h2>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:render template="/admin/listingPackage/templates/form_package_options"
                          model="[listing: listing]"/>
                <div style="display: none;">
                    <input type="hidden" value="0" class="duration-count">
                    <input type="hidden" value="0" class="schedule-count">


                    <div class="trip-duration-wrap">
                        <div class="multi-field">
                            <input type="hidden" name="durationId"/>

                            <input type="hidden" name="duration" class="duration"/>

                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-2">
                                    <label for="">Week</label>
                                    <input type="text" name="week" placeholder="00" onblur="calc()"
                                           class="form-control week">
                                </div>

                                <div class="col-xs-12 col-sm-2">
                                    <label for="">Days</label>
                                    <input type="text" name="days" placeholder="00" onblur="calc()"
                                           class="form-control days">
                                </div>

                                <div class="col-xs-12 col-sm-2">
                                    <label for="">Hours</label>
                                    <input type="text" name="hours" placeholder="00" onblur="calc()"
                                           class="form-control hours">
                                </div>

                                <div class="col-xs-12 col-sm-2">
                                    <label for="">Minutes</label>
                                    <input type="text" name="minutes" placeholder="00" onblur="calc()"
                                           class="form-control minutes">
                                </div>

                                <div class="col-xs-12 col-sm-3">
                                    <label for="">&nbsp;</label>
                                    <button type="button" class="btn btn-danger remove-duration form-control">
                                        <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="trip-schedule-wrap">
                        <div class="multi-field clearfix">
                            <input type="hidden" name="scheduleId"/>

                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-9">
                                    <input type="time" name="schedule" onblur="formatScheduleCount()"
                                           placeholder="Trip Schedule 10:00 "
                                           class="form-control schedulevalue form-group">
                                </div>

                                <div class="col-xs-12 col-sm-3">
                                    <button type="button" class="btn btn-danger remove-schedule form-control">
                                        <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <g:render template="/admin/listingPackage/templates/summary_listing" model="['listing': listing]"/>
        <g:render template="/admin/listingPackage/templates/summary_listingpackage"
                  model="['listingPackage': listingPackage]"/>
    </div>
</div>

<script>
    function calc(index) {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        var week = $('.week-' + index).val();
        var days = $('.days-' + index).val();
        var hours = $('.hours-' + index).val();
        var minutes = $('.minutes-' + index).val();

        var errMessage = "";
        if (week == '')
            week = "00";
        if (days == '')
            days = "00";
        if (days > 6) {
            errMessage = "Invalid Days"
            toastr.error(errMessage);
        }
        if (hours == '')
            hours = "00";
        if (hours > 23) {
            errMessage = "Invalid Hours"
            toastr.error(errMessage);
            return false;
        }
        if (minutes == '')
            minutes = "00";
        if (minutes > 60) {
            errMessage = "Invalid Minutes"
            toastr.error(errMessage);
            return false;
        }
        if (!errMessage) {
            var duration = week + ":" + days + ":" + hours + ":" + minutes;
            if (duration == "00:00:00:00") {
                errMessage = "duration cannot be empty";
                toastr.error(errMessage);
                return false;
            }
            else {
                $(".duration-" + index).find('.duration').val(duration);
            }
            return false;
        }
    }

    function calcOld() {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        var week = $('.week').val();
        var days = $('.days').val();
        var hours = $('.hours').val();
        var minutes = $('.minutes').val();
        var errMessage = "";
        if (week == '')
            week = "00";
        if (days == '')
            days = "00";
        if (days > 6) {
            errMessage = "Invalid Days"
            toastr.error(errMessage);
        }
        if (hours == '')
            hours = "00";
        if (hours > 23) {
            errMessage = "Invalid Hours"
            toastr.error(errMessage);
            return false;
        }
        if (minutes == '')
            minutes = "00";
        if (minutes > 60) {
            errMessage = "Invalid Minutes"
            toastr.error(errMessage);
            return false;
        }
        if (!errMessage) {
            var duration = week + ":" + days + ":" + hours + ":" + minutes;
            if (duration == "00:00:00:00") {
                errMessage = "duration cannot be empty";
                toastr.error(errMessage);
                return false;
            }
            else {
                $('.duration').val(duration);
            }
            return false;

        }
    }

    $(document).ready(function () {
        $('.form').submit(function () {
            var GlideDurationRows = $('.trip-duration .multi-field').length;
            var GlideScheduleRows = $('.trip-schedule .multi-field').length;
            if (GlideDurationRows <= 0 || GlideScheduleRows <= 0) {
                $('.message').html('<div class="alert alert-danger alert-flat fade in alert-dismissable"><a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a><div class="message" role="status"><i class="fa fa-close"></i> At least one duration and schedule must be added.</div></div>')
                $('body').animate({scrollTop: 0}, "slow");
                return false
            }
        });
        $('#to').val('<g:formatDate format="yyyy-MM-dd" date="${listingAvailability?.toDate}"/>');
    });
    $('body').on('change', '#availabilityType', function () {
        var value = $(this).val();
        if (value == 'date-single') {
            $('.date-single-wrap').show();
            $('.date-range-wrap').hide();
        } else if (value == 'date-range') {
            $('.date-single-wrap').hide();
            $('.date-range-wrap').show();
        }
        else {
            $('.date-single-wrap').hide();
            $('.date-range-wrap').hide();
        }
    });

    $('body').on('change', '#unAvailabilityType', function () {
        var value = $(this).val();
        if (value == 'date-single') {
            $('.date-single-wrap-unavailability').show();
            $('.date-range-wrap-unavailability').hide();
        } else if (value == 'date-range') {
            $('.date-single-wrap-unavailability').hide();
            $('.date-range-wrap-unavailability').show();
        }
        else {
            $('.date-single-wrap-unavailability').hide();
            $('.date-range-wrap-unavailability').hide();
        }
    });

    $('body').on('click', '#add-trip-duration', function () {
        var GlideDurationRows = $('.trip-duration .multi-field').length;
        if (GlideDurationRows < 10) {
            var durationCount = parseInt($('.duration-count').val()) + 1;
            $('.duration-count').val(durationCount);
            var template = $('.trip-duration-wrap .multi-field').clone().appendTo('.trip-duration').addClass('duration-' + durationCount);
            $('.duration-' + durationCount).find('.week').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('week-' + durationCount);
            $('.duration-' + durationCount).find('.days').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('days-' + durationCount);
            $('.duration-' + durationCount).find('.hours').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('hours-' + durationCount);
            $('.duration-' + durationCount).find('.minutes').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('minutes-' + durationCount);
        } else {
            alert("You have reach the maximum limit 10");
        }
    });

    $('body').on('click', '.remove-duration', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='durationId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeDuration', controller: 'listingPackage', namespace: 'admin')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });

    $('body').on('click', '#add-trip-schedule', function () {
        var GlideScheduleRows = $('.trip-schedule .multi-field').length;
        if (GlideScheduleRows < 10) {
            var scheduleCount = parseInt($('.schedule-count').val()) + 1;
            $('.schedule-count').val(scheduleCount);
            var template = $('.trip-schedule-wrap .multi-field').clone().appendTo('.trip-schedule').addClass('schedule-' + scheduleCount);
            $('.schedule-' + scheduleCount).find('.schedulevalue').attr({
                'onblur': 'formatScheduleCount(' + scheduleCount + ')'
            }).addClass('schedulevalue-' + scheduleCount);

        } else {
            alert("You have reach the maximum limit 10");
        }
    });

    $('body').on('click', '.remove-schedule', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='scheduleId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeSchedule', controller: 'listingPackage', namespace: 'admin')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });
</script>

<script>
    if ($('#singleStartDate').length > 0) {
        $('#singleStartDate').datetimepicker({
                defaultDate: new Date(<g:formatDate format="yyyy-MM-dd" date="${listingAvailability?.unAvailabilityFromDate}"/>),
                format: 'YYYY-MM-DD'
            }
        );
    }

    if ($('#startDate').length > 0) {
        $('#startDate').datetimepicker({

                defaultDate: new Date(<g:formatDate format="yyyy-MM-dd" date="${listingAvailability?.unAvailabilityFromDate}"/>),
                format: 'YYYY-MM-DD'
            }
        );
    }

    if($('#endDate').length> 0){
        $('#endDate').datetimepicker({
            defaultDate: new Date(<g:formatDate format="yyyy-MM-dd" date="${listingAvailability?.unAvailabilityToDate}"/>),
            format: 'YYYY-MM-DD'
        });
    }


</script>
</body>
</html>
