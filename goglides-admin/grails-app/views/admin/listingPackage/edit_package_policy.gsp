%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use WishListService--}%
<g:set var="wishListSer" bean="wishListService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
<g:set var="pageTitle" value="${listingPackage?.tripTitle}"/>
<g:set var="pageSubtitle" value="detail view of listing package"/>
<!DOCTYPE html>
<head>
    <meta name="layout" content="dashboard-2017"/>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>${pageTitle}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(uri: '/admin/listing/create')}" class="btn bttn"><i
                    class="fa fa-plus"></i> Add New Listing</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="x_panel">
            <div class="x_title">
                <h2>${pageTitle} <small>${pageSubtitle}</small></h2>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:render template="/admin/listingPackage/templates/form_package_policy"
                          model="[listing: listing]"/>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <g:render template="/admin/listingPackage/templates/summary_listing" model="['listing': listing]"/>
        <g:render template="/admin/listingPackage/templates/summary_listingpackage"
                  model="['listingPackage': listingPackage]"/>
    </div>
</div>
<script>
    $(document).ready(function () {
        // validate the comment form when it is submitted
        $(".form").validate({
            rules: {},
            messages: {}
        });
    });
</script>
</body>
</html>
