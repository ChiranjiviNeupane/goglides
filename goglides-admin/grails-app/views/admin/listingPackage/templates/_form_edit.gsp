<g:form resource="${this.listingPackage}" class="form" action="update" controller="listingPackage" namespace="admin">
    <g:hasErrors bean="${this.listingPackage}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listingPackage}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="listingPackageReferenceNo"
                   value="${listingPackage?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Trip Title</label>
                    <input type="text" name="tripTitle" value="${listingPackage?.tripTitle}"
                           placeholder="Trip Title"
                           class="form-control" id="" required="required">

                    <p>e.g. Cross Country, Tandem Flying</p>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Your Trip Code (optional)</label>
                    <input type="text" name="tripCode" value="${listingPackage?.tripCode}"
                           placeholder="Trip Code"
                           class="form-control" id="">

                    <p>If you use an internal trip/product code in your office, add it here or leave it blank</p>
                </div>

                <div class="form-group col-sm-12">
                    <label for="">Short Itinerary</label>
                    <textarea id="shortItinerary" name="shortItinerary" rows=10 cols=5
                              class="form-control shortItinerary"
                              placeholder="Short Itinerary of your listing"
                              maxlength="9000">${listingPackage?.shortItinerary}</textarea>

                    <p>Short itinerary of your trip</p>
                </div>

                <div class="form-group col-sm-12">
                    <label for="">Detail Itinerary (optional)</label>
                    <textarea id="detailItinerary" name="detailItinerary" rows=10 cols=5
                              class="form-control detailItinerary"
                              placeholder="Detail Itinerary of your listing"
                              maxlength="9000">${listingPackage?.detailItinerary}</textarea>

                    <p>This itinerary will be emailed to customer after confirmation of booking.
                    This will not be displayed while booking.</p>
                </div>

                <div class="form-group col-sm-6 col-xs-12">
                    <label for="">Includes</label>
                    <textarea id="tripIncludes" rows=10 cols=5 class="form-control tripIncludes"
                              name="tripIncludes"
                              placeholder="Detail Itinerary of your listing"
                              maxlength="9000">${listingPackage?.tripIncludes}</textarea>

                    <p>Add inclusions of this trip. (Bullet format)</p>
                </div>

                <div class="form-group col-sm-6 col-xs-12">
                    <label for="">Excludes</label>
                    <textarea id="tripExcludes" rows=10 cols=5 class="form-control tripExcludes"
                              name="tripExcludes"
                              placeholder="Detail Itinerary of your listing"
                              maxlength="9000">${listingPackage?.tripExcludes}</textarea>

                    <p>Add price not cover by this trip. (Bullet format)</p>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/set-rules', params: [id: listing?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>