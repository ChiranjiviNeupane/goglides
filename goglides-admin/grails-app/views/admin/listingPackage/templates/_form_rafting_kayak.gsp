<g:form resource="${this.listingPackage}" class="form" action="updatePackagePrice" controller="listingPackage"
        namespace="admin">
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 4.2 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="listingPackageReferenceNo" value="${listingPackage?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-12">
                    <h3>A) Set Pricing</h3>

                    <p class="req"></p>
                    <strong>Per Person (Every people charged separately)</strong>

                    <div class="price-section">
                        <div class="type-para">

                            <div class="form-group">
                                <label>Does your trip have different schedule price?</label>

                                <div>
                                    <label>
                                        <input type="radio" value="yes" name="schedulePrice"
                                               class="Form-label-radio trip-schedule-price"
                                               target="5"
                                               checked="${listingPriceType?.pricingType == 'scheduled'}"
                                               required="required">
                                        <span class="Form-label-text">Yes</span>
                                    </label>
                                </div>

                                <div>
                                    <label>
                                        <input type="radio" value="no" name="schedulePrice"
                                               class="Form-label-radio trip-schedule-price" target="6"
                                               checked="${listingPriceType?.pricingType != 'scheduled'}"
                                               required="required">
                                        <span class="Form-label-text">No</span>
                                    </label>
                                </div>
                            </div>

                            <div class="price-list-wrap clearfix">
                                <g:if test="${!listingPrices.isEmpty()}">
                                    <g:each in="${listingPrices}">
                                        <div class="multi-field">
                                            <input type="hidden" name="listingPriceId" value="${it?.id}">

                                            <div class="row form-group">
                                                <div class="col-sm-2 col-xs-6">
                                                    <label>Age Group</label>
                                                    <g:select name="ageGroup"
                                                              from="['infant', 'child', 'adult', 'senior']"
                                                              value="${it?.ageGroup}"
                                                              noSelection="['': 'Select age group']"
                                                              class="form-control"/>
                                                </div>

                                                <div class="col-sm-2 col-xs-6 price-schedule" style="display:none">
                                                    <label>Schedule</label>
                                                    <g:select name="schedule"
                                                              from="${listingSchedules}"
                                                              value="${it?.schedule}"
                                                              optionKey="schedule"
                                                              optionValue="schedule"
                                                              noSelection="['': 'Select glide type']"
                                                              class="form-control"/>
                                                </div>

                                                <div class="col-sm-2 col-xs-6">
                                                    <label>Whole sale rate</label>
                                                    <input type="text" name="wholeSaleRate" placeholder=""
                                                           class="form-control wholesale-rate"
                                                           onblur="isNumber($(this))" value="${it?.wholeSaleRate}"
                                                           requried="required">
                                                </div>

                                                <div class="col-sm-1 col-xs-6">
                                                    <label>Retail rate</label>
                                                    <input type="text" name="retailRate" placeholder=""
                                                           class="form-control retail-rate" onblur="isNumber($(this))"
                                                           value="${it?.retailRate}" required="required">
                                                </div>

                                                <div class="col-sm-1 col-xs-6">
                                                    <label for="">&nbsp;</label>
                                                    <button type="button"
                                                            class="btn btn-danger remove-price form-control">
                                                        <i class="fa fa-close" aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </g:each>
                                </g:if>
                            </div>

                            <div class="clearfix">
                                <button type="button" class="bttn btn add-price" disabled="disabled"><i
                                        class="fa fa-plus"
                                        aria-hidden="true"></i> Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing-package/edit-package-options/'+ listingPackage?.packageCode)}';">Previous</button>
                <button type="submit" class="btn btn-next bttn" id="submit-btn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<div class="price-template" style="display:none">
    <div class="multi-field-wrapper">
        <div class="multi-fields">
            <div class="multi-field">
                <input type="hidden" name="listingPriceId" value="">

                <div class="row form-group">
                    <div class="col-sm-2 col-xs-6">
                        <label>Age Group</label>
                        <g:select name="ageGroup"
                                  from="['infant', 'child', 'adult', 'senior']"
                                  value="${listingPrice?.ageGroup}"
                                  noSelection="['': 'Select age group']"
                                  class="form-control"/>
                    </div>

                    <div class="col-sm-2 col-xs-6 price-schedule" style="display:none">
                        <label>Schedule</label>
                        <g:select name="schedule"
                                  from="${listingSchedules}"
                                  value="${listingPrice?.type}"
                                  optionKey="schedule"
                                  optionValue="schedule"
                                  noSelection="['': 'Select glide type']"
                                  class="form-control"/>
                    </div>

                    <div class="col-sm-2 col-xs-6">
                        <label>Whole sale rate</label>
                        <input type="text" name="wholeSaleRate" placeholder=""
                               class="form-control wholesale-rate" onblur="isNumber($(this))">
                    </div>

                    <div class="col-sm-1 col-xs-6">
                        <label>Retail rate</label>
                        <input type="text" name="retailRate" placeholder=""
                               class="form-control retail-rate" onblur="isNumber($(this))">
                    </div>

                    <div class="col-sm-1 col-xs-6">
                        <label for="">&nbsp;</label>
                        <button type="button" class="btn btn-danger remove-price form-control">
                            <i class="fa fa-close" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>

            </div>
        </div>
        add
    </div>
</div>
<script>
    $(document).ready(function () {
        // validate the comment form when it is submitted
        $(".form").validate({
            rules: {},
            messages: {}
        });

        var priceType = '${listingPriceType?.pricingType}';
        if (priceType != 'scheduled') {
            $('.add-price').removeAttr('disabled');
        } else {
            $('.price-schedule').show();
        }

        $('.form').submit(function () {
            var priceRows = $('.price-list-wrap .multi-field').length;
            if (priceRows <= 0) {
                $('.message').html('<div class="alert alert-danger alert-flat fade in alert-dismissable"><a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a><div class="message" role="status"><i class="fa fa-close"></i> At least one listing price must be added.</div></div>')
                $('body').animate({scrollTop: 0}, "slow");
                return false
            }
        })
    });

    function isNumber(input) {
        var val = input.val();
        $("#submit-btn").attr('disabled', 'disabled');
        if ($.isNumeric(val)) {
            $("#submit-btn").removeAttr('disabled');
            return true;
        } else {
            alert('Invalid amount');
            return false;
        }
    }

    $('body').on('change', '.trip-schedule-price', function () {
        var value = $(this).val();
        if (value === 'yes') {
            $('.price-schedule').show();
        } else {
            $('.price-schedule').hide();
        }
        $('.add-price').removeAttr('disabled');
    });

    $('body').on('click', '.add-price', function () {
        var priceRows = $('.price-list-wrap .multi-field').length;
        if (priceRows < 30) {
            var template = $('.price-template .multi-field').clone().appendTo('.price-list-wrap');
            $('.price-list-wrap').find('.wholesale-rate').attr('required', 'required');
            $('.price-list-wrap').find('.retail-rate').attr('required', 'required');
        } else {
            alert("You have reach the maximum limit 30");
        }
    });

    $('body').on('click', '.remove-price', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='listingPriceId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removePrice', controller: 'listingPackage', namespace: 'admin')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });
</script>