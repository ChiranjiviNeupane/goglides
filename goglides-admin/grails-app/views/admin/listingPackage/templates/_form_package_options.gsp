<g:form resource="${this.listingPackage}" class="form" action="updatePackageOptions"
        controller="listingPackage" namespace="admin">
    <g:hasErrors bean="${this.listingPackage}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listingPackage}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${this.listingAvailabilityDays}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listingPackage}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${this.listingAvailability}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listingPackage}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 4.1 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="listingPackageReferenceNo"
                   value="${listingPackage?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Trip availability day(s)</label>

                    <div class="row">
                        <div class="col-sm-1">
                            <label>
                                Mon
                                <g:checkBox name="monday"
                                            checked="${listingAvailabilityDays?.monday == true}"
                                            class="Form-label-checkbox"/>

                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Tue
                                <g:checkBox name="tuesday"
                                            checked="${listingAvailabilityDays?.tuesday == true}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Wed
                                <g:checkBox name="wednesday"
                                            checked="${listingAvailabilityDays?.wednesday == true}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Thu
                                <g:checkBox name="thursday"
                                            checked="${listingAvailabilityDays?.thursday == true}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Fri
                                <g:checkBox name="friday"
                                            checked="${listingAvailabilityDays?.friday == true}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Sat
                                <g:checkBox name="saturday"
                                            checked="${listingAvailabilityDays?.saturday == true}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Sun
                                <g:checkBox name="sunday"
                                            checked="${listingAvailabilityDays?.sunday == true}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Trip availability date(s)</label>
                    <g:select name="availabilityType"
                              from="['date-single', 'date-range']"
                              value="${listingAvailability?.availabilityType}"
                              noSelection="['': 'Select date range']"
                              required="required"
                              class="form-control form-group"/>

                    <div class="row">
                        <div class="col-sm-12 date-single-wrap"
                             style="display: ${(listingAvailability?.availabilityType == 'date-single') ? 'block' : 'none'};">
                            <label>Single Date</label>

                            <div class="input-group date" id="datetimepicker1">
                                <input type="text" name="singleFrom"
                                       value="${listingAvailability?.fromDate}"
                                       class="form-control datepicker">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>

                        <div class="date-range-wrap"
                             style="display: ${(listingAvailability?.availabilityType == 'date-range') ? 'block' : 'none'};">
                            <div class="col-sm-12"><label>Date Range</label></div>

                            <div class="col-sm-6">
                                <label>From</label>

                                <div class="input-group date">
                                    <input type="text" name="fromDate"
                                           value="${listingAvailability?.fromDate}"
                                           id="from" class="form-control datepicker">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <label>To</label>

                                <div class="input-group date">

                                    <input type="text" name="toDate"
                                           value="${listingAvailability?.toDate}" id="to"
                                           class="form-control datepicker">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                </div>

                </div>
                <div class="form-group col-sm-6">
                    <label for="">Trip unavailable date(s)</label>
                    <g:select name="unAvailabilityType"
                              from="['date-single','date-range']"
                              value="${listingAvailability?.unAvailabilityType}"
                              noSelection="['': 'Select date range']"
                              class="form-control form-group"/>

                    <div class="row">
                        <div class="col-sm-12 date-single-wrap-unavailability"
                             style="display: ${(listingAvailability?.unAvailabilityType == 'date-single') ? 'block' : 'none'};">
                            <label>Single Date</label>

                            <div class="input-group date" id="datetimepicker2">
                                <input type="text" name="unAvailabilitySingleDate" value="${listingAvailability?.unAvailabilityFromDate}"
                                       class="form-control datepicker" id="singleStartDate">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>

                        <div class="date-range-wrap-unavailability"
                             style="display: ${(listingAvailability?.unAvailabilityType == 'date-range') ? 'block' : 'none'};">
                            <div class="col-sm-12"><label>Date Range</label></div>

                            <div class="col-sm-6">
                                <label>From</label>
                                <div class="input-group date">
                                    <input type="text" name="unAvailabilityFromDate" value="${listingAvailability?.unAvailabilityFromDate}"
                                           id="startDate" class="form-control datepicker">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>To</label>
                                <div class="input-group date">
                                    <input type="text" name="unAvailabilityToDate" value="${listingAvailability?.unAvailabilityToDate}"
                                           id="endDate" class="form-control datepicker">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="form-group col-sm-6">
                    <label>Trip Durations</label>

                    <p>Week/Days/Hours/Minutes( For multiple duration click on add option )</p>

                    <div class="multi-field-wrapper">
                        <div class="multi-fields trip-duration">
                            <g:if test="${!listingDurations?.isEmpty()}">
                                <g:each in="${listingDurations}">
                                    <div class="multi-field">
                                        <input type="hidden" name="durationId" value="${it?.id}"/>
                                        <g:set var="duration"
                                               value="${listingService.splitDuration(it.duration)}"/>
                                        <g:set var="format"
                                               value="${listingService.formatDuration(it.duration)}"/>
                                        <p>${format.d}</p>

                                        <input type="hidden" name="duration" value="${it.duration}">

                                        <div class="row form-group">
                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Week</label>
                                                <input type="text" name="week" placeholder="00"
                                                       onblur="calcOld()"
                                                       value="${duration.w}" class="form-control week"
                                                       id="week">
                                            </div>

                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Days</label>
                                                <input type="text" name="days" placeholder="00"
                                                       onblur="calcOld()"
                                                       value="${duration.d}" class="form-control days"
                                                       id="days">
                                            </div>

                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Hours</label>
                                                <input type="text" name="hours" placeholder="00"
                                                       onblur="calcOld()"
                                                       value="${duration.h}" class="form-control hours"
                                                       id="hours">
                                            </div>

                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Minutes</label>
                                                <input type="text" name="minutes" placeholder="00"
                                                       onblur="calcOld()"
                                                       value="${duration.m}"
                                                       class="form-control minutes" id="minutes">
                                            </div>

                                            <div class="col-xs-12 col-sm-3">
                                                <label for="">&nbsp;</label>
                                                <button type="button"
                                                        class="btn btn-danger remove-duration form-control">
                                                    <i class="fa fa-close"
                                                       aria-hidden="true"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                        <button type="button" class="bttn btn add-field" id="add-trip-duration">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Trip Schedules</label>

                    <p>Departure time ( For multiple schedule click on add option )</p>

                    <div class="multi-field-wrapper">
                        <div class="multi-fields trip-schedule">
                            <g:if test="${!listingSchedules?.isEmpty()}">
                                <g:each in="${listingSchedules}">
                                    <div class="multi-field clearfix">
                                        <input type="hidden" name="scheduleId" value="${it?.id}"/>

                                        <div class="row form-group">
                                            <div class="col-xs-12 col-sm-9">
                                                <input type="time" name="schedule"
                                                       onblur="formatSchedule()"
                                                       value="${it?.schedule}"
                                                       placeholder="Trip Schedule"
                                                       class="form-control form-group schedulevalue">
                                            </div>

                                            <div class="col-xs-12 col-sm-3">
                                                <button type="button"
                                                        class="btn btn-danger remove-schedule form-control">
                                                    <i class="fa fa-close"
                                                       aria-hidden="true"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                    </div>
                    <button type="button" class="bttn btn add-field" id="add-trip-schedule">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing-package/edit/'+listingPackage?.packageCode)}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>