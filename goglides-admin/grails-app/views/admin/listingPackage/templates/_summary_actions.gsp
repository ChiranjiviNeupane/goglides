<div class="x_panel">
    <div class="x_title">
        <h2>Actions</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled action-wrap">
                <li>
                    <a class="btn btn-dark col-xs-12" href="javascript:void(0);" onclick="statusUpdate('draft')"><i
                            class="fa fa-file-text"></i> Save as Draft</a>
                </li>
                <li>
                    <a class="btn btn-success col-xs-12" href="javascript:void(0);"
                       onclick="statusUpdate('publish')"><i class="fa fa-check"></i> Approve and Publish</a>
                </li>
                <li>
                    <a class="btn btn-danger col-xs-12"
                       href="${createLink(uri: '/admin/listing-package/delete?id=' + listingPackage?.packageCode)}"
                       onclick="return confirm('Are you sure you want to delete this item?')"><i
                            class="fa fa-trash-o"></i> Trash</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<script>
    function statusUpdate(status) {
        if (status != '' && confirm("${message(code: 'default.confirm.message', default: 'Are you sure?')}")) {
            $("#divLoading").addClass('show');
            $.ajax({
                url: "${createLink(uri: '/admin/listingPackage/updateStatus')}",
                type: "post",
                data: {id: '${listingPackage?.packageCode}', value: status},
                success: function (data) {
                    if (data == 'success') {

                    }
                },
                error: function (xhr) {

                },
                complete: function () {
                    $("#divLoading").removeClass('show');
                    location.reload();
                }
            });
        }
    }
</script>