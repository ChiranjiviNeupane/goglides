<div class="x_panel">
    <div class="x_title">
        <h2>Listing Summary</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled">
                <li>
                    <h4>
                        <a href="${createLink(uri: '/admin/listing/view?id=' + listing?.referenceNo)}">${listing?.title}</a>
                    </h4>
                </li>
                <li>
                    <i class="glyphicon glyphicon-briefcase"></i> <label><a
                        href="${createLink(uri: '/admin/business-directory/view/' + listing?.businessDirectory?.businessId)}">${listing?.businessDirectory?.businessName}</a>
                </label>
                </li>
            </ul>
        </div>
    </div>
</div>