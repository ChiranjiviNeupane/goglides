%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<g:form class="form" action="SaveListingMultimedia">
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 7 / ${step}</h3>
                <h5></h5>
            </div>

            <div class="form-top-right">
                <i class="fa fa-image" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-8">
                    <label>Feature Image</label>

                    <div id="feature-image"></div>

                    <p>This image will display on various deals and offer as front image.</p>
                </div>

                <div class="col-sm-offset-1 col-sm-3">
                    <label>Preview</label>
                    <g:if test="${featureImage}">
                        <div class="preview">
                            <img src="${amazonS3Service.sourceBucketCDN() + featureImage?.file}" class="img-responsive">
                            <span class="preview-hvr"><a href="javascript:void(0);"
                                                         onclick="delMultimedia(${featureImage?.id}, '${featureImage?.file}','feature')"><i
                                        class="fa fa-trash" aria-hidden="true"></i></a></span>
                        </div>
                    </g:if>
                </div>

                <div class="form-group col-sm-8">
                    <label for="">Other Images</label>

                    <div id="gallery-images"></div>

                    <p>These images will display on photo album.</p>
                </div>

                <div class="col-sm-offset-1 col-sm-3">
                    <label>Image List</label>
                    <g:if test="${galleryImages}">
                        <ul class="list-unstyled img-list">
                            <g:each in="${galleryImages}" status="i" var="gallery">
                                <li><a href="${amazonS3Service.sourceBucketCDN() + gallery?.file}"
                                       target="_blank">Image ${i}</a><a href="javascript:void(0);"
                                                                        onclick="delMultimedia(${gallery?.id}, '${gallery?.file}','gallery')"><i
                                            class="fa fa-trash pull-right" aria-hidden="true"></i></a></li>



                            </g:each>

                        </ul>
                    </g:if>
                </div>

                <div class="form-group col-sm-8">
                    <label for="">Video links</label>

                    <div class="multi-field-wrapper">
                        <div class="multi-fields">
                            <div class="multi-field">
                                <div class="form-group">
                                    <input type="text" name="file" value="${listingVideo?.file}"
                                           placeholder="Youtube video url..." class="form-control"
                                           id="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>Options to add video links of trip from Youtube/Vimeo and other sites</p>
                </div>

                <div class="form-group col-sm-4">

                </div>

                <div class="form-group col-sm-8">
                    <label>Route MAP</label>

                    <div id="route-map"></div>

                    <p>upload route MAP of trip if you have (this will send to customer after confirm booking).</p>
                </div>

                <div class="col-sm-offset-1 col-sm-3">
                    <label>Preview</label>
                    <g:if test="${listingRoute}">
                        <div class="preview">
                            <img src="${amazonS3Service.sourceBucketCDN() + listingRoute?.file}" class="img-responsive">
                            <span class="preview-hvr"><a href="javascript:void(0);"
                                                         onclick="delMultimedia(${listingRoute?.id}, '${listingRoute?.file}')"><i
                                        class="fa fa-trash" aria-hidden="true"></i></a></span>
                        </div>
                    </g:if>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-checklist', params: [id: listing?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<g:render template="/layouts/file_upload" model=""/>
<script>
    $('#feature-image').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "listing", action: "multimediaUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${listing?.id},
                type: '_feature',
                imageType: 'feature'
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key", absolute: true)}',
                    {
                        filename: filename,
                        directory: 'listing/feature'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "listing", action: "multimediaDelete")}",
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    $('#gallery-images').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "listing", action: "multimediaUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${listing?.id},
                type: '_gallery',
                imageType: 'gallery'
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key", absolute: true)}',
                    {
                        filename: filename,
                        directory: 'listing/gallery'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "listing", action: "multimediaDelete")}"
        },
        validation: {
            itemLimit: 10,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    $('#route-map').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "listing", action: "multimediaUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${listing?.id},
                type: '_routemap'
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key", absolute: true)}',
                    {
                        filename: filename,
                        directory: 'listing/routemap'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "listing", action: "multimediaDelete")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    function delMultimedia(id, key, imageType) {
        $.ajax({
            url: "${createLink(action: 'multimedia-delete')}",
            type: "post",
            data: {key: key, bucket: '${amazonS3Service.sourceBucket()}',imageType: imageType},
            success: function (data) {
                //console.log(data); //<-----this logs the data in browser's console
                if (data.status == 'success') {
                    location.reload();
                }
            },
            error: function (xhr) {
                // alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
    }
</script>
