%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:form class="form" action="saveListingGroupDiscount">
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 4.4 / ${step}</h3>
                <h5>Add Your Package</h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="listingPackageReferenceNo" value="${listingPackage?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-12">
                    <h3>C) Group Discount Policy (if any)</h3>

                    <div class="price-section">
                        <div class="type-policy">
                            <p>Add group discount policy of your company. Click add for more options.</p>

                            <div class="multi-field-wrapper group-policy-wrap">
                                <g:if test="${!groupDiscounts?.isEmpty()}">
                                    <g:each in="${groupDiscounts}">
                                        <div class="multi-field">
                                            <input type="hidden" name="policyId" value="${it?.id}">

                                            <div class="row form-group">
                                                <div class="col-sm-3 col-xs-6">
                                                    <label>Group policy</label>
                                                    <input type="text" name="groupPolicy" placeholder="Group Policy"
                                                           class="form-control"  id="" value="${it?.groupPolicy}>
                                                </div>

                                                <div class="col-sm-2 col-xs-6">
                                                    <label>Group Size (From)</label>
                                                    <input type="text" name="discountFrom" placeholder=""
                                                           onchange="isNumber($(this))" class="form-control"
                                                           value="${it?.discountFrom}" id="">
                                                </div>

                                                <div class="col-sm-2 col-xs-6">
                                                    <label>Group Size (To)</label>
                                                    <input type="text" name="discountTo" placeholder=""
                                                           onchange="isNumber($(this))" class="form-control"
                                                           value="${it?.discountTo}" id="">
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    <label>Discount %</label>
                                                    <input type="text" name="discountPercent" placeholder=""
                                                           onchange="isNumber($(this))"
                                                           value="${it?.discountPercent}" class="form-control" id="">
                                                </div>

                                                <div class="col-sm-2 col-xs-6">
                                                    <label for="">&nbsp;</label>
                                                    <button type="button"
                                                            class="btn btn-danger remove-discount form-control">
                                                        <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                    </g:each>
                                </g:if>
                            </div>
                            <button type="button" class="bttn btn add-field add-group-policy">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add</button>

                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group text-right border-top">

                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-addons', params: [id: listing?.referenceNo, package: listingPackage?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<div id="group-policy-template" class="multi-fields" style="display: none;">
    <div class="multi-field">
        <input type="hidden" name="policyId">

        <div class="row form-group">
            <div class="col-sm-3 col-xs-6">
                <label>Group policy</label>
                <input type="text" name="groupPolicy" placeholder="Group Policy" class="form-control"  id="">
            </div>

            <div class="col-sm-2 col-xs-6">
                <label>Group Size (From)</label>
                <input type="text" name="discountFrom" placeholder="" onchange="isNumber($(this))" class="form-control"
                       id="">
            </div>

            <div class="col-sm-2 col-xs-6">
                <label>Group Size (To)</label>
                <input type="text" name="discountTo" placeholder="" onchange="isNumber($(this))" class="form-control"
                       id="">
            </div>

            <div class="col-sm-3 col-xs-6">
                <label>Discount %</label>
                <input type="text" name="discountPercent" placeholder="" onchange="isNumber($(this))"
                       class="form-control" id="">
            </div>

            <div class="col-sm-2 col-xs-6">
                <label for="">&nbsp;</label>
                <button type="button" class="btn btn-danger remove-discount form-control">
                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
            </div>
        </div>
    </div>
</div>
<script>
    function isNumber(input) {
        var val = input.val();
        $("#submit-btn").attr('disabled', 'disabled');
        if ($.isNumeric(val)) {
            $("#submit-btn").removeAttr('disabled');
            return true;
        } else {
            alert('Invalid amount');
            return false;
        }
    }

    $('body').on('click', '.add-group-policy', function () {
        var multimediaRows = $('.group-policy-wrap .multi-field').length;
        if (multimediaRows < 5) {
            var template = $('#group-policy-template .multi-field').clone().appendTo('.group-policy-wrap');
        } else {
            alert("You have reach the maximum limit 5");
        }
    });

    $('body').on('click', '.remove-discount', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='policyId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeGroupDiscount')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });
</script>