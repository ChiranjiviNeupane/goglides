%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
%{--Use CommonService--}%
<g:set var="commonService" bean="commonService"/>
<!-- Fine Uploader Gallery CSS file
        ====================================================================== -->
<asset:stylesheet href="standalone/css/fine-uploader-gallery.css"/>
<!-- Fine Uploader S3 jQuery JS file
    ====================================================================== -->
<asset:javascript src="standalone/js/s3.jquery.fine-uploader.js"/>
<g:form class="form" action="SaveListingPolicies">
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 8 / ${step}</h3>
                <h5></h5>
            </div>

            <div class="form-top-right">
                <i class="fa fa-gavel" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="col-sm-12 form-group">
                    <label>Does this package cover any type of insurance coverage?</label>

                    <div>
                        <label>
                            <g:radio value="yes" name="hasInsuranceCoverage"
                                     checked="${listingPolicies?.hasInsuranceCoverage == 'yes'}"
                                     class="Form-label-radio has-insurance-coverage"
                                     required="required"/>
                            <span class="Form-label-text">Yes</span>

                            <div class="insurance-coverage-wrap"
                                 style="display: ${listingPolicies?.hasInsuranceCoverage == 'yes' ? 'block' : 'none'};">
                                <div class="form-group col-sm-9">
                                    <label>Insurance Provider</label>

                                    <div>
                                        <input type="text" name="insuranceCompany"
                                               value="${listingPolicies?.insuranceCompany}" class="form-control">
                                    </div>
                                    <label>Add your company insurance policy</label>

                                    <div id="insurance-document"></div>
                                    <g:if  test="${listingInsurance}">
                                        <ul>
                                            <g:each in="${listingInsurance}" status="i" var="insurance">
                                                <li><a href="${commonService.getImageUrl(insurance?.file)}">Insurance ${i}</a></li>
                                            </g:each>
                                        </ul>
                                    </g:if>

                                    <p>Upload your documents.</p>
                                </div>

                                <div class="form-group col-sm-9">
                                    <div>
                                        <label>Accidental insurance up to</label>
                                        <input type="text" value="${listingPolicies?.accidentalInsurance}"
                                               name="accidentalInsurance" class="form-control form-group"
                                               placeholder="USD 50,000">

                                        <label>Death insurance up to</label>
                                        <input type="text" value="${listingPolicies?.deathInsurance}"
                                               name="deathInsurance" class="form-control"
                                               placeholder="USD 100,000">
                                    </div>
                                </div>

                            </div>
                        </label>
                    </div>

                    <div>
                        <label>
                            <g:radio value="no" name="hasInsuranceCoverage"
                                     checked="${listingPolicies?.hasInsuranceCoverage == 'no'}"
                                     class="Form-label-radio has-insurance-coverage" required="required"/>
                            <span class="Form-label-text">No</span>

                            <div class="no-policy-reason-wrap"
                                 style="display: ${listingPolicies?.hasInsuranceCoverage == 'no' ? 'block' : 'none'};">
                                <label>Why your company don’t have insurance policy for this trip?</label>
                                <textarea class="form-control" name="noPolicyReason" rows="5"
                                          placeholder="">${listingPolicies?.noInsuranceReason}</textarea>
                            </div>
                        </label>
                    </div>
                </div>

                <div class="form-group col-sm-12">
                    <label for="">Update your cancellation policy</label>

                    <div>
                        <label>
                            <g:radio value="standard" name="cancellationPolicyType"
                                     checked="${listingPolicies?.cancellationPolicyType == 'standard'}"
                                     class="Form-label-radio policy-type" required="required"/>
                            <span class="Form-label-text">Use goGlides standard cancellation policy</span>
                            <a href="#">Read Policy <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                        </label>
                    </div>

                    <div>
                        <label>
                            <g:radio value="custom" name="cancellationPolicyType"
                                     checked="${listingPolicies?.cancellationPolicyType == 'custom'}"
                                     class="Form-label-radio policy-type" required="required"/>
                            <span class="Form-label-text">Update your custom cancellation policy</span>
                        </label>
                    </div>
                </div>

                <div class="form-group col-sm-12 cancellation-policy-wrap"
                     style="display: ${listingPolicies?.cancellationPolicyType == 'custom' ? 'block' : 'none'};">
                    <p><span class="req">**NOTE: - Amount or % (input value will refund to customer) ,</span>
                        Zero (0) means no refund</p>

                    <div class="cancellation-policies">
                        <g:if test="${cancellationPolicies}">
                            <g:each in="${cancellationPolicies}">
                                <div class="multi-field">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Title</label>
                                            <input type="hidden" name="cancellationId" value="${it?.id}">
                                            <g:select id="" name="policyTitle"
                                                      class="form-control input-flat"
                                                      from="['Before Departure', 'No Show', 'After Departure', 'Full Refund', 'Partial Refund', 'Re-schedule']"
                                                      value="${it?.policyTitle}"
                                                      keys="['Before Departure', 'No Show', 'After Departure', 'Full Refund', 'Partial Refund', 'Re-schedule']"
                                                      noSelection="['': 'Select']"
                                                      />
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="col-sm-12">Hour/Day</label>

                                            <div class="col-sm-6">
                                                <input type="text" name="duration" value="${it?.duration}"
                                                       onblur="isNumber($(this))" class="form-control"/>
                                            </div>

                                            <div class="col-sm-6">
                                                <g:select name="durationUnit"
                                                          from="['day', 'hour', 'minute']"
                                                          value="${it?.durationUnit}"
                                                          noSelection="['': 'Select unit']"
                                                          class="form-control sm-input"/>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="col-sm-12">Refund Amount or %</label>

                                            <div class="col-sm-6">
                                                <input type="text" name="refund" value="${it?.refund}"
                                                       onblur="isNumber($(this))" class="form-control"/>
                                            </div>

                                            <div class="col-sm-6">
                                                <g:select name="refundUnit"
                                                          from="['amount', 'percent']"
                                                          value="${it?.refundUnit}"
                                                          noSelection="['': 'Select unit']"
                                                          class="form-control sm-input"/>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Description</label>
                                            <textarea name="description" class="form-control"
                                                      palceholder="Description"
                                                      rows="1">${it?.description}</textarea>
                                        </div>

                                        <div class="col-sm-1">
                                            <label>&nbsp;</label>

                                            <div class="clearfix"></div>
                                            <button type="button" class="btn btn-danger remove-policy form-group">
                                                <i class="fa fa-close" aria-hidden="true"></i></button>
                                        </div>
                                    </div>

                                </div>
                            </g:each>
                        </g:if>
                    </div>
                    <a class="add-policy btn bttn">Add Policy</a>

                    <p><strong>No Show</strong> basically means that a user not showing up for the
                    glide or fail to present them at the glide time.</p>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-multimedia', params: [id: listing?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<div class="cancellation-fields bring-template" style="display: none;">
    <div class="multi-field">
        <div class="row">
            <div class="col-sm-2">
                <label>Title</label>
                <input type="hidden" name="cancellationId">
                <g:select id="" name="policyTitle"
                          class="form-control input-flat"
                          from="['Before Departure', 'No Show', 'After Departure', 'Full Refund', 'Partial Refund', 'Re-schedule']"
                          value=""
                          keys="['Before Departure', 'No Show', 'After Departure', 'Full Refund', 'Partial Refund', 'Re-schedule']"
                          noSelection="['': 'Select']"/>
            </div>

            <div class="col-sm-3">
                <label class="col-sm-12">Hour/Day</label>

                <div class="col-sm-6">
                    <input type="text" name="duration" value="0" onblur="isNumber($(this))" class="form-control"/>
                </div>

                <div class="col-sm-6">
                    <g:select name="durationUnit"
                              from="['day', 'hour', 'minute']"
                              value="${it?.durationUnit}"
                              noSelection="['': 'Select unit']"
                              class="form-control sm-input"/>
                </div>
            </div>

            <div class="col-sm-3">
                <label class="col-sm-12">Refund Amount or %</label>

                <div class="col-sm-6">
                    <input type="text" name="refund" value="0" onblur="isNumber($(this))" class="form-control"/>
                </div>

                <div class="col-sm-6">
                    <g:select name="refundUnit"
                              from="['amount', 'percent']"
                              value="${cancellationPolicies?.refundUnit}"
                              noSelection="['': 'Select unit']"
                              class="form-control sm-input"/>
                </div>
            </div>

            <div class="col-sm-3">
                <label>Description</label>
                <textarea name="description" class="form-control" palceholder="Description" rows="1"></textarea>
            </div>

            <div class="col-sm-1">
                <label>&nbsp;</label>

                <div class="clearfix"></div>
                <button type="button" class="btn btn-danger remove-policy form-group">
                    <i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
        </div>

    </div>
</div>
<script>
    function isNumber(input) {
        var val = input.val();
        $("#submit-btn").attr('disabled', 'disabled');
        if ($.isNumeric(val)) {
            $("#submit-btn").removeAttr('disabled');
            return true;
        } else {
            alert('Invalid amount');
            return false;
        }
    }

    $('body').on('change', '.has-insurance-coverage', function () {
        var value = $(this).val();
        if (value == 'yes') {
            $('.insurance-coverage-wrap').show();
            $('.no-policy-reason-wrap').hide();
        } else {
            $('.insurance-coverage-wrap').hide();
            $('.no-policy-reason-wrap').show();
        }
    });

    $('body').on('change', '.policy-type', function () {
        var value = $(this).val();
        if (value == 'custom') {
            $('.cancellation-policy-wrap').show();
        } else {
            $('.cancellation-policy-wrap').hide();
        }
    })

    $('body').on('click', '.add-policy', function () {
        var template = $('.cancellation-fields .multi-field').clone().appendTo('.cancellation-policies');
        console.log(template);
    });

    $('body').on('click', '.remove-policy', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='cancellationId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeCancellationPolicy')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });
</script>
<!-- Fine Uploader Customized Gallery template
    ====================================================================== -->
<script type="text/template" id="qq-template-s3">
<div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
             class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
    </div>

    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
        <span class="qq-upload-drop-area-text-selector"></span>
    </div>

    <div class="qq-upload-button-selector qq-upload-button">
        <div>Upload a file</div>
    </div>
    <span class="qq-drop-processing-selector qq-drop-processing">
        <span>Processing dropped files...</span>
        <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite"
        aria-relevant="additions removals">
        <li>
            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>

            <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     class="qq-progress-bar-selector qq-progress-bar"></div>
            </div>
            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>

            <div class="qq-thumbnail-wrapper">
                <a class="preview-link" target="_blank">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </a>
            </div>
            <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
            <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                Retry
            </button>

            <div class="qq-file-info">
                <div class="qq-file-name">
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"
                          aria-label="Edit filename"></span>
                </div>
                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                    <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                </button>
                <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                    <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                </button>
                <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                    <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                </button>
            </div>
        </li>
    </ul>

    <dialog class="qq-alert-dialog-selector">
        <div class="qq-dialog-message-selector"></div>

        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">Close</button>
        </div>
    </dialog>

    <dialog class="qq-confirm-dialog-selector">
        <div class="qq-dialog-message-selector"></div>

        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">No</button>
            <button type="button" class="qq-ok-button-selector">Yes</button>
        </div>
    </dialog>

    <dialog class="qq-prompt-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <input type="text">

        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">Cancel</button>
            <button type="button" class="qq-ok-button-selector">Ok</button>
        </div>
    </dialog>
</div>
</script>
<script>
    $('#insurance-document').fineUploaderS3({
        template: 'qq-template-s3',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "listing", action: "multimediaUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${listing?.id},
                type: '_insurance'
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key", absolute: true)}',
                    {
                        filename: filename,
                        directory: 'listing/insurance'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "listing", action: "multimediaDelete")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png', 'pdf', 'doc', 'docx']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });
</script>