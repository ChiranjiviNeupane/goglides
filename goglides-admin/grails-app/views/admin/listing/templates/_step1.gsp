<g:form class="form create-form" action="save">
    <div class="form-top" style="display: ${step ? 'block' : 'none'}">
        <div class="form-top-left">
            <h3>Step 1 / ${step}</h3>
            <h5></h5>

            <p class="req">All Fields are required unless marked optional</p>
        </div>

        <div class="form-top-right">
            <i class="fa fa-info"></i>
        </div>
    </div>
    <g:hasErrors bean="${this.listing}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listing}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <div class="form-bottom">
        <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
        <input type="hidden" name="returnurl" value="${params?.returnurl}">
        <input type="hidden" name="agent" value="${businessDirectory?.id}">

        <div class="row">
            <div class="form-group col-sm-12">
                <label for="">Product Title</label>
                <input type="text" name="title" value="${listing?.title}" placeholder="Product Title"
                       class="form-control" required="required">

                <p>Provide a short, clear title of your product</p>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-6">
                <label for="">Product Type</label>
                <g:select id="listing-category" name="listingCategory"
                          class="form-control"
                          from="${category}"
                          value="${listing?.categoryId}"
                          optionKey="id"
                          optionValue="categoryTitle"
                          noSelection="['': 'Select a Product Type']"
                          required="required"/>
                <p>Pick the category most closely associated with your glide or activity</p>
            </div>

            <div class="form-group col-sm-6">
                <!-- <label for="">Product Code (Optional)</label>
                <input type="text" name="listingCode" value="${listing?.listingCode}"
                       placeholder="Product Code (Optional)" class="form-control"/>

                <p>If you use a internal product code in your office, add it here or leave it blank</p> -->
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-6">
                <label for="">Nearest City</label>
                <input type="text" id="address" name="address" class="required form-control" placeholder="Nearest City"
                       onFocus="geolocate()" onBlur="geolocate()" value="${listing?.address}">
                <input type="hidden" id="locality" name="city" value="${listing?.city}">
                <input type="hidden" id="administrative_area_level_1" name="state" value="${listing?.state}">
                <input type="hidden" id="country" name="country" value="${listing?.country}">
                <input type="hidden" id="postal_code" name="zipCode" value="${listing?.zipCode}">
                <input type="hidden" id="lat" name="lat" value="${listing?.lat}">
                <input type="hidden" id="lng" name="lng" value="${listing?.lng}">


                <p>Pick the nearest city from where your glide starts</p>
            </div>

            <div class="form-group col-sm-6">
                <label for="nearestAirport">Nearest Airport</label>
                <input id="nearest-airport" type="text" id="nearestAirport" name="nearestAirport" value="${listing?.nearestAirport}"
                       class="form-control" placeholder="Nearest Airport" required="required"/>

                <p>Pick the nearest airport from where your activity starts</p>
            </div>
        </div>

        <div class="form-group text-right border-top">
            <button type="submit" class="btn btn-next bttn">Next</button>
        </div>
    </div>
</g:form>
<script>
    $(document).ready(function () {
        // validate the comment form when it is submitted
        $(".create-form").validate({
            rules: {},
            messages: {}
        });
    });
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var nearest_airports = [];
    var componentForm = {
        //street_number: 'short_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    function geolocate() {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                console.log(results[0].geometry.location.lat());
                console.log(results[0].geometry.location.lng());
                document.getElementById('lat').value = results[0].geometry.location.lat();
                document.getElementById('lng').value = results[0].geometry.location.lng();

            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    $(document).ready(function () {
        var nearest_city = $("#address").val();

        if (nearest_city != '' && nearest_city != null) {
            getNearestAirport(nearest_city);
        }
    });

    function getNearestAirport(formatted_address) {
        nearest_airports = [];
        $.ajax({
            url: "http://maps.googleapis.com/maps/api/geocode/json?address=airport " + formatted_address,
            success: function (response) {
                nearest_airports.push(response.results[0].address_components[0].long_name);
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=${siteSetting.getGoogleMapAPIKey()}&libraries=places&callback=initAutocomplete"
        async defer></script>
