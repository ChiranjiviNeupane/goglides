%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:form class="form" action="saveListingAddons">
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 4.3 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="listingPackageReferenceNo" value="${listingPackage?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-12">
                    <h3>B) Add-on Services</h3>
                    <strong>Per Person (Every people charged separately)</strong>

                    <div class="price-section">
                        <div class="type-multimedia">
                            <h4>Pricing Type:  Multimedia</h4>

                            <div class="col-sm-12 form-group">
                                <label>Does Multimedia Cost included on above rate?</label>

                                <div>
                                    <label>
                                        <g:radio name="includedMultimedia" class="Form-label-radio included-multimedia"
                                                 value="yes"
                                                 checked="${listingAddons?.includedMultimedia == 'yes'}"
                                                 required="required"/>
                                        <span class="Form-label-text">Yes</span>
                                    </label>
                                </div>

                                <div>
                                    <label>
                                        <g:radio name="includedMultimedia" class="Form-label-radio included-multimedia"
                                                 value="no"
                                                 checked="${listingAddons?.includedMultimedia == 'no'}"
                                                 required="required"/>
                                        <span class="Form-label-text">No</span>

                                    </label>
                                </div>
                            </div>

                            <div class="multimedia-wrap clearfix"
                                 style="display: ${listingAddons?.includedMultimedia == 'no' ? 'block' : 'none'};">
                                <div class="multimedia-addons">
                                    <g:if test="${!listingAddonMultimedia.isEmpty()}">
                                        <g:each in="${listingAddonMultimedia}">
                                            <div class="multi-field">
                                                <input type="hidden" name="multimediaAddonsId" value="${it?.id}">

                                                <div class="row form-group">
                                                    <div class="col-sm-3 col-xs-6">
                                                        <label>Age Group</label>
                                                        <g:select name="multimediaAgeGroup"
                                                                  from="['infant', 'child', 'adult', 'senior']"
                                                                  value="${it?.ageGroup}"
                                                                  noSelection="['': 'Select age group']"
                                                                  class="form-control"/>
                                                    </div>

                                                    <div class="col-sm-3 col-xs-6">
                                                        <label>Wholesale Rate</label>
                                                        <input type="text" name="multimediaWholesaleRate" placeholder=""
                                                               class="form-control wholesale-rate"
                                                               value="${it?.wholesaleRate}"
                                                               onchange="isNumber($(this))" id="#">
                                                    </div>

                                                    <div class="col-sm-3 col-xs-6">
                                                        <label>Retail rate</label>
                                                        <input type="text" name="multimediaRetailRate" placeholder=""
                                                               class="form-control retail-rate"
                                                               value="${it?.retailRate}"
                                                               onchange="isNumber($(this))" id="#">
                                                    </div>

                                                    <div class="col-sm-2 col-xs-6">
                                                        <label>&nbsp;</label>
                                                        <button type="button"
                                                                class="btn btn-danger remove-multimedia-addons form-control">
                                                            <i class="fa fa-close" aria-hidden="true"></i> Remove
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </g:each>
                                    </g:if>
                                </div>

                                <button type="button" class="bttn btn add-multimedia-addons">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add</button>

                            </div>

                        </div>

                        <div class="type-transportation">
                            <h4>Pricing Type: Transportation</h4>

                            <div class="col-sm-12 form-group">
                                <label>Does pick-up/drop-off cost included on above rate?</label>

                                <div>
                                    <label>
                                        <g:radio name="includedTransport" class="Form-label-radio included-multimedia"
                                                 value="yes"
                                                 checked="${listingAddons?.includedTransport == 'yes'}"
                                                 required="required"/>
                                        <span class="Form-label-text">Yes</span>
                                    </label>
                                </div>

                                <div>
                                    <label>
                                        <g:radio name="includedTransport" class="Form-label-radio included-transport"
                                                 value="no"
                                                 checked="${listingAddons?.includedTransport == 'no'}"
                                                 required="required"/>
                                        <span class="Form-label-text">No</span>

                                    </label>
                                </div>
                            </div>

                            <div class="transport-wrap clearfix"
                                 style="display: ${listingAddons?.includedTransport == 'no' ? 'block' : 'none'};">
                                <div class="transport-addons">
                                    <g:if test="${!listingAddonTransport.isEmpty()}">
                                        <g:each in="${listingAddonTransport}">
                                            <div class="multi-field">
                                                <input type="hidden" name="transportAddonsId" value="${it?.id}">

                                                <div class="row form-group">
                                                    <div class="col-sm-3 col-xs-6">
                                                        <label>Age Group</label>
                                                        <g:select name="transportAgeGroup"
                                                                  from="['infant', 'child', 'adult', 'senior']"
                                                                  value="${it?.ageGroup}"
                                                                  noSelection="['': 'Select age group']"
                                                                  class="form-control"/>
                                                    </div>

                                                    <div class="col-sm-3 col-xs-6">
                                                        <label>Wholesale Rate</label>
                                                        <input type="text" name="transportWholesaleRate" placeholder=""
                                                               class="form-control wholesale-rate"
                                                               value="${it?.wholesaleRate}" onchange="isNumber($(this))"
                                                               id="#">
                                                    </div>

                                                    <div class="col-sm-3 col-xs-6">
                                                        <label>Retail rate</label>
                                                        <input type="text" name="transportRetailRate" placeholder=""
                                                               class="form-control retail-rate"
                                                               value="${it?.retailRate}" onchange="isNumber($(this))"
                                                               id="#">
                                                    </div>

                                                    <div class="col-sm-2 col-xs-6">
                                                        <label>&nbsp;</label>
                                                        <button type="button"
                                                                class="btn btn-danger remove-transport-addons form-control">
                                                            <i class="fa fa-close" aria-hidden="true"></i> Remove
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                        </g:each>
                                    </g:if>
                                </div>

                                <button type="button" class="bttn btn add-transport-addons">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add</button>

                            </div>
                        </div>

                        <div class="type-discount">
                            <div class="col-sm-12 form-group">
                                <label>Do you have a group discount policy?</label>

                                <div>
                                    <label>
                                        <g:radio name="hasGroupDiscountPolicy" class="Form-label-radio group-discount"
                                                 value="yes"
                                                 checked="${listingAddons?.hasGroupDiscountPolicy == 'yes'}"
                                                 required="required"/>
                                        <span class="Form-label-text">Yes</span>

                                    </label>
                                </div>

                                <div>
                                    <label>
                                        <g:radio name="hasGroupDiscountPolicy" class="Form-label-radio group-discount"
                                                 value="no"
                                                 checked="${listingAddons?.hasGroupDiscountPolicy == 'no'}"
                                                 required="required"/>
                                        <span class="Form-label-text">No</span>
                                    </label>
                                </div>

                            </div>

                            <div class="col-sm-12 form-group addons-discount-wrap"
                                 style="display: ${listingAddons?.hasGroupDiscountPolicy == 'yes' ? 'block' : 'none'};">
                                <label>Does discount applicable for Add-on Services?</label>

                                <div>
                                    <label>
                                        <g:radio name="isDiscountApplicable"
                                                 class="Form-label-radio is-discount-applicable"
                                                 value="yes"
                                                 checked="${listingAddons?.isDiscountApplicable == 'yes'}"/>
                                        <span class="Form-label-text">Yes</span>

                                        <div class="" id="addon-discounts"
                                             style="display:${listingAddons?.isDiscountApplicable == 'yes' ? 'block' : 'none'}">
                                            <div class="form-group">
                                                <label>
                                                    <g:checkBox name="groupDiscountMultimedia"
                                                                class="Form-label-checkbox"
                                                                value="yes"
                                                                checked="${listingAddons?.groupDiscountMultimedia == 'yes'}"/>
                                                    <span class="Form-label-text">Multimedia</span>
                                                </label>
                                                <label>
                                                    <g:checkBox name="groupDiscountTransport"
                                                                class="Form-label-checkbox"
                                                                value="yes"
                                                                checked="${listingAddons?.groupDiscountTransport == 'yes'}"/>
                                                    <span class="Form-label-text">Transportation</span>
                                                </label>
                                            </div>

                                            <p>If uncheck, discount is not applicable for group booking</p>
                                        </div>
                                    </label>
                                </div>

                                <div>
                                    <label>
                                        <g:radio name="isDiscountApplicable"
                                                 class="Form-label-radio is-discount-applicable"
                                                 value="no"
                                                 checked="${listingAddons?.isDiscountApplicable == 'no'}"/>
                                        <span class="Form-label-text">No</span>
                                    </label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-package-pricing', params: [id: listing?.referenceNo, package: listingPackage?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>

<div id="multimedia-template" style="display:none">
    <div class="multi-field-wrapper">
        <div class="multi-fields">
            <div class="multi-field">
                <input type="hidden" name="multimediaAddonsId">

                <div class="row form-group">
                    <div class="col-sm-3 col-xs-6">
                        <label>Age Group</label>
                        <g:select name="multimediaAgeGroup"
                                  from="['infant', 'child', 'adult', 'senior']"
                                  value="${listingPrice?.ageGroup}"
                                  noSelection="['': 'Select age group']"
                                  class="form-control"/>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <label>Wholesale Rate</label>
                        <input type="text" name="multimediaWholesaleRate" placeholder=""
                               class="form-control wholesale-rate" onchange="isNumber($(this))" id="#">
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <label>Retail rate</label>
                        <input type="text" name="multimediaRetailRate" placeholder=""
                               class="form-control retail-rate" onchange="isNumber($(this))" id="#">
                    </div>

                    <div class="col-sm-2 col-xs-6">
                        <label>&nbsp;</label>
                        <button type="button" class="btn btn-danger remove-multimedia-addons form-control">
                            <i class="fa fa-close" aria-hidden="true"></i> Remove
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="transport-template" style="display:none">
    <div class="multi-field-wrapper">
        <div class="multi-fields">
            <div class="multi-field">
                <input type="hidden" name="transportAddonsId">

                <div class="row form-group">
                    <div class="col-sm-3 col-xs-6">
                        <label>Age Group</label>
                        <g:select name="transportAgeGroup"
                                  from="['infant', 'child', 'adult', 'senior']"
                                  value="${listingPrice?.ageGroup}"
                                  noSelection="['': 'Select age group']"
                                  class="form-control"/>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <label>Wholesale Rate</label>
                        <input type="text" name="transportWholesaleRate" placeholder=""
                               class="form-control wholesale-rate" onchange="isNumber($(this))" id="#">
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <label>Retail rate</label>
                        <input type="text" name="transportRetailRate" placeholder=""
                               class="form-control retail-rate" onchange="isNumber($(this))" id="#">
                    </div>

                    <div class="col-sm-2 col-xs-6">
                        <label>&nbsp;</label>
                        <button type="button" class="btn btn-danger remove-transport-addons form-control">
                            <i class="fa fa-close" aria-hidden="true"></i> Remove
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

    });

    function isNumber(input) {
        var val = input.val();
        $("#submit-btn").attr('disabled', 'disabled');
        if ($.isNumeric(val)) {
            $("#submit-btn").removeAttr('disabled');
            return true;
        } else {
            alert('Invalid amount');
            return false;
        }
    }

    $('body').on('change', '.included-multimedia', function () {
        var value = $(this).val();
        if (value == 'no') {
            $('.multimedia-wrap').show();
        } else {
            $('.multimedia-wrap').hide();
            $('.multimedia-addons').find('.wholesale-rate').removeAttr('required');
            $('.multimedia-addons').find('.retail-rate').removeAttr('required');
        }
    });

    $('body').on('click', '.add-multimedia-addons', function () {
        var multimediaRows = $('.multimedia-addons .multi-field').length;
        if (multimediaRows < 10) {
            var template = $('#multimedia-template .multi-field').clone().appendTo('.multimedia-addons');
            $('.multimedia-addons').find('.wholesale-rate').attr('required', 'required');
            $('.multimedia-addons').find('.retail-rate').attr('required', 'required');
        } else {
            alert("You have reach the maximum limit 10");
        }
    });

    $('body').on('click', '.remove-multimedia-addons', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='multimediaAddonsId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeListingAddons')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });

    $('body').on('change', '.included-transport', function () {
        var value = $(this).val();
        if (value == 'no') {
            $('.transport-wrap').show();

        } else {
            $('.transport-wrap').hide();
            $('.transport-addons').find('.wholesale-rate').removeAttr('required');
            $('.transport-addons').find('.retail-rate').removeAttr('required');
        }
    });

    $('body').on('click', '.add-transport-addons', function () {
        var multimediaRows = $('.transport-addons .multi-field').length;
        if (multimediaRows < 10) {
            var template = $('#transport-template .multi-field').clone().appendTo('.transport-addons');
            $('.transport-addons').find('.wholesale-rate').attr('required', 'required');
            $('.transport-addons').find('.retail-rate').attr('required', 'required');
        } else {
            alert("You have reach the maximum limit 10");
        }
    });

    $('body').on('click', '.remove-transport-addons', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='transportAddonsId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeListingAddons')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });

    $('body').on('change', '.group-discount', function () {
        var value = $(this).val();
        if (value == 'yes') {
            $('.addons-discount-wrap').show();

        } else {
            $('.addons-discount-wrap').hide();
        }
    });

    $('body').on('change', '.is-discount-applicable', function () {
        var value = $(this).val();
        if (value == 'yes') {
            $('#addon-discounts').show();

        } else {
            $('#addon-discounts').hide();
        }
    })
</script>