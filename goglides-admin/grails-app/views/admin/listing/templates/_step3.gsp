%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:form class="form" action="saveRules">
    <g:hasErrors bean="${this.listing}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listing}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
            <g:eachError bean="${this.listingRestrictions}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
            <g:eachError bean="${this.listingAgeGroup}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 3 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-cog" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row set-rule">
                <div class="form-group col-sm-9">
                    <label for="">Age Restriction</label>

                    <div class="row">
                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="minAge" value="${listingRestrictions?.minAge}" min="0" max="120"
                                   placeholder="Min" class="form-control" id="">
                        </div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="maxAge" value="${listingRestrictions?.maxAge}" min="0" max="120"
                                   placeholder="Max" class="form-control" id="">
                        </div>

                        <div class="col-sm-12"><p>Add minimum age and maximum age of the people for booking</p></div>
                    </div>
                </div>

                <div class="form-group col-sm-9">
                    <label for="">Weight Restriction (Optional)</label>

                    <div class="row">
                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="minWeight" value="${listingRestrictions?.minWeight}" min="0"
                                   placeholder="Min" class="form-control" id="">
                        </div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="maxWeight" value="${listingRestrictions?.maxWeight}" min="0"
                                   placeholder="Max" class="form-control" id="">
                        </div>

                        <div class="col-sm-12"><p>Add minimum weight and maximum weight of the people for booking</p>
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-9">
                    <label for="">Maximum people per booking</label>

                    <div class="row">
                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="maxPeople" value="${listingRestrictions?.maxPeople}" min="0"
                                   placeholder="15 pax" class="form-control" id="">
                        </div>

                        <div class="col-sm-12"><p>Maximum number of pax per booking. Not more than 15pax</p></div>
                    </div>
                </div>

                <div class="form-group col-sm-9">
                    <label for="">Maximum Capacity per group</label>

                    <div class="row">
                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="maxCapacity" value="${listingRestrictions?.maxCapacity}" min="0"
                                   placeholder="" class="form-control" id="">
                        </div>

                        <div class="col-sm-12"><p>Maximum numbers of capacity each group.</p></div>
                    </div>
                </div>

                <div class="form-group col-sm-9">
                    <label for="">Minimum people per booking</label>

                    <div class="row">
                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="minPeople" value="${listingRestrictions?.minPeople}" min="0"
                                   placeholder="" class="form-control" id="">
                        </div>

                        <div class="col-sm-12"><p>Minimum number of pax required per booking.</p></div>
                    </div>
                </div>

                <div class="form-group col-sm-9">
                    <label for="">Availability Hours</label>

                    <div class="row">
                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="availabilityHour" value="${listingRestrictions?.availabilityHour}"
                                   placeholder="24 hrs" class="form-control" id="">
                        </div>

                        <div class="col-sm-2 col-xs-6">
                            <g:select name="availabilityUnit"
                                      from="['Day', 'Hour', 'Minute']"
                                      value="${listingRestrictions?.availabilityUnit}"
                                      noSelection="['': 'Select unit']"
                                      required="required"
                                      class="form-control"/>
                        </div>

                        <div class="col-sm-12"><p>Enter the number of hours/days before your package start time
                        that you can no longer accept new bookings</p></div>
                    </div>
                </div>

                <div class="form-group col-sm-9">
                    <label for="">Booking Conformation Method</label>

                    <div>
                        <label>
                            <g:radio name="bookingConfirmation" value="instant"
                                     checked="${listingRestrictions?.bookingConfirmation == 'instant'}"
                                     class="Form-label-radio"/>
                            <span class="Form-label-text">Instant Conformation (Recommend)</span>

                            <p>Customer receives instant, automatic conformation of their bookings.</p>
                        </label>
                        <label>
                            <g:radio name="bookingConfirmation" value="request"
                                     checked="${listingRestrictions?.bookingConfirmation == 'request'}"
                                     class="Form-label-radio"/>
                            <span class="Form-label-text">On Request</span>

                            <p>Customer don't receive their confirmation, until you accept their bookings
                            via Goglides agent portal. This option might limit less visibility and may
                            impact your bookings.
                            </p>
                        </label>
                    </div>
                </div>

                <div class="form-group col-sm-9">
                    <label for="" class="space-bottom">Age Group (optional)</label>

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-1"><label for="" class="age-group">Infant</label></div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="infantMin" value="below" readonly="readonly"
                                   placeholder="Min"
                                   class="form-control" id="">
                        </div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="number" name="infantMax" value="${listingAgeGroup?.infantMax}" min="0"
                                   placeholder="Max"
                                   class="form-control" id="infantMax">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-1"><label for="">Child</label></div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="number" name="childMin" value="${listingAgeGroup?.childMin}" min="0"
                                   placeholder="Min"
                                   class="form-control" id="">
                        </div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="number" name="childMax" value="${listingAgeGroup?.childMax}" min="0"
                                   placeholder="Max"
                                   class="form-control" id="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-1"><label for="">Adult</label></div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="number" name="adultMin" value="${listingAgeGroup?.adultMin}" min="0"
                                   placeholder="Min"
                                   class="form-control" id="">
                        </div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="number" name="adultMax" value="${listingAgeGroup?.adultMax}" min="0"
                                   placeholder="Max"
                                   class="form-control" id="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-1"><label for="">Senior</label></div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="number" name="seniorMin" value="${listingAgeGroup?.seniorMin}" min="0"
                                   placeholder="Min"
                                   class="form-control" id="">
                        </div>

                        <div class="col-sm-2 col-xs-6">
                            <input type="text" name="seniorMax" value="above" readonly="readonly"
                                   placeholder="Max"
                                   class="form-control" id="">
                        </div>

                        <div class="col-sm-12"><p>Provide a different type of age group for pricing as per your
                        company rule. Leave it blank if you don't have different age group pricing</p></div>
                    </div>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/description', params: [id: listing?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<script>

    $(".form").validate({
        rules: {
            minAge: {
                required: true,
                number: true
            },
            maxAge: {
                required: true,
                number: true
            },
            minWeight: {
                number: true
            },
            maxWeight: {
                number: true
            },
            minPeople: {
                required: true,
                number: true
            },
            maxPeople: {
                required: true,
                number: true
            },
            maxCapacity: {
                required: true,
                number: true
            },
            availabilityHour: {
                required: true,
                number: true
            },
            availabilityHour: {
                required: true
            },
//            infantMin: "number",
            infantMax: "number",
            childMin: "number",
            childMax: "number",
            adultMin: "number",
            adultMax: "number",
            seniorMin: "number",
//            seniorMax: "number"
        }
    });
    $( document ).ready(function() {
        if($("#infantMax").val()==null){
            $('#infantMax').value=3

        }
    });
</script>