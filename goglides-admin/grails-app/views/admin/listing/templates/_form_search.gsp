<g:form class="form" action="search" controller="listing" namespace="admin">
    <div class="col-xs-6">
        <input type="text" class="form-control" name="listingCode" placeholder="Listing Code"/>
    </div>

    <div class="col-xs-6">
        <input type="text" class="form-control" name="title" placeholder="Title"/>
    </div>

    <div class="col-xs-12">
        <div class="ln_solid"></div>
        <input type="submit" value="Search" class="btn btn-success"/>
    </div>
</g:form>