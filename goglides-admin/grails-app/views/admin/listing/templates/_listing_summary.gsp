<div class="x_panel">
    <div class="x_title">
        <h2>Summary</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled">
                <li>
                    <i class="glyphicon glyphicon-info-sign"></i> <label>${listing?.listingCode}</label>
                </li>
                <li>
                    <i class="glyphicon glyphicon-briefcase"></i> <label><a
                        href="${createLink(uri: '/admin/business-directory/view/' + businessDirectory?.businessId)}">${businessDirectory?.businessName}</a>
                </label>
                </li>
                <li>
                    <i class="fa fa-paper-plane"></i> <label>Transferred:</label> ${listing?.transferred}
                </li>
                <li>
                    <i class="fa fa-certificate"></i> <label>Is Featured:</label> ${listing?.isFeatured}
                </li>

                <li class="text-capitalize">
                    <i class="glyphicon glyphicon-ok"></i> <label>Approved:</label> ${listing?.isApproved}
                </li>
                <li class="text-capitalize">
                    <i class="glyphicon glyphicon-ok"></i> <label>Status:</label> ${listing?.status}
                </li>
                <li><hr></li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Feature start:</label> <g:formatDate
                        date="${listing?.featuredStart}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Feature end:</label> <g:formatDate
                        date="${listing?.featuredEnd}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Created:</label> <g:formatDate
                        date="${listing?.createdAt}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Updated:</label> <g:formatDate
                        date="${listing?.modifiedAt}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Trashed:</label> <g:formatDate
                        date="${listing?.trashedAt}" type="date" style="MEDIUM"/>
                </li>
            </ul>
        </div>
    </div>
</div>