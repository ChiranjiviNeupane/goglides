<g:form class="form" action="saveDescription">
    <g:hasErrors bean="${this.listing}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listing}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 2 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-list" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-10 description-wrap">
                    <label for="">Listing Description</label>
                    <textarea id="description" name="description" rows=10 cols=5 class="form-control description"
                              placeholder="Description" maxlength="9000">${listing?.description}</textarea>

                    <p>Short description of your product not more than 9000 characters</p>
                </div>

                <div class="form-group col-sm-10 highlight-wrap">
                    <label for="">Highlights of Product (Optional)</label>
                    <textarea id="highlights" name="highlights" rows=10 cols=5 class="form-control highlights"
                              placeholder="Description" maxlength="9000">${listing?.highlights}</textarea>

                    <p>Some highlights of trip in bullets format. Maximum 10 bullets</p>
                </div>
            </div>
        </div>

        <div class="form-group text-right border-top">
            <button type="button" class="btn btn-previous bttn"
                    onclick="location.href = '${createLink(uri: '/admin/listing/edit', params: [id: listing?.referenceNo])}';">Previous</button>
            <button type="submit" class="btn btn-next bttn">Next</button>
        </div>
    </fieldset>
</g:form>
