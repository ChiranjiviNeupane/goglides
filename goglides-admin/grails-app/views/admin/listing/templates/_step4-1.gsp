%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:set var="listingService" bean="listingService"/>

<g:form class="form" action="saveListingPackageOptions">
    <g:hasErrors bean="${this.listingPackage}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listingPackage}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${this.listingAvailabilityDays}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listingPackage}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${this.listingAvailability}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.listingPackage}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 4.1 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="listingPackageReferenceNo" value="${listingPackage?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Trip availability day(s)</label>

                    <div class="row">
                        <div class="col-sm-1">
                            <label>
                                Mon
                                <g:checkBox name="monday" value="monday"
                                            checked="${listingAvailabilityDays?.monday == 'monday'}"
                                            class="Form-label-checkbox"/>

                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Tue
                                <g:checkBox name="tuesday" value="tuesday"
                                            checked="${listingAvailabilityDays?.tuesday == 'tuesday'}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Wed
                                <g:checkBox name="wednesday" value="wednesday"
                                            checked="${listingAvailabilityDays?.wednesday == 'wednesday'}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Thu
                                <g:checkBox name="thursday" value="thursday"
                                            checked="${listingAvailabilityDays?.thursday == 'thursday'}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Fri
                                <g:checkBox name="friday" value="friday"
                                            checked="${listingAvailabilityDays?.friday == 'friday'}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Sat
                                <g:checkBox name="saturday" value="saturday"
                                            checked="${listingAvailabilityDays?.saturday == 'saturday'}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>

                        <div class="col-sm-1">
                            <label>
                                Sun
                                <g:checkBox name="sunday" value="sunday"
                                            checked="${listingAvailabilityDays?.sunday == 'sunday'}"
                                            class="Form-label-checkbox"/>
                                <span class="Form-label-text"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Trip availability date(s)</label>
                    <g:select name="availabilityType"
                              from="['date-single', 'date-range']"
                              value="${listingAvailability?.availabilityType}"
                              noSelection="['': 'Select date range']"
                              required="required"
                              class="form-control form-group"/>

                    <div class="row">
                        <div class="col-sm-12 date-single-wrap"
                             style="display: ${(listingAvailability?.availabilityType == 'date-single') ? 'block' : 'none'};">
                            <label>Single Date</label>

                            <div class="input-group date" id="datetimepicker1">
                                <input type="text" name="singleFrom" value="${listingAvailability?.fromDate}"
                                       class="form-control datepicker">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>

                        <div class="date-range-wrap"
                             style="display: ${(listingAvailability?.availabilityType == 'date-range') ? 'block' : 'none'};">
                            <div class="col-sm-12"><label>Date Range</label></div>

                            <div class="col-sm-6">
                                <label>From</label>

                                <div class="input-group date">
                                    <input type="text" name="fromDate" value="${listingAvailability?.fromDate}"
                                           id="from" class="form-control datepicker">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <label>To</label>

                                <div class="input-group date">

                                    <input type="text" name="toDate" value="${listingAvailability?.toDate}" id="to"
                                           class="form-control datepicker">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label>Trip Durations</label>

                    <p>Week/Days/Hours/Minutes( For multiple duration click on add option )</p>

                    <div class="multi-field-wrapper">
                        <div class="multi-fields trip-duration">
                            <g:if test="${!listingDurations?.isEmpty()}">
                                <g:each in="${listingDurations}">
                                    <div class="multi-field">
                                        <input type="hidden" name="durationId" value="${it?.id}"/>
                                        <g:set var="duration" value="${listingService.splitDuration(it.duration)}"/>
                                        <g:set var="format" value="${listingService.formatDuration(it.duration)}"/>
                                        <p>${format.d}</p>

                                        <input type="hidden" name="duration" value="${it.duration}">

                                        <div class="row form-group">
                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Week</label>
                                                <input type="text" name="week" placeholder="00" onblur="calcOld()"
                                                       value="${duration.w}" class="form-control week" id="week">
                                            </div>

                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Days</label>
                                                <input type="text" name="days" placeholder="00" onblur="calcOld()"
                                                       value="${duration.d}" class="form-control days" id="days">
                                            </div>

                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Hours</label>
                                                <input type="text" name="hours" placeholder="00" onblur="calcOld()"
                                                       value="${duration.h}" class="form-control hours" id="hours">
                                            </div>

                                            <div class="col-xs-12 col-sm-2">
                                                <label for="">Minutes</label>
                                                <input type="text" name="minutes" placeholder="00" onblur="calcOld()"
                                                       value="${duration.m}" class="form-control minutes" id="minutes">
                                            </div>

                                            <div class="col-xs-12 col-sm-3">
                                                <label for="">&nbsp;</label>
                                                <button type="button"
                                                        class="btn btn-danger remove-duration form-control">
                                                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                        <button type="button" class="bttn btn add-field" id="add-trip-duration">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Trip Schedules</label>

                    <p>Departure time ( For multiple schedule click on add option )</p>

                    <div class="multi-field-wrapper">
                        <div class="multi-fields trip-schedule">
                            <g:if test="${!listingSchedules?.isEmpty()}">
                                <g:each in="${listingSchedules}">
                                    <div class="multi-field clearfix">
                                        <input type="hidden" name="scheduleId" value="${it?.id}"/>

                                        <div class="row form-group">
                                            <div class="col-xs-12 col-sm-9">
                                                <input type="time" name="schedule" onblur="formatSchedule()"
                                                       value="${it?.schedule}"
                                                       placeholder="Trip Schedule"
                                                       class="form-control form-group schedulevalue">
                                            </div>

                                            <div class="col-xs-12 col-sm-3">
                                                <button type="button"
                                                        class="btn btn-danger remove-schedule form-control">
                                                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                    </div>
                    <button type="button" class="bttn btn add-field" id="add-trip-schedule">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-package-edit', params: [id: listing?.referenceNo, package: listingPackage?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<div style="display: none;">
    <input type="hidden" value="0" class="duration-count">
    <input type="hidden" value="0" class="schedule-count">


    <div class="trip-duration-wrap">
        <div class="multi-field">
            <input type="hidden" name="durationId"/>

            <input type="hidden" name="duration" class="duration"/>

            <div class="row form-group">
                <div class="col-xs-12 col-sm-2">
                    <label for="">Week</label>
                    <input type="text" name="week" placeholder="00" onblur="calc()" class="form-control week">
                </div>

                <div class="col-xs-12 col-sm-2">
                    <label for="">Days</label>
                    <input type="text" name="days" placeholder="00" onblur="calc()" class="form-control days">
                </div>

                <div class="col-xs-12 col-sm-2">
                    <label for="">Hours</label>
                    <input type="text" name="hours" placeholder="00" onblur="calc()" class="form-control hours">
                </div>

                <div class="col-xs-12 col-sm-2">
                    <label for="">Minutes</label>
                    <input type="text" name="minutes" placeholder="00" onblur="calc()" class="form-control minutes">
                </div>

                <div class="col-xs-12 col-sm-3">
                    <label for="">&nbsp;</label>
                    <button type="button" class="btn btn-danger remove-duration form-control">
                        <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                </div>
            </div>
        </div>
    </div>

    <div class="trip-schedule-wrap">
        <div class="multi-field clearfix">
            <input type="hidden" name="scheduleId"/>

            <div class="row form-group">
                <div class="col-xs-12 col-sm-9">
                    <input type="time" name="schedule" onblur="formatScheduleCount()" placeholder="Trip Schedule 10:00 "
                           class="form-control schedulevalue form-group">
                </div>

                <div class="col-xs-12 col-sm-3">
                    <button type="button" class="btn btn-danger remove-schedule form-control">
                        <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function calc(index) {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        var week = $('.week-' + index).val();
        var days = $('.days-' + index).val();
        var hours = $('.hours-' + index).val();
        var minutes = $('.minutes-' + index).val();

        var errMessage = "";
        if (week == '')
            week = "00";
        if (days == '')
            days = "00";
        if (days > 6) {
            errMessage = "Invalid Days"
            toastr.error(errMessage);
        }
        if (hours == '')
            hours = "00";
        if (hours > 23) {
            errMessage = "Invalid Hours"
            toastr.error(errMessage);
            return false;
        }
        if (minutes == '')
            minutes = "00";
        if (minutes > 60) {
            errMessage = "Invalid Minutes"
            toastr.error(errMessage);
            return false;
        }
        if (!errMessage) {
            var duration = week + ":" + days + ":" + hours + ":" + minutes;
            if (duration == "00:00:00:00") {
                errMessage = "duration cannot be empty";
                toastr.error(errMessage);
                return false;
            }
            else {
                $(".duration-" + index).find('.duration').val(duration);
            }
            return false;

        }
    }
    function calcOld() {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        var week = $('.week').val();
        var days = $('.days').val();
        var hours = $('.hours').val();
        var minutes = $('.minutes').val();
        var errMessage = "";
        if (week == '')
            week = "00";
        if (days == '')
            days = "00";
        if (days > 6) {
            errMessage = "Invalid Days"
            toastr.error(errMessage);
        }
        if (hours == '')
            hours = "00";
        if (hours > 23) {
            errMessage = "Invalid Hours"
            toastr.error(errMessage);
            return false;
        }
        if (minutes == '')
            minutes = "00";
        if (minutes > 60) {
            errMessage = "Invalid Minutes"
            toastr.error(errMessage);
            return false;
        }
        if (!errMessage) {
            var duration = week + ":" + days + ":" + hours + ":" + minutes;
            if (duration == "00:00:00:00") {
                errMessage = "duration cannot be empty";
                toastr.error(errMessage);
                return false;
            }
            else {
                $('.duration').val(duration);
            }
            return false;

        }
    }
</script>
<script>
//    function formatScheduleCount(index) {
//        toastr.options = {
//            "closeButton": true,
//            "debug": false,
//            "newestOnTop": false,
//            "progressBar": false,
//            "positionClass": "toast-top-right",
//            "preventDuplicates": true,
//            "onclick": null,
//            "showDuration": "300",
//            "hideDuration": "1000",
//            "timeOut": "5000",
//            "extendedTimeOut": "1000",
//            "showEasing": "swing",
//            "hideEasing": "linear",
//            "showMethod": "fadeIn",
//            "hideMethod": "fadeOut"
//        };
//        var errMessage = "";
//        var schedule = $('.schedulevalue-' + index).val();
//        var a = schedule.split(':');
//        var hours = a[0];
//        var minutes = a[1];
//        if (hours >= '24' || !$.isNumeric(hours)) {
//            errMessage = "Invalid Hours"
//            toastr.error(errMessage);
//        }
//        if (minutes >= '60' || !$.isNumeric(hours)) {
//            errMessage = "Invalid Minutes"
//            toastr.error(errMessage);
//        }
//        if (!errMessage) {
//            var schedule = hours + ":" + minutes;
//            if (schedule == "00:00") {
//                errMessage = "Schedule cannot be empty"
//                toastr.error(errMessage);
//            }
//            else {
//                $(".schedule-" + index).find('.schedulevalue').val(schedule);
//                // alert($('.schedulevalue').val());
//            }
//        }
//
//
//    }
//    function formatSchedule() {
//        toastr.options = {
//            "closeButton": true,
//            "debug": false,
//            "newestOnTop": false,
//            "progressBar": false,
//            "positionClass": "toast-top-right",
//            "preventDuplicates": true,
//            "onclick": null,
//            "showDuration": "300",
//            "hideDuration": "1000",
//            "timeOut": "5000",
//            "extendedTimeOut": "1000",
//            "showEasing": "swing",
//            "hideEasing": "linear",
//            "showMethod": "fadeIn",
//            "hideMethod": "fadeOut"
//        };
//        var errMessage = "";
//
//        var schedule = $('.schedulevalue').val();
//        var a = schedule.split(':');
//        var hours = a[0];
//        var minutes = a[1];
//        if (hours >= '24' || !$.isNumeric(hours)) {
//            errMessage = "Invalid Hours"
//            toastr.error(errMessage);
//        }
//        if (minutes >= '60' || !$.isNumeric(hours)) {
//            errMessage = "Invalid Minutes"
//            toastr.error(errMessage);
//        }
//        if (!errMessage) {
//            var schedule = hours + ":" + minutes;
//            if (schedule == "00:00") {
//                errMessage = "Schedule cannot be empty"
//                toastr.error(errMessage);
//            }
//            else {
//                $('.schedulevalue').val(schedule);
////                alert($('.schedulevalue').val());
//            }
//        }
//
//
//    }
    $(document).ready(function () {

        $('.form').submit(function () {
            var GlideDurationRows = $('.trip-duration .multi-field').length;
            var GlideScheduleRows = $('.trip-schedule .multi-field').length;
            if (GlideDurationRows <= 0 || GlideScheduleRows <= 0) {
                $('.message').html('<div class="alert alert-danger alert-flat fade in alert-dismissable"><a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a><div class="message" role="status"><i class="fa fa-close"></i> At least one duration and schedule must be added.</div></div>')
                $('body').animate({scrollTop: 0}, "slow");
                return false
            }
        });
        $('#to').val('<g:formatDate format="yyyy-MM-dd" date="${listingAvailability?.toDate}"/>');
    });
    $('body').on('change', '#availabilityType', function () {
        var value = $(this).val();
        if (value == 'date-single') {
            $('.date-single-wrap').show();
            $('.date-range-wrap').hide();
        } else if (value == 'date-range') {
            $('.date-single-wrap').hide();
            $('.date-range-wrap').show();
        }
        else {
            $('.date-single-wrap').hide();
            $('.date-range-wrap').hide();
        }
    });

    $('body').on('click', '#add-trip-duration', function () {
        var GlideDurationRows = $('.trip-duration .multi-field').length;
        if (GlideDurationRows < 10) {
            var durationCount = parseInt($('.duration-count').val()) + 1;
            $('.duration-count').val(durationCount);
            var template = $('.trip-duration-wrap .multi-field').clone().appendTo('.trip-duration').addClass('duration-' + durationCount);
            $('.duration-' + durationCount).find('.week').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('week-' + durationCount);
            $('.duration-' + durationCount).find('.days').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('days-' + durationCount);
            $('.duration-' + durationCount).find('.hours').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('hours-' + durationCount);
            $('.duration-' + durationCount).find('.minutes').attr({
                'onblur': 'calc(' + durationCount + ')'
            }).addClass('minutes-' + durationCount);
        } else {
            alert("You have reach the maximum limit 10");
        }
    });

    $('body').on('click', '.remove-duration', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='durationId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeDuration')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });

    $('body').on('click', '#add-trip-schedule', function () {
        var GlideScheduleRows = $('.trip-schedule .multi-field').length;
        if (GlideScheduleRows < 10) {
            var scheduleCount = parseInt($('.schedule-count').val()) + 1;
            $('.schedule-count').val(scheduleCount);
            var template = $('.trip-schedule-wrap .multi-field').clone().appendTo('.trip-schedule').addClass('schedule-' + scheduleCount);

            $('.schedule-' + scheduleCount).find('.schedulevalue').attr({
                'onblur': 'formatScheduleCount(' + scheduleCount + ')'
            }).addClass('schedulevalue-' + scheduleCount);

        } else {
            alert("You have reach the maximum limit 10");
        }
    });

    $('body').on('click', '.remove-schedule', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='scheduleId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeSchedule')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });
</script>