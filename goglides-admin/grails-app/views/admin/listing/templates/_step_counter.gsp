<div class="row bs-wizard" style="border-bottom:0; position:relative;">

    <div class="col-xs-1 bs-wizard-step ${currentStep > '1' ? 'complete' : (currentStep < '1' ? 'disabled' : 'active')}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"><span>1</span></a>
    </div>

    <div class="col-xs-1 bs-wizard-step ${currentStep > '2' ? 'complete' : currentStep < '2' ? 'disabled' : 'active'}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"><span>2</span></a>
    </div>

    <div class="col-xs-1 bs-wizard-step ${currentStep > '3' ? 'complete' : (currentStep < '3' ? 'disabled' : 'active')}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"><span>3</span></a>
    </div>

    <div class="col-xs-1 bs-wizard-step ${currentStep > '4' ? 'complete' : (currentStep < '4' ? 'disabled' : 'active')}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress" style="width:277px;"><div class="progress-bar" style="width:12%;"></div></div>
        <a href="#" class="bs-wizard-dot"><span>4</span></a>
    </div>

    <div class="row bs-wizard nested-step" style="border-bottom:0; display: ${(currentStep > '4' && currentStep < '5' ? 'block' : 'none')};">

        <div class="col-xs-1 bs-wizard-step ${currentStep > '4.1' ? 'complete' : (currentStep < '4.1' ? 'disabled' : 'active')}">
            <div class="text-center bs-wizard-stepnum">&nbsp;</div>

            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"><span>4.1</span></a>
        </div>

        <div class="col-xs-1 bs-wizard-step ${currentStep > '4.2' ? 'complete' : (currentStep < '4.2' ? 'disabled' : 'active')}">
            <div class="text-center bs-wizard-stepnum">&nbsp;</div>

            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"><span>4.2</span></a>
        </div>

        <div class="col-xs-1 bs-wizard-step ${currentStep > '4.3' ? 'complete' : (currentStep < '4.3' ? 'disabled' : 'active')}">
            <div class="text-center bs-wizard-stepnum">&nbsp;</div>

            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"><span>4.3</span></a>
        </div>

        <div class="col-xs-1 bs-wizard-step ${currentStep > '4.4' ? 'complete' : (currentStep < '4.4' ? 'disabled' : 'active')}">
            <div class="text-center bs-wizard-stepnum">&nbsp;</div>

            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"><span>4.4</span></a>
        </div>
    </div>

    <div class="col-xs-1 ${(currentStep > '4' && currentStep < '5' ? 'col-xs-offset-2' : '')} bs-wizard-step ${currentStep > '5' ? 'complete' : (currentStep < '5' ? 'disabled' : 'active')}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"><span>5</span></a>
    </div>

    <div class="col-xs-1 bs-wizard-step ${currentStep > '6' ? 'complete' : (currentStep < '6' ? 'disabled' : 'active')}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"><span>6</span></a>
    </div>

    <div class="col-xs-1 bs-wizard-step ${currentStep > '7' ? 'complete' : (currentStep < '7' ? 'disabled' : 'active')}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"><span>7</span></a>
    </div>

    <div class="col-xs-1 bs-wizard-step ${currentStep > '8' ? 'complete' : (currentStep < '8' ? 'disabled' : 'active')}">
        <div class="text-center bs-wizard-stepnum">&nbsp;</div>

        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"><span>8</span></a>
    </div>

</div>