%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:form class="form" action="SaveListingPickup">
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 5 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-10">
                    <p>Add pick-up and drop off location for your product. Click Add button for
                    more locations. Leave it blank if you don't provide transportation service.</p>

                    <div class="multi-field-wrapper">
                        <div class="pickup-wrap">
                            <g:if test="${!listingPickups.isEmpty()}">
                                <g:each in="${listingPickups}">
                                    <div class="multi-field">
                                        <input type="hidden" name="pickupId" value="${it?.id}">

                                        <div class="form-group row ">
                                            <div class="col-xs-8 col-sm-8">
                                                <input type="text" name="address" placeholder="Location"
                                                       class="form-control" value="${it?.address}" id="">
                                            </div>

                                            <div class="col-xs-2 col-sm-2">
                                                <button type="button" class="btn btn-danger remove-pickup form-control">
                                                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                        <button type="button" class="bttn btn add-pickup">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                    </div>

                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-package-list', params: [id: listing?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<div class="multi-fields pickup-template" style="display: none;">
    <div class="multi-field">
        <input type="hidden" name="pickupId">

        <div class="form-group row ">
            <div class="col-xs-8 col-sm-8">
                <input type="text" name="address" placeholder="Location" class="form-control" id="">
            </div>

            <div class="col-xs-2 col-sm-2">
                <button type="button" class="btn btn-danger remove-pickup form-control">
                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('body').on('click', '.add-pickup', function () {
        var multimediaRows = $('.pickup-wrap .multi-field').length;
        if (multimediaRows < 12) {
            var template = $('.pickup-template .multi-field').clone().appendTo('.pickup-wrap');
        } else {
            alert("You have reach the maximum limit 12");
        }
    });

    $('body').on('click', '.remove-pickup', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='pickupId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeListingPickup')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });
</script>
