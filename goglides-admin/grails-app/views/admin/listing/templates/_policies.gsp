<p><strong>Cancellation :</strong></p>
<p><strong>Before Departure</strong></p>
<ul>
    <li>You may cancel with a full refund within 24 hours of booking your ticket. After that time, tickets are fully refundable, less a $15 processing fee, if cancelled at least 2 days before the trip date. Less than 02 day(s) of the schedule departure, there is a 100% cancellation fee.</li>
    <li>Above 15 days prior to your arrival: 25% of the invoice amount</li>
    <li>Between 14 days to 5 days prior to your arrival: 35% of the invoice amount</li>
    <li>Between 4 days to 02 days prior to your arrival: 50% of the invoice amount</li>
    <li>Less than 02 days prior to your arrival: 100% of the invoice amount</li>
    <li>No show: 100% of the invoice amount</li>
    <li>If the booked ticket is cancelled due to any of the reasons of the company, such ticket shall be postponed to next flight or shall returned the entire amount to booking agent. Transportation charge may not be refundable in case of service used.</li>
</ul>

<p><strong>After Departure</strong></p>
<ul>
    <li>Ticket is nonrefundable after departure.</li>
</ul>

<p><strong>CHARGE WAIVED IN CASE OF DEATH OF PASSENGER OR DEATH OF IMMEDIATE FAMILY. CERTIFICATE MUST BE NOTIFIED.
GOGLIDES MAY REFUSE REFUND IF APPLICATION IS MADE LATER THAN 15DAYS AFTER EXPIRY OF E-TICKETS.</strong></p>

<p><strong>DATE CHANGE - ANYTIME</strong></p>
<ul>
    <li>Above 2 days prior to your arrival: 20$</li>
    <li>Between 24 hours to 12 hour prior to your arrival: 25$</li>
    <li>Between 11 hours to 5 hour prior to your arrival: 30$</li>
    <li>Less than 4 hours prior to your arrival: Date change not permitted(please contact Goglides team).</li>
</ul>



