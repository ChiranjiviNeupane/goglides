%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:form class="form" action="SaveListingChecklist">
    <fieldset>
        <div class="form-top">
            <div class="form-top-left">
                <h3>Step 6 / ${step}</h3>
                <h5></h5>

                <p class="req">All Fields are required unless marked optional</p>
            </div>

            <div class="form-top-right">
                <i class="fa fa-th-list" aria-hidden="true"></i>
            </div>
        </div>

        <div class="form-bottom">
            <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
            <input type="hidden" name="returnurl" value="${params?.returnurl}">

            <div class="row">
                <div class="form-group col-sm-9">
                    <label for="">What we provide</label>

                    <div class="multi-field-wrapper">
                        <div class="provide-wrap">
                            <g:if test="${!listingChecklistProvide.isEmpty()}">
                                <g:each in="${listingChecklistProvide}">
                                    <div class="multi-field">
                                        <input type="hidden" name="provideId" value="${it?.id}">

                                        <div class="form-group row">
                                            <div class="col-xs-8 col-sm-8">
                                                <input type="text" name="provideItem" value="${it?.checklistItem}"
                                                       placeholder="Helmet" class="form-control" id="">
                                            </div>

                                            <div class="col-xs-2 col-sm-2">
                                                <button type="button" class="btn btn-danger remove-provide form-group">
                                                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                        <button type="button" class="bttn btn add-provide">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                    </div>

                </div>

                <div class="form-group col-sm-9">
                    <label for="">What to bring</label>

                    <div class="multi-field-wrapper">
                        <div class="bring-wrap">
                            <g:if test="${!listingChecklistBring.isEmpty()}">
                                <g:each in="${listingChecklistBring}">
                                    <div class="multi-field">
                                        <input type="hidden" name="bringId" value="${it?.id}">

                                        <div class="form-group row">
                                            <div class="col-xs-8 col-sm-8">
                                                <input type="text" name="bringItem" value="${it?.checklistItem}"
                                                       placeholder="Protective footwear, no open toe shoes.
" class="form-control" id="">
                                            </div>

                                            <div class="col-xs-2 col-sm-2">
                                                <button type="button" class="btn btn-danger remove-bring form-group">
                                                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                        <button type="button" class="bttn btn add-bring">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                    </div>

                </div>
            </div>

            <div class="form-group text-right border-top">
                <button type="button" class="btn btn-previous bttn"
                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-pickup', params: [id: listing?.referenceNo])}';">Previous</button>
                <button type="submit" class="btn btn-next bttn">Next</button>
            </div>
        </div>
    </fieldset>
</g:form>
<div class="multi-fields provide-template" style="display: none;">
    <div class="multi-field">
        <input type="hidden" name="provideId">

        <div class="form-group row">
            <div class="col-xs-8 col-sm-8">
                <input type="text" name="provideItem" placeholder="i.e. Helmet" class="form-control" id="">
            </div>

            <div class="col-xs-2 col-sm-2">
                <button type="button" class="btn btn-danger remove-provide form-group">
                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
            </div>
        </div>
    </div>
</div>

<div class="multi-fields bring-template" style="display: none;">
    <div class="multi-field">
        <input type="hidden" name="bringId">

        <div class="form-group row">
            <div class="col-xs-8 col-sm-8">
                <input type="text" name="bringItem" placeholder="i.e. Protective footwear, no open toe shoes.
" class="form-control" id="">
            </div>

            <div class="col-xs-2 col-sm-2">
                <button type="button" class="btn btn-danger remove-bring form-group">
                    <i class="fa fa-close" aria-hidden="true"></i> Remove</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('body').on('click', '.add-provide', function () {
        var provideRows = $('.provide-wrap .multi-field').length;
        if (provideRows < 12) {
            var template = $('.provide-template .multi-field').clone().appendTo('.provide-wrap');
        } else {
            alert("You have reach the maximum limit 12");
        }
    });

    $('body').on('click', '.remove-provide', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='provideId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeListingChecklist')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });

    $('body').on('click', '.add-bring', function () {
        var bringRows = $('.bring-wrap .multi-field').length;
        if (bringRows < 12) {
            var template = $('.bring-template .multi-field').clone().appendTo('.bring-wrap');
        } else {
            alert("You have reach the maximum limit 12");
        }
    });

    $('body').on('click', '.remove-bring', function () {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('.multi-field').find("input[name='bringId']").val();
            if (id != '') {
                $.ajax({
                    url: "${createLink(action: 'removeListingChecklist')}",
                    type: "post",
                    data: {id: id},
                    success: function (data) {
                        if (data == 'success') {
                            $('body').find("input[value=" + id + "]").closest('.multi-field').remove();
                        }
                    },
                    error: function (xhr) {

                    }
                });
            } else {
                $(this).closest('.multi-field').remove();
            }
        }
    });
</script>