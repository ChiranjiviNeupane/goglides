%{--Use ListingService--}%
<g:set var="listService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use CommonService--}%
<g:set var="commonService" bean="commonService"/>
%{--Use FriendlyUrlService--}%
<g:set var="friendlyUrlService" bean="friendlyUrlService"/>
<g:set var="pageTitle" value="Listings"/>
<g:set var="pageSubtitle" value="list of listings"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>${pageTitle}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(uri: '/admin/listing/create')}" class="btn bttn"><i
                    class="fa fa-plus"></i> Add New Listing</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Advanced Search</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
                <div class="x_content" style="display: none;">
                    <g:form url="[action: 'index', controller: 'listing']">
                        <div class="col-xs-6">
                            <input type="text" name="search" class="form-control" placeholder="advance listing search..." style="height: auto; padding: 7px 12px;">
                        </div>
                        <div class="col-xs-5">
                            <g:select name="searchAttribute" from="${["search by Title", "search by Reference No.","search by Listing Code", "search by Status"]}"
                                      keys="${['title', 'referenceNo', 'listingCode' , 'status']}"
                                      value="${name}"
                                      noSelection="['': '-Choose Search Category-']" class="form-control" style="height: auto; padding: 7px 12px;"/>
                        </div>
                        <div class="col-xs-1 form-group">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </g:form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>${pageTitle} <small>${pageSubtitle}</small></h2>

                <div class="pull-right">
                    <a class="btn btn-primary" href="${createLink(action: 'index')}"><i
                            class="fa fa-bars"></i> All <span
                            class="badge">${totalCount}</span></a>
                    <a class="btn btn-dark" href="${createLink(action: 'index', params: [state: 'draft'])}"><i
                            class="fa fa-file-text"></i> Draft <span
                            class="badge">${inActiveCount}</span></a>
                    <a class="btn btn-success" href="${createLink(action: 'index', params: [state: 'publish'])}"><i
                            class="fa fa-check"></i> Publish <span
                            class="badge">${activeCount}</span></a>
                    <a class="btn btn-danger" href="${createLink(action: 'index', params: [state: 'trash'])}"><i
                            class="fa fa-trash-o"></i> Past <span
                            class="badge">${trashCount}</span></a>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:if test="${listings}">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-condensed">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Listing</th>
                                <th>Supplier</th>
                                <th>Approved</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            <g:each in="${listings}">
                                <tr>
                                    <td>${it?.listingCode}</td>
                                    <td>${it?.title}</td>
                                    <td>${raw(commonService.truncateString(it?.businessDirectory?.businessName))}</td>
                                    <th><span class="text-capitalize">${raw(friendlyUrlService.sanitizeWithSpaces(it?.isApproved))}</span></th>
                                    <th><span class="text-capitalize">${raw(friendlyUrlService.sanitizeWithSpaces(it?.status))}</span></th>
                                    <td>
                                        <a href="${createLink(uri: '/admin/listing/view?id=' + it?.referenceNo)}"
                                           data-toggle="tooltip" title="View"><i
                                                class="fa fa-eye" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>

                    <div class="pagination pull-right">
                     %{--   <g:paginate total="${listings?.totalCount ?: 0}" class="pagination"
                                    params="${[state: params?.state]}"
                                    next='&raquo;' prev='&laquo;' maxsteps="4"/>--}%

                     <g:paginate total="${listingSize}" class="pagination"
                                 params="${[state: params?.state]}" action="index"
                                 next='&raquo;' prev='&laquo;' maxsteps="4"/>
                    </div>
                </g:if>
                <g:else>
                    <p>${message(code: 'listing.list.blank')}</p>
                </g:else>
            </div>
        </div>
    </div>
</div>
</body>
</html>