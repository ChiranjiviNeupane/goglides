<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main-v_0.2" />
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
    <!-- Fine Uploader Gallery CSS file
        ====================================================================== -->
    <asset:stylesheet href="standalone/css/fine-uploader-gallery.css" />
    <!-- Fine Uploader S3 jQuery JS file
        ====================================================================== -->
    <asset:javascript src="standalone/js/s3.jquery.fine-uploader.js"/>
</head>

<body>
<div class="container page-wrap">
    <div class="row">
        <g:render template="listing_sidebar"/>
        <div class="content-wrapper col-md-9">
            <g:if test="${flash.message}">
                <div class="alert alert-success alert-flat fade in alert-dismissable">
                    <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
                    <div class="message" role="status"><i class="fa fa-check-circle text-success"></i> ${flash.message}</div>
                </div>

            </g:if>
            <g:form class="description-form" action="saveMultimedia">
                <div class="panel panel-flat panel-shadow panel-default">
                    <div class="panel-heading"><h2>Listing Multimedia</h2>
                        ${grailsApplication.config.amazonS3.bucket}
                    </div>
                    <div class="panel-body">
                        <g:hasErrors bean="${this.listing}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.listing}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="id" value="${listing?.id}" />
                                <label for="">Feature Image</label>
                                <div class="form-group">
                                    <div id="feature-image"></div>
                                </div>
                                <ul class="clearfix">
                                    <g:if test="${featureImage?.id != null}">
                                        <li class="feature-${featureImage?.id} thumbnail col-md-3"><img src="https://com-goglides-media.s3.amazonaws.com/${featureImage?.file}"><a onclick="delFeature(${featureImage?.id}, '${featureImage?.file}')" class="pull-right"><i class="glyphicon glyphicon-remove"></i></a></li>
                                    </g:if>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="postTitle">Upload Images<span>*</span></label>
                                    <div class="form-group">
                                        <div id="gallery-images"></div>
                                    </div>
                                    <span class="text-muted">Upload some nice photos of your product, that might help to get more bookings</span>
                                    <ul class="clearfix">
                                        <g:if test="${galleryImages?.id != null}">
                                            <g:each in="${galleryImages}">
                                                <li class="gallery-${it?.id} thumbnail col-md-3"><img src="https://com-goglides-media.s3.amazonaws.com/${it?.file}"><a onclick="delGallery(${it?.id}, '${it?.file}')" class="pull-right"><i class="glyphicon glyphicon-remove"></i></a></li>
                                            </g:each>
                                        </g:if>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <g:submitButton id="submit-btn" name="Save" class="btn btn-flat btn-primary pull-right" value="${message(code: 'default.button.save.label', default: 'Save')}" />
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
<!-- Fine Uploader Customized Gallery template
    ====================================================================== -->
<script type="text/template" id="qq-template-s3">
<div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
    </div>
    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
        <span class="qq-upload-drop-area-text-selector"></span>
    </div>
    <div class="qq-upload-button-selector qq-upload-button">
        <div>Upload a file</div>
    </div>
    <span class="qq-drop-processing-selector qq-drop-processing">
        <span>Processing dropped files...</span>
        <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
        <li>
            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
            </div>
            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
            <div class="qq-thumbnail-wrapper">
                <a class="preview-link" target="_blank">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </a>
            </div>
            <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
            <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                Retry
            </button>

            <div class="qq-file-info">
                <div class="qq-file-name">
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                </div>
                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                    <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                </button>
                <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                    <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                </button>
                <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                    <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                </button>
            </div>
        </li>
    </ul>

    <dialog class="qq-alert-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">Close</button>
        </div>
    </dialog>

    <dialog class="qq-confirm-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">No</button>
            <button type="button" class="qq-ok-button-selector">Yes</button>
        </div>
    </dialog>

    <dialog class="qq-prompt-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <input type="text">
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">Cancel</button>
            <button type="button" class="qq-ok-button-selector">Ok</button>
        </div>
    </dialog>
</div>
</script>
<script>
    $('#feature-image').fineUploaderS3({
        template: 'qq-template-s3',
        request: {
            endpoint: "https://com-goglides-media.s3.amazonaws.com",
            accessKey: "AKIAJZ6UXGTVHJHZE7EQ"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "listing", action: "featureImageUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${listing?.id}
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                        '${createLink(controller: "amazonS3", action: "s3Key")}',
                        {
                            filename: filename
                        }).done(function (data) {
                    keyRetrieval.success(data);
                });
//                );
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "listing", action: "featureImageDelete")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function(id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    $('#gallery-images').fineUploaderS3({
        template: 'qq-template-s3',
        request: {
            endpoint: "https://com-goglides-media.s3.amazonaws.com",
            accessKey: "AKIAJZ6UXGTVHJHZE7EQ"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "listing", action: "galleryImagesUploadSuccess")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${listing?.id}
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                        '${createLink(controller: "amazonS3", action: "s3Key")}',
                        {
                            filename: filename
                        }).done(function (data) {
                    keyRetrieval.success(data);
                });
//                );
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "listing", action: "galleryImagesDelete")}"
        },
        validation: {
            itemLimit: 10,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function(id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    function delFeature(id, key){
        $.ajax({
            url: "${createLink(action: 'featureImageDelete')}",
            type:"post",
            data:{key: key, bucket: 'com-goglides-media'},
            success: function(data) {
                //console.log(data); //<-----this logs the data in browser's console
                $('.feature-'+id).remove();
                if(data == 'success'){

                }
            },
            error: function(xhr){
                // alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
    }

    function delGallery(id, key){
        $.ajax({
            url: "${createLink(action: 'galleryImagesDelete')}",
            type:"post",
            data:{key: key, bucket: 'com-goglides-media'},
            success: function(data) {
                //console.log(data); //<-----this logs the data in browser's console
                $('.gallery-'+id).remove();
                if(data == 'success'){

                }
            },
            error: function(xhr){
                // alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
    }
</script>
</body>
</html>
