<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main-v_0.2" />
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
    <ckeditor:resources/>
</head>
<body>
<div class="container page-wrap">
    <div class="row">
        <g:render template="listing_sidebar"/>
        <div class="content-wrapper col-md-9">
            <g:if test="${flash.message}">
                <div class="alert alert-success alert-flat fade in alert-dismissable">
                    <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
                    <div class="message" role="status"><i class="fa fa-check-circle text-success"></i> ${flash.message}</div>
                </div>

            </g:if>
            <g:form class="description-form" action="savePolicy">
                <div class="panel panel-flat panel-shadow panel-default">
                    <div class="panel-heading"><h2>Listing Policies</h2></div>
                    <div class="panel-body">
                        <g:hasErrors bean="${this.listing}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.listing}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="id" value="${listing?.id}">
                                <p class="text-danger"><strong><i class="fa fa-asterisk"></i></strong> Describe about your company policies in brief. Policies should not exceed more than 1000 characters.</p>
                                <!--<textarea class="form-control" name="policy._general" placeholder="General Policies">${generalPolicy?.description}</textarea>-->
                                <ckeditor:config var="toolbar_Mytoolbar">
                                    [
                                        [ 'Source', '-', 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ],
                                        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'BlockQuote'],
                                        ['Link', 'Unlink', 'Anchor'],
                                        ['Styles', 'Format']
                                    ]
                                </ckeditor:config>


                                <ckeditor:editor name="policy._general" height="200px" width="100%" toolbar="Mytoolbar">${generalPolicy?.description}</ckeditor:editor>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <g:submitButton id="submit-btn" name="save" class="btn btn-flat btn-primary pull-right" value="${message(code: 'default.button.save.label', default: 'Save')}" />
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function(){
        $(".policy-form").validate({
            rules: {
                description:{

                },
                messages: {

                }
            });
    });
</script>
</body>
</html>
