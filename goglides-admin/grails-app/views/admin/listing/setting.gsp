<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main-v_0.2" />
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
    <ckeditor:resources/>
</head>
<body>
<div class="container page-wrap">
    <div class="row">
        <g:render template="listing_sidebar"/>
        <div class="content-wrapper col-md-9">
            <g:if test="${flash.message}">
                <div class="alert alert-success alert-flat fade in alert-dismissable">
                    <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
                    <div class="message" role="status"><i class="fa fa-check-circle text-success"></i> ${flash.message}</div>
                </div>

            </g:if>
            <g:form class="description-form" action="saveSetting">
                <div class="panel panel-flat panel-shadow panel-default">
                    <div class="panel-heading"><h2>Listing Settings</h2></div>
                    <div class="panel-body">
                        <g:hasErrors bean="${this.listing}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.listing}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Glide Type</h3>
                                <br>
                                <div class="checkbox checkbox-primary checkbox-inline">
                                    <g:checkBox id="solo" name="glideType" value="Solo" checked="${listingSolo?.type == 'Solo'}" />
                                    <label for="solo">&nbsp; Solo</label>
                                </div>
                                <div class="checkbox checkbox-primary checkbox-inline">
                                    <g:checkBox id="tandem" name="glideType" value="Tandem" checked="${listingTandem?.type == 'Tandem'}" />
                                    <label for="tandem">&nbsp; Tandem</label>
                                </div>
                                <div class="text-muted"><strong>Solo:</strong> Single glide&nbsp;
                                    <strong>Tandem:</strong> Two people glide</div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h3>Glide Duration</h3>
                                <br>
                                <input type="hidden" name="listingId" value="${listing.id}">
                                <p>Add time(HH:MM) of gliding as per your company rule, you may add/remove more time by clicking plus/minus tab.</p>
                                <ul class="glide-duration row">
                                    <g:each in="${listingDuration}">
                                        <li class="glide-duration-${it?.id} additional_setting_options_list glide_duration clearfix">
                                            <div class="input-group bootstrap-timepicker timepicker col-md-12">
                                                <div class="col-md-6">
                                                    <input class="input-lg timepicker1 form-control input-flat" type="text" name="glideduration.${it?.id}" value="${it?.duration}">
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="#" onclick="removeDuration(${it?.id}, 'db')"><i class="fa fa-minus fa-lg text-danger"></i></a></div>
                                            </div>
                                        </li>
                                    </g:each>
                                </ul>
                                <input id="glideDuration" type="hidden" name="glide_duration_count" value="0">
                                <a id="glideDurationAdd" href="javascript:void(0)" class="add_new_time btn btn-flat btn-primary"><i class="fa fa-plus"></i> Add New</a>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h3>Schedule</h3>
                                <br>
                                <p>Add schedule of gliding everyday, you may add/remove more time by clicking plus/minus tab.</p>
                                <ul class="glide-schedule row">
                                    <g:each in="${listingSchedule}">
                                        <li class="glide-schedule-${it?.id} additional_setting_options_list glide_schedule clearfix">
                                        <div class="bootstrap-timepicker timepicker col-md-6"><input class="input-lg input-flat timepicker2 form-control" type="text" name="glideschedule.${it?.id}" value="${it?.schedule}"></div><div class="col-md-1"><a onclick="removeSchedule(${it?.id}, 'db')"><i class="fa fa-minus fa-lg text-danger"></i></a></div></span>
                                                </li>
                                    </g:each>
                                </ul>
                                <input id="glideSchedule" type="hidden" name="glide_schedule_count" value="0">
                                <a id="glideScheduleAdd" href="javascript:void(0)" class="add_new_time goBtn goBtn_primary btn btn-flat btn-primary"><i class="fa fa-plus"></i> Add New</a>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h3>Nationality(Optional)</h3>
                                <p></p>
                                <ul class="glide-nationality row">
                                    <g:each in="${listingNationality}">
                                        <li class="glide-nationality-${it.id} additional_setting_options_list nationality clearfix">
                                            <div class="col-md-6">
                                                <input class="input-lg form-control input-flat" text="text" name="glidenationality.${it.id}" value="${it?.nationality}"></div><div class="col-md-1"><a onclick="removeNationality(${it.id}, 'db')"><i class="fa fa-minus fa-lg text-danger"></i></a>
                                        </div>
                                        </li>
                                    </g:each>
                                </ul>
                                <input id="glideNationality" type="hidden" name="glide_nationality_count" value="0">
                                <a id="glideNationalityAdd" href="javascript:void(0);" class="add_new_time btn btn-primary btn-flat"><i class="fa fa-plus"></i> Add New</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <g:submitButton id="submit-btn" name="Save" class="btn btn-flat btn-primary pull-right" value="${message(code: 'default.button.save.label', default: 'Save')}" />
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
<script src="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        $(".description-form").validate({
            rules: {

            },
            messages: {

            }
        });

        durationTimePicker();
        scheduleTimePicker();
    });
    $('body').on('click','#glideDurationAdd',function(){
        $('#submit-btn').removeAttr('disabled');
        var GlideDurationRows = $('ul.glide-duration li').length;
        if(GlideDurationRows < 10){
            var id = parseInt($('#glideDuration').val());
            $('#glideDuration').val(id+1);
            $('.glide-duration').append('<li class="glide-duration-'+ id+' additional_setting_options_list glide_duration clearfix"><div class="bootstrap-timepicker timepicker col-md-6"><input class="input-lg input-flat timepicker1 form-control" type="text" name="glideduration.'+id+'" /></div><div class="col-md-1"><a onclick="removeDuration('+id+', \'auto\')" ><i class="fa fa-minus fa-lg text-danger"></i></a></div></li>');
            durationTimePicker();
        }else{
            alert("You have reach the maximun limit 10");
        }
    });

    function removeDuration(id, type){
        if (confirm('Are you sure you want to delete this?')) {
            if(type == 'auto'){
                $('.glide-duration-'+id).remove();
            }else{
                $.ajax({
                    url: "${createLink(action: 'removeDuration')}",
                    type:"post",
                    data:{id: id},
                    success: function(data) {
                        //console.log(data); //<-----this logs the data in browser's console
                        if(data == 'success'){
                            $('.glide-duration-'+id).remove();
                        }else{
                            $('.content-wrapper').prepend('<div class="alert alert-warning alert-flat fade in alert-dismissable"><a class="close" href="#" data-dismiss="alert" aria-label="close" title="close">×</a><i class="fa fa-warning text-warning"></i> Blocked! the value is assigned. Record cannot be deleted.</div>');
                        }
                    },
                    error: function(xhr){
                        //alert(xhr.responseText); //<----when no data alert the err msg
                    }
                });
            }
        }
    }

    $('body').on('click','#glideScheduleAdd',function(){
        $('#submit-btn').removeAttr('disabled');
        var GlideScheduleAddRows = $('ul.glide-schedule li').length;
        if(GlideScheduleAddRows <10){
            var id = parseInt($('#glideSchedule').val());
            $('#glideSchedule').val(id+1);
            $('.glide-schedule').append('<li class="glide-schedule-'+ id+' additional_setting_options_list glide_schedule clearfix"><div class="bootstrap-timepicker timepicker2 col-md-6"><input class="input-lg input-flat timepicker2 form-control" type="text" name="glideschedule.'+id+'" /></div><div class="col-md-1"><a onclick="removeSchedule('+id+', \'auto\')"><i class="fa fa-minus fa-lg text-danger"></i></a></div></li>');
            scheduleTimePicker();
        }
        else{
            alert("You have reach the maximun limit 10");
        }
    });

    function removeSchedule(id, type){
        if (confirm('Are you sure you want to delete this?')) {
            if(type == 'auto'){
                $('.glide-schedule-'+id).remove();
            }else{
                $.ajax({
                    url: "${createLink(action: 'removeSchedule')}",
                    type:"post",
                    data:{id: id},
                    success: function(data) {
                        //console.log(data); //<-----this logs the data in browser's console
                        if(data == 'success'){
                            $('.glide-schedule-'+id).remove();
                        }else{
                            $('.content-wrapper').prepend('<div class="alert alert-warning alert-flat fade in alert-dismissable"><a class="close" href="#" data-dismiss="alert" aria-label="close" title="close">×</a><i class="fa fa-warning text-warning"></i> Blocked! the value is assigned. Record cannot be deleted.</div>');
                        }
                    },
                    error: function(xhr){
                        //alert(xhr.responseText); //<----when no data alert the err msg
                    }
                });
            }
        }
    }

    $('#glideNationalityAdd').click(function(){
        $('#submit-btn').removeAttr('disabled');
        var GlideNationalityRows = $('ul.glide-nationality li').length;
        if(GlideNationalityRows < 10){
            var id = parseInt($('#glideNationality').val());
            $('#glideNationality').val(id+1);
            $('.glide-nationality').append('<li class="glide-nationality-'+ id+' additional_setting_options_list nationality clearfix"><div class="col-md-6"><input class="input-lg form-control input-flat" type="text" name="glidenationality.'+id+'" /></div><div class="col-md-6"><a onclick="removeNationality('+id+', \'auto\')"></div><i class="fa fa-minus fa-lg text-danger"></i></a></div></li>');
        }
        else{
            alert("You have reach the maximun limit 10");
        }
    });

    function removeNationality(id, type){
        if (confirm('Are you sure you want to delete this?')) {
            if(type == 'auto'){
                $('.glide-nationality-'+id).remove();
            }else{
                $.ajax({
                    url: "${createLink(action: 'removeNationality')}",
                    type:"post",
                    data:{id: id},
                    success: function(data) {
                        //console.log(data); //<-----this logs the data in browser's console
                        if(data == 'success'){
                            $('.glide-nationality-'+id).remove();
                        }else{
                            $('.content-wrapper').prepend('<div class="alert alert-warning alert-flat fade in alert-dismissable"><a class="close" href="#" data-dismiss="alert" aria-label="close" title="close">×</a><i class="fa fa-warning text-warning"></i> Blocked! the value is assigned. Record cannot be deleted.</div>');
                        }
                    },
                    error: function(xhr){
                        //alert(xhr.responseText); //<----when no data alert the err msg
                    }
                });
            }
        }
    }

    function durationTimePicker(){
        $(function () {
            $('.timepicker1').datetimepicker({
                format: 'HH:mm',
            });
        });
    }

    function scheduleTimePicker(){
        $(function () {
            $('.timepicker2').datetimepicker({
                format: 'LT'
            });
        });
    }

    jQuery('.description-form').submit(function(){
        var GlideDurationRows = $('ul.glide-duration li').length;
        var GlideScheduleAddRows = $('ul.glide-schedule li').length;
        //var GlideNationalityRows = $('ul.glide-nationality li').length;

        if(GlideScheduleAddRows <= 0 || GlideDurationRows <= 0){
            $('.alert').remove();
            $('.content-wrapper').prepend('<div class="alert alert-warning alert-flat fade in alert-dismissable"><i class="fa fa-warning text-warning"></i> Please! add atleast one duration and schedule.</div>');
            $('#submit-btn').attr('disabled', 'disabled');
            return false;
        }else{
            $('#submit-btn').removeAttr('disabled');
            return true;
        }
    });
</script>
</body>
</html>