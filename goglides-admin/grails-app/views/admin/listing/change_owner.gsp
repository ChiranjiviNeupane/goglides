<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main-v_0.2" />
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<div class="container page-wrap">
    <div class="row">
        <g:render template="listing_sidebar"/>
        <div class="content-wrapper col-md-9">
            <g:form action="saveOwner" method="POST" class="update-owner" id="${listing?.id}">
                <div class="panel panel-flat panel-shadow panel-default">
                    <div class="panel-heading"><h2>Change Ownership</h2></div>
                    <div class="panel-body">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">${flash.message}</div>
                        </g:if>
                        <g:hasErrors bean="${this.listing}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.listing}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>
                        <g:hiddenField name="version" value="${this.listing?.version}" />
                        <div class="row">
                            <div class="col-md-12">
                                <g:select id="listing-user" name="listingUser"
                                          class="form-control input-lg input-flat"
                                          from="${users}"
                                          value="${listing?.user.id}"
                                          optionKey="id"
                                          optionValue="username"
                                          noSelection="['':'Select an agent']"
                                          required="required" />
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <input id="submit-btn" class="btn btn-flat btn-primary pull-right" type="submit" value="${message(code: 'default.button.save.label', default: 'Save')}" />
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
<script>
    $(".update-form").validate({
        rules: {

        },
        messages: {

        }
    });
</script>
</body>
</html>
