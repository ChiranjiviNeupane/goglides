%{--Use CommonService--}%
<g:set var="userService" bean="userService"/>

<g:set var="commonService" bean="commonService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>New Listing</h2>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Hi, ${userService.user(userService.currentUser())?.firstname}! Lets get ready to add new listing.</h3>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <p>Select business from the list below for whom the listing will be added.</p>

                    <div class="form-group agent-list">
                        <g:select id="business-directory" name="businessDirectory"
                                  class="form-control agent"
                                  from="${activeBusinessDirectories}"
                                  value=""
                                  optionKey="businessId"
                                  optionValue="businessName"
                                  noSelection="['': 'Select business...']"
                                  onChange="redirectPage()"
                                  onBlur="redirectPage()"/>
                    </div>
                    <strong>OR</strong><br>
                    <h4>Add new business by clicking <a
                            href="${createLink(uri: '/admin/business-directory/create')}">here</a>.</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function redirectPage() {
        var businessDirectory = $('#business-directory option:selected').val();
        if (businessDirectory.length > 0) {
            window.location = "${createLink(controller: 'listing',namespace: 'admin',action: 'create',absolute: true)}?business=" + escape(businessDirectory);

        } else {
            var msg = '<div class="alert alert-danger alert-flat fade in alert-dismissable">' +
                '<a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>' +
                '<div class="message" role="status"><i class="fa fa-close"></i> Please select a business before processing.</div>' +
                '</div>';
            $('.alert-msg').html(msg);
        }
    }
</script>
</body>
</html>
