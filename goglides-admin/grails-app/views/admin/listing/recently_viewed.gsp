%{--Use ListingService--}%
<g:set var="listService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService" />
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017" />
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>Recently Viewed Listings</h2>
        </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>${params?.state ? 'Listings recently viewed by users.': 'Listings you viewed recently.'}</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12 badge-info">
                            <a href="${createLink(action: 'recently-viewed')}">Listing you viewed  <!--<span class="badge">${viewCount}</span>--></a>
                            <a href="${createLink(action: 'recently-viewed', params: [state: 'viewed'])}">Recent Views on your listing  <!--<span class="badge">${viewedCount}</span>--></a>
                        </div>
                    </div>
                    <g:if test="${listingView}">
                        <g:each in="${listingView}">
                        <div class="row space-bottom">
                            <div class="col-sm-3">
                                <div class="img-box" style="background-image:url('${listService.getListingFeatureImage(it?.id.toInteger())}');" ></div>
                            </div>
                            <div class="col-sm-8">
                                <h3><a href="<g:createLink absolute='true' uri='/view/${it.slug}'/>">${it?.title}</a></h3>
                                <p>${raw(listService.truncateString(it?.description))}</p>
                                <strong>${raw(priceService.getPriceRange(it?.id.toInteger()))}</strong>
                                <div class="action">
                                    <strong>Last Viewed :</strong> ${listService.getLastViewed(it?.id.toInteger())}
                                </div>
                            </div>
                        </div>
                        </g:each>
                        <div class="pagination pull-right">
                            <g:paginate total="${listingView.size() ?: 0}" class="pagination" params="${[state:params?.state]}" />
                        </div>
                    </g:if>
                    <g:else>
                        <p>${message(code: 'listing.review.blank')}</p>
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>