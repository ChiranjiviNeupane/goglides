%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>Listing - ${listing?.title}</h2>
        </div>
    </div>

    <div class="clearfix"></div>
    <g:render template="/admin/listing/templates/step_counter" model="[currentStep: '7']"/>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Add some nice images of Trip</h3>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <g:render template="/admin/listing/templates/step7"
                              model="[listing: listing, step: listingService.totalSteps()]"/>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>