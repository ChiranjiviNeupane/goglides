%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>Listing - ${listing?.title}</h2>
        </div>
    </div>

    <div class="clearfix"></div>

    <g:render template="/listing/templates/step_counter" model="[currentStep: '4']"/>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Listing Packages</h3>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <g:if test="${listingPackages.isEmpty()}">
                        ${message(code: 'listing.package.blank')}
                    </g:if>
                    <g:else>
                        <g:each in="${listingPackages}">
                            <div class="row space-bottom">
                                <div class="col-sm-2">
                                    <div class="img-box img-box-2col"
                                         style="background-image:url('${listingService.getListingFeatureImage(listing?.id)}');"></div>
                                </div>

                                <div class="col-sm-6">
                                    <h4><a href="${createLink(uri: '/admin/listing/listing-package-edit', params: [id: listing?.referenceNo, package: it?.referenceNo])}">${it?.tripTitle}</a>
                                    </h4>

                                    <p><strong>Goglides product code :</strong> ${it?.packageCode}</p>

                                    <p><strong>Your product code :</strong> ${it?.tripCode}</p>
                                    <strong>${raw(priceService.getPriceRange(it?.id, '_package'))}</strong>
                                </div>

                                <div class="col-sm-4 text-right">
                                    <div class="action">
                                        <a href="javascript:void(0);" class="btn btn-default"
                                           onclick="showPackage('${listing?.referenceNo}', '${it?.referenceNo}')">View</a>
                                        <a href="${createLink(uri: '/admin/listing/listing-package-delete/', params: [id: listing?.referenceNo, 'package': it?.referenceNo])}"
                                           class="btn btn-default"
                                           onclick="return confirm('Are you sure you want to delete this?');">Trash</a>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </g:each>
                    </g:else>

                    <div class="form-group text-right border-top">
                        <a href="${createLink(uri: '/admin/listing/listing-package', params: [id: listing?.referenceNo])}"
                           type="button" class="btn bttn pull-left">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add Package</a>

                        <button type="button" class="btn btn-previous bttn"
                                onclick="location.href = '${createLink(uri: '/admin/listing/set-rules', params: [id: listing?.referenceNo])}';">Previous</button>
                        <g:if test="${!listingPackages.isEmpty()}">
                            <button type="button"
                                    onclick="location.href = '${createLink(uri: '/admin/listing/listing-add-pickup', params: [id: listing?.referenceNo])}';"
                                    class="btn btn-next bttn">Next</button>
                        </g:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function showPackage(listing, package) {
        if (listing && package) {
            $("#divLoading").addClass('show');
            $.ajax({
                url: "${createLink(action: 'view-package')}",
                type: "post",
                data: {listing: listing, package: package},
                success: function (data) {
                    $('#packageModal .modal-body-wrap').html(data);
                },
                error: function (xhr) {

                },
                complete: function (data) {
                    $("#divLoading").removeClass('show');
                    $('#packageModal').modal('show');
                }
            });
        }
    }
</script>
<!-- Modal -->
<div class="modal" id="packageModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body-wrap listing-view">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
</body>
</html>