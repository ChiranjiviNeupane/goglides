%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>Listing - ${listing?.title}</h2>
        </div>
    </div>

    <div class="clearfix"></div>
    <g:render template="/admin/listing/templates/step_counter" model="[currentStep: '4.2']"/>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Set pricing - ${listingPackage?.tripTitle}</h3>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <g:if test="${listing?.category?.slug == 'paragliding'}">
                        <g:render template="/admin/listing/templates/step4-2-paragliding"
                                  model="[listing: listing, step: listingService.totalSteps()]"/>
                    </g:if>
                    <g:elseif test="${listing?.category?.slug == 'bungy'}">
                        <g:render template="/admin/listing/templates/step4-2-bungy"
                                  model="[listing: listing, step: listingService.totalSteps()]"/>
                    </g:elseif>
                    <g:elseif test="${listing?.category?.slug == 'rafting-kayak'}">
                        <g:render template="/admin/listing/templates/step4-2-rafting-kayak"
                                  model="[listing: listing, step: listingService.totalSteps()]"/>
                    </g:elseif>
                    <g:else>
                        <g:render template="/admin/listing/templates/step4-2"
                                  model="[listing: listing, step: listingService.totalSteps()]"/>
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>