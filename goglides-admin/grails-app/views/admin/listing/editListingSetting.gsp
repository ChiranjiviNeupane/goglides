<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h3>Edit Listing Setting</h3>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="x_panel">

            <div class="x_title">
                <h2> ${listing?.title}</h2>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <g:form action="updateListingSetting" method="post" class="form">
                    <g:hasErrors bean="${this.listing}">
                        <ul class="alert alert-danger" role="alert">
                            <g:eachError bean="${this.listing}" var="error">
                                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                        error="${error}"/></li>
                            </g:eachError>
                        </ul>
                    </g:hasErrors>
                    <input type="hidden" name="listingId" value="${listing?.id}"/>

                    <div class="form-group col-sm-10">
                        <label for="">Slug</label>
                        <input type="text" name="slug" placeholder="Slug" class="form-control"
                               value="${listing?.slug}">
                    </div>

                    <div class="form-group col-sm-10">
                        <label for="">Is Featured</label><br/>
                        <g:radioGroup name="isFeatured" class="isFeature"
                                      labels="[' Yes ', ' No ']"
                                      values="[true,false]"
                                      value="${listing?.isFeatured}"
                                      required="required">
                            ${it.label} ${it.radio}
                        </g:radioGroup>
                        <br/>
                    </div>

                    <div class="listingFeature">

                    <div class="form-group col-sm-10" style="clear: both">
                        <label for="">Feature Start Date</label>
                        <input type="text" name="featuredStart" placeholder="Feature Start Date"
                               class="form-control datepicker featuredStart"
                               id="featuredStart" value="${listing?.featuredStart}">
                    </div>

                    <div class="form-group col-sm-10">
                        <label for="">Feature End Date</label>
                        <input type="text" name="featuredEnd" placeholder="Feature End Date"
                               class="form-control datepicker featuredEnd"
                               id="featuredEnd" value="${listing?.featuredEnd}">
                    </div>

                    </div>

                    <div class="form-group col-sm-10">
                        <label for="">Is Transferred</label><br/>
                        <g:radioGroup name="transferred"
                                      labels="[' Yes ', ' No ']"
                                      values="[true, false]"
                                      value="${listing?.transferred}"
                                      required="required">
                            ${it.label} ${it.radio}
                        </g:radioGroup>
                        <br/>
                    </div>

                    <div class="form-group col-sm-12">
                        <fieldset class="buttons" onclick="return checkDate()">
                            <g:submitButton id="submit-btn" name="update" class="btn bttn"
                                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                        </fieldset>
                    </div>

                </g:form>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <g:render template="/admin/listing/templates/listing_summary" model="[listing: listing]"/>
    </div>
</div>


<script>

    var today = new Date();
    var minFeaturedStartDate = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate()


    if ($('#featuredStart').length > 0) {
        $('#featuredStart').datetimepicker({
                format: 'YYYY-MM-DD',
                defaultDate: new Date(<g:formatDate format="yyyy-MM-dd" date="${listing?.featuredStart}"/>)

            }
        );
    }


    if ($('#featuredEnd').length > 0) {
        $('.featuredEnd').datetimepicker({
                format: 'YYYY-MM-DD',
                defaultDate: new Date(<g:formatDate format="yyyy-MM-dd" date="${listing?.featuredEnd}"/>)
                // minDate: minFeaturedStartDate

            }
        );
    }


    $('.featuredEnd').on('click', function () {
        var d = new Date($('.featuredStart').val())
        var startMinDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
        $('.featuredEnd').data("DateTimePicker").minDate(startMinDate);

    });
    $('.featuredStart').data("DateTimePicker").minDate(minFeaturedStartDate);


    $(document).ready(function () {
        if($('input[name=isFeatured]:checked').val()=='false'){
            $('.listingFeature').hide();
        }
    });


        $('.isFeature').on('change',function () {
            console.log($('input[name=isFeatured]:checked').val())


            if($('input:radio[name=isFeatured]:checked').val()=='true'){
                $('.listingFeature').show();
                console.log("Clicked.");
            }

            if($('input[name=isFeatured]:checked').val()!='true'){
                $('.listingFeature').hide();
            }

        });

    function checkDate() {
        if($('.featuredStart').val()==''){
            alert("Select Featured start date");
            return false;
        }

        if($('.featuredEnd').val()==''){
            alert("Select Featured End Date ")
            return false;
        }
    }

    console.log("Date "+$('.featuredStart').val())

</script>
</body>
</html>
