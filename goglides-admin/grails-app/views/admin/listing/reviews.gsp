<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main-v_0.2" />
    %{-- Star rating --}%
    <asset:stylesheet href="standalone/js/star-rating/css/star-rating.min.css" />
    <asset:javascript src="standalone/js/star-rating/js/star-rating.min.js" />

    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    %{--Use ListingService--}%
    <g:set var="listService" bean="listingService"/>
</head>
<body>
<div class="container page-wrap">
    <div class="row">
        <aside class="sidebar sidebar-main col-md-3">
            <h2>Reviews</h2>
            <ul class="sidebar-menu">
                <li class="active"><a href="#listing-draft" aria-controls="listing-draft" role="tab" data-toggle="tab">Inactive Reviews</a><span class="pull-right badge">${reviews_draft?.size()}</span></li>
                <li><a href="#listing-publish" aria-controls="listing-publish" role="tab" data-toggle="tab">Active Reviews</a><span class="pull-right badge">${reviews_published?.size()}</span></li>
            </ul>
            <a href="${createLink(action: 'create')}" class="btn btn-flat btn-primary btn-lg">Add New Listing</a>
        </aside>
        <div class="content-wrapper col-md-9">
            <g:if test="${flash.message}">
                <div class="alert alert-success alert-flat fade in alert-dismissable">
                    <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
                    <div class="message" role="status"><i class="fa fa-check-circle text-success"></i> ${flash.message}</div>
                </div>

            </g:if>
            <div class="main_content tab-content">
                <div id="listing-draft" class="panel panel-flat panel-shadow panel-default tab-pane active fade in">
                    <div class="panel-heading"><h2>In Progress</h2></div>
                    <div class="panel-body">
                        <g:if test="${reviews_draft}">
                            <ul class="listings clearfix">
                                <g:each in="${reviews_draft}">
                                    <li class="row">
                                        <div class="col-md-8">
                                            <div class="reviews row">
                                                <div class="col-md-12">
                                                    ${it.user.firstname + ' ' + it.user.lastname}
                                                </div>
                                            </div>
                                            <div class="reviews row">
                                                <div class="col-md-12">
                                                    <input value="${it.rating}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" data-readonly="true" data-show-clear="false" data-show-caption="false">
                                                </div>
                                            </div>
                                            <div class="reviews row">
                                                <div class="col-md-12">
                                                    ${it?.comment}
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <a href="${createLink(action: 'changeReviewStatus', id: it.id, params: [status_to:'publish'])}" class="btn btn-flat btn-primary"><i class="fa fa-toggle-on" aria-hidden="true"></i> Publish</a>
                                                <a href="${createLink(action: 'changeReviewStatus', id: it?.id, params: [status_to:'trash'])}" class="btn btn-flat btn-primary"><i class="fa fa-trash"></i> Trash</a>
                                            </div>
                                        </div>
                                    </li>
                                </g:each>
                            </ul>
                        </g:if>
                        <g:else>
                            No records found.
                        </g:else>
                        <div class="pagination pull-right">
                            <g:paginate total="${listingCount ?: 0}" class="pagination" />
                        </div>
                    </div>
                </div>
                <div id="listing-publish" class="panel panel-flat panel-shadow panel-default tab-pane fade in">
                    <div class="panel-heading"><h2>Published</h2></div>
                    <div class="panel-body">
                        <g:if test="${reviews_published}">
                            <ul class="listings clearfix">
                                <g:each in="${reviews_published}">
                                    <li class="row">
                                        <div class="col-md-8">
                                            <div class="reviews row">
                                                <div class="col-md-12">
                                                    ${it.user.firstname + ' ' + it.user.lastname}
                                                </div>
                                            </div>
                                            <div class="reviews row">
                                                <div class="col-md-12">
                                                    <input value="${it.rating}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" data-readonly="true" data-show-clear="false" data-show-caption="false">
                                                </div>
                                            </div>
                                            <div class="reviews row">
                                                <div class="col-md-12">
                                                    ${it?.comment}
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <a href="${createLink(action: 'changeReviewStatus', id: it.id, params: [status_to:'draft'])}" class="btn btn-flat btn-primary"><i class="fa fa-toggle-off" aria-hidden="true"></i> Draft</a>
                                                <a href="${createLink(action: 'changeReviewStatus', id: it?.id, params: [status_to:'trash'])}" class="btn btn-flat btn-primary"><i class="fa fa-trash"></i> Trash</a>
                                            </div>
                                        </div>
                                    </li>
                                </g:each>
                            </ul>
                        </g:if>
                        <g:else>
                            No records found.
                        </g:else>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>