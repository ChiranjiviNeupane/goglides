<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main-v_0.2" />
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
    <div class="container page-wrap">
        <div class="row">
            <g:render template="listing_sidebar"/>
            <div class="content-wrapper col-md-9">
                <g:if test="${flash.message}">
                <div class="alert alert-success alert-flat fade in alert-dismissable">
                    <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
                    <div class="message" role="status"><i class="fa fa-check-circle text-success"></i> ${flash.message}</div>
                </div>

            </g:if>
            <g:form class="price-form" action="savePricing">
            <div class="panel panel-flat panel-shadow panel-default">
                <div class="panel-heading"><h2>Listing Price</h2></div>
                <div class="panel-body">
                    <g:hasErrors bean="${this.listing}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.listing}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="listingId" value="${listing.id}">
                    <ul class='price-list'>
                        <li class="frame" style="display:none">
                            <input type="hidden" name="id" value="${price?.id}">
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Age Group</label>
                                        <g:select id="age-group" name="ageGroup"
                                        class="form-control input-flat"
                                        from="${['Adult', 'Child', 'Infant', 'Senior']}"
                                        value="${price?.ageGroup}"
                                        keys="${['Adult', 'Child', 'Infant', 'Senior']}"
                                        optionValue=""
                                        noSelection="['':'Select age group']" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Glide Type</label>
                                        <g:select id="glide-type" name="listingType"
                                        class="form-control input-flat"
                                        from="${listingType}"
                                        value="${price?.type}"
                                        optionKey="type"
                                        optionValue="type"
                                        noSelection="['':'Select glide type']" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Duration</label>
                                        <g:select id="duration" name="duration"
                                        class="form-control input-flat"
                                        from="${listingDuration}"
                                        value="${price?.duration}"
                                        optionKey="duration"
                                        optionValue="duration"
                                        noSelection="['':'Select duration']" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Amount(USD)</label>
                                        <input type="number" name="amount" value="${price?.amount}" placeholder="Amount" class="form-control input-flat"  onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Multimedia(USD)</label>
                                        <input type="number" name="multimediaCharge" value="${price?.multimediaCharge}" placeholder="Multimedia Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Transportation(USD)</label>
                                        <input type="number" name="transportation" value="${price?.transportation}" placeholder="Transportation Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Amount (NPR)</label>
                                        <input type="number" name="amountInNrs" value="${price?.amountInNrs}" placeholder="Amount" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Multimedia (NPR)</label>
                                        <input type="number" name="multimediaChargeInNrs" value="${price?.multimediaChargeInNrs}" placeholder="Multimedia Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Transportation (NPR)</label>
                                        <input type="number" name="transportationInNrs" value="${price?.transportationInNrs}" placeholder="Transportation Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <a href="javascript:void(0)" class="btn btn-flat btn-danger remove-pricing"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>
                        </li>
                        <g:if test="${!listingPrice.isEmpty()}">
                        <g:each var="price" in="${listingPrice}">
                        <li>
                            <input type="hidden" name="id" value="${price?.id}">
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Age Group</label>
                                        <g:select id="age-group" name="ageGroup"
                                        class="form-control input-flat"
                                        from="${['Adult', 'Child', 'Infant', 'Senior']}"
                                        value="${price?.ageGroup}"
                                        keys="${['Adult', 'Child', 'Infant', 'Senior']}"
                                        optionValue=""
                                        noSelection="['':'Select age group']" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Glide Type</label>
                                        <g:select id="glide-type" name="listingType"
                                        class="form-control input-flat"
                                        from="${listingType}"
                                        value="${price?.type}"
                                        optionKey="type"
                                        optionValue="type"
                                        noSelection="['':'Select glide type']" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Duration</label>
                                        <g:select id="duration" name="duration"
                                        class="form-control input-flat"
                                        from="${listingDuration}"
                                        value="${price?.duration}"
                                        optionKey="duration"
                                        optionValue="duration"
                                        noSelection="['':'Select duration']" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Amount(USD)</label>
                                        <input type="number" name="amount" value="${price?.amount}" placeholder="Amount" class="form-control input-flat" required="required" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Multimedia(USD)</label>
                                        <input type="number" name="multimediaCharge" value="${price?.multimediaCharge}" placeholder="Multimedia Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Transportation(USD)</label>
                                        <input type="number" name="transportation" value="${price?.transportation}" placeholder="Transportation Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Amount (NPR)</label>
                                        <input type="number" name="amountInNrs" value="${price?.amountInNrs}" placeholder="Amount" class="form-control input-flat" required="required" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Multimedia (NPR)</label>
                                        <input type="number" name="multimediaChargeInNrs" value="${price?.multimediaChargeInNrs}" placeholder="Multimedia Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Transportation (NPR)</label>
                                        <input type="number" name="transportationInNrs" value="${price?.transportationInNrs}" placeholder="Transportation Charge (optional)" class="form-control input-flat" onBlur="isNumber($(this))" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <a href="javascript:void(0)" class="btn btn-flat btn-danger remove-pricing"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>
                        </li>
                    </g:each>
                </g:if>
            </ul>
        </div>
        <div class="col-md-12">
            <a id="add-more" class="btn btn-flat btn-default" href="javascript:void(0)"><i class="fa fa-plus"></i> Add More</a>
        </div>
    </div>
</div>
<div class="panel-footer clearfix">
    <g:submitButton id="submit-btn" name="Save" class="btn btn-flat btn-primary pull-right" value="${message(code: 'default.button.save.label', default: 'Save')}" />
</div>
</div>
</g:form>
</div>
</div>
</div>
<script>

    function isNumber(input){
        var val = input.val();
        $("#submit-btn").attr('disabled', 'disabled');
        if($.isNumeric( val)){
            $("#submit-btn").removeAttr('disabled');
            return true;
        }else{
            alert ('Invalid amount');
            return false;
        }

    }
    jQuery(document).ready(function(){
        var tableCountRow = $('.price-list li:visible').length;
        if(tableCountRow < 1){
            $("#submit-btn").attr('disabled', 'disabled');
            $('.alert').remove();
            $('.content-wrapper').prepend('<div class="alert alert-warning alert-flat fade in alert-dismissable"><i class="fa fa-warning text-warning"></i> Please! add atleast one package price.</div>');
            return false;
        }
        $(".price-form").validate({
            rules: {

            },
            messages: {

            }
        });
    });
    $(document).ready(function(){
        $('#add-more').click(function(){
            var tableCountRow = $('.price-list li:visible').length;
            if(tableCountRow < 10){
                var clone = $(".price-list .frame").clone().find('input').end().insertAfter(".price-list li:last");
                $('.price-list li:last').removeAttr('class').show();
                $(".price-list li:last").find('select option').prop('selected', false)
            }else{
                alert("You have reach the maximun limit 10");
            }
        })
    });

    $('body').on('click','.remove-pricing', function(){
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).closest('li').find("input[name='id']").val();
            if(id != ''){
                $.ajax({
                    url: "${createLink(action: 'removePricing')}",
                    type:"post",
                    data:{id: id},
                    success: function(data) {
                        //console.log(data); //<-----this logs the data in browser's console
                        if(data == 'success'){
                            $('body').find("input[value="+id+"]").closest('li').remove();
                        }else if(data == 'bookingItem'){
                            alert('Cannot delete pricing. Booking record exists.');
                        }
                    },
                    error: function(xhr){
                        // alert(xhr.responseText); //<----when no data alert the err msg
                    }
                });
            }else{
                var rowCount = '';
                rowCount = $('.price-list li').length;
                //if(rowCount > 2)
                $(this).closest('li').remove();
            }
            $("#submit-btn").removeAttr('disabled');
        }
    });
</script>
</body>
</html>
