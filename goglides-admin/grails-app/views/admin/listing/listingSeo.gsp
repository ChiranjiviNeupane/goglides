<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017" />
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>

<div class="page-title">
    <div class="title_left">
        <h3>Seo Detail</h3>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="ccol-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="x_panel">
            <div class="x_title">
                <h2> ${listing?.title}</h2>

                <div class="clearfix"></div>
            </div><br/>
            <div class="x_content">
                <g:form action="updateListingSEO" method="post" class="form">
                    <g:hasErrors bean="${this.listing}">
                        <ul class="alert alert-danger" role="alert">
                            <g:eachError bean="${this.listing}" var="error">
                                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                        error="${error}"/></li>
                            </g:eachError>
                        </ul>
                    </g:hasErrors>
                    <input type="hidden" name="listingId" value="${listing?.id}"/>
                    <div class="text-muted"><label for="Meta Title">Meta Title</label></div>

                    <div class="form-group">
                        <input id="metaTitle" class="form-control" type="text"
                               name="metaTitle" value="${listing?.metaTitle}" placeholder="Meta Title"
                               required="required">
                    </div>
                    <div class="text-muted"><label for="Meta KeyWord">Meta KeyWord</label></div>

                    <div class="form-group">
                        <input id="metaKeyword" class="form-control" type="text"
                               name="metaKeyword" value="${listing?.metaKeyword}" placeholder="Meta KeyWord "
                               required="required">
                    </div>
                    <div class="text-muted"><label for="rate">Meta Description</label></div>

                    <div class="form-group">
                        <textarea id="metaDescription" class="form-control" type="text"
                               name="metaDescription" placeholder="Meta Description"
                                  required="required" rows="5" maxlength="999">${listing?.metaDescription}</textarea>
                    </div>
                    <div class=" clearfix">
                        <input type="submit" class="btn bttn pull-right" value="Save">
                    </div>

                </g:form>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <g:render template="/admin/listing/templates/listing_summary" model="[listing: listing]"/>
    </div>
</div>


</body>
</html>
