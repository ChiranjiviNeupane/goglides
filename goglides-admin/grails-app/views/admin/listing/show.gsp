<g:set var="siteDropdownService" bean="siteDropdownService"/>
<g:set var="amazonS3Service" bean="amazonS3Service"/>

%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use WishListService--}%
<g:set var="wishListSer" bean="wishListService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
<g:set var="pageTitle" value="${listing?.title}"/>
<g:set var="pageSubtitle" value="view listing detail information"/>
<g:set var="imageProcessingService" bean="imageProcessingService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>${pageTitle}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(uri: '/admin/listing/create')}" class="btn bttn"><i
                    class="fa fa-plus"></i> Add New Listing</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="x_panel">
            <div class="x_title">
                <h2>${pageTitle} <small>${pageSubtitle}</small></h2>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab_content1" id="basic-tab" role="tab"
                               data-toggle="tab" aria-expanded="false">Basic</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content2" role="tab" id="rule-tab"
                               data-toggle="tab" aria-expanded="false">Rules</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content3" role="tab" id="package-tab"
                               data-toggle="tab" aria-expanded="true">Packages</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content4" role="tab" id="service-tab"
                               data-toggle="tab" aria-expanded="true">Services</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content5" role="tab" id="policy-tab"
                               data-toggle="tab" aria-expanded="true">Policies</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content6" role="tab" id="seo-tab"
                               data-toggle="tab" aria-expanded="true">SEO</a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content7" role="tab" id="multimedia-tab"
                               data-toggle="tab" aria-expanded="true">Multimedia</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="listing-view">
            <div id="myTabContent" class="tab-content listing-view">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                     aria-labelledby="basic-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Basic Information
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing/edit/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                </h4>
                            </div>

                            <div class="col-sm-3">
                                <div class="img-box"
                                     style="background-image:url('${listingService.getListingFeatureImage(listing?.id)}');"></div>
                                <a href="${createLink(uri: '/admin/listing/listing-multimedia/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}">Edit <i
                                        class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                            </div>

                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Title:</label></div>

                                    <div class="col-xs-12 col-sm-9">${listing?.title}</div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Type:</label></div>

                                    <div class="col-xs-12 col-sm-9">${listing?.category?.categoryTitle}</div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Nearest City:</label></div>

                                    <div class="col-xs-12 col-sm-9">${listing?.nearestCity}</div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Nearest Airport:</label></div>

                                    <div class="col-xs-12 col-sm-9">${listing?.nearestAirport}</div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Is Featured:</label></div>

                                    <div class="col-xs-12 col-sm-9"><span
                                            class="text-capitalize">${listing?.isFeatured ?: 'No'}</span></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Highlights
                                    <a href="${createLink(uri: '/admin/listing/description/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                                       class="pull-right" data-toggle="tooltip" title="Edit">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                </h4>
                                ${raw(listing?.highlights)}
                            </div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Listing Description
                                    <a href="${createLink(uri: '/admin/listing/description/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                                       class="pull-right" data-toggle="tooltip" title="Edit">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                </h4>

                                ${raw(listing?.description)}
                            </div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="rule-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12"><h4>Product rules
                                <a class="pull-right" data-toggle="tooltip" title="Edit"
                                   href="${createLink(uri: '/admin/listing/set-rules/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}">
                                    <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4></div>

                            <div class="col-sm-6">
                                <p><strong>Min Age</strong> - ${listingRestrictions?.minAge ?: 'N.A.'}yrs</p>

                                <p><strong>Max Age</strong> - ${listingRestrictions?.maxAge ?: 'N.A.'}yrs</p>
                            </div>

                            <div class="col-sm-6">
                                <p><strong>Min Weight</strong> - ${listingRestrictions?.minWeight ?: 'N.A.'}kg</p>

                                <p><strong>Max Weight</strong> - ${listingRestrictions?.maxWeight ?: 'N.A.'}kg</p>
                            </div>

                            <div class="col-sm-6">
                                <p><strong>Max No./booking</strong> - ${llistingRestrictions?.maxPeople ?: 'N.A.'}pax
                                </p>
                            </div>

                            <div class="col-sm-6">
                                <p><strong>Max Capacity/group</strong> - ${listingRestrictions?.maxCapacity ?: 'N.A.'}pax
                                </p>
                            </div>

                            <div class="col-sm-6">
                                <p><strong>Min No./booking</strong> - ${listingRestrictions?.minPeople ?: 'N.A.'}pax
                                </p>
                            </div>

                            <div class="col-sm-6">
                                <p><strong>Availability Hours</strong> - ${listingRestrictions?.availabilityHour + ' ' + listingRestrictions?.availabilityUnit}
                                </p>
                            </div>

                            <div class="col-sm-12">
                                <strong>Booking Conformation Method</strong> - ${listingRestrictions?.bookingConfirmation}
                            </div>

                            <div class="col-xs-12"><hr/></div>

                            <div class="col-sm-12">
                                <table id="table" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Infant (in yrs)</th>
                                        <th>Child (in yrs)</th>
                                        <th>Adult (in yrs)</th>
                                        <th>Senior (in yrs)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>${listingAgeGroup?.infantMin + ' - ' + listingAgeGroup?.infantMax}</td>
                                        <td>${listingAgeGroup?.childMin + ' - ' + listingAgeGroup?.childMax}</td>
                                        <td>${listingAgeGroup?.adultMin + ' - ' + listingAgeGroup?.adultMax}</td>
                                        <td>${listingAgeGroup?.seniorMin + ' - ' + listingAgeGroup?.seniorMax}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                     aria-labelledby="package-tab">
                    <section class="all-packages view-content x_panel">
                        <h4>Package Lists
                            <a class="pull-right" data-toggle="tooltip" title="Edit"
                               href="${createLink(uri: '/admin/listing/listing-package-list/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}">
                                <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                        </h4>

                        <g:if test="${listingPackage}">

                            <g:each in="${listingPackage}">
                                <div class="pkg-list">
                                    <a href="${createLink(uri: '/admin/listing-package/view/' + it?.packageCode)}">
                                        ${it?.tripTitle}
                                    </a>
                                </div>
                            </g:each>
                        </g:if>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content4"
                     aria-labelledby="service-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Pick-up/ Drop off
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(uri: '/admin/listing/listing-pickup/' + listing?.referenceNo, params: ['returnurl': 'view'])}">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>

                                <g:if test="${listingPickup}">
                                    <ul>
                                        <g:each in="${listingPickup}">
                                            <li>${it?.address}</li>
                                        </g:each>
                                    </ul>
                                </g:if>

                            </div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>What we provide
                                    <a href="${createLink(uri: '/admin/listing/listing-checklist/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                                       class="pull-right" data-toggle="tooltip" title="Edit">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                                <g:if test="${listingCheckListProvide}">
                                    <ul>
                                        <g:each in="${listingCheckListProvide}">
                                            <li>${it?.checklistItem}</li>
                                        </g:each>
                                    </ul>
                                </g:if>
                            </div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>What to bring
                                    <a href="${createLink(uri: '/admin/listing/listing-checklist/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                                       class="pull-right" data-toggle="tooltip" title="Edit">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>

                                <g:if test="${listingCheckListBring}">
                                    <ul>
                                        <g:each in="${listingCheckListBring}">
                                            <li>${it?.checklistItem}</li>
                                        </g:each>
                                    </ul>
                                </g:if>
                            </div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content5"
                     aria-labelledby="policy-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Insurance Policies
                                    <a href="${createLink(uri: '/admin/listing/listing-policies/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                                       class="pull-right" data-toggle="tooltip" title="Edit">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>

                                <g:if test="${listingPolicies?.hasInsuranceCoverage == 'yes'}">
                                    <label>Insurance VIA ${listingPolicies?.insuranceCompany}</label>

                                    <p>Accidental insurance up to ${listingPolicies?.accidentalInsurance}</p>

                                    <p>Death insurance up to ${listingPolicies?.deathInsurance}</p>
                                    <g:if test="${insuranceDocument}">
                                        <p><a href="${listingService.getInsuranceDocument(insuranceDocument?.file)}"
                                              target="_blank"><i class="fa fa-file"></i> Insurance Document</a></p>
                                    </g:if>
                                </g:if>
                            </div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Cancellation Policies
                                    <a href="${createLink(uri: '/admin/listing/listing-policies/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                                       class="pull-right" data-toggle="tooltip" title="Edit">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>

                                <g:if test="${listingCancellationPolicies}">
                                    <g:each in="${listingCancellationPolicies}">
                                        <p><label>${it?.policyTitle}</label></p>

                                        <p>${it?.description}</p>
                                    </g:each>
                                </g:if>
                            </div>
                        </div>
                    </section>
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>GoGlides Standard Cancellation Policies</h4>
                                <g:render template="/admin/listing/templates/policies"/>
                            </div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content6"
                     aria-labelledby="seo-tab">
                    <section class="view-content x_panel">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>SEO
                                    <a href="${createLink(uri: '/admin/listing/seo/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                                       class="pull-right" data-toggle="tooltip" title="Edit">
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-3"><label>Meta Title:</label></div>

                            <div class="col-xs-12 col-sm-9">${listing?.metaTitle}</div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-3"><label>Meta Keywords:</label></div>

                            <div class="col-xs-12 col-sm-9">${listing?.metaKeyword}</div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-3"><label>Meta Description:</label></div>

                            <div class="col-xs-12 col-sm-9">${raw(listing?.metaDescription)}</div>
                        </div>
                    </section>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content7"
                     aria-labelledby="multimedia-tab">
                    <section class="view-content x_panel">
                        <h4>Multimedia
                            <a href="${createLink(uri: '/admin/listing/listing-multimedia/', params: ['id': listing?.referenceNo, 'returnurl': 'view'])}"
                               class="pull-right" data-toggle="tooltip" title="Edit">
                                <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a></h4>

                        <div class="row">
                            <g:if test="${listingVideo}">
                                <div class="col-xs-12 col-sm-4"><label>Video Link:</label></div>

                                <div class="col-xs-12 col-sm-8">
                                    <div class="multi-field-wrapper">
                                        <div class="multi-fields">
                                            <div class="multi-field">
                                                <div class="form-group">
                                                    <a href="${listingVideo?.file}"><h2>${listingVideo?.file}<h2/></a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </g:if>
                        </div>

                        <div class="row">
                            <g:if test="${featureImage}">
                                <div class="col-xs-12 col-sm-3"><label>Featured Image:</label></div>

                                <div class="col-sm-offset-1 col-sm-5">

                                    <div class="preview">
                                        <img src="${amazonS3Service.destinationBucketCDN() + imageProcessingService.listingFeaturedDestionUrl(featureImage?.file,'feature')}"
                                             class="img-responsive" id="featureImage">
                                    </div>

                                    <script>

                                        function checkFeatureImage(URL) {
                                            var tester=new Image(URL);
                                            tester.onerror=function (ev) {
                                                $('#featureImage').attr('src', '${amazonS3Service.sourceBucketCDN()+featureImage?.file}');
                                                featureImageNotFound('${featureImage?.file}','feature','${amazonS3Service.destinationBucketCDN()+imageProcessingService.listingFeaturedDestionUrl(feature?.file,'feature')}')
                                            }
                                            tester.src=URL;
                                        }
                                        checkFeatureImage('${amazonS3Service.destinationBucketCDN()+imageProcessingService.listingGallerydDestionUrl(gallery?.file,'gallery')}')

                                    </script>

                                </div>
                            </g:if>
                        </div><br/><br/>

                        <div class="row">
                            <g:if test="${galleryImages}">
                                <div class="col-xs-12 col-sm-3"><label>Other Image:</label></div>

                                <div class="col-sm-9 col-lg-9 col-md-9">

                                    <div class="galleryImages">
                                        <g:each in="${galleryImages}" status="i" var="gallery">
                                            <div class="col-sm-3 col-md-3 col-lg-3" style="height: 130px">
                                                <a href="${amazonS3Service.sourceBucketCDN() + gallery?.file}"
                                                   target="_blank"><img
                                                        src="${amazonS3Service.destinationBucketCDN() + imageProcessingService.listingGallerydDestionUrl(gallery?.file,'gallery')}"
                                                        class="img-responsive" style="height: 120px" id="${gallery?.id}"></a>

                                                <script>

                                                    function imageCheck(URL) {
                                                        var tester=new Image(URL);
                                                        tester.onerror=function (ev) {
                                                            $('#${gallery?.id}').attr('src', '${amazonS3Service.sourceBucketCDN()+gallery?.file}');
                                                            galleryResizImageNotFound('${gallery?.file}','gallery','${amazonS3Service.destinationBucketCDN()+imageProcessingService.listingGallerydDestionUrl(gallery?.file,'gallery')}')
                                                        }
                                                        tester.src=URL;
                                                    }
                                                    imageCheck('${amazonS3Service.destinationBucketCDN()+imageProcessingService.listingGallerydDestionUrl(gallery?.file,'gallery')}')

                                                </script>


                                            </div>
                                        </g:each>
                                    </div>

                                </div>
                            </g:if>
                        </div>


                        <div class="row">
                            <g:if test="${listingRoute}">
                                <div class="col-xs-12 col-sm-3"><label>Route MAP:</label></div>

                                <div class="col-sm-offset-1 col-sm-5">

                                    <div class="preview">
                                        <img src="${amazonS3Service.sourceBucketCDN() + listingRoute?.file}"
                                             class="img-responsive">
                                    </div>

                                </div>
                            </g:if>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <div class="x_panel">
            <div class="x_title">
                <h2>Actions</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <ul class="list-unstyled action-wrap">
                    <li>
                        <a class="btn btn-dark col-xs-12"
                           href="${createLink(uri: '/admin/listing/manage-settings?id=' + listing?.referenceNo)}"><i
                                class="glyphicon glyphicon-edit"></i> Edit Settings</a>
                    </li>
                    <li>
                        <a class="btn btn-dark col-xs-12" href="javascript:void(0);" onclick="statusUpdate('draft')"><i
                                class="fa fa-file-text"></i> Save as Draft</a>
                    </li>
                    <li>
                        <a class="btn btn-success col-xs-12" href="javascript:void(0);"
                           onclick="statusUpdate('publish')"><i class="fa fa-check"></i> Approve and Publish</a>
                    </li>
                    <li>
                        <a class="btn btn-danger col-xs-12"
                           href="${createLink(uri: '/admin/listing/delete?id=' + listing?.referenceNo)}"
                           onclick="return confirm('Are you sure you want to delete this item?')"><i
                                class="fa fa-trash-o"></i> Trash</a>
                    </li>
                    <li>
                        <label class="">Additional:</label>
                        <g:select name="${listing?.status}"
                                  class="form-control"
                                  from="['Pending', 'Awaiting Review', 'Reviewing', 'Review Completed', 'Awaiting Approval']"
                                  keys="['pending', 'awaiting_review', 'reviewing', 'review_completed', 'awaiting_approval']"
                                  value="${listing?.status}"
                                  onchange="statusUpdate(this.value)"
                                  noSelection="['': 'Select status ...']"/>
                    </li>
                </ul>
            </div>
        </div>
        <g:render template="/admin/listing/templates/listing_summary" model="[listing: listing]"/>
    </div>
</div>
<script>

    function statusUpdate(status) {
        if (status != '' && confirm("${message(code: 'default.confirm.message', default: 'Are you sure?')}")) {
            $("#divLoading").addClass('show');
            $.ajax({
                url: "${createLink(uri: '/admin/listing/update-status')}",
                type: "post",
                data: {id: '${listing?.referenceNo}', value: status},
                success: function (data) {
                    if (data == 'success') {

                    }
                },
                error: function (xhr) {

                },
                complete: function () {
                    $("#divLoading").removeClass('show');
                    location.reload();
                }
            });
        }
    }



    function featureImageNotFound(featureImage,imageType, destinationImageUrl) {
        var imageUrl='${amazonS3Service.destinationBucketCDN()+featureImage?.file}'
        if(imageUrl==destinationImageUrl){
            return false;
        }
        else {
            $('#featureImage').attr('src','${amazonS3Service.sourceBucketCDN()+featureImage?.file}')
            var URL="${createLink(controller:'imageProcessing',action:'createNewListingFeaturedImage')}"
            $.ajax({
                    url:URL,
                    type:"POST",
                    data:{id:'${listing?.referenceNo}',
                        imageType:'feature'

                    },
                    success: function (resp) {
                        console.log("resp "+resp)
                        if(resp=="success"){
                            console.log("success")
                        }else {
                            console.log("failed...")
                        }
                    }
                }
            );
        }

    }

    function galleryResizImageNotFound(galleryImage, imageType, destinationUrl) {
        var imageUrl = '${amazonS3Service.destinationBucketCDN()}' + null

        if (imageUrl == destinationUrl) {
            return false;
        }
        else {
            var URL = "${createLink(controller:'imageProcessing',action:'createNewListingGalleryImage')}"
            $.ajax({
                    url: URL,
                    type: "POST",
                    data: {
                        id: galleryImage,
                        imageType: imageType

                    },

                    success: function (resp) {
                        console.log("resp " + resp)
                        if (resp == "success") {
                            console.log("success")
                        } else {
                            alert("failed...")
                        }
                    }
                }
            );
        }
    }



</script>

</body>
</html>