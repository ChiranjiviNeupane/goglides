%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'listing.label', default: 'Listing')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h2>Listing - ${listing?.title}</h2>
        </div>
    </div>

    <div class="clearfix"></div>

    <g:render template="/admin/listing/templates/step_counter" model="[currentStep: '9']"/>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Great! You have filled in all the details.</h3>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <g:form class="form" action="saveListingCompleted">
                        <g:hasErrors bean="${this.listing}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.listing}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                            error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>
                        <fieldset>
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3></h3>

                                    <p>You can now submit your product to <strong>GoGlides</strong> for review, and following approval, your product will be published.
                                    </p>
                                </div>

                                <div class="form-top-right">
                                    <i class="fa fa-list" aria-hidden="true"></i>
                                </div>
                            </div>

                            <div class="form-bottom">
                                <input type="hidden" name="listingReferenceNo" value="${listing?.referenceNo}">
                            </div>

                            <div class="form-group text-right border-top">
                                <button type="button" class="btn btn-previous bttn"
                                        onclick="location.href = '${createLink(uri: '/admin/listing/listing-policies', params: [id: listing?.referenceNo])}';">Previous</button>

                                <button type="submit" class="btn btn-next bttn">Approve and Publish</button>
                            </div>
                        </fieldset>
                    </g:form>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>