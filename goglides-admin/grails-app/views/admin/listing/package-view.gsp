%{--Use PriceService--}%
<g:set var="listingService" bean="listingService"/>

<g:set var="priceService" bean="priceService"/>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>

    <h3 class="modal-title" id="myModalLabel">Package Edit - ${listingPackage?.tripTitle}</h3>
</div>

<div class="modal-body">
    <section class="view-content">
        <div class="row">
            <div class="col-sm-12">
                <h4>Product details <a class="pull-right"
                                       href="${createLink(uri: '/admin/listing/listing-package-edit/', params: ['id': listing?.referenceNo, 'package': listingPackage?.referenceNo, 'returnurl': 'view'])}">Edit
                    <i class="fa fa-edit" aria-hidden="true"></i></a></h4>
            </div>

            <div class="col-sm-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#01" aria-controls="embed" role="tab" data-toggle="tab">Short Itinerary</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#02" aria-controls="embed" role="tab" data-toggle="tab">Detail Itinerary</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#03" aria-controls="embed" role="tab" data-toggle="tab">Includes</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#04" aria-controls="embed" role="tab" data-toggle="tab">Excludes</a>
                    </li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="01">
                        ${raw(listingPackage?.shortItinerary)}
                    </div>

                    <div role="tabpanel" class="tab-pane" id="02">
                        ${raw(listingPackage?.detailItinerary)}
                    </div>

                    <div role="tabpanel" class="tab-pane" id="03">
                        ${raw(listingPackage?.tripIncludes)}
                    </div>

                    <div role="tabpanel" class="tab-pane" id="04">
                        ${raw(listingPackage?.tripExcludes)}
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="view-content x_panel">
        <div class="row">
            <div class="col-sm-12"><h4>Set Date
                <a class="pull-right"
                   href="${createLink(uri: '/admin/listing/listing-package-options/', params: ['id': listing?.referenceNo, 'package': listingPackage?.referenceNo, 'returnurl': 'view'])}">Edit
                    <i class="fa fa-edit" aria-hidden="true"></i></a></h4></div>

            <div class="col-sm-12">
                <strong>Trip availability date</strong>
                <g:if test="${listingAvailability?.availabilityType == 'date-single'}">
                    <p><g:formatDate date="${listingAvailability?.fromDate}" type="datetime" style="MEDIUM"/></p>
                </g:if>
                <g:else>
                    <p><g:formatDate date="${listingAvailability?.fromDate}" type="datetime"
                                     style="MEDIUM"/> - <g:formatDate date="${listingAvailability?.toDate}"
                                                                      type="datetime" style="MEDIUM"/></p>
                </g:else>
            </div>

            <div class="col-sm-12">
                <strong>Trip availability day(s)</strong>

                <table id="table" class="table">
                    <tr>
                        <th class="text-center">Days</th>
                        <th class="text-center">Monday</th>
                        <th class="text-center">Tuesday</th>
                        <th class="text-center">Wednesday</th>
                        <th class="text-center">Thursday</th>
                        <th class="text-center">Friday</th>
                        <th class="text-center">Saturday</th>
                        <th class="text-center">Sunday</th>
                    </tr>
                    <tr>
                        <th data-th="Days">Availability</th>
                        <td data-th="Mon"
                            class="text-center">${raw(listingAvailabilityDays?.monday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                        <td data-th="Tue"
                            class="text-center">${raw(listingAvailabilityDays?.tuesday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                        <td data-th="Wed"
                            class="text-center">${raw(listingAvailabilityDays?.wednesday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                        <td data-th="Thu"
                            class="text-center">${raw(listingAvailabilityDays?.thursday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                        <td data-th="Fri"
                            class="text-center">${raw(listingAvailabilityDays?.friday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                        <td data-th="Sat"
                            class="text-center">${raw(listingAvailabilityDays?.saturday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                        <td data-th="Sun"
                            class="text-center">${raw(listingAvailabilityDays?.sunday ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>')}</td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-6">
                <strong>Trip Duration</strong>
                <ul>
                    <g:if test="${listingDurations}">
                        <g:each in="${listingDurations}">
                            <g:set var="format" value="${listingService.formatDuration(it?.duration)}"/>

                            <li>${format.d}</li>
                        </g:each>
                    </g:if>
                </ul>
            </div>

            <div class="col-sm-6">
                <strong>Trip Schedule</strong>
                <g:if test="${listingSchedules}">
                    <g:each in="${listingSchedules}">
                        <li>${listingService.formatSchedule(it?.schedule)}</li>
                        %{--<li>${it?.schedule}</li>--}%
                    </g:each>
                </g:if>
            </div>
        </div>
    </section>
    <section class="view-content x_panel">
        <div class="row">
            <div class="col-sm-12"><h4>Set Pricing
                <a class="pull-right"
                   href="${createLink(uri: '/admin/listing/listing-package-pricing/', params: ['id': listing?.referenceNo, 'package': listingPackage?.referenceNo, 'returnurl': 'view'])}">Edit
                    <i class="fa fa-edit" aria-hidden="true"></i></a></h4></div>
        </div>

        <g:if test="${packagePrice}">
            <g:each in="${packagePrice}">
                <div class="row">
                    <div class="col-sm-2">
                        <strong>Age Group</strong>

                        <p>${it?.ageGroup}</p>
                    </div>
                    <g:if test="${it?.type}">
                        <div class="col-sm-2">
                            <strong>Glide Type</strong>

                            <p>${it?.type}</p>
                        </div>
                    </g:if>
                    <g:if test="${it?.duration}">
                        <div class="col-sm-2">
                            <strong>Duration</strong>
                            <g:set var="format" value="${listingService.formatDuration(it?.duration)}"/>


                            <p>${format.d}</p>
                        </div>
                    </g:if>
                    <g:if test="${it?.schedule}">
                        <div class="col-sm-2">
                            <strong>Schedule</strong>

                            <p>${it?.schedule}</p>
                        </div>
                    </g:if>
                    <div class="col-sm-2">
                        <strong>Wholesale Rate</strong>

                        <p>${raw(priceService.getFormattedPrice(it?.wholeSaleRate))}</p>
                    </div>

                    <div class="col-sm-2">
                        <strong>Retail Rate</strong>

                        <p>${raw(priceService.getFormattedPrice(it?.retailRate))}</p>
                    </div>
                </div>
            </g:each>
        </g:if>
    </section>
    <section class="view-content x_panel">
        <div class="row">
            <div class="col-sm-12">
                <h4>Add-on Services <a class="pull-right"
                                       href="${createLink(uri: '/admin/listing/listing-addons/', params: ['id': listing?.referenceNo, 'package': listingPackage?.referenceNo, 'returnurl': 'view'])}">Edit
                    <i class="fa fa-edit" aria-hidden="true"></i></a></h4>
            </div>

            <div class="col-sm-6">
                <label>Multimedia</label>
                <g:if test="${listingAddons?.includedMultimedia == 'no'}">
                    <g:each in="${listingAddonMultimedia}">
                        <div class="row">
                            <div class="col-sm-3">
                                <strong>Age Group</strong>

                                <p>${it?.ageGroup}</p>
                            </div>

                            <div class="col-sm-4">
                                <strong>Wholesale Rate</strong>

                                <p>${raw(priceService.getFormattedPrice(it?.wholesaleRate))}</p>
                            </div>

                            <div class="col-sm-3">
                                <strong>Retail Rate</strong>

                                <p>${raw(priceService.getFormattedPrice(it?.retailRate))}</p>
                            </div>
                        </div>
                    </g:each>
                </g:if>
                <g:else>
                    <p>Multimedia Cost included on above rate</p>
                </g:else>
            </div>

            <div class="col-sm-6">
                <label>Trasport</label>
                <g:if test="${listingAddons?.includedTransport == 'no'}">
                    <g:each in="${listingAddonTransport}">
                        <div class="row">
                            <div class="col-sm-3">
                                <strong>Age Group</strong>

                                <p>${it?.ageGroup}</p>
                            </div>

                            <div class="col-sm-4">
                                <strong>Wholesale Rate</strong>

                                <p>${raw(priceService.getFormattedPrice(it?.wholesaleRate))}</p>
                            </div>

                            <div class="col-sm-3">
                                <strong>Retail Rate</strong>

                                <p>${raw(priceService.getFormattedPrice(it?.retailRate))}</p>
                            </div>
                        </div>
                    </g:each>
                </g:if>
                <g:else>
                    <p>Transportation Cost included on above rate</p>
                </g:else>
            </div>
        </div>
    </section>
    <g:if test="${listingAddons?.isDiscountApplicable == 'yes'}">
        <section class="view-content x_panel">
        <div class="row">
            <div class="col-sm-12">
                <h4>Group Discount Policy <a class="pull-right"
                                       href="${createLink(uri: '/listing/listing-group-discount/', params: ['id': listing?.referenceNo, 'package': listingPackage?.referenceNo, 'returnurl': 'view'])}">Edit
        <i class="fa fa-edit" aria-hidden="true"></i></a></h4>

        <g:if test="${discountPolicies}">
            <g:each in="${discountPolicies}" var="policy" status="i">
                <div class="row">
                    <div class="col-sm-6">
                        <strong>Group Policy</strong>

                        <p>Policy ${i + 1}</p>
                    </div>

                    <div class="col-sm-3">
                        <strong>Group Size</strong>

                        <p>${policy?.discountFrom} - ${policy?.discountTo}</p>
                    </div>

                    <div class="col-sm-3">
                        <strong>Discount in %</strong>

                        <p>${policy?.discountPercent}</p>
                    </div>
                </div>
            </g:each>
        </g:if>
        </div>
    </div>
    </g:if>
</div>
