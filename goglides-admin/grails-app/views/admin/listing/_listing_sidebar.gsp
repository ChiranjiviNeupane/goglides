<aside class="sidebar sidebar-main col-md-3">
    <h2>New Listing</h2>
    <ul class="sidebar-menu">
        <li class="${actionName == 'edit' ? 'active' : '' }"><a href="${createLink(action: 'edit', id: listing?.id)}">Listing Basic Info</a></li>
        <li class="${actionName == 'description' ? 'active' : '' }"><a href="${createLink(action: 'description', id: listing?.id)}">Listing Description</a></li>
        <li class="${actionName == 'multimedia' ? 'active' : '' }"><a href="${createLink(action: 'multimedia', id: listing?.id)}">Listing Multimedia</a></li>
        <li class="${actionName == 'setting' ? 'active' : '' }"><a href="${createLink(action: 'setting', id: listing?.id)}">Listing Setting</a></li>
        <li class="${actionName == 'pricing' ? 'active' : '' }"><a href="${createLink(action: 'pricing', id: listing?.id)}">Listing Pricing</a></li>
        <li class="${actionName == 'policy' ? 'active' : '' }"><a href="${createLink(action: 'policy', id: listing?.id)}">Listing Policy</a></li>
    </ul>
    <a href="${createLink(action: 'create')}" class="btn btn-flat btn-primary btn-lg">Add New Listing</a>
</aside>