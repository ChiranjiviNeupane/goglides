<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'siteSettings.label', default: 'SiteSettings')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>General Settings</h2>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Recent Bookings</h3>
            </div>

            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>

                            <g:sortableColumn property="title"
                                              title="${message(code: 'siteSettings.parameterName.label', default: 'Title')}"
                                              params="[namespace: '']"/>

                            <g:sortableColumn property="value"
                                              title="${message(code: 'siteSettings.parameterValue.label', default: 'Value')}"
                                              params="[namespace: '']"/>

                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${siteSettingsList}" status="i"
                                var="siteSetting">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                <td><g:link action="edit"
                                            id="${siteSetting.id}">${fieldValue(bean: siteSetting, field: "title")}</g:link></td>
                                <td>${fieldValue(bean: siteSetting, field: "value")}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>

                    <div class="pagination">
                        <g:paginate total="${siteSettingsCount ?: 0}"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>