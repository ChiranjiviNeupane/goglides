<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'siteSettings.label', default: 'SiteSettings')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>General Settings</h2>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>${siteSettings?.title}</h3>
            </div>

            <div class="x_content">
                <f:display bean="siteSettings" except="title"/>
                <g:form resource="${this.siteSettings}" method="DELETE">
                    <fieldset class="form-group text-right border-top">
                        <g:link class="btn bttn" action="edit"
                                resource="${this.siteSettings}"><g:message code="default.button.edit.label"
                                                                           default="Edit"/></g:link>
                       <!--  <input class="delete btn btn-flat btn-danger" type="submit"
                               value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                               onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/> -->
                    </fieldset>
                </g:form>

            </div>
        </div>
    </div>
</div>
</body>
</html>
