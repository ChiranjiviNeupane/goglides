<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'siteSettings.label', default: 'SiteSettings')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>General Settings</h2>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Create</h3>
            </div>

            <div class="x_content">
                <g:hasErrors bean="${this.siteSettings}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.siteSettings}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
                <g:form class="create-form" action="save" controller="site-settings" namespace="admin">

                    <div class="form-group">
                        <label for="title">Title</label>
                        <g:textField name="title" value="${siteSettings?.title}"
                                     class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label for="value">Value</label>
                        <g:textField name="value" value="${siteSettings?.value}"
                                     class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label for="defaultValue">Default Value</label>
                        <g:textField name="defaultValue" value="${siteSettings?.defaultValue}"
                                     class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label for="value">Slug</label>
                        <g:textField name="slug" value="${siteSettings?.slug}"
                                     class="form-control"/>
                    </div>

                    <fieldset class="form-group text-right border-top">
                        <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                    </fieldset>
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
