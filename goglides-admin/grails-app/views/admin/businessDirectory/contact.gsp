<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'businessDirectory.label', default: 'BusinessDirectory')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Agent Profile - ${businessDirectory?.businessName}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Agent Detail</h3>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:form resource="${this.businessDirectory}" action="save-contact" method="POST" class="form">
                    <g:hiddenField name="version" value="${this.businessDirectory?.version}"/>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Step 4 / 5</h3>
                            <h5>Contact Person</h5>
                        </div>

                        <div class="form-top-right">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <g:hasErrors bean="${this.businessDirectory}">
                        <ul class="alert alert-danger" role="alert">
                            <g:eachError bean="${this.businessDirectory}" var="error">
                                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                        error="${error}"/></li>
                            </g:eachError>
                        </ul>
                    </g:hasErrors>
                    <div class="form-bottom clearfix">
                        <div class="form-group col-sm-3">
                            <label for="">Title <span class="req">*</span></label>
                            <input type="text" name="contactJobTitle" class="form-control"
                                   value="${businessDirectory?.contactJobTitle}" required="required"/>
                        </div>

                        <div class="form-group col-sm-3">
                            <label>First Name</label>
                            <input id="contactFirstName" class="form-control" type="text"
                                   name="contactFirstName" value="${businessDirectory?.contactFirstName}"
                                   placeholder="First Name" required="required">
                        </div>

                        <div class="form-group col-sm-3">
                            <label>Middle Name</label>
                            <input class="form-control" type="text"
                                   name="contactMiddleName input-lg input-flat"
                                   value="${businessDirectory?.contactMiddleName}" placeholder="Middle Name">
                        </div>

                        <div class="form-group col-sm-3">
                            <label>Last Name</label>
                            <input id="contactLastName" class="form-control" type="text"
                                   name="contactLastName" value="${businessDirectory?.contactLastName}"
                                   placeholder="Last Name" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Email</label>
                            <input id="contactEmail" class="form-control" type="email"
                                   name="contactEmail" value="${businessDirectory?.contactEmail}"
                                   placeholder="Email" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Phone</label>
                            <input id="contactPhone" class="form-control" type="text"
                                   name="contactPhone" value="${businessDirectory?.contactPhone}"
                                   placeholder="Phone 1" required="required"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                        </div>
                    </div>

                    <div class="form-group text-right border-top clearfix">
                        <a href="${createLink(action: 'viewmap', id: businessDirectory?.businessId)}"
                           class="btn bttn">Back</a>
                        <input type="submit" class="btn bttn" value="Next"/>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $().ready(function () {
        // validate the comment form when it is submitted
        $(".form0").validate({
            rules: {
                contactEmail: {
                    required: true,
                    email: true,
                    maxlength: 100
                },
                contactJobTitle: {
                    maxlength: 50
                },
                contactMiddleName: {
                    maxlength: 50
                },
                contactFirstName: {
                    maxlength: 50
                },
                contactLastName: {
                    maxlength: 50
                }
            },
            messages: {
                contactJobTitle: {
                    required: "Job Title field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                contactFirstName: {
                    required: "First Name field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                contactLastName: {
                    required: "Last Name field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                contactEmail: {
                    required: "Email field is required.",
                    email: "Please enter a valid email.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                contactPhone: {
                    required: "Phone field is required.",
                }
            }
        });
    });
</script>
</body>
</html>