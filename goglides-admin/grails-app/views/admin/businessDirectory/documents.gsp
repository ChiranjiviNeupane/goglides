%{--Use DirectoryService--}%
<g:set var="directoryService" bean="directoryService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
%{--Use CommonService--}%
<g:set var="commonService" bean="commonService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'businessDirectory.label', default: 'GoglidesBusinessDirectory')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Agent Profile - ${businessDirectory?.businessName}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Agent Detail</h3>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:form resource="${this.businessDirectory}" method="POST" enctype="multipart/form-data"
                        action="save-documents" class="form-horizontal">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Step 5 / 5</h3>
                            <h5>Contact Person</h5>
                        </div>

                        <div class="form-top-right">
                            <i class="fa fa-certificate"></i>
                        </div>
                    </div>
                    <g:hasErrors bean="${this.businessDirectory}">
                        <ul class="alert alert-danger" role="alert">
                            <g:eachError bean="${this.businessDirectory}" var="error">
                                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                        error="${error}"/></li>
                            </g:eachError>
                        </ul>
                    </g:hasErrors>

                    <g:hiddenField name="version" value="${this.businessDirectory?.version}"/>
                    <div class="form-bottom clearfix">
                        <div class="form-group col-sm-12">
                            <label>Logo</label>

                            <div id="fine-uploader-logo"></div>

                            <p class="text-muted">Official logo, share your logo with your customer.</p>

                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="db-logo">
                                        <g:if test="${logo?.id != null}">
                                            <li class="logo-${logo?.id} col-md-12"><div class="row"><div
                                                    class="col-md-1"><a
                                                        href="${commonService.getImageUrl(logo?.file)}"
                                                        target="_blank">Logo</a></div>

                                                <div class="col-md-3"><a href="javascript:void(0);"
                                                                         onclick="delLogo(${logo?.id}, '${logo?.file}')"><i
                                                            class="fa fa-trash"></i></a></div></div></li>
                                        </g:if>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <label>Company Certificates</label>
                            <input class="form-control input-lg" type="hidden" name="registeredDocuments">

                            <div id="fine-uploader-company-certificate"></div>

                            <p class="text-muted">This document is used for evaluation purpose.</p>

                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="db-comCert">
                                        <g:if test="${comCert?.id != null}">
                                            <g:each in="${comCert}" status="i" var="certificate">
                                                <li class="com-cert-${certificate?.id} col-md-12"><div class="row"><div
                                                        class="col-md-2"><a
                                                            href="${commonService.getImageUrl(certificate?.file)}"
                                                            target="_blank">Certificate ${i + 1}</a></div>

                                                    <div class="col-md-3"><a href="javascript:void(0);"
                                                                             onclick="delComCert(${certificate?.id}, '${certificate?.file}')"><i
                                                                class="fa fa-trash"></i></a></div></div></li>
                                            </g:each>
                                        </g:if>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <label>PAN/VAT or TIPN Certificates</label>
                            <input class="form-control input-lg" type="hidden" name="taxDocuments">

                            <div id="fine-uploader-tax-certificate"></div>

                            <p class="text-muted">This information is used for evaluation purpose.</p>

                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="db-taxCert">
                                        <g:if test="${taxCert?.id != null}">
                                            <g:each in="${taxCert}" status="i" var="certificate">
                                                <li class="tax-cert-${certificate?.id} col-md-12"><div class="row"><div
                                                        class="col-md-2"><a
                                                            href="${commonService.getImageUrl(certificate?.file)}"
                                                            target="_blank">Certificate ${i + 1}</a></div>

                                                    <div class="col-md-3"><a
                                                            onclick="delTaxCert(${certificate?.id}, '${certificate?.file}')"><i
                                                                class="fa fa-trash"></i></a></div></div></li>
                                            </g:each>
                                        </g:if>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <label>Other Affiliations (if any)</label>
                            <textarea class="form-control input-lg input-flat" name="affiliations"
                                      rows='5'>${businessDirectory?.affiliations}</textarea>

                            <p class="text-muted">Do you have any affiliation?</p>
                        </div>
                    </div>

                    <div class="form-group text-right border-top clearfix">
                        <a href="${createLink(action: 'contact', id: businessDirectory?.businessId)}"
                           class="btn bttn">Back</a>

                        <input type="submit" class="btn bttn" value="Finish"/>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>
<g:render template="/layouts/file_upload" model=""/>
<script>

    var alogo = [];
    var acompCertificate = [];
    var ataxCertificate = [];

    $('#fine-uploader-logo').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "businessDirectory", action: "logoUploadSuccess", namespace: "admin")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${businessDirectory?.id}
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key")}',
                    {
                        filename: filename,
                        directory: 'agent/logo'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                    console.log("logo:" + data);
                    alogo.push(data);
                    
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "businessDirectory", action: "deleteLogo", namespace: "admin")}"
        },
        validation: {
            itemLimit: 1,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png', 'pdf']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    $('#fine-uploader-company-certificate').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "businessDirectory", action: "compCertUploadSuccess", namespace: "admin")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${businessDirectory?.id}
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key")}',
                    {
                        filename: filename,
                        directory: 'agent/certificate'
                    }).done(function (data) {
                    keyRetrieval.success(data);
                    console.log("comp-certificate:" + data);
                    acompCertificate.push(data);
                    document.getElementById('hdncompcert').value = acompCertificate;
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "businessDirectory", action: "compCertDelete", namespace: "admin")}"
        },
        validation: {
            itemLimit: 5,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png', 'pdf', 'doc', 'docx']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    $('#fine-uploader-tax-certificate').fineUploaderS3({
        template: 'qq-template',
        request: {
            endpoint: "${amazonS3Service.sourceServerUrl()}",
            accessKey: "${amazonS3Service.accessKey()}"
        },
        signature: {
            endpoint: "${createLink(controller: "amazonS3", action: "s3Signing")}"
        },
        uploadSuccess: {
            endpoint: "${createLink(controller: "businessDirectory", action: "taxCertUploadSuccess", namespace: "admin")}",
            params: {
                isBrowserPreviewCapable: qq.supportedFeatures.imagePreviews,
                id: ${businessDirectory?.id}
            }
        },
        iframeSupport: {
            localBlankPagePath: "/server/success.html"
        },
        objectProperties: {
            acl: "public-read",
            key: function (fileId) {
                var keyRetrieval = new qq.Promise();
                var filename = this.getUuid(fileId) + "." + (this.getName(fileId).split('.').pop());
                $.post(
                    '${createLink(controller: "amazonS3", action: "s3Key")}',
                    {
                        filename: filename
                    }).done(function (data) {
                    keyRetrieval.success(data);
                    console.log("tax-certificate:" + data);
                    ataxCertificate.push(data);
                    document.getElementById('hdntaxcert').value = ataxCertificate;
                });
                return keyRetrieval;
            }
        },
        cors: {
            expected: true
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            method: "POST",
            endpoint: "${createLink(controller: "businessDirectory", action: "taxCertDelete", namespace: "admin")}"
        },
        validation: {
            itemLimit: 5,
            sizeLimit: 5242880,
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png', 'pdf']
        },
        thumbnails: {
            placeholders: {
                notAvailablePath: "${assetPath(src: 'uploader/not_available-generic.png')}",
                waitingPath: "${assetPath(src: 'uploader/waiting-generic.png')}"
            }
        },
        callbacks: {
            onComplete: function (id, name, response) {
                var previewLink = qq(this.getItemByFileId(id)).getByClass('preview-link')[0];

                if (response.success) {
                    previewLink.setAttribute("href", response.tempLink)
                }
            }
        }
    });

    function delLogo(id, key) {
        $.ajax({
            url: "${createLink(action: 'deleteLogo', namespace: "admin")}",
            type: "post",
            data: {key: key, bucket: 'com-goglides-media'},
            success: function (data) {
                //console.log(data); //<-----this logs the data in browser's console
                $('.logo-' + id).remove();
                if (data == 'success') {

                }
            },
            error: function (xhr) {
                // alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
    }
    function delComCert(id, key) {
        $.ajax({
            url: "${createLink(action: 'compCertDelete', namespace: "admin")}",
            type: "post",
            data: {key: key, bucket: 'com-goglides-media'},
            success: function (data) {
                //console.log(data); //<-----this logs the data in browser's console
                $('.com-cert-' + id).remove();
                if (data == 'success') {

                }
            },
            error: function (xhr) {
                // alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
    }
    function delTaxCert(id, key) {
        $.ajax({
            url: "${createLink(action: 'taxCertDelete', namespace: "admin")}",
            type: "post",
            data: {key: key, bucket: 'com-goglides-media'},
            success: function (data) {
                //console.log(data); //<-----this logs the data in browser's console
                $('.tax-cert-' + id).remove();
                if (data == 'success') {

                }
            },
            error: function (xhr) {
                // alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
    }
</script>
</body>
</html>