%{--Use DirectoryService--}%
<g:set var="directoryService" bean="directoryService"/>
%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'businessDirectory.label', default: 'businessDirectory')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h3>${businessDirectory?.businessName}</h3>
    </div>

    <div class="title_right">
        <div class="pull-right">

        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12 col-sm-9">
        <div class="x_panel">
            <div class="x_title">
                <h2>${businessDirectory?.businessName} <small>detail view of business directory</small></h2>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="row listing-view">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left">
                            <li class="active">
                                <a href="#basic" data-toggle="tab" aria-expanded="false">Basic</a>
                            </li>
                            <li class="">
                                <a href="#contact" data-toggle="tab" aria-expanded="false">Contacts</a>
                            </li>
                            <li class="">
                                <a href="#certificate" data-toggle="tab" aria-expanded="false">Certificates</a>
                            </li>
                            <li class="">
                                <a href="#settings" data-toggle="tab" aria-expanded="true">Settings</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="basic">
                                <h4>Basic Information
                                    %{--<a href="${createLink(action: 'edit', id: businessDirectory?.businessId)}"--}%
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"

                                       href="${createLink(action: 'edit', id: businessDirectory?.businessId)}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                </h4>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-9">

                                        <p class="lead">${businessDirectory?.businessName}</p>

                                        <div class="responsive">
                                            <table class="table">
                                                <tr>
                                                    <th><label>Business ID</label></th>
                                                    <td>${businessDirectory?.businessId}</td>
                                                </tr>
                                                <tr>
                                                    <th>TAX/PAN/VAT/TIPNS</th>
                                                    <td>${businessDirectory?.registration}</td>
                                                </tr>
                                                <tr>
                                                    <th><label>Business Type:</label></th>
                                                    <td><span
                                                            class="text-capitalize">${businessDirectory?.businessType}</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3">
                                            <img class="" src="${directoryService.getFileLocation(logo?.file, 'logo')}"
                                                 alt="logo"/>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12"><p class="lead">Description</p></div>

                                    <div class="col-xs-12 col-sm-12">${businessDirectory?.businessDescription}</div>
                                </div>
                            </div>

                            <div class="tab-pane" id="contact">
                                <h4>Contact Information
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(action: 'contact', id: businessDirectory?.businessId)}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                </h4>

                                <p class="lead">Business Contact</p>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3">Name:</div>

                                    <div class="col-xs-12 col-sm-9">
                                        %{--<g:set var="user" value="${userService.user(businessDirectory?.user)}"/>--}%
                                        ${user?.firstname} ${user?.lastname}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Address:</label></div>

                                    <div class="col-xs-12 col-sm-9">
                                        <g:if test="${businessDirectory?.geolocationCity}">
                                            ${businessDirectory?.geolocationCity}
                                        </g:if>
                                        <g:if test="${businessDirectory?.geolocationStateLong}">
                                            , ${businessDirectory?.geolocationStateLong}
                                        </g:if>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Email:</label></div>

                                    <div class="col-xs-12 col-sm-9">${businessDirectory?.email}</div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Website:</label></div>

                                    <div class="col-xs-12 col-sm-9">${businessDirectory?.website}</div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Phone:</label></div>

                                    <div class="col-xs-12 col-sm-9">
                                        <g:if test="${businessDirectory?.phone1}">
                                            ${businessDirectory?.phone1}
                                        </g:if>
                                        <g:if test="${businessDirectory?.phone2}">
                                            , ${businessDirectory?.phone2}
                                        </g:if>
                                        <g:if test="${businessDirectory?.phone3}">
                                            , ${businessDirectory?.phone3}
                                        </g:if>
                                    </div>
                                </div>
                                <hr>

                                <p class="lead">Contact Person</p>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Title:</label></div>

                                    <div class="col-xs-12 col-sm-9">
                                        ${businessDirectory?.contactJobTitle}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Name:</label></div>

                                    <div class="col-xs-12 col-sm-9">
                                        <g:if test="${businessDirectory?.contactFirstName}">
                                            ${businessDirectory?.contactFirstName}
                                        </g:if>
                                        <g:if test="${businessDirectory?.contactLastName}">
                                            &nbsp; ${businessDirectory?.contactLastName}
                                        </g:if>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Email:</label></div>

                                    <div class="col-xs-12 col-sm-9">
                                        ${businessDirectory?.contactEmail}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3"><label>Phone:</label></div>

                                    <div class="col-xs-12 col-sm-9">
                                        ${businessDirectory?.contactPhone}
                                    </div>
                                </div>

                                <hr>
                                <h4>Map
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(action: 'edit', id: businessDirectory?.businessId)}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                </h4>

                                <p class="lead">Business Location</p>

                                <div class="row">
                                    <div class="col-xs-12"><p>${businessDirectory?.addressLine1}</p></div>

                                    <div class="col-xs-12">
                                        <div id="map_canvas"
                                             style="height:410px;width:100%;margin-bottom:10px;"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="certificate">
                                <h4>Certificates
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(action: 'documents', id: businessDirectory?.businessId)}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>


                                </h4>

                                <p class="lead">Company Certificates</p>
                                <ul class="list-unstyled certificate db-comCert">
                                    <g:if test="${comCert?.id != null}">
                                        <g:each in="${comCert}" status="i" var="certificate">
                                            <li class="com-cert-${certificate?.id}">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="${directoryService.getFileLocation(certificate?.file)}"
                                                           target="_blank">Certificate ${i + 1}</a>
                                                    </div>

                                                    <div class="col-md-3">

                                                    </div>
                                                </div>
                                            </li>
                                        </g:each>
                                    </g:if>
                                </ul>
                                <hr>

                                <p class="lead">PAN/VAT/TIPN Certificates</p>
                                <ul class="list-unstyled certificate db-taxCert">
                                    <g:if test="${taxCert?.id != null}">
                                        <g:each in="${taxCert}" status="i" var="certificate">
                                            <li class="tax-cert-${certificate?.id}">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="${directoryService.getFileLocation(certificate?.file)}"
                                                           target="_blank">Certificate ${i + 1}</a>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <!-- <a href="javascript:void(0);" onclick="delTaxCert(${certificate?.id}, '${certificate?.file}')"><i class="fa fa-trash"></i></a> -->
                                                    </div>
                                                </div>
                                            </li>
                                        </g:each>
                                    </g:if>
                                </ul>
                            </div>

                            <div class="tab-pane" id="settings">
                                <h4>Settings
                                    <a class="pull-right" data-toggle="tooltip" title="Edit"
                                       href="${createLink(action: 'edit', id: businessDirectory?.businessId)}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <g:render template="templates/template_actions" model=""/>
        <g:render template="templates/template_summary" model=""/>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">create ticket</h4>
            </div>

            <div class="modal-body">
                <form name="createTicket" id="createTicket" method="POST">
                    <div class="form-group">
                        <label for="category">Category:</label>
                        <select class="form-control" name="ticketCategory" id="sel1">
                            <option value="businessDirectory">Business Directory</option>
                            <option value="listing" selected>Listing</option>
                            <option value="booking">Booking</option>
                            <option value="agents">Agents</option>
                            <option value="more">more</option>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="createdBy">Created By:</label>
                                <input type="text" name="createdBy" value="{{username}}"
                                       class="form-control">
                                <input type="hidden" name="userid"
                                       value="{{username}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="createdTo">Created To:</label>
                                <input type="text" name="createdTo"
                                       value="${businessDirectory.businessName}" class="form-control"
                                       id="pwd">
                                <input type="hidden" name="busdirid" value="${businessDirectory.id}">
                                <input type="hidden" name="remark" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description">ticket description</label>
                        <textarea class="form-control" name="ticketDescription" rows="5"
                                  id="comment"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="status">Status:</label>
                        <select class="form-control" name="status" id="sel1">
                            <option value="active">Active</option>
                            <option value="pending">Pending</option>
                            <option value="resolved">Resolved</option>
                            <option value="closed">Closed</option>
                        </select>
                    </div>

                    <div class="text-center">
                        <button type="button" onclick="createTick()"
                                class="btn btn-lg btn-success">Create</button>
                        <button type="" class="btn btn-lg btn-danger">Cancel</button>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
    <div class="modal fade" id="settingModel" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Change Setting Status</h4>
            </div>

            <div class="modal-body">
                <style>
                .switch-btn{ border: 2px solid #ddd;}
                .switch-btn,.switch-btn .btn{ border-radius: 15px;}
                .btn-on.active{background-color: #5BB75B;color: white;}
                .btn-off.active{background-color: #DA4F49;color: white;}
                </style>
                <form name="changeStatus" id="changeStatus" method="POST">
                    <input type="hidden" name="businessId" value="${businessDirectory?.businessId}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="regDoc">Company Reg Doc Verfication:</label>
                                <div class="btn-group switch-btn" id="#" data-toggle="buttons">
                                    <label class="btn btn-on btn-md ">
                                        <input type="radio" value="true" id="regDoc" name="regDoc" >Verified</label>
                                    <label class="btn btn-off btn-md active">
                                        <input type="radio" value="false" name="regDoc" checked="checked">UnVerified</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="taxDoc">Tax Doc Verification:</label>
                                <div class="btn-group switch-btn" id="#" data-toggle="buttons">
                                    <label class="btn btn-on btn-md ">
                                        <input type="radio" value="true" id="taxDoc" name="taxDoc">Verified</label>
                                    <label class="btn btn-off btn-md active">
                                        <input type="radio" value="false" name="taxDoc"  checked="checked">UnVerified</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="approved"> Approved Status:</label>
                        <select class="form-control" name="approved" id="approved" >
                            <option value="approved">approved</option>
                            <option value="pending">pending</option>
                            <option value="draft">draft</option>


                        </select>
                    </div>

                    <div class="form-group">
                        <label for="statuss">Status:</label>
                        <select class="form-control" name="status" id="status" >
                            <option value="publish">publish</option>
                            <option value="draft">draft</option>

                        </select>
                    </div>

                    <div class="text-center">
                        <button type="button" onclick="changeSetting()"
                                class="btn btn-lg btn-success">Save</button>
                        <button type="reset" class="btn btn-lg btn-danger">Cancel</button>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function change_status(id) {
        $("#divLoading").addClass('show');

        $.ajax({
            url: "${createLink(uri: '/admin/business-directory/publish')}",
            type: "post",
            data: {id: id},

            success: function (data) {
                location.reload();
                $("#divLoading").removeClass('show');
            },
            error: function (xhr) {
                alert("Oops Something Wrong ")
                $("#divLoading").removeClass('show');


            }
        });
    }

    function changeClaimable(businessId, value) {
        $("#divLoading").addClass('show');
        $.ajax({
            url: "${createLink(action: 'change-claimable', namespace: 'admin')}",
            type: "post",
            data: {businessId: businessId, value: value},
            success: function (data) {
               // location.reload();
            },
            error: function (xhr) {

            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=${siteSetting.getGoogleMapAPIKey()}&libraries=places"></script>
<script>
    $(function () {
        initialize();
    });

    var lat = parseFloat('${businessDirectory?.geolocationLat}');
    var lng = parseFloat('${businessDirectory?.geolocationLong}');
    var geocoder;
    var map;
    var marker;

    function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(lat, lng);
        var mapOptions = {
            zoom: 16,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        marker = new google.maps.Marker({
            position: latlng,
            draggable: false,
            map: map,
            visible: true
        });
    }

    function changeSetting() {
        $('#myModal').modal('hide');
        var data = $("#changeStatus").serialize();


        var request = $.ajax({
            url: "${createLink(uri: '/admin/business-directory/changeSetting')}",
            method: "POST",
            data: data,
        });
        request.success(function (data) {
            location.reload();
//          console.log(data)
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);

        });
    }
</script>

</body>
</html>