<g:set var="userService" bean="userService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'businessDirectory.label', default: 'businessDirectory')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Edit Agent Profile - ${businessDirectory?.businessName}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Agent Detail</h3>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:form resource="${this.businessDirectory}" action="update" method="POST" class="form">
                    <fieldset>
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Step 1 / 5</h3>
                                <h5>Basic Information</h5>
                            </div>

                            <div class="form-top-right">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <g:hasErrors bean="${this.businessDirectory}">
                            <ul class="alert alert-danger" role="alert">
                                <g:eachError bean="${this.businessDirectory}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                            error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>
                        <div class="form-bottom">
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="">Registered Business Name <span class="text-danger">*</span>
                                    </label>
                                    <input id="businessName" class="form-control" type="text"
                                           name="businessName" value="${businessDirectory?.businessName}"
                                           placeholder="Registered Business Name" required>

                                    <p>Only shared once your account is approved.</p>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="">Business TAX/PAN/VAT/TIPNS</label>
                                    <input id="registration" class="form-control"
                                           name="registration"
                                           placeholder="Business TAX/ PAN/VAT/TIPNS"
                                           value="${businessDirectory?.registration}"
                                           required>

                                    <p>We use this data for analysis purpose only.</p>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="">Slug</label>
                                    <input id="slug" class="form-control" name="slug" value="${businessDirectory?.slug}"
                                           placeholder="Slug" required/>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="">Business Type</label>

                                    <div class="clearfix">
                                        <label>
                                            <input id="" class="Form-label-radio" type="radio" name='businessType'
                                                   value="public" required="required"
                                                   <g:if test="${businessDirectory.businessType == 'public'}">checked</g:if>>
                                            <span class="Form-label-text">Public</span>
                                        </label>
                                        <label>
                                            <input id="" class="Form-label-radio" type="radio" name='businessType'
                                                   value="private" required="required"
                                                   <g:if test="${businessDirectory.businessType == 'private'}">checked</g:if>>
                                            <span class="Form-label-text">Private</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="">Business Description</label>
                                    <textarea class="form-control" name="businessDescription"
                                              placeholder="Brief description of the company not more than 150 words."
                                              rows="10"
                                              cols="10"
                                              maxlength="900">${businessDirectory?.businessDescription}</textarea>

                                    <p>Tell more to your customer.</p>
                                </div>
                                <g:set var="user" value="${userService.user(businessDirectory?.user)}"/>

                                <div class="form-group col-sm-6">
                                    <label for="">First Name</label>
                                    <input id="" class="form-control" name="firstname"
                                           placeholder="First Name" value="${user?.firstname}"
                                           required/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="">Last Name</label>
                                    <input id="" class="form-control" name="lastname"
                                           placeholder="Last Name" value="${user?.lastname}"
                                           required=""/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="">Email</label>
                                    <input type="email" name="email" value="${businessDirectory?.email}"
                                           class="form-control" placeholder="Email"
                                           required="required"/>

                                    <p>Emails will be sent to this account. It's not a spam.</p>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="">Website</label>
                                    <input id="website" class="form-control" name="website"
                                           placeholder="Website" value="${businessDirectory?.website}" required/>

                                    <p>Tell more to your customer.</p>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label>Phone</label>
                                    <input type="text" name="phone1" value="${businessDirectory?.phone1}"
                                           class="form-control" placeholder="Phone 1"
                                           required="required"
                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>

                                    <p>This is how we can get in touch.</p>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label>Phone</label>
                                    <input type="text" name="phone2" value="${businessDirectory?.phone2}"
                                           class="form-control" placeholder="Phone 2"
                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>

                                    <p>This is how we can get in touch.</p>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label>Phone</label>
                                    <input type="text" name="phone3" class="form-control"
                                           value="${businessDirectory?.phone3}" placeholder="Phone 3"
                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>

                                    <p>This is how we can get in touch.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-right border-top clearfix">
                            <input type="submit" class="btn bttn" value="Next"/>
                        </div>
                    </fieldset>
                </g:form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        // validate the comment form when it is submitted
        $(".form0").validate({
            rules: {
                businessName: {
                    required: true,
                    maxlength: 60
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 100
                },
                businessDescription: {
                    maxlength: 10000
                },
                registration: {
                    required: true,
                    maxlength: 100
                },
                website: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {
                businessName: {
                    required: "Registered Business Name field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                businessDescription: {
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters.")
                },
                registration: {
                    required: "Business TAX/ PAN/VAT/TIPNS field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters.")
                },
                businessType: "Business Type field is required.",
                website: {
                    required: "Website field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters.")
                },
                email: {
                    required: "Email field is required.",
                    email: "Please enter a valid email.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters.")
                }
            }
        });
    });
</script>
</body>
</html>
