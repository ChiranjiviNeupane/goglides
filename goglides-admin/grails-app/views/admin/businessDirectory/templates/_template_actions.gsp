<div class="x_panel">
    <div class="x_title">
        <h2>Actions</h2>

        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled action-wrap">
                <li>
                    <a class="btn btn-success col-xs-12" href="javascript:void(0);"
                           onclick="change_status(${businessDirectory?.businessId})">

                        <i class="fa fa-fw fa-check"></i> Publish </a>
                </li>
                <li>
                    <a class="btn btn-danger col-xs-12" href="javascript:void(0);" data-toggle="modal"
                       data-target="#myModal">
                        <i class="fa fa-fw fa-times"></i> Decline</a>
                </li>
                <li class="">
                    <a class="btn btn-danger col-xs-12"
                       href="${createLink(action: 'delete', id: businessDirectory?.businessId)}"
                       onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                        <i class="fa fa-fw fa-trash-o"></i> Trash
                    </a>
                </li>
                <li>
                    <a href="${createLink(action: 'edit', id: businessDirectory?.businessId)}"
                       class="btn btn-dark col-xs-12"><i class="fa fa-fw fa-edit"></i> Edit</a>
                </li>
                <g:if test="${businessDirectory?.isClaimable == 'not_claimable'}">
                    %{--<li>--}%
                        %{--<a href="javascript:void(0);" class="btn btn-dark col-xs-12"--}%
                           %{--onclick="changeClaimable(${businessDirectory?.businessId}, 'claimable')">--}%
                            %{--<i class="fa fa-fw fa-bookmark"></i> Mark as Claimable</a>--}%
                    %{--</li>--}%
                </g:if>
                <g:else>
                    %{--<li>--}%
                        %{--<a href="javascript:void(0);" class="btn btn-dark col-xs-12"--}%
                           %{--onclick="changeClaimable(${businessDirectory?.businessId}, 'not_claimable')">--}%
                            %{--<i class="fa fa-fw fa-bookmark-o"></i> Mark as Unclaimable</a>--}%
                    %{--</li>--}%
                </g:else>
                <g:if test="${businessDirectory?.ticket != NULL}">
                    %{--<li>--}%
                        %{--<a class="btn btn-dark col-xs-12"--}%
                           %{--href="${createLink(uri: '/ticket/show/' + businessDirectory?.ticketId)}">--}%
                            %{--<i class="fa fa-fw fa-ticket"></i> View Ticket</a>--}%
                    %{--</li>--}%
                </g:if>
                <li>
                    <a class="btn btn-dark col-xs-12"
                       href="${createLink(uri: '/admin/bookings?businessId=' + businessDirectory?.businessId)}">
                        <i class="fa fa-fw fa-cart-plus"></i> Booking</a>
                </li>
                <li>
                    <a class="btn btn-dark col-xs-12"
                       href="${createLink(uri: '/admin/listing?businessId=' + businessDirectory?.businessId)}">
                        <i class="fa fa-fw fa-list"></i> Listing</a>
                </li>
                <li>
                    <a class="btn btn-dark col-xs-12" href="javascript:void(0);" data-toggle="modal"
                       data-target="#settingModel">
                        <i class="fa fa-fw fa-times"></i> Setting</a>
                    %{--<a class="btn btn-dark col-xs-12"--}%
                       %{--href="${createLink(uri: '/admin/listing?businessId=' + businessDirectory?.businessId)}">--}%
                        %{--<i class="fa fa-fw fa-list"></i> </a>--}%
                </li>
            </ul>
        </div>
    </div>
</div>