%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'businessDirectory.label', default: 'businessDirectory')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Agent Profile - ${businessDirectory?.businessName}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Agent Detail</h3>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:form resource="${this.businessDirectory}" action="savemap" method="POST"
                        class="form form-horizontal">
                    <g:hiddenField name="version" value="${this.businessDirectory?.version}"/>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Step 3 / 5</h3>
                            <h5>Confirm Business Location</h5>
                        </div>

                        <div class="form-top-right">
                            <i class="fa fa-map"></i>
                        </div>
                    </div>
                    <g:hasErrors bean="${this.businessDirectory}">
                        <ul class="alert alert-danger" role="alert">
                            <g:eachError bean="${this.businessDirectory}" var="error">
                                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                        error="${error}"/></li>
                            </g:eachError>
                        </ul>
                    </g:hasErrors>
                    <div class="form-bottom">
                        <input type="hidden" id="hdnLat" name="geolocationLat"
                               value=" ${businessDirectory.geolocationLat}"/>
                        <input type="hidden" id="hdnLng" name="geolocationLong"
                               value=" ${businessDirectory.geolocationLong}"/>
                        <input type="hidden" name="addressLine1" id="hdnCity"
                               value=" ${businessDirectory?.addressLine1}"/>

                        <div id="map_canvas" style="height:410px;width:100%;margin-bottom:10px;"></div>
                        <p>Drag marker to locate the business location.</p>

                        <div class="form-group text-right border-top clearfix">
                            <a href="${createLink(action: 'address', id: businessDirectory?.businessId)}"
                               class="btn bttn">Back</a>
                            <input type="submit" class="btn bttn" value="Next"/>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=${siteSetting.getGoogleMapAPIKey()}"></script>
<script>
    $(function () {
        initialize();
    });

    var lat = parseFloat(document.getElementById('hdnLat').value);
    var lng = parseFloat(document.getElementById('hdnLng').value);
    var geocoder;
    var map;
    var marker;
    var infowindow;

    infowindow = new google.maps.InfoWindow({size: new google.maps.Size(150, 50)});

    function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(lat, lng);
        var mapOptions = {
            zoom: 16,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        google.maps.event.addListener(map, 'click', function () {
            infowindow.close();
        });
        var marker = new google.maps.Marker({
            position: latlng,
            draggable: true,
            map: map,
            visible: true
        });
        google.maps.event.addListener(marker, 'dragend', function (evt) {
            document.getElementById('hdnLat').value = evt.latLng.lat();
            document.getElementById('hdnLng').value = evt.latLng.lng();
            geocodeLatLng(geocoder, map, infowindow);

        });
    }

    function geocodeLatLng(geocoder, map, infowindow) {
        var lat = parseFloat(document.getElementById('hdnLat').value);
        var lng = parseFloat(document.getElementById('hdnLng').value);
        var latlng = {lat: lat, lng: lng};
        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    map.setZoom(16);
                    var marker = new google.maps.Marker({
                        zoom: 16,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    infowindow.setContent(results[1].formatted_address);
                    document.getElementById('hdnCity').value = results[1].formatted_address;
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }
</script>
</body>
</html>