%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'businessDirectory.label', default: 'BusinessDirectory')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Agent Profile - ${businessDirectory?.businessName}</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Agent Detail</h3>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:form resource="${this.businessDirectory}" action="saveaddress" method="POST" class="form">
                    <g:hiddenField name="version" value="${this.businessDirectory?.version}"/>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Step 2 / 5</h3>
                            <h5>Business Location</h5>
                        </div>

                        <div class="form-top-right">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <g:hasErrors bean="${this.businessDirectory}">
                        <ul class="alert alert-danger" role="alert">
                            <g:eachError bean="${this.businessDirectory}" var="error">
                                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                        error="${error}"/></li>
                            </g:eachError>
                        </ul>
                    </g:hasErrors>
                    <div class="form-bottom">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>Address 1</label>
                                <input id="addressLine1" class="form-control" type="text"
                                       name="addressLine1" placeholder="Address 1"
                                       value="${businessDirectory?.addressLine1}" onBlur="geolocate()"
                                       required="required"/>
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Address 2</label>
                                <input class="form-control" type="text" name="addressLine2"
                                       placeholder="Address 2" value="${businessDirectory?.addressLine2}">
                            </div>

                            <div class="form-group col-sm-6">
                                <label>City</label>
                                <input id="locality" class="form-control" type="text"
                                       name="geolocationCity" placeholder="City"
                                       value="${businessDirectory?.geolocationCity}" required="required"/>
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Province/State</label>
                                <input id="administrative_area_level_1" class="form-control"
                                       type="text"
                                       name="geolocationStateLong" placeholder="Province/State"
                                       value="${businessDirectory?.geolocationStateLong}" required="required"/>
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Country</label>
                                <input id="country" class="form-control" type="text"
                                       name="geolocationCountryLong" placeholder="Country"
                                       value="${businessDirectory?.geolocationCountryLong}" required="required"/>

                                <input id="postal_code" type="hidden" name="geolocationPostcode"
                                       value="${businessDirectory?.geolocationPostcode}">
                                <input id="geolocation_lat" type="hidden" name="geolocationLat"
                                       value="${businessDirectory?.geolocationLat}">
                                <input id="geolocation_long" type="hidden" name="geolocationLong"
                                       value="${businessDirectory?.geolocationLong}">
                                <input id="geolocation_formatted_address" type="hidden"
                                       name="geolocationFormattedAddress"
                                       value="${businessDirectory?.geolocationFormattedAddress}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-right border-top clearfix">
                        <a href="${createLink(action: 'edit', id: businessDirectory?.businessId)}" class="btn bttn"><i class="fa fa-arrow-left"></i> Back</a>
                        <input type="submit" class="btn bttn" value="Next"/>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $().ready(function () {
// validate the comment form when it is submitted
        $(".form0").validate({
            rules: {
                addressLine1: {
                    maxlength: 100
                },
                addressLine2: {
                    maxlength: 100
                },
                geolocationCity: {
                    maxlength: 100
                },
                geolocationStateLong: {
                    maxlength: 100
                },
                geolocationCountryLong: {
                    maxlength: 100
                }
            },
            messages: {
                addressLine1: {
                    required: "Address Line 1 field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                addressLine2: {
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                geolocationCity: {
                    required: "City field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                geolocationStateLong: {
                    required: "Province field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                },
                geolocationCountryLong: {
                    required: "Country field is required.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                }
            }
        });
    });

    $(function () {
//document.getElementById('addressLine1').value= '';
    });

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
// Create the autocomplete object, restricting the search to geographical
// location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('addressLine1')),
            {types: ['geocode']});

// When the user selects an address from the dropdown, populate the address
// fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
// Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

// Get each component of the address from the place details
// and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                if (document.getElementById(addressType)) {

                    document.getElementById(addressType).value = val;
                }
            }
        }
    }

    function geolocate() {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('addressLine1').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                console.log(results[0].geometry.location.lat());
                console.log(results[0].geometry.location.lng());
                document.getElementById('geolocation_lat').value = results[0].geometry.location.lat();
                document.getElementById('geolocation_long').value = results[0].geometry.location.lng();

            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
</script>

<div class="map" style="display:none"></div>
<script src="https://maps.googleapis.com/maps/api/js?key=${siteSetting.getGoogleMapAPIKey()}&libraries=places&callback=initAutocomplete"
        async defer></script>
</body>
</html>