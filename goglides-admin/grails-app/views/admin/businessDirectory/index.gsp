<g:set var="userService" bean="userService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h3>Manage Business Directory</h3>
    </div>

    <div class="title_right">
        <div class="pull-right">
            <a class="btn bttn" href="${createLink(action: 'create')}">
                <i class="fa fa-briefcase"></i> Add New Business</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Advanced Search</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="x_content" style="display: none;">
                    <g:form url="[action: 'index', controller: 'businessDirectory']">
                        <div class="col-xs-6">
                            <input type="text" name="search" class="form-control" placeholder="Search for..." style="height: auto; padding: 7px 12px;">
                        </div>
                        <div class="col-xs-5">
                            <g:select name="searchAttribute" from="${["search by Business ID","search by Business Name", "search by Business Type", "search by Status"]}"
                                      keys="${['businessId','businessName', 'businessType', 'status']}"
                                      value="${name}"
                                      noSelection="['': '-Choose Search Category-']" class="form-control" style="height: auto; padding: 7px 12px;"/>
                        </div>
                        <div class="col-xs-1 form-group">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </g:form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Business Directory Lists <small
                        class="">list of business directory information ${params?.state ?: 'all'}</small>
                </h2>

                <div class="pull-right">
                    <a class="btn btn-primary" href="${createLink(action: 'index')}"><i
                            class="fa fa-bars"></i> All <span
                            class="badge">${totalCount}</span></a>
                    <a class="btn btn-dark" href="${createLink(action: 'index', params: [state: 'draft'])}"><i
                            class="fa fa-file-text"></i> Draft <span
                            class="badge">${inActiveCount}</span></a>
                    <a class="btn btn-success" href="${createLink(action: 'index', params: [state: 'publish'])}"><i
                            class="fa fa-check"></i> Publish <span
                            class="badge">${activeCount}</span></a>
                    <a class="btn btn-danger" href="${createLink(action: 'index', params: [state: 'trash'])}"><i
                            class="fa fa-trash-o"></i> Past <span
                            class="badge">${trashCount}</span></a>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:if test="${!directory}">
                    <p>No records found.</p>
                </g:if>
                <g:else>
                    <table class="table table-stripped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Business ID</th>
                            <th>Business Name</th>
                            <th>User</th>
                            <th>Joined</th>
                            <th>Approved</th>
                            <th>Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <g:each in="${directory}">
                            <tr>
                                <td>${it?.businessId}</td>
                                <td>${it?.businessName}</td>

                                <td>
                                    %{--<g:set var="user" value="${userService.user(it?.user)}"/>--}%
                                    %{--${user?.firstname} ${user?.lastname}--}%
                                </td>

                                <td>
                                    <g:formatDate date="${it?.createdAt}" type="date" style="MEDIUM"/>

                                </td>
                                <td>
                                    <g:if test="${it?.approved == 'approved'}">
                                        <span class="label label-success">Approved</span>
                                    </g:if>
                                    <g:elseif test="${it?.approved == 'declined'}">
                                        <span class="label label-danger">Declined</span>
                                    </g:elseif>
                                    <g:else>
                                        <span class="label label-info">Pending</span>
                                    </g:else>
                                </td>
                                <td class="text-capitalize">${it?.status.replace('_', ' ')}</td>
                                <td class="text-center">
                                    <a class="" href="${createLink(action: 'view', id: it?.businessId)}">
                                        <i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        </g:each>
                    </table>

                    <div class="pagination pull-right">
                        <g:paginate total="${agentsCount}" class="pagination"
                                    params="${[state: params?.state]}" action="index"
                                    next='&raquo;' prev='&laquo;' maxsteps="4"/>
                    </div>
                    </div>
                </g:else>
            </div>
        </div>
    </div>
</div>
</body>
</html>