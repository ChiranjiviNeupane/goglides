%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!-- Fine Uploader Gallery CSS file
        ====================================================================== -->


<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Update Image Info</h2>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <g:form action="updateImageInfo" class="form form-horizontal form-label-left">
                    <input type="hidden" value="${imageProcessing?.id}" name="id"/>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Choose Image Category</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <g:select name="imageCategory" from="${["profile", "feature", "gallery","banner","other"]}"
                                      noSelection="['': '-Choose Image Category-']"
                                      class="form-control col-md-7 col-xs-12" value="${imageProcessing?.imageCategory}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Width</label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="" name="width" value="${imageProcessing?.width}"
                                   class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Quality</label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="" name="quality" value="${imageProcessing?.quality}"
                                   class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                            <fieldset class="buttons">
                                <g:submitButton id="submit-btn" name="update" class="btn bttn"
                                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                            </fieldset>

                        </div>
                    </div>

                </g:form>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

</body>
</html>