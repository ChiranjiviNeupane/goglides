%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!-- Fine Uploader Gallery CSS file
        ====================================================================== -->


<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Add new Image Info</h2>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <g:form action="saveImageInfo" class="form-horizontal form-label-left">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Choose Image Category</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <g:select name="imageCategory" from="${["profile", "feature", "gallery","banner","other"]}"
                                      noSelection="['': '-Choose Image Category-']"
                                      class="form-control col-md-7 col-xs-12" id="imageCategory"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Width</label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="" name="width" value=""
                                   class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Quality</label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="" name="quality" value=""
                                   class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                            <fieldset class="buttons">
                                <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                            </fieldset>

                        </div>
                    </div>

                </g:form>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
%{--<script>

function checkData() {

    var URL="<g:createLink action="checkImageType" controller="imageProcessing" namespace="admin"/>";
    $.ajax({
        method:'POST',
        url:URL,
        data:{
            imageCategory: $('#imageCategory').val(),
            deviceSize: $('#deviceSize').val()},

            success:function(resp) {
            alert(resp)
                if(resp=='success'){

                    alert("Image Type already available...please update existing image type")
                    return false;
                }else {
                    alert("not aviale")
                }
            }

    })
}
</script>--}%
</body>
</html>