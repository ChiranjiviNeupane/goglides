%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'imageProcessing.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-title">
    <div class="title_left">
        <h2>Manage Images</h2>
    </div>
    <div class="title_right">
        <div class="pull-right">
            <a href="${createLink(action: 'create')}" class="btn bttn">Create new ImageSize</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <g:if test="${imageProcessing}">
                        <table class="table">
                            <tr>
                                <th>Image Category</th>
                                <th>Image Height</th>
                                <th>Image Width</th>
                                <th>Image Quality </th>
                                <th>Action</th>
                            </tr>
                            <g:each in="${imageProcessing}" var="i">
                                <tr>
                                    <td>${i?.imageCategory}</td>
                                    <td>${i?.height}</td>
                                    <td>${i?.width}</td>
                                    <th>${i?.quality}</th>
                                    <td>
                                        <a href="${createLink(uri: '/admin/imageProcessing/updateInfo?id=' + i?.id)}" class="btn bttn">update</a>
                                        <a href="${createLink(uri: '/admin/imageProcessing/delete?id=' + i?.id)}"
                                           class="btn bttn">Delete</a>
                                    </td>
                                </tr>
                            </g:each>
                        </table>

                        <div class="pagination">
                            %{--<g:paginate total="${}"/>--}%
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>