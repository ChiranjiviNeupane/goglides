<g:each in="${bookingItem}">
    <div class="row clearfix">
        <div class="col-xs-12">
            <h2>${it?.title} ${it?.fullName}</h2>
        </div>

        <div class="col-xs-12 col-sm-6"><label><i class="fa fa-envelope fa-fw"></i>:</label> ${it?.email}</div>

        <div class="col-xs-12 col-sm-6"><label><i class="fa fa-phone fa-fw"></i>:</label> ${it?.contactNumber}</div>

        <div class="col-xs-12 col-sm-6"><label>Nationality:</label> ${it?.nationality}</div>

        <div class="co-xs-12 col-sm-6"><label>Passport or Licence:</label> ${it?.passportNumber}</div>

        <div class="co-xs-12 col-sm-6"><label>Weight:</label> ${it?.weight}</div>

        <div class="co-xs-12 col-sm-6"><label>Age Group:</label> <span
                class="text-capitalize">${it?.ageGroup}</span></div>
        <div class="clearfix"></div>
        <hr>
    </div>
</g:each>
