<div class="ticket-wrap">
    <div class="text-right" style="margin: 5px 0;">
        <a href="" class="btn bttn" onclick="printDiv('printableTicket')"><i class="fa fa-print fa-fw"></i> Print Ticket</a>
    </div>

    <div id="printableTicket" class="table-booking-invoice table-responsive">
        <table class="tkt-wrap"
               style="width: 100%; border: 2px solid #ccc; font-size: 12px; color: #565656; letter-spacing: 0.5px;">
            <tbody bgcolor="#fff">
            <tr>
                <td style="padding: 15px 15px 0; width: 100px;">
                    <img src="" class="logo-printable" style="width: 90px;">
                </td>
                <td style="padding: 20px; vertical-align: top; line-height: 1.8;">
                    <g:set var="user" value="${userService.user(booking?.user)}"/>
                    <table style="width: 100%;">
                        <tbody>
                        <tr>
                            <td colspan="2">
                                <strong>Name :</strong>${user?.firstname} ${user?.lastname}<br>
                                <strong>Email :</strong>${user?.email}<br>
                                <strong>Phone :</strong>${user?.phone}<br>
                                <strong>Contact Person :</strong>${booking?.emergencyContactPerson} ${booking?.emergencyContactNo}
                            </td>
                            <td valign="top">
                                <strong>Invoice No :</strong>${booking?.invoiceNo}<br>
                                <strong>Booking Date :</strong>
                                <g:formatDate
                                        date="${booking?.createdAt}" type="datetime"
                                        style="MEDIUM"/><br>
                                <strong>Booking Reference :</strong> ${booking?.referenceNo}<br>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <td align="right" style="padding: 15px;">Amount : <br><strong
                        style="font-size: 36px;">${raw(priceService.getCurrencySymbol())}${booking?.totalPrice}</strong>
                </td>
            </tr>

            <tr class="all-info">
                <td colspan="3" style="padding: 0 15px;">
                    <table class="table borderless" style="width: 100%; margin: 20px 0 8px;">
                        <thead>
                        <tr style="background: #66a5ad; color: #fff;">
                            <th style="padding: 5px; font-weight: normal;">TKT. No.</th>
                            <th style="padding: 5px; font-weight: normal;">PAX Name</th>
                            <th style="padding: 5px; font-weight: normal;">PAX Type</th>
                            <th style="padding: 5px; font-weight: normal;">Fare</th>
                            <th style="padding: 5px; font-weight: normal;">Photo/Video</th>
                            <th style="padding: 5px; font-weight: normal;">Transport</th>
                            <th style="padding: 5px; font-weight: normal;">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${bookingItem}">
                            <tr>
                                <td style="padding: 5px;" data-th="TKT. No.">
                                    <span class="bt-content">${it?.ticketNo}</span>
                                </td>
                                <td data-th="PAX Name">
                                    <span class="bt-content">${it?.title + ' ' + it?.fullName}</span>
                                </td>
                                <td data-th="PAX Type">
                                    <span class="bt-content">${it?.ageGroup}</span>
                                </td>
                                <td data-th="Fare">
                                    <span class="bt-content">${it?.amount}</span>
                                </td>
                                <td data-th="Photo/Video">
                                    <span class="bt-content">${it?.multimediaCharge}</span>
                                </td>
                                <td data-th="Transport">
                                    <span class="bt-content">${it?.transportationCharge}</span>
                                </td>
                                <td data-th="Total">
                                    <span class="bt-content">${it?.totalAmount}</span>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                    <table style="width: 100%; margin-bottom: 20px;">
                        <tbody>
                        <tr>
                            <td style="padding:6px 0;"><strong>Total Amount :</strong></td>
                            <td class="title"
                                style="float: right; background: #66a5ad; color: #fff; padding: 4px 40px; font-size: 18px;">${raw(priceService.getFormattedPrice(booking?.totalPrice))}</td>
                        </tr>
                        <tr>
                            <td colspan="2"
                                style="padding: 8px 0;"><strong>In Word :</strong> <span
                                    id="output"></span></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 8px 0;">
                                <strong>Paymet Mode :</strong> ${booking?.paymentType}<br>

                                <strong>Payment Status:</strong> ${booking?.paymentStatus.replace('_', ' ')}<br>

                                <strong>Booking Status:</strong> ${booking?.status.replace('_', ' ')}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </td>
            </tr>

            <tr style="border-bottom: 2px solid #ddd; border-top: 2px solid #ddd;">
                <td colspan="3" style="padding: 0 15px;">
                    <table class="table borderless"
                           style=" width: 100%; text-align: center; margin:20px 0;">

                        <thead>
                        <tr>
                            <th style="padding: 5px; text-align: center;">
                                <strong>Flight Date</strong>
                            </th>
                            <th style="padding: 5px; text-align: center;">
                                <strong>Flight Time</strong>
                            </th>
                            <th style="padding: 5px; text-align: center;">
                                <strong>Flight Duration</strong>
                            </th>
                            <th style="padding: 5px; text-align: center;">
                                <strong>Pick up Address</strong>
                            </th>
                            <th style="padding: 5px; text-align: center;">
                                <strong>Drop off Address</strong>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="padding: 5px;" data-th="Flight Date">
                                <span class="bt-content">${booking?.bookingDate}</span>
                            </td>
                            <td style="padding: 5px;" data-th="Flight Time">
                                <span class="bt-content">
                                    ${listingService.formatSchedule(booking?.bookingTime)}
                                </span>
                            </td>
                            <td style="padding: 5px;" data-th="Flight Duration">
                                <span class="bt-content">
                                    ${listingService.formatDuration(booking?.duration)?.d}
                                </span>
                            </td>
                            <td style="padding: 5px;" data-th="Pick up Address">
                                <span class="bt-content">${booking?.pickup}</span>
                            </td>
                            <td style="padding: 5px;" data-th="Payment Method">
                                <span class="bt-content" style="">${booking?.dropLocation}</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="border-top: 1px solid #ddd;">
                <td colspan="3" style="padding: 15px;">
                    <table style="width: 100%;">
                        <tbody>
                        <tr>
                            <td><h5>Special Request</h5></td>
                        </tr>
                        <tr>
                            <td>
                                ${booking?.customerComment}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 15px;">
                    <table style="width: 100%; margin-bottom: 10px;">
                        <tbody><tr>
                            <td><h5>Package Info</h5></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <strong style="font-size: 14px;">${booking?.listing?.title}</strong>

                                <p>${raw(booking?.listing?.address)}</p>

                                <p><strong>${raw(booking?.listingPackage?.tripTitle)}</strong></p>
                                ${raw(booking?.listing?.highlights)}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <g:if test="${listingService.getInsuranceCompany(listing) != 'N.A.'}">
                <tr style="border-bottom: 2px solid #ddd; border-top: 2px solid #ddd;">
                    <td colspan="3" style="padding: 15px;">
                        <table style="width: 100%;">
                            <tbody><tr>
                                <td style="line-height: 1.8; border-right: 1px dotted #aaa; text-align: center;"><strong>Insurance Provided Via</strong>

                                    <p>${listingService.getInsuranceCompany(listing)}</p></td>
                                <td style="line-height: 1.8; border-right: 1px dotted #aaa; text-align: center;"><strong>Accidental insurance up to</strong>

                                    <p></p>
                                </td>
                                <td style="line-height: 1.8; text-align: center;"><strong>Death insurance up to</strong>

                                    <p></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </g:if>
            <tr>
                <td colspan="3" style="padding: 15px;">
                    <table style="width: 100%;">
                        <tbody>
                        <tr>
                            <td><h5>Cancellation Policy</h5></td>
                        </tr>
                        <tr style="border-bottom: 2px solid #ddd;">
                            <td>
                                <p><strong>1. Before Departure :</strong></p>
                                <ul type="circle">
                                    <li>You may cancel with a full refund within 24 hours of booking your ticket. After that time, tickets are fully refundable, less a $15 processing fee, if cancelled at least 2 days before the trip date. Less than 02 day(s) of the schedule departure, there is a 100% cancellation fee.</li>
                                    <li>Above 15 days prior to your arrival: 25% of the invoice amount.</li>
                                    <li>Between 14 days to 5 days prior to your arrival: 35% of the invoice amount.</li>
                                    <li>Between 4 days to 02 days prior to your arrival: 50% of the invoice amount.</li>
                                    <li>Less than 02 days prior to your arrival: 100% of the invoice amount.</li>
                                    <li>No show: 100% of the invoice amount.</li>
                                    <li>If the booked ticket is cancelled due to any of the reasons of the company, such ticket shall be postponed to next flight or shall returned the entire amount to booking agent. Transportation charge may not be refundable in case of service used.</li>
                                </ul>

                                <p><strong>2. After Departure :</strong></p>
                                <ul type="circle">
                                    <li>Ticket is nonrefundable after departure.</li>
                                </ul>

                                <p style="margin: 20px 0;">
                                    <strong>
                                        <u>CHARGE WAIVED IN CASE OF DEATH OF PASSENGER OR DEATH OF IMMEDIATE FAMILY. CERTIFICATE MUST BE NOTIFIED. GOGLIDES MAY REFUSE REFUND IF APPLICATION IS MADE LATER THAN 15DAYS AFTER EXPIRY OF E-TICKETS.</u>
                                    </strong>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 8px;"><h5>Date Change - Anytime</h5></td>
                        </tr>
                        <tr>
                            <td>
                                <ul type="circle">
                                    <li>Above 2 days prior to your arrival: 20$</li>
                                    <li>Between 24 hours to 12 hour prior to your arrival: 25$</li>
                                    <li>Between 11 hours to 5 hour prior to your arrival: 30$</li>
                                    <li>Less than 4 hours prior to your arrival: Date change not permitted ( Please contact Goglides team ).</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Terms and Conditions</h5></td>
                        </tr>
                        <tr>
                            <td><strong>Visit our page :</strong>
                                <a href="">http://goglides.com/terms & condition</a>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Email us on :</strong> <a href="#">info@goglides.com</a></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>