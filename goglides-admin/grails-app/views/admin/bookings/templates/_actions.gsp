<div class="x_panel">
    <div class="x_title">
        <h2>Actions</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled">
                <li>
                    <label class="">Payment Status:</label>
                    <g:select name="paymentStatus"
                              class="form-control"
                              from="['Pending', 'Paid', 'Failed', 'Refunded', 'Reversed']"
                              value="${booking?.paymentStatus}"
                              keys="['pending', 'paid', 'failed', 'refunded', 'reversed']"
                              onchange="statusUpdate(this.value, 'payment')"
                              noSelection="['': 'Select status ...']"/>
                </li>
                <li>
                    <label>Booking Status</label>
                    <g:select id="" name="status"
                              class="form-control"
                              from="['Processing', 'Confirmed', 'Completed', 'Cancelled', 'Trash']"
                              value="${booking?.status}"
                              keys="['processing', 'confirmed', 'completed', 'cancelled', 'trash']"
                              onchange="statusUpdate(this.value, 'status')"
                              noSelection="['': 'Select status']"/>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
    function statusUpdate(status, type) {
        if (status != '' && confirm("${message(code: 'default.confirm.message', default: 'Are you sure?')}")) {
            $("#divLoading").addClass('show');
            $.ajax({
                url: "${createLink(uri: '/admin/bookings/update-status')}",
                type: "post",
                data: {id: '${booking?.referenceNo}', bookingStatus: status, statusType: type},
                success: function (data) {
                    if (data == 'success') {

                    }
                },
                error: function (xhr) {

                },
                complete: function () {
                    $("#divLoading").removeClass('show');
                    location.reload();
                }
            });
        }
    }
</script>