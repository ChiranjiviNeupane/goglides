<div class="x_panel">
    <div class="x_title">
        <h2>Summary</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>

        <div class="clearfix"></div>

        <div class="x_content">
            <ul class="list-unstyled">
                <li>
                    <a href="${createLink(uri: '/admin/business-directory/view/' + booking?.businessDirectory?.businessId)}"
                       target="_blank">${booking?.businessDirectory?.businessName}</a>
                </li>
                <li><hr/></li>
                <li>
                    <a href="${createLink(uri: '/admin/listing/view/?id=' + booking?.listing?.referenceNo)}"
                       target="_blank">${booking?.listing?.title}</a>
                </li>
                <li><hr/></li>
                <li><label>Payment Mode:</label>
                    <span class="text-capitalize">${booking?.paymentType?.replace('_', ' ')}</span>
                </li>
                <li><label>Payment Status:</label>
                    <span class="text-capitalize">${booking?.paymentStatus?.replace('_', ' ')}</span>
                </li>
                <li><label>Booking Status:</label>
                    <span class="text-capitalize">${booking?.status?.replace('_', ' ')}</span>
                </li>
                <li><hr/></li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Created:</label> <g:formatDate
                        date="${booking?.createdAt}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Updated:</label> <g:formatDate
                        date="${booking?.modifiedAt}" type="date" style="MEDIUM"/>
                </li>
                <li>
                    <i class="glyphicon glyphicon-calendar"></i> <label>Trashed:</label> <g:formatDate
                        date="${booking?.trashedAt}" type="date" style="MEDIUM"/>
                </li>
            </ul>
        </div>
    </div>
</div>