%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use BookingService--}%
<g:set var="bookingService" bean="bookingService"/>

<div class="invoice-wrap">
    <div class="text-right" style="margin:5px 0;">
        <a href="javascript:void(0);" class="btn bttn" onClick="printDiv('printableArea')"><i class="fa fa-print"></i> Print Invoice</a>
    </div>

    <div id="printableArea" class="table-booking-invoice table-responsive">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td style="padding: 30px 15px;">
                    <img class="logo-printable" src="${assetPath(src: '/images/logo.png')}" style="width: 200px;">
                </td>
                <td colspan="2" style="padding: 15px;">
                    <table style="width: 100%; float: right;">
                        <tbody>
                        <tr>
                            <td><strong>Booking Date :</strong></td>
                            <td><g:formatDate
                                    date="${booking?.createdAt}" type="datetime"
                                    style="MEDIUM"/></td>
                        </tr>
                        <tr>
                            <td><strong>Invoice No. :</strong></td>
                            <td>${booking?.invoiceNo ?: booking?.id}</td>
                        </tr>
                        <tr>
                            <td><strong>Booking Reference :</strong></td>
                            <td>${booking?.referenceNo}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr style="line-height: 1.8;">
                <td colspan="2" style="padding: 30px 15px; vertical-align: top;">
                    <g:set var="user" value="${userService.user(booking?.user)}"/>
                    <table style="width: 100%;">
                        <tbody><tr>
                            <td><strong>Invoice To :</strong></td>
                            <td>${user?.firstname + ' ' + user?.lastname}</td>
                        </tr>
                        <tr>
                            <td><strong>Address :</strong></td>
                            <td>${booking?.address}</td>
                        </tr>
                        <tr>
                            <td><strong>Contact :</strong></td>
                            <td>${user?.mobile}</td>
                        </tr>
                        <tr>
                            <td><strong>Email :</strong></td>
                            <td>${user?.email}</td>
                        </tr>
                        </tbody></table>
                </td>

                <td style="padding: 15px;">
                    <table style="width: 100%; float: right; margin-bottom: 20px">
                        <tbody>
                        <tr>
                            <td colspan="2"
                                style="text-align: center; background: #66a5ad; color: #fff; padding: 5px;"><strong>Account Summary</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 6px;"><strong>Mode of Payment :</strong>
                            </td>
                            <td style="padding-top: 6px;">${booking?.paymentType.replace('_', ' ')}</td>
                        </tr>
                        <g:if test="${booking?.paymentType != 'credit_card'}">
                            <tr>
                                <td><strong>Sub Total :</strong></td>
                                <td>${booking?.totalPrice}</td>
                            </tr>
                            <tr>
                                <td><strong>Advanced :</strong></td>
                                <td>0.00</td>
                            </tr>
                            <tr style="background: #d5e6e8;">
                                <td style="padding: 2px 5px;"><strong>Total Balance Due :</strong>
                                </td>
                                <td>${booking?.totalPrice}</td>
                            </tr>
                            <tr>
                                <td><strong>Payment Due Date :</strong></td>
                                <td>
                                    <g:formatDate date="${booking?.createdAt + 2}" type="date"
                                                  style="MEDIUM"/></td>
                            </tr>
                        </g:if>
                        <g:else>
                            <tr>
                                <td><strong>Total :</strong></td>
                                <td>${booking?.grandTotal}</td>
                            </tr>
                        </g:else>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr style="border-top: 1px solid #e6e6e6; border-bottom: 1px solid #e6e6e6; line-height: 1.6;">
                <td colspan="2" style="padding: 30px 15px;">
                    <table style="width: 100%;">
                        <tbody><tr>
                            <td><strong>Package Name :</strong></td>
                            <td>${booking?.listing?.title}</td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><strong>Package Operator :</strong>
                            </td>
                            <td>
                                ${booking?.businessDirectory?.businessName}
                            </td>
                        </tr>
                        </tbody></table>
                </td>

                <td style="padding: 15px;">
                    <table style="width: 100%; float: right;">
                        <tbody>
                        <tr>
                            <td style="padding-top: 6px;"><strong>Flight Date :</strong></td>
                            <td style="padding-top: 6px;">${booking?.bookingDate}</td>
                        </tr>
                        %{--<tr>
                            <td><strong>Pick up time :</strong></td>
                            <td>${booking?.pickup}</td>
                        </tr>--}%
                        <tr>
                            <td><strong>Flight Time :</strong></td>
                            <td>${booking?.bookingTime}</td>
                        </tr>
                        <tr>
                            <td><strong>Flight Duration :</strong></td>
                            <td> ${listingService.formatDuration(booking?.duration)?.d}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="line-height: 1.8">
                <td colspan="3" style="padding: 15px;">
                    <table style="width: 100%; margin: 20px 0 8px;">
                        <thead>
                        <tr style="background: #66a5ad; color: #fff;">
                            <th style="padding: 5px; font-weight: normal;">TKT. No.</th>
                            <th style="padding: 5px; font-weight: normal;">PAX Name</th>
                            <th style="padding: 5px; font-weight: normal;">PAX Type</th>
                            <th style="padding: 5px; font-weight: normal;">Fare</th>
                            <th style="padding: 5px; font-weight: normal;">Photo/Video</th>
                            <th style="padding: 5px; font-weight: normal;">Transport</th>
                            <th style="padding: 5px; font-weight: normal;">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${bookingItem}">
                            <tr>
                                <td style="padding: 5px;" data-th="TKT. No.">
                                    <span class="bt-content">${it?.ticketNo}</span>
                                </td>
                                <td data-th="PAX Name">
                                    <span class="bt-content">${it?.title + ' ' + it?.fullName}</span>
                                </td>
                                <td data-th="PAX Type">
                                    <span class="bt-content">${it?.ageGroup}</span>
                                </td>
                                <td data-th="Fare">
                                    <span class="bt-content">${it?.amount}</span>
                                </td>
                                <td data-th="Photo/Video">
                                    <span class="bt-content">${it?.multimediaCharge}</span>
                                </td>
                                <td data-th="Transport">
                                    <span class="bt-content">${it?.transportationCharge}</span>
                                </td>
                                <td data-th="Total">
                                    <span class="bt-content">${it?.totalAmount}</span>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                    <table style="width: 100%;">
                        <tbody>
                        <tr style="border-bottom: 2px solid #ddd;">
                            <td style="padding:16px 0;"><strong>Total Amount :</strong></td>
                            <td style="float: right; background: #66a5ad; color: #fff; padding: 5px 40px; font-size: 22px;">${booking?.grandTotal}</td>
                        </tr>
                        <tr style="border-bottom: 2px solid #ddd;">
                            <td colspan="2" style="padding: 8px 0;"><strong>In Word :</strong> <span
                                    id="outputInvoice"></span></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="3" style="padding: 15px;">
                    <table style="width: 100%; border-bottom: 1px solid #e6e6e6; line-height: 1.6; margin:15px 0;">
                        <tbody>
                        <tr>
                            <td style="padding: 10px 0; width: 180px;">Payment Status :</td>
                            <td>${booking?.paymentStatus}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 0;">Pickup Address :</td>
                            <td>${booking?.address}</td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding: 10px 0;">Special Request :</td>
                            <td style="padding: 10px 0;">
                                ${booking?.customerComment}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table style="width: 100%; line-height: 1.8;">
                        <tbody>
                        <tr>
                            <td style="width: 180px;">Insurance :</td>
                            <td>Insurance provided by ${listingService.getInsuranceCompany(booking?.listing)}</td>
                        </tr>
                        <tr>
                            <td>Cancellation/Change :</td>
                            <td><a href="#"></a></td>
                        </tr>
                        <tr style="border-bottom: 1px solid #e6e6e6;">
                            <td style="padding-bottom: 10px;">View Booking Online :</td>
                            <td style="padding-bottom: 10px;"><a href="#"></a></td>
                        </tr>
                        <tr style="border-bottom: 1px solid #e6e6e6;">
                            <td colspan="2"
                                style="padding: 10px 0;">For feedback and complaints please visit : <a
                                    href="#"></a>
                            </td>
                        </tr>
                        <tr style="font-size: 13px;">
                            <td colspan="2" style="padding: 10px 0;">
                                Call Us: ${siteSetting.getPhone1() + ', ' + siteSetting.getPhone2()} (We're available 24 hours a day)<br>
                                Email Us: <a href="mailto:${siteSetting.getEmail()}">${siteSetting.getEmail()}</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
