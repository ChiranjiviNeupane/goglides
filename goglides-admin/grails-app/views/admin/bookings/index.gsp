%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
%{--Use FriendlyURLService--}%
<g:set var="friendlyUrlService" bean="friendlyUrlService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Manage Bookings</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Advanced Search</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
                <div class="x_content" style="display: none;">
                    <g:form url="[action: 'index', controller: 'bookings']">
                        <div class="col-xs-6">
                            <input type="text" name="search" class="form-control" placeholder="advance Booking search..." style="height: auto; padding: 7px 12px;">
                        </div>
                        <div class="col-xs-5">
                            <g:select name="searchAttribute" from="${["search by Payment Type", "search by Transaction Id","search by Payment Status", "search by Reference No.","search by Invoice No.","search by Booking Type"]}"
                                      keys="${['paymentType', 'transactionId', 'paymentStatus' , 'referenceNo','invoiceNo','bookingType']}"
                                      value="${name}"
                                      noSelection="['': '-Choose Search Category-']" class="form-control" style="height: auto; padding: 7px 12px;"/>
                        </div>
                        <div class="col-xs-1 form-group">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </g:form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Bookings <small>list of bookings ${params?.state ?: 'all'}</small></h2>

                <div class="pull-right">
                    <a class="btn btn-primary" href="${createLink(action: 'index')}"><i
                            class="fa fa-bars"></i> All</a>
                    <a class="btn btn-dark" href="${createLink(action: 'index', params: [state: 'processing'])}"><i
                            class="fa fa-file-text"></i> Processing</a>
                    <a class="btn btn-success" href="${createLink(action: 'index', params: [state: 'completed'])}"><i
                            class="fa fa-check"></i> Completed</a>
                    <a class="btn btn-danger" href="${createLink(action: 'index', params: [state: 'trash'])}"><i
                            class="fa fa-trash-o"></i> Trash</a>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <g:if test="${bookings}">
                    <div class="table-responsive">
                        <table id="" class="table table-stripped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="">Ref. No.</th>
                                <th class="">Package</th>
                                <th class="">Booking Date</th>
                                <th class="">Customer</th>
                                <th class="">Flight Date</th>
                                <th class="">Total</th>
                                <th class="">Payment</th>
                                <th class="">Status</th>
                                <th class="">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${bookings}">
                                <tr>
                                    <td class="">${it?.referenceNo ?: it?.id}</td>
                                    <td class="">${it?.listing?.title}</td>
                                    <td class="">
                                        <g:formatDate date="${it?.createdAt}" type="date" style="MEDIUM"/>
                                    </td>
                                    <td class="">
                                        <g:set var="user" value="${userService.user(it?.user)}"/>
                                        ${user?.firstname} ${user?.lastname}
                                    </td>
                                    <td class="">${it?.bookingDate}</td>
                                    <td class="text-right">${raw(it?.totalPrice)}</td>
                                    <td class="text-capitalize">${friendlyUrlService.sanitizeWithSpaces(it?.paymentStatus)}</td>
                                    <td class="text-capitalize">${friendlyUrlService.sanitizeWithSpaces(it?.status)}</td>
                                    <td class="">
                                        <a href="${createLink(action: 'view', id: it?.id)}">
                                            <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip"
                                               title="View"></i>
                                        </a>
                                    </td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix">
                        <div class="pagination pull-right">
                            <g:paginate total="${bookingCount}" class="pagination"
                                        params="${[state: params?.state]}"
                                        next='&raquo;' prev='&laquo;' maxsteps="4"/>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <p>${message(code: 'booking.recent.blank')}</p>
                </g:else>
            </div>
        </div>
    </div>
</div>
</body>
</html>