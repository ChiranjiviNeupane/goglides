%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use BookingService--}%
<g:set var="bookingService" bean="bookingService"/>
%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Booking</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-9 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Booking - ${booking?.listing?.title}</h3>
            </div>

            <div class="x_content">
                <div class="content-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#ticket" data-toggle="tab" aria-expanded="false">Ticket</a>
                                    </li>
                                    <li class="">
                                        <a href="#invoice" data-toggle="tab" aria-expanded="false">Invoice</a>
                                    </li>
                                    <li class="">
                                        <a href="#pax" data-toggle="tab" aria-expanded="false">Pax</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ticket">
                                        <g:render template="templates/ticket" model=""/>
                                    </div>

                                    <div class="tab-pane" id="invoice">
                                        <g:render template="templates/invoice" model=""/>
                                    </div>

                                    <div class="tab-pane" id="pax">
                                        <g:render template="templates/pax" model=""/>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <g:render template="templates/actions" model=""/>
        <g:render template="templates/summary" model=""/>
    </div>
</div>
<script>
    function printDiv(divName) {
        document.head.insertAdjacentHTML('beforeend', '<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css"  media="print"/>');
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

    var output = document.getElementById('output');
    output.innerHTML = numberToWords(parseInt(${booking?.totalPrice}, 10));
    var output = document.getElementById('outputInvoice');
    output.innerHTML = numberToWords(parseInt(${booking?.totalPrice}, 10));
</script>
</body>
</html>