<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Flight Lists</h2>
    </div>

    <div class="title_right">
        <div class="pull-right">

        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">

    <div class="col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <g:if test="${flightLists == 0}">
                    ${message(code: "default.norecords")}
                </g:if>
                <g:else>
                    <table id="table" class="table">
                        <thead>
                        <tr>
                            <th>Reference No.</th>
                            <th>Flight Date</th>
                            <th>Flight Time</th>
                            <th>Guests</th>
                            <th>Pickup Location</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${flightLists}">
                            <tr>
                                <td mdata-th="Reference No."><span class="bt-content">${it?.referenceNo}</span></td>
                                <td data-th="Flight Date"><span class="bt-content">${it?.bookingDate}</span></td>
                                <td data-th="Flight Time"><span class="bt-content">${it?.bookingTime}</span></td>
                                <td data-th="Guests"><span class="bt-content">${it?.noOfGuest}</span></td>
                                <td data-th="Location"><span class="bt-content">${it?.address}</span></td>
                                <td class="text-right" data-th="Action"><span class="bt-content">
                                    <a href="${createLink(uri: '/admin/bookings/edit?id=' + it?.id)}"><i
                                            class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="${createLink(uri: '/admin/bookings/view?id=' + it?.id)}"><i
                                            class="fa fa-eye" aria-hidden="true"></i></a>
                                </span></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:else>
            </div>
        </div>
    </div>
</div>
</body>
</html>