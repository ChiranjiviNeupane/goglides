%{--Use ListingService--}%
<g:set var="listingService" bean="listingService"/>
%{--Use PriceService--}%
<g:set var="priceService" bean="priceService"/>
%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use BookingService--}%
<g:set var="bookingService" bean="bookingService"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="dashboard-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-title">
    <div class="title_left">
        <h2>Booking</h2>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Booking Status</h3>

                <div class="clearfix"></div>
            </div>

        <div class="x_content">
            <g:form class="form booking-form" method="PUT" action="update" id="${booking?.id}">
                <div class="col-sm-4 form-group">
                    <label>Payment Status</label>
                    <g:select id="" name="paymentStatus"
                              class="form-control"
                              from="['Pending', 'Paid', 'Failed', 'Refunded', 'Reversed']"
                              value="${booking?.paymentStatus}"
                              keys="['pending', 'paid', 'failed', 'refunded', 'reversed']"
                              noSelection="['': 'Select payment status']"
                              required="required"/>
                </div>

                <div class="col-sm-4 form-group">
                    <label>Booking Status</label>
                    <g:select id="" name="status"
                              class="form-control"
                              from="['Processing', 'Confirmed', 'Completed', 'Cancelled', 'Trash']"
                              value="${booking?.status}"
                              keys="['processing', 'confirmed', 'Completed', 'cancelled', 'trash']"
                              noSelection="['': 'Select status']"
                              required="required"/>
                </div>

                <div class="col-sm-4 form-group">
                    <label>Action</label><br>
                    <g:submitButton id="submit-btn" name="create" class="btn bttn"
                                    value="${message(code: 'default.button.confirm.label', default: 'Update')}"/>
                </div>
            </g:form>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h3>Booking Details - ${listing?.title}</h3>
        </div>

        <div class="x_content">
            <div class="content-wrap">
                <div class="container">
                    <div class="invoice-wrap">
                        <div class="text-right" style="margin-bottom:30px;"><a href="javascript:void(0);"
                                                                               class="btn bttn"
                                                                               onClick="printDiv('printableArea')">Print Invoice</a>
                        </div>

                        <div id="printableArea" class="table-booking-invoice table-responsive">
                            <table class="tkt-wrap" style="width: 100%; border: 2px solid #ccc; font-size: 12px; color: #565656; letter-spacing: 0.5px;">
                                <tbody bgcolor="#fff">

                                <tr>
                                    <td style="padding: 15px 15px 0; width: 100px;">
                                        <img src="images/logo-sm.png" class="logo-printable" style="width: 90px;"></td>
                                    <td style="padding: 20px; vertical-align: top; line-height: 1.8;">
                                        <table style="width: 100%;">
                                            <tbody><tr>
                                                <td colspan="2">
                                                    <strong>Name :</strong>${booking?.user?.firstname + ' ' + booking?.user?.lastname}<br>
                                                    <strong>Email :</strong>${booking?.user?.email}<br>
                                                    <strong>Phone :</strong>${booking?.user?.mobile}<br>
                                                    <strong>Contact Person :</strong>${booking?.emergencyContactPerson} ${booking?.emergencyContactNo}
                                                </td>
                                                <td valign="top">
                                                    <strong>Invoice No :</strong>   ${booking?.invoiceNo}<br>
                                                    <strong>Booking Date :</strong>  <g:formatDate
                                                        date="${booking?.createdAt}" type="datetime" style="MEDIUM"/><br>
                                                    <strong>Booking Reference :</strong> ${booking?.referenceNo}
                                                </td>
                                            </tr>

                                            </tbody></table>
                                    </td>
                                    <td align="right" style="padding: 15px;">Amount : <br><strong
                                            style="font-size: 36px;">${raw(priceService.getCurrencySymbol())}${booking?.totalPrice}</strong>
                                    </td>
                                </tr>

                                <tr class="all-info">
                                    <td colspan="3" style="padding: 0 15px;">
                                        <table class="table borderless" style="width: 100%; margin: 20px 0 8px;">
                                            <thead>
                                            <tr style="background: #66a5ad; color: #fff;">
                                                <th style="padding: 5px; font-weight: normal;">TKT. No.</th>
                                                <th style="padding: 5px; font-weight: normal;">PAX Name</th>
                                                <th style="padding: 5px; font-weight: normal;">PAX Type</th>
                                                <th style="padding: 5px; font-weight: normal;">Fare</th>
                                                <th style="padding: 5px; font-weight: normal;">Photo/Video</th>
                                                <th style="padding: 5px; font-weight: normal;">Transport</th>
                                                <th style="padding: 5px; font-weight: normal;">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <g:each in="${bookingItem}">
                                                <tr>
                                                    <td style="padding: 5px;" data-th="TKT. No."><span
                                                            class="bt-content">${it?.ticketNo}</span></td>
                                                    <td data-th="PAX Name"><span
                                                            class="bt-content">${it?.title + ' ' + it?.fullName}</span></td>
                                                    <td data-th="PAX Type"><span class="bt-content">${it?.ageGroup}</span></td>
                                                    <td data-th="Fare"><span
                                                            class="bt-content">${raw(priceService.getFormattedPrice(bookingService.getRate(it?.ageGroup, bookingPrice)))}</span>
                                                    </td>
                                                    <td data-th="Photo/Video"><span
                                                            class="bt-content">${raw(priceService.getFormattedPrice(bookingService.getMultimediaRate(it, it?.ageGroup, bookingPrice)))}</span>
                                                    </td>
                                                    <td data-th="Transport"><span
                                                            class="bt-content">${raw(priceService.getFormattedPrice(bookingService.getTransportRate(it, it?.ageGroup, bookingPrice)))}</span>
                                                    </td>
                                                    <td data-th="Total"><span
                                                            class="bt-content">${raw(priceService.getFormattedPrice(bookingService.getTotalRate(it, it?.ageGroup, bookingPrice)))}</span>
                                                    </td>
                                                </tr>
                                            </g:each>
                                            </tbody>
                                        </table>
                                        <table style="width: 100%; margin-bottom: 20px;">
                                            <tbody>
                                            <tr>
                                                <td style="padding:6px 0;"><strong>Total Amount :</strong></td>
                                                <td class="title"
                                                    style="float: right; background: #66a5ad; color: #fff; padding: 4px 40px; font-size: 18px;">${raw(priceService.getFormattedPrice(booking?.totalPrice))}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"
                                                    style="padding: 8px 0; display: none;"><strong>In Word :</strong>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="border-bottom: 2px solid #ddd; border-top: 2px solid #ddd;">
                                    <td colspan="3" style="padding: 0 15px;">
                                        <table class="table borderless"
                                               style=" width: 100%; text-align: center; margin:20px 0;">

                                            <thead>
                                            <tr>
                                                <th style="padding: 5px; text-align: center;">
                                                    <strong>Flight Date</strong>
                                                </th>
                                                <th style="padding: 5px; text-align: center;">
                                                    <strong>Flight Time</strong>
                                                </th>
                                                <th style="padding: 5px; text-align: center;">
                                                    <strong>Flight Duration</strong>
                                                </th>
                                                <th style="padding: 5px; text-align: center;">
                                                    <strong>Pick up Address</strong>
                                                </th>
                                                <th style="padding: 5px; text-align: center;">
                                                    <strong>Pick up Time</strong>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr><td style="padding: 5px;" data-th="Flight Date"><span class="bt-content">${booking?.bookingDate}</span></td>
                                                <td style="padding: 5px; border-right: 1px dotted #aaa;" data-th="Flight Time"><span class="bt-content">${booking?.bookingTime}</span></td>
                                                <td style="padding: 5px; border-right: 1px dotted #aaa;" data-th="Flight Duration"><span class="bt-content">${booking?.duration}</span></td>
                                                <td style="padding: 5px; border-right: 1px dotted #aaa;" data-th="Pick up Address"><span class="bt-content">${booking?.pickup}</span></td>
                                                <td style="padding: 5px;" data-th="Pick up Time"><span class="bt-content" style=""></span></td>
                                            </tr></tbody>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="padding: 15px;">
                                        <table style="width: 100%; margin-bottom: 10px;">
                                            <tbody><tr>
                                                <td><h5>Package Info</h5></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 200px;"><img
                                                        src="${listingService.getListingFeatureImage(booking?.listing?.id)}"
                                                        style="width: 180px;"></td>
                                                <td valign="top">
                                                    <strong style="font-size: 14px;">${booking?.listing?.title}</strong>

                                                    <p>${raw(booking?.listing?.address)}</p>

                                                    ${raw(booking?.listing?.highlights)}
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>

                                <tr style="border-top: 1px solid #ddd;">
                                    <td colspan="3" style="padding: 15px;">
                                        <table style="width: 100%;">
                                            <tbody><tr>
                                                <td><h5>Special Request</h5></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    ${booking?.customerComment}
                                                </td>
                                            </tr>

                                            </tbody></table>
                                    </td>
                                </tr>
                                <g:if test="${listingService.getInsuranceCompany(listing)!= 'N.A.'}">
                                    <tr style="border-bottom: 2px solid #ddd; border-top: 2px solid #ddd;">
                                        <td colspan="3" style="padding: 15px;">
                                            <table style="width: 100%;">
                                                <tbody><tr>
                                                    <td style="line-height: 1.8; border-right: 1px dotted #aaa; text-align: center;"><strong>Insurance Provided Via</strong>

                                                        <p>${listingService.getInsuranceCompany(listing)}</p></td>
                                                    <td style="line-height: 1.8; border-right: 1px dotted #aaa; text-align: center;"><strong>Accidental insurance up to</strong>

                                                        <p></p></td>
                                                    <td style="line-height: 1.8; text-align: center;"><strong>Death insurance up to</strong>

                                                        <p></p></td>
                                                </tr>

                                                </tbody></table>
                                        </td>
                                    </tr>
                                </g:if>
                                <tr>
                                    <td colspan="3" style="padding: 15px;">
                                        <table style="width: 100%;">

                                            <tbody><tr>
                                                <td><h5>Cancellation Policy</h5></td>
                                            </tr>

                                            <tr style="border-bottom: 2px solid #ddd;">
                                                <td>
                                                    <p><strong>1. Before Departure :</strong></p>
                                                    <ul type="circle">
                                                        <li>You may cancel with a full refund within 24 hours of booking your ticket. After that time, tickets are fully refundable, less a $15 processing fee, if cancelled at least 2 days before the trip date. Less than 02 day(s) of the schedule departure, there is a 100% cancellation fee.</li>
                                                        <li>Above 15 days prior to your arrival: 25% of the invoice amount.</li>
                                                        <li>Between 14 days to 5 days prior to your arrival: 35% of the invoice amount.</li>
                                                        <li>Between 4 days to 02 days prior to your arrival: 50% of the invoice amount.</li>
                                                        <li>Less than 02 days prior to your arrival: 100% of the invoice amount.</li>
                                                        <li>No show: 100% of the invoice amount.</li>
                                                        <li>If the booked ticket is cancelled due to any of the reasons of the company, such ticket shall be postponed to next flight or shall returned the entire amount to booking agent. Transportation charge may not be refundable in case of service used.</li>
                                                    </ul>

                                                    <p><strong>2. After Departure :</strong></p>
                                                    <ul type="circle">
                                                        <li>Ticket is nonrefundable after departure.</li>
                                                    </ul>

                                                    <p style="margin: 20px 0;"><strong><u>CHARGE WAIVED IN CASE OF DEATH OF PASSENGER OR DEATH OF IMMEDIATE FAMILY. CERTIFICATE MUST BE NOTIFIED. GOGLIDES MAY REFUSE REFUND IF APPLICATION IS MADE LATER THAN 15DAYS AFTER EXPIRY OF E-TICKETS.</u>
                                                    </strong></p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="padding-top: 8px;"><h5>Date Change - Anytime</h5></td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <ul type="circle">
                                                        <li>Above 2 days prior to your arrival: 20$</li>
                                                        <li>Between 24 hours to 12 hour prior to your arrival: 25$</li>
                                                        <li>Between 11 hours to 5 hour prior to your arrival: 30$</li>
                                                        <li>Less than 4 hours prior to your arrival: Date change not permitted ( Please contact Goglides team ).</li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><h5>Terms and Conditions</h5></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Visit our page :</strong> <a
                                                        href="https://www.goglides.com">http://goglides.com</a>
                                                </td>
                                            </tr>

                                            </tbody></table>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<script>
    function printDiv(divName) {
        document.head.insertAdjacentHTML('beforeend', '<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css"  media="print"/>');
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
</body>
</html>