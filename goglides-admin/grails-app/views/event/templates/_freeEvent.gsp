<div class="event-wrap">

    <section class="event-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-7 no-padding-right">
                    <div class="info-text" style="padding-top: 45px;">
                        <h1>${event?.title}</h1>

                        <div>

                            <div class="read-more-wrap showMore">
                                ${raw(event?.description)}
                            </div>
                            <label for="post-01" class="more">Show More</label>
                        </div>

                    </div>
                </div>


                <div class="col-sm-6 col-md-5">
                    <h2 class="title-bg">Free Event</h2>
                    <i class="fa fa-caret-down title-arrow" aria-hidden="true"></i>

                    <div class="white-block text-center pricing-block">
                        <div class="contact-info">

                            <g:form action="askPhoneNumber">
                                <input type="hidden" name="eventName" value="${event?.title}"/>
                                <div class="input-group">
                                    <input type="text" class="form-control"
                                           placeholder="Enter Your Phone Number *" name="phoneNumber"
                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'> <span
                                        class="input-group-btn">
                                    <button class="btn bttn" type="submit">Submit</button>
                                </span>
                                </div>
                            </g:form>

                            <p>* Drop your contact now and we will contact you.</p></div>

                    </div>


                </div>
            </div>
        </div>
    </section>

    %{-- added section--}%
    <section>
        <div class="container">
            <div class="inquiry-block text-center">
                <h4>Want to know more about this event? <a
                        href="#inq-form" class="btn bttn">Contact Event Manager</a></h4>
            </div>
        </div>
    </section>
    %{--ends  --}%

    <section class="blue-block text-center event-counter">
        <div class="container">

            <div class="row">

                <div class="col-sm-5">
                    <div class="">
                        <p class="title no-margin-bottom">Event Date</p>

                        <p>
                            <i class="fa fa-clock-o" aria-hidden="true"></i> <g:formatDate type="date" style="MEDIUM"
                                                                                           date="${event?.startDate}"/> - <g:formatDate
                                type="date" style="MEDIUM" date="${event?.endDate}"/>
                        </p>

                        <p id="databaseDate" style="display: none;"><i class="fa fa-clock-o"
                                                                       aria-hidden="true"></i><g:formatDate
                                format="yyyy-MM-dd" date="${event?.endDate}"/></p>
                    </div>
                </div>

                <div class="col-sm-7">
                    <p id="counter"></p>
                </div>

            </div>
        </div>
    </section>

    <section class="event-info block">
        <div class="container">
            <div class="row">
                %{-- remove map and added inquery form--}%
                <div class="col-sm-6 col-md-7" id="inq-form">
                    <h2>Inquiry Form</h2>
                    <g:form action="saveUserQuery" method="post" id="userForm">
                        <input type="hidden" name="eventId" value="${event?.id}"/>

                        <div class="row">

                            <div class="form-group col-xs-12 col-sm-6 col-md-4">
                                <label for="">Full Name</label>
                                <input name="name" type="text" class="form-control" id="name"
                                       placeholder="Enter Full Name">
                            </div>

                            <div class="form-group col-xs-12 col-sm-6 col-md-4">
                                <label for="">Contact Number</label>
                                <input name="phoneNumber" type="text" class="form-control" id="phoneNumber"
                                       placeholder="Enter Phone Number"
                                       onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                            </div>

                            <div class="form-group col-xs-12 col-sm-6 col-md-4">
                                <label for="">E-mail</label>
                                <input name="email" type="email" class="form-control" id="email"
                                       placeholder="Enter Email">
                            </div>

                            <div class="form-group col-xs-12 col-sm-12">
                                <label for="">Inquiry</label>
                                <textarea name="message" rows="5" class="form-control" id="message"
                                          placeholder="If you have any queries? Please comment."></textarea>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12">
                                <recaptcha:ifEnabled>
                                    <recaptcha:recaptchaExplicit loadCallback="onloadCallback"/>
                                    <div id="html_element"></div>
                                </recaptcha:ifEnabled>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12">
                                <button type="submit" class="btn-lg btn bttn"
                                        onclick="return formValidate()">Send Message</button>
                            </div>

                        </div>
                    </g:form>
                </div>
                %{--added section ends--}%



                <div class="col-sm-6 col-md-5">
                    <div class="contact-block text-center">
                        <h3>Venue</h3>

                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> ${event?.location}</p>

                        <h3>Contact Details</h3>

                        <p><i class="fa fa-phone" aria-hidden="true"></i> <g:if test="${event?.phone1 != null}">
                            ${event?.phone1}
                        </g:if> <g:if test="${event?.phone2 != null}">
                            ${" , " + event?.phone2}
                        </g:if>
                        <g:if test="${event?.phone3 != null}">
                            ${" , " + event?.phone3}
                        </g:if>

                        </p>

                        <p><i class="fa fa-envelope-open" aria-hidden="true"></i>   ${event?.email}</p>

                        <div style="margin: 40px 0;">
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>

                            <div class="fb-share-button" data-href="https://www.goglides.com/event/${event?.slug}"
                                 data-layout="button_count" data-size="large" data-mobile-iframe="true">
                                <a target="_blank"
                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.goglides.com%2Fevent%2F${event?.slug}&amp;src=sdkpreparse"
                                   class="fb-xfbml-parse-ignore">Share</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

</div>
</div>
