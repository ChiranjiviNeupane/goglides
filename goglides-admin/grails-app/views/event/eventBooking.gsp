<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="frontend-2017"/>
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="content-wrap winner-wrap">
    <div class="container">
        <g:if test="${eventTicketType =='fee'}">
            <g:render template="/event/bookingTemplate/paidTicketEvent"/>
        </g:if>
        <g:if test="${eventTicketType=='free'}">
            <g:render template="/event/bookingTemplate/freeTicketEvent"/>
        </g:if>
    </div>

</div>

</body>
</html>