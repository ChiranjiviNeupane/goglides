%{--Use SiteSettingsService--}%
<g:set var="siteSetting" bean="siteSettingsService"/>
%{--Use CommonService--}%
<g:set var="commonService" bean="commonService"/>
%{--Use PilotService--}%
<g:set var="pilotService" bean="pilotService"/>
<!DOCTYPE html>
<html>
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>${siteSetting.getSiteTitle()} - ${siteSetting.getTagline()}</title>
    <meta name="keyword" content="${siteSetting.getMetaKeyword()}"/>
    <meta name="description" content="${siteSetting.getMetaDescription()}"/>
    <meta name="author" content="">
    <meta name="yandex-verification" content="0c013f6bdc3dffc8"/>
    <meta name="msvalidate.01" content="CB70A8C0F44A88A032381C5339D4EC87"/>
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:url" content="${createLink(uri: '/promotion', absolute: true)}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Goglides - Offer">
    <meta property="og:description" content="Visit Goglides and participate now to win a free ticket.">
    <meta property="og:image:url" content="http://mockup.goglides.com/html/images/banner.jpg">
    <meta property="fb:app_id" content="1269340669788615">
    <!-- Favicons-->
    <link rel="icon" href='${assetPath(src: '/images/favicon.ico')}' type="image/x-icon"/>

    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

    <!-- CSS -->
    <asset:stylesheet href="css/bootstrap.css"/>
    <asset:stylesheet href="css/font-awesome.css"/>
    <asset:stylesheet href="css/datetimepicker.min.css"/>
    <asset:stylesheet href="css/styles.css"/>
    <asset:stylesheet href="css/frontend-custom.css"/>
    <asset:stylesheet href="css/toastr.min.css"/>
    <asset:stylesheet href="v_0.2/bootstrap-toggle/css/bootstrap-toggle.min.css"/>
    <asset:stylesheet href="css/table.css"/>

    <!-- Jquery -->
    <asset:javascript src="js/jquery.min.js"/>
    <asset:javascript src="js/bootstrap.js"/>
    <asset:javascript src="js/moment.js"/>

    <asset:javascript src="js/datetimepicker.js"/>
    <asset:javascript src="js/toastr.min.js"/>
    <asset:javascript src="v_0.2/bootstrap-toggle/js/bootstrap-toggle.min.js"/>

    <asset:javascript src="standalone/js/jquery.validate.js"/>
</head>

<body>

<div class="wrapper">
    <div class="promo-page-wrap">
        <g:if test="${flash.msg}">
            <div class="alert alert-success alert-flat fade in alert-dismissable">
                <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>

                <div class="message" role="status"><i class="fa fa-check-circle"></i> ${flash.msg}</div>
            </div>
        </g:if>
        <div class="row">

            <div class="col-md-8 visible-xs">
                <img src="http://mockup.goglides.com/html/images/banner.jpg" class="img-responsive">
                <a href="../promo-info.html" style="padding: 15px; font-size: 16px;" target="blank">How it works ?</a>
            </div>

            <div class="col-sm-5 col-md-4">
                <div class="promo-page-wrap">
                    <form>
                        <fieldset style="display: block;">

                            <div class="form-bottom">

                                <h2>Like &amp; Share</h2>

                                </p>
                                <br>

                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=63&layout=button&action=like&size=large&show_faces=false&share=false&height=65&appId=1269340669788615"
                                                width="63" height="65" style="border:none;overflow:hidden"
                                                scrolling="no"
                                                frameborder="0" allowTransparency="true"></iframe>
                                    </div>

                                    <div class="col-sm-3">

                                    </div>
                                </div>
                                <br>
                                <br>



                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>


        </div>

    </div>
</div>
<style>
footer {
    display: none;
}
</style>
<asset:javascript src="js/table.min.js"/>
<asset:javascript src="js/scripts.js"/>
</body>
</html>