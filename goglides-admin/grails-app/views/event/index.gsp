%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!-- Fine Uploader Gallery CSS file-->
<g:set var="eventService" bean="eventService"/>
<g:set var="imageProcessingService" bean="imageProcessingService"/>

<!doctype html>
<html lang="en">
<head>
    <title>Goglides</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
%{--    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/header-footer.css">--}%
    <asset:stylesheet src="dashboard-2017/css/style.css"/>
    <asset:stylesheet src="dashboard-2017/css/font-awesome.min.css"/>
    <asset:stylesheet src="dashboard-2017/css/event.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

</head>

<body>

<div class="wrapper">

    <g:render template="eventheader/eventIndex"/>

    <div class="content-wrap winner-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 lists">
                    <a class="btn bttn" role="button" data-toggle="collapse" href="#collapseExample"
                       aria-expanded="false"
                       aria-controls="collapseExample">
                        Our Events
                    </a><br/><br/>

                    <div class="collapse in" id="collapseExample">

                        <g:if test="${event}">
                            <div class="row">
                                <g:each in="${event}" var="e">
                                    <div class="col-sm-4 text-center form-group">
                                        <div class="cart-div">

                                            <div class="img-box" id="${e?.id}" style="background-image:url('${amazonS3Service.destinationBucketCDN()+imageProcessingService.getEventImageDestionServerUrl(e?.id,'feature')}')">
                                            <script>

                                                    function imageCheck(URL) {
                                                        var tester=new Image(URL);
                                                        var eventId='${e?.id}';
                                                        tester.onerror=function (ev) {
                                                            $('#${e?.id}').css('background-image', 'url(${amazonS3Service.sourceBucketCDN()+imageProcessingService.getEventImageSourceServerUrl(e?.id)})');
                                                            imageNotFound('${e?.id}','feature','${amazonS3Service.destinationBucketCDN()+imageProcessingService.getEventImageDestionServerUrl(e?.id,'feature')}')
                                                        }
                                                        tester.src=URL;
                                                    }
                                                    imageCheck('${amazonS3Service.destinationBucketCDN()+imageProcessingService.getEventImageDestionServerUrl(e?.id,'feature')}')

                                                </script>
                                            </div>

                                        </div>

                                        <div class="img-text">
                                            <h4>
                                                <span>${e?.title}</span>
                                            </h4>

                                            <p><i class="fa fa-map-marker"></i> ${e?.location}</p>

                                            <p><i class="fa fa-calendar"></i> <g:formatDate type="date"
                                                                                            date="${e?.startDate}"
                                                                                            style="MEDIUM"/> - <g:formatDate
                                                    type="date" style="MEDIUM" date="${e?.endDate}"></g:formatDate>
                                            </p>
                                            <a href="<g:createLink absolute='true' controller='event' action="eventDetail" params="[slug: e.slug]"/>" class="btn bttn">View</a>
                                        </div>
                                    </div>


                                </g:each>
                            </div>
                        </g:if>
                        <g:else>
                            Event is not Available
                        </g:else>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    function imageNotFound(eventId, imageType, destinationUrl) {
        var imageUrl = '${amazonS3Service.destinationBucketCDN()}' + null

        if (imageUrl == destinationUrl) {
            return false;
        }
        else {
            var URL = "${createLink(controller:'imageProcessing',action:'createImage')}"
            $.ajax({
                    url: URL,
                    type: "POST",
                    data: {
                        id: eventId,
                        imageType: imageType

                    },

                    success: function (resp) {
                        console.log("resp " + resp)
                        if (resp == "success") {
                            console.log("success")
                        } else {
                            alert("failed...")
                        }
                    }
                }
            );
        }
    }

</script>
<g:render template="eventheader/eventFooter"/>

