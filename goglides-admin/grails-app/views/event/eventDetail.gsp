%{--Use SiteDropdownService--}%
<g:set var="siteDropdownService" bean="siteDropdownService"/>
%{--Use AmazonS3Service--}%
<g:set var="amazonS3Service" bean="amazonS3Service"/>
<!-- Fine Uploader Gallery CSS file-->
<g:set var="eventService" bean="eventService"/>
<g:set var="siteSetting" bean="siteSettingsService"/>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <meta name="og:url" content="${createLink(uri: '/event/' + event?.slug, absolute: true)}"/>
    <meta name="og:title" content="${siteSetting.getSiteTitle() + ' - ' + event?.title}"/>
    <meta name="og:type" content="place"/>
    <meta name="og:description" content="${event?.title}"/>
    <meta name="og:image" content="${amazonS3Service?.sourceBucketCDN() + eventMultimedia?.file}"/>
    <meta property="fb:app_id" content="1269340669788615">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    %{--    <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/header-footer.css">--}%
    <asset:stylesheet src="dashboard-2017/css/style.css"/>
    <asset:stylesheet src="dashboard-2017/css/event.css"/>
    <asset:stylesheet src="dashboard-2017/css/font-awesome.min.css"/>
    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="dashboard-2017/js/moment.min.js"/>
</head>

<body>

<div class="wrapper">
    <g:render template="eventheader/eventNavbar"/>

    <section class="banner-block" id="#myImg">
        <img src="${amazonS3Service?.sourceBucketCDN() + eventMultimedia?.file}" class="img-responsive event-banner"
             id="myImg">
    </section>



    <g:if test="${eventCharge.size() == 1}">
        <g:render template="/event/templates/eventTicket1"/>
    </g:if>
    <g:if test="${eventCharge.size() == 2}">
        <g:render template="/event/templates/eventTicket2"/>
    </g:if>
    <g:if test="${eventCharge.size() == 3}">
        <g:render template="/event/templates/eventTicket3"/>
    </g:if>
    <g:if test="${eventCharge.size() > 3}">
        <g:render template="/event/templates/eventTickets"/>
    </g:if>
    <g:if test="${eventCharge.size() == 0}">
        <g:render template="/event/templates/freeEvent"/>
    </g:if>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> <!-- goglides -->
                <ins class="adsbygoogle" style="display:inline-block;width:1140px;height:91px"
                     data-ad-client="ca-pub-9695312075487390"
                     data-ad-slot="9886728344"></ins>
                <script> (adsbygoogle = window.adsbygoogle || []).push({}); </script>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        var onloadCallback = function () {
            grecaptcha.render('html_element', <recaptcha:renderParameters theme="" type="image" tabindex="2"/>);
        };

        $('.more').click(function () {
        var texts=$('.more').text();
        if(texts=='Show More'){
            $('.showMore').css(
        {
            "height": "auto",
            "overflow": "auto",
			'transition': 'all 0.5s ease'
        });
            $('.more').text("Show Less");

        }
        if(texts=='Show Less'){
            $('.showMore').css(
                {
                    "height": "200px",
                    "overflow": "hidden",
					'transition': 'all 0.5s ease'
                });
            $('.more').text("Show More");
        }

        });

  function formValidate() {
      if(!$('#name').val()){
          alert("please enter name")
          return false;
      }


      if(!$('#phoneNumber').val()){
          alert("please enter PhoneNumber")
          return false;
      }

      if(!$('#email').val()){
          alert("please enter Email")
          return false;
      }

      if(!$('#message').val()){
          alert("please enter message")
          return false;
      }

  }
    </script>
    <script>

        // Set the date we're counting down to

        var time = '${event?.endTime}';
        console.log(" time " + time)
        var d = moment(time, 'HH:mm: A').diff(moment().startOf('day'), 'seconds');

        var countDownDate = new Date($('#databaseDate').text().replace(/-/g, "/"));
        countDownDate.setDate(countDownDate.getDate() + 1)
        var x = setInterval(function () {
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate.getTime() - now;
            console.log("distance " + distance)

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("counter").innerHTML = days + "<span> Days </span> " + hours + "<span> Hours</span> "
                + minutes + "<span> Min </span> " + seconds + "<span> Sec <span>";

            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("counter").innerHTML = "EXPIRED";
            }
        }, 1000);


        $("#myImg").error(function () {
            $(this).hide();
        });


    </script>
<style>
@media( max-width:767px){ .event-banner{ height:auto;}}
</style>

<g:render template="eventheader/eventFooter"/>