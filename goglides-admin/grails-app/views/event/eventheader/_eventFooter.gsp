<footer>
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 ft-logo">
                    <a href="${createLink(uri: '/../')}"><img src="${createLink(uri: '/../assets/images/ft-logo.png')}"
                                                                                  class="img-responsive"></a>

                    <p><strong>Thrill with gliding experience !</strong></p>
                </div>

                <div class="col-sm-3">
                    <h6>about</h6>
                    <ul class="list-unstyled">
                        <li><a href="${createLink(uri: '/../about-us')}">About Us</a></li>
                        <li><a href="${createLink(uri: '/../terms-and-conditions')}">Terms & Conditions</a></li>
                        <li><a href="${createLink(uri: '/../privacy-polices')}">Privacy Policy</a></li>
                        <li><a href="${createLink(uri: '/../contact')}">Contact Us</a></li>
                        <li><a href="${createLink(uri: '/../career')}">Career</a></li>
                    </ul>
                </div>

                <div class="col-sm-4">
                    <h6>our address</h6>

                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> JP Marg, Thamel Kathmanu, Nepal</p>

                    <p><i class="fa fa-phone" aria-hidden="true"></i> +977-14219235, 14263722</p>

                    <p><i class="fa fa-envelope-open" aria-hidden="true"></i> info@goglides.com</p>
                </div>

                <div class="col-sm-2">
                    <h6>business hours</h6>

                    <p>Mon-Fri  9am-6pm</p>

                    <p>Sat  Closed</p>

                    <p>Sun  9am-11am</p>
                </div>
            </div>
        </div>
    </section>

    <section class="social-block text-center">
        <ul class="list-inline">
            <li><a href="https://www.facebook.com/goglides"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://twitter.com/go_glides"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="https://plus.google.com/+Goglides"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCgl6AILUwi4QQzgeaCnw6qw?guided_help_flow=3"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            <li><a href="https://goglides.tumblr.com"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/goglides"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>

        <p>Copyright © 2018 GoGlides. All rights reserved</p>
    </section>

</footer>
<!-- JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>