<g:set var="commonService" bean="commonService"/>
<g:set var="userService" bean="userService"/>
<header>
    <div class="container">
        <div class="row">

            <div class="col-xs-6 col-sm-3 logo">
                <a href="${createLink(uri: '/../')}"><img src="${createLink(uri: '/../assets/images/ft-logo.png')}"
                                                          class="img-responsive"></a>
            </div>

            <div class="col-sm-9">
                <nav class="navbar navbar-default navigation-bar">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <g:if test="${userService.currentUser()}">
                                <g:if test="${userService.getRole() == "ROLE_ADMIN" || userService.getRole()=="ROLE_STAFF"}">
                                    <li><a href="${createLink(uri: '/admin/dashboard')}">Dashboard</a></li>
                                </g:if></g:if>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">Categories <i _ngcontent-c2=""
                                                                                            class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="${createLink(uri: '/../search')}">Paragliding</a></li>
                                    <li><a href="${createLink(uri: '/../search')}">Bungy</a></li>
                                    <li><a href="${createLink(uri: '/../search')}">Rafting</a></li>
                                </ul>
                            </li>
                            <li><a href="${createLink(uri: '/event')}">Events</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">Offers <i _ngcontent-c2=""
                                                                                            class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                     <li><a href="${createLink(uri: '/../winner-list')}">Winner List</a></li>


                                </ul>
                            </li>


                            <li><a href="https://blog.goglides.com">blog</a></li>

                            <g:if test="${userService.currentUser()}">
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                       aria-expanded="false">${userService.user(userService.currentUser())?.firstname+" "+userService.user(userService.currentUser())?.lastname } <i
                                            class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="tog"><a href="${createLink(uri: '/../my-booking')}"><i
                                                class="fa fa-shopping-cart"></i>  Booking</a></li>
                                        <li class="tog"><a href="${createLink(uri: '/../my-wishlist')}"><i
                                                class="fa fa-heart"></i> Wish List</a></li>
                                        <!-- <li class="tog"><a routerLink="/"><i class="fa fa-comment"></i> Reviews</a></li> -->

                                        <li class="tog"><a href="${createLink(uri: '/../profile')}"><i
                                                class="fa fa-pencil"></i>  edit profile</a></li>
                                        <li class="tog"><a href="${createLink(uri: '/sso/logout')}"><i
                                                class="fa fa-power-off"></i> logout</a></li>
                                    </ul>
                                </li>
                            </g:if>
                            <g:else>
                                <li><a href="${createLink(uri: '/')}">login</a></li>
                            </g:else>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>

        </div>
    </div>
</header>