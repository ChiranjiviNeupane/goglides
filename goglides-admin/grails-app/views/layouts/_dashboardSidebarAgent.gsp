<ul class="nav side-menu">
    <li class="<g:if test="${params.controller == 'dashboard'}">active</g:if>"><a
            href="${createLink(uri: '/dashboard')}"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a><i class="fa fa-list-ul" aria-hidden="true"></i> Listings <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="${createLink(uri: '/listing')}">Manage</a></li>
            <li><a href="${createLink(uri: '/listing/recently-viewed')}">Recently Viewed</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Bookings<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class=""><a href="${createLink(uri: '/agent-bookings')}">Manage</a></li>
            <li><a href="${createLink(uri: '/walk-in')}">Add New</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-group" aria-hidden="true"></i> Staffs<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="${createLink(uri: '/manage-staff')}">Manage</a></li>
            <li><a href="${createLink(uri: '/manage-staff/create')}">Add New</a></li>
        </ul>
    </li>
    <li class="<g:if test="${params.controller == 'wish-list'}">active</g:if>"><a
            href="${createLink(uri: '/wish-list')}"><i class="fa fa-list-alt"></i> Wish List</a></li>
</ul>