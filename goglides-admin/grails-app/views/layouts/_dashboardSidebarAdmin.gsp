<g:set var="userService" bean="userService"/>

<ul class="nav side-menu">

    <li class="<g:if test="${params.controller == 'dashboard'}">active</g:if>"><a
            href="${createLink(uri: '/admin/dashboard')}"><i class="fa fa-home"></i> Dashboard</a></li>
<li>
    <g:if test="${userService.getRole() == "ROLE_ADMIN"}">
        <a><i class="fa fa-list-ul" aria-hidden="true"></i> Listings <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="${createLink(uri: '/admin/listing')}">Manage</a></li>
            <li><a href="${createLink(uri: '/admin/listing-review')}">Listing Review</a></li>
        </ul>
        </li>
        <li>
            <a><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Bookings <span
                    class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li class=""><a href="${createLink(uri: '/admin/bookings')}">Manage</a></li>
            </ul>
        </li>
        <li>
            <a><i class="fa fa-briefcase" aria-hidden="true"></i> Business Directories <span
                    class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li class=""><a href="${createLink(uri: '/admin/business-directory')}">Manage</a></li>
                %{--<li class=""><a href="${createLink(uri: '/admin/claim-business')}">Business Claims</a></li>--}%
            </ul>
        </li>
        <li>
            <a><i class="fa fa-users" aria-hidden="true"></i> Users <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="${createLink(uri: '/admin/user')}">Users</a></li>
                <li><a href="${createLink(uri: '/admin/contact-book')}">Contact Book</a></li>
            </ul>
        </li>
        <li>
            <a><i class="fa fa-users" aria-hidden="true"></i> Marketing <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="${createLink(uri: '/admin/promotion')}">Manage</a></li>
            </ul>
        </li>
    %{--<li>--}%
    %{--<a><i class="fa fa-users" aria-hidden="true"></i> Pilots<span class="fa fa-chevron-down"></span></a>--}%
    %{--<ul class="nav child_menu">--}%
    %{--<li><a href="${createLink(uri: '/admin/pilot')}">Pilots</a></li>--}%
    %{--</ul>--}%
    %{--</li>--}%
    %{--<li>--}%
    %{--<a><i class="fa fa-users" aria-hidden="true"></i> Staffs<span class="fa fa-chevron-down"></span></a>--}%
    %{--<ul class="nav child_menu">--}%
    %{--<li><a href="${createLink(uri: '/admin/manage-staff')}">Manage</a></li>--}%
    %{--<li><a href="${createLink(uri: '/admin/manage-staff/create/')}">Add Staff</a></li>--}%
    %{--</ul>--}%
    %{--</li>--}%

        <li>
            <a><i class="fa fa-shopping-basket" aria-hidden="true"></i> Event<span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li><a href="${createLink(uri: '/admin/event')}">Manage</a></li>
                <li><a href="${createLink(uri: '/admin/event/create')}">Add Event</a></li>
                <li><a href="${createLink(uri: '/admin/queryTicket')}">Query Ticket</a></li>
            </ul>
        </li>


    %{--<li>--}%
    %{--<a><i class="fa fa-shopping-basket" aria-hidden="true"></i> Marketing<span class="fa fa-chevron-down"></span>--}%
    %{--</a>--}%
    %{--<ul class="nav child_menu">--}%
    %{--<li><a href="${createLink(uri: '/admin/promotion')}">Manage</a></li>--}%
    %{--<li><a href="${createLink(uri: '/admin/promotion/create')}">Add New</a></li>--}%
    %{--</ul>--}%
    %{--</li>--}%
        <li>
            <a><i class="fa fa-cogs" aria-hidden="true"></i> Settings <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="${createLink(uri: '/admin/site-settings')}">Site Settings</a></li>
                <li><a href="${createLink(uri: '/admin/forex')}">Forex Rate</a></li>
            </ul>
        </li>

    </g:if>
    <g:else>

        <li>
            <a><i class="fa fa-users" aria-hidden="true"></i> Contact Detail <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li><a href="${createLink(uri: '/admin/contact-book')}">Contact Book</a></li>
            </ul>
        </li>
    </g:else>
</ul>