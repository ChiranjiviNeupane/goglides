<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert-msg">
                <g:if test="${flash.message}">
                    <div class="alert alert-success alert-flat fade in alert-dismissable">
                        <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>

                        <div class="message" role="status"><i class="fa fa-check-circle"></i> ${flash.message}</div>
                    </div>
                </g:if>
                <g:if test="${flash.success}">
                    <div class="alert alert-success alert-flat fade in alert-dismissable">
                        <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>

                        <div class="message" role="status"><i class="fa fa-check-circle"></i> ${flash.success}</div>
                    </div>
                </g:if>
                <g:if test="${flash.warning}">
                    <div class="alert alert-warning alert-flat fade in alert-dismissable">
                        <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>

                        <div class="message" role="status"><i class="fa fa-warning"></i> ${flash.warning}</div>
                    </div>
                </g:if>
                <g:if test="${flash.danger}">
                    <div class="alert alert-danger alert-flat fade in alert-dismissable">
                        <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>

                        <div class="message" role="status"><i class="fa fa-close"></i> ${flash.danger}</div>
                    </div>
                </g:if>
            </div>

            <div class="message"></div>
        </div>
    </div>
</div>
