<g:set var="siteSetting" bean="siteSettingsService"/>
<g:set var="commonService" bean="commonService"/>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
<head>
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>${siteSetting.getSiteTitle()}</title>
    <meta name="keyword" content="${siteSetting.getMetaKeyword()}"/>
    <meta name="description" content="${siteSetting.getMetaDescription()}"/>
    <meta name="author" content="">

    <!-- Favicons-->
    <link rel="icon" href='${assetPath(src: '/dashboard-2017/images/favicon.ico')}' type="image/x-icon"/>

    <!-- CSS -->
    <asset:stylesheet href="dashboard-2017/css/bootstrap.min.css"/>
    <asset:stylesheet href="dashboard-2017/css/font-awesome.min.css"/>
    <asset:stylesheet href="dashboard-2017/css/jquery.mCustomScrollbar.min.css"/>
    <asset:stylesheet href="dashboard-2017/css/datetimepicker.min.css"/>
    <asset:stylesheet href="dashboard-2017/css/toastr.min.css"/>
    <asset:stylesheet href="dashboard-2017/css/style.css"/>
    <asset:stylesheet href="dashboard-2017/css/custom.css"/>

    <!-- jQuery -->
    <asset:javascript src="dashboard-2017/js/jquery.min.js"/>
    <!-- Bootstrap -->
    <asset:javascript src="dashboard-2017/js/bootstrap.min.js"/>
    <!-- jQuery custom content scroller -->
    <asset:javascript src="dashboard-2017/js/jquery.mCustomScrollbar.concat.min.js"/>
    <!-- bootstrap-daterangepicker -->
    <asset:javascript src="dashboard-2017/js/moment.min.js"/>
    <asset:javascript src="dashboard-2017/js/datetimepicker.js"/>
    <asset:javascript src="dashboard-2017/js/toastr.min.js"/>
    <!-- Custom scripts -->
    <asset:javascript src="dashboard-2017/js/jquery.validate.min.js"/>
    <asset:javascript src="dashboard-2017/js/script.js"/>
    <asset:javascript src="dashboard-2017/js/numToWords.test.js"/>


    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Google Analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-79840196-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <g:render template="/layouts/dashboardSidebar2017"/>
        <g:render template="/layouts/dashboardTopNav2017"/>
        <!-- page content -->
        <div class="right_col" role="main">
            <g:render template="/layouts/flashMessage"/>
            <g:layoutBody/>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Copyright &copy; <g:formatDate format="yyyy" date="${new Date()}"/>
                . ${siteSetting.getSiteTitle()} - ${siteSetting.getTagline()}.
                Ver.-1.0.0001
            </div>

            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<div id="divLoading"></div>
<!-- Custom Theme Scripts -->
<asset:javascript src="dashboard-2017/js/custom.js"/>
</body>
</html>