%{--Use UserService--}%
<g:set var="userService" bean="userService"/>
<g:set var="amazonS3Service" bean="amazonS3Service"/>

<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="${createLink(uri: '/../')}" class="site_title hidden-xs"><img
                    src="${assetPath(src: '/dashboard-2017/images/logo.png')}" class="img-responsive"></a>
            <a href="${createLink(uri: '/../')}" class="site_title visible-xs"><img
                    src="${assetPath(src: '/dashboard-2017/images/logo-sm.png')}" class="img-responsive"></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="${amazonS3Service.sourceBucketCDN()+userService.user(userService.currentUser())?.profile}" alt="..."
                     class="img-circle profile_img" id="imageNotFound" onerror="userProfileNotFound()">
            </div>

            <div class="profile_info">
                <span>Welcome,</span>
                <h2>${userService.user(userService.currentUser())?.firstname}</h2>
            </div>

            <div class="clearfix"></div>
        </div>
        <!-- /menu profile quick info -->
        <br/>
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <g:render template="/layouts/dashboardSidebarAdmin" model="[]"/>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>
<script>
    function userProfileNotFound() {
        $('#imageNotFound').attr('src',"${assetPath(src: '/dashboard-2017/images/user.png')}")
    }
</script>