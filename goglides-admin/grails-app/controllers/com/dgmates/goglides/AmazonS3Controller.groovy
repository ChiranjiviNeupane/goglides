package com.dgmates.goglides

import grails.converters.JSON
import grails.util.Environment

class AmazonS3Controller {

    AmazonS3Service amazonS3Service
    def springSecurityService
    UserService userService

    def s3Signing() {
        render amazonS3Service.s3Signing().toString()
    }

    def s3Key() {
        def user = userService.currentUser()
        println("User s   "+user)
        def result
        if (params.directory) {
            result = params.directory + "/" + Environment.current.name.encodeAsHex() + "/" + user.id.encodeAsMD5() + "/" + params.filename
        } else {
            result = Environment.current.name.encodeAsHex() + "/" + user.id.encodeAsMD5() + "/" + params.filename
        }
        render result
    }

    def s3UploadSuccess() {
        params.put("urlS3", amazonS3Service.s3Upload(params?.key))
        render "success"
    }

    def s3Delete() {
        amazonS3Service.s3Delete(params.bucket, params?.key)
        def result = [status: "success"]
        render result as JSON
    }

}