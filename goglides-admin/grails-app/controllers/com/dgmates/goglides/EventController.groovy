package com.dgmates.goglides

import com.megatome.grails.RecaptchaService
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.RequestMapping

import static org.springframework.http.HttpStatus.NOT_FOUND


class EventController {
    def springSecurityService
    def emailMessagingService
    AmazonS3Service amazonS3Service
    def siteSettingsService
    RecaptchaService recaptchaService

    def index(Integer max) {
        Date reminderDate = new Date().clearTime()
        def event=Event.createCriteria().list{
            eq("status", 'publish')
            ge('endDate',reminderDate)
        }
        render(view: 'index',model: [event: event])
    }


    def eventDetail(){
        def slug=params?.slug
        Date reminderDate = new Date().clearTime()
        def event=Event.findBySlugAndStatus(slug,"publish")
        if(event){
            if(event?.endDate < reminderDate){
            flash.message="This event is expired."
                redirect(action: 'index')
                return
            }

        def eventMultimedia=EventMultimedia.findByEvent(event)
        def eventCharge=EventCharge.findAllByEventAndStatus(event,"publish")
        if(eventCharge!=null){
            render (view: '/event/eventDetail',model: [
                    event: event,
                    eventMultimedia: eventMultimedia,
                    eventCharge: eventCharge
            ])
        }}
        else {
            notFound()
            return
        }

    }





    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }



    def joinUs() {
        render(view: "social-network")
    }

    @Transactional
    def saveUserQuery(){
        def recaptchaOK = true
        if (!recaptchaService.verifyAnswer(session, request.getRemoteAddr(), params)) {
            recaptchaOK = false
        }

        if (recaptchaOK) {
            recaptchaService.cleanUp(session)

            def event=Event.get(params?.eventId)
            def name=params?.name
            def email=params?.email
            def phoneNumber=params?.phoneNumber
            def message=params?.message
            def queryTicket=new QueryTicket(
                    event: event,
                    name: name,
                    email: email,
                    phoneNumber: phoneNumber,
                    message: message
            ).save(failOnError:true, flush:true)

            def userMessage = "<h2> Event Name: "+event?.title+"</h2><br/> User Name: " + params.name + " <br>From : " + params.email + '<br>Message:<br>' + params.message
            if (emailMessagingService.sendEmail(siteSettingsService.getEmail(),siteSettingsService.getEmail(), 'GoGlides - Event Query',userMessage )) {
                flash.message = "Your query has been submitted."
            } else {
                flash.message = "Your query cannot be submitted at the moment."
            }


        } else {
            flash.message = "Invalid captcha. Your query is not submitted."

            redirect(uri: '/event')
            return
        }
        redirect(action: 'index')
        return

    }


    @Transactional
    def askPhoneNumber(){
        def phoneNumber=params?.phoneNumber
        def eventName=params?.eventName
        def message="<h1>Event Name: "+eventName+"<br/> <h1>Contact Number: "+phoneNumber+"</h1>"
        if (emailMessagingService.sendEmail(siteSettingsService.getEmail(), siteSettingsService.getEmail() , 'GoGlides -  Event: Phone Number',message )) {
            flash.message = "Your query has been submitted."
        } else {
            flash.message = "Your query cannot be submitted at the moment."
        }
        redirect(action: 'index')

    }


}
