package com.dgmates.goglides.admin

import grails.converters.JSON
import grails.transaction.Transactional
import com.dgmates.goglides.*
import org.codehaus.groovy.runtime.InvokerHelper


import static org.springframework.http.HttpStatus.*

class ListingPackageController {
    static namespace = 'admin'

    ListingPackageService listingPackageService

    def view() {
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        def listing = listingPackage?.listing
        def listingAvailability = ListingAvailability.findByListingAndListingPackage(listing, listingPackage)
        def listingAvailabilityDays = ListingAvailabilityDays.findByListingAndListingPackage(listing, listingPackage)
        def listingDurations = ListingDuration.findAllByListingAndListingPackage(listing, listingPackage)
        def listingSchedules = ListingSchedule.findAllByListingAndListingPackage(listing, listingPackage)
        def packagePrice = ListingPrice.findAllByListingAndListingPackageAndStatus(listing, listingPackage, 'publish')
        def listingAddons = ListingAddons.findByListingAndListingPackage(listing, listingPackage)
        def listingAddonMultimedia = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'multimedia')
        def listingAddonTransport = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'transport')
        def discountPolicies = ListingGroupDiscount.findAllByListingAndListingPackage(listing, listingPackage)

        render(view: 'view', model: [
                'listing'                : listing,
                'listingPackage'         : listingPackage,
                'listingAvailability'    : listingAvailability,
                'listingAvailabilityDays': listingAvailabilityDays,
                'listingDurations'       : listingDurations,
                'listingSchedules'       : listingSchedules,
                'packagePrice'           : packagePrice,
                'listingAddons'          : listingAddons,
                'listingAddonMultimedia' : listingAddonMultimedia,
                'listingAddonTransport'  : listingAddonTransport,
                'discoutnPolicies'       : discountPolicies

        ])
    }

    @Transactional
    def updateStatus() {
        if (!request.xhr) {
            notFound()
            return
        }
        def msgStatus
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        if (listingPackage) {
            listingPackage.status = params?.value
            listingPackage.save flush: true
            msgStatus = 'success'
        }
        return msgStatus
    }

    @Transactional
    def delete() {
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        if (listingPackage) {
            listingPackage.status = 'trash'
            listingPackage.save flush: true

            def listingPackagePrice = ListingPrice.findAllByListingPackage(listingPackage)
            if (listingPackagePrice) {
                listingPackagePrice.each { price ->
                    price.status = 'trash'
                    price.save flush: true
                }
            }
        }
        redirect(action: 'view', id: listingPackage?.packageCode)
        return
    }

    def edit() {
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        def listing = listingPackage?.listing
        render(view: 'edit', model: [
                'listingPackage': listingPackage,
                'listing'       : listing
        ])
    }

    @Transactional
    def update(ListingPackage listingPackage) {
        if (listingPackage == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (listingPackage.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond listingPackage.errors, view: 'create'
            return
        }
        listingPackage.save flush: true

        redirect(action: 'edit-package-options', id: listingPackage?.packageCode)
        return
    }

    def editPackageOptions() {
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        def listing = listingPackage?.listing
        def listingAvailability = ListingAvailability.findByListingAndListingPackage(listing, listingPackage) ?: new ListingAvailability()
        def listingAvailabilityDays = ListingAvailabilityDays.findByListingAndListingPackage(listing, listingPackage) ?: new ListingAvailabilityDays()
        def listingDurations = ListingDuration.findAllByListingAndListingPackage(listing, listingPackage)
        def listingSchedules = ListingSchedule.findAllByListingAndListingPackage(listing, listingPackage)
        render(view: 'edit_package_options', model: [
                listing                : listing,
                listingPackage         : listingPackage,
                listingAvailability    : listingAvailability,
                listingAvailabilityDays: listingAvailabilityDays,
                listingDurations       : listingDurations,
                listingSchedules       : listingSchedules
        ])
    }

    def updatePackageOptions(ListingPackage listingPackage) {
        def listingPackageId = listingPackage.id
        def listing = listingPackage?.listing
        def listingAvailabilityDays = ListingAvailabilityDays.findById(listingPackage.listingAvailabilityDays.id)
        if (listingAvailabilityDays != null) {
            listingAvailabilityDays.sunday = (params?.sunday == "on") ? 1 : 0
            listingAvailabilityDays.monday = (params?.monday == "on") ? 1 : 0
            listingAvailabilityDays.tuesday = (params?.tuesday == "on") ? 1 : 0
            listingAvailabilityDays.wednesday = (params?.wednesday == "on") ? 1 : 0
            listingAvailabilityDays.thursday = (params?.thursday == "on") ? 1 : 0
            listingAvailabilityDays.friday = (params?.friday == "on") ? 1 : 0
            listingAvailabilityDays.saturday = (params?.saturday == "on") ? 1 : 0
            listingAvailabilityDays.save(flush: true)
        } else {
            def listingAvailability = new ListingAvailabilityDays()
            listingAvailability.sunday = (params?.sunday == "on") ? 1 : 0
            listingAvailability.monday = (params?.monday == "on") ? 1 : 0
            listingAvailability.tuesday = (params?.tuesday == "on") ? 1 : 0
            listingAvailability.wednesday = (params?.wednesday == "on") ? 1 : 0
            listingAvailability.thursday = (params?.thursday == "on") ? 1 : 0
            listingAvailability.friday = (params?.friday == "on") ? 1 : 0
            listingAvailability.saturday = (params?.saturday == "on") ? 1 : 0
            listingAvailability.listing = listing
            listingAvailability.listingPackage = listingPackage
            listingAvailability.save(flush: true, failOnError: true)
        }

        def lisAvailability = ListingAvailability.findByListingPackage(listingPackage)
        if (lisAvailability != null) {
            lisAvailability.availabilityType = params?.availabilityType
            lisAvailability.unAvailabilityType = params?.unAvailabilityType
            if (params.availabilityType == 'date-single') {
                lisAvailability.fromDate = Date.parse('yyyy-MM-dd', params.singleFrom)
            } else {
                lisAvailability.fromDate = Date.parse('yyyy-MM-dd', params.fromDate)
                lisAvailability.toDate = Date.parse('yyyy-MM-dd', params.toDate)
            }

            if (params.unAvailabilityType == 'date-single') {
                lisAvailability.unAvailabilityFromDate = Date.parse('yyyy-MM-dd', params.unAvailabilitySingleDate)
            } else {
                lisAvailability.unAvailabilityFromDate = Date.parse('yyyy-MM-dd', params.unAvailabilityFromDate)
                lisAvailability.unAvailabilityToDate = Date.parse('yyyy-MM-dd', params.unAvailabilityToDate)
            }
            lisAvailability.save(flush: true)
        } else {
            def listingAvailability = new ListingAvailability()
            listingAvailability.listing = listing
            listingAvailability.listingPackage = listingPackage
            listingAvailability.availability = 'available'
            listingAvailability.availabilityType = params?.availabilityType
            listingAvailability.unAvailabilityType = params?.unAvailabilityType
            if (params.availabilityType == 'date-single') {
                listingAvailability.fromDate = Date.parse('yyyy-MM-dd', params.singleFrom)
            } else {
                listingAvailability.fromDate = Date.parse('yyyy-MM-dd', params.fromDate)
                listingAvailability.toDate = Date.parse('yyyy-MM-dd', params.toDate)
            }

            if (params.unAvailabilityType == 'date-single') {
                listingAvailability.unAvailabilityFromDate = Date.parse('yyyy-MM-dd', params.unAvailabilitySingleDate)
            } else {
                listingAvailability.unAvailabilityFromDate = Date.parse('yyyy-MM-dd', params.unAvailabilityFromDate)
                listingAvailability.unAvailabilityToDate = Date.parse('yyyy-MM-dd', params.unAvailabilityToDate)
            }
            listingAvailability.save()
        }

        ListingDuration.where { listingPackage == listingPackage }.deleteAll()
        def listingDurations = params.list('duration')
        if (listingDurations.size() > 1) {
            listingDurations.eachWithIndex { value, index ->
                if (value != '') {
                    new ListingDuration(
                            listing: listing,
                            listingPackage: listingPackage,
                            duration: value,
                    ).save(flush: true)
                }
            }
        } else {
            new ListingDuration(
                    listing: listing,
                    listingPackage: listingPackage,
                    duration: params.duration,
            ).save(failOnError: true)
        }

        ListingSchedule.where { listingPackage == listingPackage }.deleteAll()
        def listingSchedules = params.list('schedule')

        if (listingSchedules.size() > 1) {
            listingSchedules.eachWithIndex { value, index ->
                if (value != '') {
                    new ListingSchedule(
                            listing: listing,
                            listingPackage: listingPackage,
                            schedule: value
                    ).save(flush: true)
                }
            }
        } else {
            new ListingSchedule(
                    listing: listing,
                    listingPackage: listingPackage,
                    schedule: params.schedule
            ).save(failOnError: true)
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageOption')
                redirect(action: 'editPackagePrice', id: listingPackage?.packageCode)
            }
            '*' { respond listing, [status: CREATED] }
        }
    }

    @Transactional
    def removeDuration() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingDuration = ListingDuration.get(params.id)
        if (listingDuration) {
            listingDuration.delete(flush: true)
            render 'success'
        }
        return
    }

    @Transactional
    def removeSchedule() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingSchedule = ListingSchedule.get(params.id)
        if (listingSchedule) {
            listingSchedule.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def editPackagePrice() {
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        def listing = listingPackage?.listing
        def listingSchedules = ListingSchedule.findAllByListingAndListingPackage(listing, listingPackage)
        def listingDurations = ListingDuration.findAllByListingAndListingPackage(listing, listingPackage)
        def listingPrices = ListingPrice.findAllByListingAndListingPackageAndStatus(listing, listingPackage, 'publish')
        render(view: 'edit_package_price', model: [
                listing         : listing,
                listingPackage  : listingPackage,

                listingSchedules: listingSchedules,
                listingDurations: listingDurations,
                listingPrices   : listingPrices
        ])
    }

    def updatePackagePrice(ListingPackage listingPackage) {
        def listing = listingPackage?.listing
        def ageGroup = params.list('ageGroup')
        def listingPrice
        def listingPrices = ListingPrice.findAllByListingPackage(listingPackage)
        if (listingPrices) {
            listingPrices.each { price ->
                def bookingItem = BookingItems.findByListingPrice(price)
                if (!bookingItem) {
                    price.delete(flush: true)
                } else {
                    price.status = 'trash'
                    price.save(flush: true)
                }
            }
        }
        if (ageGroup.size() > 1) {
            ageGroup.eachWithIndex { value, index ->
                if (value != '') {
                    listingPrice = new ListingPrice(
                            listing: listing,
                            listingPackage: listingPackage,
                            ageGroup: value,
                            wholeSaleRate: params.wholeSaleRate[index],
                            retailRate: params.retailRate[index]
                    )
                    if (params.duration) {
                        listingPrice.duration = params.duration[index]

                    }
                    if (params.type) {
                        listingPrice.type = params.type[index]
                    }
                    if (params.rateType == 'yes') {
                        listingPrice.pricingType = 'scheduled'
                        listingPrice.schedule = params.schedule[index]
                    }
                    if (params.wholeSaleRate[index] && params.retailRate[index] && listingPrice.validate())
                        listingPrice.save(flush: true)
                    else {
                        transactionStatus.setRollbackOnly()
                        respond listingPrice.errors, view: 'package-pricing'
                        return
                    }
                }
            }
        } else {
            listingPrice = new ListingPrice(
                    listing: listing,
                    listingPackage: listingPackage,
                    ageGroup: params.ageGroup,
                    wholeSaleRate: params.wholeSaleRate,
                    retailRate: params.retailRate
            )
            if (listing?.category?.slug == 'paragliding') {
                listingPrice.duration = params.duration
            }
            if (params.type) {
                listingPrice.type = params.type
            }
            if (params.rateType == 'yes') {
                listingPrice.pricingType = 'scheduled'
                listingPrice.schedule = params.schedule
            }
            if (params.wholeSaleRate && params.retailRate && listingPrice.validate())
                listingPrice.save(flush: true)

        }
        listingPackage.rateType = params.rateType
        listingPackage.save(flush: true)
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packagePricing')
                redirect(action: 'editPackageAddons', id: listingPackage?.packageCode)
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removePrice() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingPrice = ListingPrice.get(params.id)
        if (listingPrice) {
            listingPrice.delete(flush: true)
            render 'success'
        }
        return
    }

    def editPackageAddons() {
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        def listing = listingPackage?.listing
        def listingAddons = ListingAddons.findByListingAndListingPackage(listing, listingPackage)
        def listingAddonPrice = new ListingAddonsPrice()
        def listingAddonMultimedia = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'multimedia')
        def listingAddonTransport = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'transport')
        render(view: 'edit_package_addons', model: [
                listing               : listing,
                listingPackage        : listingPackage,
                listingAddons         : listingAddons,
                listingAddonPrice     : listingAddonPrice,
                listingAddonMultimedia: listingAddonMultimedia,
                listingAddonTransport : listingAddonTransport
        ])
    }

    def updatePackageAddons(ListingPackage listingPackage) {
        def listing = listingPackage?.listing
        ListingAddons.where { listingPackage == listingPackage }.deleteAll()
        def listingAddons = new ListingAddons(params)
        listingAddons.listing = listing
        listingAddons.listingPackage = listingPackage
        listingAddons.save(flush: true)

        ListingAddonsPrice.where { listingPackage == listingPackage }.deleteAll()
        def listingAddonMultimedia
        if (params.includedMultimedia == 'no') {
            def multimediaAgeGroup = params.list('multimediaAgeGroup')
            if (multimediaAgeGroup.size() > 1) {
                multimediaAgeGroup.eachWithIndex { value, index ->
                    if (value != '') {
                        listingAddonMultimedia = new ListingAddonsPrice()
                        listingAddonMultimedia.listing = listing
                        listingAddonMultimedia.listingPackage = listingPackage
                        listingAddonMultimedia.addonType = 'multimedia'
                        listingAddonMultimedia.ageGroup = params.multimediaAgeGroup[index]
                        listingAddonMultimedia.wholesaleRate = params.multimediaWholesaleRate[index]
                        listingAddonMultimedia.retailRate = params.multimediaRetailRate[index]
                        listingAddonMultimedia.save(flush: true)
                    }
                }
            } else {
                listingAddonMultimedia = new ListingAddonsPrice()
                listingAddonMultimedia.listing = listing
                listingAddonMultimedia.listingPackage = listingPackage
                listingAddonMultimedia.addonType = 'multimedia'
                listingAddonMultimedia.ageGroup = params.multimediaAgeGroup
                listingAddonMultimedia.wholesaleRate = params.multimediaWholesaleRate
                listingAddonMultimedia.retailRate = params.multimediaRetailRate
                listingAddonMultimedia.save(flush: true)
            }
        }

        def listingAddonTransport
        if (params.includedTransport == 'no') {
            def transportAgeGroup = params.list('transportAgeGroup')
            if (transportAgeGroup.size() > 1) {
                transportAgeGroup.eachWithIndex { value, index ->
                    if (value != '') {
                        listingAddonTransport = new ListingAddonsPrice()
                        listingAddonTransport.listing = listing
                        listingAddonTransport.listingPackage = listingPackage
                        listingAddonTransport.addonType = 'transport'
                        listingAddonTransport.ageGroup = params.transportAgeGroup[index]
                        listingAddonTransport.wholesaleRate = params.transportWholesaleRate[index]
                        listingAddonTransport.retailRate = params.transportRetailRate[index]
                        listingAddonTransport.save(flush: true)
                    }
                }
            } else {
                listingAddonTransport = new ListingAddonsPrice()
                listingAddonTransport.listing = listing
                listingAddonTransport.listingPackage = listingPackage
                listingAddonTransport.addonType = 'transport'
                listingAddonTransport.ageGroup = params.transportAgeGroup
                listingAddonTransport.wholesaleRate = params.transportWholesaleRate
                listingAddonTransport.retailRate = params.transportRetailRate
                listingAddonTransport.save(flush: true)
            }
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageAddon')
                redirect(action: 'editPackagePolicy', id: listingPackage?.packageCode)
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeListingAddons() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingAddons = ListingAddonsPrice.get(params.id)
        if (listingAddons) {
            listingAddons.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def editPackagePolicy() {
        def listingPackage = listingPackageService.listingPackageByCode(params?.id)
        def listing = listingPackage?.listing
        def listingGroupDiscount = new ListingGroupDiscount()
        def groupDiscounts = ListingGroupDiscount.findAllByListingAndListingPackage(listing, listingPackage)

        render(view: 'edit_package_policy', model: [
                listing             : listing,
                listingPackage      : listingPackage,
                listingGroupDiscount: listingGroupDiscount,
                groupDiscounts      : groupDiscounts
        ])
    }

    @Transactional
    def updatePackagePolicy(ListingPackage listingPackage) {
        def listing = listingPackage?.listing
        ListingGroupDiscount.where { listingPackage == listingPackage }.deleteAll()
        def groupDiscount
        def listingGroupDiscount = params.list('discountPercent')
        if (listingGroupDiscount.size() > 1) {
            listingGroupDiscount.eachWithIndex { value, index ->
                if (value != '') {
                    groupDiscount = new ListingGroupDiscount()
                    groupDiscount.listing = listing
                    groupDiscount.listingPackage = listingPackage
                    groupDiscount.groupPolicy = params.groupPolicy[index]
                    groupDiscount.discountFrom = params.discountFrom[index]
                    groupDiscount.discountTo = params.discountTo[index]
                    groupDiscount.discountPercent = value
                    groupDiscount.save(flush: true)
                }
            }
        } else {
            groupDiscount = new ListingGroupDiscount()
            groupDiscount.listing = listing
            groupDiscount.listingPackage = listingPackage
            groupDiscount.groupPolicy = params.groupPolicy
            groupDiscount.discountFrom = params.discountFrom
            groupDiscount.discountTo = params.discountTo
            groupDiscount.discountPercent = params.discountPercent
            groupDiscount.save(flush: true)
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageGroupDiscount')
                redirect(controller: 'listing', action: 'view', namespace: 'admin', id: listing?.referenceNo)
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeGroupDiscount() {
        if (!request.xhr) {
            notFound()
            return
        }
        def groupDiscount = ListingGroupDiscount.get(params.id)
        if (groupDiscount) {
            groupDiscount.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'listingPackage.label', default: 'ListingPackage'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

}