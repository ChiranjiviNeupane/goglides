package com.dgmates.goglides.admin

import com.dgmates.goglides.DashboardService
import com.dgmates.goglides.ReportBookingService
import com.dgmates.goglides.ReportBusinessDirectoryService
import com.dgmates.goglides.ReportUserService
import com.dgmates.goglides.UserService

import javax.servlet.http.HttpServletRequest

class DashboardController {

    static namespace = 'admin'

    DashboardService dashboardService
    ReportBookingService reportBookingService
    ReportUserService reportUserService
    ReportBusinessDirectoryService reportBusinessDirectoryService
    UserService userService

    def index(Integer max) {
        params.max = Math.min(params.max?.toInteger() ?: 10, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def userInstance = userService.currentUser()

        def lifetimeSales = dashboardService.getLifetimeSales(userInstance)
        def monthlySales = dashboardService.getMonthlySales(userInstance)
        def totalListings = dashboardService.getTotalListings(userInstance)
        def totalBookings = dashboardService.getTotalBookings(userInstance)

        def stats = [:]
        stats.lifetimeSales = lifetimeSales
        stats.monthlySales = monthlySales
        stats.totalListings = totalListings
        stats.totalBookings = totalBookings

        [stats: stats]
    }

    def getFlightDetails() {
        params.max = 5
        params.max = Math.min(params.max?.toInteger() ?: 10, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def userInstance = userService.currentUser()
        def flightLists = reportBookingService.getFlightList(userInstance, params)
        render('view': 'templates/_list_flights', model: [
                flightLists: flightLists
        ])
    }

    def getBookingDetails() {
        params.max = 5
        params.max = Math.min(params.max?.toInteger() ?: 10, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def userInstance = userService.currentUser()
        def bookingLists = reportBookingService.getBookingList(userInstance, params)
        render('view': 'templates/_list_bookings', model: [
                bookingLists: bookingLists
        ])
    }

    def getUserDetails() {
        params.max = 5
        params.max = Math.min(params.max?.toInteger() ?: 10, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def userInstance = userService.currentUser()
        def userLists = reportUserService.getUserList(userInstance, params)
        render('view': 'templates/_list_users', model: [
                userLists: userLists
        ])
    }

    def getBusinessDirectoryDetails() {
        params.max = 5
        params.max = Math.min(params.max?.toInteger() ?: 10, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def userInstance = userService.currentUser()
        def directoryLists = reportBusinessDirectoryService.getDirectoryList(userInstance, params)
        render('view': 'templates/_list_business_directories', model: [
                directoryLists: directoryLists
        ])
    }

    def logoff(){
        userService.logoutUrl()
       // HttpServletRequest.logout()
        // redirect(url: userService.logoutUrl +"/protocol/openid-connect/logout?&redirect_uri=http%3A%2F%2Flocalhost%3A8082%2Fsso%2Flogin&state=7e4331a3-88e8-4409-8e02-9e597c5b694b&login=true&scope=openid")
    }

}