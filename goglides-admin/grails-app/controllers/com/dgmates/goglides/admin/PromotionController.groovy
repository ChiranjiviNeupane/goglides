package com.dgmates.goglides.admin

import com.dgmates.goglides.Promotion
import com.dgmates.goglides.PromotionUser
import com.dgmates.goglides.User
import com.dgmates.goglides.UserService

//import com.dgmates.goglides.Testimonial
//import com.dgmates.helpnetwork.User
import grails.converters.JSON
import grails.transaction.Transactional
import org.codehaus.groovy.runtime.InvokerHelper

import java.sql.Date
import java.text.SimpleDateFormat

@Transactional
class PromotionController {

    static namespace = 'admin'
    static allowedMethods = [save: "POST", delete: "GET"]

    def promotionService
    def amazonS3Service
    UserService userService


    def index() {
        def promotion = Promotion.createCriteria().list() {}
        render(view: "/admin/promotion/index", model: [promotion: promotion])
        return
    }

    def create() {
        respond new Promotion(params)
    }

    def save(Promotion promotion) {
        String key
        def title = params.title.toString()
        def description = params.description.toString()
        def hasPopUp = params.hasPopUp.toString()
        def status = params.status12.toString()
        def startDate = dateConverter(params?.startDate)
        def endDate = dateConverter(params?.endDate)
        def type = params.type.toString()
        def sort = params.sort
        def banner = key
        def resultAnnouncementDate = dateConverter(params?.resultAnnouncementDate)
        def bannerImage = params.bannerImage.toString()
        def user = userService.currentUser()
        def promotions = new Promotion(
                title: title,
                banner: banner,
                description: description,
                startDate: startDate,
                endDate: endDate,
                hasPopUp: hasPopUp,
                status: status,
                type: type,
                sort: sort,
                resultAnnouncementDate:resultAnnouncementDate,
                bannerImage: bannerImage,
                user: user
        ).save(flush: true, failOnError: true)
        key=null
        redirect(action: 'index')
    }

    def edit(int id) {
        def promotion = Promotion.get(id)
        render(view: "/admin/promotion/edit", model: [promotion: promotion])
        return
    }

    @Transactional
    def update() {
        String key
        def promotion = Promotion.get(params?.id)
        promotion.title = params.title.toString()
        promotion.hasPopUp = params.hasPopUp.toBoolean()
        promotion.status = params.status12.toString()
        promotion.description = params.description.toString()
        promotion.startDate = dateConverter(params?.startDate)
        promotion.endDate = dateConverter(params?.endDate)
        promotion.type = params.type.toString()
        promotion.sort = Integer.parseInt(params.sort)
        promotion.bannerImage =params?.bannerImage.toString()
        promotion.save(flush: true, failOnError: true)
        redirect(action: 'index')
    }

    def delete(int id) {
        def promotion = Promotion.get(id)
        promotion.delete flush: true
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'promotion.label', default: 'Promotion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def participants() {
        def promotionId = params?.id
        def promotion = Promotion.get(promotionId)
        def participants = PromotionUser.findAllByPromotion(promotion, [sort: 'winnerRank', order: 'DESC'])
        render('view': 'index-participant', model: [
                promotion   : promotion,
                participants: participants
        ])
    }

    def winnerList() {
        def promotion = Promotion.get(params?.id)
        if (promotion?.hasResultDrawn != '1') {
            def startDate = promotion?.startDate //replace by promotion start date
            def endDate = promotion?.endDate //replace by promotion end date
                promotionService.PickWinners(promotion, startDate, endDate)
            flash.info = "Congratulation! Contest winners are selected."
        } else {
            flash.info = 'Winner list already drawn.'
        }
        redirect(action: 'participants', params: [id: promotion?.id])
    }

    def restResult() {
        def promotion = Promotion.get(params?.id)
        promotionService.ResetWinnerList(promotion)
        flash.success = 'Winner list reset completed.'
        redirect(action: 'index')
    }

    def view(Promotion promotion) {
        def participants = PromotionUser.findAllByPromotion(promotion, [sort: 'winnerRank', order: 'DESC'])
        def sql = "SELECT count(createdAt) as count, DATE(createdAt) as date FROM PromotionUser WHERE promotion=:promotion group by DATE(createdAt) order by (createdAt)"
        def participantList = PromotionUser.executeQuery(sql, [promotion: promotion])
        def label = []
        def data = []
        participantList.each { val ->
            label.push(val[0])
            data.push(val[1])
        }
        data = data.collect { "'" + it + "'" }
        render(view: 'view', model: [
                promotion   : promotion,
                label       : label,
                data        : data,
                participants: participants
        ])
    }

    public Date dateConverter(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
        java.util.Date utilDate = simpleDateFormat.parse(date)
        Date sqlDate = new Date(utilDate.getTime())
        return sqlDate
    }

    @Transactional
    def multimediaUploadSuccess() {
        String key
        println("promotion id"+params?.id)
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        key = params?.key
        def promotion=Promotion?.get(params?.id)
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        println("param key"+params?.key)
        if(promotion){
            promotion.banner=params?.key
            promotion.save(failOnError:true, flush: true)
        }
        else {
            key = params?.key
        }

    }



    @Transactional
    def multimediaDelete() {
        amazonS3Service.s3Delete(params.bucket, params.key)
        key = null
        def result = [status: "success"]
        render result as JSON
    }

    def viewParticipatedUser() {
        def promotionId = params?.id
        def promotion = Promotion.get(promotionId)
        def participants = PromotionUser.findAllByPromotion(promotion,[sort: 'user',order: 'asc'])
        render('view': 'promotionUserDetails', model: [
                promotion   : promotion,
                participants: participants
        ])
    }
    def winnerUpdate(){
        def promotion = Promotion.createCriteria().list{
            order("id","desc")
        }
        render(view: 'winnerUpdate', model:[promotion: promotion])
    }

    def getUserList(params,Integer max){
        def userList = []
        def pid = params?.pid
        params.max = Math.min(max ?: 30, 1000)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def promotion = Promotion.get(pid)
        def promotionUser = PromotionUser.createCriteria()
        def promUser = promotionUser.list(max: params.max, offset: params.offset) {
            eq('promotion',promotion)
            order("winnerRank","desc")
        }
        def i =0 ;
        promUser.each { pu->
            def user = User.get(pu.user.id)
            userList[i] = [
                    promotionUser: pu,
                    user: userService.mapUser(user)
            ]
            i++
        }
        render userList as JSON
        return
    }

    def setWinnerRank(params) {
        def uImage = params?.uImage
        def promotionUser = PromotionUser.findById(params?.id)
        promotionUser.contactStatus = params?.contactStatus
        promotionUser.prizeClaim = params?.prizeClaim
        promotionUser.userImage = uImage
        promotionUser.winnerRank = params?.rank
            if (promotionUser.save(flush: true, failOnError: true)) {
                render promotionUser as JSON
                return
            } else {
                render(message:"update unsuccessful")
            }
        }

    def insertBannerImage(params){
        def sql = "from Promotion p where p.startDate='"+params?.s+"' and p.endDate='"+params?.e+"'";
        if(Promotion.executeQuery(sql)){
            def bannerImage =  params?.bannerImage;
            def sql1 = "UPDATE Promotion p SET p.bannerImage = ? where p.startDate = ? AND endDate = ?"
            def updateEntry = Promotion.executeUpdate(sql,[bannerImage,params?.s,params?.e])
            if(updateEntry == true){
                render("Inserted")
                return
            }
        }else{
            render("Entry not found in database")
            return
        }
    }

    def rankCount(params){
        println(params.pid)
        def sql = "select count(id) from PromotionUser WHERE winnerRank is not null and promotion="+params?.pid
        def count = PromotionUser.executeQuery(sql)
        render(count)
        return
    }
}
