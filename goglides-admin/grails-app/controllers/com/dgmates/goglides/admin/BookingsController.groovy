package com.dgmates.goglides.admin

import com.dgmates.goglides.BookingService
import com.dgmates.goglides.UserService
import grails.transaction.Transactional
import static org.springframework.http.HttpStatus.*
import com.dgmates.goglides.Listing
import com.dgmates.goglides.BookingItems
import com.dgmates.goglides.ListingPackage
import com.dgmates.goglides.ListingPayment

class BookingsController {
    static namespace = 'admin'
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    UserService userService
    BookingService bookingService

    def index(Integer max) {
        params.max = Math.min(max ?: 30, 100)
        def bookings
        def searchData = params?.search
        def searchAttribute = params?.searchAttribute
        def bookingCount=0

        if(searchData){
            def wild = '*' + searchData.toLowerCase().trim() + '*'
            bookings=ListingPayment.search().list(){
                wildcard searchAttribute, wild
            }

        }else {
            switch (params?.state) {
                case 'all':
                    bookings = bookingService.bookings(userService.currentUser(), params)
                    break
                default:
                    bookings = bookingService.bookings(userService.currentUser(), params)
                    break
            }
            bookingCount=bookings.totalCount
        }

        render(view: 'index', model: [
                bookings: bookings,
                bookingCount:bookingCount
        ])
    }

    def view(int id) {
        def booking = ListingPayment.get(id)
        def bookingItem = BookingItems.findAllByListingPayment(booking)
        render(view: 'view', model: [
                booking    : booking,
                bookingItem: bookingItem
        ])
    }

    def edit(int id) {
        def booking = ListingPayment.get(id)
        def listing = Listing.get(booking?.listingId)
        def listingPackage = ListingPackage.get(booking?.listingPackageId)
        def bookingItem = BookingItems.findAllByListingPayment(booking)
        def bookingPrice = bookingService.getBookedPackagePrice(listing, listingPackage, booking)
        render(view: 'edit', model: [
                booking     : booking,
                listing     : listing,
                bookingItem : bookingItem,
                bookingPrice: bookingPrice])
    }

    @Transactional
    def update(int id) {
        def booking = ListingPayment.get(id)
        booking.paymentStatus = params.paymentStatus
        booking.status = params.status
        booking.save(failOnError: true)

        request.withFormat {
            form multipartForm {
                flash.message = 'Booking updated successfully.'
                redirect(action: 'view', id: booking.id)
            }
            '*' { respond listing, [status: CREATED] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'businessDirectory.label', default: 'BusinessDirectory'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def updateStatus() {
        if (request.xhr) {
            def referenceNo = params.id
            def type = params.statusType
            def status = params.bookingStatus
            def booking = ListingPayment.findByReferenceNo(referenceNo)
            if (type == 'payment') {
                booking.paymentStatus = status
            } else if (type == 'status') {
                booking.status = status
            }
            booking.save flush: true
        }
    }
}