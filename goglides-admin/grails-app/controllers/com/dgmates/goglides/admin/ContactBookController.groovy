package com.dgmates.goglides.admin

import com.dgmates.goglides.ContactBook
import grails.transaction.Transactional
import org.hibernate.search.cfg.PropertyDescriptor


class ContactBookController {
    static namespace = 'admin'
    static allowedMethods = [save: "POST", update: "PUT"]

    def index(Integer max) {
        def list
        def listSize
        def searchData = params?.search
        def searchCategory = params?.searchAttribute
        if (searchData) {

            def wild = '*' + searchData.toLowerCase().trim() + '*'
            switch (searchCategory) {
                case 'businessType':
                    list = ContactBook.search().list() {
                        wildcard "businessType", wild
                    }
                    break

                case 'companyName':
                    list = ContactBook.search().list() {
                        wildcard "companyName", wild
                    }
                    break

                case 'email':
                    list = ContactBook.search().list() {
                        wildcard "email", wild
                    }
                    break

                case 'contactPerson':
                    list = ContactBook.search().list() {
                        wildcard "contactPerson", wild
                    }
                    break

                default:
                    list = ContactBook.search().list() {
                        wildcard "email", wild
                        wildcard "businessType", wild
                        wildcard "companyName", wild
                        wildcard "contactPerson", wild
                    }
            }
        } else {
            list = ContactBook.list(max: params.max ?: 10, offset: params.offset ?: 0)

        }
        listSize = list.size()

        render(view: "index", model: [contactBook: list, contactBookSize: listSize])
        return
    }

    def create() {
        def contactBook = new ContactBook(params)
        render(view: "create", model: [contactBook: contactBook])
    }

    def save(ContactBook contactBook) {
        if (contactBook == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        contactBook.save(fallOnError: true)
        redirect(action: 'index')
    }

    def updates(int id) {
        def contactBook = ContactBook.get(id)
        render(view: "edit", model: [contactBook: contactBook])
        return
    }

    @Transactional
    def update(ContactBook contactBook) {
        if (contactBook == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        contactBook.save(flush: true, fallOnError: true)
        redirect(action: 'index')
    }

    @Transactional
    def delete(int id) {
        def contactBook = ContactBook.get(id)
        contactBook.delete flush: true
        redirect(action: 'index')
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'promotion.label', default: 'Promotion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

}
