package com.dgmates.goglides.admin

import com.dgmates.goglides.Event
import com.dgmates.goglides.EventBooking
import com.dgmates.goglides.EventCharge
import com.dgmates.goglides.EventMultimedia
import com.dgmates.goglides.EventTicket
import com.dgmates.goglides.ImageProcessing
import grails.converters.JSON
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

import java.sql.Date

import static org.springframework.http.HttpStatus.NOT_FOUND

class EventController {
    static namespace = 'admin'
    def amazonS3Service
    def userService
    def imageProcessingService
    def index(Integer max) {
        def events = Event.list(max: params.max ?: 10, offset: params.offset ?: 0)
        render(view: '/admin/event/index', model: [events: events,size:Event.count()])
    }


    def create(){
        def event=new Event(params)
        render (view: 'createEventDescription',model: [event:event])
    }

    def testRich(){
        render(view: 'test')
    }

    def saveEventDescription(){
        def user = userService.currentUser()
        def title=params?.title
        def location=params?.location
        def startDate=Date.parse('yyyy-MM-dd',params?.startDate)
        def endDate=Date.parse('yyyy-MM-dd',params?.endDate)
        def startTime=params?.startTime
        def endTime=params?.endTime
        def description=params?.description
        def status=params?.status12
        def totalAvailableStock=params?.totalAvailableStock
        def eventType=params?.eventType
        def event=new Event(
                user: user,
                title: title,
                location: location,
                startDate: startDate,
                endDate: endDate,
                startTime: startTime,
                endTime: endTime,
                description: description,
                status: status,
                totalAvailableStock: totalAvailableStock,
                eventType: eventType
        ).save(failOnError: true,flush:true)
        render (view: 'createEventMultimedia', model: [event:event])
    }

    @Transactional
    def saveSetting(Event event) {
        event.save(failOnError: true, flush: true)
        redirect(action: 'index')
    }

    @Transactional
    def multimediaImagesUploadSuccess() {
        // println(params)
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        def event_id = params.id
        def event = Event.findById(event_id)
        def featureImage = new EventMultimedia(event: event, file: params.key)
        featureImage.save(flush: true)
        render "success"
    }


    @Transactional
    def multimediaImagesDelete() {
        amazonS3Service.s3Delete(params.bucket, params.key)
        def featureImage = EventMultimedia.findWhere(file: params.key)
        if (featureImage) {
            featureImage.delete(flush: true)
        }
        def result = [status: "success"]
        render result as JSON
    }

    @Transactional
    def delete(int id) {

        def event = Event.get(id)
        event.status="Trash"
//        EventMultimedia.findAllByEvent(event).each {eventMultimedia ->
//            eventMultimedia.delete(failOnError: true, flush: true)
//        }
        EventCharge.findAllByEvent(event).each{ eventCharge ->
            eventCharge.status="Trash"
            eventCharge.save(failOnError: true, flush: true)
        }
        /*  EventBooking.findAllByEvent(event).each{ eventMultimedia ->
              eventMultimedia.status='Trash'
              eventMultimedia.save(failOnError: true, flush: true)
          }*/
        event.save(failOnError: true, flush: true)
        redirect(action: 'index')
    }




    @Transactional
    def view(int id){
        def event=Event.get(id)
        def eventMultimedia=EventMultimedia.findByEvent(event)
        def eventCharge=EventCharge.findAllByEvent(event)
        render (view: 'show', model: [
                event: event,
                eventCharge: eventCharge,
                eventMultimedia:eventMultimedia
        ])
    }


    def updateDescription( int id) {
        def event = Event.get(id)
        render(view: 'edit', model: [
                event      : event
        ])
    }

    @Transactional
    def eventDescriptionUpdate(){
        def event=Event.get(params?.id)
        event.title = params.title.toString()
        event.location = params.location.toString()
        event.status=params?.status12.toString()
        event.startDate = Date.parse('yyyy-MM-dd', params.startDate)
        event.startTime = params.startTime.toString()
        event.endDate = Date.parse('yyyy-MM-dd', params.endDate)
        event.endTime = params.endTime.toString()
        event.description = params.description.toString()
        event.save(failOnError:true, flush: true)
        redirect(action:'index')
    }

    @Transactional
    def updateImage(int id){
        def event = Event.get(id)
        def eventMultimedia=EventMultimedia.findByEvent(event)
        render(view: 'updateEventMultimedia', model: [
                event      : event,
                eventMultimedia: eventMultimedia
        ])
    }


    @Transactional
    def multimediaImagesUploadUpdateSuccess() {
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        def event_id = params.id
        def imageType=params?.imageType
        def event = Event.findById(event_id)
        def eventMultimedia=EventMultimedia.findByEvent(event)
        if(eventMultimedia) {
            amazonS3Service.s3Delete(amazonS3Service.sourceBucket(),eventMultimedia.file)
            def resizeImageUrl=imageProcessingService.checkImageType(eventMultimedia?.file,imageType)
            imageProcessingService.deleteResizeImageFromS3DestinationBucket(resizeImageUrl)
            eventMultimedia.file = params?.key
            eventMultimedia.save(failOnError: true, flush: true)
        }else {
            new EventMultimedia(event: event, file: params.key).save(flush:true, failOnError:true)
        }
        render "success"
    }

    def addNewTicket(){
        def event=Event.get(params?.id)
        render (view: "createEventTicket",model: [event: event])

    }

    @Transactional
    def addTicket(){
        def event=Event.get(params?.eventId)
        def ticketTitle=params?.ticketTitle
        def rate=params?.rate
        def description=params?.description
        def wholeSaleRate=params?.wholeSaleRate
        def retailRate=params?.retailRate
        def status=params?.status12
        def availableStock=params?.availableStock
        def eventCharge=new EventCharge(
                event: event,
                ticketTitle: ticketTitle,
                rate: rate,
                description: description,
                wholeSaleRate: wholeSaleRate,
                retailRate: retailRate,
                status: status,
                availableStock: availableStock
        ).save(failOnError: true, flush: true)

//        def list=EventCharge.list();
//        if(list.size()>0){
//            event.eventType="fee"
//            event.save(failOnError: true, flush: true)
//        }
        redirect(action: 'view',params: [id:params?.eventId])
    }

    @Transactional
    def updateTicket( ) {
        def event=Event.get(params?.eid)
        def eventCharge=EventCharge.get(params?.id)
        render(view: '/admin/event/updateTicket', model: [
                eventCharge: eventCharge,
                event: event
        ])

    }

    @Transactional
    def deleteTicket(){
        def eventCharge=EventCharge.get(params?.id)
        if(eventCharge){
            eventCharge.status='Trash'
            eventCharge.save(failOnError: true, flush: true)
            render 'success'
        }
        return

    }

    def updateEventTickets(){

        def event=Event.get(params?.eventId)
        def eventCharge=EventCharge.get(params?.eventChargeId)
        eventCharge.ticketTitle=params?.ticketTitle
        eventCharge.description=params?.description
        eventCharge.wholeSaleRate=params?.wholeSaleRate
        eventCharge.retailRate=params?.retailRate
        eventCharge.status=params?.status12
        eventCharge.availableStock=params?.availableStock
        eventCharge.save(flush:true,failOnError: true)
        redirect(action: 'view',params: [id:event?.id])
        //remaining update tickets....
    }


    @Transactional
    def bookingDetails(){
        def event=Event.get(params?.id)
        def eventBooking=EventBooking.findAllByEvent(event)
        render (view: 'showBookingDetail',model:[
                eventBooking:eventBooking,
                event: event
        ])
    }


    def printTicket(){
        def eventBooking=EventBooking.get(params?.id)
        def eventTicket=EventTicket.findAllByEventBooking(eventBooking)
        render(view: 'printTicket', model: [
                eventBooking: eventBooking,
                eventTicket:eventTicket
        ])
    }


    def editEventBooking(){
        def eventBooking=EventBooking?.get(params?.id)
        render(view: 'editEventBookingPayment',model: [
                eventBooking: eventBooking
        ])
    }



    def updateEventBooking(){

        def eventBooking=EventBooking.get(params?.id)
        eventBooking.firstname=params?.firstname
        eventBooking.lastname=params?.lastname
        eventBooking?.phone=params?.phone
        eventBooking?.email=params?.email
        eventBooking?.quantity=params?.quantity
        eventBooking?.totalAmount=params?.totalAmount
        eventBooking?.paymentStatus=params?.paymentStatus
        eventBooking?.paymentMode=params?.paymentMode
        eventBooking?.status=params?.status12
        eventBooking.save(failOnError: true, flush: true)
        redirect(action: 'bookingDetails',params: [id:eventBooking?.event?.id])

    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }



}