package com.dgmates.goglides

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ForexController {
    static namespace = 'admin'
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def forexList = Forex.list(params)
        respond Forex.list(params), model: [forexCount: Forex.count(), forex: forexList]
    }

    def show(Forex forex) {
        respond forex
    }

    def convert(params) {
        def usRate = params.rate
        def current = Forex.findByCurrencyCode("NPR")
        def rate = current.rate
        def npr = usRate.toInteger() * rate
        render npr
        return
    }

    def create() {
        respond new Forex(params)
    }

    @Transactional
    def save(Forex forex) {
        if (forex == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (forex.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond forex.errors, view: 'create'
            return
        }

        forex.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'forex.label', default: 'Forex'), forex.id])
                redirect forex
            }
            '*' { respond forex, [status: CREATED] }
        }
    }

    def edit(int id) {
        def value = Forex.get(id)
        render(view: "/admin/forex/edit", model: [forex: value])
    }

    @Transactional
    def update(Forex forex) {
        if (forex == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (forex.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond forex.errors, view: 'edit'
            return
        }

        forex.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'forex.label', default: 'Forex'), forex.id])
                redirect forex
            }
            '*' { respond forex, [status: OK] }
        }
    }

    def del(int id) {
        if (id == null) {
            notFound()
            return
        } else {
            def forex = Forex.get(id)
            forex.delete flush: true
            redirect(action: 'index')
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'forex.label', default: 'Forex'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
