package com.dgmates.goglides.admin

import com.dgmates.goglides.*
import grails.converters.JSON
import grails.transaction.Transactional
import org.codehaus.groovy.runtime.InvokerHelper
import java.sql.Date
import java.text.SimpleDateFormat
import static org.springframework.http.HttpStatus.*

class ListingController {

    static namespace = 'admin'
    static allowedMethods = [save: "POST", update: "PUT"]

    AmazonS3Service amazonS3Service
    ListingService listingService
    UserService userService
    def imageProcessingService

    def index(Integer max) {
        params.max = Math.min(params.max?.toInteger() ?: 30, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def listings
        def listingSize = 0
        def userInstance = userService.currentUser()
        def searchData=params?.search
        def searchAttribute=params?.searchAttribute
        if(searchData){
            def wild = '*' + searchData.toLowerCase().trim() + '*'
            switch (searchAttribute)
            {
                case 'title':
                    listings = Listing.search().list(){
                        wildcard "title",wild
                    }
                    break
                case "referenceNo":
                    listings = Listing.search().list(){
                        wildcard "referenceNo", wild
                    }
                    break
                case "listingCode":
                    listings = Listing.search().list(){
                        wildcard "listingCode",wild
                    }
                    break
                case "status":
                    listings = Listing.search().list(){
                        wildcard "status", wild
                    }
                println("listing "+listings)
                    break
                default:
                    listings = Listing.search().list(){
                        wildcard "title",wild
                        wildcard "referenceNo", wild
                        wildcard "status", wild
                    }
                    break

            }

        }else {
            listings = listingService.getAllListings(userInstance, params)
            listingSize=listings.totalCount
        }
        render(view: 'index', model: [
                listings: listings,
                listingSize: listingSize
        ])
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'listing.label', default: 'Listing'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def formStep(Listing listing) {
        if (flash.message) {
            flash.message = flash.message
        }
        if (listing.isApproved == 'approved') {
            redirect(action: 'view', id: listing?.referenceNo)
            return
//        } else if (params?.returnurl == 'view') {
//            redirect(action: 'view', id: listing?.referenceNo)
//            return
        } else {
            switch (listing.stepNo) {
                case 'step-1':
                    redirect(action: 'create')
                    break
                case 'step-2':
                    redirect(action: 'description', id: listing?.referenceNo)
                    break
                case 'step-3':
                    redirect(action: 'set-rules', id: listing?.referenceNo)
                    break
                case 'step-4':
                    packageFormStep(listing)
                    break
                case 'step-4-index':
                    redirect(action: 'listing-package-list', id: listing?.referenceNo, namespace: 'admin')
                    break
                case 'step-5':
                    redirect(action: 'listing-pickup', id: listing?.referenceNo)
                    break
                case 'step-6':
                    redirect(action: 'listing-checklist', id: listing?.referenceNo)
                    break
                case 'step-7':
                    redirect(action: 'listing-multimedia', id: listing?.referenceNo)
                    break
                case 'step-8':
                    redirect(action: 'listing-policies', id: listing?.referenceNo)
                    break
                default:
                    redirect(action: 'create')
                    break
            }
        }
    }

    def packageFormStep(Listing listing) {
        if (flash.message) {
            flash.message = flash.message
        }
        switch (listing.stepPackageNo) {
            case 'step-1':
                redirect(action: 'listing-package-options', id: listing?.referenceNo, packageId: listing?.stepPackageId)
                break
            case 'step-2':
                redirect(action: 'listing-package-pricing', id: listing?.referenceNo, packageId: listing?.stepPackageId)
                break
            case 'step-3':
                redirect(action: 'listing-addons', id: listing?.referenceNo, packageId: listing?.stepPackageId)
                break
            case 'step-4':
                redirect(action: 'listing-group-discount', id: listing?.referenceNo, packageId: listing?.stepPackageId)
                break
            default:
                def listingPackages = ListingPackage.findByListing(listing)
                if (!listingPackages) {
                    redirect(action: 'listing-package', id: listing?.referenceNo)
                    return
                }
                redirect(action: 'listing-package-list', id: listing?.referenceNo)
                break
        }
    }

    def view(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        def listingRestrictions = ListingRestrictions.findByListing(listing)
        def listingAgeGroup = ListingAgeGroup.findByListing(listing)
        def listingPackage = ListingPackage.findAllByListingAndStatus(listing, 'publish', [sort: "tripTitle", order: "asc"])
        def listingPickup = ListingPickupPoint.findAllByListing(listing)
        def listingCheckListProvide = ListingChecklist.findAllByListingAndChecklistType(listing, 'provide')
        def listingCheckListBring = ListingChecklist.findAllByListingAndChecklistType(listing, 'bring')
        def businessDirectory = BusinessDirectory.get(listing?.businessDirectoryId)
        def listingPolicies = ListingPolicies.findByListing(listing)
        def listingCancellationPolicies = ListingCancellationPolicies.findAllByListing(listing)
        def insuranceDocument = ListingMultimedia.findByListingAndType(listing, '_insurance')
        def featureImage = ListingMultimedia.findByListingAndType(listing, '_feature')
        def galleryImages = ListingMultimedia.findAllByListingAndType(listing, '_gallery')
        def listingVideo = ListingMultimedia.findByListingAndType(listing, '_video')
        def listingRoute = ListingMultimedia.findByListingAndType(listing, '_routemap')

        render(view: 'show', model: [
                listing                    : listing,
                listingRestrictions        : listingRestrictions,
                listingAgeGroup            : listingAgeGroup,
                listingPackage             : listingPackage,
                listingPickup              : listingPickup,
                listingCheckListProvide    : listingCheckListProvide,
                listingCheckListBring      : listingCheckListBring,
                businessDirectory          : businessDirectory,
                listingPolicies            : listingPolicies,
                listingCancellationPolicies: listingCancellationPolicies,
                insuranceDocument          : insuranceDocument,
                featureImage               : featureImage,
                galleryImages              : galleryImages,
                listingVideo               : listingVideo,
                listingRoute               : listingRoute
        ])
    }

    def viewPackage() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listing = listingService.getListingByReferenceNo(params?.listing)

        def listingPackage = ListingPackage.findByReferenceNo(params?.package)
        def listingAvailability = ListingAvailability.findByListingAndListingPackage(listing, listingPackage)
        def listingAvailabilityDays = ListingAvailabilityDays.findByListingAndListingPackage(listing, listingPackage)
        def listingDurations = ListingDuration.findAllByListingAndListingPackage(listing, listingPackage)
        def listingSchedules = ListingSchedule.findAllByListingAndListingPackage(listing, listingPackage)
        def packagePrice = ListingPrice.findAllByListingAndListingPackageAndStatus(listing, listingPackage, 'publish')
        def listingAddons = ListingAddons.findByListingAndListingPackage(listing, listingPackage)
        def listingAddonMultimedia = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'multimedia')
        def listingAddonTransport = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'transport')
        def discountPolicies = ListingGroupDiscount.findAllByListingAndListingPackage(listing, listingPackage)

        render(view: 'package-view', model: [
                listing                : listing,
                listingPackage         : listingPackage,
                listingAvailability    : listingAvailability,
                listingAvailabilityDays: listingAvailabilityDays,
                listingDurations       : listingDurations,
                listingSchedules       : listingSchedules,
                packagePrice           : packagePrice,
                listingAddons          : listingAddons,
                listingAddonMultimedia : listingAddonMultimedia,
                listingAddonTransport  : listingAddonTransport,
                discountPolicies       : discountPolicies
        ])
    }

    def newListing() {
        def activeBusinessDirectories = BusinessDirectory.findAllByApproved('approved', [sort: 'businessName', order: 'ASC'])

        render(view: 'new_listing', model: [
                activeBusinessDirectories: activeBusinessDirectories
        ])
    }

    def create(Integer business) {

        if (!business) {
            flash.warning = 'Please select a business from list or add new business directory.'
            redirect(action: 'new-listing')
            return
        }


        def listing = new Listing(params)
        def category = Category.findAllWhere(status: 'publish') ?: new Category(params)
        def businessDirectory = BusinessDirectory.findByBusinessId(business)

        render(view: 'create', model: [
                listing          : listing,
                category         : category,
                businessDirectory: businessDirectory
        ])
    }

    @Transactional
    def save(Listing listing) {
        if (params?.business) {
            def userInstanace = userService.currentUser()
            listing.user = userInstanace
        }
        if (params?.listingReferenceNo)
            listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (listing.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond listing.errors, view: 'create'
            return
        }

        def category = Category.get(params.listingCategory.toInteger())
        InvokerHelper.setProperties(listing, params)
        if (params?.agent) {
            def businessDirectory = BusinessDirectory.get(params?.agent)
            listing.businessDirectory = businessDirectory
        }
        listing.nearestCity = params?.address
        listing.stepNo = 'step-2'
        //listing.isApproved = 'pending'
        listing.category = category

        (listing.save(flush: true))

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.basic')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
    }

    def edit(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        def category = Category.findAllWhere(status: 'publish') ?: new Category(params)

        render(view: 'edit', model: [
                listing : listing,
                category: category,
        ])
    }

    def description(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        render(view: 'description', model: [
                listing: listing
        ])
    }

    @Transactional
    def saveDescription(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (listing.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond listing.errors, view: 'description'
            return
        }

        InvokerHelper.setProperties(listing, params)
        listing.stepNo = "step-3"
        listing.save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.description')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    def setRules(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        def listingRestrictions = ListingRestrictions.findByListing(listing) ?: new ListingRestrictions()
        def listingAgeGroup = ListingAgeGroup.findByListing(listing) ?: new ListingAgeGroup()
        render(view: 'set_rules', model: [
                listing            : listing,
                listingRestrictions: listingRestrictions,
                listingAgeGroup    : listingAgeGroup
        ])
    }

    @Transactional
    def saveRules(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)

        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def listingRestrictions = ListingRestrictions.findByListing(listing) ?: new ListingRestrictions()
        InvokerHelper.setProperties(listingRestrictions, params)
        listingRestrictions.listing = listing

        if (!listingRestrictions.validate()) {
            transactionStatus.setRollbackOnly()
            respond listingRestrictions.errors, view: 'set_rules'
            return
        }

        def listingAgeGroup = ListingAgeGroup.findByListing(listing) ?: new ListingAgeGroup()
        InvokerHelper.setProperties(listingAgeGroup, params)
        listingAgeGroup.listing = listing
        if (!listingAgeGroup.validate()) {
            transactionStatus.setRollbackOnly()
            respond listingAgeGroup.errors, view: 'set_rules'
            return
        }

        listingRestrictions.save(flush: true)
        listingAgeGroup.save flush: true

        listing.stepNo = 'step-4'
        listing.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.rules')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    def listingPackage(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        def listingPackage = new ListingPackage()

        render(view: 'package-create', model: [
                listing       : listing,
                listingPackage: listingPackage
        ])
    }

    @Transactional
    def saveListingPackage(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def listingPackage
        if (params.listingPackageReferenceNo) {
            listingPackage = ListingPackage.findByReferenceNo(params.listingPackageReferenceNo)
        } else {
            listingPackage = new ListingPackage()
        }
        InvokerHelper.setProperties(listingPackage, params)
        listingPackage.listing = listing

        if (!listingPackage.validate()) {
            transactionStatus.setRollbackOnly()
            respond listingPackage.errors, view: 'package-create'
            return
        }

        listingPackage.save(flush: true)
        listing.stepNo = 'step-4'
        listing.stepPackageId = listingPackage.id.toInteger()
        listing.stepPackageNo = 'step-1'
        listing.save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.package')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    def listingPackageEdit(Listing listing) {
        listing = listingService.getListingByReferenceNo(params?.id)
        if (!listing) {
            notFound()
        }
        def listingPackage = ListingPackage.findByReferenceNo(params?.package) ?: new ListingPackage()

        render(view: 'package-create', model: [
                listing       : listing,
                listingPackage: listingPackage
        ])
    }

    @Transactional
    def listingPackageDelete(Listing listing) {
        listing = listingService.getListingByReferenceNo(params?.id)
        def listingPackage = ListingPackage.findByReferenceNo(params?.package)
        listingPackage.status = 'trash'
        if (listingPackage.save(flush: true))
            flash.succes = message(code: 'listing.package.delete.success')
        redirect(action: 'listing-package-list', id: listing?.referenceNo)
    }

    def ListingPackageOptions(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        def listingPackage = ListingPackage.get(listing?.stepPackageId)
        if (!listingPackage) {
            notFound()
        }
        def listingAvailability = ListingAvailability.findByListingAndListingPackage(listing, listingPackage) ?: new ListingAvailability()
        def listingAvailabilityDays = ListingAvailabilityDays.findByListingAndListingPackage(listing, listingPackage) ?: new ListingAvailabilityDays()
        def listingDurations = ListingDuration.findAllByListingAndListingPackage(listing, listingPackage)
        def listingSchedules = ListingSchedule.findAllByListingAndListingPackage(listing, listingPackage)

        render(view: 'package-option-create', model: [
                listing                : listing,
                listingPackage         : listingPackage,
                listingAvailability    : listingAvailability,
                listingAvailabilityDays: listingAvailabilityDays,
                listingDurations       : listingDurations,
                listingSchedules       : listingSchedules
        ])
    }

    @Transactional
    def saveListingPackageOptions(Listing listing) {
        //render params
        //return
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def listingPackage

        listingPackage = ListingPackage.findByReferenceNo(params.listingPackageReferenceNo)
        if (listingPackage == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        ListingAvailabilityDays.where { listingPackage == listingPackage }.deleteAll()
        def listingAvailabilityDays = new ListingAvailabilityDays()
        InvokerHelper.setProperties(listingAvailabilityDays, params)
        listingAvailabilityDays.listing = listing
        listingAvailabilityDays.listingPackage = listingPackage

        if (!listingAvailabilityDays.validate() || !listingAvailabilityDays.save()) {
            if (listingAvailabilityDays.hasErrors()) {
                listingAvailabilityDays.errors.allErrors.each {
                    println it
                }
            }
            transactionStatus.setRollbackOnly()
            respond listingAvailabilityDays.errors, view: 'package-option-create'
            return
        }
        ListingAvailability.where { listingPackage == listingPackage }.deleteAll()
        def listingAvailability = new ListingAvailability()
        InvokerHelper.setProperties(listingAvailabilityDays, params)
        listingAvailability.listing = listing
        listingAvailability.listingPackage = listingPackage
        listingAvailability.availability = 'available'
        listingAvailability.availabilityType = params?.availabilityType
        if (params.availabilityType == 'date-single') {
            listingAvailability.fromDate = Date.parse('yyyy-MM-dd', params.singleFrom)
        } else {
            listingAvailability.fromDate = Date.parse('yyyy-MM-dd', params.fromDate)
            listingAvailability.toDate = Date.parse('yyyy-MM-dd', params.toDate)
        }
        if (!listingAvailability.validate() || !listingAvailability.save()) {
            if (listingAvailability.hasErrors()) {
                listingAvailability.errors.allErrors.each {
                    println it
                }
            }
            transactionStatus.setRollbackOnly()
            respond listingAvailability.errors, view: 'package-option-create'
            return
        }

        ListingDuration.where { listingPackage == listingPackage }.deleteAll()
        def listingDurations = params.list('duration')
//        def listingDurationUnits = params.list('durationUnit')
        if (listingDurations.size() > 1) {
            listingDurations.eachWithIndex { value, index ->
                if (value != '') {
                    new ListingDuration(
                            listing: listing,
                            listingPackage: listingPackage,
                            duration: value,
                            // durationUnit: listingDurationUnits[index]
                    ).save(flush: true)
                }
            }
        } else {
            new ListingDuration(
                    listing: listing,
                    listingPackage: listingPackage,
                    duration: params.duration,
                    //durationUnit: params.durationUnit
            ).save(failOnError: true)
        }

        ListingSchedule.where { listingPackage == listingPackage }.deleteAll()
        def listingSchedules = params.list('schedule')

        if (listingSchedules.size() > 1) {
            listingSchedules.eachWithIndex { value, index ->
                if (value != '') {
                    new ListingSchedule(
                            listing: listing,
                            listingPackage: listingPackage,
                            schedule: value.toString()
                    ).save(flush: true)
                }
            }
        } else {
            new ListingSchedule(
                    listing: listing,
                    listingPackage: listingPackage,
                    schedule: params.schedule.toString()
            ).save(failOnError: true)
        }
        listing.stepNo = 'step-4'
        listing.stepPackageId = listingPackage.id.toInteger()
        listing.stepPackageNo = 'step-2'
        listing.save(flush: true)
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageOption')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeDuration() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingDuration = ListingDuration.get(params.id)
        if (listingDuration) {
            listingDuration.delete(flush: true)
            render 'success'
        }
        return
    }

    @Transactional
    def removeSchedule() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingSchedule = ListingSchedule.get(params.id)
        if (listingSchedule) {
            listingSchedule.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def listingPackagePricing(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        def listingPackage = ListingPackage.get(listing?.stepPackageId)
        if (!listingPackage) {
            notFound()
        }


        def listingSchedules = ListingSchedule.findAllByListingAndListingPackage(listing, listingPackage)
        def listingDurations = ListingDuration.findAllByListingAndListingPackage(listing, listingPackage)
        def listingPrices = ListingPrice.findAllByListingAndListingPackageAndStatus(listing, listingPackage, 'publish')
        render(view: 'package-pricing', model: [
                listing         : listing,
                listingPackage  : listingPackage,

                listingSchedules: listingSchedules,
                listingDurations: listingDurations,
                listingPrices   : listingPrices
        ])
    }

    @Transactional
    def saveListingPackagePricing(Listing listing) {

        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def listingPackage
        listingPackage = ListingPackage.findByReferenceNo(params.listingPackageReferenceNo)
        if (listingPackage == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }



        def ageGroup = params.list('ageGroup')
        def listingPrice
        def listingPrices = ListingPrice.findAllByListingPackage(listingPackage)
        if (listingPrices) {
            listingPrices.each { price ->
                def bookingItem = BookingItems.findByListingPrice(price)
                if (!bookingItem) {
                    price.delete(flush: true)
                } else {
                    price.status = 'trash'
                    price.save(flush: true)
                }
            }
        }
        if (ageGroup.size() > 1) {
            ageGroup.eachWithIndex { value, index ->
                if (value != '') {
                    listingPrice = new ListingPrice(
                            listing: listing,
                            listingPackage: listingPackage,
                            ageGroup: value,
                            wholeSaleRate: params.wholeSaleRate[index],
                            retailRate: params.retailRate[index]
                    )
                    if (params.duration) {
                        listingPrice.duration = params.duration[index]

                    }
                    if (params.type) {
                        listingPrice.type = params.type[index]
                    }
                    if (params.rateType == 'yes') {
                        listingPrice.pricingType = 'scheduled'
                        listingPrice.schedule = params.schedule[index]
                    }
                    if (params.wholeSaleRate[index] && params.retailRate[index] && listingPrice.validate())
                        listingPrice.save(flush: true)
                    else {
                        transactionStatus.setRollbackOnly()
                        respond listingPrice.errors, view: 'package-pricing'
                        return
                    }
                }
            }
        } else {
            listingPrice = new ListingPrice(
                    listing: listing,
                    listingPackage: listingPackage,
                    ageGroup: params.ageGroup,
                    wholeSaleRate: params.wholeSaleRate,
                    retailRate: params.retailRate
            )
            if (listing?.category?.slug == 'paragliding') {
                listingPrice.duration = params.duration
            }
            if (params.type) {
                listingPrice.type = params.type
            }
            if (params.rateType == 'yes') {
                listingPrice.pricingType = 'scheduled'
                listingPrice.schedule = params.schedule
            }
            if (params.wholeSaleRate && params.retailRate && listingPrice.validate())
                listingPrice.save(flush: true)

        }

        listingPackage.rateType = params.rateType
        listingPackage.save(flush: true)

        listing.stepNo = 'step-4'
        listing.stepPackageId = listingPackage.id.toInteger()
        listing.stepPackageNo = 'step-3'
        listing.save(flush: true)
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packagePricing')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removePrice() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingPrice = ListingPrice.get(params.id)
        if (listingPrice) {
            listingPrice.delete(flush: true)
            render 'success'
        }
        return
    }

    def listingAddons(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        def listingPackage = ListingPackage.get(listing?.stepPackageId)
        if (!listingPackage) {
            notFound()
        }

        def listingAddons = ListingAddons.findByListingAndListingPackage(listing, listingPackage)
        def listingAddonPrice = new ListingAddonsPrice()
        def listingAddonMultimedia = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'multimedia')
        def listingAddonTransport = ListingAddonsPrice.findAllByListingAndListingPackageAndAddonType(listing, listingPackage, 'transport')
        render(view: 'package-addons', model: [
                listing               : listing,
                listingPackage        : listingPackage,
                listingAddons         : listingAddons,
                listingAddonPrice     : listingAddonPrice,
                listingAddonMultimedia: listingAddonMultimedia,
                listingAddonTransport : listingAddonTransport
        ])
    }

    @Transactional
    def saveListingAddons(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def listingPackage
        listingPackage = ListingPackage.findByReferenceNo(params.listingPackageReferenceNo)
        if (listingPackage == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }


        ListingAddons.where { listingPackage == listingPackage }.deleteAll()
        def listingAddons = new ListingAddons(params)
        listingAddons.listing = listing
        listingAddons.listingPackage = listingPackage
        listingAddons.save(flush: true)

        ListingAddonsPrice.where { listingPackage == listingPackage }.deleteAll()
        def listingAddonMultimedia
        if (params.includedMultimedia == 'no') {
            def multimediaAgeGroup = params.list('multimediaAgeGroup')
            if (multimediaAgeGroup.size() > 1) {
                multimediaAgeGroup.eachWithIndex { value, index ->
                    if (value != '') {
                        listingAddonMultimedia = new ListingAddonsPrice()
                        listingAddonMultimedia.listing = listing
                        listingAddonMultimedia.listingPackage = listingPackage
                        listingAddonMultimedia.addonType = 'multimedia'
                        listingAddonMultimedia.ageGroup = params.multimediaAgeGroup[index]
                        listingAddonMultimedia.wholesaleRate = params.multimediaWholesaleRate[index]
                        listingAddonMultimedia.retailRate = params.multimediaRetailRate[index]
                        listingAddonMultimedia.save(flush: true)
                    }
                }
            } else {
                listingAddonMultimedia = new ListingAddonsPrice()
                listingAddonMultimedia.listing = listing
                listingAddonMultimedia.listingPackage = listingPackage
                listingAddonMultimedia.addonType = 'multimedia'
                listingAddonMultimedia.ageGroup = params.multimediaAgeGroup
                listingAddonMultimedia.wholesaleRate = params.multimediaWholesaleRate
                listingAddonMultimedia.retailRate = params.multimediaRetailRate
                listingAddonMultimedia.save(flush: true)
            }
        }

        def listingAddonTransport
        if (params.includedTransport == 'no') {
            def transportAgeGroup = params.list('transportAgeGroup')
            if (transportAgeGroup.size() > 1) {
                transportAgeGroup.eachWithIndex { value, index ->
                    if (value != '') {
                        listingAddonTransport = new ListingAddonsPrice()
                        listingAddonTransport.listing = listing
                        listingAddonTransport.listingPackage = listingPackage
                        listingAddonTransport.addonType = 'transport'
                        listingAddonTransport.ageGroup = params.transportAgeGroup[index]
                        listingAddonTransport.wholesaleRate = params.transportWholesaleRate[index]
                        listingAddonTransport.retailRate = params.transportRetailRate[index]
                        listingAddonTransport.save(flush: true)
                    }
                }
            } else {
                listingAddonTransport = new ListingAddonsPrice()
                listingAddonTransport.listing = listing
                listingAddonTransport.listingPackage = listingPackage
                listingAddonTransport.addonType = 'transport'
                listingAddonTransport.ageGroup = params.transportAgeGroup
                listingAddonTransport.wholesaleRate = params.transportWholesaleRate
                listingAddonTransport.retailRate = params.transportRetailRate
                listingAddonTransport.save(flush: true)
            }
        }

        if (params?.hasGroupDiscountPolicy == 'yes') {
            listing.stepPackageId = listingPackage.id.toInteger()
            listing.stepPackageNo = 'step-4'
        } else {
            listing.stepNo = 'step-4-index'
            listing.stepPackageId = listingPackage.id.toInteger()
            listing.stepPackageNo = ''
        }

        listing.save(flush: true)
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageAddon')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeListingAddons() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingAddons = ListingAddonsPrice.get(params.id)
        if (listingAddons) {
            listingAddons.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def listingGroupDiscount(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
            return
        }
        def listingPackage = ListingPackage.get(listing?.stepPackageId)
        if (!listingPackage) {
            notFound()
            return
        }

        def listingGroupDiscount = new ListingGroupDiscount()
        def groupDiscounts = ListingGroupDiscount.findAllByListingAndListingPackage(listing, listingPackage)

        render(view: 'package-group-discount', model: [
                listing             : listing,
                listingPackage      : listingPackage,
                listingGroupDiscount: listingGroupDiscount,
                groupDiscounts      : groupDiscounts
        ])
    }

    @Transactional
    def saveListingGroupDiscount(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def listingPackage
        listingPackage = ListingPackage.findByReferenceNo(params.listingPackageReferenceNo)
        if (listingPackage == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        ListingGroupDiscount.where { listingPackage == listingPackage }.deleteAll()

        def groupDiscount
        def listingGroupDiscount = params.list('discountPercent')
        if (listingGroupDiscount.size() > 1) {
            listingGroupDiscount.eachWithIndex { value, index ->
                if (value != '') {
                    groupDiscount = new ListingGroupDiscount()
                    groupDiscount.listing = listing
                    groupDiscount.listingPackage = listingPackage
                    groupDiscount.groupPolicy = params.groupPolicy[index]
                    groupDiscount.discountFrom = params.discountFrom[index]
                    groupDiscount.discountTo = params.discountTo[index]
                    groupDiscount.discountPercent = value
                    groupDiscount.save(flush: true)
                }
            }
        } else {
            groupDiscount = new ListingGroupDiscount()
            groupDiscount.listing = listing
            groupDiscount.listingPackage = listingPackage
            groupDiscount.groupPolicy = params.groupPolicy
            groupDiscount.discountFrom = params.discountFrom
            groupDiscount.discountTo = params.discountTo
            groupDiscount.discountPercent = params.discountPercent
            groupDiscount.save(flush: true)
        }

        listing.stepNo = 'step-4-index'
        listing.stepPackageId = listingPackage.id.toInteger()
        listing.stepPackageNo = ''
        listing.save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageGroupDiscount')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeGroupDiscount() {
        if (!request.xhr) {
            notFound()
            return
        }
        def groupDiscount = ListingGroupDiscount.get(params.id)
        if (groupDiscount) {
            groupDiscount.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def listingPackageList(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        def listingPackages = ListingPackage.findAllByListingAndStatus(listing, 'publish')

        render(view: 'package-list', model: [
                listing        : listing,
                listingPackages: listingPackages
        ])
    }

    @Transactional
    def listingAddPickup(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        listing.stepNo = 'step-5'
        listing.save(flush: true)
        redirect(action: 'form-step', id: listing?.id, params: [returnurl: params?.returnurl])
    }

    def listingPickup(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }

        def listingPickup = new ListingPickupPoint()
        def listingPickups = ListingPickupPoint.findAllByListing(listing)
        render(view: 'listing-pickup', model: [
                listing       : listing,
                listingPickup : listingPickup,
                listingPickups: listingPickups
        ])
    }

    @Transactional
    def saveListingPickup(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        def listingPickup
        ListingPickupPoint.where { listing == listing }.deleteAll()
        def address = params.list('address')
        if (address.size() > 1) {
            address.eachWithIndex { value, index ->
                if (value != '') {
                    listingPickup = new ListingPickupPoint()
                    listingPickup.listing = listing
                    listingPickup.address = value
                    listingPickup.save(flush: true)
                }
            }
        } else {
            listingPickup = new ListingPickupPoint()
            listingPickup.listing = listing
            listingPickup.address = params.address
            listingPickup.save(flush: true)
        }
        listing.stepNo = 'step-6'
        listing.save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageGroupDiscount')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeListingPickup() {
        if (!request.xhr) {
            notFound()
            return
        }
        def listingPickup = ListingPickupPoint.get(params.id)
        if (listingPickup) {
            listingPickup.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def listingChecklist(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }

        def listingChecklist = new ListingChecklist()
        def listingChecklistProvide = ListingChecklist.findAllByListingAndChecklistType(listing, 'provide')
        def listingChecklistBring = ListingChecklist.findAllByListingAndChecklistType(listing, 'bring')

        render(view: 'listing-checklist', model: [
                listing                : listing,
                listingChecklist       : listingChecklist,
                listingChecklistProvide: listingChecklistProvide,
                listingChecklistBring  : listingChecklistBring
        ])
    }

    @Transactional
    def saveListingChecklist(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        ListingChecklist.where { listing == listing }.deleteAll()
        def listingChecklistProvide
        def provideItems = params.list('provideItem')
        if (provideItems.size() > 1) {
            provideItems.eachWithIndex { value, index ->
                if (value != '') {
                    listingChecklistProvide = new ListingChecklist()
                    listingChecklistProvide.listing = listing
                    listingChecklistProvide.checklistType = 'provide'
                    listingChecklistProvide.checklistItem = value
                    listingChecklistProvide.save(flush: true)
                }
            }
        } else {
            listingChecklistProvide = new ListingChecklist()
            listingChecklistProvide.listing = listing
            listingChecklistProvide.checklistType = 'provide'
            listingChecklistProvide.checklistItem = params.provideItem
            listingChecklistProvide.save(flush: true)
        }
        def listingChecklistBring
        def bringItems = params.list('bringItem')
        if (bringItems.size() > 1) {
            bringItems.eachWithIndex { value, index ->
                if (value != '') {
                    listingChecklistBring = new ListingChecklist()
                    listingChecklistBring.listing = listing
                    listingChecklistBring.checklistType = 'bring'
                    listingChecklistBring.checklistItem = value
                    listingChecklistBring.save(flush: true)
                }
            }
        } else {
            listingChecklistBring = new ListingChecklist()
            listingChecklistBring.listing = listing
            listingChecklistBring.checklistType = 'bring'
            listingChecklistBring.checklistItem = params.bringItem
            listingChecklistBring.save(flush: true)
        }

        listing.stepNo = 'step-7'
        listing.save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageGroupDiscount')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeListingChecklist() {
        if (!request.xhr) {
            notFound()
            return
        }
        def checklist = ListingChecklist.get(params.id)
        if (checklist) {
            checklist.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def listingMultimedia(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }

        def listingMultimedia = new ListingMultimedia()
        def featureImage = ListingMultimedia.findByListingAndType(listing, '_feature')
        def galleryImages = ListingMultimedia.findAllByListingAndType(listing, '_gallery')
        def listingVideo = ListingMultimedia.findByListingAndType(listing, '_video')
        def listingRoute = ListingMultimedia.findByListingAndType(listing, '_routemap')

        render(view: 'listing-multimedia', model: [
                listing          : listing,
                listingMultimedia: listingMultimedia,
                featureImage     : featureImage,
                galleryImages    : galleryImages,
                listingVideo     : listingVideo,
                listingRoute     : listingRoute
        ])
    }

    @Transactional
    def saveListingMultimedia(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        def video = ListingMultimedia.findByListingAndType(listing, '_video') ?: new ListingMultimedia()
        video.listing = listing
        video.file = params.file
        video.type = '_video'
        video.save(flush: true)

        listing.stepNo = 'step-8'
        listing.save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageGroupDiscount')
                redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def multimediaUploadSuccess() {
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        def listing = Listing.get(params.id)
        def imageType = params?.imageType
        if (params?.type == '_feature') {
            def feature = ListingMultimedia.findByListingAndType(listing, '_feature')
            if (feature) {
                amazonS3Service.s3Delete(amazonS3Service.sourceBucket(), feature?.file)
                def resizeImageUrl = imageProcessingService.checkImageType(params?.key, imageType)
                imageProcessingService.deleteResizeImageFromS3DestinationBucket(resizeImageUrl)
                feature.delete(flush: true)
            }
        }
        new ListingMultimedia(
                listing: listing,
                file: params.key,
                type: params.type
        ).save(flush: true)
        render "success"
    }

    @Transactional
    def multimediaDelete() {
        def imageType = params?.imageType
        amazonS3Service.s3Delete(params.bucket, params.key)
        def media = ListingMultimedia.findWhere(file: params.key)
        def resizeImageUrl = imageProcessingService.checkImageType(params?.key, imageType)
        if (media) {
            media.delete(flush: true)
            imageProcessingService.deleteResizeImageFromS3DestinationBucket(resizeImageUrl)
        }
        def result = [status: "success"]
        render result as JSON
    }

    def listingPolicies(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }

        def listingPolicies = ListingPolicies.findByListing(listing) ?: new ListingPolicies()
        def listingInsurance = ListingMultimedia.findAllByListingAndType(listing, '_insurance')
        def cancellationPolicies = ListingCancellationPolicies.findAllByListing(listing) ?: new ListingCancellationPolicies()

        render(view: 'listing-policy', model: [
                listing             : listing,
                listingPolicies     : listingPolicies,
                cancellationPolicies: cancellationPolicies,
                listingInsurance    : listingInsurance
        ])
    }

    @Transactional
    def saveListingPolicies(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        ListingPolicies.where { listing == listing }.deleteAll()

        def listingPolicies = new ListingPolicies(params)
        listingPolicies.listing = listing
        listingPolicies.save(flush: true)

        if (listingPolicies.cancellationPolicyType == 'custom') {
            def cancellationPolicy
            ListingCancellationPolicies.where { listing == listing }.deleteAll()
            def policyTitle = params.list('policyTitle')
            if (policyTitle.size() > 1) {
                policyTitle.eachWithIndex { value, index ->
                    cancellationPolicy = new ListingCancellationPolicies()
                    cancellationPolicy.listing = listing
                    cancellationPolicy.policyTitle = params.policyTitle[index]
                    cancellationPolicy.duration = params.duration[index]
                    cancellationPolicy.durationUnit = params.durationUnit[index]
                    cancellationPolicy.refund = params.refund[index]
                    cancellationPolicy.refundUnit = params.refundUnit[index]
                    cancellationPolicy.description = params.description[index]
                    if (cancellationPolicy.validate()) {
                        cancellationPolicy.save()
                    }
                }
            } else {
                cancellationPolicy = new ListingCancellationPolicies()
                cancellationPolicy.listing = listing
                cancellationPolicy.policyTitle = params.policyTitle
                cancellationPolicy.duration = params.duration
                cancellationPolicy.durationUnit = params.durationUnit
                cancellationPolicy.refund = params.refund
                cancellationPolicy.refundUnit = params.refundUnit
                cancellationPolicy.description = params.description
                if (cancellationPolicy.validate()) {
                    cancellationPolicy.save()
                }
            }

        }

        listing.stepNo = ''
        listing.save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.packageGroupDiscount')
                if (params?.returnurl) {
                    redirect(action: 'form-step', id: listing.id, params: [returnurl: params?.returnurl])
                } else {
                    redirect(action: 'listing-completed', id: listing?.referenceNo)
                }
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def removeCancellationPolicy() {
        if (!request.xhr) {
            notFound()
            return
        }
        def cancellationPolicy = ListingCancellationPolicies.get(params.id)
        if (cancellationPolicy) {
            cancellationPolicy.delete(flush: true)
            render 'success'
        } else {
            render 'error'
        }
        return
    }

    def listingCompleted(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (!listing) {
            notFound()
        }
        render(view: 'listing-completed', model: [
                listing: listing
        ])
    }

    @Transactional
    def saveListingCompleted(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.listingReferenceNo)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        listing.isApproved = 'approved'
        listing.status = 'publish'
        listing.save(flush: true)
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'listing.success.submit')
                redirect(action: 'index')
            }
            '*' { respond listing, [status: CREATED] }
        }
        return
    }

    @Transactional
    def delete(Listing listing) {
        listing = listingService.getListingByReferenceNo(params.id)
        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        listing.status = 'trash'
        listing.trashedAt = new Date()
        listing.save(failOnError: true)

        flash.message = message(code: 'listing.success.trash')
        redirect(action: 'index')
        return
    }

    def recentlyViewed(Integer max) {
        params.max = Math.min(params.max?.toInteger() ?: 10, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0

        def userInstance = userService.currentUser()
        def active
        def listingsActive
        def recent
        def recent_viewed_owned_listings
        def agentOrAdmin = false

        if (grails.plugin.springsecurity.SpringSecurityUtils.ifAllGranted("ROLE_ADMIN") || grails.plugin.springsecurity.SpringSecurityUtils.ifAllGranted("ROLE_AGENT")) {
            active = Listing.createCriteria()
            listingsActive = active.list {
                ne('status', 'trash')
                eq('status', 'publish')
                if (grails.plugin.springsecurity.SpringSecurityUtils.ifAllGranted("ROLE_AGENT")) {
                    eq('user', userInstance)
                }
            }

            if (listingsActive) {
                recent = ListingRecentlyViewed.createCriteria()
                recent_viewed_owned_listings = recent.listDistinct() {
                    order("modifiedAt", "desc")
                    maxResults(params.max)
                    firstResult(params.offset)
                    if (!grails.plugin.springsecurity.SpringSecurityUtils.ifAllGranted("ROLE_ADMIN")) {
                        'in'("listing", listingsActive)
                    }
                    projections {
                        distinct("listing")
                    }
                }
            }
            agentOrAdmin = true
        }

        active = Listing.createCriteria()
        listingsActive = active.list {
            ne('status', 'trash')
            eq('status', 'publish')
        }

        recent = ListingRecentlyViewed.createCriteria()
        def recent_viewed_listings = recent.listDistinct() {
            maxResults(params.max)
            firstResult(params.offset)
            order("modifiedAt", "desc")
            'in'("listing", listingsActive)
            eq('user', userInstance)
            projections {
                distinct("listing")
            }
        }
        def listingView
        def viewCount
        def viewedCount

        switch (params.state) {
            case 'viewed':
                listingView = recent_viewed_owned_listings
                viewCount = recent_viewed_listings ? recent_viewed_listings.size() : 0
                viewedCount = recent_viewed_owned_listings ? recent_viewed_owned_listings.size() : 0
                break
            default:
                listingView = recent_viewed_listings
                viewCount = recent_viewed_listings ? recent_viewed_listings.size() : 0
                viewedCount = recent_viewed_owned_listings ? recent_viewed_owned_listings.size() : 0
                break
        }

        render(view: 'recently_viewed', model: [
                listingView                 : listingView,
                viewCount                   : viewCount,
                viewedCount                 : viewedCount,
                recent_viewed_listings      : recent_viewed_listings,
                recent_viewed_owned_listings: recent_viewed_owned_listings,
                agentOrAdmin                : agentOrAdmin,
                listingsActive              : listingsActive
        ])
    }

    @Transactional
    def updateStatus() {
        if (!request.xhr) {
            notFound()
            return
        }
        def msgStatus
        def listing = listingService.getListingByReferenceNo(params?.id)
        if (listing) {
            listing.isApproved = 'approved'
            listing.status = params?.value
            listing.save flush: true
            msgStatus = 'success'
            //flash.message = message(code: 'listing.success.statusUpdate')
        }
        return msgStatus
    }

    def seo() {
        def listing = listingService.getListingByReferenceNo(params?.id)
        if (!listing) {
            notFound()
        }
        render(view: 'listingSeo', model: [
                listing: listing
        ])
        return
    }

    def updateListingSEO() {
        def listing = Listing.get(params?.listingId)

        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (listing.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond listing.errors, view: 'listingSeo'
            return
        }

        listing.metaTitle = params?.metaTitle
        listing.metaKeyword = params?.metaKeyword
        listing.metaDescription = params?.metaDescription
        listing.save(failOnError: true, flush: true)
        redirect(action: 'view', params: [id: listing?.referenceNo])
    }

    def manageSettings() {
        def listing = listingService.getListingByReferenceNo(params?.id)
        def businessDirectory = BusinessDirectory.get(listing?.businessDirectoryId)

        render(view: 'editListingSetting', model: [listing          : listing,
                                                   businessDirectory: businessDirectory])
        return
    }

    def updateListingSetting() {
        def listing = Listing.get(params?.listingId)

        if (listing == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (listing.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond listing.errors, view: 'editListingSetting'
            return
        }

        listing.featuredStart = dateConverter(params?.featuredStart)
        listing.featuredEnd = dateConverter(params?.featuredEnd)
        listing.isFeatured = (params?.isFeatured).toBoolean()
        listing.slug = params?.slug
        listing.transferred = (params?.transferred).toBoolean()
        listing.save(failOnError: true, flush: true)
        redirect(action: 'view', params: [id: listing?.referenceNo])

    }

    public Date dateConverter(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
        java.util.Date utilDate = simpleDateFormat.parse(date)
        Date sqlDate = new Date(utilDate.getTime())
        return sqlDate
    }
}