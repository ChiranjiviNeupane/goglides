package com.dgmates.goglides.admin

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import com.dgmates.goglides.SiteSettings

class SiteSettingsController {

    static namespace = 'admin'
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        def siteSettings = SiteSettings.list(params)
        render(view: 'index', model: [siteSettingsList: siteSettings, siteSettingsCount: SiteSettings.count()])
    }

    def show(SiteSettings siteSettings) {
        render(view: 'show', model: [siteSettings: siteSettings])
    }

    def create() {
        render(view: 'create', model: [siteSettings: new SiteSettings(params)])
    }

    @Transactional
    def save(SiteSettings siteSettings) {
        if (siteSettings == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (siteSettings.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond siteSettings.errors, view: 'create'
            return
        }

        siteSettings.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'siteSettings.label', default: 'SiteSettings'), siteSettings.id])
                redirect siteSettings
            }
            '*' { respond siteSettings, [status: CREATED] }
        }
    }

    def edit(SiteSettings siteSettings) {
        render(view: 'edit', model: [siteSettings: siteSettings])
    }

    @Transactional
    def update(SiteSettings siteSettings) {
        if (siteSettings == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (siteSettings.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond siteSettings.errors, view: 'edit'
            return
        }

        siteSettings.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'siteSettings.label', default: 'SiteSettings'), siteSettings.id])
                redirect siteSettings
            }
            '*' { respond siteSettings, [status: OK] }
        }
    }

    @Transactional
    def delete(SiteSettings siteSettings) {

        if (siteSettings == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        siteSettings.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'siteSettings.label', default: 'SiteSettings'), siteSettings.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'siteSettings.label', default: 'SiteSettings'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
