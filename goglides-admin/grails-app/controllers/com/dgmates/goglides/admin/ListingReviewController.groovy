package com.dgmates.goglides

import grails.transaction.Transactional

class ListingReviewController {
    static namespace = 'admin'

    static allowedMethods = [save: "POST", update: "PUT"]

    def index(Integer max) {
        params.max = Math.min(max ?: 30, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def review = ListingReview.createCriteria()
        def reviews = review.list(max: params.max, offset: params.offset) {
            if (params?.state) {
                eq('status', params?.state)
            } else {
                ne('status', 'trash')
            }
            order("id", "DESC")
        }

        render(view: 'index', model: [
                reviews: reviews
        ])
    }

    def view(ListingReview listingReview) {
        render(view: 'view', model: [
                review: listingReview
        ])
    }

    def delete(ListingReview listingReview) {
        listingReview.status = "trash"
        listingReview.save(flush: true)

        redirect(action: 'index')
    }

    @Transactional
    def publish(ListingReview listingReview) {
        listingReview.status = 'publish'
        if (!listingReview.save(flash: true)) {
            listingReview.errors.allErrors.each {
                println it
            }
        }
        redirect(action: 'view', id: listingReview?.id)
    }

}