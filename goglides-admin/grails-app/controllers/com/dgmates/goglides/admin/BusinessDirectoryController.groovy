package com.dgmates.goglides.admin

import com.dgmates.goglides.AmazonS3Service
import com.dgmates.goglides.DirectoryService
import com.dgmates.goglides.EmailMessagingService
import com.dgmates.goglides.ReportBusinessDirectoryService
import com.dgmates.goglides.UserService
import grails.transaction.Transactional
import grails.converters.JSON
import static org.springframework.http.HttpStatus.*
import com.dgmates.goglides.BusinessDirectory
import com.dgmates.goglides.BusinessDirectoryMultimedia
import com.dgmates.goglides.BusinessDirectoryTransferLog
import com.dgmates.goglides.User

class BusinessDirectoryController {
    static namespace = 'admin'

    AmazonS3Service amazonS3Service
    EmailMessagingService emailMessagingService
    def groovyPageRenderer
    DirectoryService directoryService
    ReportBusinessDirectoryService reportBusinessDirectoryService
    UserService userService

    def index(Integer max) {
        params.max = Math.min(params.max?.toInteger() ?: 30, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        def directory
        def agentsCount=0
        def searchData=params?.search
        def searchAttribute=params?.searchAttribute
        if(searchData){
            def wild = '*' + searchData.toLowerCase().trim() + '*'
            directory=BusinessDirectory.search().list(){
                wildcard searchAttribute, wild
            }

        }else {
            switch (params?.state) {
                case 'draft':
                    directory = directoryService.getAgentsByParams(params, 'draft')
                    break
                case 'publish':
                    directory = directoryService.getAgentsByParams(params, 'publish')
                    break
                case 'trash':
                    directory = directoryService.getAgentsByParams(params, 'trash')
                    break
                default:
                    directory = directoryService.getAgentsByParams(params)
                    break
            }
            agentsCount=directory.totalCount
        }
        render(view: 'index', model: [directory: directory,
            agentsCount:agentsCount])
    }

    def view(BusinessDirectory businessDirectory) {
        businessDirectory = BusinessDirectory.findByBusinessId(params?.id)
        def logo = BusinessDirectoryMultimedia.findByBusinessDirectoryAndMediaType(businessDirectory, '_logo') ?: new BusinessDirectoryMultimedia()
        def comCert = BusinessDirectoryMultimedia.findAllByBusinessDirectoryAndMediaType(businessDirectory, '_compCert') ?: new BusinessDirectoryMultimedia()
        def taxCert = BusinessDirectoryMultimedia.findAllByBusinessDirectoryAndMediaType(businessDirectory, '_taxCert') ?: new BusinessDirectoryMultimedia()
        render(view: 'view', model: [
                businessDirectory: businessDirectory,
                logo             : logo,
                comCert          : comCert,
                taxCert          : taxCert,

        ])
    }

    def show(BusinessDirectory businessDirectory) {
        render(view: 'view', model: [businessDirectory: businessDirectory])
    }
    def publish(params){
        def businessDir= BusinessDirectory.findByBusinessId(params.id)
        businessDir.status="publish"
        render(businessDir.status)
        businessDir.save(flush: true)
    }
    def changeSetting(){
        println(params)
        def taxDoc = params.taxDoc;
        def regDoc = params.regDoc;
        println("taxDoc:"+taxDoc+""+regDoc )
//        return
        def businessDir= BusinessDirectory.findByBusinessId(params.businessId)
        businessDir.taxDocumentsVerification = params.taxDoc
        businessDir.registeredDocumentsVerification = params.regDoc
        businessDir.status = params.status
        businessDir.approved = params.approved
        businessDir.save(flush:true)
        println(businessDir.taxDocumentsVerification)
        render businessDir as JSON
        return
    }
    def create() {
        def businessDirectory = new BusinessDirectory(params)
        def user = new User(params)
        render(view: 'create', model: [
                businessDirectory: businessDirectory,
                user             : user
        ])
    }

    @Transactional
    def save(BusinessDirectory businessDirectory) {
        if (businessDirectory == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (businessDirectory.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render(view: 'create', model: [
                    businessDirectory: businessDirectory
            ])
            return
        }

        def user = [:]
        user['firstname'] = params?.firstname
        user['lastname'] = params?.lastname
        user['email'] = params?.email
        user['username'] = params?.email
        def userInstance = userService.create(user, "ROLE_SUPPLIER")
        businessDirectory = directoryService.saveBusinessDirectory(userInstance, params)
        directoryService.saveBusinessStaff(userInstance, businessDirectory)

        request.withFormat {
            form multipartForm {
                flash.message = "Business directory information saved."
                redirect(action: 'address', id: businessDirectory?.businessId)
            }
            '*' { respond businessDirectory, [status: CREATED] }
        }

    }

    def edit(BusinessDirectory businessDirectory) {
        businessDirectory = BusinessDirectory.findByBusinessId(params?.id)
        render(view: 'edit', model: [
                businessDirectory: businessDirectory,
        ])
    }

    @Transactional
    def update(BusinessDirectory businessDirectory) {

        if (businessDirectory == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (businessDirectory.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render(view: 'edit', model: [
                    businessDirectory: businessDirectory
            ])
            return
        }

        businessDirectory.save flush: true
        //TODO: update keycloak user information.

        request.withFormat {
            form multipartForm {
                flash.message = "Business directory information saved."
                redirect(action: 'address', id: businessDirectory?.businessId)
            }
            '*' { respond businessDirectory, [status: OK] }
        }
    }

    def address(BusinessDirectory businessDirectory) {
        businessDirectory = BusinessDirectory.findByBusinessId(params?.id)
        render(view: 'address', model: [
                businessDirectory: businessDirectory])
    }

    @Transactional
    def saveaddress(BusinessDirectory businessDirectory) {
        if (businessDirectory == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (businessDirectory.hasErrors()) {
            transactionStatus.setRollbackOnly()

            render(view: 'address', model: [
                    businessDirectory: businessDirectory,
            ])

            return
        }

        businessDirectory.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = "Business directory information saved."
                redirect(action: 'viewmap', id: businessDirectory?.businessId)
            }
            '*' { respond businessDirectory, [status: OK] }
        }
    }

    def viewmap(BusinessDirectory businessDirectory) {
        businessDirectory = BusinessDirectory.findByBusinessId(params?.id)
        render(view: 'view-map', model: [
                businessDirectory: businessDirectory
        ])
    }

    @Transactional
    def savemap(BusinessDirectory businessDirectory) {
        if (businessDirectory == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (businessDirectory.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond businessDirectory.errors, view: 'contact'
            return
        }

        businessDirectory.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = "Business directory information saved."
                redirect(action: 'contact', id: businessDirectory?.businessId)
            }
            '*' { respond businessDirectory, [status: OK] }
        }

    }

    def contact(BusinessDirectory businessDirectory) {
        businessDirectory = BusinessDirectory.findByBusinessId(params?.id)

        render(view: 'contact', model: [businessDirectory: businessDirectory])
    }

    @Transactional
    def saveContact(BusinessDirectory businessDirectory) {
        if (businessDirectory == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (businessDirectory.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond businessDirectory.errors, view: 'contact'
            return
        }

        businessDirectory.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = "Business directory information saved."
                redirect(action: 'documents', id: businessDirectory?.businessId)
            }
            '*' { respond businessDirectory, [status: OK] }
        }
    }

    def documents(BusinessDirectory businessDirectory) {
        businessDirectory = BusinessDirectory.findByBusinessId(params?.id)
        def logo = BusinessDirectoryMultimedia.findByBusinessDirectoryAndMediaType(businessDirectory, '_logo') ?: new BusinessDirectoryMultimedia()
        def comCert = BusinessDirectoryMultimedia.findAllByBusinessDirectoryAndMediaType(businessDirectory, '_compCert') ?: new BusinessDirectoryMultimedia()
        def taxCert = BusinessDirectoryMultimedia.findAllByBusinessDirectoryAndMediaType(businessDirectory, '_taxCert') ?: new BusinessDirectoryMultimedia()
        render(view: 'documents', model: [
                businessDirectory: businessDirectory,
                logo             : logo,
                comCert          : comCert,
                taxCert          : taxCert
        ])
    }

    @Transactional
    def saveDocuments(BusinessDirectory businessDirectory) {
        if (businessDirectory == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (businessDirectory.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond businessDirectory.errors, view: 'documents'
            return
        }
        businessDirectory.save flush: true

        //TODO: Send email notifying the agent has been created.

        request.withFormat {
            form multipartForm {
                flash.message = "Business directory information saved."
                redirect(action: 'index')
            }
            '*' { respond goglidesBusinessDirectory, [status: OK] }
        }
    }

    def delete(BusinessDirectory businessDirectory) {
        businessDirectory = BusinessDirectory.findByBusinessId(params?.id)
        if (businessDirectory == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        businessDirectory.status = 'trash'
        businessDirectory.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'businessDirectory.label', default: 'BusinessDirectory'), businessDirectory.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'businessDirectory.label', default: 'BusinessDirectory'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    @Transactional
    def logoUploadSuccess() {
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        def businessDirectory = BusinessDirectory.get(params.id)
        def logoDbSave = new BusinessDirectoryMultimedia(params)
        logoDbSave.businessDirectory = businessDirectory
        logoDbSave.mediaType = '_logo'
        logoDbSave.file = params.key
        logoDbSave.save(flush: true)
        render "success"
    }

    @Transactional
    def deleteLogo() {
        amazonS3Service.s3Delete(params.bucket, params.key)
        def logo = BusinessDirectoryMultimedia.findWhere(file: params.key)
        if (logo) {
            logo.delete(flush: true)
        }
        def result = [status: "success"]
        render result as JSON
    }

    def logo(BusinessDirectory goglidesBusinessDirectory) {
        def logo = BusinessDirectoryMultimedia.findWhere(businessDirectoryId: params.id.toInteger(), mediaType: '_logo');
        if (logo) {
            render '<img class="" src="' + amazonS3Service.sourceServerUrl() + logo.file + '">'
        } else {
            render ''
        }
    }

    @Transactional
    def compCertUploadSuccess() {
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        def businessDirectory = BusinessDirectory.get(params.id)
        def logoDbSave = new BusinessDirectoryMultimedia(params)
        logoDbSave.businessDirectory = businessDirectory
        logoDbSave.mediaType = '_compCert'
        logoDbSave.file = params.key
        logoDbSave.save(flush: true)
        render "success"
    }

    @Transactional
    def compCertDelete() {
        amazonS3Service.s3Delete(params.bucket, params.key)
        def logo = BusinessDirectoryMultimedia.findWhere(file: params.key)
        if (logo) {
            logo.delete(flush: true)
        }
        def result = [status: "success"]
        render result as JSON
    }

    @Transactional
    def taxCertUploadSuccess() {
        params.put("urlS3", amazonS3Service.s3Upload(params.key))
        def businessDirectory = BusinessDirectory.get(params.id)
        def logoDbSave = new BusinessDirectoryMultimedia(params)
        logoDbSave.businessDirectory = businessDirectory
        logoDbSave.mediaType = '_taxCert'
        logoDbSave.file = params.key
        logoDbSave.save(flush: true)
        render "success"
    }

    @Transactional
    def taxCertDelete() {
        amazonS3Service.s3Delete(params.bucket, params.key)
        def logo = BusinessDirectoryMultimedia.findWhere(file: params.key)
        if (logo) {
            logo.delete(flush: true)
        }
        def result = [status: "success"]
        render result as JSON
    }

    def viewTransferLog(int id) {
        def directory = BusinessDirectory.findByBusinessId(id)
        def transferLogs = BusinessDirectoryTransferLog.findWhere(businessDirectoryOld: directory, [order: 'desc', sort: 'id'])
        render(view: 'transfer-log', model: [
                transferLogs: transferLogs
        ])
    }

    @Transactional
    def changeStatus() {
        def businessDirectory = BusinessDirectory(params?.id)
        if (params?.value == 'approved') {
            businessDirectory.approved = 'approved'
            businessDirectory.status = 'publish'
            businessDirectory.save flush: true
        }
    }

    def changeClaimable() {
        def businessDirectory = BusinessDirectory.findByBusinessId(params?.businessId)
        if (businessDirectory) {
            businessDirectory.isClaimable = params?.value
            businessDirectory.save(flush: true)
            return true
        }
        return false
    }

    def searchDirectory() {
        def userInstance = userService.currentUser()
        params.max = 5
        params.max = Math.min(params.max?.toInteger() ?: 10, 100)
        params.offset = params.offset ? params.offset.toInteger() : 0
        params.type = params?.sort.encodeAsRaw()
        def directoryLists = reportBusinessDirectoryService.getDirectoryList(userInstance, params)

        render('view': 'index', model: [
                agents: directoryLists
        ])
    }
}