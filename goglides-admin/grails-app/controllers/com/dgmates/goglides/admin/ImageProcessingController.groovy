package com.dgmates.goglides.admin

import com.amazonaws.services.s3.model.CreateBucketRequest
import com.dgmates.goglides.Event
import com.dgmates.goglides.EventMultimedia
import com.dgmates.goglides.ImageProcessing
import com.dgmates.goglides.Listing
import com.dgmates.goglides.ListingMultimedia
import grails.transaction.Transactional
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

import static org.springframework.http.HttpStatus.NOT_FOUND

class ImageProcessingController {
    def imageProcessingService
    def imageProcessingAttribute="thumbnail"
    def amazonS3Service
    def amazonWebService
    static namespace = 'admin'
    def index() {

        def imageProcessing=ImageProcessing.list();
        render(view: 'index',model: [imageProcessing: imageProcessing])
    }

    def create(){
    def imageProcessing=new ImageProcessing(params)
        render(view: 'create', model: [imageProcessing:imageProcessing])
    }


    @Transactional
    def saveImageInfo(ImageProcessing imageProcessing){
        if(imageProcessing==null){
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if(imageProcessing.hasErrors()){
            transactionStatus.setRollbackOnly()
            respond imageProcessing.errors, view: 'create'
            return
        }
        imageProcessing.save(flush: true,failOnError: true)
        redirect(action: 'index')

    }


    def updateInfo(){
        def imageProcessing=ImageProcessing.get(params?.id)
        render(view: 'edit',model: [imageProcessing: imageProcessing])
    }

    @Transactional
    def updateImageInfo(){
        def imageProcessing=ImageProcessing.get(params?.id)
        imageProcessing.imageCategory=params?.imageCategory
        imageProcessing.height=Integer.parseInt(params?.height)
        imageProcessing.width=Integer.parseInt(params?.width)
        imageProcessing.quality=Integer.parseInt(params?.quality)
        imageProcessing.save(flush: true,failOnError: true)
        redirect(action: 'index')
    }

    def delete(){
        def imageProcessing=ImageProcessing.get(params?.id)
        imageProcessing.delete(flush: true, failOnError: true)
        redirect(action: 'index')
    }


    def createNewImage(){
        def imageProcessing=ImageProcessing.findByImageCategory(params?.imageCategory)
        if(imageProcessing){
            imageProcessingService.imageProcessing(params?.id,imageProcessingAttribute,imageProcessing.height,imageProcessing.width, imageProcessing?.quality)
        }
        render "success"
    }


    @RequestMapping(method = RequestMethod.POST)
    def createImage(){
        def imageProcessing=ImageProcessing.findByImageCategory(params?.imageType)
        def event=Event.get(params?.id)
        if(event && imageProcessing){
            def eventMultimeida=EventMultimedia.findByEvent(event)
            imageProcessingService.imageProcessing(eventMultimeida?.file,imageProcessingAttribute,imageProcessing.width,imageProcessing.quality)
        }
        render "success"

    }


    @RequestMapping(method = RequestMethod.POST)
    def createNewListingFeaturedImage(){
        def listing=Listing.findByReferenceNo(params?.id)
        def imageProcessing=ImageProcessing.findByImageCategory(params?.imageType)
        if(listing && imageProcessing){
            def listingFeatureImage=ListingMultimedia.findByListingAndType(listing,'_feature')
            imageProcessingService.imageProcessing(listingFeatureImage?.file,imageProcessingAttribute,imageProcessing?.width,imageProcessing.quality)
        }
        render "success"
    }


    @RequestMapping(method = RequestMethod.POST)
    def createNewListingGalleryImage(){
        def imageUrl=params?.id
        def listingMultimedia=ListingMultimedia?.findByFile(params?.id)
        def imageProcessing=ImageProcessing.findByImageCategory(params?.imageType)
        if(imageProcessing && listingMultimedia){
            imageProcessingService.imageProcessing(imageUrl,imageProcessingAttribute,imageProcessing?.width,imageProcessing.quality)
        }
        render "success"
    }


    def createNewProfileImage(){
        println("url "+params?.id)
        println("category "+params?.imageType)
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'imageProcessing.label', default: 'ImageProcessing'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
