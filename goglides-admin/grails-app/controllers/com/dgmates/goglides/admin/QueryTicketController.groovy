package com.dgmates.goglides.admin

import com.dgmates.goglides.QueryTicket

import static org.springframework.http.HttpStatus.NOT_FOUND

class QueryTicketController {
    static namespace = 'admin'

    def index(Integer max) {

        def queryTicket=QueryTicket.list(max: params.max ?: 10, offset: params.offset ?: 0)
        render(view: 'index', model: [
                queryTicket: queryTicket,
                size:QueryTicket.count()
        ])
        return

    }


    def create() {
        def queryTicket=new QueryTicket(params)
        render(view: 'create', model: [queryTicket:queryTicket])
        return
    }


    def save(QueryTicket queryTicket){
      if(queryTicket==null){
          flash.message="Please enter details"
          notFound()
      }
      queryTicket.save(failOnError:true, flush:true)
        redirect(action: 'index')
    }


    def view(int id){
        def queryTicket=QueryTicket.get(id);
        render(view: 'edit', model: [queryTicket: queryTicket])
        return
    }

    def update(){
        def queryTicket=QueryTicket.get(params?.id)
        queryTicket.name=params?.name
        queryTicket.email=params?.email
        queryTicket.phoneNumber=params?.phoneNumber
        queryTicket.message=params?.message
        queryTicket?.status=params?.status
        queryTicket.save(failOnError: true, flush:true)
        redirect(action: 'index')
    }

    def delete(){
        def queryTicket=QueryTicket?.get(params?.id)
        queryTicket.delete(flush: true, failOnError: true)
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'queryTicket.label', default: 'QueryTicket'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

}
