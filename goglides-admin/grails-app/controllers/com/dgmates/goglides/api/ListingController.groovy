package com.dgmates.goglides.api

import com.dgmates.goglides.*
import com.dgmates.goglides.UserService

import java.text.SimpleDateFormat

class ListingController {
    static namespace = "api"
    static responseFormats = ['json', 'xml']
    ListingService listingService
    AmazonS3Service amazonS3Service
    UserService userService

    def index() {
       render "test works"
    }

    def getSpecialListing() {
        def specialListings = Listing.findAllWhere(status: 'publish', isFeatured: true, isApproved: 'approved', [sort: 'id', order: 'desc', max: 3])
        respond(specialListings)
    }

    def getListingById(Long id) {
        def listing = Listing.get(id)
        respond(listing)
    }

    def getFeaturedImageById(params){
        def listing = Listing.get(params.id)
        def image = ListingMultimedia.findByTypeAndListing('_feature', listing)
        if(image != null) {
            respond ([image : image])
        }
        respond([image : null])
    }
    def getCategory(params){
        def category = Category.get(params.id)
        respond(category)
    }
    def getPricing(params){
        def listing = Listing.get(params.id)
        def price = ListingPrice.createCriteria()
        def result = price.list{
            eq('listing',listing)
            projections  {
                min('retailRate')
            }
        }
        respond(result)
    }
    def getRating(params){
        def listing = Listing.get(params.id)
        def rating = ListingReview.createCriteria()
        def result = rating.list {
            eq('listing',listing)
            projections{
                avg('rating')

            }

        }
        respond(result)
    }
    def getReview(params){
        def listing = Listing.get(params.id)
        def rating = ListingReview.createCriteria()
        def result = rating.list {
            eq('listing',listing)
            projections{
                count('comment')
            }
        }
        respond(result)
    }
    def getBusinessDirectory(params){
        def businessDir = BusinessDirectory.get(params.id)
        respond(businessDir)
    }
    def getBusinessSlug () {
        def businessDir = BusinessDirectory.get(params.id)
        respond([slug: businessDir.slug])
    }
    def getBusinessDirectoryByListingId(params){
       def listing = Listing.get(params.id)
       def businessDir = BusinessDirectory.get(listing.businessDirectory.id)
       respond(businessDir)
    }
    def getBusinessDetailsBySlug(params){
        def businessData
        def businessDir = BusinessDirectory.findWhere(slug: params?.id, status: "publish")
        def businessLogo = BusinessDirectoryMultimedia.findWhere(
            businessDirectory: businessDir,
             mediaType: '_logo')
        def reviews = ListingReview.createCriteria()
        def review = reviews.list{
            and {
                eq('businessDirectory', businessDir)
                eq('status', 'publish')
            }
            projections {
                avg('rating')
                count('id')
            }
        }

        def listings = Listing.findAllWhere(businessDirectory: businessDir, status: 'publish')
        def listingList = []
        def gallery = []
        def video = []
        listings.each { listing ->
            def priceCriteria = ListingPrice.createCriteria()
            def price = priceCriteria.list {
                and {
                    eq('listing', listing)
                    eq('status', 'publish')
                }
                projections {
                    min('retailRate')
                }
            }

            def listingReviewCriteria = ListingReview.createCriteria()
            def listingReview = listingReviewCriteria.list {
                and {
                    eq('listing', listing)
                    eq('status', 'publish')
                }
                projections {
                    avg('rating')
                    count('id')
                }
            }

            def listingMultimedia = ListingMultimedia.findAllByListing(listing)

            def listingFeatureImage

            listingMultimedia.each{ multimedia ->
                if(multimedia?.type == "_feature")
                    listingFeatureImage = multimedia?.file
                if(multimedia?.type == "_gallery")
                    gallery.add(multimedia?.file)
                if(multimedia?.type == "_video")
                    video.add(multimedia?.file)
            }

            def category = Category.get(listing.category.id)

            listingList.add([id: listing?.id, referenceNo: listing?.referenceNo, slug: listing?.slug, title: listing?.title, city: listing?.city, country: listing?.country, price: price[0], review:listingReview[0][0], reviewCount: listingReview[0][1], image: listingFeatureImage, category: category.categoryTitle])
//            render(listingList)

          businessData = [id: businessDir.id, businessId: businessDir.businessId, name: businessDir.businessName,
                            description: businessDir.businessDescription, city: businessDir.geolocationCity,
                            country: businessDir.geolocationCountryLong,
                            logo: businessLogo.file, review:review[0][0], reviewCount: review[0][1],
                            verified: businessDir.approved, listings: listingList,
                            multimedia: [gallery: gallery, video: video]]
            }
        respond(businessData)

    }
    def getBusinessIdByListingId(){
        def listing = Listing.get(params.id)
        respond([businessId : listing.businessDirectory.id])
    }
    def getBusinessLogo(params){
        def business = BusinessDirectory.get(params.id)
        def businessLogo = BusinessDirectoryMultimedia.findWhere(businessDirectory: business,mediaType: '_logo')
        respond(businessLogo)
    }
    def getListingDetailBySlug(params){
        def listingDetails =Listing.findBySlug(params.id)
        respond(listingDetails)
    }
    def getPackage(params){
        def listing = Listing.get(params.id)
        def packagelist = ListingPackage.findAllWhere(listing: listing,status: "publish")
        respond(packagelist)
    }
    def getFeaturedImageListing(params){
        def listing = Listing.get(params.id)
        def image = ListingMultimedia.findWhere(listing: listing, type: '_feature')
        respond(image)
    }
    def getDuration(params){
        def listingPackage = ListingPackage.get(params.id)
        def durations = ListingDuration.findAllWhere(listingPackage: listingPackage)
        def unAvailability = ListingAvailability.findByListingPackage(listingPackage)
        def data = [
            durations : durations,
            unAvailability : unAvailability
        ]
        respond(data)
    }
    def getSchedule(params){
        def listingPackage = ListingPackage.get(params.id)
        def schedule = ListingSchedule.findAllWhere(listingPackage: listingPackage)

        respond(schedule)
    }

    def getRandomListing() {
        def sql = "SELECT l.id,l.title, l.city, l.country, min(p.retailRate), q.categoryTitle,m.file,Count(r.comment),avg(r.rating),l.slug,l.referenceNo, l.createdAt, b.businessName, bm.file, b.slug FROM Listing l " +
                "JOIN ListingPrice p " + 
                "ON l.id = p.listing "+
                "JOIN Category q " +
                "ON q.id = l.category "+
                "JOIN ListingMultimedia m "+
                "ON l.id = m.listing " +
                "LEFT JOIN ListingReview r "+
                "ON l.id = r.listing "+
                "JOIN BusinessDirectory b ON l.businessDirectory.id = b.id " +
                "JOIN BusinessDirectoryMultimedia bm ON b.id = bm.businessDirectory.id " +
                "WHERE l.status='publish' "+
                "AND m.type = '_feature'" +
//                "WHERE r.status='publish' "+
                "GROUP BY l.id "
                // "ORDER BY RAND()"

        def randomListing = Listing.executeQuery(sql,[max:params.max, offset : params.offset])
        def count = Listing.executeQuery(sql)
//
//        def randomListings = Listing.createCriteria( ).list {
//            eq('status', 'publish')
//            isNull('transferred')
//            sqlRestriction(" is_approved = 'approved' order by RAND()")
//            maxResults(3)
//        }
        respond([randomList : randomListing, count: count.size()])
    }



    def getPilots() {
        def sql = "select u.firstname as name,u.profilePicture, u.lastname from Pilot p JOIN p.user u where u.id=p.user and p.approved='approved' and p.available='yes'"
        def pilot = Pilot.executeQuery(sql)
        respond(pilot)
    }
    def getAvailableCategoryCount(){
        def paraglidingCount
        def count = [:]
        def categories = Category.findAllByStatus('publish')
        if (categories){
            categories.each {
                category ->
                    def c=     paraglidingCount = Listing.countByCategory(category)
                    count[category?.slug] = c
            }
        }


        respond(count)
    }


    def getRatingById(params) {
        def listing = Listing.get(params.id)
        def ratingList = ListingReview.createCriteria()
        def rating = ratingList.list {
            eq('listing',listing)
            projections{
                avg('rating')
            }
        }
             respond( rating )
    }

    def getlisting(params){
        def sql ="select id,slug,referenceNo,description,address,nearestAirport from Listing where slug ='"+params.id+"'"
        def maps = Listing.executeQuery(sql)
        respond(maps)
    }

    def getIdFromSlug(params){
        def sql ="SELECT id from Listing WHERE slug='"+params.id+"'"
        def slug = Listing.executeQuery(sql)
        if(slug[0] == null){
            slug[0] = 0
        }
        respond( slug)
    }

    def getPackageByListBySlug(){
        def req = request.JSON
        def sql
        def date = new Date()
        SimpleDateFormat ft =new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss")
        def todaysDate = ft.format(date)
        if(req.from !=null && req.to != null){
            sql = "SELECT l.id,l.tripTitle,l.detailItinerary,l.packageCode,l.shortItinerary,l.tripIncludes,l.tripExcludes,p.retailRate,p.id," +
                    "lad.monday,lad.tuesday,lad.wednesday,lad.thursday,lad.friday,lad.saturday,lad.sunday" +
                    " FROM ListingPackage l" +
                    " JOIN ListingPrice p ON l.id = p.listingPackage " +
                    " JOIN ListingAvailability la ON l.id = la.listingPackage" +
                    " JOIN ListingAvailabilityDays lad ON l.id = lad.listingPackage" +
                    " WHERE l.listing.id = '"+req.id+"'" +
                    " AND l.status='publish'" +
                    " AND '" + req.from + "' BETWEEN la.fromDate AND la.toDate" +
                    " AND '" + req.to + "' BETWEEN la.fromDate AND la.toDate" +
                    " GROUP BY l.id"
        } else if(req.from !=null && req.to == null) {
            sql = "SELECT l.id,l.tripTitle,l.detailItinerary,l.packageCode,l.shortItinerary,l.tripIncludes,l.tripExcludes,p.retailRate,p.id,"+
                    "lad.monday,lad.tuesday,lad.wednesday,lad.thursday,lad.friday,lad.saturday,lad.sunday" +
                    " FROM ListingPackage l" +
                    " JOIN ListingPrice p ON l.id = p.listingPackage " +
                    " JOIN ListingAvailability la ON l.id = la.listingPackage" +
                    " JOIN ListingAvailabilityDays lad ON l.id = lad.listingPackage" +
                    " WHERE l.listing.id = '"+req.id+"'" +
                    " AND l.status='publish'" +
                    " AND '" + req.from + "' BETWEEN la.fromDate AND la.toDate" +
                    " GROUP BY l.id"
        } else {
            sql = "SELECT l.id,l.tripTitle,l.detailItinerary,l.packageCode,l.shortItinerary,l.tripIncludes,l.tripExcludes,p.retailRate,p.id," +
                    "lad.monday,lad.tuesday,lad.wednesday,lad.thursday,lad.friday,lad.saturday,lad.sunday" +
                    " FROM ListingPackage l" +
                    " JOIN ListingPrice p ON l.id = p.listingPackage " +
                    " JOIN ListingAvailability la ON l.id = la.listingPackage" +
                    " JOIN ListingAvailabilityDays lad ON l.id = lad.listingPackage" +
                    " WHERE l.listing.id = '"+req.id+"'" +
                    " AND l.status='publish'" +
                    " AND (la.toDate IS NULL" +
                    " OR '"+ todaysDate +"' BETWEEN la.fromDate AND la.toDate)" +
                    " GROUP BY l.id"

        }
        def packages = ListingPackage.executeQuery(sql, [max: req.max, offset : req.offset])
        def count = ListingPackage.executeQuery(sql)
        respond([packages : packages, count : count.size()])
    }

    def getMinRetailRateByListingId(Long id){
        def minRate = ListingPrice.executeQuery("SELECT min(lp.retailRate) FROM ListingPrice lp WHERE lp.listing.id =" + id)
        respond ([minRate : minRate[0]])
    }

    def getListingMapsBySlug(params){
        def sql ="SELECT lat,lng FROM Listing Where slug ='"+params.id+"'"
        def maps = Listing.executeQuery(sql)
        respond(maps)
    }   

    def getListingHeadById(params){
        def listHead= Listing.findWhere(slug: params.id)
        respond(listHead)
    }

    def getListingDescByID(params){

        def sql = "SELECT description FROM Listing WHERE id = '13'"
        def desc =Listing.executeQuery(sql)
        respond(raw(desc))

    }
    def getListingAddressById(){
        def sql = "SELECT city,country, nearestCity,nearestAirport FROM Listing Where id ='13'"
        def address = Listing.executeQuery(sql)
        respond(address)
    }

    /******* Review APIs *********/
    // search all, serach by ID , save and update

    def getAllReviews(Integer max){
        params.max = Math.min(max ?: 10, 100)
        respond([reviewList : ListingReview.list(params), reviewCount : ListingReview.count()])
    }

    def getAllReviewsByListingId(){
        def listing = Listing.get(params.id)
        def reviews
        def reviewCount
        if(params.star != '0') {
            reviews = ListingReview.findAllWhere(listing:listing, rating: params.star, status:"publish", [offset:params.offset,max: params.max, sort: "createdAt", order: "desc"])
            reviewCount = ListingReview.findAllWhere(listing:listing, rating: params.star, status:"publish")
        } else {
            reviews = ListingReview.findAllWhere(listing:listing, status:"publish", [offset:params.offset,max: params.max, sort: "createdAt", order: "desc"])
            reviewCount = ListingReview.findAllWhere(listing:listing, status:"publish")
        }
        respond([reviewList : reviews, reviewCount : reviewCount.size()])
    }

    def getReviewById(Long id){
        respond(ListingReview.get(id))
    }

    def addReview(ListingReview review){
        def reviewe = ListingReview.createCriteria()
        def hasReview = reviewe.list{
            like("listing",  review.listing)
            like("user", review.user)
            and {
                ne("status", "decline")
            }
            maxResults(1)
        }
        if(hasReview!=null){
            if((review.blogUrl != null && review.blogUrl !="") && !review.blogUrl.startsWith('http://')){
                review.blogUrl = 'http://' + review.blogUrl
            }
            respond (review.save(flush:true))
        }
        respond (null)
    }

    def checkReview(){
        def listing = Listing.get(params.listing)
        def user = User.get(params.user)
        def reviewe = ListingReview.createCriteria()
        def hasReview = reviewe.list{
            like("listing", listing)
            like("user", user)
            and {
                ne("status", "decline")
            }
            maxResults(1)
        }
        if(hasReview!=null){
            respond ([review: hasReview])
        } 
        respond ([review: null])
    }

    def updateReview(ListingReview review){
        if((review.blogUrl != null && review.blogUrl !="") && !review.blogUrl.startsWith('http://')){
            review.blogUrl = 'http://' + review.blogUrl
        }
        respond(review.save())
    }

    def countReviewRatingByListingId(){
        def listing = Listing.get(params.id)
        def reviews = ListingReview.findAllWhere(listing: listing, rating: params.rate, status: "publish")
        respond([count: reviews.size()])
    }

    def avgReviewRatingByListingId(){
        def listing = Listing.get(params.id)
        def avg = ListingReview.executeQuery("SELECT AVG(l.rating) AS avg FROM ListingReview l WHERE l.listing=:listing AND l.status='publish'", [listing : listing]);
        respond([avg: avg[0]])
    }

    def getAllMultimediaByReviewId(){
        def review = ListingReview.get(params.id)
        def multimedia = null
        if(review){
            multimedia = Multimedia.findAllWhere(listingReview: review)
        }
        respond([multimedia : multimedia])
    }

    def saveMultimedia(Multimedia multimedia){
        if(multimedia.fileType == '_video'){
            if((multimedia.url != null && multimedia.url !="") && !multimedia.url.startsWith('http://')){
                multimedia.url = 'http://' + multimedia.url
            }
        }
        respond([multimedia : multimedia.save()])
    }

    def updateMultimedia(Multimedia multimedia){
        if(multimedia.fileType == '_video'){
            if((multimedia.url != null && multimedia.url !="") && !multimedia.url.startsWith('http://')){
                multimedia.url = 'http://' + multimedia.url
            }
        }
        respond([multimedia: multimedia.save()])
    }

    def getMultimediaByReviewId() {
        def listingReview = ListingReview.get(params.review)
        respond ([multimedia:Multimedia.findAllWhere(listingReview: listingReview)])
    }

    def deleteMultimediaById(Long id){
        def multimedia = Multimedia.get(id)
        if(multimedia.fileType == "_image"){
            def bucket = amazonS3Service.sourceBucket()
            amazonS3Service.s3Delete(bucket, multimedia.url)
        }
        if(multimedia != null) {
            // multimedia.delete()
            if(Multimedia.executeUpdate("DELETE FROM Multimedia m WHERE m.id=:id",[id:id])) {
                respond(['status': '200'])
            } else {
                respond(['status': 'image not found'])
            }
        } else {
            respond(['status': 'image not found'])
        }
    }
    /******* End of Review APIs *********/



    /******* Listing Detail APIs *********/
    // search all by listing Id
    def getAllRoutesByListingId(Long id){
        def listing = Listing.get(id)
        respond(ListingPickupPoint.findAllByListing(listing))
    }

    def getAllCheckListsByListingId(Long id){
        def listing = Listing.get(id)
        respond(ListingChecklist.findAllByListing(listing))
    }

    def getAllPoliciesByListingId(Long id){
        def listing = Listing.get(id)
        respond(ListingPolicies.findAllByListing(listing))
    }

    def getAllCancellingPoliciesByListingId(Long id){
        def listing = Listing.get(id)
        respond(ListingCancellationPolicies.findAllByListing(listing))
    }

    def getAllRestrictionsByListingId(Long id){
        def listing = Listing.get(id)
        respond(ListingRestrictions.findAllByListing(listing))
    }

    def getRestrictionByListingId(){
        def listing = Listing.get(params.id)
        respond(ListingRestrictions.findByListing(listing))
    }
    /******* End of Listing Detail APIs *********/

    def getAvailableSit() {
        def listing = Listing.get(params.id)
        Date bdate = new SimpleDateFormat("yyyy-MM-dd").parse(params.date);
        def query = "SELECT SUM(lp.noOfGuest) as bookedSit FROM ListingPayment lp WHERE lp.listing = :listing AND DATE(lp.bookingDate) = :bdate"
        def booked = ListingPayment.executeQuery(query, [listing:listing, bdate: bdate])
        if(booked[0] != null){
            respond ([booked : booked[0]])
        } else {
            respond ([booked : 0])
        }
    }


    

    def getWishList(){
        def listing = Listing.get(params.listingId)
        def user = User.get(params.userId)
        def wishList = WishList.findWhere(listing : listing, user : user)
        respond ([wishList : wishList])
    }
    
    def saveWishList(WishList wishList){
        def list = WishList.findWhere(listing : wishList.listing, user : wishList.user)
        if(list){
            respond([wishList : null])
        } else {
            respond([wishList : wishList.save()])
        }
    }

    def deleteWishList(){
        def listing = Listing.get(params.listingId)
        def user = User.get(params.userId)
        def wishList = WishList.findWhere(listing : listing, user : user)
        if(wishList){
            if(WishList.executeUpdate("DELETE FROM WishList l WHERE l.listing = :listing AND l.user = :user",[listing : listing, user: user])
            ) {
                respond ([status : 200]) 
            }
        } else {
            respond([status : 300])
        }
        respond ([status :500])
    }

    def getSimilarListing(){
        def check = request.JSON
        def listingList = Listing.executeQuery(
                    " SELECT l.id, l.category.id, l.address, l.city, l.country, l.title, min(lp.retailRate), avg(lr.rating), bd.businessName, bd.createdAt, lm.file, c.categoryTitle, l.slug, l.createdAt, bm.file, bd.slug" +
                    " FROM Listing l" +
                    " JOIN Category c ON l.category.id = c.id" +
                    " JOIN ListingPrice lp ON lp.listing.id = l.id" +
                    " JOIN ListingReview lr ON lr.listing.id = l.id" +
                    " JOIN BusinessDirectory bd ON bd.id = l.businessDirectory.id" +
                    " JOIN BusinessDirectoryMultimedia bm ON bd.id = bm.businessDirectory.id" +
                    " JOIN ListingMultimedia lm ON lm.listing.id = l.id"+
                    " WHERE c.categoryTitle='"+ check.category +"'" +
                    " OR l.country ='"+ check.country +"'" +
                    " AND lm.type='_feature'" +
                    " AND l.status = 'publish'" +
                    " GROUP BY l.id"
                ,[max: 3] 
            )
        respond(listingPackages : listingList)
    }

    def getListingCountryAndCategory(Long id){
        def listing  = Listing.get(id)
        def category = Category.get(listing.category.id)
        respond([country : listing.country, category: category.categoryTitle])
    }

    def getListingAddress(Long id){
        def listing  = Listing.get(id)
        respond([address : listing?.city])
    }

    def getMyWishList(){
        def wishlist
        wishlist = WishList.findAllByUser(User.get(params.id))
        respond(wishlist);
    }

    def getWishListListing(){
        def listing = Listing.get(params.id)
        def price = ListingPrice.createCriteria()
        def minPrice = price.list {
            eq("listing", listing)
            projections{
                min("retailRate")
            }
        }
        def wishList =[
                "id" :listing.id,
                "title": listing.title,
                "listingCode" :listing.listingCode,
                "listingCity" :listing.city,
                "slug":listing.slug,
                "rate":minPrice
        ]
        respond(wishList)
    }



    /*Message Contact/query Api*/
    def getMessageByEmail(){
        respond ([messageList : Message.findAllWhere(email : params.email)])
    }

    def getMessageByName(){
        respond ([messageList : Message.findAllWhere(fullname: params.name)])
    }

    def getMessage(Long id){
        return ([message : Message.get(id)])
    }

    def saveMessage(Message message){
        respond ([message : message.save()])
    }

    def deleteMesssage(Long id){
        def message = Message.get(id)
        message.delete()
    }

    def getReviewPhotoById(Long id){
        def review = ListingReview.findById(id)
        def multimedia = Multimedia.findAllWhere(listingReview: review, fileType: "_image").url;
        respond(multimedia)
    }
}
