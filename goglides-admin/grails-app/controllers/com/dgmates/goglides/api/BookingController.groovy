package com.dgmates.goglides.api

import com.dgmates.goglides.BookingItems
import com.dgmates.goglides.BookingService
import com.dgmates.goglides.BusinessDirectory
import com.dgmates.goglides.Listing
import com.dgmates.goglides.ListingAddons
import com.dgmates.goglides.ListingAddonsPrice
import com.dgmates.goglides.ListingAvailability
import com.dgmates.goglides.ListingPackage
import com.dgmates.goglides.ListingPayment
import com.dgmates.goglides.ListingPolicies
import com.dgmates.goglides.ListingPrice
import com.dgmates.goglides.ListingService
import com.dgmates.goglides.ListingMultimedia
import com.dgmates.goglides.User
import com.dgmates.goglides.UserService
import com.dgmates.goglides.api.ServerService

import com.dgmates.goglides.Ticket
import com.sun.org.apache.xml.internal.utils.ListingErrorHandler
import grails.transaction.Transactional
import groovyx.net.http.*
import sun.security.krb5.internal.Ticket

import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*

import java.text.SimpleDateFormat


class BookingController {
    static namespace = "api"
    static responseFormats = ['json', 'xml']
    ListingService listingService
    BookingService bookingService
    ServerService serverService
    UserService userService

    def index() {
        return ("hello hey there")
    }

    def getPackageDetail() {
        def bookingInfo = request.JSON
        def listingPackage = ListingPackage.get(bookingInfo.packId)
        respond(listingPackage)
    }

    def getPrice() {
        def data = request.JSON
        def bookPackage = ListingPackage.get(data.packId)
        if (bookPackage.rateType == "yes") {
            def yes = ListingPrice.findWhere(
                    listingPackage: bookPackage,
                    duration: data.duration,
                    schedule: data.schedule,
                    ageGroup: "adult",
                    type: data.PackType,
                    status: "publish")
            respond(yes)
        } else {
            def no = ListingPrice.findWhere(
                    listingPackage: bookPackage,
                    duration: data.duration,
                    ageGroup: "adult",
                    type: data.PackType,
                    status: "publish")
            respond(no)
        }

    }

    def getMultimediaCost() {
        def request = request.JSON
        def listPackage = ListingPackage.get(request?.packageId)
        def ageGroup = request?.ageGroup
        def hasAddons = ListingAddons.findWhere(listing: listPackage.listing, listingPackage: listPackage)
        if(hasAddons.includedMultimedia == 'yes') {
            def multiMediaCost = ListingAddonsPrice.findWhere(addonType: "multimedia", listingPackage: listPackage, ageGroup: ageGroup)
            respond(multiMediaCost)
        }
        respond ([retailRate: 0])
    }

    def getTransportCost() {
        def request = request.JSON
        def listPackage = ListingPackage.get(request?.packageId)
        def ageGroup = request?.ageGroup
        def hasAddons = ListingAddons.findWhere(listing: listPackage.listing, listingPackage: listPackage)
        if(hasAddons.includedTransport == 'yes') {
            def transportCost = ListingAddonsPrice.findWhere(addonType: "transport", listingPackage: listPackage, ageGroup: ageGroup)
            respond(transportCost)
        }
        respond ([retailRate: 0])
    }

    def getAddonsStat(){
        def listingPackage = ListingPackage.get(params.id)
        def add = ListingAddons.findWhere(listingPackage: listingPackage)
        respond([mAdd: add.includedMultimedia, tAdd: add.includedTransport])
    }

    def test() {
        respond(test: "test")
    }

    def getpriceByAgeGroup() {
        def data = request.JSON
//        respond(data)
        def bookPackage = ListingPackage.get(data.packId)
        def price = ListingPrice.findWhere(
                listingPackage: bookPackage,
                duration: data.duration,
                ageGroup: data.ageGroup,
                type: data.PackType,
                status: "publish")
        if (!price) {
            price = ListingPrice.findWhere(
                    listingPackage: bookPackage,
                    duration: data.duration,
                    ageGroup: 'adult',
                    type: data.PackType,
                    status: "publish")
        }
        respond(price)
//
//    }
    }

    @Transactional
    def execute() {
        println("Before executePayal : " + params)
        def executePaypal = serverService.executePaypalPayment(params)
        if (executePaypal.responseStatus == 'success') {
            println("executePayal : " + executePaypal.booking)
//            bookingService.sendBookingEmail(executePaypal.booking)
        }
        return [bookingId: executePaypal.booking.id]
//        System.out.println("wow doen")
    }

    @Transactional
    def cancel(ListingPayment booking) {
//        booking.status = 'cancelled'
//        def listing = Listing.get(booking?.listingId)
//        def userId = springSecurityService.principal.id
//        if (booking?.userId != userId) {
////            notFound()
//            return
//        }
//        booking.save(flush: true)
//
//        def listingPackage = ListingPackage.get(booking?.listingPackageId)
//        def bookingItem = BookingItems.findAllByListingPayment(booking)
//        def bookingPrice = bookingService.getBookedPackagePrice(listing, listingPackage, booking)
//        render(view: 'cancel', model: [
//                booking     : booking,
//                listing     : listing,
//                bookingItem : bookingItem,
//                bookingPrice: bookingPrice])
        return
    }

    def paypalError() {
        render(view: 'error')
        return
    }

    def executeBooking() {
        def bookingInfo = request.JSON
        /* validate form*/
        def paxs = bookingInfo.pax
        def packageDetail = bookingInfo.packageDetail
        def booking = new ListingPayment()
        def bookingItem = []

        def paxCounter = 0
        def multimediaCouter = 0
        def transportCounter = 0
        def infantCounter = 0
        def childCounter = 0
        def adultCounter = 0
        def seniorCounter = 0
        def multimediaRate = 0
        def trasportRate = 10
        def infantRate = 0
        def childRate = 0
        def seniorRate = 0
        def adultRate = 0
        def totalAmount = 0

        def grandTotalAmount = 0
        def totalmm = 0
        def totaltr = 0
        def errorMsg = ""
        def paxDetail = []
        def bookingConfirm = false

        def bookedPackage = ListingPackage.get(packageDetail.packageId)
        def listing = Listing.get(bookedPackage.listingId)

        def multimediaPriceObj =  ListingAddonsPrice.findWhere(addonType: "multimedia", listingPackage: bookedPackage, ageGroup: "adult");
        def transportPriceObj = ListingAddonsPrice.findWhere(addonType: "transport", listingPackage: bookedPackage, ageGroup: "adult");
        multimediaRate = multimediaPriceObj!=null ?multimediaPriceObj.retailRate : 0
        trasportRate = transportPriceObj!=null ?transportPriceObj.retailRate : 0

        if (bookedPackage.rateType == "yes") {
            def adultR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    schedule: packageDetail.packShedule,
                    ageGroup: "adult",
                    type: packageDetail.packType,
                    status: "publish")
            adultRate = adultR!=null?adultR.retailRate : 0

            def infantR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    schedule: packageDetail.packShedule,
                    ageGroup: "infant",
                    type: packageDetail.packType,
                    status: "publish")
            infantRate = infantR!=null ? infantR.retailRate : adultRate

            def childR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    schedule: packageDetail.packShedule,
                    ageGroup: "child",
                    type: packageDetail.packType,
                    status: "publish")
            childRate = childR!=null ? childR.retailRate : adultRate
            def seniorR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    schedule: packageDetail.packShedule,
                    ageGroup: "senior",
                    type: packageDetail.packType,
                    status: "publish")
            seniorRate = seniorR!=null ? seniorR.retailRate : adultRate
        } else {
            def adultR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    ageGroup: "adult",
                    type: packageDetail.packType,
                    status: "publish")
            adultRate = adultR!=null?adultR.retailRate : 0

            def infantR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    ageGroup: "infant",
                    type: packageDetail.packType,
                    status: "publish")
            infantRate = infantR!=null ? infantR.retailRate : adultRate

            def childR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    ageGroup: "child",
                    type: packageDetail.packType,
                    status: "publish")
            childRate = childR!=null ? childR.retailRate : adultRate
            def seniorR = ListingPrice.findWhere(
                    listingPackage: bookedPackage,
                    duration: packageDetail.packduration,
                    ageGroup: "senior",
                    type: packageDetail.packType,
                    status: "publish")
            seniorRate = seniorR!=null ? seniorR.retailRate : adultRate
        }

        def valid = false
        paxs.each { curPax ->
            paxCounter++
            if (curPax.multiMedia) {
                multimediaCouter++
                totalmm += multimediaRate
            }
            if (curPax.transport) {
                transportCounter++
                totaltr += trasportRate
            }
        }
        paxs.ageGroup.each { age ->
            if (age == "infant") {
                infantCounter++
                totalAmount += infantRate
            }
            if (age == "child") {
                childCounter++
                totalAmount += childRate
            }
            if (age == "adult") {
                adultCounter++
                totalAmount += adultRate
            }
            if (age == "senior") {
                seniorCounter++
                totalAmount += seniorRate
            }
        };
        grandTotalAmount = totalAmount + totalmm + totaltr

        if (packageDetail.paxCount != paxCounter) {
            valid = false
            errorMsg += "..pax invalid"
        } else {
            valid = true
        }
        if (packageDetail.multimediaCount != multimediaCouter) {
            valid = false
            errorMsg += "..multimedia invalid"
        } else {
            valid = true
        }
        if (packageDetail.transportCount != transportCounter) {
            valid = false
            errorMsg += "..transport invalid"
        } else {
            valid = true
        }
        if (packageDetail.multimediaAmount != totalmm) {
            valid = false
            errorMsg += "..multimedia amount  invalid"
        } else {
            valid = true
        }
        if (packageDetail.transportAmount != totaltr) {
            valid = false
            errorMsg += "..transport amount invalid"
        } else {
            valid = true
        }
        if (valid) {
            def packageTotalAmount = packageDetail.paxAmount + packageDetail.multimediaAmount + packageDetail.transportAmount
            if (packageDetail.paxAmount != totalAmount && packageTotalAmount != grandTotalAmount) {
                valid = false
                errorMsg += "total  amount invalid"
            } else {
                valid = true
            }
        }

        def bookingRespond

        if (valid) {
            booking.listingPackage = bookedPackage
            booking.listing = listing
            booking.referenceNo = bookingService.generateReferenceNo()
            booking.bookingDate = packageDetail?.bookingDate
            booking.bookingTime = packageDetail?.packShedule
            booking.pickup = bookingInfo?.pickup
            booking.dropLocation = bookingInfo?.drop
            booking.duration = packageDetail?.packduration
            booking.emergencyContactNo = bookingInfo?.emergencyPerson?.contact
            booking.emergencyContactPerson = bookingInfo?.emergencyPerson?.name
            booking.paymentType = bookingInfo?.payMode
            booking.customerComment = bookingInfo?.specialRequest
            booking.guestInfant = infantCounter
            booking.guestChild = childCounter
            booking.guestAdult = adultCounter
            booking.guestSenior = seniorCounter
            booking.businessDirectory = listing?.businessDirectory
            booking.includeMultimedia = multimediaCouter
            booking.includeTransportation = transportCounter
            booking.noOfGuest = packageDetail?.paxCount
            booking.totalMultimedia = totalmm
            booking.totalTransport = totaltr
            booking.totalPrice = packageDetail?.paxAmount
            booking.grandTotal = grandTotalAmount
            def user = User.get(bookingInfo.userId)
            booking.user = user
            booking.appVersion = "new"
            if (booking.save(flush: true, failOnError: true)) {
                paxs.each { pax ->
                    paxDetail = paxs
                    bookingItem = new BookingItems()
                    bookingItem.title = pax?.title
                    bookingItem.fullName = pax?.fullName
                    bookingItem.contactNumber = pax?.contactNumber
                    bookingItem.email = pax?.email
                    bookingItem.passportNumber = pax?.lisence
                    bookingItem.nationality = pax?.nationality
                    bookingItem.weight = pax?.weight
                    bookingItem.ageGroup = pax?.ageGroup
                    bookingItem.addMultimedia = pax?.multiMedia ? "yes" : "no"
                    bookingItem.addTransport = pax?.transport ? "yes" : "no"
                    bookingItem.ticketNo = bookingService.generateTicketNo()
                    bookingItem.listingPayment = booking
                    bookingItem.multimediaCharge = pax?.multiMedia ? multimediaRate : 0
                    bookingItem.transportationCharge = pax?.transport ? trasportRate : 0
                    switch (pax?.ageGroup) {
                        case 'infant':
                            bookingItem.amount = infantRate
                            break
                        case 'child':
                            bookingItem.amount = childRate
                            break
                        case 'adult':
                            bookingItem.amount = adultRate
                            break
                        case 'senior':
                            bookingItem.amount = seniorRate
                            break
                        default:
                            bookingItem.amount = adultRate
                            break

                    }
                    bookingItem.totalAmount = bookingItem.amount + bookingItem.multimediaCharge + bookingItem.transportationCharge

                    if (bookingItem.save(flush: true, failOnError: true)) {
                        bookingConfirm = true
                    }
                }
            }

            bookingRespond = [
                    PaxCounter       : paxCounter,
                    multimediaCounter: multimediaCouter,
                    transportCounter : transportCounter,
                    infantCounter    : infantCounter,
                    childCounter     : childCounter,
                    adultCounter     : adultCounter,
                    seniorCounter    : seniorCounter,
                    totalmultimedia  : totalmm,
                    totaltransport   : totaltr,
                    totalAmount      : totalAmount,
                    valid            : valid,
                    errorMessage     : errorMsg,
                    adultRate        : adultRate,
                    childRate        : childRate,
                    infantRate       : infantRate,
                    seniorRate       : childRate,
                    booking          : booking,
                    paxDetail        : paxDetail,
                    bookingItem      : bookingItem,
                    payment          : [
                            error : false,
                            url   : '',
                            method: '',
                            value : ''
                    ]
            ]
            if (bookingConfirm) {
                switch (bookingInfo.payMode) {
                    case 'cash':
                        println('cash')
                        serverService.sendNewOrderEmail(booking)
                        break
                    case 'paypal':
                        println('paypal')
                        serverService.sendNewOrderEmail(booking)
                        def paypalResponse = serverService.processPaypalPayment(booking, listing)
                        if (paypalResponse.responseStatus == "error") {
                            errorMsg += "... " + paypalResponse.message
                            bookingRespond.errorMessage = errorMsg
                            bookingRespond.payment = true
                            println("Inside paypal response error block")
                        }
                        if (paypalResponse.responseStatus == "success") {
                            if (saveInvoiceId(booking, paypalResponse.payment.id)) {
                                bookingRespond.payment.url = paypalResponse.url ?: '/'
                                bookingRespond.payment.method = paypalResponse.method
                                bookingRespond.payment.value = paypalResponse.payment
                                bookingRespond.payment.error = false
                            }
                        }
                        break
                    case 'ESewa':
                        println('ESewa')
                        serverService.sendNewOrderEmail(booking)
                        break
                    case 'Khalti' :
                        println('Khalti')
                        serverService.sendNewOrderEmail(booking)
                        break
                    case 'iPay' :
                        println('iPay')
                        serverService.sendNewOrderEmail(booking)
                        break
                    default:
                        println('cash deflaut')
                        break
                }
            }
            //respond(message:booking)
        }

        //switch the pay mode
        respond([response: bookingRespond])

    }

    def saveInvoiceId(ListingPayment booking, String transactionId) {
        ListingPayment temp = booking
        temp.transactionId = transactionId
        booking.properties = temp
        return booking.save(flush: true)
    }

    def paymentSuccess() {
        def obj = request.JSON
        def listingPayment = null
        def temp = null
        def saved = null
        switch(obj.paymentType) {
            case 'Paypal':
                listingPayment = ListingPayment.findWhere(transactionId : obj.paymentId)
                temp = listingPayment
                temp.payerId = obj.PayerID
                temp.payerToken = obj.token
                temp.paymentStatus = "paid"
                listingPayment.properties = temp
                saved = listingPayment.save(flush: true)
                if(saved){
                    serverService.sendBookingEmail(listingPayment);
                }
            break
            case 'Khalti':
                def http = new HTTPBuilder('https://khalti.com/api/payment/verify/')
                http.request(POST) {
                    requestContentType = URLENC
                    body = [token: obj.token, amount: obj.amount]
                    headers.'Authorization' = "Key "+"live_secret_key_e912db6a157c43408323c8877d0d08e1"
                    headers.'Accept' = 'application/json'
                    response.success = { resp, json ->
                        listingPayment = ListingPayment.findWhere(referenceNo : obj.referenceNo)
                        temp = listingPayment
                        temp.payerId = obj.mobile
                        temp.payerToken = obj.token
                        temp.transactionId = json.idx
                        temp.paymentStatus = "paid"
                        listingPayment.properties = temp
                        saved = listingPayment.save(flush: true)
                        if(saved){
                            serverService.sendBookingEmail(listingPayment);
                        }
                    }
                }
            break
        }
        respond([id : saved.referenceNo])
    }

    def cancelPayment() {
        def obj = request.JSON
        def listingPayment = null
        def temp = null
        def saved = null
        switch(obj.paymentType) {
            case 'Paypal':
                listingPayment = ListingPayment.get(obj.id)
                temp = listingPayment
                temp.payerToken = obj.token
                temp.paymentStatus = "cancelled"
                listingPayment.properties = temp
                saved = listingPayment.save(flush: true)
            break
            case 'Khalti':
                // def http = new HTTPBuilder('https://khalti.com/api/payment/verify/')
                // http.request(POST) {
                //     requestContentType = URLENC
                //     body = [token: obj.token, amount: obj.amount]
                //     headers.'Authorization' = "Key "+"test_secret_key_c5b2c65457b94165bfbd79df410c0d25"
                //     headers.'Accept' = 'application/json'
                //     response.success = { resp, json ->
                //         listingPayment = ListingPayment.findWhere(referenceNo : obj.referenceNo)
                //         temp = listingPayment
                //         temp.payerId = obj.mobile
                //         temp.payerToken = obj.token
                //         temp.transactionId = json.idx
                //         temp.paymentStatus = "paid"
                //         listingPayment.properties = temp
                //         saved = listingPayment.save(flush: true)
                //     }
                // }
            break
        }
    }

    def printInvoice() {
        def listingPayment = ListingPayment.findByReferenceNo(params.id);
        if( listingPayment != null) {
            def bookinInfo = [
                    bookingDate        : listingPayment.createdAt,
                    invoiceNo          : listingPayment.invoiceNo,
                    bookingRef         : listingPayment.referenceNo,
                    payMode            : listingPayment.paymentType,
                    reservationDate    : listingPayment.bookingDate,
                    reservationTime    : listingPayment.bookingTime,
                    reservationDuration: listingPayment.duration,
                    paidStatus         : listingPayment.paymentStatus,
                    pickUp             : listingPayment.pickup,
                    drop               : listingPayment.dropLocation,
                    specialRequest     : listingPayment.customerComment,
                    payStatus          : listingPayment.paymentStatus,
                    grandTotal         : listingPayment.grandTotal,

            ]

            def bookingItem = BookingItems.findAllWhere(listingPayment: listingPayment);

            def userRef = User.get(listingPayment.user.id)
            def userObj = userService.mapUser(userRef)
            def user = [
                    firstName: userObj.firstname,
                    lastName : userObj.lastname,
                    contact  : userObj.mobile,
                    address  : userObj.address,
                    email    : userObj.email,
            ]
            def bookPack = ListingPackage.get(listingPayment.listingPackage.id)
            def packageInfo = [
                    packageTitle: bookPack.tripTitle,
                    packageCode : bookPack.packageCode,
            ]
            def businessDirectory = BusinessDirectory.get(bookPack.listing.businessDirectory.id)
            def packageOperator = [
                    OperatorName   : businessDirectory.businessName,
                    OperatorAddress: businessDirectory.addressLine1,
                    phone1         : businessDirectory.phone1,
                    phone2         : businessDirectory.phone2,
                    email          : businessDirectory.email,
            ]
            respond("bookingInfo": bookinInfo,
                    "paxs": bookingItem,
                    "user": user,
                    "package": packageInfo,
                    "businessDirectory": packageOperator
            )
        }
        respond("bookingInfo": null)
    }

    def getTicket() {
        def listingPayment = ListingPayment.findByReferenceNo(params.id);
        if(listingPayment != null){
            def bookinInfo = [
                    bookingDate        : listingPayment.createdAt,
                    invoiceNo          : listingPayment.invoiceNo,
                    bookingRef         : listingPayment.referenceNo,
                    payMode            : listingPayment.paymentType,
                    reservationDate    : listingPayment.bookingDate,
                    reservationTime    : listingPayment.bookingTime,
                    reservationDuration: listingPayment.duration,
                    paidStatus         : listingPayment.paymentStatus,
                    pickUp             : listingPayment.pickup,
                    drop               : listingPayment.dropLocation,
                    specialRequest     : listingPayment.customerComment,
                    payStatus          : listingPayment.paymentStatus,
                    grandTotal         : listingPayment.grandTotal,
                    status             : listingPayment.status,
                    id                  :listingPayment.id,
                    businessDirectory   : listingPayment.businessDirectory,
                    emg_contact_person  : listingPayment.emergencyContactPerson,
                    emg_contact_no      : listingPayment.emergencyContactNo,
                    bookingStatus       : listingPayment.status
            ]


            def bookingItem = BookingItems.findAllWhere(listingPayment: listingPayment);

            def userRef = User.get(listingPayment.user.id)
            def userObj = userService.mapUser(userRef)
            def user = [
                    firstName: userObj.firstname,
                    lastName : userObj.lastname,
                    mobile   : userObj.mobile,
                    phone    : userObj.phone,
                    address  : userObj.address,
                    email    : userObj.email,
            ]
            def bookPack = ListingPackage.get(listingPayment.listingPackage.id)
            def packageInfo = [
                    packageTitle: bookPack.tripTitle,
                    packageCode : bookPack.packageCode
            ]
            def listing = Listing.get(bookPack.listing.id)
            def listingImage = ListingMultimedia.findWhere(listing: listing, type: '_feature')
            def listingInfo = [
                    listingId         : listing.id,
                    listingAddress    : listing.address,
                    listingDescription: listing.description,
                    listingTitle      : listing.title,
                    listingImage      : listingImage.file
            ]
            print(listing)
            def insurancePolicies = ListingPolicies.findWhere(listing: listing)
//            println("insurance"+insurancePolicies)
            def insurance
            if (insurancePolicies!=null){
                if (insurancePolicies.hasInsuranceCoverage=="yes"){
                    insurance =[
                            accdientInsurance : insurancePolicies.accidentalInsurance,
                            deathInsurance : insurancePolicies.deathInsurance,
                            insuranceCompany : insurancePolicies.insuranceCompany,
                    ]}
            }
            else {
                insurance =[
                        accdientInsurance : "N/A",
                        deathInsurance : "N/A",
                        insuranceCompany : "N/A",
                ]
            }
            print(insurance)
            def businessDirectory = BusinessDirectory.get(bookPack.listing.businessDirectory.id)
            def packageOperator = [
                    OperatorName   : businessDirectory.businessName,
                    OperatorAddress: businessDirectory.addressLine1,
                    phone1         : businessDirectory.phone1,
                    phone2         : businessDirectory.phone2,
                    email          : businessDirectory.email,
            ]
            respond("bookingInfo": bookinInfo,
                    "paxs": bookingItem,
                    "user": user,
                    "package": packageInfo,
                    "listing": listingInfo,
                    "businessDirectory": packageOperator,
                    "insurance" : insurance
            )
        }
        respond("bookingInfo": null)
    }

    def getMyBooking() {
        def user = User.get(params.id)
        def bookedInfo = []
        def status = 'trash'
        def bookings =ListingPayment.createCriteria().list(max: params?.max, offset: params?.offset) {
            ne("status",status)
            eq("user",user)
            order("id",'desc')
        }
        bookings.each { book ->
            def pack = ListingPackage.findById(book.listingPackage.id)
            def listing = Listing.get(book.listing.id)
            bookedInfo.push(
                    userId: user.id,
                    listingId: listing.id,
                    listingTitle: listing.title,
                    packageTitle: pack.tripTitle,
                    packageCode: pack.packageCode,
                    refCode: book.referenceNo,
                    address: listing.city,
                    country: listing.country,
                    date: book.bookingDate,
                    schedule: book.bookingTime,
                    duration: book.duration,
                    bookedAt: book.createdAt,
                    totalAmount: book.grandTotal,
                    bookingStatus: book.status,
                    businessDirectory:listing.businessDirectory,
                    listingPaymentId:book.id
            )
        }
        respond(bookedInfo)
    }

    def cancelRequest(com.dgmates.goglides.Ticket ticket) {
        def data = []
        def status = ticket?.status
        def listingPaymentId = ticket?.listingPayment
        def userRef = User.findById(ticket?.createdBy)
        def user = userService.mapUser(userRef)
        def listing = Listing.findById(ticket?.createdTo)
        def listingPackage = ListingPackage.findByListing(listing)
        def businessDirectory = BusinessDirectory.findById(listing.businessDirectory.id)
        def listingPayment = ListingPayment.findById(ticket?.listingPayment.id)

        data.push(
                email:user.email,
                title:listing.title,
                businessName:businessDirectory.businessName,
                bookedDate:listingPayment?.bookingDate,
                reason:ticket?.ticketDescription
        )
        if (ticket.save()) {
            listingPayment['status'] = status
            if (listingPayment.save(flush:true)) {
                try {
                    serverService.bookingCancelRequestEmail(data)
                    respond(message:"Cancellation Request has been sent")
                }catch(Exception e){
                    respond("Exception:"+e.message)
                }
            }
        }
    }

    def getUnAvailableDate(Listing listing){
        def listingPackage = ListingPackage.findByListing(listing)
        def listingUnavailability = ListingAvailability.findByListingAndListingPackage(listing,listingPackage)
        respond("listingUnavailability":listingUnavailability)
    }

    def getMyBookingCount(){
        def user = User.get(params?.id)
        def status = 'trash'
        def bookingCount = ListingPayment.countByUserAndStatusNotEqual(user,status)
        respond("bookingCount":bookingCount)
    }

    def ticket(Long id){
        def u = User.get(id)
//        def t = com.dgmates.goglides.Ticket.findByUser(u)
        def user = userService.mapUser(u)
        respond(user)
    }
}