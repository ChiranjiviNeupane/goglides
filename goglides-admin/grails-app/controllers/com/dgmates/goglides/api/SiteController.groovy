package com.dgmates.goglides.api

import com.dgmates.goglides.Listing


class SiteController {
    static namespace = "api"
    static responseFormats = ['json','xml']

    def getSpecialListing(){
        def specialListings = Listing.findAllWhere(status: 'publish', isFeatured: true, transferred: null, isApproved: 'approved', [sort: 'id', order: 'desc', max:3 ])
        respond(specialListings)

    }
    def index() {
        def ourPilots = Pilot.findAllWhere(approved: 'approved', visibleToPublic: 'yes', status: 'publish', [sort: 'id', order: 'desc', max: 6])

    }
}
