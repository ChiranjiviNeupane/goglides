package com.dgmates.goglides.api

import com.dgmates.goglides.Forex


class ForeignExchangeController {
    static namespace = "api"
    static responseFormats = ['json', 'xml']

    def getList(){
        def list  = Forex.list()
        respond list
    }

    def getRateByCurrencyCode(params){
        def value = Forex.findAllWhere(currencyCode:params?.ccode)
        respond value['rate']
    }
}