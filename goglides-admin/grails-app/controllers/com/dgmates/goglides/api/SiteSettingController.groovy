package com.dgmates.goglides.api

import com.dgmates.goglides.SiteSettings

class SiteSettingController {
    static namespace = "api"
    static responseFormats = ['json', 'xml']

    def getBy(params) {
        def value = SiteSettings.findBySlug(params.slug)
        respond value
    }
}