package com.dgmates.goglides.api

import com.dgmates.goglides.User
import com.dgmates.goglides.UserService
import com.dgmates.goglides.UserPreferences
import com.dgmates.goglides.AmazonS3Service

class UserController {
    static namespace = "api"
    static responseFormats = ['json', 'xml']

    UserService userService
    AmazonS3Service amazonS3Service

    def index() {
        respond(message:"hello")
//        render("hello")
    }

    def getUser(params) {
        def user = User.get(params?.id)
        respond([user: userService.mapUser(user)])
    }

    def checkUser() {
        def data = request.JSON
        def user = User.findByReferenceId(data.ref)
        if(user != null){
            respond ([user:[id: user.id, referenceId :user.referenceId]])
        } else {
            respond([user: null])
        }
    }

    def saveUser() {
        def data = request.JSON
        def user = new User()
        user.referenceId=data.ref
        user.save()
        respond ([user:[id: user.id, referenceId :user.referenceId]])
    }

    def updateKcUser() {
        def data = request.JSON
        def userRef = User.get(data.id)
        if(userRef != null){
            def user = userService.mapUser(userRef)
            if(user?.profile != null && user?.profile != ""){
                def bucket = amazonS3Service.sourceBucket()
                println("profile Image deleted")
                amazonS3Service.s3Delete(bucket, user.profile)
            }
            respond ([user: userService.update(userRef, data)])
        } else {
            respond([user: null])
        }
    }

    def reset() {
        def data = request.JSON
        def userRef = User.get(data.id)
        if(userRef != null){
            if(userService.changePassword(userRef, data.ps))
                respond ([stat: true])
        }
        respond ([stat: false])
    }
    
    def deleteProfileImage() {
        def data = request.JSON
        data.profile = "deleted"
        def userRef = User.get(data.id)
        if(userRef != null) {
            def user = userService.mapUser(userRef)
            if(user?.profile != null && user?.profile != "") {
                def bucket = amazonS3Service.sourceBucket()
                amazonS3Service.s3Delete(bucket, user.profile)
                println("profile Image deleted")
                def updateStat = userService.update(userRef, data)
                if(updateStat != null) {
                    respond([stat: "200"])
                }
            }
        } 
        respond ([user: null])
    }

    // def getRole() {
    //     respond ([roles:userService.userRole()])
    // }

    
//    def getUser(){
    def user = User.findByReferenceId("asdjkaksd")
//        def user = User.findByUsername(params.user)
//        respond(user)
//    }
//    def getUserInfo(params){
//        def userDetails = User.findByEmail(params.email)
//        respond(userDetails)
//    }



    /******* User APIs *********/
    //For Review

   def getUserProfileById(){
    def userRef = User.get(params.id)
    def user = userService.mapUser(userRef)
    respond ([url: user.profile])
   }

   def getUserNameById(){
       def userRef = User.get(params.id)
       def user = userService.mapUser(userRef)
       respond([firstname : user.firstname, lastname : user.lastname])
   }

    /******* End of User APIs *********/

    def saveAndUpdateUserCurrencyPreference(UserPreferences data){
        def user = User.findById(data.user.id)
        def u = UserPreferences.findByUser(user)
        if(u != null){
           u['currencyCode'] = data['currencyCode']
            u.save(flush:true)
            respond([message: "updated!"])
        }else {
            data.save()
            respond ([message : "saved!"])
        }
    }

    def getUserCurrencyPreference(){
        def user = User.findByReferenceId(params?.rid)
        def currencyPref = UserPreferences.findWhere(user: user)
        if(currencyPref == null){
            respond([id:"1",currencyCode:"USD",user:[id: null]])
        }else
            respond(currencyPref)
    }

}
