package com.dgmates.goglides.api

import com.dgmates.goglides.AmazonS3Service
import com.dgmates.goglides.Promotion
import com.dgmates.goglides.PromotionUser
import com.dgmates.goglides.User
import com.dgmates.goglides.UserService

class PromotionController {

    static namespace = "api"
    static responseFormats = ['json', 'xml']
    UserService userService
    AmazonS3Service amazonS3Service

    def index() {
        respond(Hello: "hello")
    }

    def winnerList() {
        def user = []
        def winnerList = []
        def i = 0
        def j = 0
        def usr = [
                userImage: "",
                user     : "",
                prize    : "",
                rank     : ""
        ]
        def serverUrl = amazonS3Service.sourceBucketCDN()
        def promotionUser
        def prize = ["Free Paragliding Ticket + T-shirt", "2 Movie Tickets + T-shirt", "1 Movie Tickets + T-shirt"]
        def rank = ['1st Winner', '2nd Winner', '3rd Winner']
        def promotion = Promotion.createCriteria().list {
            eq('status', "completed")
            order('id', 'desc')
        }
        promotion.each { p ->
            promotionUser = PromotionUser.createCriteria().list {
                eq('promotion', p)
                order('winnerRank', 'asc')
                isNotNull('winnerRank')
            }
            user.push(promotionUser)
        }
        user.each { u ->
            usr = []
            i = 0
            u.each { uu ->
                usr[i] = [
                        userImage: serverUrl+uu.userImage,
                        firstname     : userService.mapUser(User.get(uu.user.id)).firstname,
                        lastname : userService.mapUser(User.get(uu.user.id)).lastname,
                        prize    : prize[i],
                        rank     : rank[i]
                ]
                i++
            }

            winnerList[j] = [
                    date : promotion.createdAt[j],
                    image: serverUrl+promotion.bannerImage[j],
                    data : usr
            ]
            j++
        }
        respond(winnerList)
    }
}