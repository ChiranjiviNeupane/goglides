package goglides

class UrlMappings {

    static mappings = {
        "/$namespace/$controller/$action/$id?"()
        "/$namespace/$controller/$action?"()
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }
        "/"(controller: 'dashboard', action: 'index', namespace: 'admin')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}